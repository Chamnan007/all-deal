<?php
App::uses('AdvertisementsController', 'Controller');

/**
 * AdvertisementsController Test Case
 *
 */
class AdvertisementsControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.advertisement',
		'app.cinema',
		'app.location',
		'app.contact'
	);

}
