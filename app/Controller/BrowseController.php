<?php

/********************************************************************************

File Name: BrowseController.php
Description: managing browse module

Powered By: ABi Investment Group Co., Ltd,

Changed History:

    Date                Author              Description
    2014/06/10          Sim Chhayrambo      Initial Version
    2014/06/13          Sim Chhayrambo      Check deals by business status
    2015/02/23          Rattana             Randomly Deal Images to display

*********************************************************************************/

App::uses('AppController', 'Controller');

/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class BrowseController extends AppController {

/**
 * Components
 *
 * @var array
 */
    public $components = array('Paginator');
    var $uses = array('City', 'BusinessCategory', 'Location', 'GoodsCategory', 'Deal', 'Province', 'InterestsCategory');

/**
 * index method
 *
 * @return void
 */

    public  function index()
    {   

        $this->Deal->recursive = 3;
        $getForm = $this->request->query;

        $params = $this->request->params['named'];
        $category = (!empty($params['category'])) ? $params['category'] : "";

        $now = date("Y-m-d H:i:s");

        if(empty($getForm) && !empty($category)){

            $mainCategory = $category;

            $categoryInfo = $this->BusinessCategory->findBySlug($category);
            $this->set('businessCategoryInfo', $categoryInfo);
            if(!empty($categoryInfo)){
                $categoryID = $categoryInfo['BusinessCategory']['id'];
            }else{
                $categoryID = "XXX-XXX";
            }

            $deals = $this->Deal->find('all', array(
                        'joins' => array(
                            array(
                                'table' => 'tb_businesses',
                                'alias' => 'Business',
                                'type' => 'LEFT',
                                'conditions' => array(
                                    'Deal.business_id = Business.id'
                                )
                            ),
                            array(
                                'table' => 'tb_provinces',
                                'alias' => 'Province',
                                'type' => 'LEFT',
                                'conditions' => array(
                                    'Business.province = Province.province_code'
                                )
                            )
                        ),
                        'conditions' => array('Deal.status'=>1, 'Deal.valid_from <=' => $now, 'Deal.valid_to >'=>$now, 'Business.business_main_category'=>$categoryID, 'Business.status'=>1),
                        'fields' => array('Deal.*', 'Business.*', 'Province.*'),
                        'order' => array('Deal.created DESC'),
                        'limit' => $this->limit
                    ));

            $this->set('deals', $deals);

            // GET TOTAL DEALS
            $total_deals = $this->Deal->find('count', array(
                        'joins' => array(
                            array(
                                'table' => 'tb_businesses',
                                'alias' => 'Business',
                                'type' => 'LEFT',
                                'conditions' => array(
                                    'Deal.business_id = Business.id'
                                )
                            ),
                            array(
                                'table' => 'tb_provinces',
                                'alias' => 'Province',
                                'type' => 'LEFT',
                                'conditions' => array(
                                    'Business.province = Province.province_code'
                                )
                            )
                        ),
                        'conditions' => array('Deal.status'=>1, 'Deal.valid_from <='=>$now, 'Deal.valid_to >'=>$now, 'Business.business_main_category'=>$categoryID, 'Business.status'=>1),
                        'fields' => array('Deal.*', 'Business.*', 'Province.*'),
                        'order' => array('Deal.created DESC')
                    ));

            $total_pages = ceil($total_deals/$this->limit); 
            $this->set('total_deals', $total_deals);
            $this->set('total_pages', $total_pages);

        }else{

            $paramsMainCategory = (!empty($getForm['mainCategory'])) ? $getForm['mainCategory'] : "";
            $paramsCategory = (!empty($getForm['businessCategory'])) ? $getForm['businessCategory'] : "";
            $paramsLocation = (!empty($getForm['location'])) ? $getForm['location'] : "";
            $paramsInterest = (!empty($getForm['interest'])) ? $getForm['interest'] : "";
            $paramsService = (!empty($getForm['service'])) ? $getForm['service'] : "";
            $conditions = array();
            $now = date('Y-m-d H:i:s');

            $mainCategory = $paramsMainCategory;

            if(!empty($paramsMainCategory)){
                $this->set('filterMainCategory', $paramsMainCategory);

                $categoryInfo = $this->BusinessCategory->findBySlug($paramsMainCategory);
                $this->set('businessCategoryInfo', $categoryInfo);
                $categoryID = $categoryInfo['BusinessCategory']['id'];

                $arrCondition[0] = array('Business.business_main_category' => $categoryID);
            }

            if(!empty($paramsLocation)){
                $this->set('filterLocation', $paramsLocation);
                foreach ($paramsLocation as $key) {
                    if($key != "All"){
                        $city = $this->City->findByCityName($key);
                        $cityCode = $city['City']['city_code'];
                        $arrCondition[1]["OR"][] = array('Business.city' => $cityCode);
                    }
                }
            }

            if(!empty($paramsInterest)){
                $this->set('filterInterest', $paramsInterest);
                foreach ($paramsInterest as $key) {
                    if($key != "All") $arrCondition[2]["OR"][] = array('Deal.interest_category LIKE' => "%$key%");
                }
            }

            if(!empty($paramsService)){
                $this->set('filterService', $paramsService);
                foreach ($paramsService as $key) {
                    if($key != "All") $arrCondition[3]["OR"][] = array('Deal.services_category LIKE' => "%$key%");
                }
            }

            $arrCondition[4] = array('Deal.status' => 1, 'Deal.valid_from <='=>$now, 'Deal.valid_to >' => $now, 'Business.status'=>1);

            if(!empty($paramsCategory)){
                $this->set('filterCategory', $paramsCategory);
                foreach ($paramsCategory as $key) {

                    $subCategoryInfo = $this->BusinessCategory->findByCategory($key);
                    $subCategoryID = $subCategoryInfo['BusinessCategory']['id'];

                    $arrCondition[5]["OR"][] = array('Business.business_sub_category' => $subCategoryID);
                }
            }

            $conditions = $arrCondition;

            $deals = $this->Deal->find('all', array(
                        'joins' => array(
                            array(
                                'table' => 'tb_businesses',
                                'alias' => 'Business',
                                'type' => 'LEFT',
                                'conditions' => array(
                                    'Deal.business_id = Business.id'
                                )
                            ),
                            array(
                                'table' => 'tb_provinces',
                                'alias' => 'Province',
                                'type' => 'LEFT',
                                'conditions' => array(
                                    'Business.province = Province.province_code'
                                )
                            )
                        ),
                        'conditions' => $conditions,
                        'fields' => array('Deal.*', 'Business.*', 'Province.*'),
                        'order' => array('Deal.created DESC'),
                        'limit' => $this->limit
                    ));

            $this->set('deals', $deals);


            $total_deals = $this->Deal->find('count', array(
                        'joins' => array(
                            array(
                                'table' => 'tb_businesses',
                                'alias' => 'Business',
                                'type' => 'LEFT',
                                'conditions' => array(
                                    'Deal.business_id = Business.id'
                                )
                            ),
                            array(
                                'table' => 'tb_provinces',
                                'alias' => 'Province',
                                'type' => 'LEFT',
                                'conditions' => array(
                                    'Business.province = Province.province_code'
                                )
                            )
                        ),
                        'conditions' => $conditions,
                        'fields' => array('Deal.*', 'Business.*', 'Province.*'),
                        'order' => array('Deal.created DESC')
                    ));
            
            $total_pages = ceil($total_deals/$this->limit); 
            $this->set('total_deals', $total_deals);
            $this->set('total_pages', $total_pages);

        }

        if(!empty($deals)){
            
            $randKey = array_rand($deals);

            $featureDeal = $deals[$randKey];
            $this->set('featureDeal', $featureDeal);

        }

        if(!empty($paramsCategory)){

            $this->set('subCategory', $paramsCategory);

        }

        $this->set('mainCategory', $mainCategory);

        $this->set('limit', $this->limit);
    }



    public  function getMore($page = 1, $secure_token = "")
    {   
        $this->Deal->recursive = 3;

        if( !empty($secure_token) && $secure_token == $this->token ){

            $now = date("Y-m-d H:i:s");

            $params = $_REQUEST['params'];

            $paramsMainCategory = (!empty($params['mainCategory'])) ? $params['mainCategory'] : "";
            $paramsSubCategory = (!empty($params['businessCategory'])) ? $params['businessCategory'] : "";
            $paramsLocation = (!empty($params['location'])) ? $params['location'] : "";
            $paramsInterest = (!empty($params['interest'])) ? $params['interest'] : "";
            $paramsService = (!empty($params['service'])) ? $params['service'] : "";
            $conditions = array();
            $now = date('Y-m-d H:i:s');

            $mainCategory = $paramsMainCategory;

            if(!empty($paramsMainCategory)){
                $this->set('filterMainCategory', $paramsMainCategory);

                $categoryInfo = $this->BusinessCategory->findBySlug($paramsMainCategory);
                $this->set('businessCategoryInfo', $categoryInfo);
                $categoryID = $categoryInfo['BusinessCategory']['id'];

                $arrCondition[0] = array('Business.business_main_category' => $categoryID);
            }

            if(!empty($paramsLocation)){
                $this->set('filterLocation', $paramsLocation);
                foreach ($paramsLocation as $key) {
                    if($key != "All"){
                        $city = $this->City->findByCityName($key);
                        $cityCode = $city['City']['city_code'];
                        $arrCondition[1]["OR"][] = array('Business.city' => $cityCode);
                    }
                }
            }

            if(!empty($paramsInterest)){
                $this->set('filterInterest', $paramsInterest);
                foreach ($paramsInterest as $key) {
                    if($key != "All"){
                        $arrCondition[2]["OR"][] = array('Deal.interest_category LIKE' => "%$key%");
                    }
                }
            }

            if(!empty($paramsService)){
                $this->set('filterService', $paramsService);
                foreach ($paramsService as $key) {
                    if($key != "All"){
                        $arrCondition[3]["OR"][] = array('Deal.services_category LIKE' => "%$key%");
                    }
                }
            }

            $arrCondition[4] = array('Deal.status' => 1, 'Deal.valid_from <='=>$now, 'Deal.valid_to >' => $now, 'Business.status'=>1);

            if(!empty($paramsSubCategory)){
                $this->set('filterCategory', $paramsSubCategory);
                foreach ($paramsSubCategory as $key) {

                    $subCategoryInfo = $this->BusinessCategory->findByCategory($key);
                    $subCategoryID = $subCategoryInfo['BusinessCategory']['id'];

                    $arrCondition[5]["OR"][] = array('Business.business_sub_category' => $subCategoryID);
                }
            }

            $conditions = $arrCondition;

            $strDeal = "";

            $deals = $this->Deal->find('all', array(
                        'joins' => array(
                            array(
                                'table' => 'tb_businesses',
                                'alias' => 'Business',
                                'type' => 'LEFT',
                                'conditions' => array(
                                    'Deal.business_id = Business.id'
                                )
                            ),
                            array(
                                'table' => 'tb_provinces',
                                'alias' => 'Province',
                                'type' => 'LEFT',
                                'conditions' => array(
                                    'Business.province = Province.province_code'
                                )
                            )
                        ),
                        'conditions' => $conditions,
                        'fields' => array('Deal.*', 'Business.*', 'Province.*'),
                        'order' => array('Deal.created DESC'),
                        'limit' => $this->limit,
                        'page' => $page
                    ));

            foreach ($deals as $key => $value) {

                $limit_deal_title = $this->limit_deal_title;
                $limit_business_title = $this->limit_business_title;

                $split = end(explode("/", $value['Deal']['image']));
                $thumb_img =  "img/deals/thumbs/" . $split ;
                $url = $this->webroot;
                $business_name = $value['Business']['business_name'];
                $business_street = $value['Business']['street'];
                $business_location = $value['Business']['location'];
                $deal_title = $value['Deal']['title'];
                if(strlen($deal_title) > $limit_deal_title){
                    $deal_title = substr($value['Deal']['title'],0,$limit_deal_title-3).'...';   
                }
                if(strlen($business_name) > $limit_business_title){
                    $business_name = substr($business_name,0,$limit_business_title-3).'...';   
                }

                if( $other_deal_imgs = $value['Deal']['other_images'] ){
                    $imgs = json_decode($other_deal_imgs);
                    $data_img = array();

                    foreach( $imgs as $k => $img ){
                        $data_img[] = $img;
                    }

                    $selected = array_rand($data_img);

                    $thumb_img = str_replace("/deals/", "/deals/thumbs/", $data_img[$selected] );
                }

                $original = "";
                $discount = $value['Deal']['discount_category'];

                $prev_original  = 0;
                $prev_discount  = 0;

                if( $items = $value['DealItemLink'] ){

                    $max_percentage = 0;

                    foreach( $items as $k => $v ){

                        $ori        = ($v['original_price'] != 0 )?$v['original_price']:$v['ItemDetail']['price'];
                        $disc       = $v['discount'];
                        $item_id    = $v['item_id'];

                        $percentage = 100 - ( $disc * 100 / $ori );

                        if( $percentage > $max_percentage ){
                            $max_percentage = $percentage;
                            $prev_original  = $ori;
                            $prev_discount  = $disc;
                        }else if( $percentage == $max_percentage ){
                            if( $disc < $prev_discount ){
                                $prev_original  = $ori;
                                $prev_discount  = $disc;
                            }
                        }

                    }

                    $original = "$" . number_format($prev_original, 2) ;
                    $discount = "$" . number_format($prev_discount, 2);                        
                }

                $strDeal .= '<li class="col-sm-4 col-xs-4">' .
                                '<div class="new-project bg-shadow add-margin-bottom deal-img-container">' .  
                                    '<div>' . 
                                        '<a href="' . $url . 'deal/' . $value['Deal']['slug'] . '">' . 
                                            '<img src="' . $this->webroot . $thumb_img . '" title="' . $value['Deal']['title'] . '">' .
                                        '</a>' . 
                                    '</div>' . 
                                    '<div id="bx-pager">' . 
                                        '<a data-slide-index="0" href="' . $url . 'deal/' . $value['Deal']['slug'] . '"' . ' class="active">' . 
                                            '<div class="new-project-body">' . 
                                                '<h4>' . 
                                                    $deal_title . 
                                                '</h4>' . 
                                                '<i>' . 
                                                    $business_name .
                                                '</i><br>' .
                                                '<i class="icon-location">' . 
                                                    $business_location . ', ' . $value['Province']['province_name'] .
                                                '</i>' .
                                                '<div class="clearfix"></div>' .
                                                '<h4 class="price-off pull-right">'. $discount . '</h4>' .
                                                '<h4 style="color:#666; float:right; margin-right: 10px; text-decoration: line-through;">'. $original . '</h4>' .
                                            '</div>' . 
                                        '</a>' . 
                                    '</div>' . 
                                '</div>' . 
                            '</li>';

            }

            $result['params'] = $conditions;
            $result['strDeal'] = $strDeal;
            echo json_encode($result);
            exit();

        }else{
            return $this->redirect(array('controller'=>'error-page'));
        }
    }
}