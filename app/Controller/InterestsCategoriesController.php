<?php
App::uses('AdministratorAppController', 'Administrator.Controller');
/**
 * InterestsCategories Controller
 *
 * @property InterestsCategory $InterestsCategory
 * @property PaginatorComponent $Paginator
 */
class InterestsCategoriesController extends AdministratorAppController {

/**
 * Components
 *
 * @var array
 */
	var $context = 'InterestsCategory';

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->InterestsCategory->recursive = 0;
		
		$this->conditionFilter['status'] = 1;
		$this->paginate['conditions'] = $this->conditionFilter;
		$this->paginate['order'] = array("interestsCategory.category" => 'ASC' ) ;
		parent::index();
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->InterestsCategory->exists($id)) {
			throw new NotFoundException(__('Invalid interests category'));
		}
		$options = array('conditions' => array('InterestsCategory.' . $this->InterestsCategory->primaryKey => $id));
		$this->set('interestsCategory', $this->InterestsCategory->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			parent::save();
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->InterestsCategory->exists($id)) {
			throw new NotFoundException(__('Invalid interests category'));
		}
		if ($this->request->is(array('post', 'put'))) {
			parent::save($id);
		} else {
			$options = array('conditions' => array('InterestsCategory.' . $this->InterestsCategory->primaryKey => $id));
			$this->request->data = $this->InterestsCategory->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {

		if (!$this->InterestsCategory->exists($id)) {
			throw new NotFoundException(__('Invalid Category'));
		}

		$this->InterestsCategory->id = $id;
		
		if ($this->InterestsCategory->save(array('status'=>0))) 
		{
			$this->Session->setFlash(__('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button><b>Success!!!</b> The Goods Category has been deleted.</div>'));

			$obj 	= $this->InterestsCategory->findById($id);
			$logMessage = json_encode($obj);
			parent::generateLog($logMessage,' DELETE :'.$id);
		} else {
			$this->Session->setFlash(__('<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button><b>Failed!!!</b> The Goods Category could not be deleted. Please, try again.</div>'));
		}

		// var_dump($this->request->data); exit;
	
		return $this->redirect(array('action' => 'index'));
	}


}
