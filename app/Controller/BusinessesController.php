<?php

/********************************************************************************

File Name: BusinessController.php
Description: controlling Business Module

Powered By: ABi Investment Group Co., Ltd,

Changed History:

    Date                Author              Description
    2014/06/10          Sim Chhayrambo      Initial Version
    2014/06/12          Sim Chhayrambo      Add function "check_existing_business"
    2014/06/26          Sim Chhayrambo      Using default_layout instead of register_layout

*********************************************************************************/

App::uses('AdministratorAppController', 'Administrator.Controller');

App::uses('Province', 'Administrator.Model');
App::uses('City', 'Administrator.Model');
App::uses('Location', 'Administrator.Model');
App::uses('BusinessCategory', 'Administrator.Model');
App::uses('User', 'Administrator.Model');
App::uses('OperationHour', 'Administrator.Model');

App::uses('Deal', 'Administrator.Model');
App::uses('InterestsCategory', 'Administrator.Model');
App::uses('ServicesCategory', 'Administrator.Model');
App::uses('GoodsCategory', 'Administrator.Model');
App::uses('DiscountCategory', 'Administrator.Model');


/**
 * Businesses Controller
 *
 * @property Business $Business
 * @property PaginatorComponent $Paginator
 */
class BusinessesController extends AppController {

/**
 * Components
 *
 * @var array
 */
    var $context = 'Business';
    var $uses = array('Business', 'BusinessesMedia', 'BusinessCategory', 'Province', 'BusinessBranch' );

/**
 * index method
 *
 * @return void
 */


    public function index() {

        $params = $this->request->query;
        $paramsMainCategory = (!empty($params['mainCategory'])) ? $params['mainCategory'] : "";
        $paramsLocation = (!empty($params['location'])) ? $params['location'] : "";
        $paramsCategory = (!empty($params['businessCategory'])) ? $params['businessCategory'] : "";
        $paramsName = (!empty($params['businessName'])) ? $params['businessName'] : "";
        $conditions = array();
        $now = date('Y-m-d H:i:s');

        $getCategory = (!empty($params['category'])) ? $params['category'] : "";


        if(!empty($getCategory)){

            $mainCategory = $getCategory;

            $cateInfo = $this->BusinessCategory->findBySlug($getCategory);
            $this->set('businessCategoryInfo', $cateInfo);
            if(!empty($cateInfo)){
                $cateID = $cateInfo['BusinessCategory']['id'];    
            }else{
                $cateID = "XXX-XXX";
            }
            $arrCondition[0]["OR"][] = array('Business.business_main_category' => $cateID);

        }else{

            if(!empty($paramsMainCategory) ){
                $mainCategory = $paramsMainCategory;
                $this->set('filterCategory', $paramsMainCategory);
                $cateInfo = $this->BusinessCategory->findBySlug($paramsMainCategory);
                $this->set('businessCategoryInfo', $cateInfo);
                if(!empty($cateInfo)){
                    $cateID = $cateInfo['BusinessCategory']['id'];    
                }else{
                    $cateID = "XXX-XXX";
                }
                // $cateID = $cateInfo['BusinessCategory']['id'];
                $arrCondition[4] = array('Business.business_main_category' => $cateID);
            }else{
                $mainCategory = "";
            }

            if(!empty($paramsCategory)){
                $this->set('filterCategory', $paramsCategory);
                foreach ($paramsCategory as $key) {

                    $cateInfo = $this->BusinessCategory->findBySlug($key);
                    if(!empty($cateInfo)){
                        $cateID = $cateInfo['BusinessCategory']['id'];    
                    }else{
                        $cateID = "XXX-XXX";
                    }
                    // $cateID = $cateInfo['BusinessCategory']['id'];
                    $arrCondition[0]["OR"][] = array('Business.business_sub_category' => $cateID);
                }
            }

            if(!empty($paramsLocation)){
                $this->set('filterLocation', $paramsLocation);
                foreach ($paramsLocation as $key) {
                    if(!empty($key)){
                        $city = $this->City->findBySlug($key);
                        $cityCode = $city['City']['city_code'];
                        $arrCondition[1]["OR"][] = array('Business.city' => $cityCode);
                    }
                }
            }

            if(!empty($paramsName)){
                $this->set('filterName', $paramsName);
                $paramsName = trim($paramsName);
                $arrName = explode(" ", $paramsName);
                foreach ($arrName as $key => $value) {
                    if(!empty($value)){
                        $arrCondition[3]["OR"][] = array('Business.business_name LIKE' => "%$value%");        
                    }
                }
                
                
            }


        }

        $arrCondition[2] = array('Business.status' => 1);

        $conditions = $arrCondition;

        $businesses = $this->Business->find('all', array(
                        'joins' => array(
                            array(
                                'table' => 'tb_provinces',
                                'alias' => 'Provinces',
                                'type' => 'LEFT',
                                'conditions' => array(
                                    'Business.province = Provinces.province_code'
                                )
                            )
                        ),
                        'conditions'=>$conditions, 
                        'fields' => array('Business.*', 'Provinces.*'),
                        'order'=>array('Business.business_name'),
                        'limit'=>$this->limit));

        foreach ($businesses as $key => $busValue) {
            $mainCategoryInfo = $this->BusinessCategory->findById($busValue['Business']['business_main_category']);
            $businesses[$key]['CategoryInfo']['mainCategory'] = $mainCategoryInfo['BusinessCategory']['category'];

            // if(!empty($busValue['Business']['business_sub_category'])){
            //     $subCategoryInfo = $this->BusinessCategory->findById($busValue['Business']['business_sub_category']);
            //     $businesses[$key]['CategoryInfo']['subCategory'] = $subCategoryInfo['BusinessCategory']['category'];
            // }else{
            //     $businesses[$key]['CategoryInfo']['subCategory'] = "";
            // }


        }

        $this->set('businesses', $businesses);


        // GET TOTAL DEALS
        $total_business = $this->Business->find('count', array(
                        'joins' => array(
                            array(
                                'table' => 'tb_provinces',
                                'alias' => 'Provinces',
                                'type' => 'LEFT',
                                'conditions' => array(
                                    'Business.province = Provinces.province_code'
                                )
                            )
                        ),
                        'conditions'=>$conditions, 
                        'fields' => array('Business.*', 'Provinces.*'),
                        'order'=>array('Business.business_name')));

        $total_pages = ceil($total_business/$this->limit);  
        $this->set('total_business', $total_business);
        $this->set('total_pages', $total_pages);

        if(!empty($paramsCategory)){

            $this->set('subCategory', $paramsCategory);

        }

        $this->set('getCategory', $getCategory);

        $this->set('mainCategory', $mainCategory);

        $this->set('limit', $this->limit);



    }

    public function detail($slug = ""){

        // $businessInfo = $this->Business->findBySlug($slug);
        $this->Business->recursive = 3;
        $businessInfo = $this->Business->find('first', array(
                    'joins' => array(
                        array(
                            'table' => 'tb_provinces',
                            'alias' => 'Provinces',
                            'type' => 'LEFT',
                            'conditions' => array(
                                'Business.province = Provinces.province_code'
                            )
                        ),
                        array(
                            'table' => 'tb_cities',
                            'alias' => 'Cities',
                            'type' => 'LEFT',
                            'conditions' => array(
                                'Business.city = Cities.city_code'
                            )
                        )
                    ),
                    'conditions' => array(
                        'Business.slug' => $slug
                    ),
                    'fields' => array('Business.*', 'Provinces.*', 'Cities.*')
                ));

        $businessInfo['main_category'] = $this->BusinessCategory->findById($businessInfo['Business']['business_main_category']);
        $businessInfo['sub_category'] = $this->BusinessCategory->findById($businessInfo['Business']['business_sub_category']);

        $businessMedia = array();

        if(!empty($businessInfo)){

            $businessID = $businessInfo['Business']['id'];    
            $businessMedia = $this->BusinessesMedia->find('all', array('conditions'=>array('BusinessesMedia.business_id'=>$businessID)));

            // Get Current Deal
            $dealConditions = array( 'conditions' => array( 'Deal.status' => 1,
                                                            'Deal.business_id' => $businessID,
                                                            'Deal.valid_from <=' => date('Y-m-d H:i:s') ,
                                                            'Deal.valid_to >' => date('Y-m-d H:i:s') 
                                                        ),
                                     'order' => array('Deal.created' => 'DESC') );
            $this->Deal->recursive = 2;
            $deals = $this->Deal->find('all', $dealConditions);
            $businessInfo['Deals'] = $deals;

            $branches = $this->BusinessBranch->find('all', 
                                                    array( 
                                                        "conditions" => 
                                                            array( "BusinessBranch.status" => 1,
                                                                    "BusinessBranch.business_id" => $businessID ),
                                                        'order' => array( 'BusinessBranch.branch_name' => 'ASC' )
                                                    )
                                                );
            $this->set('branches', $branches);

        }
        
        $this->set('businessInfo', $businessInfo);
        $this->set('businessMedia', $businessMedia);

         // var_dump($businessInfo);exit();

    }

    public function register(){

        // $this->layout = 'register';
        
    }

    public function check_existing_business($business_name = ""){

        if(!empty($business_name)){
            $getBusiness = $this->Business->findByBusinessName($business_name);
            if(!empty($getBusiness)){
                $result = true;
            }else{
                $result = false;
            }
        }else{
            $result = false;
        }

        $data['result'] = $result;
        echo json_encode($data);
        exit();

    }

    public  function getMore($page = 1, $secure_token = "")
    {   

        if( !empty($secure_token) && $secure_token == $this->token ){

            $now = date("Y-m-d H:i:s");

            $paramsMainCategory = $paramsCategory = $paramsLocation = "";

            if(!empty($_REQUEST['params'])){

                $params = $_REQUEST['params'];

                $paramsMainCategory = (!empty($params['mainCategory'])) ? $params['mainCategory'] : "";
                $paramsCategory = (!empty($params['category'])) ? $params['category'] : "";
                $paramsLocation = (!empty($params['location'])) ? $params['location'] : "";    
                $paramsName = (!empty($params['businessName'])) ? $params['businessName'] : "";       
            }

            if(!empty($paramsMainCategory)){
                $this->set('filterCategory', $paramsMainCategory);
                $cateInfo = $this->BusinessCategory->findBySlug($paramsMainCategory);
                $this->set('businessCategoryInfo', $cateInfo);
                if(!empty($cateInfo)){
                    $cateID = $cateInfo['BusinessCategory']['id'];    
                }else{
                    $cateID = "XXX-XXX";
                }
                // $cateID = $cateInfo['BusinessCategory']['id'];
                $arrCondition[4] = array('Business.business_main_category' => $cateID);
            }

            if(!empty($paramsCategory)){
                $this->set('filterCategory', $paramsCategory);
                foreach ($paramsCategory as $key) {

                    $cateInfo = $this->BusinessCategory->findBySlug($key);
                    if(!empty($cateInfo)){
                        $cateID = $cateInfo['BusinessCategory']['id'];    
                    }else{
                        $cateID = "XXX-XXX";
                    }
                    // $cateID = $cateInfo['BusinessCategory']['id'];
                    $arrCondition[0]["OR"][] = array('Business.business_sub_category' => $cateID);
                }
            }

            if(!empty($paramsLocation)){
                $this->set('filterLocation', $paramsLocation);
                foreach ($paramsLocation as $key) {
                    if(!empty($key)){
                        $city = $this->City->findBySlug($key);
                        $cityCode = $city['City']['city_code'];
                        $arrCondition[1]["OR"][] = array('Business.city' => $cityCode);
                    }
                }
            }

            if(!empty($paramsName)){
                $this->set('filterName', $paramsName);
                $paramsName = trim($paramsName);
                $arrName = explode(" ", $paramsName);
                foreach ($arrName as $key => $value) {
                    if(!empty($value)){
                        $arrCondition[3]["OR"][] = array('Business.business_name LIKE' => "%$value%");        
                    }
                }
                
            }

            $arrCondition[2] = array('Business.status' => 1);

            $conditions = $arrCondition;

            // $businesses = $this->Business->find('all', array('conditions'=>$conditions, 'limit'=>$this->limit, 'page' => $page));

            $businesses = $this->Business->find('all', array(
                            'joins' => array(
                                array(
                                    'table' => 'tb_provinces',
                                    'alias' => 'Provinces',
                                    'type' => 'LEFT',
                                    'conditions' => array(
                                        'Business.province = Provinces.province_code'
                                    )
                                )
                            ),
                            'conditions'=>$conditions, 
                            'fields' => array('Business.*', 'Provinces.*'),
                            'order'=>array('Business.business_name'),
                            'limit'=>$this->limit,
                            'page' => $page));

            $strBusiness = "";

            foreach ($businesses as $key => $busValue) {
                $mainCategoryInfo = $this->BusinessCategory->findById($busValue['Business']['business_main_category']);
                $businesses[$key]['CategoryInfo']['mainCategory'] = $mainCategoryInfo['BusinessCategory']['category'];
            }

            foreach ($businesses as $key => $busValue) {
                if(!empty($busValue['Business']['business_sub_category'])){
                    $subCategoryInfo = $this->BusinessCategory->findById($busValue['Business']['business_sub_category']);
                    $businesses[$key]['CategoryInfo']['subCategory'] = $subCategoryInfo['BusinessCategory']['category'];
                }else{
                    $businesses[$key]['CategoryInfo']['subCategory'] = "";
                }
            }

            foreach ($businesses as $key => $value) {

                $limit_deal_title = $this->limit_deal_title;
                $limit_business_title = $this->limit_business_title;

                $logo = $value['Business']['logo'];
                $url = $this->webroot;

                if(!empty($value['CategoryInfo']['mainCategory'])){
                    $mainCategory = $value['CategoryInfo']['mainCategory'];
                }else{
                    $mainCategory = "";
                }

                if(!empty($value['CategoryInfo']['subCategory'])){
                    $subCategory = $value['CategoryInfo']['subCategory'];
                }else{
                    $subCategory = "";
                }

                if(!empty($subCategory)){
                    $strCategory = $subCategory . " - " . $mainCategory; 
                }else{
                    $strCategory = $mainCategory; 
                }

                $business_name = $value['Business']['business_name'];

                if(strlen($business_name) > $limit_deal_title){
                    $business_name = substr($business_name,0,$limit_deal_title-3).'...';   
                }

                if(strlen($strCategory) > $limit_business_title){
                    $strCategory = substr($strCategory,0,$limit_business_title-3).'...';   
                }

                $link = Router::url("/merchants/profile/" . $value['Business']['slug'], true );

                $strBusiness .= '<li class="col-sm-4 col-xs-4">' .
                                    '<div class="new-project bg-shadow add-margin-bottom business-img-container">' .  
                                        '<div class="img-container">' . 
                                            '<a href="' . $link . '">' . 
                                                '<img src="' . $url . $logo . '" title="' . $value['Business']['business_name'] . '">' .
                                            '</a>' . 
                                        '</div>' . 
                                        '<div id="bx-pager">' . 
                                            '<a data-slide-index="0" href="' . $link . '" class="active">' . 
                                                '<div class="new-project-body">' . 
                                                    '<h4>' . 
                                                        $business_name . 
                                                    '</h4>' . 
                                                    '<i>' .
                                                        $strCategory . 
                                                    '</i><br>' . 
                                                    '<i class="icon-location">' . 
                                                        $value['Business']['location'] . ", " . $value['Provinces']['province_name'] .
                                                    '</i>' . 
                                                '</div>' . 
                                            '</a>' . 
                                        '</div>' . 
                                    '</div>' . 
                                '</li>';

            }

            $result['all_business'] = $businesses;
            $result['strBusiness'] = $strBusiness;
            echo json_encode($result);
            exit();

        }else{
            return $this->redirect(array('controller'=>'error-page'));
        }

    }

    public function welcome(){
        
        $this->layout = "welcome";
    }

}