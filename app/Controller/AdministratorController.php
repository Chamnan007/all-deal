<?php

/********************************************************************************

File Name: administratorController.php
Description: extends from AppController

Powered By: ABi Investment Group Co., Ltd,

Changed History:

    Date                Author              Description
    2014/06/10          Sim Chhayrambo      Initial Version

*********************************************************************************/
App::uses('AppController', 'Controller');
/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class AdministratorController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator' );

/**
 * index method
 *
 * @return void
 */
	public function index() {
		// echo 'Hello';
		$this->redirect('/Administrator');
	}


}
