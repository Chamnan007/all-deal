<?php

/********************************************************************************

File Name: DestinationsController.php
Description: controlling destination module function

Powered By: ABi Investment Group Co., Ltd,

Changed History:

    Date                Author              Description
    2014/06/10          Sim Chhayrambo      Initial Version
    2014/06/13          Sim Chhayrambo      Add function "getMore"

*********************************************************************************/

App::uses('AdministratorAppController', 'Administrator.Controller');

App::uses('Province', 'Administrator.Model');
App::uses('City', 'Administrator.Model');
App::uses('Location', 'Administrator.Model');
App::uses('BusinessCategory', 'Administrator.Model');
App::uses('User', 'Administrator.Model');
App::uses('OperationHour', 'Administrator.Model');

App::uses('Deal', 'Administrator.Model');
App::uses('InterestsCategory', 'Administrator.Model');
App::uses('ServicesCategory', 'Administrator.Model');
App::uses('GoodsCategory', 'Administrator.Model');
App::uses('DiscountCategory', 'Administrator.Model');
/**
 * Destinations Controller
 *
 * @property Destination $Destination
 * @property PaginatorComponent $Paginator
 */
class DestinationsController extends AppController {

/**
 * Components
 *
 * @var array
 */

/**
 * index method
 *
 * @return void
 */
    var $context = 'Destination';
    var $uses = array('Deal', 'Destination', 'ServicesCategory');

    public function index() {

        $this->Deal->recursive = 3;

        $params = $this->request->params['named'];

        $getForm = $this->request->query;

        $now = date('Y-m-d H:i:s');

        $arrCondition = array();

        $this->set('getDestination', '');

        if( !empty($getForm) && isset($getForm['destination']) ){

            $destinations = $getForm['destination'];

            $this->set('filterDestination', $destinations);

            $arrCondition[0]["OR"][] = array('Deal.travel_destination LIKE' =>'%"All"%');

            foreach ($destinations as $key) {

                if($key != "All"){

                    $desInfo = $this->Destination->findBySlug($key);
                    $desID = $desInfo['Destination']['id'];
                    $arrCondition[0]["OR"][] = array('Deal.travel_destination LIKE' =>'%"' . $desID . '"%');

                }else{
                    $servicesCondition = array('ServicesCategory.slug LIKE' => '%travel%');
                    $servicesInfo = $this->ServicesCategory->find('first', array('conditions'=>$servicesCondition));
                    $travelCate = $servicesInfo['ServicesCategory']['category'];
                    // $arrCondition[0]["OR"][] = array('Deal.services_category LIKE' =>'%"Travel"%');
                    $arrCondition[0]["OR"][1] = array('Deal.services_category LIKE' => "%$travelCate%");
                    $arrCondition[0]["OR"][2] = array('Deal.services_category LIKE' => "%Travel%");
                }

            }

        }else{

            $params = $this->request->params['named'];
            $paramsDestination = (!empty($params['destination'])) ? $params['destination'] : "";

            $this->set('getDestination', $paramsDestination);

            if($paramsDestination != "All" && !empty($paramsDestination)){

                $desInfo = $this->Destination->findBySlug($paramsDestination);
                if(!empty($desInfo)){
                    $desID = $desInfo['Destination']['id'];
                }else{
                    $desID = "XXX-XXX";
                }

                $arrCondition[0]["OR"][] = array('Deal.travel_destination LIKE' =>'%"All"%');
                $arrCondition[0]["OR"][] = array('Deal.travel_destination LIKE' =>'%"' . $desID . '"%');

            }else{
                $servicesCondition = array('ServicesCategory.slug LIKE' => '%travel%');
                $servicesInfo = $this->ServicesCategory->find('first', array('conditions'=>$servicesCondition));
                $travelCate = $servicesInfo['ServicesCategory']['category'];
                // $arrCondition[0]["OR"][] = array('Deal.services_category LIKE' =>'%"Travel"%');
                $arrCondition[0]["OR"][1] = array('Deal.services_category LIKE' => "%$travelCate%");
                $arrCondition[0]["OR"][2] = array('Deal.services_category LIKE' => "%Travel%");
            }
        }

        $arrCondition[1] = array('Deal.status' => 1, 'Deal.valid_from <=' => $now, 'Deal.valid_to >' => $now, 'Business.status'=>1);
        $conditions = $arrCondition;

        // var_dump($conditions);

        // $deals = $this->Deal->find('all', array('conditions'=>$conditions, 'limit'=>$this->limit));
        // $this->set('deals', $deals);     

        $deals = $this->Deal->find('all', array(
                    'joins' => array(
                        array(
                            'table' => 'tb_businesses',
                            'alias' => 'Business',
                            'type' => 'LEFT',
                            'conditions' => array(
                                'Deal.business_id = Business.id'
                            )
                        ),
                        array(
                            'table' => 'tb_provinces',
                            'alias' => 'Province',
                            'type' => 'LEFT',
                            'conditions' => array(
                                'Business.province = Province.province_code'
                            )
                        )
                    ),
                    'conditions' => $conditions,
                    'fields' => array('Deal.*', 'Business.*', 'Province.*'),
                    'order' => array('Deal.created DESC'),
                    'limit' => $this->limit
                ));
        $this->set('deals', $deals);

        // GET TOTAL DEALS
        $total_deals = $this->Deal->find('count', array(
                    'joins' => array(
                        array(
                            'table' => 'tb_businesses',
                            'alias' => 'Business',
                            'type' => 'LEFT',
                            'conditions' => array(
                                'Deal.business_id = Business.id'
                            )
                        ),
                        array(
                            'table' => 'tb_provinces',
                            'alias' => 'Province',
                            'type' => 'LEFT',
                            'conditions' => array(
                                'Business.province = Province.province_code'
                            )
                        )
                    ),
                    'conditions' => $conditions,
                    'fields' => array('Deal.*', 'Business.*', 'Province.*'),
                    'order' => array('Deal.created DESC'),
                    'limit' => $this->limit
                ));
        $total_pages = ceil($total_deals/$this->limit); 
        $this->set('total_deals', $total_deals);
        $this->set('total_pages', $total_pages);

        $this->set('limit', $this->limit);
        
    }

    public  function getMore($page = 1, $secure_token = "")
    {   
        $this->Deal->recursive = 3;

        if( !empty($secure_token) && $secure_token == $this->token ){

            $now = date("Y-m-d H:i:s");
            $paramsDestination = "";

            if(!empty($_REQUEST['params'])){

                $params = $_REQUEST['params'];

                $paramsDestination = (!empty($params['destination'])) ? $params['destination'] : "";

            }

            $arrCondition[0]["OR"][] = array('Deal.travel_destination LIKE' =>'%"All"%');

            if(!empty($paramsDestination)){

                foreach ($paramsDestination as $key) {

                    if($key != "All"){

                        $desInfo = $this->Destination->findBySlug($key);
                        $desID = $desInfo['Destination']['id'];
                        $arrCondition[0]["OR"][] = array('Deal.travel_destination LIKE' =>'%"' . $desID . '"%');

                    }else{
                        $servicesCondition = array('ServicesCategory.slug LIKE' => '%travel%');
                        $servicesInfo = $this->ServicesCategory->find('first', array('conditions'=>$servicesCondition));
                        $travelCate = $servicesInfo['ServicesCategory']['category'];
                        // $arrCondition[0]["OR"][] = array('Deal.services_category LIKE' =>'%"Travel"%');
                        $arrCondition[0]["OR"][1] = array('Deal.services_category LIKE' => "%$travelCate%");
                        $arrCondition[0]["OR"][2] = array('Deal.services_category LIKE' => "%Travel%");
                    }

                }

            }else{
                $servicesCondition = array('ServicesCategory.slug LIKE' => '%travel%');
                $servicesInfo = $this->ServicesCategory->find('first', array('conditions'=>$servicesCondition));
                $travelCate = $servicesInfo['ServicesCategory']['category'];
                // $arrCondition[0]["OR"][] = array('Deal.services_category LIKE' =>'%"Travel"%');
                $arrCondition[0]["OR"][1] = array('Deal.services_category LIKE' => "%$travelCate%");
                $arrCondition[0]["OR"][2] = array('Deal.services_category LIKE' => "%Travel%");
            }

            $arrCondition[1] = array('Deal.status' => 1, 'Deal.valid_from <=' => $now, 'Deal.valid_to >' => $now, 'Business.status' => 1);
            $conditions = $arrCondition;

            $deals = $this->Deal->find('all', array(
                        'joins' => array(
                            array(
                                'table' => 'tb_businesses',
                                'alias' => 'Business',
                                'type' => 'LEFT',
                                'conditions' => array(
                                    'Deal.business_id = Business.id'
                                )
                            ),
                            array(
                                'table' => 'tb_provinces',
                                'alias' => 'Province',
                                'type' => 'LEFT',
                                'conditions' => array(
                                    'Business.province = Province.province_code'
                                )
                            )
                        ),
                        'conditions' => $conditions,
                        'fields' => array('Deal.*', 'Business.*', 'Province.*'),
                        'order' => array('Deal.created DESC'),
                        'limit' => $this->limit,
                        'page' => $page
                    ));

            $strDeal = "";

            foreach ($deals as $key => $value) {

                $limit_deal_title = $this->limit_deal_title;
                $limit_business_title = $this->limit_business_title;

                $split = end(explode("/", $value['Deal']['image']));
                $thumb_img =  "img/deals/thumbs/" . $split ;
                $url = $this->webroot;
                $business_name = $value['Business']['business_name'];
                $business_street = $value['Business']['street'];
                $business_location = $value['Business']['location'];
                $deal_title = $value['Deal']['title'];
                if(strlen($deal_title) > $limit_deal_title){
                    $deal_title = substr($value['Deal']['title'],0,$limit_deal_title-3).'...';   
                }
                if(strlen($business_name) > $limit_business_title){
                    $business_name = substr($business_name,0,$limit_business_title-3).'...';   
                }

                if( $other_deal_imgs = $value['Deal']['other_images'] ){
                    $imgs = json_decode($other_deal_imgs);
                    $data_img = array();

                    foreach( $imgs as $k => $img ){
                        $data_img[] = $img;
                    }

                    $selected = array_rand($data_img);

                    $thumb_img = str_replace("/deals/", "/deals/thumbs/", $data_img[$selected] );
                }

                
                $original = "";
                $discount = $value['Deal']['discount_category'];

                $prev_original  = 0;
                $prev_discount  = 0;

                if( $items = $value['DealItemLink'] ){

                    $max_percentage = 0;

                    foreach( $items as $k => $v ){

                        $ori        = ($v['original_price'] != 0 )?$v['original_price']:$v['ItemDetail']['price'];
                        $disc       = $v['discount'];
                        $item_id    = $v['item_id'];

                        $percentage = 100 - ( $disc * 100 / $ori );

                        if( $percentage > $max_percentage ){
                            $max_percentage = $percentage;
                            $prev_original  = $ori;
                            $prev_discount  = $disc;
                        }else if( $percentage == $max_percentage ){
                            if( $disc < $prev_discount ){
                                $prev_original  = $ori;
                                $prev_discount  = $disc;
                            }
                        }

                    }

                    $original = "$" . number_format($prev_original, 2) ;
                    $discount = "$" . number_format($prev_discount, 2);                        
                }

                $strDeal .= '<li class="col-sm-4 col-xs-4">' .
                                '<div class="new-project bg-shadow add-margin-bottom deal-img-container">' .  
                                    '<div>' . 
                                        '<a href="' . $url . 'deal/' . $value['Deal']['slug'] . '">' . 
                                            '<img src="' . $this->webroot . $thumb_img . '" title="' . $value['Deal']['title'] . '">' .
                                        '</a>' . 
                                    '</div>' . 
                                    '<div id="bx-pager">' . 
                                        '<a data-slide-index="0" href="' . $url . 'deal/' . $value['Deal']['slug'] . '"' . ' class="active">' . 
                                            '<div class="new-project-body">' . 
                                                '<h4>' . 
                                                    $deal_title . 
                                                '</h4>' . 
                                                '<i>' . 
                                                    $business_name .
                                                '</i><br>' .
                                                '<i class="icon-location">' . 
                                                    $business_location . ', ' . $value['Province']['province_name'] .
                                                '</i>' .
                                                '<div class="clearfix"></div>' .
                                                '<h4 class="price-off pull-right">'. $discount . '</h4>' .
                                                '<h4 style="color:#666; float:right; margin-right: 10px; text-decoration: line-through;">'. $original . '</h4>' .
                                            '</div>' . 
                                        '</a>' . 
                                    '</div>' . 
                                '</div>' . 
                            '</li>';
            }

            $result['all_deals'] = $deals;
            $result['strDeal'] = $strDeal;
            echo json_encode($result);
            exit();

        }else{
            return $this->redirect(array('controller'=>'error-page'));
        }
    }

}