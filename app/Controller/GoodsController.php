<?php

/********************************************************************************

File Name: GoodsController.php
Description: Controller for Goods

Powered By: ABi Investment Group Co., Ltd,

Changed History:

    Date                Author              Description
    2014/06/13          Sim Chhayrambo      Check deals by business status
    2014/06/23          Sim Chhayrambo      Clean up URL
    2014/06/27          Sim Chhayrambo      Add search by business name
    2014/08/11          Sim Chhayrambo      Fix load more (filter by good categories)

*********************************************************************************/

App::uses('AppController', 'Controller');

/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
*/

class GoodsController extends AppController {

/**
 * Components
 *
 * @var array
 */
    public $components = array('Paginator');
    var $uses = array('City', 'BusinessCategory', 'Location', 'GoodsCategory', 'Deal', 'Province', 'InterestsCategory', 'Goods', 'ServicesCategory');

/**
 * index method
 *
 * @return void
 */
    public  function index()
    {   
        $this->Deal->recursive = 3;

        $now = date("Y-m-d H:i:s");
        $conditions = array();

        $params = $this->request->params['named'];
        $category = (!empty($params['category'])) ? $params['category'] : "";
        $location = (!empty($params['location'])) ? $params['location'] : "";
        $filter = (!empty($params['filter'])) ? $params['filter'] : "";
        $interest = (!empty($params['interest'])) ? $params['interest'] : "";
        $service = (!empty($params['service'])) ? $params['service'] : "";

        $this->set('searchCategory', "");
        $this->set('searchLocation', "");
        $this->set('searchText', "");
        $this->set('searchCity', "");
        $this->set('filterInterest', array());
        $this->set('filterService', array());
        $this->set('namedInterest', "");
        $this->set('namedService', "");
        $this->set('title', "All Deals");

        if(!empty($params)){

            if(!empty($category)){

                $cateInfo = $this->GoodsCategory->findBySlug($category);
                if(!empty($cateInfo)){
                    $cateName = $cateInfo['GoodsCategory']['category'];    
                }else{
                    $cateName = "XXX-XXX";
                }
                
                $deals = $this->Deal->find('all', array(
                            'joins' => array(
                                array(
                                    'table' => 'tb_businesses',
                                    'alias' => 'Business',
                                    'type' => 'LEFT',
                                    'conditions' => array(
                                        'Deal.business_id = Business.id'
                                    )
                                ),
                                array(
                                    'table' => 'tb_provinces',
                                    'alias' => 'Province',
                                    'type' => 'LEFT',
                                    'conditions' => array(
                                        'Business.province = Province.province_code'
                                    )
                                )
                            ),
                            'conditions' => array(  'Deal.status'=>1, 
                                                    'Business.status'=>1, 
                                                    'Deal.valid_from <='=>$now, 
                                                    'Deal.valid_to >'=>$now, 
                                                    'Deal.goods_category LIKE'=>"%$cateName%"),
                            'fields' => array('Deal.*', 'Business.*', 'Province.*'),
                            'order' => array('Deal.created DESC'),
                            'limit' => $this->limit
                        ));

                $this->set('title', $category);
                $this->set('searchCategory', $category);
                $this->set('deals', $deals);


                // GET TOTAL DEALS
                $total_deals = $this->Deal->find('count', array(
                            'joins' => array(
                                array(
                                    'table' => 'tb_businesses',
                                    'alias' => 'Business',
                                    'type' => 'LEFT',
                                    'conditions' => array(
                                        'Deal.business_id = Business.id'
                                    )
                                ),
                                array(
                                    'table' => 'tb_provinces',
                                    'alias' => 'Province',
                                    'type' => 'LEFT',
                                    'conditions' => array(
                                        'Business.province = Province.province_code'
                                    )
                                )
                            ),
                            'conditions' => array(  'Deal.status'=>1, 
                                                    'Business.status'=>1, 
                                                    'Deal.valid_from <='=>$now, 
                                                    'Deal.valid_to >'=>$now, 
                                                    'Deal.goods_category LIKE'=>"%$cateName%"),
                            'fields' => array('Deal.*', 'Business.*', 'Province.*'),
                            'order' => array('Deal.created DESC')
                        ));
                $total_pages = ceil($total_deals/$this->limit); 
                $this->set('total_deals', $total_deals);
                $this->set('total_pages', $total_pages);
            }

            if(!empty($location)){

                if($location != "All"){

                    $city = $this->City->findBySlug($location);
                    if(!empty($city)){
                        $cityCode = $city['City']['city_code'];    
                    }else{
                        $cityCode = "XXX-XXX";
                    }
                    
                    $deals = $this->Deal->find('all', array(
                                'joins' => array(
                                    array(
                                        'table' => 'tb_businesses',
                                        'alias' => 'Business',
                                        'type' => 'LEFT',
                                        'conditions' => array(
                                            'Deal.business_id = Business.id'
                                        )
                                    ),
                                    array(
                                        'table' => 'tb_provinces',
                                        'alias' => 'Province',
                                        'type' => 'LEFT',
                                        'conditions' => array(
                                            'Business.province = Province.province_code'
                                        )
                                    )
                                ),
                                'conditions' => array('Deal.status'=>1, 'Deal.valid_from <='=>$now, 'Deal.valid_to >'=>$now, 'Business.city'=>$cityCode, 'Business.status'=>1),
                                'fields' => array('Deal.*', 'Business.*', 'Province.*'),
                                'order' => array('Deal.created DESC'),
                                'limit' => $this->limit
                            ));

                    $this->set('title', $location);
                    $this->set('searchLocation', $location);
                    $this->set('deals', $deals);

                    // GET TOTAL DEALS
                    $total_deals = $this->Deal->find('count', array(
                                'joins' => array(
                                    array(
                                        'table' => 'tb_businesses',
                                        'alias' => 'Business',
                                        'type' => 'LEFT',
                                        'conditions' => array(
                                            'Deal.business_id = Business.id'
                                        )
                                    ),
                                    array(
                                        'table' => 'tb_provinces',
                                        'alias' => 'Province',
                                        'type' => 'LEFT',
                                        'conditions' => array(
                                            'Business.province = Province.province_code'
                                        )
                                    )
                                ),
                                'conditions' => array('Deal.status'=>1, 'Deal.valid_from <='=>$now, 'Deal.valid_to >'=>$now, 'Business.city'=>$cityCode, 'Business.status'=>1),
                                'fields' => array('Deal.*', 'Business.*', 'Province.*'),
                                'order' => array('Deal.created DESC')
                            ));

                    $total_pages = ceil($total_deals/$this->limit); 
                    $this->set('total_deals', $total_deals);
                    $this->set('total_pages', $total_pages);

                }elseif ($location == "All") {

                    $deals = $this->Deal->find('all', array(
                                'joins' => array(
                                    array(
                                        'table' => 'tb_businesses',
                                        'alias' => 'Business',
                                        'type' => 'LEFT',
                                        'conditions' => array(
                                            'Deal.business_id = Business.id'
                                        )
                                    ),
                                    array(
                                        'table' => 'tb_provinces',
                                        'alias' => 'Province',
                                        'type' => 'LEFT',
                                        'conditions' => array(
                                            'Business.province = Province.province_code'
                                        )
                                    )
                                ),
                                'conditions' => array('Deal.status'=>1, 'Deal.valid_from <='=>$now, 'Deal.valid_to >'=>$now, 'Business.status'=>1),
                                'fields' => array('Deal.*', 'Business.*', 'Province.*'),
                                'order' => array('Deal.created DESC'),
                                'limit' => $this->limit
                            ));

                    $this->set('title', $location);
                    $this->set('searchLocation', $location);
                    $this->set('deals', $deals);

                    // GET TOTAL DEALS
                    $total_deals = $this->Deal->find('count', array(
                                'joins' => array(
                                    array(
                                        'table' => 'tb_businesses',
                                        'alias' => 'Business',
                                        'type' => 'LEFT',
                                        'conditions' => array(
                                            'Deal.business_id = Business.id'
                                        )
                                    ),
                                    array(
                                        'table' => 'tb_provinces',
                                        'alias' => 'Province',
                                        'type' => 'LEFT',
                                        'conditions' => array(
                                            'Business.province = Province.province_code'
                                        )
                                    )
                                ),
                                'conditions' => array('Deal.status'=>1, 'Deal.valid_from <='=>$now, 'Deal.valid_to >'=>$now, 'Business.status'=>1),
                                'fields' => array('Deal.*', 'Business.*', 'Province.*'),
                                'order' => array('Deal.created DESC')
                            ));

                    $total_pages = ceil($total_deals/$this->limit); 
                    $this->set('total_deals', $total_deals);
                    $this->set('total_pages', $total_pages);
                }
            }

            if(!empty($filter)){

                $deals = $this->Deal->find('all', array(
                            'joins' => array(
                                array(
                                    'table' => 'tb_businesses',
                                    'alias' => 'Business',
                                    'type' => 'LEFT',
                                    'conditions' => array(
                                        'Deal.business_id = Business.id'
                                    )
                                ),
                                array(
                                    'table' => 'tb_provinces',
                                    'alias' => 'Province',
                                    'type' => 'LEFT',
                                    'conditions' => array(
                                        'Business.province = Province.province_code'
                                    )
                                )
                            ),
                            'conditions' => array('Deal.status'=>1, 'Deal.valid_from <='=>$now, 'Deal.valid_to >'=>$now, 'Business.status'=>1),
                            'order' => array('Deal.created DESC'),
                            'fields' => array('Deal.*', 'Business.*', 'Province.*'),
                            'order' => array('Deal.created DESC'),
                            'limit' => $this->limit
                        ));

                $this->set('deals', $deals);

                $this->set('title', "New Deals");

                // GET TOTAL DEALS
                $total_deals = $this->Deal->find('count', array(
                            'joins' => array(
                                array(
                                    'table' => 'tb_businesses',
                                    'alias' => 'Business',
                                    'type' => 'LEFT',
                                    'conditions' => array(
                                        'Deal.business_id = Business.id'
                                    )
                                ),
                                array(
                                    'table' => 'tb_provinces',
                                    'alias' => 'Province',
                                    'type' => 'LEFT',
                                    'conditions' => array(
                                        'Business.province = Province.province_code'
                                    )
                                )
                            ),
                            'conditions' => array('Deal.status'=>1, 'Deal.valid_from <='=>$now, 'Deal.valid_to >'=>$now, 'Business.status'=>1),
                            'order' => array('Deal.created DESC'),
                            'fields' => array('Deal.*', 'Business.*', 'Province.*'),
                            'order' => array('Deal.created DESC')
                        ));

                $total_pages = ceil($total_deals/$this->limit); 
                $this->set('total_deals', $total_deals);
                $this->set('total_pages', $total_pages);
            }

            if(!empty($interest)){

                $deals = $this->Deal->find('all', array(
                            'joins' => array(
                                array(
                                    'table' => 'tb_businesses',
                                    'alias' => 'Business',
                                    'type' => 'LEFT',
                                    'conditions' => array(
                                        'Deal.business_id = Business.id'
                                    )
                                ),
                                array(
                                    'table' => 'tb_provinces',
                                    'alias' => 'Province',
                                    'type' => 'LEFT',
                                    'conditions' => array(
                                        'Business.province = Province.province_code'
                                    )
                                )
                            ),
                            'conditions' => array('Deal.status' => 1, 'Deal.valid_from <='=>$now, 'Deal.valid_to >'=>$now, 'Deal.interest_category LIKE' => "%Date Night%", 'Business.status'=>1),
                            'order' => array('Deal.created DESC'),
                            'fields' => array('Deal.*', 'Business.*', 'Province.*'),
                            'limit' => $this->limit
                        ));

                $this->set('deals', $deals);

                $this->set('title', "Date Night Deals");
                $this->set('namedInterest', 'Date Night');


                // GET TOTAL DEALS
                $total_deals = $this->Deal->find('count', array(
                            'joins' => array(
                                array(
                                    'table' => 'tb_businesses',
                                    'alias' => 'Business',
                                    'type' => 'LEFT',
                                    'conditions' => array(
                                        'Deal.business_id = Business.id'
                                    )
                                ),
                                array(
                                    'table' => 'tb_provinces',
                                    'alias' => 'Province',
                                    'type' => 'LEFT',
                                    'conditions' => array(
                                        'Business.province = Province.province_code'
                                    )
                                )
                            ),
                            'conditions' => array('Deal.status' => 1, 'Deal.valid_from <='=>$now, 'Deal.valid_to >'=>$now, 'Deal.interest_category LIKE' => "%Date Night%", 'Business.status'=>1),
                            'order' => array('Deal.created DESC'),
                            'fields' => array('Deal.*', 'Business.*', 'Province.*')
                        ));
                $total_pages = ceil($total_deals/$this->limit); 
                $this->set('total_deals', $total_deals);
                $this->set('total_pages', $total_pages);
            }

            if(!empty($service)){

                $deals = $this->Deal->find('all', array(
                            'joins' => array(
                                array(
                                    'table' => 'tb_businesses',
                                    'alias' => 'Business',
                                    'type' => 'LEFT',
                                    'conditions' => array(
                                        'Deal.business_id = Business.id'
                                    )
                                ),
                                array(
                                    'table' => 'tb_provinces',
                                    'alias' => 'Province',
                                    'type' => 'LEFT',
                                    'conditions' => array(
                                        'Business.province = Province.province_code'
                                    )
                                )
                            ),
                            'conditions' => array('Deal.status' => 1, 'Deal.valid_from <='=>$now, 'Deal.valid_to >'=>$now, 'Deal.services_category LIKE' => "%Travel%", 'Business.status'=>1),
                            'order' => array('Deal.created DESC'),
                            'fields' => array('Deal.*', 'Business.*', 'Province.*'),
                            'limit' => $this->limit
                        ));

                $this->set('deals', $deals);

                $this->set('title', "Getaway Deals");
                $this->set('namedService', 'Getaway');


                // GET TOTAL DEALS
                $total_deals = $this->Deal->find('count', array(
                            'joins' => array(
                                array(
                                    'table' => 'tb_businesses',
                                    'alias' => 'Business',
                                    'type' => 'LEFT',
                                    'conditions' => array(
                                        'Deal.business_id = Business.id'
                                    )
                                ),
                                array(
                                    'table' => 'tb_provinces',
                                    'alias' => 'Province',
                                    'type' => 'LEFT',
                                    'conditions' => array(
                                        'Business.province = Province.province_code'
                                    )
                                )
                            ),
                            'conditions' => array('Deal.status' => 1, 'Deal.valid_from <='=>$now, 'Deal.valid_to >'=>$now, 'Deal.services_category LIKE' => "%Travel%", 'Business.status'=>1),
                            'order' => array('Deal.created DESC'),
                            'fields' => array('Deal.*', 'Business.*', 'Province.*')
                        ));
                $total_pages = ceil($total_deals/$this->limit); 
                $this->set('total_deals', $total_deals);
                $this->set('total_pages', $total_pages);
            }

        }else{

            // FILTER VAR
            $filterParams = $this->request->query;
            $paramsCategory = (!empty($filterParams['category'])) ? $filterParams['category'] : "";
            $paramsLocation = (!empty($filterParams['location'])) ? $filterParams['location'] : "";
            $paramsInterest = (!empty($filterParams['interest'])) ? $filterParams['interest'] : "";
            $paramsService = (!empty($filterParams['service'])) ? $filterParams['service'] : "";

            $searchText = (!empty($filterParams['searchText'])) ? $filterParams['searchText'] : "";
            $searchCity = (!empty($filterParams['searchCity'])) ? $filterParams['searchCity'] : "";

            // trim searchText
            $searchText = trim($searchText);

            if(!empty($searchCity)){

                $arrCondition[0] = array('Deal.status' => 1, 'Deal.valid_from <='=>$now, 'Deal.valid_to >' => $now, 'Business.status'=>1);
                $arrConditionBusiness[0] = array('Deal.status' => 1, 'Deal.valid_from <='=>$now, 'Deal.valid_to >' => $now, 'Business.status'=>1);
                $arrConditionTotal[0] = array('Deal.status' => 1, 'Deal.valid_from <='=>$now, 'Deal.valid_to >' => $now, 'Business.status'=>1);

                if(!empty($searchText)){

                    $arrSearchText = explode(" ", $searchText);

                    foreach ($arrSearchText as $key => $value) {

                        if(!empty($value)){

                            $arrCondition[1]["OR"][] = array('Deal.title LIKE' => "%$value%");

                            $arrConditionBusiness[1]["OR"][] = array('Business.business_name LIKE' => "%$value%");

                            $arrConditionTotal[1]["OR"][1]["OR"][] = array('Deal.title LIKE' => "%$value%");
                            $arrConditionTotal[1]["OR"][2]["OR"][] = array('Business.business_name LIKE' => "%$value%");

                        }
                    }

                }

                if($searchCity != "all"){

                    $city = $this->City->findBySlug($searchCity);
                    $cityCode = $city['City']['city_code'];

                    $arrCondition[2] = array('Business.city' => $cityCode);
                    $arrConditionBusiness[2] = array('Business.city' => $cityCode);
                    $arrConditionTotal[2] = array('Business.city' => $cityCode);
                }

                $conditions = $arrCondition;

                $business_conditions = $arrConditionBusiness;

                $total_deals_condition = $arrConditionTotal;

                $dealsArr = $this->Deal->find('all', array(
                            'joins' => array(
                                array(
                                    'table' => 'tb_businesses',
                                    'alias' => 'Business',
                                    'type' => 'LEFT',
                                    'conditions' => array(
                                        'Deal.business_id = Business.id'
                                    )
                                ),
                                array(
                                    'table' => 'tb_provinces',
                                    'alias' => 'Province',
                                    'type' => 'LEFT',
                                    'conditions' => array(
                                        'Business.province = Province.province_code'
                                    )
                                )
                            ),
                            'conditions' => $conditions,
                            'fields' => array('Deal.*', 'Business.*', 'Province.*'),
                            'order' => array('Deal.created DESC'),
                            'limit' => $this->limit
                        ));

                $dealsBusiness = $this->Deal->find('all', array(
                            'joins' => array(
                                array(
                                    'table' => 'tb_businesses',
                                    'alias' => 'Business',
                                    'type' => 'LEFT',
                                    'conditions' => array(
                                        'Deal.business_id = Business.id'
                                    )
                                ),
                                array(
                                    'table' => 'tb_provinces',
                                    'alias' => 'Province',
                                    'type' => 'LEFT',
                                    'conditions' => array(
                                        'Business.province = Province.province_code'
                                    )
                                )
                            ),
                            'conditions' => $arrConditionBusiness,
                            'fields' => array('Deal.*', 'Business.*', 'Province.*'),
                            'order' => array('Deal.created DESC'),
                            'limit' => $this->limit
                        ));

                $deals = array_merge($dealsArr, $dealsBusiness); // merge dealsArr with dealBusiness

                $deals = array_unique($deals, SORT_REGULAR); // make array unique

                $deals = array_slice($deals, 0, $this->limit); // get first number of limit in array

                $this->set('deals', $deals);

                $this->set('searchText', $searchText);

                $this->set('searchCity', $searchCity);

                // GET TOTAL DEALS
                $total_deals = $this->Deal->find('count', array(
                            'joins' => array(
                                array(
                                    'table' => 'tb_businesses',
                                    'alias' => 'Business',
                                    'type' => 'LEFT',
                                    'conditions' => array(
                                        'Deal.business_id = Business.id'
                                    )
                                ),
                                array(
                                    'table' => 'tb_provinces',
                                    'alias' => 'Province',
                                    'type' => 'LEFT',
                                    'conditions' => array(
                                        'Business.province = Province.province_code'
                                    )
                                )
                            ),
                            'conditions' => $total_deals_condition,
                            'fields' => array('Deal.*', 'Business.*', 'Province.*'),
                            'order' => array('Deal.created DESC')
                        ));
                $total_pages = ceil($total_deals/$this->limit); 
                $this->set('total_deals', $total_deals);
                $this->set('total_pages', $total_pages);

            }else{

                $arrCondition[0] = array('Deal.status' => 1, 'Deal.valid_from <='=>$now,'Deal.valid_to >' => $now, 'Business.status'=>1);

                if(!empty($paramsCategory)){
                    $this->set('filterCategory', $paramsCategory);
                    foreach ($paramsCategory as $key) {
                        $goodsCategoryInfo = $this->GoodsCategory->findBySlug($key);
                        $goodsCategory = $goodsCategoryInfo['GoodsCategory']['category'];
                        $arrCondition[1]["OR"][] = array('Deal.goods_category LIKE' => "%$goodsCategory%");
                    }
                }

                if(!empty($paramsLocation)){
                    $this->set('filterLocation', $paramsLocation);
                    foreach ($paramsLocation as $key) {
                        if($key == "all_location") continue;
                        $city = $this->City->findBySlug($key);
                        $cityCode = $city['City']['city_code'];
                        $arrCondition[2]["OR"][] = array('Business.city' => $cityCode);
                    }
                }

                if(!empty($paramsInterest)){
                    $this->set('filterInterest', $paramsInterest);
                    foreach ($paramsInterest as $key) {
                        if($key != "All"){
                            $interestCateInfo = $this->InterestsCategory->findBySlug($key);
                            $interestCate = $interestCateInfo['InterestsCategory']['category'];
                            $arrCondition[3]["OR"][] = array('Deal.interest_category LIKE' => "%$interestCate%");  
                        } 
                    }
                }

                if(!empty($paramsService)){
                    $this->set('filterService', $paramsService);
                    foreach ($paramsService as $key) {
                        if($key != "All"){
                            $serviceCateInfo = $this->ServicesCategory->findBySlug($key);
                            $serviceCate = $serviceCateInfo['ServicesCategory']['category'];
                            $arrCondition[4]["OR"][] = array('Deal.services_category LIKE' => "%$serviceCate%");
                        } 
                    }
                }

                $conditions = $arrCondition;
                
                $deals = $this->Deal->find('all', array(
                            'joins' => array(
                                array(
                                    'table' => 'tb_businesses',
                                    'alias' => 'Business',
                                    'type' => 'LEFT',
                                    'conditions' => array(
                                        'Deal.business_id = Business.id'
                                    )
                                ),
                                array(
                                    'table' => 'tb_provinces',
                                    'alias' => 'Province',
                                    'type' => 'LEFT',
                                    'conditions' => array(
                                        'Business.province = Province.province_code'
                                    )
                                )
                            ),
                            'conditions' => $conditions,
                            'fields' => array('Deal.*', 'Business.*', 'Province.*'),
                            'order' => array('Deal.created DESC'),
                            'limit' => $this->limit
                        ));

                $this->set('deals', $deals);

                // GET TOTAL DEALS
                $total_deals = $this->Deal->find('count', array(
                            'joins' => array(
                                array(
                                    'table' => 'tb_businesses',
                                    'alias' => 'Business',
                                    'type' => 'LEFT',
                                    'conditions' => array(
                                        'Deal.business_id = Business.id'
                                    )
                                ),
                                array(
                                    'table' => 'tb_provinces',
                                    'alias' => 'Province',
                                    'type' => 'LEFT',
                                    'conditions' => array(
                                        'Business.province = Province.province_code'
                                    )
                                )
                            ),
                            'conditions' => $conditions,
                            'fields' => array('Deal.*', 'Business.*', 'Province.*'),
                            'order' => array('Deal.created DESC')
                        ));
                $total_pages = ceil($total_deals/$this->limit); 
                $this->set('total_deals', $total_deals);
                $this->set('total_pages', $total_pages);


            }

        }

        if(!empty($deals)){
            
            $randKey = array_rand($deals);

            $featureDeal = $deals[$randKey];
            $this->set('featureDeal', $featureDeal);

        }

        $this->set('limit', $this->limit);

    }

    public  function getMore($page = 1, $secure_token = ""){   

        $this->Deal->recursive = 3;

        if( !empty($secure_token) && $secure_token == $this->token ){

            $now = date("Y-m-d H:i:s");
            $strDeal = "";
            $paramsCategory = $paramsLocation = $paramsInterest = $paramsService = "";

            if(!empty($_REQUEST['params'])){
                
                $params = $_REQUEST['params'];

                $paramsCategory = (!empty($params['category'])) ? $params['category'] : "";
                $paramsLocation = (!empty($params['location'])) ? $params['location'] : "";
                $paramsInterest = (!empty($params['interest'])) ? $params['interest'] : "";
                $paramsService = (!empty($params['service'])) ? $params['service'] : "";

                $paramsSearchCity = (!empty($params['searchCity'])) ? $params['searchCity'] : "";
                $paramsSearchText = (!empty($params['searchText'])) ? $params['searchText'] : "";
            }

            $arrCondition[0] = array('Deal.status' => 1, 'Deal.valid_from <='=>$now, 'Deal.valid_to >' => $now, 'Business.status'=>1);
            
            $arrConditionBusiness[0] = array('Deal.status' => 1, 'Deal.valid_from <='=>$now, 'Deal.valid_to >' => $now, 'Business.status'=>1);

            // condition by searching name or city
            if(!empty($paramsSearchCity) && !empty($paramsSearchText)){

                if(!empty($paramsSearchText)){

                    $paramsSearchText = trim($paramsSearchText);

                    $arrSearchText = explode(" ", $paramsSearchText);

                    foreach ($arrSearchText as $key => $value) {

                        if(!empty($value)){

                            $arrCondition[1]["OR"][] = array('Deal.title LIKE' => "%$value%");

                            $arrConditionBusiness[1]["OR"][] = array('Business.business_name LIKE' => "%$value%");

                        }
                    }

                }

                if($paramsSearchCity != "all"){

                    $city = $this->City->findBySlug($paramsSearchCity);
                    $cityCode = $city['City']['city_code'];

                    $arrCondition[2] = array('Business.city' => $cityCode);
                    $arrConditionBusiness[2] = array('Business.city' => $cityCode);
                }

                $conditions = $arrCondition;

                $business_conditions = $arrConditionBusiness;

                $deals = $this->Deal->find('all', array(
                            'joins' => array(
                                array(
                                    'table' => 'tb_businesses',
                                    'alias' => 'Business',
                                    'type' => 'LEFT',
                                    'conditions' => array(
                                        'Deal.business_id = Business.id'
                                    )
                                ),
                                array(
                                    'table' => 'tb_provinces',
                                    'alias' => 'Province',
                                    'type' => 'LEFT',
                                    'conditions' => array(
                                        'Business.province = Province.province_code'
                                    )
                                )
                            ),
                            'conditions' => $conditions,
                            'fields' => array('Deal.*', 'Business.*', 'Province.*'),
                            'order' => array('Deal.created DESC'),
                            'limit' => $this->limit,
                            'page' => $page
                        ));

                $dealsBusiness = $this->Deal->find('all', array(
                            'joins' => array(
                                array(
                                    'table' => 'tb_businesses',
                                    'alias' => 'Business',
                                    'type' => 'LEFT',
                                    'conditions' => array(
                                        'Deal.business_id = Business.id'
                                    )
                                ),
                                array(
                                    'table' => 'tb_provinces',
                                    'alias' => 'Province',
                                    'type' => 'LEFT',
                                    'conditions' => array(
                                        'Business.province = Province.province_code'
                                    )
                                )
                            ),
                            'conditions' => $arrConditionBusiness,
                            'fields' => array('Deal.*', 'Business.*', 'Province.*'),
                            'order' => array('Deal.created DESC'),
                            'limit' => $this->limit,
                            'page' => $page
                        ));
                

                $result = array_merge($deals, $dealsBusiness); // merge deals with dealBusiness

                $result = array_unique($result, SORT_REGULAR); // make array unique

                $result = array_slice($result, 0, $this->limit); // get first number of limit in array

                $deals = $result;

            // condition by search button
            }else{

                if(!empty($paramsCategory)){
                    foreach ($paramsCategory as $key) {
                        // $arrCondition[1]["OR"][] = array('Deal.goods_category LIKE' => "%$key%");
                        // 2014-08-11
                        $cateInfo = $this->GoodsCategory->findBySlug($key);
                        if(!empty($cateInfo)){
                            $cateName = $cateInfo['GoodsCategory']['category'];    
                        }else{
                            $cateName = "XXX-XXX";
                        }
                        $arrCondition[1]["OR"][] = array('Deal.goods_category LIKE' => "%$cateName%");
                        // END
                    }
                }

                if(!empty($paramsLocation)){
                    foreach ($paramsLocation as $key) {
                        if($key == "all_location" || $key == "All" ) continue;
                        $city = $this->City->findBySlug($key);
                        $cityCode = $city['City']['city_code'];
                        $arrCondition[2]["OR"][] = array('Business.city' => $cityCode);
                    }
                }

                if(!empty($paramsInterest)){
                    foreach ($paramsInterest as $key) {
                        if($key == "All" ) continue;
                        $interestCateInfo = $this->InterestsCategory->findBySlug($key);
                        $interestCate = $interestCateInfo['InterestsCategory']['category'];
                        $arrCondition[3]["OR"][] = array('Deal.interest_category LIKE' => "%$interestCate%");
                    }
                }

                if(!empty($paramsService)){
                    foreach ($paramsService as $key) {
                        $serviceCateInfo = $this->ServicesCategory->findBySlug($key);
                        $serviceCate = $serviceCateInfo['ServicesCategory']['category'];
                        $arrCondition[4]["OR"][] = array('Deal.services_category LIKE' => "%$serviceCate%");
                    }
                }

                $conditions = $arrCondition;

                $deals = $this->Deal->find('all', array(
                            'joins' => array(
                                array(
                                    'table' => 'tb_businesses',
                                    'alias' => 'Business',
                                    'type' => 'LEFT',
                                    'conditions' => array(
                                        'Deal.business_id = Business.id'
                                    )
                                ),
                                array(
                                    'table' => 'tb_provinces',
                                    'alias' => 'Province',
                                    'type' => 'LEFT',
                                    'conditions' => array(
                                        'Business.province = Province.province_code'
                                    )
                                )
                            ),
                            'conditions' => $conditions,
                            'fields' => array('Deal.*', 'Business.*', 'Province.*'),
                            'order' => array('Deal.created DESC'),
                            'limit' => $this->limit,
                            'page' => $page
                        ));

            }

            foreach ($deals as $key => $value) {

                $limit_deal_title = $this->limit_deal_title;
                $limit_business_title = $this->limit_business_title;

                $split = end(explode("/", $value['Deal']['image']));
                $thumb_img =  "img/deals/thumbs/" . $split ;
                $url = $this->webroot;
                $business_name = $value['Business']['business_name'];
                $business_street = $value['Business']['street'];
                $business_location = $value['Business']['location'];
                $deal_title = $value['Deal']['title'];
                if(strlen($deal_title) > $limit_deal_title){
                    $deal_title = substr($value['Deal']['title'],0,$limit_deal_title-3).'...';   
                }
                if(strlen($business_name) > $limit_business_title){
                    $business_name = substr($business_name,0,$limit_business_title-3).'...';   
                }

                if( $other_deal_imgs = $value['Deal']['other_images'] ){
                    $imgs = json_decode($other_deal_imgs);
                    $data_img = array();

                    foreach( $imgs as $k => $img ){
                        $data_img[] = $img;
                    }

                    $selected = array_rand($data_img);

                    $thumb_img = str_replace("/deals/", "/deals/thumbs/", $data_img[$selected] );
                }

                $original = "";
                $discount = $value['Deal']['discount_category'];

                $prev_original  = 0;
                $prev_discount  = 0;

                if( $items = $value['DealItemLink'] ){

                    $max_percentage = 0;

                    foreach( $items as $k => $v ){

                        $ori        = ($v['original_price'] != 0 )?$v['original_price']:$v['ItemDetail']['price'];
                        $disc       = $v['discount'];
                        $item_id    = $v['item_id'];

                        $percentage = 100 - ( $disc * 100 / $ori );

                        if( $percentage > $max_percentage ){
                            $max_percentage = $percentage;
                            $prev_original  = $ori;
                            $prev_discount  = $disc;
                        }else if( $percentage == $max_percentage ){
                            if( $disc < $prev_discount ){
                                $prev_original  = $ori;
                                $prev_discount  = $disc;
                            }
                        }

                    }

                    $original = "$" . number_format($prev_original, 2) ;
                    $discount = "$" . number_format($prev_discount, 2);                        
                }

                $strDeal .= '<li class="col-sm-4 col-xs-4">' .
                                '<div class="new-project bg-shadow add-margin-bottom deal-img-container">' .  
                                    '<div>' . 
                                        '<a href="' . $url . 'deal/' . $value['Deal']['slug'] . '">' . 
                                            '<img src="' . $this->webroot . $thumb_img . '" title="' . $value['Deal']['title'] . '">' .
                                        '</a>' . 
                                    '</div>' . 
                                    '<div id="bx-pager">' . 
                                        '<a data-slide-index="0" href="' . $url . 'deal/' . $value['Deal']['slug'] . '"' . ' class="active">' . 
                                            '<div class="new-project-body">' . 
                                                '<h4>' . 
                                                    $deal_title . 
                                                '</h4>' . 
                                                '<i>' . 
                                                    $business_name .
                                                '</i><br>' .
                                                '<i class="icon-location">' . 
                                                    $business_location . ', ' . $value['Province']['province_name'] .
                                                '</i>' .
                                                '<div class="clearfix"></div>' .
                                                '<h4 class="price-off pull-right">'. $discount . '</h4>' .
                                                '<h4 style="color:#666; float:right; margin-right: 10px; text-decoration: line-through;">'. $original . '</h4>' .
                                            '</div>' . 
                                        '</a>' . 
                                    '</div>' . 
                                '</div>' . 
                            '</li>';
            }

            $result['all_deals'] = $deals;
            $result['strDeal'] = $strDeal;
            echo json_encode($result);
            exit();

        }else{
            return $this->redirect(array('controller'=>'error-page'));
        }
    }
}