<?php

/********************************************************************************

File Name: AppController.php
Description: Base Controller

Powered By: ABi Investment Group Co., Ltd,

Changed History:

    Date                Author              Description
    2014/06/13          Sim Chhayrambo      Check deals by business status
    2014/07/16          Sim Chhayrambo      Add Token
    2015/May/2015       Rattana             Add Some Global Variables

*********************************************************************************/

/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 */

App::uses('Controller', 'Controller');
App::uses('Userlog', 'Administrator.Model');
App::uses('MailConfigure', 'Administrator.Model');
App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

App::import('Vendor', 'MPDF54/mpdf');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package     app.Controller
 * @link        http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

    var $admin_email = "info@alldeal.asia";
    var $senderDisplayName = 'All Deal';

    var $gender = array("Male"=>"Male", "Female" => "Female");

    var $status = array( 
                        "0"     => array( 'color' => 'warning','status'=> "Pending"),
                        "1"     => array( 'color' => 'success','status'=> "Active"), 
                        "-1"    => array( 'color' => 'important','status'=> "Inactive")
                        );

    var $deal_status =  array( 
                        "0"     => array( 'color' => 'warning','status'=> "Pending"),
                        "1"     => array( 'color' => 'success','status'=> "Active"),
                        "-1"    => array( 'color' => 'warning','status'=> "Expired")
                        );

    var $access_level = array(
                        "5" => "Admin",
                        "4" => "Sale", 
                        "0" => "Registered User", 
                        "6" => "Super Admin"
                        );

    var $access_level_action = array(
                                "5" => "Admin",
                                "4" => "Sale", 
                                "0" => "Registered User", 
                        );

    var $member_biz_level= array(
                                "2" => "User",
                                "1" => "Admin"
                            );

    var $biz_access_level = array(
                                "1" => "Premium Member",
                                "3" => "Corporate Member"
                            );

    var $payment_method     = array(
                                "0" => "All Deal Bonus",
                                "1" => "PayPal",
                                "2" => "All Deal Credit",
                                "3" => "Balance Topup",
                                "4" => "PayGo"
                                );

    var $__CREDIT_PAYMENT       = 1;

    var $expand_request_status  = array(
                                        "0"     => array( 'color' => 'info','status'=> "Pending"),
                                        "1"     => array( 'color' => 'sucess','status'=> "Approved"),
                                        "-1"     => array( 'color' => 'important','status'=> "Rejected")
                                     ); 

    var $buy_transaction_status = array(
                                        "0"     => array( 'color' => 'warning','status'=> "Unredeemed"),
                                        "1"     => array( 'color' => 'info','status'=> "Redeemed")
                                     );

    var $dod_text   = array(
                                "0"     => array( 'color' => 'warning','status'=> "No"),
                                "1"     => array( 'color' => 'info','status'=> "Yes")
                             );

    var $pay_method     = array(    '1' => 'Bank Transfer', 
                                    '2' => 'Cash', 
                                    '3' => 'Cheque');


    var $operation_hours = array(
                                '' => '(none)',
                                '00:00:00' => '12:00am',
                                '00:30:00' => '12:30am',
                                '01:00:00' => '01:00am',
                                '01:30:00' => '01:30am',
                                '02:00:00' => '02:00am',
                                '02:30:00' => '02:30am',
                                '03:00:00' => '03:00am',
                                '03:30:00' => '03:30am',
                                '04:00:00' => '04:00am',
                                '04:30:00' => '04:30am',
                                '05:00:00' => '05:00am',
                                '05:30:00' => '05:30am',
                                '06:00:00' => '06:00am',
                                '06:30:00' => '06:30am',
                                '07:00:00' => '07:00am',
                                '07:30:00' => '07:30am',
                                '08:00:00' => '08:00am',
                                '08:30:00' => '08:30am',
                                '09:00:00' => '09:00am',
                                '09:30:00' => '09:30am',
                                '10:00:00' => '10:00am',
                                '10:30:00' => '10:30am',
                                '11:00:00' => '11:00am',
                                '11:30:00' => '11:30am',
                                '12:00:00' => '12:00pm',
                                '12:30:00' => '12:30pm',
                                '13:00:00' => '01:00pm',
                                '13:30:00' => '01:30pm',
                                '14:00:00' => '02:00pm',
                                '14:30:00' => '02:30pm',
                                '15:00:00' => '03:00pm',
                                '15:30:00' => '03:30pm',
                                '16:00:00' => '04:00pm',
                                '16:30:00' => '04:30pm',
                                '17:00:00' => '05:00pm',
                                '17:30:00' => '05:30pm',
                                '18:00:00' => '06:00pm',
                                '18:30:00' => '06:30pm',
                                '19:00:00' => '07:00pm',
                                '19:30:00' => '07:30pm',
                                '20:00:00' => '08:00pm',
                                '20:30:00' => '08:30pm',
                                '21:00:00' => '09:00pm',
                                '21:30:00' => '09:30pm',
                                '22:00:00' => '10:00pm',
                                '22:30:00' => '10:30pm',
                                '23:00:00' => '11:00pm',
                                '23:30:00' => '11:30pm'
                                );

    var $mons = array(1 => "Jan", 2 => "Feb", 3 => "Mar", 4 => "Apr", 5 => "May", 6 => "Jun", 7 => "Jul", 8 => "Aug", 9 => "Sep", 10 => "Oct", 11 => "Nov", 12 => "Dec");

    var $uses = array('City', 'BusinessCategory', 'Location', 'GoodsCategory', 'Deal', 'Province', 'InterestsCategory', 'ServicesCategory', 'Business', 'Destination');

    var $limit = 30;
    var $limit_deal_title = 21;
    var $limit_business_title = 25;

    var $max_expried_day = 7;

    var $genders_array = array(
                'male'      => 'Male',
                'female'    => 'Female'
            );

    var $context = "";    
    var $token = "07361cf61ea5c49a2025593eb485ca14092e6308";
    var $LOGGED_SESSION = NULL;
    var $__NUMBER_OF_ITEM_LIMIT = 5;

    var $appId = "4F71BFABD034QDF8BA0E319EDA874CASDW";
    var $appKey = "49A8A97A8D4EADDKSD934D40E4874E9E367943D654F10E2023EDSAD";

    var $__CART_ITEM = NULL ;

    var $__EXPIRATION_PERIOD = 7 ; // 7 Days Forward

    var $__COUPON_EXPIRATION_PERIOD = 6 ; // 6 Months after purchased

    var $__MAXIMUM_IMAGE_SIZE = 2048000; // 2MB Maximum
    
    var $dealWidth          = 705;
    var $dealHeight         = 422;

    var $access_token = "A015R1Guac9tRtz8XvIVhi6xT6Gysa87W2iKgt2f8n3.BP0" ; // For Paypal Payment

    var $MONTH_ARRAY = array( '1'   => 'January',
                              '2'   => 'February',
                              '3'   => 'March',
                              '4'   => 'April',
                              '5'   => 'May',
                              '6'   => 'June',
                              '7'   => 'July',
                              '8'   => 'August',
                              '9'   => 'September',
                              '10'  => 'October',
                              '11'  => 'November',
                              '12'  => 'December'
                            );



    var $food_drink_categories   = array( 1, 7, 11 );
    var $beauty_spa_categories   = array( 8, 29 );
    var $shopping_categories     = array( 2, 3, 4, 6, 12, 13, 15, 16, 22, 23, 24, 30, 32, 34 );
    var $things_to_do_categories = array( 7, 9, 10, 14, 21, 25, 26, 28, 31, 33 );
    var $getaways_categories     = array( 16, 17, 27, 33 );

    // PayGo Setting
    var $__PAYGO_CREATE_TRANSACTION = 'https://apitest.paygo24.com/json/create_transaction';
    var $__PAYGO_CHECK_TRANSACTION  = 'https://apitest.paygo24.com/json/check_transaction';
    var $__PAYGO_PARTNER_LOGIN      = 'idms';
    var $__PAYGO_PARTNER_PASSWORD   = 'Bus*29Px_ExzL2';

    public function beforeFilter(){

        date_default_timezone_set('Asia/Phnom_Penh');

        $this->set('MONTH_ARRAY', $this->MONTH_ARRAY);

        $this->set('__MAXIMUM_IMAGE_SIZE', $this->__MAXIMUM_IMAGE_SIZE);
        
        $this->set('__DEAL_WIDTH', $this->dealWidth);
        $this->set('__DEAL_HEIGHT', $this->dealHeight);

        if( $this->Session->read('Buyer') ){
            $this->LOGGED_SESSION = $this->Session->read("Buyer");
        }

        $this->set('__BUYER_LOGGED_SESSION', $this->LOGGED_SESSION);

        // Check cart session
        $carts = $this->Session->read('Buyer.__SES_CART_DATA');
        $this->__CART_ITEM = $carts;

        $this->set('__SES_CART_DATA', $carts );
        if( $carts ){
            $carts = count($carts);
        }else{
            $carts = 0;
        }
        
        $this->set('cart_count', $carts);

        $cities = $this->City->find('all', array('conditions' => array('status'=>1), 'order' => array('City.city_name ASC')));
        $this->set('cities', $cities);

        $categories = $this->BusinessCategory->find('all', array('conditions' => array('status'=>1, 'parent_id'=>0), 'order' => array('category ASC')));
        $this->set('categories', $categories);

        $provinces = $this->Province->find('all', array('conditions' => array('Province.status'=>1), 'order' => array('province_name ASC')));
        $this->set('provinces', $provinces);

        $ProvinceList = $this->City->find('all' , array('conditions' => array('City.status'=>1), 'order' => array('city_name ASC')));
        $this->set('provinceList', $ProvinceList);

        // Rattana // 09-July-2015
        // Get Locations of PP City
        $locationConds['conditions'] = array( 'Location.city_code' => 'PP', 'Location.status' => 1 );
        $this->set('locations', $this->Location->find('all', $locationConds) );

        $goodCate = $this->GoodsCategory->find('all', array('conditions' => array('GoodsCategory.status'=>1), 'order' => array('category ASC')));
        $this->set('goodCate', $goodCate);

        $business = $this->Business->find('all', array('conditions'=>array('Business.status'=>1), 'order'=> array('Business.business_name ASC')));
        $this->set('business', $business);

        $now = date("Y-m-d H:i:s");
        $this->set('mons', $this->mons);

        $menuBusinessInfo = $this->Business->find('first', array('conditions' => array(
                                                                                "NOT" => array("Business.logo" => array("img/business/logos/default.jpg")),
                                                                                'Business.status'=>1, 
                                                                                'Business.member_level'=>1),
                                                             'order'=>'rand()'));

        $this->set('menuBusinessInfo', $menuBusinessInfo);

        $this->set('limit_deal_title', $this->limit_deal_title);
        $this->set('limit_business_title', $this->limit_business_title);
        $this->set('token',$this->token);

        // Rattana
        $this->set('genders', $this->genders_array);

        // PAYGO Information
        $this->set('__PAYGO_API_CREATE_TRANSACTION_URL', $this->__PAYGO_CREATE_TRANSACTION );
        $this->set('__PAYGO_API_CHECK_TRANSACTION_URL', $this->__PAYGO_CHECK_TRANSACTION );
        $this->set('__PAYGO_PARTNER_LOGIN', $this->__PAYGO_PARTNER_LOGIN );
        $this->set('__PAYGO_PARTNER_PASSWORD', $this->__PAYGO_PARTNER_PASSWORD );

    }

    public function generateRegisterToken( $str = NULL ){

        $defautl = 20;
        $second  = date('s');
        if( $second == 0 ){
            $second = $defautl;
        }
        for ($i = 0; $i < $second ; $i++) { 
            $str = sha1($str);
        }

        return $str;
    }


    public function generateReferalToken( $str = NULL ){

        $defautl = 20;
        $second  = date('s');
        
        if( $second == 0 ){
            $second = $defautl;
        }
        for ($i = 0; $i < $second ; $i++) { 
            $str = sha1($str);
        }

        return $str;
    }

    public function generateLog($message,$action, $user_id)
    {
        // $user = CakeSession::read("Auth.User");     
        $this->Userlog = new Userlog();
        $total = $this->Userlog->find('count');
        if($total>10000)
        {
            $this->Userlog->query('TRUNCATE TABLE '.$this->Userlog->table);
        }
        $this->Userlog->create();
        $this->Userlog->save(
                            array('userid'=>$user_id,                            
                                'log'=>$message,
                                'action'=>$action,
                                'context'=>$this->getContext(),
                                'ip'=>$this->request->clientIp()
                                )
                            );

    }

    function getContext() {

        if(isset($this->context) && $this->context)return $this->context;
        $className = get_class($this);
        $className = substr($className, 0,strlen($className)- strlen('Controller'));
        return $className;

    }

    public function getSortId()
    {
        return md5($this->getContext());
    }

    public function setState($name,$value='')
    {
        $states = $this->getState();
        $states[$name] = $value;
        $this->Session->write(sha1($this->getSortId()),$states);    
    }

    /*
        Get State of the object
    */
    public function getState($name='',$value='')
    {
        $stores = sha1($this->getSortId());
        $states = array();
        if($this->Session->check($stores))
        {
            $states = $this->Session->read($stores);

        }
        if(!$name) return $states;
        if(isset($states[$name])) return $states[$name];
        return $value;      
        
    }

    public function sendMail( $recipients = NULL, $subject = NULL , $content = NULL, $attachment = NULL, $sender= NULL ){

        $usermessage = $content;

        // User Default config
        $config = new MailConfigure();

        $Email = new CakeEmail();
        $Email->config('default'); 

        $Email->emailFormat('html')
              ->to($recipients)
              ->subject($subject);

        if( $attachment ){
            $Email->attachments($attachment);
        }

        $sent = $Email->send($usermessage);

        return $sent;

    }


    public function generateAccessTokenForPaypal(){

        // Generate Access Token
        // ===================================

        $ch = curl_init();
        $clientId   = "AZnG3QD26XKeCFyBrfXYx5Go8PfpIg_RmhUG2dM-2IVhHLPnjNq5DX6l4Eq3Z8AyIzZQaT13lN9Z5Xu_";
        $secret     = "EJfIDTVy7IQP5KqpmdSI4TWql2oZYA7_2_iHS4UHSumTsL2xodzeoVuPWtNc6fU06f4YOZMPY6-JxIdP";

        curl_setopt($ch, CURLOPT_URL, "https://api.sandbox.paypal.com/v1/oauth2/token");
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
        curl_setopt($ch, CURLOPT_USERPWD, $clientId.":".$secret);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "grant_type=client_credentials");

        $result = curl_exec($ch);
        curl_close($ch);

        if(empty($result)){
            return false;
        }
        else
        {
            $json = json_decode($result);
            return $json->access_token ;
        }

        exit;
    }
    
}