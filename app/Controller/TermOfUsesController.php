<?php

/********************************************************************************

File Name: TermOfUseController.php
Description:

Powered By: ABi Investment Group Co., Ltd,

Changed History:

    Date                Author              Description
    2014/07/10          Sim Chhayrambo      Initial Version

*********************************************************************************/

App::uses('AppController', 'Controller');

/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class TermOfUsesController extends AppController {

/**
 * Components
 *
 * @var array
 */
    public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
    public  function index()
    {   

    }
}