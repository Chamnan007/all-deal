<?php

/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 */

/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */
    Router::connect('/', array('controller' => 'index', 'action' => 'index', 'home'));

    Router::connect('/administrator/login',array('controller'=>'users','action'=>'login','plugin'=>'administrator'));
    Router::connect('/administrator/forgot',array('controller'=>'users','action'=>'forgot','plugin'=>'administrator'));

    Router::connect('/administrator/user',array('controller'=>'buyers','plugin'=>'administrator'));
    Router::connect('/administrator/user/view/*',array('controller'=>'buyers','plugin'=>'administrator', 'action' => 'view' ));
    Router::connect('/administrator/user/topup/*',array('controller'=>'buyers','plugin'=>'administrator', 'action' => 'topup' ));
    Router::connect('/administrator/user/topupView/*',array('controller'=>'buyers','plugin'=>'administrator', 'action' => 'topupView' ));

    Router::connect('/administrator/merchants',array('controller'=>'businesses','plugin'=>'administrator'));
    Router::connect('/administrator/merchants/add',array('controller'=>'businesses', 'action' => 'add', 'plugin'=>'administrator'));
    Router::connect('/administrator/merchants/edit/*',array('controller'=>'businesses', 'action' => 'edit', 'plugin'=>'administrator'));
    Router::connect('/administrator/merchants/view/*',array('controller'=>'businesses', 'action' => 'view', 'plugin'=>'administrator'));

    Router::connect('/administrator', array('controller' => 'dashboards', 'action' => 'index', 'plugin'=> 'administrator'));
    Router::connect('/administrator/finance/revenue/*', array('controller' => 'SystemRevenues', 'action' => 'index', 'plugin'=> 'administrator'));
    Router::connect('/administrator/finance/payment-history/*', array('controller' => 'SystemRevenues', 'action' => 'payment', 'plugin'=> 'administrator'));
    Router::connect('/administrator/payment/view/*', array('controller' => 'SystemRevenues', 'action' => 'view', 'plugin'=> 'administrator'));
    Router::connect('/administrator/finance/make-payment/*', array('controller' => 'SystemRevenues', 'action' => 'makepayment', 'plugin'=> 'administrator'));
    Router::connect('/administrator/transaction', array('controller' => 'BuyerTransactions', 'action' => 'index', 'plugin'=> 'administrator'));
    Router::connect('/administrator/transaction/view/*', array('controller' => 'BuyerTransactions', 'action' => 'view', 'plugin'=> 'administrator'));
    Router::connect('/administrator/transaction/redeem', array('controller' => 'BuyerTransactions', 'action' => 'redeem', 'plugin'=> 'administrator'));

    Router::connect('/member',array('controller'=>'MemberUsers','action'=>'login','plugin'=>'member'));
    Router::connect('/member/login',array('controller'=>'MemberUsers','action'=>'login','plugin'=>'member'));
    Router::connect('/member/forgot',array('controller'=>'MemberUsers','action'=>'forgot','plugin'=>'member'));

    Router::connect('/member/finance/revenue/*', array('controller' => 'BusinessRevenues', 'action' => 'index', 'plugin'=> 'member'));
    Router::connect('/member/finance/balance/*', array('controller' => 'BusinessRevenues', 'action' => 'balance', 'plugin'=> 'member'));

    Router::connect('/member/transaction', array('controller' => 'BuyerTransactions', 'action' => 'index', 'plugin'=> 'member'));
    Router::connect('/member/transaction/view/*', array('controller' => 'BuyerTransactions', 'action' => 'view', 'plugin'=> 'member'));
    Router::connect('/member/transaction/redeem', array('controller' => 'BuyerTransactions', 'action' => 'redeem', 'plugin'=> 'member'));

    /**
     * ...and connect the rest of 'Pages' controller's URLs. 
    */


    Router::connect('/merchants',array('controller' => 'businesses', 'action' => 'welcome')); // merchant welcome
    Router::connect('/merchant',array('controller' => 'businesses', 'action' => 'welcome')); // merchant welcome
    Router::connect('/merchants/login',array('controller' => 'businesses', 'action' => 'welcome')); // merchant welcome
    Router::connect('/merchants/list/*',array('controller' => 'businesses', 'action' => 'index'));
    Router::connect('/merchants/profile/*',array('controller' => 'businesses', 'action' => 'detail')); // updated 2014-07-09
    Router::connect('/merchants/register/*', array('controller' => 'businesses', 'action' => 'register')); // added 2014-07-10

    Router::connect('/', array('controller' => 'index', 'action' => 'index')); // added 2014-07-10
    Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));
    Router::connect('/search/*',array('controller' => 'search', 'action' => 'index'));
    Router::connect('/goods/*',array('controller' => 'goods', 'action' => 'index'));
    Router::connect('/products/*',array('controller' => 'goods', 'action' => 'getMore')); // added 2014-07-03
    Router::connect('/travel/*',array('controller' => 'destinations', 'action' => 'index'));
    Router::connect('/deal/*',array('controller' => 'deals', 'action' => 'detail')); // updated 2014-07-03
    Router::connect('/deals/',array('controller' => 'deals', 'action' => 'index')); // updated 2014-07-03
    // Router::connect('/deals/index/',array('controller' => 'deals', 'action' => 'index')); // updated 2014-07-03
    Router::connect('/filter/*',array('controller' => 'deals', 'action' => 'getMore')); // updated 2014-07-03
    Router::connect('/terms/*', array('controller' => 'termOfUses', 'action' => 'index')); // added 2014-07-10
    Router::connect('/privacy/*', array('controller' => 'privacyPolicies', 'action' => 'index')); // added 2014-07-10


    Router::connect('/deals/checkIfDealAvailableAjax__',array('controller' => 'deals', 'action' => 'checkIfDealAvailableAjax__')); // updated 2014-07-03
    

    // Rattana
    Router::connect('/users/signin', array('controller' => 'shops', 'action' => 'signin') );
    Router::connect('/users/signin/*', array('controller' => 'shops', 'action' => 'signin') );
    Router::connect('/users/signup', array('controller' => 'shops', 'action' => 'signup') );
    Router::connect('/users/signup/*', array('controller' => 'shops', 'action' => 'signup') );
    Router::connect('/users/forgot-password', array('controller' => 'shops', 'action' => 'forgotpassword'));
    Router::connect('/cart', array('controller' => 'shops', 'action' => 'index'));
    Router::connect('/myaccount', array('controller' => 'shops', 'action' => 'myaccount'));
    Router::connect('/myprofile', array('controller' => 'shops', 'action' => 'myprofile'));
    Router::connect('/refer', array('controller' => 'shops', 'action' => 'refer'));
    Router::connect('/success/*', array('controller' => 'shops', 'action' => 'success'));
    Router::connect('/users/activatereset/*', array('controller' => 'shops', 'action' => 'activateReset')); 
    Router::connect('/payment/paygo/*', array('controller' => 'shops', 'action' => 'paygo')); 
    Router::connect('/payment/paygo-confirm/*', array('controller' => 'shops', 'action' => 'paygoConfirm')); 

    // 2015-04-02
    // route for web service
    // 2015-07-22 Testing
    Router::connect('/webservices',array('controller'=>'functions','action'=>'index','plugin'=>'webservices')); // Done

    // Router::connect('/webservices/getcity',array('controller'=>'functions','action'=>'getCity','plugin'=>'webservices')); // Done
    Router::connect('/webservices/getbusinesscategory',array('controller'=>'functions','action'=>'getBusinessCategory','plugin'=>'webservices')); // Done
    Router::connect('/webservices/getGoodsCategory',array('controller'=>'functions','action'=>'getGoodsCategory','plugin'=>'webservices')); // Done

    Router::connect('/webservices/signup',array('controller'=>'functions','action'=>'signup','plugin'=>'webservices'));
    Router::connect('/webservices/functions/signUpVerification',array('controller'=>'functions','action'=>'signUpVerification','plugin'=>'webservices'));
    Router::connect('/webservices/savemyprofile',array('controller'=>'functions','action'=>'saveMyProfile','plugin'=>'webservices'));
    Router::connect('/webservices/resetuserpassword',array('controller'=>'functions','action'=>'resetUserPassword','plugin'=>'webservices'));

    // Router::connect('/webservices/getnewdeals',array('controller'=>'functions','action'=>'getNewDeals','plugin'=>'webservices')); // Done
    // Router::connect('/webservices/getnewdeals/*',array('controller'=>'functions','action'=>'getNewDeals','plugin'=>'webservices')); // Done
    // Router::connect('/webservices/getdeals',array('controller'=>'functions','action'=>'getDeals','plugin'=>'webservices'));

    // Use App Webservices    
    Router::connect('/webservices/signin',array('controller'=>'functions','action'=>'signin','plugin'=>'webservices'));
    Router::connect('/webservices/getprovince',array('controller'=>'functions','action'=>'getProvinceList','plugin'=>'webservices')); // Done
    Router::connect('/webservices/getlocation',array('controller'=>'functions','action'=>'getLocation','plugin'=>'webservices')); // Done
    Router::connect('/webservices/getsangkat',array('controller'=>'functions','action'=>'getSangkatByLocation','plugin'=>'webservices')); // Done
    Router::connect('/webservices/referfriend',array('controller'=>'functions','action'=>'referFriend','plugin'=>'webservices'));
    Router::connect('/webservices/getuserinfo',array('controller'=>'functions','action'=>'getUserDetailInformation','plugin'=>'webservices'));
    Router::connect('/webservices/getalldealcredithistory',array('controller'=>'functions','action'=>'getAllDealCreditTransaction','plugin'=>'webservices'));
    Router::connect('/webservices/getmycoupon',array('controller'=>'functions','action'=>'getTransactionHistory','plugin'=>'webservices'));
    Router::connect('/webservices/gettransactiondetail',array('controller'=>'functions','action'=>'getTransactionDetail','plugin'=>'webservices'));
    Router::connect('/webservices/getdealoftheday',array('controller'=>'functions','action'=>'getDealOfTheDay','plugin'=>'webservices')); // Done
    Router::connect('/webservices/getlastminutedeals',array('controller'=>'functions','action'=>'getLastMinuteDeals','plugin'=>'webservices')); // Done
    Router::connect('/webservices/getnearbydeals',array('controller'=>'functions','action'=>'getNearByDeals','plugin'=>'webservices')); // Done
    Router::connect('/webservices/getdealpackages',array('controller'=>'functions','action'=>'getDealPackages','plugin'=>'webservices')); // Done
    Router::connect('/webservices/getmerchants',array('controller'=>'functions','action'=>'getBusinesses','plugin'=>'webservices'));
    Router::connect('/webservices/getdealsbymerchant',array('controller'=>'functions','action'=>'getDealsByMerchant','plugin'=>'webservices')); // Done
    
    // Merchant Services
    Router::connect('/webservices/merchantlogin',array('controller'=>'functions','action'=>'merchantLogin','plugin'=>'webservices'));
    Router::connect('/webservices/resetmerchantpassword',array('controller'=>'functions','action'=>'resetMerchantPassword','plugin'=>'webservices'));
    Router::connect('/webservices/getverifycoupon',array('controller'=>'functions','action'=>'getVerifyCouponByCode','plugin'=>'webservices'));
    Router::connect('/webservices/redeemcoupon',array('controller'=>'functions','action'=>'redeemPurchasedTransaction','plugin'=>'webservices'));

       
    // Mobile
    Router::connect('/mobiles',array('controller'=>'deals','action' => 'index', 'plugin'=>'mobiles'));
    Router::connect('/mobile',array('controller'=>'deals','action' => 'index', 'plugin'=>'mobiles'));
    Router::connect('/mobiles/deals/',array('controller'=>'deals','action' => 'index','plugin'=>'mobiles'));
    
    Router::connect('/mobiles/merchant',array('controller'=>'merchant','action'=>'index','plugin'=>'mobiles'));
    Router::connect('/mobiles',array('controller'=>'deals','action'=>'detail','plugin'=>'mobiles'));
    // end route for web Services
/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
    CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
    require CAKE . 'Config' . DS . 'routes.php';