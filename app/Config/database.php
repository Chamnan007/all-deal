<?php
class DATABASE_CONFIG {

	
	public $default = array(
		'datasource' => 'Database/Mysql',
		'persistent' => false,
		'host' => 'localhost',
		'login' => 'root',
		'password' => '',
		'database' => 'all_deal',
		'prefix' => 'tb_',
	);
}
