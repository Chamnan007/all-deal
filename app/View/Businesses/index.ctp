<?php 

/********************************************************************************

File Name: index.php
Description: displaying business UI

Powered By: ABi Investment Group Co., Ltd,

Changed History:

    Date                Author              Description
    2014/06/10          Sim Chhayrambo      Initial Version
    2014/06/13          Sim Chhayrambo      Check no image logo
                                            Add pagination
    2014/06/23          Sim Chhayrambo      Clean up URL
    2014/06/30          Sim Chhayrambo      Add maximum characters for search box
    2014/07/13          Sim Chhayrambo      Remove space in thumbnail, and limit title

*********************************************************************************/

// SEARCH PARAMS
$arrCategory = array();
$arrSubCategory = array();
$arrLocation = array();
$strDescription = "";
$businessesTitle = "All Merchants";
$searchValue = "";

if(!empty($subCategory)){
    $arrSubCategory = $subCategory;
}


if(!empty($filterLocation)){
    $arrLocation = $filterLocation;
}

if(!empty($this->request->params['named'])){
    $param = $this->request->params['named'];
    foreach($param as $k => $v ){
        // $params[$k][] = $v;
        $params["mainCategory"] = $v;
    }
    $jParams = json_encode($params);
}else{
    $jParams = json_encode($this->params['url']);  
}

if(!empty($this->request->named['category'])){
    $strDescription .= ", " . $this->request->named['category'];
}

// if(!empty($arrCategory)){
//     foreach ($arrCategory as $key => $value) {
//         $strDescription .= ", " . $value;
//     }
// }


if(!empty($arrSubCategory)){
    foreach ($arrSubCategory as $key => $value) {
        $strDescription .= ", " . $value;
    }
}

if(!empty($location)){
    foreach ($location as $key => $value) {
        $strDescription .= ", " . $value;
    }
}

if(empty($strDescription)){
    $strDescription .= "All Business Type";
}

if(!empty($filterName)){
    $businessesTitle = 'Searching for "' . $filterName . '"';
    $searchValue = $filterName;
}

if(!empty($businessCategoryInfo)){

    $bussCateInfo = $businessCategoryInfo['BusinessCategory'];
    $businessesTitle = $bussCateInfo['category'];
    $bussSubCategory = $businessCategoryInfo['ChildCategory'];

}else{

    $bussCateInfo = $bussSubCategory = "";
    
}

$metaDescription = substr("All Deal: " . ltrim($strDescription, ","),0,300).'...';
$this->Html->meta('description', $metaDescription, array('inline'=>false));

?>

<style type="text/css">
.business-search-box{
    padding: 0 5px;
    border-radius: 5px;
    border: 1px solid #ccc;
    width: 100%;   
    height: 30px;
}
.bg-shadow{
    background: none;
}
.new-project-body{
    background: #DDD;
}
.img-container{
    height: 225px;
}
</style>

<div class="container">
    <input type="hidden" name="secure_token" id="secure_token" value="<?php echo $token ?>">
    <div style="height: 20px;"></div>
    <div class="col-sm-12 no-padding" style="margin: 10px 0; border-bottom: 1px dotted #999;">
        <h1 style="line-height: 40px; font-size: 28px;"><?php echo $businessesTitle ?></h1>
    </div>
    <div class="col-sm-12 no-padding clearfix" style="margin: 20px 0;">
        <div class="col-sm-3 no-padding-left">

            <form method="get" name="frmFilter" id="frmFilter" action="<?php echo $this->Html->url(array('controller'=>'businesses', 'action'=>'index')) ?>">

            <input type="hidden" name="mainCategory" value="<?php echo $mainCategory; ?>" >

            <div class="category-name add-margin-bottom">
                <label>
                    <span class="name truncated"><?php echo __('Business Name:'); ?></span>
                </label>
                <input type="text" name="businessName" class="business-search-box" maxlength="100" placeholder="Search Businesses Here" value="<?php echo $searchValue ?>">
            </div>

            <div class="categories-wrapper">
                <h3 style="margin-bottom: 5px; font-weight: bold;">
                    <?php echo __('Category') ?>
                </h3>
                <ul>
                    <?php 
                    foreach ($categories as $catKey => $catValue) {
                        $selected = ($catValue['BusinessCategory']['slug'] == $mainCategory)?" color:#8dc63f ; font-weight:bold;":"color:#333" ;
                    ?>
                    <li class="category">
                        <a href="<?php echo $this->Html->url(array('controller'=>'businesses', 'action'=>'index')) ?>?category=<?php echo $catValue['BusinessCategory']['slug'] ?>" style="font-size: 12px; <?php echo $selected ?>">
                            <span class="title"><?php echo $catValue['BusinessCategory']['category'] ?></span>
                        </a>
                    </li>
                    <?php } ?>
                </ul>
            </div>


            <div class="categories-wrapper">
                <h3 style="margin-bottom: 5px; font-weight: bold;">
                    <?php echo __('Location') ?>
                </h3>
                <ul>
                    <li class="category">
                        <div class="category-name">
                            <input type="checkbox" class="mainLocation" id="mainLocation" value="" name="location[]">
                            <label for="mainLocation">
                                <span class="name truncated"><?php echo __('All') ?></span>
                            </label>
                            
                        </div>
                    </li>
                    <?php 
                    foreach ($cities as $cityKey => $cityValue) { 
                        $strChecked = "";
                        if (in_array($cityValue['City']['slug'], $arrLocation)){
                            $strChecked = "checked='checked'";
                        }
                    ?>
                    <li class="category">
                        <div class="category-name">
                            <input value="<?php echo $cityValue['City']['slug'] ?>" type="checkbox" id="<?php echo "province_" . $cityValue['City']['id'] ?>" class="subLocation" name="location[]" <?php echo $strChecked ?>>
                            <label for="<?php echo "province_" . $cityValue['City']['id'] ?>">
                                <span class="name truncated"><?php echo $cityValue['City']['city_name'] ?></span>
                                <!-- <span class="deal-counts">(18)</span> -->
                            </label>
                        </div>
                    </li>
                    <?php } ?>
                </ul>
            </div>

            <div class="categories-wrapper">
                <div class="category-reset">
                    <span>
                        <a href="<?php echo $this->Html->url(array('action'=>'index')) ?>">
                            <i class="icon-cancel" style="color: #428bca;"></i>
                            <?php echo __('Reset All'); ?>
                        </a>
                    </span>
                    <button name="filter" class="pull-right btn-green" style="border: 1px solid #95b959; border-radius: 3px;">
                        <i class="icon-eye" style="color: #fff;"></i>
                        <?php echo __('Search') ?>
                    </button>
                </div>
            </div>
            
            </form>
        </div>
        <div class="col-sm-9 no-padding">
            <?php if(!empty($businesses)){ ?>
            <div class="col-sm-12 no-padding clearfix">
                <div class="recent-works col-sm-12 no-padding">
                    <div class="recent-works-carousel">
                        <div class="carousel slide" id="Recentworks">
                            <div class="carousel-inner">
                                <div class="item active">
                                    <ul class="thumbnails">
                                        <div class="row" id="business-container">
                                            <?php 
                                            foreach ($businesses as $busKey => $bValue) { 
                                                $img =  $bValue['Business']['logo'];
                                                if(empty($img)) $img = "img/no-image.jpg";

                                                if(!empty($bValue['CategoryInfo']['mainCategory'])){
                                                    $mainCategory = $bValue['CategoryInfo']['mainCategory'];
                                                }else{
                                                    $mainCategory = "";
                                                }

                                                if(!empty($bValue['CategoryInfo']['subCategory'])){
                                                    $subCategory = $bValue['CategoryInfo']['subCategory'];
                                                }else{
                                                    $subCategory = "";
                                                }

                                                if(!empty($subCategory)){
                                                    $strCategory = $subCategory . " - " . $mainCategory; 
                                                }else{
                                                    $strCategory = $mainCategory; 
                                                }

                                                $business_name = $bValue['Business']['business_name'];


                                                if(strlen($business_name) > $limit_deal_title){
                                                    $business_name = substr($business_name,0,$limit_deal_title-3).'...';   
                                                }

                                                if(strlen($strCategory) > $limit_business_title){
                                                    $strCategory = substr($strCategory,0,$limit_business_title-3).'...';   
                                                }

                                            ?>
                                                <li class="col-sm-4 col-xs-4">
                                                    <div class="new-project bg-shadow add-margin-bottom business-img-container">
                                                        <div class="img-container">
                                                            <a href="<?php echo $this->Html->url(array('controller'=>'businesses', 'action'=>'detail', $bValue['Business']['slug'])) ?>">
                                                                <img src="<?php echo $this->webroot . $img ?>" title="<?php echo $bValue['Business']['business_name'] ?>">
                                                            </a>
                                                        </div>
                                                        <div id="bx-pager">
                                                            <a data-slide-index="0" href="<?php echo $this->Html->url(array('controller'=>'businesses', 'action'=>'detail', $bValue['Business']['slug'])) ?>" class="active">
                                                                <div class="new-project-body">
                                                                    <h4><?php echo $business_name ?></h4>
                                                                    <i><?php echo $strCategory ?></i><br>
                                                                    <i class="icon-location"><?php echo $bValue['Business']['location'] . ", " . $bValue['Provinces']['province_name'] ?></i>
                                                                </div>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </li>
                                            <?php } ?>
                                        </div>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="pagination_footer" style="width: 100%; text-align: center;">
                    <h4 id="showNumber"></h4>
                    <a href="#" id="view-more" class="button btn-green" style="width: 140px; margin: 0 auto; margin-top: 10px; padding: 10px; display: none;"> <?php echo __('VIEW') . " " . $limit . " " . __('MORE') ?> <i class="icon-angle-down"></i></a>
                </div>
                <div class="ajax-loading" style="width: 100%; text-align: center; display:none;">
                    <img src="<?php echo $this->webroot . "img/bx_loader.gif" ?>">
                </div>
            </div>
            <?php }else{?>
            <div class="not-found">
                <h1><?php echo __('No Business Found!') ?></h1>
            </div>
            <?php } ?>
        </div>
    </div>
</div>

<script type="text/javascript">

$(function(){
    
    $("input.subLocation").click(function(){
        $("input.mainLocation").attr("checked",false);  
    });

    $("input.mainLocation").click(function(){
        $("input.subLocation").attr("checked",false);
    });

    var track_click = 1,
        total_business = <?php echo $total_business; ?>,
        total_pages = <?php echo $total_pages; ?>,
        limit = <?php echo $limit; ?>,
        current_business = limit * track_click;
        params = <?php echo $jParams; ?>,
        divContainer = $("div#business-container"),
        secure_token = $("input#secure_token").val();

    if(current_business > total_business){
        current_business = total_business;
    }

    if(total_business <= limit){
        $("a#view-more").hide();
    }else{
        $("a#view-more").show();
    }

    $("h4#showNumber").text("Show "+current_business+" of "+total_business);

    $("a#view-more").click(function() {
        
        event.preventDefault();

        var me = $(this);

        track_click++;

        $.ajax({
            url:"<?php echo $this->Html->url(array('controller'=>'businesses', 'action'=>'getMore')) ?>"+"/"+track_click+"/"+secure_token,
            data:{params:params},
            type:'post',       
            beforeSend:function(){
                me.hide(); //hide load more button on click
                $('.ajax-loading').show(); //show loading image
            },
            success:function(data){
                data = $.parseJSON(data);
                strBusiness = data.strBusiness;
                console.log(data);
                if(track_click <= total_pages) {
                    divContainer.append(strBusiness)
                    me.show(); //bring back load more button
                    $('.ajax-loading').hide(); //hide loading image once data is received
                    // $("html, body").animate({scrollTop: me.offset().top}, 500); //scroll page to button element
                    if(track_click > total_pages-1){
                        me.hide();
                    }
                }
            },
            error: function(error){
            }
        });

        current_business = limit * track_click;

        if(current_business > total_business){
            current_business = total_business;
        }

        $("h4#showNumber").text("Show "+current_business+" of "+total_business);

    });
    
});

</script>