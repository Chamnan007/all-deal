<?php 

/**

* @author: Engleang SAM

* @package: App.View.Helper

* Date          Modified        Changes
* 20-May-2015   Rattana         Add Function Format Number
*
*/


class MyHtmlHelper extends AppHelper

{

	 public $helpers = array('Html');

	 public function editButton($id,$controller="")

	 {

	 	$url = array('action'=>'edit',

	 				$id);

	 	if($controller) $url['controller']= $controller;

	 	$st = $this->Html->link(                                      

                                $this->Html->tag('i',

                                		'',

                                			array('class'=>'fa fa-edit',
                                				 'style'=>'color:blue',
                                				)).' '.__('Edit')

                                  ,

                                   $url,

                                    array(

								        'escape'=>false,

								        'class'=>'btn btn-default btn-xs'

								        )

                                	);

	 	return $st;

	 }
	 public function changeCounterButton($id,$controller="")

	 {

	 	$url = array('action'=>'changeCounter',

	 				$id);

	 	if($controller) $url['controller']= $controller;

	 	$st = $this->Html->link(                                      

                                $this->Html->tag('i',

                                		'',

                                			array('class'=>'fa fa-edit')).' '.__('SetCounter')

                                  ,

                                   $url,

                                    array(

								        'escape'=>false,

								        'class'=>'btn btn-default btn-xs'

								        )

                                	);

	 	return $st;

	 }


	 public function getDateFormat($date)

	 {

	 	$strDate = date_format(date_create($date),'d/m/Y');

	 	return $strDate;

	 }

	 public function deleteButton($id)

	 {

	 	$st = $this->Html->link(                                      

                                $this->Html->tag('i',

                                		'',

                                			array('class'=>'fa fa-minus-circle',
                                				 'style'=>'color:red;'))

                                	.' '.__('Delete')

                                  ,

                                    array(                                        

                                        'action' => 'delete',

                                         $id

                                    ),

                                    array(

								        'escape'=>false,

								        'class'=>'btn btn-default btn-xs btn-delete'

								        )

                                	);

	 	return $st;	

	 }
	
	public function state($id,$state=0,$canAccess=true)

	{
		$content= array(	0=>'<span class="label label-grape">inactive</span>',
							1=>'<span class="label label-success">active</span>',
						);
		if($canAccess)
		$content = array(

					0=>	$this->Html->link(                                        

                                    $this->Html->tag('span',

                                        			'inactive',

                                        			array('class'=>'label label-grape'))

                                    ,

                                    array(                                        

                                        'action' => 'state',

                                         $id,1

                                    ),

                                    array(



                                    	'rel'=>'tooltip',

								        'data-placement'=>'left',

								        'data-original-title'=>'Toogle to Activate',								        

								        'escape'=>false

								        )

                                	),

					1=> $this->Html->link(                                        

                                        $this->Html->tag('span','active',array('class'=>'label label-success'))                                                                                

                                            

                                    ,

                                    array(

                                        

                                        'action' => 'state',

                                        $id,0

                                    ),

                                    array(

                                    	'rel'=>'tooltip',

								        'data-placement'=>'left',

								        'data-original-title'=>'Toogle to Deactivate',								        

								        'escape'=>false

								        )

                                ),

					-1=> $this->Html->link(                                        

                                        $this->Html->tag('span','pendding',array('class'=>'label label-warning'))                                                                                

                                            

                                    ,

                                    array(

                                        

                                        'action' => 'state',

                                        $id,1

                                    ),

                                    array(

                                    	'rel'=>'tooltip',

								        'data-placement'=>'left',

								        'data-original-title'=>'Toogle to Activate',								        

								        'escape'=>false

								        )

                                )

					);		

		

			return  $content[$state];

		



	}

	function checkAll()

	{

		return '<input type="checkbox" class="checkAll" title="Check All" />';

	}

	function checkBox($id,$i)

	{


		return '<input type="checkbox" 

					name = "cb[]"

					class="cb" id="cb'.$i.'" value="'.$id.'" />';	

					

	}

	function printTicket($ticket_id)

	{

		$st = $this->Html->link(                                      

                                $this->Html->tag('i',

                                		'',

                                			array('class'=>'fa fa-print')).' '.__('Print Ticket')

                                  ,

                                    array(           

                                    	'controller'=>'ticket',                             

                                        'action' => 'printTicket',

                                         $ticket_id



                                    ),

                                    array(

								        'escape'=>false,

								        'class'=>'btn btn-default btn-xs'

								        )

                                	);

		return $st;

	}

	public function formatNumber( $number = 0, $decimal = 4, $currency = NULL ){

        if( $number == 0 || $number == NULL ){
            return "-";
        }
        else if( $number < 0 ){
            $number = $this->checkNumber($number);
            return $currency . " (" . (str_replace("-", "", $number)) . ")";
        }else{
            $number = $this->checkNumber($number);
            return $currency . " " . ($number);
        }
    }

    public function number_format_keep_decimals($num) {
        if ($point = strrpos($num, '.')){

            $val = explode(".", $num);

            if( strlen(end($val)) <= 1 ){
                return number_format($num,2);                
            }

            return number_format($num, strlen($num) - $point - 1);
            
        }else {
            return number_format($num,2);
        }
    }

    public function checkNumber( $number = 0 ) {
    
        if( strpos($number,".") != false ){
           $val = explode(".", $number);

           $number = $val[0] . "." . substr(end($val), 0, 5);
           $number = round($number, 4);
        }

        $number = $this->number_format_keep_decimals($number);
        
        return $number;
    }
	

}

 ?>