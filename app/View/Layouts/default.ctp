<?php
/**
 *
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */


/********************************************************************************

File Name: Layouts/default.ctp
Description: 

Powered By: ABi Investment Group Co., Ltd,

Changed History:

    Date                Author              Description
    2014/06/10          Sim Chhayrambo      Initial Version
    2014/06/24          Sim Chhayrambo      Add Modal.js
    2015/10/22          Phea Rattana        Add Share Button
        
*********************************************************************************/


// $webDescription = __d('cake_dev', '');
$webDescription = "All Deal";
$metaDescription = "all deals, deal, local deal, phnom penh deal, siem reap deal, travel, food and drink, restaurant, accommodation, hotel, shopping, thing to do, grocery, beauty and spa, activity";

$ogTitle = "";
$ogDescription = "";
$ogUrl = "";
$ogStieName = "All Deal";
$ogImage = "";

$getController = ($this->params['controller']);
$getAction = ($this->params['action']);

if($getController == "deals" && $getAction == "detail"){

    $full_url = Router::url( $this->here, true );

    if(!empty($data)){
    
        $dealInfo = $data['Deal'];
        $id = $dealInfo['id'];
        $title = $dealInfo['title'];
        $image = $dealInfo['image'];
        $desc = $dealInfo['description'];
        $discount = $dealInfo['discount_category'];
        $validTo = $dealInfo['valid_to'];

        if(!empty($discount)){
            $ogTitle = $title . " (" . $discount . ") ";    
        }else{
            $ogTitle = $title;    
        }
        $ogDescription = strip_tags($desc); // remove html tags
        $replace = array("<ul>", "</ul>", "<li>", "</li>");
        $ogDescription = str_replace($replace, "", $ogDescription); // remove ul li tags
        $ogDescription = substr($ogDescription,0,300).'...';
        $ogUrl = $full_url;
        $ogStieName = "All Deal";
        $ogImage = $this->webroot . $image;

    }

}

$param = "";
$paramValue = "";
$getParams = $this->params['named'];


if( $getParams){
    foreach ($getParams as $key => $value) {
        $param = $key;
        $paramValue = $value;
    }
}

$searchDeal = "";

if(!empty($searchText)){
    $searchDeal = $searchText;
}
?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:og="http://ogp.me/ns#"
      xmlns:fb="http://www.facebook.com/2008/fbml">
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# website: http://ogp.me/ns/website#">

    <!-- FOR FB SHARE -->
    <meta property="og:title" content="<?php echo $ogTitle ?>"/>
    <meta property="og:type" content="Deal"/>
    <meta property="og:url" content="<?php echo $ogUrl ?>"/>
    <meta property="og:image" content="<?php echo $ogImage ?>"/>
    <meta property="og:image:width" content="220" />
    <meta property="og:image:height" content="134" />
    <meta property="og:site_name" content="<?php echo $ogStieName ?>"/>
    <meta property="fb:admins" content="USER_ID"/>
    <meta property="og:description" content="<?php echo $ogDescription ?>"/>

    <?php echo $this->Html->charset(); ?>
    <title>

        <?php echo $webDescription ?> |
        <?php 
            if($title_for_layout == "Index"){

                $title_for_layout = "The Best Daily Deal & Discount in Cambodia";

            }elseif($title_for_layout == "Browse" && $this->params['action'] == "index"){

                $queryParams = $this->request->query;

                if(!empty($getParams['type'])){

                    $title_for_layout = $getParams['category'];    
                    $title_for_layout = str_replace("-", " ", $title_for_layout);
                    $title_for_layout = ucwords($title_for_layout);

                }elseif(!empty($queryParams)){

                    $strCategory = "";

                    if(!empty($queryParams['mainCategory'])){
                        $mainCategory = str_replace("-", " ", $queryParams['mainCategory']);
                        $mainCategory = ucwords($mainCategory);
                        $strCategory .= ", " . $mainCategory;
                    }

                    if(!empty($queryParams['businessCategory'])){
                        $arrCategory = $queryParams['businessCategory'];
                        foreach ($arrCategory as $key => $value) {
                            $businessCategory = str_replace("-", " ", $value);
                            $businessCategory = ucwords($businessCategory);
                            $strCategory .= ", " . $businessCategory;
                        }   
                    }

                    if(!empty($queryParams['location'])){
                        $arrLocation = $queryParams['location'];
                        foreach ($arrLocation as $key => $value) {
                            $location = str_replace("-", " ", $value);
                            $location = ucwords($location);
                            $strCategory .= ", " . $location;
                        }
                    }

                    if(!empty($queryParams['interest'])){
                        $arrInterest = $queryParams['interest'];
                        foreach ($arrInterest as $key => $value) {
                            $interest = str_replace("-", " ", $value);
                            $interest = ucwords($interest);
                            $strCategory .= ", " . $interest;
                        }
                    }

                    if(!empty($queryParams['service'])){
                        $arrService = $queryParams['service'];
                        foreach ($arrService as $key => $value) {
                            $service = str_replace("-", " ", $value);
                            $service = ucwords($service);
                            $strCategory .= ", " . $service;
                        }
                    }

                    $title_for_layout = ltrim ($strCategory,',');

                }

            }elseif($title_for_layout == "Destinations"){


                $queryParams = $this->request->query;

                if(!empty($getParams['destination'])){

                    $title_for_layout = $getParams['destination'];  
                    $title_for_layout = str_replace("-", " ", $title_for_layout);
                    $title_for_layout = ucwords($title_for_layout);
                    if($title_for_layout == "All"){
                        $title_for_layout = "All Destinations";
                    }

                }elseif(!empty($queryParams)){

                    $strCategory = "";

                    if(!empty($queryParams['destination'])){
                        $arrDestination = $queryParams['destination'];
                        foreach ($arrDestination as $key => $value) {
                            if($value == "All"){
                                $strCategory = "All Destinations";
                            }else{
                                $destination = str_replace("-", " ", $value);
                                $destination = ucwords($destination);
                                $strCategory .= ", " . $destination;
                            }
                        }   
                    }

                    $title_for_layout = ltrim ($strCategory,',');

                }else{

                    $title_for_layout = "All Destinations";

                }

            }elseif($title_for_layout == "Search" ){

                $urlQuery = $this->params['url'];

                if( $search_text){
                    $title_for_layout = "Searching for " . $search_text;
                }

                $str_location = "";

                if( $urlQuery['location'] && !in_array('all', $urlQuery['location']) ){
                    $str_location = $urlQuery['location'];
                    $str_location = implode(", ", $str_location);
                    $str_location = str_replace("-", " ", $str_location);
                    $str_location = ucwords($str_location);
                }

                if( $search_text && $str_location ){
                    $title_for_layout .= " in " . $str_location;
                }elseif ($str_location) {
                    $title_for_layout = "Searching in " . $str_location;
                }else if( $urlQuery['type'] && $urlQuery['type'] != "" ){
                    $title_for_layout = $urlQuery['type'];
                    $title_for_layout = str_replace("-", " ", $title_for_layout);
                    $title_for_layout = "Searching " . ucwords($title_for_layout);
                }

            }elseif( $title_for_layout == "Deals" ){

                if( $this->params['action'] == 'detail' ){
                    if(!empty($dealInfo)){
                        $title_for_layout = $dealInfo['title'];    
                    }   
                }else if( $this->params['action'] == 'index' ){

                    $getParams = $this->params['named'];

                    if( isset($getParams['type']) ){
                        $title_for_layout = str_replace("-", " " , $getParams['type']);
                        $title_for_layout = str_replace(" and ", " & ", $title_for_layout);
                        $title_for_layout = ucwords($title_for_layout);
                    }else if( $getParams['location']){

                        $title_for_layout = str_replace("-", " " , $getParams['location']);
                        $title_for_layout = str_replace(" and ", " & ", $title_for_layout);
                        $title_for_layout = "Searching deals in " . ucwords($title_for_layout);
                    }
                }

            }elseif($title_for_layout == "Businesses" && $this->params['action'] == 'detail'){

                if(!empty($businessInfo)){
                    $title_for_layout = $businessInfo['Business']['business_name'];    
                }
                

            }elseif($title_for_layout == "Businesses" && !empty($filterName)){

                $title_for_layout = "Searching for " . $filterName;

            }elseif($title_for_layout == "Businesses" && $this->params['action'] == 'index'){


                $queryParams = $this->request->query;

                if(!empty($getParams['category'])){

                    $title_for_layout = $getParams['category'];  
                    $title_for_layout = str_replace("-", " ", $title_for_layout);
                    $title_for_layout = ucwords($title_for_layout);

                }elseif(!empty($queryParams)){

                    $strCategory = "";

                    // if(!empty($queryParams['mainCategory'])){
                    //     $mainCategory = $queryParams['mainCategory'];
                    //     $mainCategory = str_replace("-", " ", $mainCategory);
                    //     $mainCategory = ucwords($mainCategory);
                    //     $strCategory .= ", " . $mainCategory;
                    // }

                    if(!empty($queryParams['businessCategory'])){
                        $arrCategory = $queryParams['businessCategory'];
                        foreach ($arrCategory as $key => $value) {
                            $businessCategory = str_replace("-", " ", $value);
                            $businessCategory = ucwords($businessCategory);
                            $strCategory .= ", " . $businessCategory;
                        }   
                    }


                    if(!empty($queryParams['location'])){
                        $arrLocation = $queryParams['location'];
                        foreach ($arrLocation as $key => $value) {
                            $location = str_replace("-", " ", $value);
                            $location = ucwords($location);
                            $strCategory .= ", " . $location;
                        }
                    }

                    $title_for_layout = ltrim ($strCategory,',');
                    if(empty($title_for_layout)){
                        $title_for_layout = "All Businesses";
                    }
                }else{
                    $title_for_layout = "All Businesses";
                }

            }elseif($title_for_layout == "Businesses" && $this->params['action'] == 'register'){

                $title_for_layout = "Merchants Registration";

            }elseif($title_for_layout == "TermOfUses"){

                $title_for_layout = "Terms of Use";

            }elseif($title_for_layout == "PrivacyPolicies"){

                $title_for_layout = "Privacy Policy";

            }elseif( $title_for_layout == 'Shops' ){

                if( $this->params['action'] == 'index' ){
                    $title_for_layout = "My Cart";
                }elseif( $this->params['action'] == 'signin' ){
                    $title_for_layout = "Sign In";
                }elseif( $this->params['action'] == 'signup' ){
                    $title_for_layout = 'Sign Up';
                }elseif( $this->params['action'] == 'myaccount' ){
                    $title_for_layout = "My Account";
                }elseif( $this->params['action'] == 'myprofile' ){
                    $title_for_layout = "My Profile";
                }elseif( $this->params['action'] == 'paygo' ){
                    $title_for_layout = "Pay with PayGo";
                }elseif( $this->params['action'] == 'paygoConfirm' ){
                    $title_for_layout = "PayGo Payment Confirmation";
                }

            }

            if( strtolower($title_for_layout) == 'businesses' ){
                $title_for_layout = "Merchants";
            }

            if( strtolower($title_for_layout) == 'all businesses' ){
                $title_for_layout = "All Merchants";
            }
            
            echo $title_for_layout;
        ?>
    </title>
    <?php

        echo $this->Html->meta('keywords', $metaDescription, array('inline'=>false));
        echo $this->Html->meta('icon');

        // echo $this->Html->css('cake.generic');
        echo $this->Html->css('bootstrap')."\n";
        echo $this->Html->css('bootstrap-theme.min')."\n";
        echo $this->Html->css('jquery.fancybox')."\n";
        echo $this->Html->css('infinity-glyph')."\n";
        echo $this->Html->css('styles', array('id'=>'color-style'))."\n";
        echo $this->Html->css('rating_star')."\n";
        echo $this->Html->css('datepicker')."\n";
        // echo $this->Html->css('chosen')."\n";
        echo $this->Html->css('menu')."\n";
        echo $this->Html->css('jquery.countdown')."\n";
        echo $this->Html->css('msgBoxLight')."\n";

        // Rattana- Slider
        echo $this->Html->css('../js/slider/bjqs') . "\n";

        echo $this->fetch('meta')."\n";
        echo $this->fetch('css')."\n";
        echo $this->fetch('script')."\n";
        

        echo $this->Html->script('jquery-1.10.2.min')."\n";
        echo $this->Html->script('bootstrap.min')."\n";

        // Daterange picker
        echo $this->Html->css('../js/daterange-picker/daterangepicker-bs3') . "\n";
        echo $this->Html->script('daterange-picker/moment') . "\n";
        echo $this->Html->script('daterange-picker/daterangepicker') . "\n";
        
    
    ?>
    <!-- <meta name="viewport" content="width=device-width, user-scalable=no"> -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

    <link href="https://netdna.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">

</head>

<body>
     <!-- Load Facebook SDK for JavaScript -->
    <div id="fb-root"></div>

    <script>

        // This is called with the results from from FB.getLoginStatus().
        function statusChangeCallback(response) {
            
            if (response.status === 'connected') {
                // Logged into your app and Facebook.
                testAPI();
            } else if (response.status === 'not_authorized') {
                // The person is logged into Facebook, but not your app.
                document.getElementById('status').innerHTML = 'Please log ' +
                'into this app.';
            } else {
                // The person is not logged into Facebook, so we're not sure if
                // they are logged into this app or not.
                document.getElementById('status').innerHTML = 'Please log ' +
                'into Facebook.';
            }
        }

        // This function is called when someone finishes with the Login
        // Button.  See the onlogin handler attached to it in the sample
        // code below.
        function checkLoginState() {

            FB.getLoginStatus(function(response) {
                statusChangeCallback(response);
                if (response.status === 'connected') {
                    // Logged into your app and Facebook.
                    addSubscriber();
                }
            });
        }

        window.fbAsyncInit = function() {
            FB.init({
                appId: '755035674549332', 
                oauth   : true,
                status  : true, // check login status
                cookie  : true, // enable cookies to allow the server to access the session
                xfbml   : true // parse XFBML
            });

            FB.getLoginStatus(function(response) {
                statusChangeCallback(response);
            });

        };

        // Load the SDK asynchronously
        (function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));

        // Here we run a very simple test of the Graph API after login is
        // successful.  See statusChangeCallback() for when this call is made.
        function testAPI() {
            FB.api(
                '/me', 
                function(response) {
                    document.getElementById('status').innerHTML =
                    'Logging in as, ' + response.name + '!';
            });
        }

        // function addSubscriber() {
        //     FB.api(
        //         '/me', 
        //         function(response) {
                    
        //             // FETCH DATA
        //             var subscriber_name = response.name,
        //                 subscriber_phone = 'NULL',
        //                 subscriber_email = response.email;

        //             // CHECK EXISTING EMAIL
        //             var url = '<?php echo $this->Html->url(array("controller"=>"newsLetters", "action"=>"check_existing_email")) ?>' + "/"  + subscriber_email ;

        //             $.ajax({
        //                 type: 'POST',
        //                 url: url,
        //                 data: { email : subscriber_email },
        //                 dataType: 'json',
                        
        //                 beforeSend: function(){

        //                     $('.fb-loading').hide(); //hide facebook button
        //                     $('.fb-ajax-loading').show(); //show loading image

        //                 },
        //                 success: function (data){

        //                     var result = data.result;

        //                     if(result){

        //                         $('.fb-loading').show(); //show facebook button
        //                         $('.fb-ajax-loading').hide(); //hide loading image
                                            
        //                         $("div#subcribe-form").hide();
        //                         $("div.modal-backdrop").hide();

        //                         $.msgBox({
        //                             title:"Alert",
        //                             content:"You're Already Subscribed to the Newsletter!",
        //                             type:"info"
        //                         });
                                
        //                     }else{
                                
        //                         var url = '<?php echo $this->Html->url(array("controller"=>"newsLetters", "action"=>"subscribe")) ?>' + "/"  + subscriber_name + "/" + subscriber_phone + "/" + subscriber_email ;

        //                         $.ajax({
        //                             type: 'POST',
        //                             url: url,
        //                             data: { name:subscriber_name, phone:subscriber_phone, email:subscriber_email },
        //                             dataType: 'json',
                                    
        //                             beforeSend: function(){
        //                             },
        //                             success: function (data){

        //                                 console.log(data);

        //                                 var result = data.result;

        //                                 if(result){

        //                                     $('.fb-loading').show(); //show facebook button
        //                                     $('.fb-ajax-loading').hide(); //hide loading image

        //                                     $("div#subcribe-form").hide();
        //                                     $("div.modal-backdrop").hide();

        //                                     $.msgBox({
        //                                         title:"Success",
        //                                         content:"You have successfully subscribed to our newsletter!"
        //                                     });
                                            
        //                                 }else{

        //                                     $.msgBox({
        //                                         title: "Failed",
        //                                         content: "Failed to add!",
        //                                         type: "error",
        //                                         buttons: [{ value: "Ok" }],
        //                                     });

        //                                 }

        //                             },

        //                             error: function(xhr, status, error) {
        //                                 var err = eval("(" + xhr.responseText + ")");
        //                                 console.log(err);
        //                             }

        //                         });
        //                     }

        //                 },

        //                 error: function(xhr, status, error) {
        //                     var err = eval("(" + xhr.responseText + ")");
        //                     console.log(err);
        //                 }

        //             });
        //             // END
        //     });
        // }

    </script>

    <?php echo $this->element('header'); ?>

    <?php echo $this->Session->flash(); ?>

    <?php 

        echo $this->fetch('content'); 

        //echo $this->element('sql_dump') ;
    ?>

    <?php echo $this->element('footer'); ?>

    
    
    <script type="text/javascript" language="javascript"></script>

    <?php 
        echo $this->Html->script('bootstrap-hover-dropdown.min')."\n";
        echo $this->Html->script('jquery.bxslider')."\n";
        echo $this->Html->script('jquery.nivo.slider.pack')."\n";
        echo $this->Html->script('owl-carousel/owl.carousel')."\n";
        echo $this->Html->script('jquery.fancybox')."\n";
        echo $this->Html->script('jquery.easing.1.3')."\n";
        echo $this->Html->script('progressbar-plugin')."\n";
        echo $this->Html->script('jquery.fittext')."\n";        
        echo $this->Html->script('color-picker')."\n";
        echo $this->Html->script('jquery.retina')."\n";
        // echo $this->Html->script('html5lightbox')."\n";
        echo $this->Html->script('jquery.dotdotdot')."\n";
        echo $this->Html->script('rating_star')."\n";
        echo $this->Html->script('parsley.min')."\n";
        echo $this->Html->script('bootstrap-datepicker.js')."\n";
        echo $this->Html->script('custom')."\n";

        echo $this->Html->script('jquery.plugin')."\n";
        echo $this->Html->script('jquery.countdown')."\n";
        echo $this->Html->script('bootstrap-modal')."\n";
        echo $this->Html->script('jquery.msgBox')."\n";

        // Rattana - Slider
        echo $this->Html->script('slider/js/bjqs-1.3.min')."\n";        
        
    ?>

</body>
</html>