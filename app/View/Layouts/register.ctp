<?php
/**
 *
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
// $cakeDescription = __d('cake_dev', '');
$cakeDescription = "All Deal";
// $metaDescription = substr($contact_option['Contact']['about'],0,300).'...';

?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<?php echo $this->Html->charset('ISO-8859-1');?>
	
	<title>
		<?php echo $cakeDescription ?>:
		<?php echo "Business Registration"; ?>
	</title>
	<?php

		// echo $this->Html->meta('keywords', $metaDescription, array('inline'=>false));
		echo $this->Html->meta('icon');

		// echo $this->Html->css('cake.generic');
		echo $this->Html->css('bootstrap')."\n";
		echo $this->Html->css('bootstrap-theme.min')."\n";
		echo $this->Html->css('jquery.fancybox')."\n";
		echo $this->Html->css('infinity-glyph')."\n";
		echo $this->Html->css('styles', array('id'=>'color-style'))."\n";
		echo $this->Html->css('rating_star')."\n";
		echo $this->Html->css('datepicker')."\n";
		// echo $this->Html->css('chosen')."\n";
		echo $this->Html->css('menu')."\n";
		echo $this->Html->css('jquery.countdown')."\n";
		echo $this->Html->css('msgBoxLight')."\n";

		echo $this->fetch('meta')."\n";
		echo $this->fetch('css')."\n";
		echo $this->fetch('script')."\n";
		

		echo $this->Html->script('jquery-1.10.2.min')."\n";
		echo $this->Html->script('bootstrap.min')."\n";
		
	
	?>
	<!-- <meta name="viewport" content="width=device-width, user-scalable=no"> -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

</head>

<body>

	<div id="fb-root"></div>
	<script>
		(function(d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) return;
			js = d.createElement(s); js.id = id;
			js.src = "//connect.facebook.net/en_GB/all.js#xfbml=1";
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
	</script>

	<?php echo $this->element('header'); ?>

	<?php echo $this->Session->flash(); ?>

	<?php echo $this->fetch('content'); ?>

	<?php echo $this->element('footer'); ?>

	<script type="text/javascript" language="javascript"></script>

	<?php 
		echo $this->Html->script('bootstrap-hover-dropdown.min')."\n";
		echo $this->Html->script('jquery.bxslider')."\n";
		echo $this->Html->script('jquery.nivo.slider.pack')."\n";
		echo $this->Html->script('owl-carousel/owl.carousel')."\n";
		echo $this->Html->script('jquery.fancybox')."\n";
		echo $this->Html->script('jquery.easing.1.3')."\n";
		echo $this->Html->script('progressbar-plugin')."\n";
		echo $this->Html->script('jquery.fittext')."\n";		
		echo $this->Html->script('color-picker')."\n";
		echo $this->Html->script('jquery.retina')."\n";
		echo $this->Html->script('html5lightbox')."\n";
		echo $this->Html->script('jquery.dotdotdot')."\n";
		echo $this->Html->script('rating_star')."\n";
		echo $this->Html->script('parsley.min')."\n";
		echo $this->Html->script('bootstrap-datepicker.js')."\n";
		echo $this->Html->script('custom')."\n";

		echo $this->Html->script('jquery.plugin')."\n";
		echo $this->Html->script('jquery.countdown')."\n";
		echo $this->Html->script('jquery.msgBox')."\n";
		
	?>

</body>
</html>
