<?php
/**
 *
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */



/********************************************************************************

File Name: Layouts/default.ctp
Description: 

Powered By: ABi Investment Group Co., Ltd,

Changed History:

    Date                Author              Description
    2014/06/10          Sim Chhayrambo      Initial Version
    2014/06/24          Sim Chhayrambo      Add Modal.js

*********************************************************************************/


// $webDescription = __d('cake_dev', '');
$webDescription = "All Deal";
$metaDescription = "all deals, deal, local deal, phnom penh deal, siem reap deal, travel, food and drink, restaurant, accommodation, hotel, shopping, thing to do, grocery, beauty and spa, activity";

?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:og="http://ogp.me/ns#"
      xmlns:fb="http://www.facebook.com/2008/fbml">
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# website: http://ogp.me/ns/website#">

    <!-- FOR FB SHARE -->
    <!-- <meta property="og:title" content="<?php echo $ogTitle ?>"/>
    <meta property="og:type" content="Deal"/>
    <meta property="og:url" content="<?php echo $ogUrl ?>"/>
    <meta property="og:image" content="<?php echo $ogImage ?>"/>
    <meta property="og:image:width" content="220" />
    <meta property="og:image:height" content="134" />
    <meta property="og:site_name" content="<?php echo $ogStieName ?>"/>
    <meta property="fb:admins" content="USER_ID"/>
    <meta property="og:description" content="<?php echo $ogDescription ?>"/> -->

    <?php echo $this->Html->charset(); ?>
    <title>

        <?php echo $webDescription ?> |

        <?php
            $title_for_layout = "Merchants";
            echo $title_for_layout;
        ?>
    </title>
    <?php

        echo $this->Html->meta('keywords', $metaDescription, array('inline'=>false));
        echo $this->Html->meta('icon');

        // echo $this->Html->css('cake.generic');
        echo $this->Html->css('bootstrap')."\n";
        echo $this->Html->css('styles')."\n";


        echo $this->fetch('meta')."\n";
        echo $this->fetch('css')."\n";
        echo $this->fetch('script')."\n";
        

        echo $this->Html->script('jquery-1.10.2.min')."\n";
        echo $this->Html->script('bootstrap.min')."\n";        
    
    ?>
    <!-- <meta name="viewport" content="width=device-width, user-scalable=no"> -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

    <link href="https://netdna.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">

</head>

<body>

    <?php echo $this->fetch('content'); ?>

    <?php //echo $this->element('footer'); ?>
    
    <script type="text/javascript" language="javascript"></script>

    <?php 
        echo $this->Html->script('bootstrap-hover-dropdown.min')."\n";
        echo $this->Html->script('jquery.bxslider')."\n";
        echo $this->Html->script('jquery.nivo.slider.pack')."\n";
        echo $this->Html->script('owl-carousel/owl.carousel')."\n";
        echo $this->Html->script('jquery.fancybox')."\n";
        echo $this->Html->script('jquery.easing.1.3')."\n";
        echo $this->Html->script('progressbar-plugin')."\n";
        echo $this->Html->script('jquery.fittext')."\n";        
        echo $this->Html->script('color-picker')."\n";
        echo $this->Html->script('jquery.retina')."\n";
        // echo $this->Html->script('html5lightbox')."\n";
        echo $this->Html->script('jquery.dotdotdot')."\n";
        echo $this->Html->script('rating_star')."\n";
        echo $this->Html->script('parsley.min')."\n";
        echo $this->Html->script('bootstrap-datepicker.js')."\n";
        echo $this->Html->script('custom')."\n";

        echo $this->Html->script('jquery.plugin')."\n";
        echo $this->Html->script('jquery.countdown')."\n";
        echo $this->Html->script('bootstrap-modal')."\n";
        echo $this->Html->script('jquery.msgBox')."\n";

        
    ?>

</body>
</html>