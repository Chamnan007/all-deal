<?php 

/********************************************************************************

File Name: search.ctp
Description: UI for searching deals

Powered By: ABi Investment Group Co., Ltd,

Changed History:

    Date                Author              Description
    2014/06/10          Sim Chhayrambo      Initial Version

*********************************************************************************/



// SEARCH PARAMS
$arrLocation = array();
$arrInterest = array();
$arrService = array();
$arrSubCategory = array();

if(!empty($subCategory)){
    $arrSubCategory = $subCategory;
}

if(!empty($filterLocation)){
    $arrLocation = $filterLocation;
}

if(!empty($filterInterest)){
    $arrInterest = $filterInterest;
}

if(!empty($filterService)){
    $arrService = $filterService;
}

?>

<div class="container">
    <!-- <div class="col-sm-12 no-padding" style="margin: 10px 0;">
        <a href="">
            <img src="<?php echo $this->webroot . "img/07.jpg"?>" width="940" height="100">
        </a>
    </div> -->
    <div style="height: 20px;"></div>
    <div class="col-sm-12 no-padding" style="margin: 10px 0; border-bottom: 1px dotted #999;">
        <?php 
        if(!empty($searchText)){ ?>
            <h1 style="line-height: 40px; font-size: 28px;">Searching for "<?php echo $searchText ?>"</h1>
        <?php } ?>
    </div>
    <div class="col-sm-12 no-padding clearfix" style="margin: 20px 0;">
        <div class="col-sm-3 no-padding-left">

            <form method="get" name="frmFilter" id="frmFilter" action="<?php echo $this->Html->url(array('controller'=>'deals', 'action'=>'search')) ?>">
            <div class="categories-wrapper">
                <h3 style="margin-bottom: 5px; font-weight: bold;">
                    Location
                    <button name="filter" class="pull-right">Filter</button>
                </h3>
                <ul>
                    <?php 
                        $strChecked = "";
                        if($searchCity == "all") $strChecked = "checked='checked'";
                    ?>
                    <li class="category">
                        <div class="category-name">
                            <input value="all" type="checkbox" class="location" id="all_location" name="location[]" <?php echo $strChecked ?>>
                            <label for="all_location">
                                <span class="name truncated">All</span>
                                <!-- <span class="deal-counts">(23)</span> -->
                            </label>
                            
                        </div>
                    </li>
                    <?php 
                    foreach ($cities as $cityKey => $cityValue) { 
                        $strChecked = "";
                        if ($cityValue['City']['city_name'] == $searchCity){
                            $strChecked = "checked='checked'";
                        }
                    ?>
                    <li class="category">
                        <div class="category-name">
                            <input value="<?php echo $cityValue['City']['city_name'] ?>" type="checkbox" id="<?php echo "province_" . $cityValue['City']['id'] ?>" class="location" name="location[]" <?php echo $strChecked ?>>
                            <label for="<?php echo "province_" . $cityValue['City']['id'] ?>">
                                <span class="name truncated"><?php echo $cityValue['City']['city_name'] ?></span>
                                <!-- <span class="deal-counts">(18)</span> -->
                            </label>
                        </div>
                    </li>
                    <?php } ?>
                </ul>
            </div>
            <div class="categories-wrapper">
                <h3 style="margin-bottom: 5px; font-weight: bold;">Interest</h3>
                <ul>
                    <li class="category">
                        <div class="category-name">
                            <input type="checkbox" id="all_interest" value="" class="interest" name="interest[]">
                            <label for="all_interest">
                                <span class="name truncated">All</span>
                                <!-- <span class="deal-counts">(120)</span> -->
                            </label>
                        </div>
                    </li>
                    <?php 
                    foreach ($interestCate as $iKey => $iValue) { 
                        $strChecked = "";
                        if(in_array($iValue['InterestsCategory']['category'], $arrInterest)){
                            $strChecked = "checked='checked'";
                        }
                    ?>
                    <li class="category">
                        <div class="category-name">
                            <input value="<?php echo $iValue['InterestsCategory']['category'] ?>" type="checkbox" id="<?php echo "interest_" . $iValue['InterestsCategory']['id'] ?>" class="interest" name="interest[]" <?php echo $strChecked ?>>
                            <label for="<?php echo "interest_" . $iValue['InterestsCategory']['id'] ?>">
                                <span class="name truncated"><?php echo $iValue['InterestsCategory']['category'] ?></span>
                                <!-- <span class="deal-counts">(10)</span> -->
                            </label>
                        </div>
                    </li>
                    <?php } ?>
                </ul>
            </div>
            <div class="categories-wrapper">
                <h3 style="margin-bottom: 5px; font-weight: bold;">Service</h3>
                <ul>
                    <li class="category">
                        <div class="category-name">
                            <input type="checkbox" id="all_service" value="" class="service" name="service[]">
                            <label for="all_service">
                                <span class="name truncated">All</span>
                                <!-- <span class="deal-counts">(120)</span> -->
                            </label>
                        </div>
                    </li>
                    <?php 
                    foreach ($serviceCate as $sKey => $sValue) { 
                        $strChecked = "";
                        if(in_array($sValue['ServicesCategory']['category'], $arrService)){
                            $strChecked = "checked='checked'";
                        }
                    ?>
                    <li class="category">
                        <div class="category-name">
                            <input value="<?php echo $sValue['ServicesCategory']['category'] ?>" type="checkbox" id="<?php echo "service_" . $sValue['ServicesCategory']['id'] ?>" class="service" name="service[]" <?php echo $strChecked ?>>
                            <label for="<?php echo "service_" . $sValue['ServicesCategory']['id'] ?>">
                                <span class="name truncated"><?php echo $sValue['ServicesCategory']['category'] ?></span>
                                <!-- <span class="deal-counts">(10)</span> -->
                            </label>
                        </div>
                    </li>
                    <?php } ?>
                </ul>
            </div>
            </form>
        </div>
        <div class="col-sm-9 no-padding">
            <?php if(!empty($deals)){ ?>
            <div class="col-sm-12 no-padding clearfix">
                <div class="recent-works col-sm-12 no-padding">
                    <div class="recent-works-carousel">
                        <div class="carousel slide" id="Recentworks">
                            <div class="carousel-inner">
                                <div class="item active">
                                    <ul class="thumbnails">
                                        <div class="row" id="deal-container">
                                            <?php 
                                            foreach ($deals as $dKey => $dValue) { 
                                                $split = end(explode("/", $dValue['Deal']['image']));
                                                $thumb_img =  "img/deals/thumbs/" . $split ;
                                            ?>
                                                <li class="col-sm-4 col-xs-4">
                                                    <div class="new-project bg-shadow add-margin-bottom">
                                                        <div>
                                                            <a href="<?php echo $this->Html->url(array('controller'=>'deals', 'action'=>'detail', $dValue['Deal']['slug'])) ?>">
                                                                <img src=" <?php echo $this->webroot . $thumb_img ?>" width="100%" height="134" title="Title">
                                                            </a>
                                                        </div>
                                                        <div id="bx-pager">
                                                            <a data-slide-index="0" href="#" class="active">
                                                                <div class="new-project-body">
                                                                    <h4 class="add-min-height"><?php echo $dValue['Deal']['title'] ?></h4>
                                                                    <h4 class="price-off"><?php echo $dValue['Deal']['discount_category'] ?></h4>
                                                                </div>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </li>
                                            <?php } ?>
                                        </div>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php }else{?>
            <h1 style="text-align: center;">No Deals Found...</h1>
            <?php } ?>
        </div>
    </div>
</div>