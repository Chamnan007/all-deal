<?php 

/********************************************************************************

File Name: detail.ctp
Description: displaying detail for deals

Powered By: ABi Investment Group Co., Ltd,

Changed History:

    Date                Author              Description
    2014/06/10          Sim Chhayrambo      Initial Version
    2014/06/16          Sim Chhayrambo      Add marker to google map
    2014/06/19          Sim Chhayrambo      Check empty data and add meta description
    2014/06/24          Sim Chhayrambo      Change UI, add business website
    2014/07/13          Sim Chhayrambo      Remove space in thumbnail, limit title
    2014/07/22          Sim Chhayrambo      Remove words "all deal" from meta description
    2015/02/23          Phea Rattana        Add Multiple Locations on Map, at related item link, 
                                            Random Images deal, check for operation hour

*********************************************************************************/

if(!empty($data)){

$strPayment = "";

$dealInfo = $data['Deal'];
$id = $dealInfo['id'];
$title = $dealInfo['title'];
// $image = $dealInfo['image'];
$desc = $dealInfo['description'];
$conditions = $dealInfo['deal_condition'];
$discount = $dealInfo['discount_category'];
$validTo = $dealInfo['valid_to'];

if ( $validTo >= date("Y-m-d H:i:s") ) {
 
$image = $dealInfo['image'];
if( $other_images = $dealInfo['other_images']) {
    $other_images = json_decode($other_images);
    $images = array();
    foreach( $other_images as $k => $v ){
        if( $v != "img/deals/default.jpg" ){
            $images[] = $v;
        }
    }
}else{
    $images[] = $image;
}
// Biz Info
// ================================
$businessInfo = $data['Business'];

$businessName = $businessInfo['business_name'];

$businessStreet = $businessInfo['street'];
$businessLocation = $businessInfo['location'];
$businessProvince = $data['Province']['province_name'];
$businessCity   = $data['City']['city_name'];
$businessSangkat = ($data['BusinessInfo']['SangkatInfo']['name'])?$data['BusinessInfo']['SangkatInfo']['name']:"";

$businessEmail  = $businessInfo['email'];
$businessPhone1 = $businessInfo['phone1'];
$businessPhone2 = ($businessInfo['phone2'])?" / " . $businessInfo['phone2']:""; ;
$businessLogo = $businessInfo['logo'];
$businessDescription = $businessInfo['description'];
$businessLat = $businessInfo['latitude'];
$businessLong = $businessInfo['longitude'];
$businessSlug = $businessInfo['slug'];
$payment = json_decode($businessInfo['accept_payment']);

if(!empty($payment)){
    foreach( $payment as $key => $value ){
        $strPayment .= $this->Html->image( $value . '.jpg', array(  'alt' => ucfirst($value),
              'title'=>ucfirst($value),
              'style'=>'width:40px; margin-right:5px;'));
    }
}

$strCategory = "";
$businessMainCategory = $businessCategory['main_category'];
$businessSubCategory = $businessCategory['sub_category'];
if(!empty($businessSubCategory)){
    $strCategory = $businessSubCategory . " - " . $businessMainCategory;
}else{
    $strCategory = $businessMainCategory;
}

$businessWebsite = str_replace("http://", "", $businessInfo['website']);
$businessWebsite = str_replace("https://", "", $businessInfo['website']);
$businessWebsite = str_replace("www.", "", $businessWebsite);
$businessWebsite = "www." . $businessWebsite;
$businessWebsite = trim($businessWebsite, "/");

$full_url = Router::url( $this->here, true );

$metaTitle = trim($title);
$metaInfo = strip_tags($desc);
$newMetaDescription = $metaTitle . ". " . $metaInfo;

if(strlen($newMetaDescription) >= 300){
    $newMetaDescription = substr($newMetaDescription,0,300) . '...';
}

$metaDescription = $newMetaDescription;
$this->Html->meta('description', $metaDescription, array('inline'=>false));

$data_braches = array();
$time = time();

if( isset($branches) && !empty($branches) ){
  foreach( $branches as $k => $val ){
     $time += 300;

     $data_braches[$k]['index']         = $time;
     $data_braches[$k]['branch_name']   = $val['BusinessBranch']['branch_name'];
     $data_braches[$k]['latitude']      = $val['BusinessBranch']['latitude'];
     $data_braches[$k]['longitude']     = $val['BusinessBranch']['longitude'];
  }

}

$deal_items = $data['DealItemLink'];

$prev_original  = 0;
$prev_discount  = 0;

if( $deal_items ){

    $max_percentage = 0;

    foreach( $deal_items as $k => $v ){

        $ori        = ($v['original_price'] != 0 )?$v['original_price']:$v['ItemDetail']['price'];
        $disc       = $v['discount'];
        $item_id    = $v['item_id'];

        $percentage = 100 - ( $disc * 100 / $ori );

        if( $percentage > $max_percentage ){
            $max_percentage = $percentage;
            $prev_original  = $ori;
            $prev_discount  = $disc;
        }else if( $percentage == $max_percentage ){
            if( $disc < $prev_discount ){
                $prev_original  = $ori;
                $prev_discount  = $disc;
            }
        }

    }

    $original = "$" . number_format($prev_original, 2);
    $discount = "$" . number_format($prev_discount, 2);                        
}

?>

<style>

    .deal-category p{
        display: inline-block;
        width: 49%;
        vertical-align: top;
    }
                    
    .related-item-container{
        width: 133px;
        height: 150px;
        border: 1px solid #CCC;
        background: #DDD;
        float:left;
        margin-right:10px;
        margin-bottom: 10px;
        position: relative;
    }

    .related-item-container .related-img{
        position: absolute;
        /*border: 1px solid red ;*/
        width: 133px ;
        height: 75px;
    }

    .related-item-container .related-img img{
        width: 131px;
        height: 100%;
    }

    .related-item-container .related-info{
        position: absolute;
        /*border: 1px solid blue ;*/
        width: 130px ;
        height: 70px;
        bottom: 0px;
        padding: 3px;
    }

    .related-item-container .related-info h4{
        font-size: 16px;
        color:#333;
    }

    .item-price{
        position: absolute;
        bottom: 0px;
        width: 127px;
    }

    .original-price{
        text-align: left;
        width: 100%;
        color: #333;
        font-weight: normal;
        text-decoration: line-through;
        color: red;
    }

    .discounted-price{
        text-align: right;
        width: 100%;
        color: #06F;
        font-size: 18px;
    }


    .related-item-container:hover,
    .original-price,
    .discounted-price{
        cursor: pointer;
    }

    .my-btn{
        border: 1px solid #8dc63f ;
        color: #8dc63f  !important;
        font-weight: bold;
        background: white;
        color: white !important;
        background: #8dc63f  !important;
    }

    .my-btn:hover{
        cursor: pointer;

        -moz-text-shadow: 1px 1px 2px #333;
        -webkit-text-shadow: 1px 1px 2px #333;
        text-shadow: 1px 1px 2px #333;

        background: #83c41a !important;
        border: 1px solid #83c41a;
    }

   
    .alert-error{
        color: red;
        background: rgb(200, 54, 54) !important; /* The Fallback */
        background: rgba(200, 54, 54, 0.5) !important;
    }
    .alert-dismissable .close{
        top: -35px !important;
        right: -30px !important;
        color: #666 !important;
    }
</style>


<div class="container">
   
    <div style="height: 20px;"></div>
    <div class="col-sm-12 no-padding" style="margin: 10px 0;">
        <h1 style="line-height: 40px; font-size: 28px;"><?php echo $title; ?></h1>
    </div>

    <div class="col-sm-12 no-padding clearfix">
        <div class="col-sm-3 no-padding-left">
            <div class="special-widget">
                <div class="swidget-body">
                    <ul>
                        <li style="padding: 0;">
                            <a href="<?php echo $this->Html->url(array('controller'=>'businesses', 'action'=>'detail', $businessSlug)) ?>">
                                <img src="<?php echo $this->webroot . $businessLogo ?>" width="220">
                            </a>
                            
                        </li>
                        <?php if($deal_items && !empty($deal_items) ){ ?>
                        <li style="overflow:hidden">
                            <h4 style="color:#888; font-size:18px; float:left; margin-right: 10px; text-decoration: line-through;"><?php echo $original ?></h4>
                            <h4 class="price-off pull-right" style="font-size:18px;"><?php echo $discount ?></h4>
                        </li>
                        <?php }else if(!empty($discount)){ ?>
                        <li>
                            <h4 class="price-off" style="font-size: 18px;"><?php echo $discount; ?></h4>
                        </li>
                        <?php } ?>
                        <li style="padding-bottom:20px;">
                            <h4 style="color: orange;"><i class="icon-clock"></i><?php echo __('Expire On') ?> : <?php echo date("d M Y", strtotime($validTo)); ?></h4>
                            <div id="defaultCountdown"></div>
                        </li>
                        <li>
                            <div class="fb-share-button" data-href="<?php echo $full_url ?>" data-type="button_count"></div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-sm-9 no-padding" id="banner-fade">
            <!-- <img src="<?php echo $this->webroot . $image?>" width="705" height="422"> -->
            <ul class="bjqs">
                <?php foreach( $images as $key => $img ){ ?>
                    <li><img src="<?php echo $this->webroot . $img ;?>" title="" ></li>
                <?php } ?>
            </ul>
        </div>

    </div>

    <div class="col-sm-12 no-padding clearfix" style="margin-top: 20px;">
        <div class="col-sm-8 no-padding-left">

            <article id="article">

                <!-- Item Listing -->
                <?php if($deal_items): ?>
                    <h2 style="float:left"><?php echo __('Item List'); ?></h2>
                    
                    <?php if($deal_items && !empty($deal_items) ){ ?>
                        <div style="text-align: right; position:relative; float:right;">
                            <a  href="#" id="btn-buy-deal"
                                class="btn my-btn btn-buy-deal"
                                deal-id="<?php echo $id ?>"
                                title="<?php echo __('Buy') ?>" ><?php echo __('BUY') ?>
                            </a>
                            <div class="ajax-loading" style="position:absolute; top:1px; right: 10px;display:none; ">
                                <img src="<?php echo $this->webroot . "img/bx_loader.gif" ?>">
                            </div>
                        </div>
                    <?php } ?>

                    <div class="clearfix"></div>
                    <table id="detail-table" style="margin-top:10px;">
                        <thead>

                            <tr style="font-size:13px;">
                                <th width="100px"><?php echo __('Image'); ?></th>
                                <th style="text-align:left"><?php echo __('Name') ?></th>
                                <th width="120px;" style="text-align:right; padding-right:5px;"><?php echo __('Original Price') ?></th>
                                <th width="140px;" style="text-align:right; padding-right:5px;">
                                    <?php echo __('Discounted Price') ?>
                                </th>
                                <th style="text-align:right; padding-right:5px;" width="100px;"><?php echo __('Saved') ?></th>
                            </tr>

                        </thead>

                        <tbody>        
                            <?php foreach( $deal_items as $k => $item ): 

                                    $full_img   = $item['ItemDetail']['image'];
                                    $thumb_img  = str_replace('/menus/', '/menus/thumbs/', $item['ItemDetail']['image']);

                                    $item_desc = strip_tags($item['ItemDetail']['description']);

                                    $limit_length = 43;
                                    if(strlen($item_desc) > $limit_length){
                                        $item_desc = substr($item_desc,0, $limit_length - 3 ) .'...';   
                                    }

                                    $ori_price = ($item['original_price'] != 0 )?$item['original_price']:$item['ItemDetail']['price'] ;

                                    $saved_price = $ori_price - $item['discount'];

                                    $saved_percentage = $saved_price * 100 / $ori_price ;
                            ?>
                                <tr>
                                    <td>
                                        <div class="div-img" title="Click to see detail information"
                                             data-title='<?php echo $item['ItemDetail']['title'] ?>'
                                             data-img ='<?php echo $this->webroot . $full_img ?>'
                                             data-desc='<?php echo $item['ItemDetail']['description'] ?>'
                                            >
                                            <img src="<?php echo $this->webroot . $thumb_img ?>" 
                                                 alt="<?php echo $item['ItemDetail']['title'] ?>"
                                                 title="<?php echo $item['ItemDetail']['title'] ?>"
                                                 class="item-img"
                                                 data-img = "<?php echo $full_img ?>"
                                                 style="width:100px; padding: 2px; border: 1px solid #EEE; max-height: 100px; max-width:100px">

                                            <div class="view-detail">
                                                <div class="icon">
                                                    <img src="<?php echo $this->webroot . 'img/search-icon.png' ?>" style="width:100%">
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <strong style="font-size:13px"><?php echo $item['ItemDetail']['title'] ?></strong><br>
                                        <p style="font-size:12px; color:#555"><?php echo $item_desc ?></p>
                                    </td>

                                    <td style="font-size: 14px; color: #666; text-align: right; text-decoration: line-through">
                                        $ <?php echo number_format($ori_price, 2) ?>
                                    </td>

                                    <td style="font-size: 17px; color: #8dc63f ; font-weight: bold; text-align: right">
                                        $ <?php echo number_format($item['discount'], 2) ?>
                                    </td>
                                    
                                    <td style="text-align:right; padding-right:5px; font-size:14px; color: #666;">
                                        <?php echo "$ " . number_format($saved_price, 2) ?><br/>
                                        <strong><?php echo number_format($saved_percentage) . " %" ?></strong>
                                    </td>

                                </tr>

                            <?php endforeach ?>
                        </tbody>
                    </table>
                <?php endif ?>
                <!-- End Item Listing -->

                <!-- Deal Description -->
                <div class="article-entry">
                    <h2><?php echo __('Description'); ?></h2>
                    <?php echo $desc; ?>
                </div>

                <!-- Deal Conditions -->
                <div class="article-entry">
                    <h2><?php echo __('Conditions'); ?></h2>
                    <?php echo $conditions; ?>
                    
                    <p style="margin-top:20px;">
                        <span style="font-size:13px;">
                            <strong>Note:</strong> Cancellation policy is according to each store's policy.<br/>
                            IDMS (Cambodia), All Deal and its affiliates doesn’t warrant any products or services listed in this coupon.
                        </span>
                    </p>
                </div>

                <!-- Deal Company -->
                <div class="article-entry">
                    <h2><?php echo __('About') . " " . $businessName; ?></h2>
                    <?php echo $businessDescription ?>
                </div>

            </article>
            <?php 
            if(!empty($businessMedia)){ ?>
            <div class="col-sm-12 no-padding">
                <article id="article">
                    <div class="article-entry">
                        <div class="recent-works" style="margin: 0;">
                            <div class="recent-works-header clearfix">
                                <h2><?php echo __('Image Gallery') ?></h2>
                            </div>
                            <div class="recent-works-carousel">
                                <div class="carousel slide" id="Recentworks">
                                    <div class="carousel-inner">
                                        <div class="item active">
                                            <ul class="thumbnails">
                                                <div class="row">
                                                    <?php
                                                    foreach($businessMedia as $bKey => $bValue) { 
                                                        $strLeft = "";
                                                        if($bKey == 0) $strLeft = "style='left:70%;'";
                                                        $media_path = $bValue['BusinessesMedia']['media_path'];
                                                        if(!empty($media_path)){
                                                    ?>
                                                    <li class="col-sm-3 col-xs-3">
                                                        <div class="img-overlay">
                                                            <img src="<?php echo $this->webroot . $media_path ?>" width="100%" height="124">
                                                            <div class="recent-icons" <?php echo $strLeft; ?>>
                                                                <a href="<?php echo $this->webroot . $media_path ?>" class="fancybox"><i class="icon-search"></i></a>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <?php } } ?>
                                                </div>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
            <?php 
            if(!empty($businessMedia[0]['BusinessesMedia']['media_embeded'])){ ?>
            <div class="col-sm-12 clearfix" style="border: 1px solid #f2f2f2; padding: 10px 0;">
                <h2 style="margin: 10px 15px; font-weight: bold;"><?php echo __('Videos') ?></h2>
                <?php 
                foreach ($businessMedia as $buKey => $buValue) { 
                    $media_embeded = $buValue['BusinessesMedia']['media_embeded'];
                    $media_embeded = explode("?v=", $media_embeded);
                    $media_embeded = (isset($media_embeded[1]))?$media_embeded[1]:NULL;

                    if( $media_embeded != NULL) {
                ?>
                    <div class="col-sm-6">
                        <iframe 
                            width="100%" height="220"
                            src="<?php echo "http://www.youtube.com/embed/" . $media_embeded ?>" allowfullscreen>
                        </iframe>
                    </div>
                <?php } } ?>
            </div>
            <?php } } ?>
        </div>
        <div class="col-sm-4 no-padding-right info-container">
            <article id="article">
                <div class="article-entry">
                    <h2><?php echo __('Contact Info'); ?></h2>
                    <p style="font-weight: bold;"><?php echo $businessName ?></p>
                    <p><?php echo __('Address') . " : " . $businessStreet . ", " . $businessSangkat . ", " . $businessLocation . ", " . $businessCity;?></p>
                    <p><?php echo __('Phone') . " : " .  $businessPhone1 . $businessPhone2 ?></p>
                    <p><?php echo __('Email') . " : " . $businessEmail ?></p>
                    <p>
                        <?php echo __('Website') . " : ";?>
                        <a href="http://<?php echo $businessWebsite ?>" target="_blank" style="color: #ff9311; text-decoration: underline;"><?php echo $businessWebsite ?></a>
                    </p>
                    <p><?php echo __('Category') . " : " . $strCategory; ?></p>
                    <p>
                        <span style="float:left; margin-top:3px;"><?php echo __('Accept Payment') . " : "; ?></span>
                        <span style="float:left; margin-left:3px;"><?php echo $strPayment; ?></span>
                    </p><br><br>
                    <h2><?php echo __('Operating Hours'); ?></h2>
                    <?php 
                    foreach ($opHours as $opKey => $opValue) { 
                        $opDay = $opValue['OperationHour']['day'];
                        $opDay = substr($opDay, 0, 3);

                        if(!empty($opValue['OperationHour']['from'])){
                            $opFrom = date("h:i A", strtotime($opValue['OperationHour']['from']));
                        }else{
                            $opFrom = "";
                        }
                        
                        if(!empty($opValue['OperationHour']['to'])){
                            $opTo = date("h:i A", strtotime($opValue['OperationHour']['to']));   
                        }else{
                            $opTo = "";
                        }

                        if( !$opFrom && !$opTo ){
                            $opFrom = "Closed";
                            $opTo = "";
                        }else if( $opFrom == $opTo ){
                            $opFrom = "24 Hours";
                            $opTo   = "";
                        }else{
                            if( !$opFrom ){
                                $opFrom = "<i>Empty</i>";
                            }
                            if( !$opTo ){
                                $opTo = "<i>Empty</i>";
                            }
                        }

                        if( $opTo ){
                            $opTo = " - " . $opTo ;
                        }

                        echo "<p>";
                        echo "<span style='display:inline-block; width:48px; color: orange;'>" . strtoupper($opDay) . " : " . "</span>";
                        echo "<span>" . $opFrom . $opTo . "</span>";
                        echo "</p>";

                    } 

                    ?>
                </div>
            </article>
            <div style="width:100%; height:250px; margin:0 auto;" id="map-canvas"></div>
            <div style="height: 10px;"></div>
        </div>
    </div>
    <div class="col-sm-12 no-padding clearfix" style="margin-top: 30px;">
        <div class="container no-padding-left">
            <div class="recent-works col-sm-12 no-padding-left">
                <div class="recent-works-header clearfix">
                    <h2 style="width: 100%;">
                        <span style="float: left;"><?php echo __('Related Deals') ?></span>
                        <!-- <span>
                            <a href="#" class="button btn-green pull-right"><i class="icon-plus"></i> View All</a>
                        </span> -->
                    </h2>
                </div>
                <div class="recent-works-carousel">
                    <div class="carousel slide" id="Recentworks">
                        <div class="carousel-inner">
                            <div class="item active">
                                <ul class="thumbnails">
                                    <div class="row">
                                        <?php 
                                        foreach($relatedDeals as $reKey => $reValue) { 
                                            $split = end(explode("/", $reValue['Deal']['image']));
                                            $thumb_img =  "img/deals/thumbs/" . $split ;
                                            $business_name = $reValue['Business']['business_name'];
                                            $business_street = $reValue['Business']['street'];
                                            $business_location = $reValue['Business']['location'];
                                            $deal_title = $reValue['Deal']['title'];
                                            if(strlen($deal_title) > $limit_deal_title){
                                                $deal_title = substr($reValue['Deal']['title'],0,$limit_deal_title-3).'...';   
                                            }
                                            if(strlen($business_name) > $limit_business_title){
                                                $business_name = substr($business_name,0,$limit_business_title-3).'...';   
                                            }

                                            if( $other_deal_imgs = $reValue['Deal']['other_images'] ){
                                                $imgs = json_decode($other_deal_imgs);
                                                $data_img = array();

                                                foreach( $imgs as $k => $img ){
                                                    $data_img[] = $img;
                                                }

                                                $selected = array_rand($data_img);

                                                $thumb_img = str_replace("/deals/", "/deals/thumbs/", $data_img[$selected] );
                                            }
                                            
                                            $original = "";
                                            $discount = $reValue['Deal']['discount_category'];

                                            $prev_original  = 0;
                                            $prev_discount  = 0;

                                            if( $items = $reValue['DealItemLink'] ){

                                                $max_percentage = 0;

                                                foreach( $items as $k => $v ){

                                                    $ori        = ($v['original_price'] != 0 )?$v['original_price']:$v['ItemDetail']['price'];
                                                    $disc       = $v['discount'];
                                                    $item_id    = $v['item_id'];

                                                    $percentage = 100 - ( $disc * 100 / $ori );

                                                    if( $percentage > $max_percentage ){
                                                        $max_percentage = $percentage;
                                                        $prev_original  = $ori;
                                                        $prev_discount  = $disc;
                                                    }else if( $percentage == $max_percentage ){
                                                        if( $disc < $prev_discount ){
                                                            $prev_original  = $ori;
                                                            $prev_discount  = $disc;
                                                        }
                                                    }

                                                }

                                                $original = "$" . number_format($prev_original, 2) ;
                                                $discount = "$" . number_format($prev_discount, 2);                        
                                            }
                                        ?>
                                            <li class="col-sm-3 col-xs-3">
                                                <div class="new-project bg-shadow add-margin-bottom deal-img-container">
                                                    <div>
                                                        <a href="<?php echo $this->Html->url(array('controller'=>'deals', 'action'=>'detail', $reValue['Deal']['slug'])) ?>">
                                                            <img src="<?php echo $this->webroot . $thumb_img ?>" title="<?php echo $reValue['Deal']['title'] ?>">
                                                        </a>
                                                    </div>
                                                    <div id="bx-pager">
                                                        <a data-slide-index="0" href="<?php echo $this->Html->url(array('controller'=>'deals', 'action'=>'detail', $reValue['Deal']['slug'])) ?>" class="active">
                                                            <div class="new-project-body">
                                                                <h4><?php echo $deal_title ?></h4>
                                                                <i><?php echo $business_name ?></i><br>
                                                                <i class="icon-location"><?php echo $business_location . ", " . $reValue['Province']['province_name'] ?></i>
                                                                <div class="clearfix"></div>
                                                                <h4 class="price-off pull-right"><?php echo $discount ?></h4>
                                                                <h4 style="color:#666; float:right; margin-right: 10px; text-decoration: line-through;"><?php echo $original ?></h4>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </li>
                                        <?php } ?>
                                    </div>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
        </div>
    </div>
</div>

<div id="blur-background"></div>
<div id="item-list-modal">
    
    <h4 style="float:left; font-size: 23px;" id="item-title">Item List</h4>
    <div id="close" title="Close">X</div>
    <div class="clearfix"></div>
    <!-- <hr> -->

    <div id="content-show" style="padding-top:10px;">     
        
        <div id="flashMessage" class="alert alert-dismissable alert-warning" style="display:none;">
            <span></span>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        </div>

        <form action="<?php echo $this->Html->url(array('controller'=>'shops', 'action'=>'postAddToCart')) ?>" 
                method='POST' id='frmPurchaseItem' >        
            <div id="item-content">
                <table id="detail-table">
                    <thead>
                        <tr>
                            <th width="100px"><?php echo __('Image'); ?></th>
                            <th style="text-align:left"><?php echo __('Name') ?></th>
                            <th width="120px;"><?php echo __('Original Price') ?></th>
                            <th width="150px;"><?php echo __('Discounted Price') ?></th>
                            <th width="50px"><?php echo __('QTY') ?></th>
                            <th width="120px" style="text-align:right;"><?php echo __('Amount') ?></th>
                        </tr>
                    </thead>

                    <tbody class="deal-items-row" >      

                        <?php foreach( $deal_items as $k => $item ): 

                                $full_img   = $item['ItemDetail']['image'];
                                $thumb_img  = str_replace('/menus/', '/menus/thumbs/', $item['ItemDetail']['image']);

                                $amount = 0;
                        ?>
                            <tr>
                                <td>
                                    <img src="<?php echo $this->webroot . $thumb_img ?>" 
                                         alt="<?php echo $item['ItemDetail']['title'] ?>"
                                         title="<?php echo $item['ItemDetail']['title'] ?>"
                                         class="item-img"
                                         data-img = "<?php echo $full_img ?>"
                                         style="width:100px; padding: 2px; border: 1px solid #EEE;">
                                </td>
                                <td>
                                    <strong><?php echo $item['ItemDetail']['title'] ?></strong><br/>
                                    <span   style="font-size:13px; color: #666">Available Qty : 
                                        <strong style="font-size: 14px;"><?php echo number_format($item['available_qty'], 2) ?></strong>
                                    </span>
                                </td>

                                <td style="font-size: 15px; color: #666; text-align: center; text-decoration: line-through">
                                    $ <?php echo number_format($item['ItemDetail']['price'], 2) ?>
                                </td>

                                <td style="font-size: 15px; color: #06F; font-weight: bold; text-align: center">
                                    $ <?php echo number_format($item['discount'], 2) ?>
                                </td>

                                <td style="text-align:center">
                                    <input  class="input-qty" type="text" 
                                            style="width:40px; text-align:center;" 
                                            maxlength="1" value="0" 
                                            data-price="<?php echo $item['discount'] ?>"
                                            data-maxItem="<?php echo $maxItem ?>"
                                            data-available-qty="<?php echo $item['available_qty'] ?>"
                                            name="item[qty][]" />

                                    <input type="hidden" name="item[business_id][]" value="<?php echo $businessInfo['id'] ?>"/>
                                    <input type="hidden" name="item[deal_id][]" value="<?php echo $dealInfo['id'] ?>"/>
                                    <input type="hidden" name="item[item_id][]" value="<?php echo $item['ItemDetail']['id'] ?>"/>
                                    <input type="hidden" name="item[price][]" value="<?php echo $item['discount'] ?>"/>

                                </td>
                                <td class="amount" style="text-align:right; padding-right:10px; font-weight:bold;" data-amount="0">
                                    $ <?php echo number_format($amount, 2) ?>
                                </td>
                            </tr>

                        <?php endforeach ?>
                    </tbody>
                </table>
            </div>
        </form>
    </div>

    <div id="loading" style="position: absolute; top:30%; left: 45%; text-align: center; display:none;">
        <img src="<?php echo $this->webroot . "img/ajax-loader.gif" ?>" alt="">
        <h3>Loading...</h3>
    </div>

    <div id="message-box" style="display:none;">
        Successfully added to your card.
    </div>

    <h3 style="position:absolute; left:20px; bottom: 15px; font-size:22px;">
        Total Amount : <span style="color: #8dc63f " id="total-amount">$ 0.00</span>
    </h3>

    <input type="button" id="close-btn" class="btn btn-google" value="Close"
            style="position: absolute; bottom:10px; right:10px;" >

    <a  href="#" class="btn my-btn pay-now" 
        style="position:absolute; right:80px; bottom:10px;"><?php echo __('PAY NOW') ?>
    </a>
</div>

<div id="item-detail-modal">
    <div class="title">
        <h3 id="item-name">Detail</h3>
        <div id="close-modal" title="Close">X</div>
    </div>

    <div class="content">
        <div id="image"></div>
        <div id="desc">
            
        </div>
    </div>

    <div class="footer">
        <input type="button" id="close-btn-modal" class="btn btn-google" value="Close"
            style="position: absolute; bottom:10px; right:0px;" >
    </div>
</div>

<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
<script src="https://google-maps-utility-library-v3.googlecode.com/svn/tags/markerwithlabel/1.1.9/src/markerwithlabel.js" type="text/javascript"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

<script>

    var listBranchMarker = {};
    var map = null;
    var pin_icon_blue = '<?php echo $this->webroot ?>' + 'img/pin-blue.png';
    var iw = null;

    function addBranchMarker(index,latLng, brandName, latLngText) {

        listBranchMarker[index] = new MarkerWithLabel({
                map:map,
                position:latLng,
                icon: pin_icon_blue,
                labelContent: brandName  ,
                title:brandName,

                labelAnchor : new google.maps.Point(45, 67),
                labelClass : "labels-branch",
                labelInBackground : false
            });

        var b_name = new google.maps.InfoWindow({ content: brandName + latLngText });

        google.maps.event.addListener( listBranchMarker[index], "click", function (e) { b_name.open(map, this); });
        
    }

    function initialize() {


      var  latitude  = <?php echo $businessLat ?>,
           longitude = <?php echo $businessLong ?>,
           myLatlng  = new google.maps.LatLng( latitude, longitude ),
           businessesName = "<?php echo $businessName ?>";

      var image = '<?php echo $this->webroot ?>' + 'img/pin-yellow.png';

      var mapOptions = {
        zoom: 12,
        center: myLatlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      }

      map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

      var marker = new MarkerWithLabel({
              map: map,
              position: myLatlng,
              // title: businessesName,
              icon : image,
              labelContent : businessesName,
              labelAnchor : new google.maps.Point(26, 64),
              labelClass : "labels",
              labelColor : "red",
              labelInBackground : false
          });

        var iw = new google.maps.InfoWindow({
                       content: businessesName
                     });


        google.maps.event.addListener(marker, "click", function (e) { iw.open(map, this); });

        var data_branch = <?php echo json_encode($data_braches) ?>;
        $.each( data_branch, function( ind, val ){
            var name  = val.branch_name,
                lat   = val.latitude,
                lng   = val.longitude,
                index = val.index;

            var newLatLng  = new google.maps.LatLng(lat, lng );

            latLngText = " (" + lat + ", " + lng + ")";
            addBranchMarker( index ,newLatLng, name, latLngText );
        })

    }

    google.maps.event.addDomListener(window, 'load', initialize);

    $(function () {
        var austDay = new Date();
        austDay = new Date("<?php echo date('F d, Y H:i:s', strtotime($validTo)) ?>");
        $('#defaultCountdown').countdown({until: austDay});
        $('#year').text(austDay);

        // Slider
        $('#banner-fade').bjqs({
        height      : 422,
        width       : 705,
        responsive  : false
      });

    });

    $('#close, #close-btn').click( function(){
        
        if( $('#item-list-modal').is(':visible') ){
            $('#item-list-modal').animate({'top':'60px'});
            $('#item-list-modal').animate({'top':'-200%'}, function(){
                $(this).hide();
                $('#blur-background').hide();   
            });
        }

        document.onmousewheel = start;
    });

    function stop(){
        return false;
    }

    function start(){
        return true;
    }

    $('.btn-buy-deal').click( function(e){

        e.preventDefault();

        var me = $(this);
        var slug = "<?php echo $slug ?>";
        var logged = "<?php echo $__USER_LOGGED ?>";
        var loader = me.parent().find('div.ajax-loading');

        loader.show();
        $('#item-list-modal').show();

        setTimeout(function(){

            loader.hide();

            if( logged == '' ){

                var url = '<?php echo $this->Html->url(array( "controller" => "shops", "action"=>"signin")) ?>' ;
                url = url + "?ref=" + slug;
                
                window.location.href = url;
                
            }else{
                deal_id = me.attr('deal-id');

                // Check if deal still available
                var url     = '<?php echo $this->Html->url(array( "controller" => "deals", "action"=>"checkIfDealAvailableAjax__")) ?>' ;
                
                url += "/" + deal_id;

                console.log(url);
                $.ajax({

                    type: 'POST',
                    url: url,
                    data: {},
                    dataType: 'json',
                    
                    success: function (data){
                        
                        // if status == false => Deal is no longer available
                        if( data.status == false ){

                            var url = '<?php echo $this->Html->url(array( "controller" => "deals", "action"=>"detail")) ?>' ;
                            url = url + "/" + slug;
                            
                            window.location.href = url;

                        }else{

                            var content = $('div#content-show');

                            $('div#loading').hide();
                            $('div#message-box').hide();
                            $('#blur-background').show();

                            $('#item-list-modal').animate({'top':'60px'});
                            $('#item-list-modal').animate({'top':'50px'});

                            document.onmousewheel = stop;
                        }

                    },

                    error: function( err, msg ){
                        console.log(err);
                    }

                });
            }

        }, 300);


    });

    $('.add-to-cart').click( function(e){

        e.preventDefault();

        var me = $(this);
        var spinner = me.parent().find(".loading-spin");

        me.hide();
        spinner.show();


        setTimeout( function(){
            
            var url     = '<?php echo $this->Html->url(array( "controller" => "shops", "action"=>"addToCart")) ?>' ;
            var bis_id  = me.attr('data-business-id'),
                deal_id = me.attr('data-deal-id'),
                item_id = me.attr('data-item-id'),
                price   = me.attr('data-price');
            
            $.ajax({
                type: 'POST',
                url: url,
                data: { business_id : bis_id,
                        deal_id : deal_id,
                        item_id : item_id,
                        price   : price
                      },
                dataType: 'json',
                
                success: function (data){

                    if( data.status == true ){
                        var count = $('div#cart-count').text();

                        count = parseInt(count) + 1;

                        $('div#cart-count').text(count);
                        // Show message
                        spinner.hide();
                        me.show();
                        $('div#message-box').fadeIn();

                        setTimeout( function(){
                            $('div#message-box').fadeOut();
                        }, 1000);

                    }else{
                        location.reload();
                    }
                    
                    
                },
                error: function ( err ){
                    console.log(err);
                }

            });

        },200);

    });

    $('.item-img').mouseover( function(){
       
        var me = $(this),
            view = me.parent().find('div.view-detail');

        view.fadeIn();

    });

    $('.view-detail').mouseleave( function(){
       
        var me = $(this),
            view = me.parent().find('div.view-detail');

        view.fadeOut();

    });

    $('.view-detail').click( function( e ){
        e.preventDefault();
        
        var me      = $(this),
            parent  = me.closest('.div-img');

        var modal   = $('#item-detail-modal');

        var title = parent.attr('data-title');
        modal.find('#item-name').text(title);

        var img = "<img src='" + parent.attr('data-img') + "' style='width:100%;' />";
        modal.find('#image').html(img);

        var desc = parent.attr('data-desc');
        modal.find('#desc').html(desc);

        $('#blur-background').show();
        modal.show();
        modal.animate({marginTop:'40px'});
        modal.animate({marginTop:'30px'});

    });

    $('#close-modal, #close-btn-modal').click( function(){

        if( $('#item-detail-modal').is(':visible') ){
            $('#item-detail-modal').animate({marginTop:'40px'});
            $('#item-detail-modal').animate({marginTop:'-200%'}, function(){
                $('#item-detail-modal').hide();
                $('#blur-background').hide();
            });
        }

    })

    $(document).keydown(function(e) {
        
        // ESCAPE key pressed
        if (e.keyCode == 27) {

            if( $('#item-list-modal').is(":visible") ){

                $('#item-list-modal').animate({'top':'60px'});
                $('#item-list-modal').animate({'top':'-200%'}, function(){
                    $(this).hide();
                    $('#blur-background').hide();
                });

            }
            
            if( $('#item-detail-modal').is(':visible') ){
                $('#item-detail-modal').animate({marginTop:'40px'});
                $('#item-detail-modal').animate({marginTop:'-200%'}, function(){
                    $('#item-detail-modal').hide();
                    $('#blur-background').hide();
                });
            }

            document.onmousewheel = start;
        }

    });

    $('.pay-now').click(function(event){

        event.preventDefault();

        var form = $('form#frmPurchaseItem');

        form.submit();

    });

    $('input.input-qty').keydown(function(e){
        
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) || 
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }

    })

    $('input.input-qty').keyup(function(e){
        
        var me      = $(this),
            qty     = me.val(),
            price   = parseFloat(me.attr('data-price')),
            maxItem = parseFloat(me.attr('data-maxItem')),
            av_qty  = parseFloat(me.attr('data-available-qty') );

        if( qty === "" ){
            qty = 0;
        }
    
        if( av_qty < maxItem ){
            maxItem = av_qty;
        }    

        if( qty > maxItem ){
            qty = maxItem;
            me.val(maxItem);

            var msg = "Sorry! You can buy only " + maxItem + " units each time.";

            $('#flashMessage').find('span').text(msg);
            $('#flashMessage').fadeIn();
            setTimeout(function(){
                $('#flashMessage').fadeOut();
            },2000);

        }

        var amount = qty * price;

        var tr          = me.closest('tr');
        var td_amount   = tr.find('td.amount');

        td_amount.attr('data-amount', amount);

        amount = amount.toFixed(2);
        td_amount.text( "$ " + amount );

        var total_amount = calculateTotalAmount();

        total_amount = total_amount.toFixed(2);

        $('#total-amount').text('$ ' + total_amount);

    })

    
    function calculateTotalAmount(){

        var tbody = $('tbody.deal-items-row');
        var total = 0;

        var item_rows = tbody.find('tr');
        $.each( item_rows, function(ind, val){

            var row_amount  = $(val).find('td.amount');
            var amount      = row_amount.attr('data-amount');

            total = total + parseFloat(amount);
        })

        if( total == 0 ){
            $('.pay-now').hide();
        }else{
            $('.pay-now').show();
        }

        return total;
    }

    calculateTotalAmount();


</script>

<?php 

    }else{ require_once(dirname(__FILE__).'/../Elements/not_found.ctp'); }

}else{ require_once(dirname(__FILE__).'/../Elements/not_found.ctp'); }

?>