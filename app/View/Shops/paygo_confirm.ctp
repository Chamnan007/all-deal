<?php 
header('Access-Control-Allow-Origin: *');  

/********************************************************************************

File Name: index.ctp (Your Cart Shopping)
Description: for business registration form

Powered By: ABi Investment Group Co., Ltd,

Changed History:

    Date                Author              Description
    2015/10/06          Phea Rattana        Initial Version

*********************************************************************************/
?>


<style type="text/css">
    .register-form label{
        width: 140px;
    }
    .alert-error{
        color: red;
        background: rgb(200, 54, 54); /* The Fallback */
        background: rgba(200, 54, 54, 0.5);
    }
    .alert-dismissable .close{
        top: -35px;
        right: -30px;
        color: #666;
    }

    #history-list td{
        font-size: 12px !important;
    }

    #history-list td{
        padding-left: 10px;
    }

    .center{
        text-align: center !important;
    }

    .right{
        text-align: right !important;
    }

    #history-list input[type='text']{
        border: 1px solid #CCC !important;
        height: 25px !important;
        border-radius: 0px !important;
    }

    #history-list input[type='text']:focus{
        border: 1px solid #8dc63f  !important;
    }

    #history-list tr:hover{
        background: none !important;
    }

    .total{
        background: rgba(169, 228, 123, 0.35);
        width: 100%;
        padding: 20px 0;
        overflow: hidden;
    }

    .total .title{
        width: 80%;
        text-align: right;
        font-size: 20px;
        float: left;
    }

    .total .grand-total{
        float: right;
        font-size: 20px;
        padding-right: 50px;
    }

    .remove-item{
        float: right;
        width: 18px;
        height: 18px;
        background: url(img/remove.png);
        background-size: 18px 18px;
        cursor: pointer;
    }
    .remove-item:hover{
        background: url(img/remove-hover.png);
        background-size: 18px 18px;
    }
    

    .my-btn{
        border: 1px solid #8dc63f ;
        color: #8dc63f  !important;
        font-weight: bold;
        background: white;
    }

    .my-btn:hover{
        cursor: pointer;
        color: white !important;
        background: #8dc63f  !important;
    }
    .my-btn-active{
        color: white !important;
        background: #8dc63f  !important;
    }

    .my-btn-inactive{
        background-color: #222 !important;
        color: white !important;
    }

    li.msg-warning{
        padding-left: 10px !important;
        padding-right: 10px !important;
        display: none;
    }

    li.error{
        background: #FDE0E0;
        color: red;
    }

    li.success{
        background: #D3F5A6;
        color: green;
    }

</style>

<div class="register-form clearfix">
    
    <form action="<?php echo $this->Html->url(array("action"=>"paygoConfirm")) ?>?ref=<?php echo $token ?>" method="POST">
        <h1 style="text-transform: uppercase;"><?php echo __('PayGo Payment Confirmation'); ?></h1>
        
        <div class="col-sm-6 no-padding" style="min-height: 100px;">
            <legend>Purchase Summary</legend>

            <table style="margin-top: 10px; width:100%; font-size:12px;" id="history-list">
                <tr>
                    <th width="20px;">N<sup style="font-size:10px;">o</sup></th>
                    <th>Item</th>
                    <th class="center" width="100px;">Unit Price</th>
                    <th width="20px"></th>
                    <th class="center" width="50px">QTY</th>
                    <th width="80px" style="text-align:right">Subtotal</th>
                </tr>

                <?php 
                    $grand_total = 0;
                    
                    foreach( $items as $key => $val ){ 

                        $item_id        = $val['item_id'];
                        $item_qty       = $val['qty'];
                        $item_price     = $val['unit_price'];

                        $amount = $item_price * $item_qty;
                        $grand_total += $amount;

                        $detail = $val['ItemDetail'];
                        $thumb_img  = str_replace('/menus/', '/menus/thumbs/', $detail['image']);
                        $purchase_description[] = $detail['title'];

                    ?>
                    <tr class="item">
                        <td><?php echo $key + 1 ?></td>
                        <td><?php echo $detail['title'] ?></td>
                        <td class="center item-price">$ <?php echo number_format($item_price, 2) ?></td>
                        <td class="center">x</td>
                        <td class="center"><?php echo $item_qty ?></td>
                        <td class="right item-amount" style="padding-right:20px;" data-val="<?php echo $amount ?>">$ <?php echo number_format($amount, 2) ?></td>
                    </tr>

                <?php } ?>

            </table>

            <div class="total" style="maring-top:10px;">
                <div class="title">
                    Total Amount
                </div>
                <div class="grand-total" style=" padding-right: 20px !important;" >
                    <span style="font-weight:bold;" >$ <?php echo number_format( $grand_total, 2 ) ?></span>
                    <input type="hidden" name="amount" value="<?php echo $grand_total ?>" >
                    <input type="hidden" name="order_id" value="<?php echo $order_id ?>" >
                </div>
            </div>

        </div>

        <div class="col-sm-6" style="border-left: 1px solid #DDD">

            <div id="block-confirm" >
                <legend>Payment Confirmation</legend>
                <ol id="form-element">
                    <li class="msg-warning"></li>
                    <li>
                        <label class="required"><?php echo __('Validation Code') ?></label>
                        <input type="text" name="validation_code" id="validation_code" value="" placeholder="Validation Code" required>
                    </li>
                </ol>
            </div>

            <div class="col-sm-12 no-padding clearfix" style="text-align:right; padding-top:10px !important;">
                
                <div class="block-btn">
                    
                    <a href="<?php echo $this->Html->url(array("action"=>"index")) ?>?ref=<?php echo $token ?>">
                        <input type="button"
                                class='btn my-btn my-btn-inactive' 
                                value="Cancel" 
                                style="width:100px; margin-right:10px;">
                    </a>

                    <input type="submit" id='btn-confirm-payment' 
                            class='btn my-btn my-btn-active' 
                            value="Submit" 
                            style="width:100px; margin-right:18px;">
                </div>

                <div class="ajax-loading" style="width: 100%; text-align: center; margin-top:20px; display:none;">
                    <img src="<?php echo $this->webroot . "img/bx_loader.gif" ?>">
                    <br>Checking...
                </div>
            </div>

        </div>

    </form>
</div>

<div class="clearfix" style="padding-bottom:20px;"></div>

<script type="text/javascript">

    

</script>
