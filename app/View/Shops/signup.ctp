<?php 

/********************************************************************************

File Name: index.ctp (Your Cart Shopping)
Description: for business registration form

Powered By: ABi Investment Group Co., Ltd,

Changed History:

    Date                Author              Description
    2015/04/06          Phea Rattana        Initial Version

*********************************************************************************/

$currentYear = date("Y");
$fromYear = $currentYear - 120;
$toYear = $currentYear;

$name   = "";
$gedner = "";
$dob_d  = "";
$dob_m  = "";
$dob_y  = "";

$province   = "";
$city       = "";
$location   = "";
$street     = "";
$phone1     = "";
$phone2     = "";
$email      = "";

if( isset($save) && $save == false ){

    $data = $data['Buyer'];

    $name   = $data['full_name'];
    $gender = $data['gender'];
    $dob_d  = $data['dob']['day'];
    $dob_m  = $data['dob']['month'];
    $dob_y  = $data['dob']['year'];

    $province   = $data['province_code'];
    $city       = $data['city_code'];
    $location   = $data['location'];
    $street     = $data['street'];

    $phone1     = $data['phone1'];
    $phone2     = $data['phone2'];
    $email      = $data['email'];
}

?>

<style type="text/css">
.register-form label{
    width: 140px;
}
.alert-error{
    color: red;
    background: rgb(200, 54, 54); /* The Fallback */
    background: rgba(200, 54, 54, 0.5);
}
.alert-dismissable .close{
    top: -35px;
    right: -30px;
    color: #666;
}
</style>

<div class="register-form clearfix">
    <form id="frmAdd" method="POST" action="<?php echo $this->Html->url(array("action"=>"signup")) ?>" enctype="multipart/form-data">
        <h1 style="text-transform: uppercase;"><?php echo __('Sign Up'); ?></h1>
        
        <div class="col-sm-12 no-padding clearfix" style="min-height: 150px;">

            <div class="col-sm-4 no-padding" style="min-height: 100px;">
                <div class="clearfix" style="padding-top: 20px;"></div>
                <p>
                    Already a member ?  
                    <a href="<?php echo $this->Html->url(array('controller'=>'shops', 'action'=>'signin')) ?>">SIGN IN HERE !</a>
                </p>
            </div>

            <div class="col-sm-8" style="border-left: 1px solid #DDD">
                <legend>Sign Up Information</legend>

                <?php if( ( isset($save) && $save == true ) ){ ?>

                <!-- Afer Sign Up Successfully -->
                    <div class="clearfix" style="padding-top:30px;"></div>
                    <h4>Your account has been registered.</h4>

                    <div class="clearfix" style="padding-top:20px;"></div>

                    <p>Please sign in to email and click on verification link to activate your account.</p>
                

                <?php }elseif($verified == "verified" ){ ?>

                    <div class="clearfix" style="padding-top:30px;"></div>
                    <h4>Your account is now activated.</h4>

                    <div class="clearfix" style="padding-top:20px;"></div>

                    <a href="<?php echo $this->Html->url(array('controller'=>'shops', 'action'=>'signin')) ?>">Sign In here !</a> and enjoy shopping with us.

                <?php }else{ ?>

                <ol>
                    <li>
                        <label class="required"><?php echo __('Name') ?></label>
                        <input type="text" class="input_email" id="fullname" name="Buyer[full_name]" placeholder="<?php echo __('Name') ?>" required value="<?php echo $name; ?>" maxLength="50">
                    </li>

                    <li>
                        <label class="required"><?php echo __('Gender') ?></label>

                        <select name="Buyer[gender]" id="gender">
                            <?php foreach( $genders as $key => $val ){ 
                                    $selected = ($key == $gender )?" selected='selected'":"";
                            ?>
                                <option value="<?php echo $key ?>" <?php echo $selected ?>><?php echo $val ?></option>
                            <?php } ?>
                        </select>

                    </li>

                    <li>
                        <label class="required"><?php echo __('Date of Birth') ?></label>
                        <select style="width:128px;" id="dob-day" name="Buyer[dob][day]" required>
                            <option value=""><?php echo __('Day') ?></option>
                            <?php for ($i=1; $i < 32 ; $i++) { 

                                $selected = ($i == $dob_d )?" selected='selected'":"";
                                echo "<option value='".$i."' " . $selected . ">".$i."</option>";
                            } ?>
                        </select>
                        <select style="width:128px;" id="dob-month" name="Buyer[dob][month]" required>
                            <option value=""><?php echo __('Month') ?></option>
                            <?php foreach( $mons as $key => $value ){
                                $selected = ( $key == $dob_m )?" selected='selected'":"";

                                echo "<option value='".$key."'" . $selected . ">".$value."</option>";
                            } ?>
                        </select>
                        <select style="width:130px;" id="dob-year" name="Buyer[dob][year]" required>
                            <option value=""><?php echo __('Year') ?></option>

                            <?php for ( $i=$fromYear; $i <= $toYear ; $i++ ) { 
                                $selected = ($i == $dob_y )?" selected='selected'":"";
                                echo "<option value='".$i."'" . $selected . ">".$i."</option>";
                            } ?>

                        </select>
                    </li>

                    <li>
                        <label class="required"><?php echo __('Province/ City') ?></label>
                        <select name="Buyer[city_code]" id="city" required>
                            <option value=""><?php echo __('Select province/ city') ?></option>
                            <?php foreach ($provinceList as $proKey => $proValue) {
                                $selected = ( $proValue['City']['city_code'] == $province )?" selected='selected'":"";

                                echo "<option value='".$proValue['City']['city_code']."'" . $selected . ">".$proValue['City']['city_name']."</option>";
                            } ?>
                        </select>
                    </li>

                    <li>
                        <label class="required"><?php echo __('Location/ Khan') ?></label>
                        <select name="Buyer[location]" id="location" required>
                            <option value="" data-id=""><?php echo __('Select Location') ?></option>
                        </select>
                        <input type="hidden" name="Buyer[location_id]" id="location-id">
                    </li>

                    <li>
                        <label><?php echo __('Sangkat') ?></label>
                        <select name="Buyer[sangkat_id]" id="sangkat-select">
                            <option value=""><?php echo __('Select Sangkat') ?></option>
                        </select>
                    </li>
                    <li>
                        <label><?php echo __('Street') ?></label>
                        <input type="text" id="street" name="Buyer[street]" placeholder="<?php echo __('Street') ?>" pattern=".{1,100}" required title="100 characters maximum" maxLength='100' value="<?php echo $street ?>">
                    </li>

                    <li>
                        <label class="required"><?php echo __('Phone 1') ?></label>
                        <input type="text" class="input_phone" id="phone-1" name="Buyer[phone1]" placeholder="<?php echo __('Phone 1') ?>" pattern=".{1,11}" required  maxLength="20" value="<?php echo $phone1 ?>">
                    </li>

                    <li>
                        <label><?php echo __('Phone 2') ?></label>
                        <input type="text" class="input_phone" id="phone-2" name="Buyer[phone2]" placeholder="<?php echo __('Phone 2') ?>" pattern=".{1,11}" maxLength="20" value="<?php echo $phone2 ?>">
                    </li>
                    
                    <?php if(isset($referal)){ ?>
                        <li>
                            <label class="required"><?php echo __('Email') ?></label>
                            
                            <input type="hidden" name="Buyer[referer_id]" value="<?php echo $referal['referer_id'] ?>" >

                            <input type="email" class="input_email" id="email" name="Buyer[email]" placeholder="<?php echo __('Email') ?>" required readonly="readonly" maxLength='50' value="<?php echo $referal['email'] ?>">
                        </li>
                    <?php }else{ ?>
                        <li>
                            <label class="required"><?php echo __('Email') ?></label>
                            <input type="email" class="input_email" id="email" name="Buyer[email]" placeholder="<?php echo __('Email') ?>" required maxLength='50' value="<?php echo $email ?>">
                        </li>
                    <?php } ?>

                    <li>
                        <label class="required"><?php echo __('Password') ?></label>
                        <input type="password" name="Buyer[password]" class="input_password" id="password" placeholder="<?php echo __('Password') ?>" pattern=".{6,30}" required title="Password must contain 6-30 characters" >
                    </li>
                    <li>
                        <label class="required"><?php echo __('Confirm password') ?></label>
                        <input type="password" name="Buyer[confirm_password]" class="input_password" id="confirm-password" placeholder="<?php echo __('Confirm Password') ?>" pattern=".{6,30}" required title="Password must contain 6-30 characters">
                    </li>

                    <li>
                        <label><input type="checkbox" name="agreed" id="agreed" style="width: 20px; margin-top: 9px;" value="1"></label>
                        <span style="float:left; width:70%;">
                            <label for="agreed" style="float:right; width:100%; text-align:left;">I have read and agreed to the <a href="<?php echo $this->Html->url(array('controller'=>'termOfUses', 'action'=>'index')) ?>" target="_blank"><?php echo __('Terms of Use'); ?></a> and 
                    <a href="<?php echo $this->Html->url(array('controller'=>'privacyPolicies', 'action'=>'index')) ?>" target="_blank"><?php echo __('Privacy Policy'); ?></a></label>
                        </span>
                    </li>
                </ol>

                <div class="col-sm-12 no-padding clearfix" style="text-align:center;">
                    <button type="submit" id="btn-sign-up"><?php echo __('SIGN UP') ?></button>
                    <div class="ajax-loading" style="width: 100%; text-align: center; display:none;">
                        <img src="<?php echo $this->webroot . "img/bx_loader.gif" ?>">
                    </div>
                </div>

                <?php } ?>

            </div>

        </div>
        
    </form>
</div>

<script type="text/javascript">

    function msieversion() {

            var ua = window.navigator.userAgent;
            var msie = ua.indexOf("MSIE ");
            var ieversion;

            if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer, return version number
                return parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)));
            else                 // If another browser, return 0
                return false;
    }

    var checkIEBrowser = msieversion();

    var city_code        = "<?php echo $city ?>";
    var location_code    = "<?php echo $location ?>";


   
    $("select#city").change( function (){
        var me = $(this);

        var locationSelect  = $("select#location");
        var sangkatSelect   = $('select#sangkat-select')
        var city_code       = me.val();

        var url = '<?php echo $this->Html->url(array("controller"=>"Locations", "action"=>"get_location_by_city_code")) ?>' + "/"  + city_code ;

        if( city_code != '' ){
            $.ajax({
                type: 'POST',
                url: url,
                data: { city_code : city_code },
                dataType: 'json',
                
                beforeSend: function(){
                    locationSelect.html("<option value=''>Loading...</option>");
                    sangkatSelect.html("<option value=''>Loading...</option>");
                },
                success: function (data){

                    var data = data.data;

                    locationSelect.html("<option value=''>Select location/Khan</option>");
                    sangkatSelect.html("<option value=''>Select Sangkat</option>");
                    
                    $.each( data, function (ind, val ){
                        var loc = val.Location;

                        locationSelect.append("<option value='" + loc.location_name + "' data-id='"+ loc.id +"'>" + loc.location_name + "</option>" )
                    });
                },
                error: function ( err ){
                    console.log(err);
                }

            });

        }else{

            locationSelect.html("<option value=''>select an option</option>");
            sangkatSelect.html("<option value=''>select an option</option>");

        }

    });


    $('select#location').change( function(){

        var me      = $(this);
        
        var optoin_selected = $('select#location option:selected');
        var location_id     = optoin_selected.attr('data-id');
        var sangkatSelect   = $('select#sangkat-select');

        $('input#location-id').val(location_id);

        var url = '<?php echo $this->webroot ?>' + 'sangkats/getSangkatByLocation/' + location_id ;

        if( location_id != '' ){
            $.ajax({
                type: 'POST',
                url: url,
                data: { location_id : location_id },
                dataType: 'json',
                
                beforeSend: function(){
                    sangkatSelect.html('<option>Loading...</option>');
                },
                success: function (result){
                    var data = result.data;

                    console.log(data);

                    sangkatSelect.html("<option value=''>select an option</option>");

                    $.each( data, function (ind, val ){
                      var sk = val.Sangkat;
                      sangkatSelect.append("<option value='" + sk.id + "'>" + sk.name + "</option>" );

                    });

                },

                error: function(xhr, status, error) {
                  var err = eval("(" + xhr.responseText + ")");
                    console.log(err);
                }

            });

        }else{
            sangkatSelect.html("<option value=''>select an option</option>");
        }
    })


    $('form#frmAdd label.required').append("<span class='red-star'> * </span>");

    $("input.input_phone").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) || 
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

    $('button#btn-sign-up').click ( function(){

        event.preventDefault();
         
        var msg = new Array();

        var btn         = $(this),
            loading     = $('div.ajax-loading');

        btn.hide();
        loading.show();

        var name        = $('input#fullname').val().trim(),
            dob_day     = $('select#dob-day').val(),
            dob_month   = $('select#dob-month').val(),
            dob_year    = $('select#dob-year').val(),
            province    = $('select#province').val(),
            location    = $('select#location').val(),
            phone1      = $('input#phone-1').val().trim(),
            email       = $('input#email').val().trim(),
            password    = $('input#password').val(),
            cnf_password= $('input#confirm-password').val(),
            agreed      = $('input#agreed');

        if( name == "" ){ 
            msg.push('Please enter name !'); 
        }

        if( dob_day == "" || dob_month == "" || dob_year == "" ){  
            msg.push('Please select date of birth !') 
        }

        if( province == "" ) {
            msg.push('Please select province !');
        }

        if( location == ""){
            msg.push('Please select location !');
        }

        if( phone1 == ''){
            msg.push('Please enter phone number !');
        }

        if( email == ""){
            msg.push('Please enter email address !');
        }else{
            if( !validateEmail(email) ){
                msg.push('Invalid Email Adress !');  
            }
        }

        if( password == ""){
            msg.push('Please enter password!');
        }else if( password.length < 6 ){
            msg.push('Password must be 6 characters at least !');
        }

        if( cnf_password == "" ){
            msg.push('Please enter confirm password !');
        }

        if( password != "" && cnf_password != "" && password != cnf_password ){
            msg.push('Password & Confirm password does not match !');
        }

        if( msg.length > 0 ){
            var msg_error = "";
            $.each( msg, function(ind,val){
                msg_error += "<br>" + val;
            })

            loading.hide();
            btn.show();

            $.msgBox({
                title: "Failed",
                content: msg_error,
                type: "error",
                buttons: [{ value: "Ok" }],
            });

        }else{

            if( agreed.is(':checked') ){
                
                var url = '<?php echo $this->Html->url(array("controller"=>"shops", "action"=>"checkDuplicateEmail")) ?>' + "/"  + email ;

                $.ajax({
                    type: 'POST',
                    url: url,
                    data: { email : email },
                    dataType: 'json',

                    success: function( data ){
                        if( data.status == true ){
                            $('form#frmAdd').submit();
                        }else{

                            loading.hide();
                            btn.show();

                            $.msgBox({
                                title: "Failed",
                                content: "This email is already registered. Please try with another email address !",
                                type: "error",
                                buttons: [{ value: "Ok" }],
                            }); 
                        }
                    },

                    error: function(err, message ){
                        console.log(message);
                    }
                });

            }else{

                loading.hide();
                btn.show();

                $.msgBox({
                    title: "Failed",
                    content: "Please agree with Terms of Use and Privacy Policy !",
                    type: "error",
                    buttons: [{ value: "Ok" }],
                });

                return false;
            }

        }
        

        return false;
    })

    function validateEmail(email) {
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        return emailReg.test( email );
    }

</script>
