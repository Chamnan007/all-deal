<?php 

/********************************************************************************

File Name: index.ctp (Your Cart Shopping)
Description: for business registration form

Powered By: ABi Investment Group Co., Ltd,

Changed History:

    Date                Author              Description
    2015/04/06          Phea Rattana        Initial Version

*********************************************************************************/
?>

<div class="register-form clearfix">
    <h1 style="text-transform: uppercase;"><?php echo __('Purchase Information'); ?></h1>
    
    <div class="col-sm-12 no-padding clearfix" style="min-height: 150px;">
       
    	<?php echo $this->Session->flash(); ?>
    	
    </div>        
</div>

<script type="text/javascript">

</script>
