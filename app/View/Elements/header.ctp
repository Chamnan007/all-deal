<?php 

/********************************************************************************

File Name: header.ctp
Description: header file

Powered By: ABi Investment Group Co., Ltd,

Changed History:

    Date                Author              Description
    2014/06/10          Sim Chhayrambo      Initial Version
    2014/06/24          Sim Chhayrambo      Add Subcribe Form
    2014/06/30          Sim Chhayrambo      Add maximum characters to search box
    2014/07/09          Sim Chhayrambo      Check empty string
    2014/07/12          Sim Chhayrambo      Check Menu Deal
    2015/July/08        Rattana Phea        Change Menu and Deal order Listing

*********************************************************************************/

if(!empty($menuBusinessInfo)){
    $businessName = $menuBusinessInfo['Business']['business_name'];
    $businessLogo = $menuBusinessInfo['Business']['logo'];
    $businessSlug = $menuBusinessInfo['Business']['slug'];    
}

$cart = false;
$signin = false;
$signup = false;

if( $title_for_layout == "Shops" ){
    
    if( $this->params['action'] == 'index' ){
        $cart = true; 
    }elseif( $this->params['action'] == 'signin' ){
        $signin = true;
    }elseif( $this->params['action'] == 'signup' ){
        $signup = true;
    }

}

?>

<!--
  Below we include the Login Button social plugin. This button uses
  the JavaScript SDK to present a graphical Login button that triggers
  the FB.login() function when clicked.
-->

<!-- <fb:login-button scope="public_profile,email" onlogin="checkLoginState();">Subscribe with facebook
</fb:login-button>

<div id="status">
</div> -->

<!-- <div class="fb-login-button" data-max-rows="1" data-size="medium" data-show-faces="false" data-auto-logout-link="false">Login with facebook</div> -->


<div style="width: 100%; background: #222;">
    <div class="container custom-header" style="background: #222;">
        <div class="row">
            <header id="header" class="clearfix" style="position: relative;">
                <div class="col-sm-3 no-padding">
                    <a href="<?php echo $this->Html->url(array('controller'=>'index', 'action'=>'index')); ?>">
                        <h1 style="width: 100px; padding: 20px 0px 10px 0px; margin-left:10px;">
                            <img src="<?php echo $this->webroot . "img/logo/AD_LOGO.png"?>" width="100%">
                        </h1>
                    </a>
                </div>
                
                <div class="col-sm-9" style="margin-top: 34px;">
                    <form method="get" id="search" action="<?php echo $this->Html->url(array('controller'=>'search', 'action'=>'index')) ?>">
                        <select name="location[]" class="select-city" style="float: left; margin-right: 10px; width: 150px;">
                            <option value="all"><?php echo __('All Locations') ?></option>
                            <?php 
                            foreach ($locations as $key => $value) { ?>
                            <option value="<?php echo $value['Location']['slug'] ?>"><?php echo $value['Location']['location_name'] ?></option>
                            <?php } ?>
                        </select>

                        <input name="search-text" type="text" size="40" maxlength="100" placeholder="Search Deals Here" style="float: left;" />
                        
                        <a href="<?php echo $this->Html->url(array('controller'=>'businesses', 'action'=>'welcome')) ?>" class="pull-right merchant-btn subscribe-button"><?php echo __('Merchants'); ?></a>

                        <?php if( !$__BUYER_LOGGED_SESSION ){ ?>
                            <a href="<?php echo $this->Html->url(array('controller'=>'shops', 'action'=>'signup')) ?>" class="pull-right header-button" style="margin-right:4px;"><?php echo __('SING UP'); ?></a>
                            <a href="<?php echo $this->Html->url(array('controller'=>'shops', 'action'=>'signin')) ?>" class="pull-right header-button" style="margin-right:4px;"><?php echo __('SIGN IN'); ?></a>
                        <?php }else{ ?>
                            <a href="<?php echo $this->Html->url(array('controller'=>'shops', 'action'=>'signout')) ?>" class="pull-right header-button" style="margin-right:4px;"><?php echo __('SIGN OUT'); ?></a>
                        <?php } ?>

                    </form>

                   <!--  <div class="merchant-popup">
                        
                        <ul>
                            <a href="<?php echo $this->Html->url(array('controller'=>'MemberUsers', 'action'=>'login','plugin'=>'member')) ?>"><li>Sign In</li></a>
                            <a href="<?php echo $this->Html->url(array('controller' => 'businesses', 'action' => 'register')) ?>"><li>Register</li></a>
                        </ul>
                        
                    </div> -->
                </div>

                <div id="cart">

                    <?php if( $__BUYER_LOGGED_SESSION ){ 

                        $info = $__BUYER_LOGGED_SESSION['__SES_LOGGED_INFO'];

                        $user_name = strtoupper($info['Buyer']['full_name']);
                    ?>
		                <div id="cart-count" style="display:none"><?php echo $cart_count ?></div> 
		                <a class="<?php echo($cart)?'active':'' ?>" href="<?php echo $this->Html->url(array('controller'=>'shops', 'action'=>'index')) ?>" title="Your Cart">
		                    <!-- <div id="cart-count">1</div> -->
		                    <div id="cart-icon"></div>
		                    cart
		                </a>

                        <div id="separator">|</div>
                        <a class="<?php echo( !$cart && !$signin && !$signup )?'active':'' ?>" id="user-name-menu"  href="#" title="<?php echo $user_name ?>">
                            <div id="down-icon"></div>
                            <?php echo $user_name ?>
                        </a>

                        <div id="user-drop-menu">
                            <ul>
                                <li title="My Account">
                                    <a href="<?php echo $this->Html->url(array('controller'=>'shops', 'action'=>'myaccount')) ?>">My Account</a>
                                </li>
                                <li title="My Profile">
                                    <a href="<?php echo $this->Html->url(array('controller'=>'shops', 'action'=>'myprofile')) ?>">My Profile</a>
                                </li>
                                <li title="Refer Friends">
                                    <a href="<?php echo $this->Html->url(array('controller'=>'shops', 'action'=>'refer')) ?>">Refer Friends</a>
                                </li>
                                <li title="Sign Out">
                                    <a href="<?php echo $this->Html->url(array('controller'=>'shops', 'action'=>'signout')) ?>">Sign Out</a>
                                </li>
                            </ul>
                        </div>

                    <?php } ?>
                    
                </div>

            </header>
        </div>
    </div>
</div>

<div style="width: 100%; background: #8dc63f ;">
    <div id="nav-wrapper" class="clearfix">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 no-padding">
                    <ul id="menu">
                        <li>
                            <a href="<?php echo $this->Html->url(array('controller'=>'index', 'action'=>'index')) ?>"><?php echo __('Home') ?></a>
                        </li>

                        <li>
                            <a href="<?php echo $this->Html->url(array('controller' => 'deals', 'action' => 'index')) ?>?type=deal-of-the-day"><?php echo __('Deal of the Day') ?></a>
                        </li>

                        <li>
                            <a href="<?php echo $this->Html->url(array('controller'=>'deals', 'action' => 'index')) ?>?type=last-minute-deal"><?php echo __('Last Minute Deal') ?></a>
                        </li>

                        <li>
                            <a href="<?php echo $this->Html->url(array('controller'=>'deals', 'action' => 'index'))?>?type=food-and-drink"><?php echo __('Food & Drink') ?></a>
                        </li>

                        <li><a href="#" class="drop"><?php echo __('Location') ?></a><!-- Begin 4 columns Item -->
                            <div class="dropdown_1column"><!-- Begin 4 columns container -->
                                <div class="col_1">
                                    <ul>
                                        <li>
                                            <a href="<?php echo $this->Html->url(array('controller'=>'deals', 'action' => 'index')) ?>?location=all">
                                                <span class="title"><?php echo __('All') ?></span>
                                                <!-- <span class="count">16</span> -->
                                            </a>
                                        </li>
                                        <?php 
                                        foreach ($locations as $key => $val) { ?>
                                        <li>
                                            <a href="<?php echo $this->Html->url(array('controller'=>'deals')) . '?location=' . $val['Location']['slug'] ?>">
                                                <span class="title"><?php echo $val['Location']['location_name'] ?></span>
                                                <!-- <span class="count">16</span> -->
                                            </a>
                                        </li>
                                        <?php } ?>
                                    </ul>  
                                </div>
                            </div><!-- End 4 columns container -->
                        </li><!-- End 4 columns Item -->

                        <!-- BUSINESS -->
                        <li><a href="<?php echo $this->Html->url(array('controller'=>'businesses', 'action'=>'index')) ?>" class="drop"><?php echo __('Merchants') ?></a>
                            <div class="dropdown_2columns">
                                <div class="col_1">
                                    <ul class="nav-menu">
                                        <?php 
                                        foreach ($categories as $catKey => $catValue) { ?>
                                        <li>
                                            <a href="<?php echo $this->Html->url(array('controller'=>'businesses', 'action' => 'index')) ?>?category=<?php echo $catValue['BusinessCategory']['slug'] ?>">
                                                <span class="title"><?php echo $catValue['BusinessCategory']['category'] ?></span>
                                                <!-- <span class="count">12</span> -->
                                            </a>
                                        </li>
                                        <?php } ?>
                                    </ul>  
                                </div>
                                <?php 
                                if(!empty($menuBusinessInfo)){
                                ?>
                                <div class="col_1 menu-align-center">
                                    <ul>
                                        <li>
                                            <a href="<?php echo $this->Html->url(array('controller'=>'businesses', 'action'=>'detail', $businessSlug)) ?>" 
                                                class="image-box" 
                                                style="height: 200px; background: #fff;">
                                                <img src="<?php echo $this->webroot . $businessLogo ?>" width="200" height="200">
                                            </a>
                                            <i><?php echo $businessName ?></i>
                                        </li>
                                    </ul> 
                                </div>
                                <?php } ?>
                            </div>
                        </li>
                        <!-- END BUSINESS -->
                    </ul>
                </div>
            </div>
        </div>
    </div> 
</div>