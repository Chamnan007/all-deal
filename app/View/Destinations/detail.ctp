<?php 

/********************************************************************************

File Name: detail.ctp
Description: displaying detail for destinations

Powered By: ABi Investment Group Co., Ltd,

Changed History:

    Date                Author              Description
    2014/06/10          Sim Chhayrambo      Initial Version

*********************************************************************************/



$businessDeal = $businessInfo['Deals'];

$opHours = $businessInfo['OperationHour'];

// var_dump($businessInfo);
// exit();

$businessInfo = $businessInfo['Business'];
$businessName = $businessInfo['business_name'];
$businessStreet = $businessInfo['street'];
$businessLocation = $businessInfo['location'];
$businessEmail = $businessInfo['email'];
$businessPhone1 = $businessInfo['phone1'];
$businessPhone2 = $businessInfo['phone2'];
$businessLogo = $businessInfo['logo'];
$businessDescription = $businessInfo['description'];
$businessLat = $businessInfo['latitude'];
$businessLong = $businessInfo['longitude'];



?>
<div class="container">
    <!-- <div class="col-sm-12 no-padding" style="margin: 10px 0;">
        <a href="">
            <img src="<?php echo $this->webroot . "img/07.jpg"?>" width="940" height="100">
        </a>
    </div> -->
    <div style="height: 20px;"></div>
    <div class="col-sm-12 no-padding clearfix">
        <!-- <div class="col-sm-4 no-padding-left">
            <div class="special-widget">
                <div class="swidget-body">
                    <ul>
                        <li><img src="<?php echo $this->webroot . $businessLogo ?>" width="220"></li>
                        <li>
                            <a href="#"><i class="icon-facebook"></i></a>
                            <a href="#"><i class="icon-twitter"></i></a>
                            <a href="#"><i class="icon-linkedin"></i></a>
                            <a href="#"><i class="icon-behance"></i></a>
                            <a href="#"><i class="icon-dribbble"></i></a>
                            <a href="#"><i class="icon-skype"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div> -->
        <div class="col-sm-12 no-padding">
            <article id="article">
                <div class="article-entry">
                    <img src="<?php echo $this->webroot . $businessLogo ?>" width="220">
                    <h2><?php echo $businessName; ?></h2>
                    <?php echo $businessDescription ?>
                    <div class="col-sm-6 no-padding">
                        <h2>Contact Info</h2>
                        <p><?php echo "Address: " . $businessStreet . ", " . $businessLocation ?></p>
                        <p><?php echo "Phone: " . $businessPhone1 . " / " . $businessPhone2 ?></p>
                        <p><?php echo "Email: " . $businessEmail ?></p>
                        <h2>Operating Hour</h2>
                        <?php 
                        foreach ($opHours as $opKey => $opValue) { 
                            if(!empty($opValue['from']) && !empty($opValue['to'])){
                                $opDay = $opValue['day'];
                                $opDay = substr($opDay, 0, 3);
                                $opFrom = date("h:i A", strtotime($opValue['from']));
                                $opTo = date("h:i A", strtotime($opValue['to']));
                                echo "<p>";
                                echo "<span style='display:inline-block; width:48px; color: orange;'>" . strtoupper($opDay) . " : " . "</span>";
                                echo "<span>" . $opFrom . " - " . $opTo . "</span>";
                                echo "</p>";
                            }
                        } ?>
                    </div>
                    <div class="col-sm-6 no-padding"></div>
                    <div class="col-sm-12" style="width:100%; height:250px; margin:0 auto;" id="map-canvas"></div>
                </div>
            </article>
        </div>
    </div>
    <div class="col-sm-12 no-padding clearfix">
        <div class="col-sm-12 no-padding">
            <?php 
            if(!empty($businessMedia)){ ?>
            <div class="col-sm-12 no-padding">
                <article id="article">
                    <div class="article-entry">
                        <div class="recent-works">
                            <div class="recent-works-header clearfix">
                                <h2>Store Images</h2>
                                <!-- <div class="control-box clearfix">
                                    <a data-slide="next" href="#Recentworks" class="client-carousel-control"><i class="icon-angle-right"></i></a>
                                    <a data-slide="prev" href="#Recentworks" class="client-carousel-control"><i class="icon-angle-left"></i></a>
                                </div> -->
                            </div>
                            <div class="recent-works-carousel">
                                <div class="carousel slide" id="Recentworks">
                                    <div class="carousel-inner">
                                        <div class="item active">
                                            <ul class="thumbnails">
                                                <div class="row">
                                                    <?php
                                                    foreach($businessMedia as $bKey => $bValue) { 
                                                        $strLeft = "";
                                                        if($bKey == 0) $strLeft = "style='left:70%;'";
                                                        $media_path = $bValue['BusinessesMedia']['media_path'];
                                                        if(!empty($media_path)){
                                                    ?>
                                                    <li class="col-sm-2 col-xs-2">
                                                        <div class="img-overlay">
                                                            <img src="<?php echo $this->webroot . $media_path ?>" width="100%" height="124">
                                                            <div class="recent-icons" <?php echo $strLeft; ?>>
                                                                <a href="<?php echo $this->webroot . $media_path ?>" class="fancybox"><i class="icon-search"></i></a>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <?php } } ?>
                                                </div>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
            <div class="col-sm-12 clearfix" style="border: 1px solid #f2f2f2; padding: 10px 0; margin-bottom: 20px;">
                <h2 style="margin: 10px 15px; font-weight: bold;">Store Videos</h2>
                <?php 
                foreach ($businessMedia as $buKey => $buValue) { 
                    $media_embeded = $buValue['BusinessesMedia']['media_embeded'];
                    if(!empty($media_embeded)){
                ?>
                    <div class="col-sm-6">
                        <!-- <iframe 
                            width="100%" height="220"
                            src="http://www.youtube.com/embed/<?php echo $media_embeded ?>" allowfullscreen>
                        </iframe> -->
                        <!-- <a 
                            href="http://www.youtube.com/embed/Pj9ubmzF3iE" 
                            class="html5lightbox" 
                            data-width="960" 
                            data-height="480">
                            <img src="<?php echo $this->webroot . "img/avatar.jpg"?>" width="100%" height="220">
                        </a> -->
                        <?php echo $media_embeded ?>
                    </div>
                <?php } } ?>
            </div>
            <?php } ?>
        </div>
    </div>
    <?php 
    if (!empty($businessDeal)) { ?>
    <div class="col-sm-12 no-padding clearfix">
        <div class="container no-padding-left">
            <div class="recent-works col-sm-12 no-padding-left">
                <div class="recent-works-header clearfix">
                    <h2 style="width: 100%;">
                        <span style="float: left;">Business Deals</span>
                        <!-- <span>
                            <a href="#" class="button btn-green pull-right"><i class="icon-plus"></i> View All</a>
                        </span> -->
                    </h2>
                </div>
                <div class="recent-works-carousel">
                    <div class="carousel slide" id="Recentworks">
                        <div class="carousel-inner">
                            <div class="item active">
                                <ul class="thumbnails">
                                    <div class="row">
                                        <?php 
                                        foreach($businessDeal as $busKey => $busValue) { 
                                            $split = end(explode("/", $busValue['image']));
                                            $thumb_img =  "img/deals/thumbs/" . $split ;
                                        ?>
                                            <li class="col-sm-3 col-xs-3">
                                                <div class="new-project bg-shadow add-margin-bottom">
                                                    <div>
                                                        <a href="<?php echo $this->Html->url(array('controller'=>'deals', 'action'=>'detail', $busValue['slug'])) ?>">
                                                            <img src=" <?php echo $this->webroot . $thumb_img ?>" width="220" height="134" title="<?php echo $busValue['title'] ?>">
                                                        </a>
                                                    </div>
                                                    <div id="bx-pager">
                                                        <a data-slide-index="0" href="#" class="active">
                                                            <div class="new-project-body">
                                                                <h4 class="add-min-height"><?php echo $busValue['title'] ?></h4>
                                                                <h4 class="price-off"><?php echo $busValue['discount_category'] ?></h4>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </li>
                                        <?php } ?>
                                    </div>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
        </div>
    </div>
    <?php } ?>
</div>


<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
<script src="https://google-maps-utility-library-v3.googlecode.com/svn/tags/markerwithlabel/1.1.9/src/markerwithlabel.js" type="text/javascript"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

<script>

    function initialize() {

      var  latitude  = <?php echo $businessLat ?>,
           longitude = <?php echo $businessLong ?>,
           myLatlng  = new google.maps.LatLng( latitude, longitude );

      var mapOptions = {
        zoom: 12,
        center: myLatlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      }

      var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    }

    google.maps.event.addDomListener(window, 'load', initialize);

</script>