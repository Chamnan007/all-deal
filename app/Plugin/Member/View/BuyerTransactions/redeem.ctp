<?php 

	/********************************************************************************
	    File: Buyers Listing
	    Author: PHEA RATTANA
	
	    Confidential ABi Technologies property.
	
	  	Changed History:
	  	Date 					Author				Description
	  	2015/01/May 			PHEA RATTANA		Initial
	*********************************************************************************/

	$status = $data['BuyerTransaction']['status'];

 ?>
<style>
  
  table, tr, td{
     vertical-align: top; 
  }

</style>

<div class="users index">
	<div class="row-fluid">		
		<!-- Pie: Box -->
		<div class="span12">
			
			<form action="" method="POST" id="frmPurchaseTransaction" >

				<!-- Pie: Top Bar -->
				<div class="top-bar">
					<h3><i class="icon-list"></i> Redeem Transaction Purchase</h3>
				</div>
				<!-- / Pie: Top Bar -->

				<!-- Pie: Content -->
				<div class="well" style="min-height:300px; color: #333">

					<!-- <a href="<?php echo $this->Html->url(array('action' => 'redeem')); ?>" class="btn btn-linkedin">Purchase Transaction List</a>
					
					<div class="clearfix"></div> -->

					<fieldset>

						<div style="display:block; clear:both">
							<label for="BuyerTransactionCode" style="float:left; width:100px; padding-top:5px;">Coupon Code</label>
							<input 	name="coupon-code" 
									placeholder="Coupon Code" 
									style="float:left; width:150px;" 
									id="BuyerTransactionCode" 
									required="required"
									autofocus
									value="<?php echo strtoupper(@$code) ?>"
									maxlength="15" type="text" >

							<button class="btn btn-linkedin"
									style="margin-left:20px;">Get Coupon</button>
						</div>

					</fieldset>

					<div class="clearfix"></div>

					<?php if( $data){ ?>

						<?php if( $status == 0 ){ ?>
							<a href="#" class="btn btn-skype pull-right" id="approve-redeem">Approve Redeem</a>
						<?php } ?>

						<div class="span6" style="margin-left:0px;">
							<legend>Purchase Information</legend>

							<table width="100%">
								<tr>
									<td width="120px;">Purchased Date</td>
									<td width="20px">:</td>
									<td>
										<?php 
											echo date('d-F-Y', strtotime($data['BuyerTransaction']['created']));
											echo " at " . date('h:i:s A', strtotime($data['BuyerTransaction']['created']));
										?>
									</td>
								</tr>

								<tr>
									<td>Payment Type</td>
									<td>:</td>
									<td><?php echo $payment_method[$data['BuyerTransaction']['payment_type']] ?></td>
								</tr>

								<?php if( $data['BuyerTransaction']['payment_type'] == $__CREDIT_TYPE ){ ?>

									<tr>
										<td>Card Number (Last 4 numbers)</td>
										<td>:</td>
										<td><?php echo str_pad($data['BuyerTransaction']['credit_card_number'], 16, "*", STR_PAD_LEFT  ) ?></td>
									</tr>

								<?php } ?>

								<tr>
									<td>Purchased Amount</td>
									<td>:</td>
									<td style="color:blue; font-weight:bold; font-size: 15px;">
										<?php echo "$" . $this->MyHtml->formatNumber($data['BuyerTransaction']['amount']) ?></td>
								</tr>

								<tr>
									<td style="padding-top:10px;"></td>
								</tr>

							</table>

						</div>	
					<?php if( $status == 0 ){ ?>
						<div class="span6">
							<legend>Buyer Information</legend>

							<?php 
								$buyerInfo = $data['BuyerInfo'];

								$tel = $buyerInfo['phone1'];
								if( $buyerInfo['phone2']){
									$tel .= " / " . $buyerInfo['phone2'];
								}
							?>

							<table width="100%">
								<tr>
									<td width="120px;">Name</td>
									<td width="20px;">:</td>
									<td><?php echo ucwords($buyerInfo['full_name']) ?></td>
								</tr>
								<tr>
									<td>Phone</td>
									<td>:</td>
									<td><?php echo $tel ?></td>
								</tr>

								<tr>
									<td>Email</td>
									<td>:</td>
									<td><?php echo $buyerInfo['email'] ?></td>
								</tr>
								
								<tr>
									<td>Address</td>
									<td>:</td>
									<td>
										<?php 
											echo ($buyerInfo['street'])?$buyerInfo['street'] . ", ":"";
											echo $buyerInfo['location'] . ", " . $buyerInfo['city_code'];
										 ?>
									</td>
								</tr>

							</table>
						</div>
					<?php } ?>	

					<?php 
						$dealInfo = $data['DealInfo'];
					 ?>
						<div class="clearfix"></div>
						<div class="span6" style="margin-left:0px;">
							<legend>Deal Information</legend>

							<table width="100%">
								<tr>
									<td width="120px;">Deal Code</td>
									<td width="20px;">:</td>
									<td>
										<a 	href="<?php echo $this->Html->url(array('action' => 'view', 'controller' => 'deals', $dealInfo['id'] )) ?>"
											target="_blank" >
											<strong><?php echo $dealInfo['deal_code'] ?></strong>
										</a>
									</td>
								</tr>	
								<tr>
									<td>Deal Title</td>
									<td>:</td>
									<td><?php echo $dealInfo['title'] ?></td>
								</tr>
								<tr>
									<td>Description</td>
									<td>:</td>
									<td style="vertical-align:top !important;"><?php echo $dealInfo['description'] ?></td>
								</tr>
								<tr>
									<td>Term & Conditions</td>
									<td>:</td>
									<td style="vertical-align:top !important;"><?php echo $dealInfo['deal_condition'] ?></td>
								</tr>
							</table>

						</div>

						<div class="span6">

							<legend>Purchase Status</legend>
							
							<div class="span12" style="margin-left:0px;">
								<table width="100%">

									<tr>
										<td width="120px;">Status</td>
										<td width="20px;">:</td>
										
										<td>
											<span class="label label-<?php echo $purchase_status[$status]['color'] ?>">
											<?php echo h($purchase_status[$status]['status']); ?></span>
										</td>

									</tr>

									<?php if($status == 1){ ?>
										<tr>
											<td>Note</td>
											<td>:</td>
											<td><?php echo $data['BuyerTransaction']['received_note'] ?></td>
										</tr>

										<tr>
											<td width="120px;">Mark Received By</td>
											<td width="20px;">:</td>										
											<td>
												<?php 
													echo ($data['MarkReceivedBy'])?$data['MarkReceivedBy']['last_name'] . " " . $data['MarkReceivedBy']['first_name']:"";
												?>
											</td>

										</tr>

										<tr>
											<td>Date</td>
											<td>:</td>
											<td>
												<?php 
													echo date('d-F-Y', strtotime($data['BuyerTransaction']['received_date'])); 
													echo " at " . date('h:i:s A', strtotime($data['BuyerTransaction']['received_date']));
												?>
											</td>
										</tr>

									<?php } ?>
								</table>
							</div>

						</div>

						<legend>Purchase Items</legend>

						<?php 
							$purchaseDetail = $data['TransactionDetail'];
						 ?>
							
							<table class="table-list">
								<thead>
									<th width="30px">N<sup>o</sup></th>
									<th width="80px;">Image</th>
									<th>Name</th>
									<th width="120px;">Unit Price</th>
									<th width="120px;">Qty</th>
									<th width="120px;">Amount</th>
								</thead>

								<tbody>
									<?php foreach( $purchaseDetail as $k => $item ){ 

											$full_img = $this->webroot . $item['ItemDetail']['image'] ;

											$img  = $this->webroot . str_replace('/menus/', '/menus/thumbs/', $item['ItemDetail']['image']);
									?>	
										<tr>
											<td style="text-align:center"><?php echo $k + 1 ?></td>
											<td>
												<a href="<?php echo $full_img ?>"  rel="shadowbox">
													<img src="<?php echo $img ?>" style="width:80px; max-width:80px;">
												</a>
											</td>
											<td><?php echo $item['ItemDetail']['title'] ?></td>
											<td style="text-align:right">
												<?php echo "$" . $this->MyHtml->formatNumber($item['unit_price']) ?></td>
											<td style="text-align:center">
												<?php echo $item['qty'] ?></td>
											<td style="text-align:right">$<?php echo $this->MyHtml->formatNumber($item['amount']) ?></td>
										</tr>
									<?php } ?>
								</tbody>

							</table>

					<?php }else if($search) { ?>

						<h5><i>No Coupon Found !</i></h5>

					<?php } ?>

				</div>

			</form>

		</div>
		<!-- / Pie -->
		
	</div>

</div>

<div id="mark-received-modal" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true"
  style="padding-bottom: 30px !important; ">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel"><i class="icon-edit"></i> Mark Redeem</h3>
  </div>

  <!-- Any Info-customization -->
  <form action="<?php echo $this->Html->url(array('action' => 'markRedeem')); ?>" 
      style="padding: 20px;" method="POST" id="markCompleteForm"
      enctype = "multipart/form-data" >

    <div style="width:100%;">	
		<input type="hidden" name="data[BuyerTransaction][id]" value="<?php echo $data['BuyerTransaction']['id'] ?>" >

      	<label><b>Redeem Note</b></label>
      	<textarea name="data[BuyerTransaction][received_note]" 
      			  style="width:96%" placeholder="Redeem Note"
      			  maxLength="300" required="required"></textarea>

    </div>

    <div class="submit" style="margin-top: 20px; clear: both;">
      <input class="btn btn-primary" type="submit" value="Submit">
      <button class="btn btn-google cancel" id="note" data-dismiss='modal' type="button">Cancel</button>
    </div>
      
  </form>

</div> 

<script>
	
	$(document).ready( function(){

    	Shadowbox.init();

		$('#approve-redeem').click( function(event){

			event.preventDefault();

			var me = $(this);

			var modal = $('div#mark-received-modal');

			modal.modal('show');

		});

	});

</script>