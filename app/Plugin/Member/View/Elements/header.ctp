<?php 

  /********************************************************************************
      File: Admin Template Header
      Author: PHEA RATTANA
  
      Confidential ABi Technologies property.
  
      Changed History:
      Date              Author          Description
      2014/05/01        PHEA RATTANA    Initial
      2014/06/26        RATTANA         Checking Permission
  *********************************************************************************/
  

  $controller = $this->params['controller'];


  $action   = ($this->params['action'] != 'index' )?$this->params['action']:NULL ;

  $c1 = $controller;

  if( $controller == "business_categories"){
    $controller = "businesses";
    $action = "Category";
  }

  $controller = str_replace("_", " ", $controller);

  $breadcrumb = $controller;

  if( $breadcrumb == "MemberUsers" ){
    $breadcrumb = "Members";
  } 

  if( $breadcrumb == "BuyerTransactions" ){
    $breadcrumb = "Purchase Transactions";
  }

  if( $breadcrumb == "BusinessRevenues" ){
    $breadcrumb = "Finance";
  }

  if( !$action ){
    $action = "index";
  }

  $user = CakeSession::read("Auth.MemberUser");

?>

<div id="settings" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel"><i class="icon-cog"></i> Account settings</h3>
  </div>

  <!-- Any Info-customization -->
  <form action="">
    

  </form>

</div> 

<div id="content" class="container" style="margin: 0 auto; width: 1170px; min-height:400px;">

  <div class="navbar navbar-inverse navbar-fixed-top">
    <!-- Top Fixed Bar: Navbar Inner -->
    <div class="navbar-inner">

      <!-- Top Fixed Bar: Container -->
      <div class="container">

        <!-- Mobile Menu Button -->
        <a href="#">
          <button type="button" class="btn btn-navbar mobile-menu">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </a>
        <!-- / Mobile Menu Button -->

        <!-- / Logo / Brand Name -->
        <a class="brand" href="<?php echo $this->Html->url(array('controller'=>'dashboards', 'plugin'=> 'member')) ?>" style="font-size:20px; padding:5px; margin-left:20px;"><img src="<?php echo Router::url('/img/logo/admin_logo.png', true); ?>" alt="" style="width:70px;"></a>
        <!-- / Logo / Brand Name -->

        <!-- User Navigation -->
        <ul class="nav pull-right">

          <!-- User Navigation: User -->
          <li class="dropdown">

            <!-- User Navigation: User Link -->
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="icon-user icon-white"></i> 
              <span class="hidden-phone"><?php echo urldecode($user['last_name'] . " " . urldecode($user['first_name'])) ?></span>
            </a>
            <!-- / User Navigation: User Link -->

            <!-- User Navigation: User Dropdown -->
            <ul class="dropdown-menu">
              <!-- <li><a href="#"><i class="icon-user"></i> Profile</a></li>
              <li><a href="#settings" data-toggle="modal"><i class="icon-cog"></i> Settings</a></li> -->
              <li><a href="<?php echo $this->Html->url( array('controller' => 'MemberUsers', 'action' => 'profile')); ?>"><i class="icon-user"></i> My Profile</a></li>
              <li class="divider"></li>
              <li><a href="<?php echo $this->Html->url( array('controller' => 'MemberUsers', 'action' => 'logout')); ?>"><i class="icon-off"></i> Logout</a></li>
            </ul>
            <!-- / User Navigation: User Dropdown -->

          </li>
          <!-- / User Navigation: User -->

        </ul>
        <!-- / User Navigation -->

      </div>
      <!-- / Top Fixed Bar: Container -->

    </div>
    <!-- / Top Fixed Bar: Navbar Inner -->

    <!-- Top Fixed Bar: Breadcrumb -->
    <div class="breadcrumb clearfix">

      <!-- Top Fixed Bar: Breadcrumb Container -->
      <div class="container">

        <!-- Top Fixed Bar: Breadcrumb Location -->
        <ul class="pull-left">
          <li>
            <a href="<?php echo $this->Html->url(array('plugin'=>'member', 'controller'=>'dashboards')); ?>"><i class="icon-home"></i> Home</a> 
            <span class="divider">/</span>
          </li>
          <li class="<?php echo(!$action)?'active':'' ?>">
            <a href="<?php echo $this->Html->url(array('plugin'=>'member', 'controller'=>$c1, 'action'=>'index')); ?>"><i class="icon-list-alt"></i> <?php echo ucwords($breadcrumb) ?></a>
            <?php echo($action)?"<span class='divider'>/</span>":"" ?>
          </li>
          <?php if($action){ ?>
          <li class="active">
            <a href="#"><i class="icon-list-alt"></i> <?php echo ucwords($action) ?></a>
          </li>
          <?php } ?>

        </ul>
        <!-- / Top Fixed Bar: Breadcrumb Location -->

      </div>
      <!-- / Top Fixed Bar: Breadcrumb Container -->

    </div>
    <!-- / Top Fixed Bar: Breadcrumb -->

  </div>
  <!-- Main Navigation: Box -->

<div class="navbar navbar-inverse" id="nav">

  <!-- Main Navigation: Inner -->
  <div class="navbar-inner">

    <!-- Main Navigation: Nav -->
    <ul class="nav">

      <!-- Main Navigation: Dashboard -->
      <li class="<?php echo($controller=='dashboards')?'active':'' ?>">
        <a href="<?php echo $this->Html->url(array('controller'=>'dashboards', 'action'=>'index', 'plugin'=> 'member')); ?>"><i class="icon-align-justify"></i> Merchant Information</a></li>
      <!-- / Main Navigation: Dashboard -->

      <!-- Main Navigation: General -->
      <li class="<?php echo($controller=='deals')?'active':'' ?>">
        <a href="<?php echo $this->Html->url(array('controller'=>'deals', 'action'=>'index', 'plugin'=> 'member')); ?>"><i class="icon-align-justify"></i> Deals</a></li>

      <li class="<?php echo($controller=='BuyerTransactions' && ( $action == "index" || $action == 'view' ) )?'active':'' ?>">
        <a href="<?php echo $this->Html->url(array('controller'=>'BuyerTransactions', 'action'=>'index', 'plugin'=> 'member')); ?>"><i class="icon-list"></i> Purchase Transactions </a></li>
    
      <li class="<?php echo($controller=='BuyerTransactions' && $action=="redeem" )?'active':'' ?>">
        <a href="<?php echo $this->Html->url(array('controller'=>'BuyerTransactions', 'action'=>'redeem', 'plugin'=> 'member')); ?>"><i class="icon-list"></i> Redeem Purchase </a>
      </li>

      <?php if( isset($business_member_level) && in_array($business_member_level, array(1,3) ) && $user['access_level'] == 1 ){ ?>

      <li class="<?php echo($controller=='MemberUsers' &&  $action != 'profile' && $action != 'editProfile')?'active':'' ?>">
        <a href="<?php echo $this->Html->url(array('controller'=>'MemberUsers', 'action'=>'index', 'plugin'=> 'member')); ?>"><i class="icon-user"></i> Members</a></li>
      <?php } ?>

      <?php if( isset($business_member_level) && in_array($business_member_level, array(1,3) ) ){ 
                if( $user['access_level'] == 1 ){
      ?>

      <li class="dropdown <?php echo($controller=='reports' || $controller == 'BusinessRevenues')?'active':'' ?>">
          
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="icon-list"></i> Finance <b class="caret"></b>
          </a>

          <ul class="dropdown-menu">

            <!-- <li>
              <a href="<?php echo $this->Html->url(array('controller'=>'reports', 'action'=>'index', 'plugin'=> 'member')); ?>">
                <i class="icon-list"></i> Report
              </a>
            </li> -->

            <?php if($user['access_level'] == 1 ): ?>
              <li>
                <a href="<?php echo $this->Html->url(array('controller'=>'BusinessRevenues', 'action'=>'index', 'plugin'=> 'member')) ?>">
                <i class="icon-list-alt"></i> Revenue</a>
              </li>
              <li>
                <a href="<?php echo $this->Html->url(array('controller'=>'BusinessRevenues', 'action'=>'balance', 'plugin'=> 'member')) ?>">
                <i class="icon-list-alt"></i> Balance</a>
              </li>
            <?php endif; ?>
          </ul>
      </li>

      <?php }else{ ?>

        <!-- <li class="<?php echo($controller=='reports')?'active':'' ?>">
          <a href="<?php echo $this->Html->url(array('controller'=>'reports', 'action'=>'index', 'plugin'=> 'member')); ?>"><i class="icon-list"></i> My Report</a>
        </li> -->

      <?php }

      }?>
      
    </ul>

  </div>
  <!-- / Main Navigation: Inner -->

</div>
