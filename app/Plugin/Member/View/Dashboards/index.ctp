<?php 

/********************************************************************************
    File: Dashboard Index for Business
    Author: PHEA RATTANA

    Confidential ABi Technologies property.

    Changed History:
    Date          Author        Description
    2014/mm/dd        PHEA RATTANA    Initial
*********************************************************************************/

//var_dump($business); exit;

   $active_tab = 'detail';

  if( isset($this->params['pass'][0]) && $this->params['pass'][0] == 'media'  ){

      $active_tab = "media" ;

  }else if(isset($this->params['pass'][0]) && $this->params['pass'][0] == 'menu'  ){
      $active_tab = "menu" ;
  }

  $menu_id = 0;

  if( isset($this->params['pass'][1]) ){
    $menu_id = $this->params['pass'][1];
  }


if( isset($business) && !empty($business) ){

  $province   = urldecode($business['Province']['province_name']);
  $city     = urldecode($business['Province']['province_name']);

  $location = ( $business['Business']['location'] != "" )? $business['Business']['location'] . ", " :"";
  $street   = ( $business['Business']['street'] != "" )? $business['Business']['street'] . ", " :"";

  $sangkat  = ( $business['SangkatInfo']['name'])? $business['SangkatInfo']['name'] . ", " :"";

  $address = $street . $sangkat . $location . $city ;

  $main_category = urldecode($business['MainCategory']['category']);
  $sub_category = urldecode($business['SubCategory']['category']);


  $biz_name = urldecode($business['Business']['business_name']);

  $lat = $business['Business']['latitude'];
  $lng = $business['Business']['longitude'];

//var_dump($this->Html->url(null, true)); exit;

  // Branches Data
  $data_braches = array();
  $time = time();

  if( isset($business['Branches']) && !empty($business['Branches']) ){
      foreach( $business['Branches'] as $k => $val ){
         $time += 300;

         $data_braches[$k]['index']       = $time;
         $data_braches[$k]['branch_name'] = $val['branch_name'];
         $data_braches[$k]['latitude'] = $val['latitude'];
         $data_braches[$k]['longitude'] = $val['longitude'];
      }
  }

  $limit = 10;

  $total_items = $count_menu_items ;
  $array_total_items = array();

  foreach( $total_items as $key => $val ){
     $menu_cate_id = $val['BusinessMenuItem']['menu_id'];
     $array_total_items[$menu_cate_id] = $val[0]['total'];
  }


?>


<div class="business form">
  <div class="row-fluid">
    <div class="top-bar">
      <ul class="tab-container">
          <li <?php echo ($active_tab == 'detail')?" class='active'":"" ?>><a href="#tab-detail"><i class="icon-list"></i> Merchant Detail</a></li>
          <li <?php echo ($active_tab == 'media')?" class='active'":"" ?>><a href="#tab-media"><i class="icon-picture"></i> Merchant Media</a></li>

          <?php 
            if(  isset($business_member_level) && in_array($business_member_level, array(1,3) ) &&  $user["access_level"] == 1 ): 
          ?>
            <li <?php echo ($active_tab == 'menu')?" class='active'":"" ?>><a href="#tab-menu"><i class="icon-list-alt"></i> Menu/ Item</a></li>
        <?php endif; ?>
        
      </ul>
    </div>

    <div class="tab-content">
      <div class="tab-content" style="color: #333 !important;">
        <div class="tab-pane  <?php echo ($active_tab == 'detail')?' active':'' ?>" id="tab-detail">
          <legend><?php echo __('Detail Information'); ?></legend> 
          <div class="span6" style="margin-left:0px;">   
          
          <?php if( isset($business_member_level) && in_array($business_member_level, array(1,3) ) && $user["access_level"] == 1 ): ?>
            <a href="<?php echo $this->Html->url( array('action' => 'edit', 'controller' => 'businesses', 'plugin' => 'member' )); ?>"> 
              <button class="btn btn-linkedin" type="button"><i class="icon-edit"></i> Edit Information</button>
            </a>

          <?php endif; ?>

            <h5 style="margin-top:20px;"><?php echo __($biz_name); ?></h5>

            <table width="100%">

              <tr>
                <td style="width: 120px;">Category </td>
                <td style="width: 30px"> : </td>
                <td><?php echo h($main_category); ?></td>
              </tr>

              <tr>
                <td>Address </td>
                <td> : </td>
                <td><?php echo h($address); ?></td>
              </tr>

              <tr>
                <td>Email</td>
                <td> : </td>
                <td>
                  <?php echo h($business['Business']['email']) ?>
                </td>
              </tr>

              <tr>
                <td>Website</td>
                <td> : </td>
                <td>
                  <a href="<?php echo (!empty($business['Business']['website']))?"http://" . str_replace("http://", "", $business['Business']['website'] ):"" ?>" target="_blank"><?php echo (!empty($business['Business']['website']))?str_replace("http://", "", $business['Business']['website'] ):"" ?></a>
                  
                </td>
              </tr>

              <tr>
                <td>Phone</td>
                <td> : </td>
                <td>
                  <?php echo h($business['Business']['phone1']) ?>
                  <?php echo($business['Business']['phone2'] != NULL )?" / " . $business['Business']['phone2']:"" ?>
                </td>
              </tr>

              <tr>
                <td>Accept Payment</td>
                <td> : </td>
                <td>
                  <?php $payment = json_decode($business['Business']['accept_payment']) ;

                  if( !empty($payment) ){

                    foreach( $payment as $key => $value ){
                      // echo "<strong>" . ucfirst($value) . "</strong>, ";
                      echo $this->Html->image( $value . '.jpg', array(  'alt' => ucfirst($value),
                                            'title'=>ucfirst($value),
                                            'style'=>'width:50px; margin-right:5px;'));
                    }
                  }
                  ?>
                      
                </td>
              </tr>

              <tr>
                <td width="120px;">Member Level </td>
                <td> : </td>
                <td <?php echo($business['Business']['member_level'] == 1)?" style='color:red'":" style='color:green'" ?>>
                  <b><?php echo $access_level[$business['Business']['member_level']] ?></b>
                </td>
              </tr>

              <tr>
                <td>Registered Date</td>
                <td> : </td>
                <td style="color: #06F">
                  <?php echo h(date("d-F-Y h:i:s A", strtotime($business['Business']['created']) )) ?>
                </td>
              </tr>

              <tr>
                <td>Registered By: </td>
                <td> : </td>
                <td>
                  <?php echo urldecode( $business['RegisteredBy']['first_name'] . " " . 
                      $business['RegisteredBy']['last_name'] ) ?>
                </td>
              </tr>

              <tr>
                <td>Business Status</td>
                <td> : </td>
                <td>
                  <span class="label label-<?php echo $status[$business['Business']['status']]['color'] ?>">
                    <?php echo h($status[$business['Business']['status']]['status']); ?>
                  </span>
                </td>
              </tr>

              <?php if( $business['Business']['status'] == 1 ){ ?>
                <tr>
                  <td>Approved Date</td>
                  <td> : </td>
                  <td>
                    <?php echo h(date("d-F-Y h:i:s A", strtotime($business['Business']['approved_date']) )) ?>
                  </td>
                </tr>
                <tr>
                  <td>Approved By</td>
                  <td> : </td>
                  <td>
                    <?php echo urldecode( $business['ApprovedBy']['first_name'] . " " . 
                                $business['ApprovedBy']['last_name'] ) ?>
                  </td>
                </tr>
              <?php } else if( $business['Business']['status'] == 2){ ?>
                <tr>
                  <td>Suspended Date</td>
                  <td> : </td>
                  <td>
                    <?php echo h(date("d-F-Y h:i:s A", strtotime($business['Business']['suspend_date']) )) ?>
                  </td>
                </tr>
              <?php }elseif( $business['Business']['status'] == -1){ ?>
                <tr>
                  <td>Inactive Date</td>
                  <td> : </td>
                  <td>
                    <?php echo h(date("d-F-Y h:i:s A", strtotime($business['Business']['inactive_date']) )) ?>
                  </td>
                </tr>
              <?php } ?>

            </table>

            <div style="clear:both; padding-top: 10px;"></div>
            <h5 style="margin-right:10px;"><?php echo __('Description'); ?></h5>
            <?php echo $business['Business']['description'] ?>

          </div>

          <div class="span5">

          <?php if( isset($business_member_level) && in_array($business_member_level, array(1,3) ) &&  $user["access_level"] == 1 ): ?>
            <a href="<?php echo $this->Html->url( array('action' => 'operationHourEdit', 'controller' => 'businesses', 'plugin' => 'member' )); ?>" id="editHour"  data-toggle="modal" data-detail='<?php  echo json_encode($business['OperationHour']) ?>'> 
              <button class="btn btn-skype" type="button"><i class="icon-edit"></i> Edit Operation Hour</button>
            </a>

          <?php endif; ?>

            <h5 style="margin-top:20px;"><?php echo __('Operation Hours'); ?></h5>

            <table width="100%">

              <?php foreach( $business['OperationHour'] as $key => $hour ):
              ?>
                <tr>
                  <td width="100px;"><?php echo strtoupper($hour['day']) ;?></td>
                  <td> : </td>
                  <td>
                  <?php 

                    if( $hour['from'] == NULL && $hour['to'] == NULL ){
                      echo "<i>Closed</i>";
                    }else if( $hour['from'] == $hour['to'] ){ 
                        echo "24 hours";
                     }else{ ?>
                      <?php echo ($hour['from']!=NULL) ? date("h:i:s A", strtotime($hour['from']) ):"<i>Empty</i>"; ?> - 
                      <?php echo ($hour['to']!=NULL) ? date("h:i:s A", strtotime($hour['to']) ):"<i>Empty</i>"; ?>
                      
                    <?php } ?>
                  </td>
                </tr>
              <?php endforeach; ?>

            </table>

            <hr><h5><?php echo __('Logo'); ?></h5>

            <div style="width:200px; height: 200px; border: 1px solid #CCC; float:left">
              <a rel="shadowbox" href="<?php echo $this->webroot . $business['Business']['logo'] ?>">
                <img src="<?php echo $this->webroot . $business['Business']['logo'] ?>" alt="" style="width: 100%; height: 100%;">
              </a>
            </div>

          <?php if(  isset($business_member_level) && in_array($business_member_level, array(1,3) ) &&  $user["access_level"] == 1 ): ?>
            

            <a href="#logo_change" data-toggle="modal" style="float:left; margin-left: 20px;"> 
              <button class="btn btn-linkedin" type="button"><i class="icon-edit"></i> Change Logo</button>
            </a>

            <a href="#removeLogo" data-toggle="modal" style="float:left; margin-left: 20px; margin-top: 20px;"> 
              <button class="btn btn-google cancel" type="button"><i class="icon-edit"></i> Remove Logo</button>
            </a>

          <?php endif; ?>   



           <!--  <div class="clearfix"></div>
            
            <h5 style="margin-right:10px;"><?php echo __('Map'); ?></h5>
      
            <div style="margin-bottom:5px;">
              <img style="width:17px;margin-right:10px;" src="<?php echo $this->webroot . 'img/pin-yellow.png' ?>" alt="">
              Main Location
            </div>

            <div style="padding-bottom:20px;">
              <img style="width:15px;margin-right:10px;" src="<?php echo $this->webroot . 'img/pin-blue.png' ?>" alt="">
              Branches Location
            </div>

            <div id="map" style="width:100%; height: 300px; border: 1px solid #CCC; "></div>  -->       


          </div>

            <div class="clearfix"></div>
            
            <h5 style="margin-right:10px;"><?php echo __('Map'); ?></h5>
      
            <div style="margin-bottom:5px;">
              <img style="width:17px;margin-right:10px;" src="<?php echo $this->webroot . 'img/pin-yellow.png' ?>" alt="">
              Main Location
            </div>

            <div style="padding-bottom:20px;">
              <img style="width:15px;margin-right:10px;" src="<?php echo $this->webroot . 'img/pin-blue.png' ?>" alt="">
              Branches Location
            </div>

            <div id="map" style="width:98%; height: 300px; border: 1px solid #CCC; "></div>
        </div>

        <div class="tab-pane <?php echo ($active_tab == 'media')?' active':'' ?>" id="tab-media">
          <legend><?php echo __('Merchant Media'); ?></legend>
      
          <div class="span6">
            <h5>Pictures</h5>

            <?php if(  isset($business_member_level) && in_array($business_member_level, array(1,3) ) &&  $user["access_level"] == 1 ): ?>
              <a href="#media_add_image" data-toggle="modal"> 
                <button class="btn btn-linkedin" type="button"><i class="icon-picture"></i> Upload Pictures</button>
              </a>
            <?php endif; ?>

            <div class="gallery no-padding" style="padding-left: 0px;">

            <?php 

              $pictures = $business['Pictures'];

              if( !empty($pictures) ){
                foreach( $pictures as $key => $pic ){
            ?>
              <div class="element" style="width: 150px ; height: 150px;" id="box-<?php echo $pic['id'] ?>" >
                <a rel="shadowbox" href="<?php echo $this->webroot . $pic['media_path'] ?>">
                  <img src="<?php echo $this->webroot . $pic['media_path'] ?>" width="240" height="240" class="img-polaroid">
                </a>
              <?php if(  isset($business_member_level) && in_array($business_member_level, array(1,3) ) &&  $user["access_level"] == 1 ): ?>
                <div  class="remove-btn"
                    href="#remove_img"
                    data-toggle="modal"
                    data-id="<?php echo $pic['id'] ?>"
                    title="remove" >
                  <img src="<?php echo $this->webroot ?>img/trash-red.png" alt="">
                </div>
              <?php endif; ?>

              </div>  
            
            <?php
                }
              }
            ?>

            </div>

          </div>

          <div class="span6" style="margin-left:0px;">
            <h5>Videos</h5>

            <?php if(  isset($business_member_level) && in_array($business_member_level, array(1,3) ) &&  $user["access_level"] == 1 ): ?>
              <a href="#media_add_video" data-toggle="modal"> 
                <button class="btn btn-linkedin" type="button"><i class="icon-plus"></i> Add Video</button>
              </a>

            <?php endif; ?>

            <div style="padding-top: 20px;">
              <?php 
                $videos = $business['Videos'];

                if( !empty($videos) ){
                  foreach( $videos as $key => $video ){
                    echo "<div class='video_box' id='box-" . $video['id'] . "' >";
                    // echo $video['media_embeded'];
                    $src = str_replace("watch?v=", 'embed/', $video['media_embeded']);
              ?>  
                    <iframe src="<?php echo $src ?>" 
                        frameborder="0" allowfullscreen>
                    </iframe>

                    <?php if(  isset($business_member_level) && in_array($business_member_level, array(1,3) ) &&  $user["access_level"] == 1 ): ?>
                    <div  class="remove-btn1"
                        href="#remove_video"
                        data-toggle="modal"
                        data-id="<?php echo $video['id'] ?>"
                        title="remove" >
                      <img src="<?php echo $this->webroot ?>img/trash-red.png" alt="">
                    </div>
                    
                    <?php endif; ?>

              <?php
                    echo "</div>";
                  }
                }
              ?>
            </div>
          </div>

        </div>

        <div class="tab-pane <?php echo ($active_tab == 'menu')?" active":"" ?>" id="tab-menu">

            <div class="span4">

                <legend><?php echo __('Category'); ?></legend>

                <?php 
                    $menuCategories = $business['MenuCategories'];
                ?>

                <a href="#" id="add-menu-category">
                    <button class="btn btn-linkedin" type="button"><i class="icon-plus"></i> Add New Category</button>
                </a>
                <div style="clear: both; padding-top: 20px;" ></div>

                <table class="table-list" id="category-table-list">

                    <thead>
                        <th width="30px;"><?php echo __('No'); ?></th>
                        <th><?php echo __('Category Name'); ?></th>
                        <th width="50px;"><?php echo __('Items') ?></th>
                        <th width="70px;"><?php echo __('Action'); ?></th>
                    </thead>

                    <tbody>
                        <?php 
                            if( $menuCategories ){
                                foreach( $menuCategories as $k => $val ){
                                    
                                    $total = isset($array_total_items[$val['id']])?$array_total_items[$val['id']]:0 ;

                                    $selected = ($menu_id == $val['id'] )?" selected":"" ;
                        ?>
                            <tr class="menu-row <?php echo $selected ?>" data-id="<?php echo $val['id'] ?>">
                              <td><?php echo $k + 1; ?></td>
                              <td class="name" ><?php echo $val['name'] ?></td>
                              <td style="text-align:center; font-weight:bold"><?php echo $total ?></td>
                              <td style="text-align:right">
                                <a href="#" class="btn btn-foursquare edit-menu-category" style="font-size:12px;"
                                    data-action="<?php echo $this->Html->url(array('action' => 'saveMenuCategory', $business['Business']['id'], $val['id'])); ?>">
                                    <i class="icon-edit"></i>
                                </a>

                                <a href="#" class="btn btn-google delete-menu-category" style="font-size:12px;"
                                    data-action="<?php echo $this->Html->url(array('action' => 'deleteMenuCategory', $business['Business']['id'], $val['id'])); ?>">
                                    <i class="icon-trash"></i>
                                </a>
                              </td>
                            </tr>
                        <?php 
                                }
                            }else{
                        ?>
                            <tr>
                                <td colspan="4"><i>Empty Category.</i></td>
                            </tr>
                        <?php 
                            }
                        ?>
                    </tbody>

                </table>

            </div>

            <div class="span8">
                <legend><?php echo __('Menu/ Services/ Items'); ?></legend>

                <h5 class="pull-left" style="margin-bottom:0px; color:#06F" id="menu-name"></h5>
                <a href="#" id="add-menu-item" class='pull-right' menu-id="0"
                    data-action="<?php echo $this->Html->url(array('action' => 'saveMenuItem', $business['Business']['id'])); ?>">
                    <button class="btn btn-linkedin" type="button"><i class="icon-plus"></i> Add New Item</button>
                </a>

                <div class="span12" style="margin-left:0px; text-align:center; margin-top:50px; display:none;" id="loading-item">
                    <img src="<?php echo $this->webroot . "img/ajax-loader.gif" ?>" alt="">
                    <h5>Loading...</h5>
                </div>

                <div class="span12" style="margin-left:0px;" id="item-container">
                    
                    <div style="clear: both; padding-top: 20px;" ></div>
                    <table class="table-list" id="menu-item-table">

                        <thead>
                            <th width="30px;"><?php echo __('No'); ?></th>
                            <th width="100px"><?php echo __('Image') ?></th>
                            <th><?php echo __('Title'); ?></th>
                            <th width="100px;"><?php echo __('Price') ?></th>
                            <th width="110px;"><?php echo __('Action'); ?></th>
                        </thead>

                        <tbody>
                                <tr class="item-row" style="display:none">
                                    <td class='item-no' ></td>
                                    <td class='item-image'>
                                        <a href="#"  rel="shadowbox">
                                          <img src="#" alt=""
                                           style="max-width:100px; padding: 2px; border: 1px solid #CCC;" >
                                        </a>
                                    </td>
                                    <td class='item-title'></td>
                                    <td class='item-price' style="font-weight: bold; color: #06F; text-align:right"></td>
                                    <td class='item-action' style="text-align:right">
                                        <!-- View -->
                                        <a href="#" class="btn btn-skype view-menu-item" style="font-size:12px;">
                                            <i class="icon-search"></i>
                                        </a>

                                        <!-- Edit -->
                                        <a href="#" class="btn btn-foursquare edit-menu-item" style="font-size:12px;"
                                            data-action="<?php echo $this->Html->url(array('action' => 'saveMenuItem', $business['Business']['id'])); ?>">
                                            <i class="icon-edit"></i>
                                        </a>
                                        
                                        <!-- Delete -->
                                        <a href="#" class="btn btn-google delete-menu-item" style="font-size:12px;"
                                            data-action="<?php echo $this->Html->url(array('action' => 'deleteMenuItem', $business['Business']['id'])); ?>">
                                            <i class="icon-trash"></i>
                                        </a>
                                    </td>
                                </tr>

                                <tr id="menu-item-empty">
                                    <td colspan="5">
                                        <i>Empty item.</i>
                                    </td>
                                </tr>
                        </tbody>

                    </table>
                </div>

                <div class="span12" style="margin-left:0px; text-align:center; margin-top:30px;">
                    <img src="<?php echo $this->webroot . "img/ajax-loader.gif" ?>" alt="" id="load-more-img" style="display:none" >
                    <button id="load-more-btn" type="button" class="btn btn-linkedin" style="display:none">Load More</button>
                </div>

            </div>
          
        </div>
      </div>
    </div>

  </div>
</div>


<!-- Include View for Business Category and Items -->
<?php include( dirname(__FILE__) . "/business-menu-modal.ctp" ); ?>

<!-- Remove Image -->
<div id="remove_img" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel"><i class="icon-picture"></i> Are You Sure ?</h3>
  </div>

  <form action="#" style="padding: 20px;" method="POST" id="remove_image"
      enctype = "multipart/form-data" >

    <div>
      <h5>Are you sure you want to delete this image?</h5>
    </div>

    <div class="submit" style="margin-top: 20px; clear: both;">
      <input class="btn btn-primary" data-dismiss='modal' type='button'  value="Yes" id="yes">
      <button class="btn btn-google cancel" data-dismiss='modal' type="button">No</button>
    </div>
      
  </form>

</div>

<!-- Remove Video -->
<div id="remove_video" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel"><i class="icon-picture"></i> Are You Sure ?</h3>
  </div>

  <form action="#" style="padding: 20px;" method="POST" id="remove_image"
      enctype = "multipart/form-data" >

    <div>
      <h5>Are you sure you want to delete this video?</h5>
    </div>

    <div class="submit" style="margin-top: 20px; clear: both;">
      <input class="btn btn-primary" data-dismiss='modal' type='button'  value="Yes" id="yes1">
      <button class="btn btn-google cancel" data-dismiss='modal' type="button">No</button>
    </div>
      
  </form>

</div>

<!-- Upload Images -->
<div id="media_add_image" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true"
  style="padding-bottom: 30px !important; ">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel"><i class="icon-picture"></i> Upload Pictures</h3>
  </div>

  <!-- Any Info-customization -->
  <form action="<?php echo $this->Html->url(array('action' => 'uploadMedia', 0 )); ?> " 
      style="padding: 20px;" method="POST" id="ChangeLogoForm"
      enctype = "multipart/form-data" >

    <div>
      <h5>Select Pictures : ( JPG, JPEG, PNG ) 300 x 300 Pixels minimum.</h5>
      <br>
      <input type="file" name="images[]" multiple required='required'>
    </div>

    <div class="submit" style="margin-top: 20px; clear: both;">
      <input class="btn btn-primary" type="submit" value="Upload">
      <button class="btn btn-google cancel" data-dismiss='modal' type="button">Cancel</button>
    </div>
      
  </form>

</div> 

<!-- Upload Video -->
<div id="media_add_video" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true"
  style="padding-bottom: 30px !important; ">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel"><i class="icon-edit"></i> Add Video</h3>
  </div>

  <!-- Any Info-customization -->
  <form action="<?php echo $this->Html->url(array('action' => 'uploadMedia', 1)); ?>" 
      style="padding: 20px;" method="POST"
      enctype = "multipart/form-data" >

    <div>
      <h5>Video URL : </h5>
      <!-- <textarea name="video_url" cols="30" rows="10" required="required" style="width: 90% !important"></textarea> -->
      <i style="color:#999">***Note: link from youtube only.</i>
      <input type="text" name="video_url" required="required" style="width: 90% !important">
    </div>

    <div class="submit" style="margin-top: 20px; clear: both;">
      <input class="btn btn-primary" type="submit" value="Save">
      <button class="btn btn-google cancel" data-dismiss='modal' type="button">Cancel</button>
    </div>
      
  </form>

</div>


<div id="logo_change" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true"
  style="padding-bottom: 30px !important; ">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel"><i class="icon-edit"></i> Change Business Logo</h3>
  </div>

  <!-- Any Info-customization -->
  <form action="<?php echo $this->Html->url(array('action' => 'changeLogo' )); ?>" 
      style="padding: 20px;" method="POST" id="ChangeLogoForm"
      enctype = "multipart/form-data" >

    <div>
      <h5>Select new logo ( JPG, JPEG, PNG ). Minimume size 225 x 225 pixels.</h5>
      <input type="file" name="logo" required="required">
    </div>

    <div class="submit" style="margin-top: 20px; clear: both;">
      <input class="btn btn-primary" type="submit" value="Upload">
      <button class="btn btn-google cancel" data-dismiss='modal' type="button">Cancel</button>
    </div>
      
  </form>

</div>


<!-- Remove Business Logo -->
<div id="removeLogo" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true"
  style="padding-bottom: 30px !important; ">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel"><i class="icon-edit"></i> Are You Sure ?</h3>
  </div>

  <!-- Any Info-customization -->
  <form action="<?php echo $this->Html->url(array('action' => 'removeLogo')); ?>" 
      style="padding: 20px;" method="POST" id="ChangeLogoForm"
      enctype = "multipart/form-data" >

    <div>
      <h5>Are you sure you want to remove this logo?</h5>
    </div>

    <div class="submit" style="margin-top: 20px; clear: both;">
      <input class="btn btn-primary" type="submit" value="Yes">
      <button class="btn btn-google cancel" data-dismiss='modal' type="button">No</button>
    </div>
      
  </form>

</div> 


<div id="message" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true"
  style="">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel"><i class="icon-exclamation"></i> Warning !</h3>
  </div>

  <div class="span6" style="padding-top: 10px; padding-bottom: 20px;">
    
    <h5 id="message"></h5>

    <button class="btn btn-google cancel" data-dismiss='modal' type="button">Close</button>
  </div>

</div> 

<script src="<?php echo $this->webroot . "js/admin/markerwithlabel.js" ?>" type="text/javascript"></script>
<script src="https://google-maps-utility-library-v3.googlecode.com/svn/tags/markerwithlabel/1.1.9/src/markerwithlabel.js" type="text/javascript"></script>
<script>

  var listBranchMarker = {};
  var map = null;
  var pin_icon_blue = '<?php echo $this->webroot ?>' + 'img/pin-blue.png';
  var iw = null;

  function addBranchMarker(index,latLng, brandName, latLngText) {

      listBranchMarker[index] = new MarkerWithLabel({
              map:map,
              position:latLng,
              icon: pin_icon_blue,
              labelContent: brandName  ,
              title:brandName,

              labelAnchor : new google.maps.Point(45, 67),
              labelClass : "labels",
              labelInBackground : false
      });

      var b_name = new google.maps.InfoWindow({ content: brandName + latLngText });

      google.maps.event.addListener( listBranchMarker[index], "click", function (e) { b_name.open(map, this); });
      
  }

  function initialize() {

        var fixLatitude = <?php echo $lat; ?>;
        var fixLongtitude = <?php echo $lng; ?>;
        var desc = "<?php echo $biz_name ?>";

        // alert(desc);

        var pin_icon = '<?php echo $this->webroot ?>' + 'img/pin-yellow.png';

        var myLatlng  = new google.maps.LatLng( fixLatitude, fixLongtitude );

        var mapOptions = {
                            zoom: 13,
                            center: myLatlng,
                            mapTypeId: google.maps.MapTypeId.ROADMAP
                        };

        map = new google.maps.Map( document.getElementById('map'), mapOptions);
        google.maps.event.trigger(map, "resize");

        var marker = new MarkerWithLabel({
                    map: map,
                    position: myLatlng,
                    icon : pin_icon,
                    // labelContent : number,
                    labelAnchor : new google.maps.Point( 5, 36 ),   
                    labelClass : "pin-label",
                    labelInBackground : false
                });

        var iw = new google.maps.InfoWindow({
                       content: desc
                     });


        google.maps.event.addListener(marker, "click", function (e) { iw.open(map, this); });

        var data_branch = <?php echo json_encode($data_braches) ?>;
        $.each( data_branch, function( ind, val ){
            var name  = val.branch_name,
                lat   = val.latitude,
                lng   = val.longitude,
                index = val.index;

            var newLatLng  = new google.maps.LatLng(lat, lng );

            latLngText = " (" + lat + ", " + lng + ")";
            addBranchMarker( index ,newLatLng, name, latLngText );
        })
    }

    $(document).ready(function(){

    $('table#category-table-list').find('tr.menu-row.selected').click();

    google.maps.event.addDomListener(window, 'load', initialize );

    Shadowbox.init();

    $(".remove-btn").click ( function(){

      var me = $(this),
        id = me.data('id');

      $('#yes').attr('data-id',id);     

    });

    $("#yes").click( function(){

      var id = $(this).attr('data-id');

      var url = "<?php echo $this->webroot ?>" + "member/businesses/removeMedia/" + id ;

      // console.log(url); return false;

      $.ajax({
              type: 'POST',
              url: url,
              data: {},
              dataType: 'json',
              
              success: function (data){

          // console.log(data);

          var boxId = "#box-" + id;

          $(boxId).fadeOut(500);

              },

              error: function ( err ){
                console.log(err);
                console.log(url);
              }

          });
    })


    $(".remove-btn1").click ( function(){

      var me = $(this),
        id = me.data('id');

      $('#yes1').attr('data-id',id);      

    });

    $("#yes1").click( function(){

      var id = $(this).attr('data-id');

      var url = "<?php echo $this->webroot ?>" + "member/businesses/removeMedia/" + id ;

      $.ajax({
              type: 'POST',
              url: url,
              data: {},
              dataType: 'json',
              
              success: function (data){

              // console.log(data);

              var boxId = "#box-" + id;

              $(boxId).fadeOut(500);

              },

              error: function ( err ){
                // console.log(err);
              }

          });
    });

    });

  // Menu script
    $('#add-menu-category').click( function(e){
        e.preventDefault();
        $('#add-menu-category-modal').find('input').val("");
        $('#add-menu-category-modal').modal('show');
    });

    // Edit Menu Category
    $('.edit-menu-category').click( function(e){
        e.preventDefault();

        var edit_modal  = $('div#edit-menu-category-modal');
        var edit_form   = $('form#editMenuCategoryForm');

        var row         = $(this).closest('tr'),
            id          = row.data('id'),
            name        = row.find('td.name').text(),
            action      = $(this).data('action');

        edit_form.attr('action', action);
        edit_form.find('input#menu-category-name').val(name);

        edit_modal.modal('show');

        return false;
    });


    // Delete Menu Category
    $('.delete-menu-category').click ( function(e){
        e.preventDefault();

        var delete_modal  = $('div#delete-menu-category-modal');
        var delete_form   = $('form#deleteMenuCategoryForm');

        var row         = $(this).closest('tr'),
            id          = row.data('id'),
            action      = $(this).data('action');

        delete_form.attr('action', action);

        delete_modal.modal('show');

        return false;

    });

    // Click on row, and get its item or product
    // ============================
    var loading_item = $('div#loading-item');
    loading_item.hide();

    var item_container = $('div#item-container');
    // item_container.hide();

    var item_row = $('tr.item-row');
    $('tr.item-row').remove();

    var item_row_empty = $('tr#menu-item-empty');
    var menu_id_selected = <?php echo $menu_id ?>;

    var menu_item_tbody = item_container.find('table#menu-item-table').find('tbody');

    function countNumberOfRow(){

        var count = $('table#menu-item-table').find('tbody').find('tr.item-row').length;

        if( count == 0 ){
            item_row_empty.show();
        }

    }

    var i            = 1;
    var limit_data   = <?php echo $limit ?>;
    var offset       = 0;
    var page         = 1;

    $('.menu-row').click( function(){

        i               = 1;
        limit_data      = <?php echo $limit ?>;
        page            = 1;
        offset          = ( page - 1) * limit_data ;

        loading_item.hide();
        item_container.hide();

        $(this).closest('tbody').find('tr').removeClass('selected');
        $(this).addClass('selected');

        var menu_id     = $(this).attr('data-id');
        var menu_name   = $(this).find('td.name').text();

        $('#add-menu-item').attr('menu-id', menu_id);
        $('h5#menu-name').text(menu_name);
        $('#load-more-btn').attr('menu-id', menu_id);

        setTimeout(function(){
            loading_item.show();
        }, 100);

        var getItemURL__ = "<?php echo $this->Html->url( array('action' => 'getMenuItemByMenuIDAjax__')); ?>" ;
        
        menu_item_tbody.find('tr.item-row').remove();

        if( menu_id ){
            $.ajax({

                url: getItemURL__,
                type: "POST",
                data: { menu_id: menu_id, limit: limit_data + 1, offset:offset },
                async: false,
                dataType: 'json',

                success: function(data){

                    if( data.status == true ){

                        item_row_empty.hide();
                        var root = "<?php echo $this->webroot ?>";
                        var result = data.result;

                        if( result.length > limit_data ){
                            $('#load-more-btn').show();
                            result.splice(limit_data, 1);
                            
                        }else{
                            $('#load-more-btn').hide();
                        }

                        $.each( result, function(ind, val){
                           
                            var img     = root + val.BusinessMenuItem.image;
                            var price   = parseFloat(val.BusinessMenuItem.price) ;

                            var thm_img = img.replace("/menus/", "/menus/thumbs/" ) ;

                            price = price.toFixed(2);

                            item_row.attr('id', val.BusinessMenuItem.id);

                            item_row.find('td.item-no').text(i);
                            // item_row.find('td.item-image').find('a').attr('href', img);
                            item_row.find('td.item-image').find('a').attr('href', "#");
                            item_row.find('td.item-image').find('img').attr('src', thm_img);

                            item_row.find('td.item-title').text(val.BusinessMenuItem.title);
                            item_row.find('td.item-price').text("$ " + price);

                            item_row.find('.edit-menu-item').attr('data-id', val.BusinessMenuItem.id);
                            item_row.find('.view-menu-item').attr('data-id', val.BusinessMenuItem.id);
                            item_row.find('.delete-menu-item').attr('data-id', val.BusinessMenuItem.id);

                            menu_item_tbody.append(item_row.clone().show());

                            i++;

                        });

                        Shadowbox.init();

                        setTimeout(function(){
                            loading_item.hide();
                            item_container.fadeIn();
                        }, 500);

                    }else{
                        $('#load-more-btn').hide();
                        setTimeout(function(){
                            loading_item.hide();
                            item_row_empty.show();
                            item_container.fadeIn();
                        }, 500);
                    }

                },

                error: function(err, msg){
                    console.log(msg)
                }

            });
        }

    });
    
    // Load More Items
    $('#load-more-btn').click( function(){

        page++;
        offset = ( page - 1) * limit_data;

        var me          = $(this);
        var menu_id     = me.attr('menu-id');

        me.hide();
        $('#load-more-img').show();

        var getItemURL__ = "<?php echo $this->Html->url( array('action' => 'getMenuItemByMenuIDAjax__')); ?>" ;
        var show = 0;

        if( menu_id ){
            $.ajax({

                url: getItemURL__,
                type: "POST",
                data: { menu_id: menu_id, limit: limit_data + 1, offset:offset },
                async: false,
                dataType: 'json',

                success: function(data){

                    if( data.status == true ){

                        item_row_empty.hide();
                        var root = "<?php echo $this->webroot ?>";
                        var result = data.result;

                        if( result.length > limit_data ){
                            show = 1;
                            result.splice(limit_data, 1);                            
                        }else{
                            $('#load-more-btn').hide();
                        }

                        $.each( result, function(ind, val){
                           
                            var img     = root + val.BusinessMenuItem.image;
                            var price   = parseFloat(val.BusinessMenuItem.price) ;

                            var thm_img = img.replace("/menus/", "/menus/thumbs/" ) ;

                            price = price.toFixed(2);

                            item_row.attr('id', val.BusinessMenuItem.id);

                            item_row.find('td.item-no').text(i);
                            // item_row.find('td.item-image').find('a').attr('href', img);
                            item_row.find('td.item-image').find('a').attr('href', "#");
                            item_row.find('td.item-image').find('img').attr('src', thm_img);

                            item_row.find('td.item-title').text(val.BusinessMenuItem.title);
                            item_row.find('td.item-price').text("$ " + price);

                            item_row.find('.edit-menu-item').attr('data-id', val.BusinessMenuItem.id);
                            item_row.find('.view-menu-item').attr('data-id', val.BusinessMenuItem.id);
                            item_row.find('.delete-menu-item').attr('data-id', val.BusinessMenuItem.id);

                            menu_item_tbody.append(item_row.clone().show());

                            i++;

                        });

                        if( show == 1 ){
                            setTimeout( function(){
                                $('#load-more-img').hide();
                                me.show();
                            },500);
                        }else{
                            setTimeout( function(){
                                $('#load-more-img').hide();
                                me.hide();
                            },500);
                        }

                        Shadowbox.init();

                    }else{                        
                        $('#load-more-img').hide();
                        $('#load-more-btn').hide();
                    }

                },

                error: function(err, msg ){

                }
            });
        }

    });

    // Add Menu Item
    // ===================
    $('#add-menu-item').click( function(e){

        e.preventDefault();

        var menu_id = $(this).attr('menu-id');
        var action  = $(this).attr('data-action');

        action += "/" + menu_id;

        if( menu_id == 0 ){

            $("div#message").find('h5').text("Please select category first !");
            $('div#message').modal('show');
            return false;
        }

        var add_item_modal = $('div#add-menu-item-modal');
        add_item_modal.find('form').attr('action', action);

        add_item_modal.find('input').val("");
        CKEDITOR.instances.itemDescription.setData("");

        add_item_modal.modal('show');

    })

    // Edit Menu Item
    $('table#menu-item-table').on('click', '.edit-menu-item', function(event){
        event.preventDefault();

        var me      = $(this),
            id      = me.data('id'),
            action    = me.data('action') ;

        var menu_id = $('#add-menu-item').attr('menu-id');

        var URL__ = "<?php echo $this->Html->url( array('action' => 'getMenuItemDetailAjax__')); ?>" ;

        if( id ){
            $.ajax({

                url: URL__,
                type: "POST",
                data: { item_id: id},
                async: false,
                dataType: 'json',

                success: function(data){

                    if( data.status == true ){

                        var editModal   = $('div#edit-menu-item-modal');
                        var form        = editModal.find('form');
                        var detail      = data.detail;

                        form.find('#BusinessMenuItemTitle').val(detail.title);
                        form.find('#BusinessMenuItemPrice').val(detail.price);
                        form.find('#data_old_img').val(detail.image);
                        CKEDITOR.instances.itemDescriptionEdit.setData(detail.description);

                        form.attr('action', action + "/" + menu_id + "/" + id );
                        editModal.modal('show');

                    }else{

                       $('div#message').find('h5').text(data.message);
                       $('div#message').modal('show');
                       return false;
                    }

                },

                error: function(err, msg){
                    console.log(msg);
                }

            });
        }

    })

    // View Detail Menu Item
    $('table#menu-item-table').on('click', '.view-menu-item', function(event){

        event.preventDefault();
        var me      = $(this),
            id      = me.data('id');

        var menu_id = $('#add-menu-item').attr('menu-id');

        var URL__ = "<?php echo $this->Html->url( array('action' => 'getMenuItemDetailAjax__')); ?>" ;

        if( id ){
            $.ajax({

                url: URL__,
                type: "POST",
                data: { item_id: id},
                async: false,
                dataType: 'json',

                success: function(data){

                    if( data.status == true ){

                        var root = "<?php echo $this->webroot ?>";

                        var viewModal   = $('div#detail-menu-item-modal');
                        var detail      = data.detail;
                        var price = parseFloat(detail.price).toFixed(2);

                        viewModal.find('#title').text(detail.title);
                        viewModal.find('#price').text("$ " + price);
                        viewModal.find('#desc').html(detail.description);
                        viewModal.find('#image').attr('src', root + detail.image );
                        viewModal.find('#image').attr('alt', detail.title );

                        viewModal.modal('show');

                    }else{

                       $('div#message').find('h5').text(data.message);
                       $('div#message').modal('show');
                       return false;
                    }

                },

                error: function(err, msg){
                    console.log(msg);
                }

            });
        }

    })

    
    // Delete Menu Item
    $('table#menu-item-table').on('click', '.delete-menu-item', function(event){

        event.preventDefault();

        var me      = $(this),
            id      = me.data('id');

        $('div#delete-menu-item-modal').find('#confirm-delete-item').attr('item-id', id );
        $('div#delete-menu-item-modal').modal('show');

    });

    $('#confirm-delete-item').click( function(){
        var me  = $(this),
            id  = me.attr('item-id');

        var deleteItemURL__ = "<?php echo $this->Html->url( array('action' => 'deleteMenuItemAjax__')); ?>" ;

        var deleteModal = $('div#delete-menu-item-modal');

        if( id ){
            $.ajax({
                url: deleteItemURL__,
                type: "POST",
                data: { item_id: id},
                async: false,
                dataType: 'json',

                success: function(data){

                    if( data.status == true ){
                        deleteModal.modal('hide');
                        $('table#menu-item-table').find('tbody').find('tr#' + id).fadeOut(1000).remove();

                        countNumberOfRow();

                    }else{
                        console.log(data.message);
                    }

                },

                error: function(err, msg){
                    
                }

            });
        }

    });



   $('input.menu-item-image').on( 'change', function(){

        var me = $(this);

        var max_size = <?php echo $__MAXIMUM_IMAGE_SIZE ?>;
        var files = $($(this)[0].files);
        var size = files[0].size ;

        var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.png)$/;

        var error = false;
        var ext_msg   = "";
        var size_msg  = "";
        var dimension_msg = "";

        var min_deal_width  = <?php echo $__DEAL_WIDTH ?>;
        var min_deal_height = <?php echo $__DEAL_HEIGHT ?>;

        if (!regex.test(files[0].name.toLowerCase())) {
            error = true;
            ext_msg = "Invalid Image type." ;
        }

        if( error == false && size > max_size ){
            error = true;
            size_msg = "Images reached size limitation. Images should be less then 2MB.";
        }

        if( error == false ){ 
          var file = files[0];
          var reader = new FileReader();
          var image  = new Image();

          reader.readAsDataURL(file);  
          reader.onload = function(_file) {
              image.src    = _file.target.result;              // url.createObjectURL(file);
              image.onload = function() {
                  var w = this.width,
                      h = this.height;

                  if( w < min_deal_width && h < min_deal_height ){

                    $('input.menu-item-image').val('');
                    $('div#message').find('h5').text("Image size should be " + min_deal_width + " x " + min_deal_height + " pixels." );
                    $('div#message').modal('show');

                    return false;
                  }
                  
              };      
          };

        }else{
          
          var mss = "";
          if( ext_msg ){
              mss += ext_msg + "<br>" ;
          }

          if( size_msg ){
              mss += size_msg + "<br>" ;
          }

          if( dimension_msg ){
              mss += dimension_msg;
          }
          
          $('input.menu-item-image').val('');
          $('div#message').find('h5').text( mss );
          $('div#message').modal('show');

          return false;

        }

      })


</script>

<?php } ?>