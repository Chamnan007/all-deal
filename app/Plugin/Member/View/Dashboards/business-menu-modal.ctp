
<div id="add-menu-category-modal" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true"
  style="">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel"><i class="icon-plus"></i> Add Menu Category</h3>
  </div>
    
  <form action="<?php echo $this->Html->url(array('action' => 'saveMenuCategory', $business['Business']['id'])); ?>" 
      style="padding: 20px;" method="POST"  data-validate = "parsley"
      id="addMenuCategoryForm" >
    
    <div class="span6" style="padding-top: 10px; padding-bottom: 20px;">
        
        <label for="" class="pull-left" style="padding-top:5px; margin-right:20px;">Category Name</label>

        <input  type="text" name="BusinessMenu[name]" 
                maxlength="100" required='required' 
                placeholder="Category Name"
                style="width: 330px;" 
                class="pull-left">
        
    </div>

    <div class="submit" style=" margin-left:20px;">
      <!-- <input class="btn btn-primary" type="button" value="Save" id="save"> -->
      <button class="btn btn-primary" type="submit" >Save</button>
      <button class="btn btn-google cancel" data-dismiss='modal' type="button">Cancel</button>
    </div>

  </form>

</div> 

<div id="edit-menu-category-modal" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true"
  style="">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel"><i class="icon-edit"></i> Edit Menu Category</h3>
  </div>
    
  <form action="" 
      style="padding: 20px;" method="POST"  data-validate = "parsley"
      id="editMenuCategoryForm" >
    
    <div class="span6" style="padding-top: 10px; padding-bottom: 20px;">
        
        <label for="" class="pull-left" style="padding-top:5px; margin-right:20px;">Category Name</label>

        <input  type="text" name="BusinessMenu[name]" 
                maxlength="100" required='required' 
                placeholder="Category Name"
                id="menu-category-name"
                style="width: 330px;" 
                class="pull-left">
        
    </div>

    <div class="submit" style=" margin-left:20px;">
      <!-- <input class="btn btn-primary" type="button" value="Save" id="save"> -->
      <button class="btn btn-primary" type="submit" >Save</button>
      <button class="btn btn-google cancel" data-dismiss='modal' type="button">Cancel</button>
    </div>

  </form>

</div> 


<div id="delete-menu-category-modal" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true"
  style="">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel" style="color:red"><i class="icon-exclaimation"></i> Are you sure?</h3>
  </div>
    
  <form action="" 
      style="padding: 20px; padding-top:0px;" method="POST"  data-validate = "parsley"
      id="deleteMenuCategoryForm" >
    
    <div class="span6" style="padding-top: 10px; padding-bottom: 20px;">
        
        <h5>Once you deleted, all items in this category will be deleted. Are you sure you want to delete this category? </h5>
        
    </div>

    <div class="submit" style=" margin-left:20px;">
      <!-- <input class="btn btn-primary" type="button" value="Save" id="save"> -->
      <button class="btn btn-primary" type="submit" >Yes</button>
      <button class="btn btn-google cancel" data-dismiss='modal' type="button">No</button>
    </div>

  </form>

</div> 

<!-- Menu Item -->

<div id="add-menu-item-modal" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true"
  style=" width:900px; left:38%">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel"><i class="icon-plus"></i> Add New Item</h3>
  </div>
    
  <form action="#" 
      style="padding: 20px;" method="POST"  data-validate = "parsley"
      id="addMenuItem"
      enctype = "multipart/form-data" >
    
    <div class="span5">
        <?php 
            echo $this->Form->input(   'title' ,array(
                       'type'     => 'text',
                       'label'    => 'Title',
                       'required' => 'required',
                        'id'        => 'BusinessMenuItemTitle',
                        'name'      => "BusinessMenuItem[title]",
                        'placeholder' => 'Title',
                        'maxLength' => '100',
                        'style'     => 'width:310px'
                    ));


            echo $this->Form->input(   'Price' ,array(
                       'type'     => 'text',
                       'label'    => 'Price',
                       'required' => 'required',
                        'id'        => 'BusinessMenuItemPrice',
                        'name'      => "BusinessMenuItem[price]",
                        'placeholder' => 'Price (USD)',
                        'style'     => 'width:310px',
                        'onkeypress' => 'return numericAndDotOnly(this)'
                    ));

        ?>

        <h5>Select Image</h5>
        <label for="" style='color: blue'>Allow Image ( PNG, JPEG, JPG, GIF) and Image size should be 705 x 422 pixels</label>
        <input type="file" name='menuItemImage' class="menu-item-image" required="required">


    </div>
    <div class="span5" style="padding-bottom: 20px; width: 420px;">
        
        <?php 
            echo $this->Form->input('description', array('placeholder' => 'Description',
                               'type' => 'textarea',
                               'class' => 'ckeditor',
                               'name' => 'BusinessMenuItem[description]',
                               'id' => 'itemDescription',
                               'maxlength' => '300'));
        ?>
        
    </div>

    <div class="submit" style=" margin-left:20px; clear: both;">
      <!-- <input class="btn btn-primary" type="button" value="Save" id="save"> -->
      <button class="btn btn-primary" type="submit" >Save</button>
      <button class="btn btn-google cancel" data-dismiss='modal' type="button">Cancel</button>
    </div>

  </form>

</div>


<!-- Edit View Menu Item -->
<div id="edit-menu-item-modal" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true"
  style=" width:900px; left:38%">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel"><i class="icon-edit"></i> Edit Item</h3>
  </div>
    
  <form action="#" 
      style="padding: 20px;" method="POST"  data-validate = "parsley"
      id="editMenuItem"
      enctype = "multipart/form-data" >
    
    <div class="span5">
        <?php 
            echo $this->Form->input(   'title' ,array(
                       'type'     => 'text',
                       'label'    => 'Title',
                       'required' => 'required',
                        'id'        => 'BusinessMenuItemTitle',
                        'name'      => "BusinessMenuItem[title]",
                        'placeholder' => 'Title',
                        'maxLength' => '100',
                        'style'     => 'width:310px'
                    ));


            echo $this->Form->input(   'Price' ,array(
                       'type'     => 'text',
                       'label'    => 'Price',
                       'required' => 'required',
                        'id'        => 'BusinessMenuItemPrice',
                        'name'      => "BusinessMenuItem[price]",
                        'placeholder' => 'Price (USD)',
                        'style'     => 'width:310px',
                        'onkeypress' => 'return numericAndDotOnly(this)'
                    ));

        ?>

        <h5>Select New Image</h5>
        <label for="" style='color: blue'>Allow Image ( PNG, JPEG, JPG, GIF) and Image size should be 705 x 422 pixels</label>
        <input type="file" name='menuItemImage'  class="menu-item-image">
        <input type="hidden" id="data_old_img" name="data_old_img" >

    </div>
    <div class="span5" style="padding-bottom: 20px; width: 420px;">
        
        <?php 
            echo $this->Form->input('description', array('placeholder' => 'Description',
                               'type' => 'textarea',
                               'class' => 'ckeditor',
                               'name' => 'BusinessMenuItem[description]',
                               'id' => 'itemDescriptionEdit',
                               'maxlength' => '300'));
        ?>
        
    </div>

    <div class="submit" style=" margin-left:20px; clear: both;">
      <button class="btn btn-primary" type="submit" >Save Change</button>
      <button class="btn btn-google cancel" data-dismiss='modal' type="button">Cancel</button>
    </div>

  </form>

</div>


<!-- Detail View Menu Item -->
<div id="detail-menu-item-modal" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true"
  style=" width:900px; left:38%">
  
  <form action="#">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      <h3 id="myModalLabel"><i class="icon-list"></i> Detail Item</h3>
    </div>

    <div class="span6" style="padding-top: 20px; ">
        <table>

            <tr>
                <td width="100px">Title</td>
                <td width="20px">:</td>
                <td style="font-size:16px; font-weight: bold" id="title"></td>
            </tr>
            <tr>
                <td>Price</td>
                <td>:</td>
                <td style="color:#06F; font-size:15px; font-weight: bold" id="price"></td>
            </tr>

            <tr>
                <td>Description</td>
                <td>:</td>
                <td style="color:#06F; font-size:15px;">
                  <div id="desc" style="max-height:300px; width:340px;  overflow-y: scroll"></div>
                </td>
            </tr>

        </table>
    </div>

    <div class="span5">
        <h5 style="font-size:15px">Image</h5>
        <img src="" alt="" id="image" style="max-width: 350px; height: auto; max-height:300px; padding: 5px; border: 1px solid #CCC; " >
    </div>
    
    <div class="clearfix"></div>
    <div class="submit" style=" margin-left:20px; clear: both;">
      <button class="btn btn-google cancel" data-dismiss='modal' type="button">Close</button>
    </div>
  </form>

</div>


<div id="delete-menu-item-modal" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true"
  style="">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel" style="color:red"><i class="icon-exclaimation"></i> Are you sure?</h3>
  </div>
    
  <form action="#" 
      style="padding: 20px; padding-top:0px;" method="POST"  data-validate = "parsley"
      id="deleteMenuItemForm" >
    
    <div class="span6" style="padding-top: 10px; padding-bottom: 20px;">
        
        <h5>Are you sure you want to delete this item?</h5>
        
    </div>

    <div class="submit" style=" margin-left:20px;">
      <!-- <input class="btn btn-primary" type="button" value="Save" id="save"> -->
      <button class="btn btn-primary" type="button" id="confirm-delete-item" >Yes</button>
      <button class="btn btn-google cancel" data-dismiss='modal' type="button">No</button>
    </div>

  </form>

</div> 

<style>
  p{
    margin-top:0px;
  }
</style>