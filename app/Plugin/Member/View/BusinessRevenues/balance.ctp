<?php 

	/********************************************************************************
	    File: Buyers Listing
	    Author: PHEA RATTANA
	
	    Confidential ABi Technologies property.
	
	  	Changed History:
	  	Date 					Author				Description
	  	2015/01/May 			PHEA RATTANA		Initial
	*********************************************************************************/

	$groupedData 	= array();
	$date 			= array();
	$deal_amount 	= array();
	$deal_balance 	= array();
	$status 		= 1;
	$debit 			= 0;
	$credit 		= 0;
	$status 		= 1;

	foreach( $data as $key => $val ){
		
		$type = $val['BusinessRevenue']['type'] ;
		// 1: Transaction
		// 2: Payment

		if( $type == 1 ){
			$transactionDetail = $val['TransactionInfo'];
			$dealInfo 		   = $val['TransactionInfo']['DealInfo'];
			$deal_code 		   = $dealInfo['deal_code'];
			$deal_id   		   = $dealInfo['id'];

			$deal_amount[$deal_code] += $val['BusinessRevenue']['total_amount'];

			if( $dealInfo['status'] == -1 || strtotime($dealInfo['valid_to']) < time() ){
				$status = -1;
			}

			$groupedData[$deal_code]['type']				= $type;
			$groupedData[$deal_code]['deal_id'] 			= $deal_id;
			$groupedData[$deal_code]['date'][]  			= strtotime($val['BusinessRevenue']['created']);
			$groupedData[$deal_code]['status']				= $status;
			$groupedData[$deal_code]['credit']				= $deal_amount[$deal_code];
			$groupedData[$deal_code]['debit']				= 0;
			$groupedData[$deal_code]['method']				= "";
			$groupedData[$deal_code]['transferred_date']	= "";
			$groupedData[$deal_code]['detail'][]			= $val['TransactionDetail'];

		}else{

			$paymentInfo = $val['PaymentInfo'];

			$deal_id = explode(",", $paymentInfo['deal_ids']);

			$method = "- " . $pay_method[$paymentInfo['payment_method']];

			if( $paymentInfo['payment_method'] == 1 ){ // Bank Transferred
				$method .= "<br>";
				$method .= "- Via: " . $paymentInfo['from_bank'] . " - " . $paymentInfo['to_bank'] ;
			}

			$groupedData[$paymentInfo['id']]['type']				= $type;
			$groupedData[$paymentInfo['id']]['deal_id'] 			= $deal_id;
			$groupedData[$paymentInfo['id']]['date'][] 				= strtotime($val['BusinessRevenue']['created']);
			$groupedData[$paymentInfo['id']]['status']				= -1;
			$groupedData[$paymentInfo['id']]['credit']				= 0;
			$groupedData[$paymentInfo['id']]['debit']				= $val['BusinessRevenue']['total_amount'];
			$groupedData[$paymentInfo['id']]['method']				= $method ;
			$groupedData[$paymentInfo['id']]['transferred_date']	= $paymentInfo['created'];
			$groupedData[$paymentInfo['id']]['detail'][]			= "" ;
		}
		
	}
	
 ?>
<style>
  table, tr, td{
     vertical-align: top; 
  }
</style>

<div class="users index">
	<div class="row-fluid">		
		<!-- Pie: Box -->
		<div class="span12">
			
			<form action="" method="POST" id="frmBuyerList" >

				<!-- Pie: Top Bar -->
				<div class="top-bar">
					<h3><i class="icon-list"></i> Balance History</h3>
				</div>
				<!-- / Pie: Top Bar -->

				<!-- Pie: Content -->
				<div class="well" style="padding-bottom: 100px; min-height: 200px;">

				<div class="clearfix"></div>

					<h5 style="float:left">Avaialable Balance : <span style="color: blue">$ <?php echo $this->MyHtml->formatNumber($ending_balance) ?></span></h5>

					<a id="btn-search-clear" href="#" class="btn btn-google pull-right" >Clear Search</a>
					<a id="btn-search-transaction" href="#" class="btn btn-linkedin pull-right" style="margin-right:10px;">Search History</a>

					<div class="clearfix"></div>
					
					<table class="table-list">
						<thead>
							<tr>
								<th width="10px"></th>
								<th width="160px;">Date</th>
								<th width="100px">Deal Code</th>
								<th width="100px">Deal Status</th>
								<th width="150px;">Debits</th>
								<th width="150px;">Credits</th>
								<th>Method</th>
								<th width="100px;">Transferred Date</th>
								<th width="100px">Balance</th>
							</tr>
						</thead>
						<tbody>

						<?php if($groupedData){ 

							$old_credit = $old_debit = 0;
						?>
						<?php foreach ($groupedData as $key => $value ): 

								@$count++;		

								$date = max($value['date']);
								$date = date('d-F-Y h:i:s A', $date);

								$deal_id 	= $value['deal_id'];

								$deal_code 	= $key;		
								
								$status 		= $value['status'];
								$deal_status 	= "Active";
								$color 			= "red" ;

								if( $status == -1 ){
									$deal_status = "Finished";
									$color 		 = "blue";
								}

								$debit = $value['debit'];
								$credit = $value['credit'];

								$transferred_date = ($value['transferred_date'])?date('d-F-Y', strtotime($value['transferred_date'])):"-";

								$method = ($value['method'])?$value['method']:"-";

								$balance = $ending_balance + $old_debit - $old_credit;

								$ending_balance = $balance;
								$old_debit 	= $debit;
								$old_credit = $credit;
								
						?>

							<!-- Data Row Here -->
							<tr>
								<td style="text-align:center" class="" data-code="<?php echo $key ?>">
									<strong>+</strong>
								</td>
								<td class="expand" data-code="<?php echo $key ?>">
									<?php echo $date ?>
								</td>
								<td>
									<?php if( $value['type'] == 1){ ?>
									<a 	href="<?php echo $this->Html->url(array('action' => 'view','controller' => 'deals', $deal_id )); ?>" 
										title="View Deal"
										target="_blank">
										<strong><?php echo $deal_code ?></strong>
									</a>
									<?php }else{
										foreach( $deal_id as $ind => $id ){
											$deal_code = $deal_data[$id]['deal_code'];
									 ?>	
										 <a 	href="<?php echo $this->Html->url(array('action' => 'view','controller' => 'deals', $id )); ?>" 
											title="View Deal"
											target="_blank">
											<strong><?php echo $deal_code ?></strong>
										</a>

										<?php echo(isset($deal_data[$ind+1]))?", ":"" ?>

									<?php }
										} 
									?>
								</td>
								<td style="color:<?php echo $color ?>">
									<?php echo $deal_status; ?>
								</td>
								<td style="text-align:right">
									<?php echo ($debit)?"$ " .$this->MyHtml->formatNumber($debit):"-" ?>
								</td>
								<td style="text-align:right">
									<?php echo ($credit)?"$ " .$this->MyHtml->formatNumber($credit):"-" ?>
								</td>
								<td>
									<?php echo $method ?>
								</td>
								<td>
									<?php echo $transferred_date ?>
								</td>

								<td style="font-weight:bold; text-align:right">
									<?php echo ($balance)?"$ " .$this->MyHtml->formatNumber($balance):"-" ?>
								</td>

							</tr>

						<?php 
							endforeach;

							}else{ ?>
							<tr>
								<td colspan="10"><i>No Transaction Found !</i></td>
							</tr>

						<?php } ?>
						</tbody>
					</table>

				</div>
				<!-- / Pie: Content -->

			</form>

		</div>
		<!-- / Pie -->
		
	</div>

</div>


<div id="search-transaction-modal" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel"><i class="icon-search"></i> Search Transaction</h3>
  </div>

  <!-- Any Info-customization -->
  <form action="<?php echo $this->Html->url(array('action' => 'balance')); ?>" 
      	style="padding: 20px;" method="POST" id="transactionSearchForm"
      	enctype = "multipart/form-data" >

    <div style="width:100%;">

		<fieldset>

			<div style="display:block; clear:both">
				<label for="BuyerTransactionPurchaseDate" style="float:left; width:100px; padding-top:5px;">Purchase Date</label>
				<div id="dealValidFromPicker" class="input-append date datetimepicker" style="float:left">
	                <input data-format="dd-MM-yyyy" type="text" 
	                    name="DateFrom"
	                    style="width: 135px"
	                    value="<?php echo @$balanceStates['DateFrom'] ?>"
	                    placeholder="From">
	                <span class="add-on">
	                  <i data-time-icon="icon-time" data-date-icon="icon-calendar">
	                  </i>
	                </span>
	            </div>
				<span style="float:left; margin-left:8px; margin-right:7px; padding-top:5px;">TO</span>
				<div id="dealValidFromPicker" class="input-append date datetimepicker" style="float:left">
	                <input data-format="dd-MM-yyyy" type="text" 
	                    name="DateTo"
	                    style="width: 135px"
	                    value="<?php echo @$balanceStates['DateTo'] ?>"
	                    placeholder="To">
	                <span class="add-on">
	                  <i data-time-icon="icon-time" data-date-icon="icon-calendar">
	                  </i>
	                </span>
	            </div>
				
			</div>

		</fieldset>

    </div>

    <div class="submit" style="margin-top: 40px; clear: both;">
      <input class="btn btn-primary" type="submit" value="Submit Search">
      <button type="button" class="btn btn-linkedin reset-btn">Reset</button>
      <button class="btn btn-google cancel" data-dismiss='modal' type="button">Close</button>
    </div>
      
  </form>

</div> 


<script>
	$(document).ready( function(){
		$('.datetimepicker').datetimepicker();
		$('.datetimepicker').find('input').attr('readonly', 'readonly');
		$('.datetimepicker').find('input').css('cursor', 'default');
	})

	$('#clear-search').click( function(){
		$('input#search-text').val("");
		$('select#status').val('all');

		$('form#frmPurchaseTransaction').submit();
	})

	$('#btn-search-transaction').click( function(event){

		event.preventDefault();

		var modal = $('div#search-transaction-modal');

		modal.modal('show');

	});

	$('#btn-search-clear').click( function(){
		var modal = $('div#search-transaction-modal');

		$('.reset-btn').click();
		$('form#transactionSearchForm').submit();
	});

	$('.reset-btn').click( function(){

	 	var modal = $('div#search-transaction-modal');

	 	modal.find('input[type="text"]').val("");
	 	modal.find('select#merchant').val("");

	})
</script>