<?php 

	/********************************************************************************
	    File: Buyers Listing
	    Author: PHEA RATTANA
	
	    Confidential ABi Technologies property.
	
	  	Changed History:
	  	Date 					Author				Description
	  	2015/01/May 			PHEA RATTANA		Initial
	*********************************************************************************/

	$groupedData = array();
	$date = array();
	$deal_amount = array();
	$deal_revenue = array();
	$status = 1;

	foreach( $data as $key => $val ){
		$deal_code = $val['DealInfo']['deal_code'];
		$deal_id   = $val['BuyerTransaction']['deal_id'];

		$deal_amount[$deal_code] += $val['BuyerTransaction']['amount'];

		$date[$deal_code][] = $val['BuyerTransaction']['created'];

		if( $val['DealInfo']['status'] == -1 || strtotime($val['DealInfo']['valid_to']) < time() ){
			$status = -1;
		}

		$groupedData[$deal_code]['deal_id'] 		= $deal_id;
		$groupedData[$deal_code]['date'] 			= $date[$deal_code][0];
		$groupedData[$deal_code]['status']			= $status;
		$groupedData[$deal_code]['total_amount'] 	= $deal_amount[$deal_code] ;
		$groupedData[$deal_code]['commission'] 		= $val['BuyerTransaction']['commission_percent'] ;
		$groupedData[$deal_code]['detail'][] 		= $val;
	}
	
 ?>

<style>
  table, tr, td{
     vertical-align: top; 
  }

  .row-active{
  	 background-color: rgb(249, 252, 210) !important;
  	 font-weight: bold !important;
  }

  .sub-row{
  	display: none;
  	cursor: pointer;
  }

  .sub-row:hover{
  	font-weight: bold;
  }

  .sub-row-active{
  	display: table-row !important;
  }

  .sub-row-click{
  	font-weight: bold;
  }

  .expand{
  	cursor: pointer;
  }

  .tran-detail{
  	display: none;
  	background-color: #EEE; 
  	font-size: 12px;
  }

  .tran-detail-active{
  	display: table-row !important;
  }

</style>

<div class="revenue index">
	<div class="row-fluid">		
		<!-- Pie: Box -->
		<div class="span12">
			
			<form action="" method="POST" id="frmBuyerList" >

				<!-- Pie: Top Bar -->
				<div class="top-bar">
					<h3><i class="icon-list"></i> Revenue History</h3>
				</div>
				<!-- / Pie: Top Bar -->

				<!-- Pie: Content -->
				<div class="well" style="min-height:300px;">

				<div class="clearfix"></div>

					<h5 style="float:left; color:#333;"><?php echo $period_string ?> : 
						<span style="color: blue; margin-left:20px;">
							<?php echo ($total_revenue)?"$ " .$this->MyHtml->formatNumber($total_revenue):"$ 0.00" ?>
						</span>
					</h5>

					<a id="btn-search-clear" href="#" class="btn btn-google pull-right" >
						<i class="icon icon-trash"></i> Clear Search
					</a>
					<a id="btn-search-transaction" href="#" class="btn btn-linkedin pull-right" style="margin-right:10px;">
						<i class="icon icon-search"></i> Filter
					</a>

					<div class="clearfix"></div>
					
					<table class="table-list">
						<thead>
							<tr>
								<th width="10px"></th>
								<th width="160px;">Date</th>
								<th width="100px">Deal Code</th>
								<th width="100px">Deal Status</th>
								<th width="150px;">Amount</th>
								<th width="150px;">Commission</th>
								<th width="150px;">Revenue</th>
							</tr>
						</thead>
						<tbody>

						<?php if($groupedData){ 

								$current_date = "";
								$now = date('Y-m-d H:i:s');
								$row_total= 0;
						?>
						<?php foreach ($groupedData as $key => $value ): 

								@$count++;	

								$deal_id 	= $value['deal_id'];
								$deal_code 	= $key;

								$status 		= $value['status'];
								$deal_status 	= "Active";
								$color 			= "red" ;

								if( $status == -1 ){
									$deal_status = "Finished";
									$color 		 = "blue";
								}

								$amount 	= $value['total_amount'];
								$commission = $value['commission'];
								$com_amount	= $amount * $commission / 100 ;

								$revenue 	= $amount - $com_amount;

								$row_total 	+= $revenue;	
						?>
							<tr>
								<td style="text-align:center" class="expand indicator" data-code="<?php echo $key ?>"><strong>+</strong></td>
								<td class="expand" data-code="<?php echo $key ?>"><?php echo date('d-F-Y h:i:s A', strtotime($value['date'])) ?></td>

								<td>
									<a 	href="<?php echo $this->Html->url(array('action' => 'view','controller' => 'deals', $deal_id )); ?>" 
										title="View Deal"
										target="_blank">
										<strong><?php echo $deal_code ?></strong>
									</a>
								</td>
								<td style="color:<?php echo $color ?>">
									<?php echo $deal_status; ?>
								</td>
								<td style="text-align:right">
									<?php echo ($amount)?"$ " .$this->MyHtml->formatNumber($amount):"-" ?>
								<td style="text-align:right">
									<?php echo ($commission)?$this->MyHtml->formatNumber($commission) . "%" :"-" ?>
								</td>
								<td style="text-align:right">
									<?php echo ($revenue)?"$ " .$this->MyHtml->formatNumber($revenue):"-" ?>
								</td>
							</tr>
							<?php 

								$detail = $value['detail'];

								foreach( $detail as $k => $val ):

									$amount 	= $val['BuyerTransaction']['amount'];
									$commission = $val['BuyerTransaction']['commission_percent'];
									$com_amount	= $amount * $commission / 100 ;

									$revenue 	= $amount - $com_amount;

							?>
									<tr class="sub-row sub-row-<?php echo $key ?>" 
										title="View Detail"
										data-id="<?php echo $val['BuyerTransaction']['id'] ?>"
										style="background-color: rgb(244, 252, 231) !important;">
										<td colspan="4" style="padding-left:50px;">
											<?php echo date('d-F-Y h:i:s A', strtotime($val['BuyerTransaction']['created'])) ?>
										</td>
										<td style="text-align:right">
											<?php echo ($amount)?"$ " . $this->MyHtml->formatNumber($amount):"-" ?>
										<td style="text-align:right">
											<?php echo ($commission)?$this->MyHtml->formatNumber($commission) . "%" :"-" ?>
										</td>
										<td style="text-align:right">
											<?php echo ($revenue)?"$ " . $this->MyHtml->formatNumber($revenue):"-" ?>
										</td>
									</tr>

							<?php
										$tranDetail = $val['TransactionDetail'];

										foreach( $tranDetail as $ind => $item ):

											$item_price = $item['unit_price'];
											$qty 		= $item['qty'];

											$amount 	= $item_price * $qty;
							?>
										<tr class="tran-detail tran-<?php echo $val['BuyerTransaction']['id'] ?>" >
											<td colspan="4" style="padding-left: 80px">
												<?php echo "- " . $item['ItemDetail']['title'] ?>
											</td>
											<td style="text-align:left">
												<?php echo ($item_price)?"$ " .$this->MyHtml->formatNumber($item_price):"-" ?>
												<span style="padding: 0 10px;">x</span><?php echo $qty ?>
												<span style="padding: 0 10px;">=</span>
												<span style="float:right">
													<?php echo ($amount)?"$ " .$this->MyHtml->formatNumber($amount):"-" ?>
												</span>
											</td>
											<td colspan="2"></td>
										</tr>

							<?php
									endforeach;
								endforeach;
							 ?>
						<?php endforeach; ?>

						<?php }else{ ?>
							<tr>
								<td colspan="10"><i>No Transaction Found !</i></td>
							</tr>
						<?php } ?>
						</tbody>
					</table>

					<!-- <p class="page">
						<?php
						echo $this->Paginator->counter(array(
						'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total')
						));
						?>	
					</p> -->

					<!-- <div class="paging">
					<?php
						echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
						echo $this->Paginator->numbers(array('separator' => ''));
						echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
					?>
					</div> -->
				</div>
				<!-- / Pie: Content -->

			</form>

		</div>
		<!-- / Pie -->
		
	</div>

</div>


<div id="search-transaction-modal" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel"><i class="icon-search"></i> Search Transaction</h3>
  </div>

  <!-- Any Info-customization -->
  <form action="<?php echo $this->Html->url(array('action' => 'index')); ?>" 
      	style="padding: 20px;" method="POST" id="transactionSearchForm"
      	enctype = "multipart/form-data" >

    <div style="width:100%;">

		<fieldset>

			<div style="display:block; clear:both">
				<label for="BuyerTransactionPurchaseDate" style="float:left; width:100px; padding-top:5px;">Purchase Date</label>
				<div id="dealValidFromPicker" class="input-append date datetimepicker" style="float:left">
	                <input data-format="dd-MM-yyyy" type="text" 
	                    name="DateFrom"
	                    style="width: 135px"
	                    value="<?php echo @$revenueStates['DateFrom'] ?>"
	                    placeholder="From">
	                <span class="add-on">
	                  <i data-time-icon="icon-time" data-date-icon="icon-calendar">
	                  </i>
	                </span>
	            </div>
				<span style="float:left; margin-left:8px; margin-right:7px; padding-top:5px;">TO</span>
				<div id="dealValidFromPicker" class="input-append date datetimepicker" style="float:left">
	                <input data-format="dd-MM-yyyy" type="text" 
	                    name="DateTo"
	                    style="width: 135px"
	                    value="<?php echo @$revenueStates['DateTo'] ?>"
	                    placeholder="To">
	                <span class="add-on">
	                  <i data-time-icon="icon-time" data-date-icon="icon-calendar">
	                  </i>
	                </span>
	            </div>
				
			</div>

		</fieldset>

    </div>

    <div class="submit" style="margin-top: 40px; clear: both;">
      <input class="btn btn-primary" type="submit" value="Submit Search">
      <button type="button" class="btn btn-linkedin reset-btn">Reset</button>
      <button class="btn btn-google cancel" data-dismiss='modal' type="button">Close</button>
    </div>
      
  </form>

</div> 


<script>
	$(document).ready( function(){
		$('.datetimepicker').datetimepicker();
		$('.datetimepicker').find('input').attr('readonly', 'readonly');
		$('.datetimepicker').find('input').css('cursor', 'default');
	})

	$('#clear-search').click( function(){
		$('input#search-text').val("");
		$('select#status').val('all');

		$('form#frmPurchaseTransaction').submit();
	})

	$('#btn-search-transaction').click( function(event){

		event.preventDefault();

		var modal = $('div#search-transaction-modal');

		modal.modal('show');

	});

	$('#btn-search-clear').click( function( event ){

		event.preventDefault();

		var modal = $('div#search-transaction-modal');

		$('.reset-btn').click();
		$('form#transactionSearchForm').submit();
	});

	$('.reset-btn').click( function( event ){

		event.preventDefault();

	 	var modal = $('div#search-transaction-modal');

	 	modal.find('input[type="text"]').val("");
	 	modal.find('select#merchant').val("");

	});

	$('.expand').click( function( event ){

		event.preventDefault();

		var me 		= $(this);
		var tr 		= me.closest('tr');
		var tbody 	= tr.closest('tbody');
		var deal_code = me.attr('data-code');

		tbody.find('.indicator').find('strong').text('+');

		tbody.find('tr').not(tr).removeClass('row-active');
		tr.toggleClass('row-active');

		tbody.find('tr.sub-row').not('tr.sub-row-' + deal_code).removeClass('sub-row-active');
		tbody.find('tr.sub-row-' + deal_code).toggleClass('sub-row-active');

		if( tr.hasClass('row-active') ){
			tr.find('.indicator').find('strong').text('-');
		}

		// Hide Tran Detail
		tbody.find('tr.tran-detail').removeClass('tran-detail-active');
		tbody.find('tr.sub-row').removeClass('sub-row-click');

	})

	$('.sub-row').click( function(event){
		event.preventDefault();

		var me 		= $(this),
			tbody 	= me.closest('tbody'),
			tran_id = me.attr('data-id') ;

		tbody.find('tr.sub-row').not(me).removeClass('sub-row-click');
		me.toggleClass('sub-row-click');

		tbody.find('tr.tran-detail').not(me).removeClass('tran-detail-active');
		tbody.find('tr.tran-'+ tran_id ).toggleClass('tran-detail-active');

	});

</script>