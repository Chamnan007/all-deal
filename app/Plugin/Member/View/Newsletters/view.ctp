<?php 
	/********************************************************************************
	    File: Newsletter Listing
	    Author: PHEA RATTANA
	
	    Confidential ABi Technologies property.
	
	  	Changed History:
	  	Date 					Author				Description
	  	2014/June/04 			PHEA RATTANA		Initial
	*********************************************************************************/

	$recipients = $data['Newsletter']['recipient'];
	$recipient = "";
	if( !empty($recipients) ){
		$recipients = json_decode($recipients);

		if( in_array("all", $recipients)){
			
			$recipient = 'All Businesses';

		}else{

			$count_email = count($recipients);

			foreach( $recipients as $k => $v ){
				$recipient .= " <b>;</b> " . $v;
			}

			$other = json_decode($data['Newsletter']['other_recipient']) ;

			$count_email += count($other);

			foreach($other as $k => $v ){
				$recipient .= " <b>;</b> " . $v;
			}

			$recipient = trim($recipient,  " <b>;</b> " );
		}
	}

 ?>

<style>
	
	table, tr, td{
		vertical-align: top;
	}

	p{
		margin-top: 0px !important;
	}

	img{
		max-width: 1000px !important;
		max-height: 600px !important;
		padding: 2px;
		border: 1px solid #CCC;
	}

</style>

<div class="newsletters index">
	<div class="row-fluid">		
		<!-- Pie: Box -->
		<div class="span12">

			<!-- Pie: Top Bar -->
			<div class="top-bar">
				<h3><i class="icon-list"></i> Newsletter</h3>
			</div>
			<!-- / Pie: Top Bar -->

			<!-- Pie: Content -->
			<div class="well">

				<a href="<?php echo $this->Html->url( array('action' => 'index')); ?>"> 
					<button class="btn btn-linkedin" type="button"><i class="icon-list"></i> Sent Newsletters List</button>
				</a>
				

				<div class="clearfix"></div>

				<div class="span11">
					<table width="100%">
						<tr>
							<td width="100px"><b>Subject </b></td>
							<td width="20px"> : </td>
							<td><strong><?php echo $data['Newsletter']['subject'] ?></strong></td>
						</tr>

						<tr>
							<td width="100px"><b>Sender </b></td>
							<td> : </td>
							<td><?php echo $data['Newsletter']['sender'] ?></td>
						</tr>	
					
						<tr>
							<td width="120px"><b>Recipients </b></td>
							<td> : </td>
							<td>
								<strong>Total of : <b><?php echo $count_email ?></b> emails. </strong><br>
								<?php echo $recipient ?>
							</td>
						</tr>
					
						<tr>
							<td width="120px"><b>Sent By </b></td>
							<td> : </td>
							<td><?php echo $data['SentBy']['last_name'] . " " . $data['SentBy']['first_name'] ?></td>
						</tr>

						<tr>
							<td width="120px"><b>Sent Date </b></td>
							<td> : </td>
							<td style="color:#06F"><?php echo date("d-F-Y H:i:s A", strtotime($data['Newsletter']['created']) ) ?></td>
						</tr>

					</table>
					<br>
					<h6>Message Content : </h6>
					
					<div style="padding-left:20px;">
						<?php echo $data['Newsletter']['message'] ?>	
					</div>
					
				</div>
			
			</div>
			<!-- / Pie: Content -->

		</div>
		<!-- / Pie -->
		
	</div>

</div>

<script>


</script>


