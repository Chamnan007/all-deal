<?php 
  /********************************************************************************
      File: Newsletter Listing
      Author: PHEA RATTANA
  
      Confidential ABi Technologies property.
  
      Changed History:
      Date          Author        Description
      2014/June/04      PHEA RATTANA    Initial
  *********************************************************************************/

  // var_dump($email);

  $total_email = count($email);

?>

<style>

  .control-group{
    background-image: none !important;
    border-bottom: 1px solid #e5e5e5 !important;
    box-shadow: none !important;
  }

</style>

<div class="newsletters index">
  <div class="row-fluid">   
    <!-- Pie: Box -->
    <div class="span12">

      <!-- Pie: Top Bar -->
      <div class="top-bar">
        <h3><i class="icon-list"></i> Compose Newsletter</h3>
      </div>
      <!-- / Pie: Top Bar -->

      <!-- Pie: Content -->
      <div class="well">

        <a href="<?php echo $this->Html->url( array('action' => 'index')); ?>"> 
          <button class="btn btn-linkedin" type="button"><i class="icon-list-alt"></i> Sent Newsletters List</button>
        </a>    
        
        <div class="clearfix"></div>

        <?php if(empty($configure)){ ?>

          <div class="span10">
            <h5>You haven't had email configuration yet.</h5>
            <?php if( $user_data['access_level'] == 1 ){ ?>

            <h5>Go to <b>'Newsletter' &rarr; 'Configuration'</b> to make configuration.</h5>

            <h5>Or click here : 
              <a href="<?php echo $this->Html->url(array('action'=>'config')) ?>">
                <i class="icon-cog"></i> 
                Configuration
              </a>
            </h5>
            <?php }else{ ?>

            <h5>Contact your Adminsitrator for this configuration.</h5>
            <?php } ?>
          </div>

        <?php }else{ ?>

        <?php echo $this->Form->create('Newsletter'); ?>

        <form   action="<?php echo $this->Html->url(array('action' => 'compose')) ?>" 
            id="NewsletterComposeForm" method="post" accept-charset="utf-8">
        
          <div class="span10">
            <legend style="margin-bottom: 0px;">Compose New Newsletter</legend>

            <div class="control-group">
              <label class="control-label" for="inputNormal"><i class="icon-user"></i>Sender</label>
              <div class="controls">
                <span><b><?php echo $configure['MailConfigure']['user_name'] ?></b></span>
                <input type="hidden" name="data[Newsletter][sender]" value="<?php echo $configure['MailConfigure']['user_name'] ?>" >
              </div>
            </div>

            <div class="control-group" id="recipient">
              <label class="control-label" for="inputNormal"><i class="icon-user"></i>Recipients</label>
              <div class="controls">
                <span style="margin-right:15px;"><b  id="total_email"><?php echo $total_email ?></b> mails will be sent.</span>
                <a href="#select_recipient" data-toggle="modal" style="color: blue">Select Recipients</a>
                <input type="hidden" id="recipient_value" name="data[Newsletter][recipient]" value='["all_biz", "all_sub"]'>
              </div>
            </div>

            <div class="control-group">
              <label class="control-label" for="other_recipient"><i class="icon-envelope-alt"></i>Other Recipients</label>
              <div class="controls">
                <input type="text" id="other_recipient" placeholder="Other Recipients" class="input-block-level"
                    name="data[Newsletter][other_recipient]" >
                <i>Note : Separate each email by semicolon (;)</i>
              </div>
            </div>

            <div class="control-group">
              <label class="control-label" for="subject"><i class="icon-envelope-alt"></i>Subject</label>
              <div class="controls">
                <input type="text" id="subject" placeholder="Subject" class="input-block-level"
                    name="data[Newsletter][subject]" maxlength="1000"
                    required="required" >
              </div>
            </div>

            <div class="control-group">
              <label class="control-label" for=""><i class="icon-picture"></i> Add Image</label>
              <div class="controls">
                <a href="#select_images" data-toggle="modal" style="color: blue">Upload Images</a>
              </div>
            </div>

            <div class="control-group">
              <label class="control-label" for="message"><i class="icon-envelope-alt"></i>Message</label>
              <div class="controls">
                <textarea   name="data[Newsletter][message]" 
                      id="message" cols="30" rows="10" class="ckeditor"
                      maxlength="65535"></textarea>
              </div>
            </div>

            <div class="control-group">
              <label class="control-label" for="signature"><i class="icon-edit"></i>Mail Signature</label>
              <div class="controls">
                <input type="checkbox" id="signature" name="signature" checked="checked">
              </div>
            </div>

            <div class="submit" style="margin-top: 20px; clear: both; float:right; padding-right:15px;">
              <a class="btn btn-google cancel" href="<?php echo $this->Html->url( array('action' => 'index')); ?>">Cancel</a>
              <input class="btn btn-primary" type="submit" value="Send" id="send">
            </div>  

          </div>

        </form>

        <?php } ?>

      </div>
    <!-- / Pie -->

    </div>
    
  </div>

</div>

<div id="select_recipient" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true"
  style="padding-bottom: 10px !important;" >

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 id="myModalLabel"><i class="icon-user"></i> Select Recipients</h4>
  </div>
  
  <div class="span6" style="padding:20px;">

    <div class="input checkbox" style="width:85%;">
            <input  style="float:left; margin-right: 6px;" type="checkbox" checked="checked" id='all_biz' value="all_biz/<?php echo count($biz_email) ?>">
            <label for="all_biz">All Businesses</label>
        </div>

        <div class="input checkbox" style="width:85%;">
            <input  style="float:left; margin-right: 6px;" type="checkbox" checked="checked" id='all_sub' value="all_sub/<?php echo count($sub_email) ?>">
            <label for="all_sub">All Subscribers</label>
        </div>

  </div>

  <div class="submit" style="margin-top: 40px; clear: both; margin-left:20px; margin-bottom: 20px;">
    <button class="btn btn-google cancel" data-dismiss='modal' type="button" id="cancel">Cancel</button>
    <button class="btn btn-primary cancel" data-dismiss='modal' type="button" id="select">Select</button>
  </div>

</div>


<div id="select_images" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true"
  style="padding-bottom: 10px !important;">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 id="myModalLabel"><i class="icon-picture"></i> Add Image</h4>
  </div>
  
  <div class="span6" style="padding:20px;">
    <div id="upload-wrapper">

      <form   action="<?php echo $this->Html->url(array('action'=> 'upload_images')) ?>" 
          onSubmit="return false" 
          method="post" 
          enctype="multipart/form-data" id="MyUploadForm">
        <input name="ImageFile" id="imageInput" type="file" required="required"/>
        <input type="submit"  id="submit-btn" value="Upload" />
        <img src="<?php echo $this->webroot; ?>img/ajax-loader.gif" id="loading-img" style="display:none;" alt="Please Wait"/>
      </form>

      <div id="progressbox" style="display:none;">
        <div id="progressbar"></div >
        <div id="statustxt">0%</div>
      </div>

      <div id="output"></div>
    </div>
  </div>

</div>

<div id="remove_images" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true"
  style="padding-bottom: 10px !important;">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h4 id="myModalLabel"><i class="icon-question"></i> Remove Attached Images</h4>
  </div>
  
  <div class="span6" style="padding:20px;">
    <div id="upload-wrapper">
      <h5>Are you sure you want to remove this image?</h5>
    </div>
  </div>

  <div class="submit" style="margin-top: 40px; clear: both; margin-left:20px; margin-bottom: 20px;">
    <button class="btn btn-google cancel" data-dismiss='modal' type="button" id="cancel">No</button>
    <button class="btn btn-primary cancel" data-dismiss='modal' type="button" id="yes">Yes</button>
  </div>

</div>



<style>

    .loading-wrapper{
        display: none;
        position: absolute;
        top: 0px;
        left: 0px;
        background-color: rgba(0,0,0,0.5);
        z-index: 999;
        width: 100%;
        height: 100%;
        overflow-x: none;
    }

    .loading{
        display: block;
        position: fixed;
        z-index: 9999;        

        left: 50%;
        top: 50%;

        width: 500px;
        height: 350px;

        margin-left: -20%;
        margin-top: -15%;

        /*border: 1px solid red;*/
    }

    .loading img{
        width: 100%;
        height: 100%;
    }



</style>

<div class="loading-wrapper">
    <div class="loading">
        <img src="<?php echo Router::url("/img/sending.gif"); ?>" alt="">
    </div>
</div>

<script>

  $("#NewsletterComposeForm").submit( function (){
        $('.loading-wrapper').show();
        // return false;
  })

  $("#select").click( function() {
    var me = $(this);

    var ckbox = me.parent().parent().find("input:checked");
    var recipient = new Array();

    var total = 0;

    if( ckbox.length <= 0 ){
      alert('Please select at least one option !!!'); 
      return false;
    }else{
      ckbox.each( function(ind, val){

        var value = ckbox[ind].value;

        var res = value.split("/");
        
        total += parseInt(res[1]);

        recipient.push(res[0]);
      });
    }

    $("b#total_email").text(total);

    var js_arr = JSON.stringify(recipient); 

    $('#recipient_value').val(js_arr);

  })

  // $("#cancel").click( function(){

  //  var me = $(this);
  //  var ckbox = me.parent().parent().find("input:checked");

  // })

$(document).ready(function() { 

  var progressbox     = $('#progressbox');
  var progressbar     = $('#progressbar');
  var statustxt       = $('#statustxt');
  var completed       = '0%';
  
  var options = { 
      // target:   '#output1',   // target element(s) to be updated with server response 
      beforeSubmit:  beforeSubmit,  // pre-submit callback 
      uploadProgress: OnProgress,
      success:       afterSuccess,  // post-submit callback 
      resetForm: true        // reset the form after successful submit 
    }; 
    
   $('#MyUploadForm').submit(function() { 
    $(this).ajaxSubmit(options);
    return false; 
  });
  
  //when upload progresses  
  function OnProgress(event, position, total, percentComplete) {
    //Progress bar
    progressbar.width(percentComplete + '%') //update progressbar percent complete
    statustxt.html(percentComplete + '%'); //update status text

    if(percentComplete>50) {
      statustxt.css('color','#fff'); //change status text to white after 50%
    }

  }

  //after succesful upload
  function afterSuccess(data){

    $('#submit-btn').show();  //hide submit button
    $('#loading-img').hide(); //hide submit button

    $(".close").click();
    progressbox.hide()

    var id = data.split('/').pop(-1) ;

    id = id.substring(0, id.indexOf(".") );

    var imgUrl = "<?php echo Router::url('/', true) ?>" + data ;

      if(typeof(imgUrl)=='undefined' ||imgUrl.length==0)return;
      
      var imgHtml = CKEDITOR.dom.element.createFromHtml("<img src='" + imgUrl + "' alt='' align='left'/>");
      CKEDITOR.instances.message.insertElement(imgHtml);

    // $(".action-delete").click( function() {
    //  $("div#remove_images").find($('#yes')).attr('data-img', data );
    //  $("div#remove_images").find($('#yes')).attr('data-id', id );
    // });
  }

  //function to check file size before uploading.
  function beforeSubmit() {

     //check whether browser fully supports all File API
     if (window.File && window.FileReader && window.FileList && window.Blob)
    {

      if( !$('#imageInput').val()) //check empty input filed
      {
        $("#output").html("Please select images before uploading...!");
        return false
      }
      
      var fsize = $('#imageInput')[0].files[0].size; //get file size
      var ftype = $('#imageInput')[0].files[0].type; // get file type
      
      //allow only valid image file types 
      switch(ftype)
          {
              case 'image/png': case 'image/gif': case 'image/jpeg': case 'image/pjpeg':
                  break;
              default:
                  $("#output").html("<b>"+ftype+"</b> Unsupported file type!");
          return false
          }
      
      //Allowed file size is less than 1 MB (1048576)
      if(fsize>1048576) {

        $("#output").html("<b>"+bytesToSize(fsize) +"</b> Too big Image file! <br />Please reduce the size of your photo by using an image editor.");
        return false;

      }
      
      //Progress bar
      progressbox.show(); //show progressbar
      progressbar.width(completed); //initial value 0% of progressbar
      statustxt.html(completed); //set status text
      statustxt.css('color','#000'); //initial color of status text

          
      $('#submit-btn').hide(); //hide submit button
      $('#loading-img').show(); //hide submit button
      $("#output").html("");


    }
    else
    {
      //Output error to older unsupported browsers that doesn't support HTML5 File API
      $("#output").html("Please upgrade your browser, because your current browser lacks some new features we need!");
      return false;
    }
  }

  //function to format bites bit.ly/19yoIPO
  function bytesToSize(bytes) {
     var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
     if (bytes == 0) return '0 Bytes';
     var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
     return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
  }

  $("#yes").click( function (){
    var me = $(this);
    var img = me.data('img');

    var img = img.split('/').pop(-1) ;

    var url = '<?php echo $this->webroot ?>' + 'administrator/newsletters/removeImage/' + img ;

    $.ajax({
            type: 'POST',
            url: url,
            data: {},
            dataType: 'json',
            
            success: function (data){
                if( data.status == true ){
                  var id = img;

                  $("#" + me.data('id') ).fadeOut() ;
                }
            },

            error: function ( err, msg ){
              console.log(msg);
            }

        });

  })

}); 

  

</script>


