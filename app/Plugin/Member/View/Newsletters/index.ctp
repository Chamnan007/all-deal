<?php 
	/********************************************************************************
	    File: Newsletter Listing
	    Author: PHEA RATTANA
	
	    Confidential ABi Technologies property.
	
	  	Changed History:
	  	Date 					Author				Description
	  	2014/June/04 			PHEA RATTANA		Initial
	*********************************************************************************/

	// var_dump($data);	
	
 ?>

<style>
	
	table, tr, td{
		vertical-align: top;
	}

</style>

<div class="newsletters index">
	<div class="row-fluid">		
		<!-- Pie: Box -->
		<div class="span12">

			<!-- Pie: Top Bar -->
			<div class="top-bar">
				<h3><i class="icon-list"></i> Newsletter</h3>
			</div>
			<!-- / Pie: Top Bar -->

			<!-- Pie: Content -->
			<div class="well">

			<a href="<?php echo $this->Html->url( array('action' => 'compose')); ?>"> 
				<button class="btn btn-linkedin" type="button"><i class="icon-plus"></i> Compose New Newsletter</button>
			</a>
			

			<div class="clearfix"></div>
				
				<table class="table-list">
					<thead>
						<tr>
							<!--th><?php echo $this->Paginator->sort('id'); ?></th-->
							<th width="200px;"><?php echo $this->Paginator->sort('created', "Send Date" ); ?></th>
							<th><?php echo $this->Paginator->sort('subject'); ?></th>
							<th width="200px;"><?php echo __("Sent By") ; ?></th>
							<th width="100px;" class="actions"><?php echo __('Actions'); ?></th>
						</tr>
					</thead>
					<tbody>
						<?php foreach( $data as $newsletter ): ?>
							<tr>
								<td><?php echo date( 'd-M-Y H:i:s A' , strtotime($newsletter['Newsletter']['created']) ) ?></td>
								<td><?php echo $newsletter['Newsletter']['subject'] ?></td>
								<td><?php echo urldecode($newsletter['SentBy']['last_name'] . " " . $newsletter['SentBy']['first_name'] ) ?></td>
								<td class="actions" >
									<a href="<?php echo $this->Html->url(array('action' => 'view', $newsletter['Newsletter']['id'])); ?>"> 
										<button class="btn btn-foursquare" type="button">Detail</button>
									</a>
								</td>
							</tr>
						<?php endforeach; ?>
					</tbody>
				</table>

				<p class="page">
					<?php
					echo $this->Paginator->counter(array(
					'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total')
					));
					?>	
				</p>

				<div class="paging">
				<?php
					echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
					echo $this->Paginator->numbers(array('separator' => ''));
					echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
				?>
				</div>
			</div>
			<!-- / Pie: Content -->

		</div>
		<!-- / Pie -->
		
	</div>

</div>

<script>


</script>


