<?php 
	/********************************************************************************
	    File: Add User
	    Author: PHEA RATTANA
	
	    Confidential ABi Technologies property.
	
	  	Changed History:
	  	Date 					Author				Description
	  	2014/01/05 				PHEA RATTANA		Initial
	  	2015/May/21 			RATTANA 			Changed Word Using
	*********************************************************************************/

	$pro_name = array();
	if( !empty($provinces) ){
		foreach( $provinces as $key => $value ){
			$pro_name[$value['Province']['province_code']] = $value['Province']['province_code'] . " - " . $value['Province']['province_name'];
			// $pro_code[] = $value['Province']['province_code'];
		}
	}


	$city_name = array();
	if( !empty($cities) ){
		foreach( $cities as $key => $value ){
			$city_name[$value['City']['city_code']] = $value['City']['city_code'] . " - " . $value['City']['city_name'];
		}
	}

	$status_arr = array();
	foreach ($status as $key => $value) {
		$status_arr[$key] = $value['status'];
	}


	// var_dump($cities);

?>

<div class="users form">
	<div class="row-fluid">		
		<!-- Pie: Box -->
		<div class="span12">

			<!-- Pie: Top Bar -->
			<div class="top-bar">
				<h3><i class="icon-list"></i> Manage Merchant Members</h3>
			</div>
			<!-- / Pie: Top Bar -->

			<!-- Pie: Content -->
			<div class="well">

			<a href="<?php echo $this->Html->url( array('action' => 'index')); ?>"> 
				<button class="btn btn-linkedin" type="button"><i class="icon-list-alt"></i> Members List</button>
			</a>


			<a href="<?php echo $this->Html->url( array('action' => 'view', $this->request->data['MemberUser']['id'])); ?>"> 
				<button class="btn btn-skype" type="button"><i class="icon-list-alt"></i> Detail Information</button>
			</a>
			

			<div class="clearfix"></div>
				
				<?php echo $this->Form->create('MemberUser', array('data-validate'=>'parsley') ); ?>
					<fieldset>

						<legend><?php echo __('Edit Member Information'); ?></legend>

						<div class="span4">
							<?php
								echo $this->Form->input('id');
								// echo $this->Form->input('user_id');
								echo $this->Form->input('first_name', array('placeholder' => 'First Name'));
								echo $this->Form->input('last_name', array('placeholder' => 'Last Name'));
								// echo $this->Form->input('gender');

								echo $this->Form->input(	'gender' ,array(
														   'type' 		=> 'select',
														   'label' 		=> 'Gender',
														   // 'values'		=> $pro_code,
														   'options'	=> $gender
														));

								echo $this->Form->input('dob', array(	'style'=>'width:70px;',
													'label' => 'Date of Birth', 'dateFormat' => 'DMY',
													'minYear' => date('Y') - 90,
													'maxYear' => date('Y')) );
							?>	
						</div>

						<div class="span4">
							<?php 
								echo $this->Form->input(	'province_code' ,array(
														   'type' 		=> 'select',
														   'label' 		=> 'Province',
														   'empty' => 'Select a province',
														   'options'	=> $pro_name
														));

								
								echo $this->Form->input(	'city_code' ,array(
														   'type' 		=> 'select',
														   'label' 		=> 'City',
														   'empty' 		=> 'Select a city',
														   // 'default'	=> $this->request->data['MemberUser']['city_code'],
														   'options'	=> $city_name
														));

								echo $this->Form->input('street', array('placeholder' => 'Street'));


								echo $this->Form->input('position', array('placeholder' => 'Position'));
							 ?>
						</div>

						<div class="span3">
							<?php

								echo $this->Form->input('email', array('placeholder' => 'Email', 'type' => 'email', 'required' => 'required','maxlength' => 50 ));

								echo $this->Form->input('phone', array('placeholder' => 'Phone Number'));
								

								echo $this->Form->input( 	'access_level' ,array(
														    'type' 		=> 'select',
														    'label' 	=> 'Access Level',
														    'options'	=> $access_level
														));

								echo $this->Form->input( 	'status' ,array(
														    'type' 		=> 'select',
														    'label' 	=> 'Status',
														    'options'	=> $status_arr
														));
								
							?>	
						</div>
	
						<div class='clearfix span4' style="padding-top: 30px;">
							<?php 
								echo $this->Form->button(__('Save'), array('class' => 'btn btn-primary', 'id' => 'save', 'type' => 'button') ); 
							 ?>
						</div>	
						
					</fieldset>
			

			</div>
			<!-- / Pie: Content -->

		</div>
		<!-- / Pie -->
		
	</div>

</div>

<style>


	.equalto, .required, .type {
		color: red;
		font-size: 12px !important;
		list-style-type: none;
	}

	.parsley-error-list{
		margin: 0px !important;
		padding: 0px !important;
	}

</style>

<div id="message" class="modal hide fade" tabindex="-1" 
	role="dialog" aria-labelledby="myModalLabel" 
	aria-hidden="true"
	style="">

	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="myModalLabel"><i class="icon-exclamation"></i> Warning !</h3>
	</div>

	<div class="span6" style="padding-top: 10px; padding-bottom: 20px;">
		
		<h5 id="message"></h5>

		<button class="btn btn-google cancel" data-dismiss='modal' type="button">Close</button>
	</div>

</div> 

<a href="#message" data-toggle="modal" id="message"></a>



<div id="pass-alert" class="modal hide fade" tabindex="-1" 
	role="dialog" aria-labelledby="myModalLabel" 
	aria-hidden="true"
	style="">

	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="myModalLabel"><i class="icon-exclamation"></i> Warning !</h3>
	</div>

	<div class="span6" style="padding-top: 10px; padding-bottom: 20px;">
		
		<h5 id="pass-alert"></h5>

		<button class="btn btn-google cancel" data-dismiss='modal' type="button">Close</button>
	</div>

</div> 

<a href="#pass-alert" data-toggle="modal" id="pass-alert"></a>


<script type="text/javascript">

    $(document).ready(function(){

    	var checkIEBrowser = msieversion();
	
		function msieversion() {

	            var ua = window.navigator.userAgent;
	            var msie = ua.indexOf("MSIE ");
	            var ieversion;

	            if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer, return version number
	                return parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)));
	            else                 // If another browser, return 0
	                return false;
	    }

    	$("#UserConfirmPassword").blur( function (){
	       	return $(this).parsley( 'validate');
    	});


    	$("#MemberUserProvinceCode").change( function (){
			var me = $(this);

			var citySelect = $("#MemberUserCityCode");

			var pro_code = me.val();

			var url = '<?php echo $this->webroot ?>' + 'member/MemberUsers/get_city_by_province/' + pro_code ;

			if( pro_code != '' ){
				$.ajax({
		            type: 'POST',
		            url: url,
		            data: { pro_code : pro_code },
		            dataType: 'json',
		            
		            beforeSend: function(){
		                citySelect.html('<option>Loading...</option>');
		                // console.log("Loading...");
		            },
		            success: function (data){
		               	// console.log('success');

		               	var data = data.data;

		               	citySelect.html("<option value=''>select a city</option>")
		               	$.each( data, function (ind, val ){
		               		var city = val.City;

		               		citySelect.append("<option value='" + city.city_code + "'>" + city.city_code + " - " + city.city_name + "</option>" )
		               	});

		               	citySelect.removeClass('chzn-done').show().next().remove();
	             		citySelect.chosen();

		            },

		            error: function ( err ){
		            	// console.log(err);
		            }

		        });

			}else{
				citySelect.parent().find('span').text('select an option');
				citySelect.html("<option value=''>select an option</option>")
			}

		});

		function validateEmail(){


			var email = $("#MemberUserEmail").val();
			var id = $("#MemberUserId").val();

			var result = true;

			var url = '<?php echo $this->webroot ?>' + 'member/MemberUsers/checkDuplicateEmail/' + email + "/" + id ;

			$.ajax({
		            type: 'POST',
		            url: url,
		            data: { email : email, id : id },
		            dataType: 'json',
		            async: false,
		            
		            success: function (data){
		               	// console.log(data);

		               	if( data.status == 1 ){
		               		result = true;
		               	}else{
		               		result = false;
		               	}
		            },

		            error: function ( err ){
		            	// console.log(err);
		            }

		        });

			return result;
		}


		$("#save").click( function(){

			if( checkIEBrowser ){
				var fName = $('#MemberUserFirstName').val();
				var lName = $('#MemberUserLastName').val();
				var province = $('select#MemberUserProvinceCode').val();
				var city = $('select#MemberUserCityCode').val();
				var phone = $('#MemberUserPhone').val();
				var email = $('#MemberUserEmail').val();

				if( fName == "" || lName == "" ){ alert("Please enter both First Name and Last Name."); return false; }
				if( province == "" || city == "" ){ alert("Please Province and City."); return false; }
				if( phone == "" ){ alert("Please enter Phone number."); return false; }
				if( email == "" ){ alert("Please enter email."); return false; }
			}

			if(validateEmail()){
				$("#MemberUserEditForm").submit();
			}else{
				$("h5#message").text("This email has already been registered. Please try another email.");
				$("a#message").click();
				return false;	
			}
			return false;
		})


	});


</script>