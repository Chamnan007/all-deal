<?php 
	/********************************************************************************
	    File: Add User
	    Author: PHEA RATTANA
	
	    Confidential ABi Technologies property.
	
	  	Changed History:
	  	Date 					Author				Description
	  	2014/01/05 				PHEA RATTANA		Initial
	  	2015/May/21 			RATTANA 			Changed Word Using
	*********************************************************************************/

	$pro_name = array();

	if( !empty($provinces) ){
		foreach( $provinces as $key => $value ){
			$pro_name[$value['Province']['province_code']] = $value['Province']['province_code'] . " - " . $value['Province']['province_name'];
			// $pro_code[] = $value['Province']['province_code'];
		}
	}


	$status_arr = array();

	foreach ($status as $key => $value) {
		$status_arr[$key] = $value['status'];
	}


?>

<div class="users form">
	<div class="row-fluid">		
		<!-- Pie: Box -->
		<div class="span12">

			<!-- Pie: Top Bar -->
			<div class="top-bar">
				<h3><i class="icon-list"></i> Manage Merchant Members</h3>
			</div>
			<!-- / Pie: Top Bar -->

			<!-- Pie: Content -->
			<div class="well">

			<a href="<?php echo $this->Html->url( array('action' => 'index')); ?>"> 
				<button class="btn btn-linkedin" type="button"><i class="icon-list-alt"></i> Members List</button>
			</a>
			

			<div class="clearfix"></div>
				
				<?php echo $this->Form->create('MemberUser', array('data-validate'=>'parsley') ); ?>
					<fieldset style="margin-bottom: 50px;">

						<legend><?php echo __('Create New User'); ?></legend>

						<div class="span4">
							<?php
							
								echo $this->Form->input('first_name', array('placeholder' => 'First Name'));
								echo $this->Form->input('last_name', array('placeholder' => 'Last Name'));


								echo $this->Form->input(   'gender' ,array(
														   'type' 		=> 'select',
														   'label' 		=> 'Gender',
														   'options'	=> $gender
														));


								echo $this->Form->input(	'dob', array(	
															'style'		=>'width:70px;',
											   				'id'		=> 'MemberUserDob','dateFormat' => 'DMY',
											   				'name'		=> "MemberUser[dob][]",
											   				'type'		=> 'date',
											   				'label'		=> 'Date of Birth',
															'minYear' 	=> date('Y') - 90,
															'maxYear' 	=> date('Y')) );
							?>	
						</div>

						<div class="span4">

							<?php 

								echo $this->Form->input(	'province_code' ,array(
														   'type' 		=> 'select',
														   'label' 		=> 'Province',
														   'empty' => 'Select a province',
														   'required' => true,
														   'options'	=> $pro_name
														));

								
								echo $this->Form->input(	'city_code' ,array(
														   'type' 		=> 'select',
														   'label' 		=> 'City',
														   'empty'		=> 'Select a province',
														   'required' => true,
														   'options'	=> array()
														));

								echo $this->Form->input('street', array('placeholder' => 'Street'));



								echo $this->Form->input( 	'access_level' ,array(
														    'type' 		=> 'hidden',
														    'value' 	=> 2 ,
														));

								echo $this->Form->input('position', array('placeholder' => 'Position'));

							 ?>
						</div>

						<div class="span3">
							<?php


								echo $this->Form->input('phone', array('placeholder' => 'Phone Number'));
								echo $this->Form->input('email', array('placeholder' => 'Email', 'type' => 'email', 'required' => 'required' ));
								echo $this->Form->input('password', array('placeholder' => 'Password', 'required' => 'required', 'maxlength' => 30 ));
								echo $this->Form->input('confirm_password', array('placeholder' => 'Confirm Password', 
														'type'=>'password', 'data-equalto' => '#MemberUserPassword', 'required' => 'required'));


								echo $this->Form->input( 	'status' ,array(
														    'type' 		=> 'hidden',
														    'label' 	=> 'Status',
														    'value'	=> 1
														));
								
							?>	
						</div>

						<!-- <legend><?php echo __('Permission'); ?></legend> -->


	
						<div class='clearfix span4'>
							
							<?php 
								echo $this->Form->button(__('Save'), array('class' => 'btn btn-primary', 'id' => 'save', 'type' => 'button') ); 
							?>


						</div>	
						
					</fieldset>
			

			</div>
			<!-- / Pie: Content -->

		</div>
		<!-- / Pie -->
		
	</div>

</div>

<div id="message" class="modal hide fade" tabindex="-1" 
	role="dialog" aria-labelledby="myModalLabel" 
	aria-hidden="true"
	style="">

	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="myModalLabel"><i class="icon-exclamation"></i> Warning !</h3>
	</div>

	<div class="span6" style="padding-top: 10px; padding-bottom: 20px;">
		
		<h5 id="message"></h5>

		<button class="btn btn-google cancel" data-dismiss='modal' type="button">Close</button>
	</div>

</div> 

<a href="#message" data-toggle="modal" id="message"></a>



<div id="pass-alert" class="modal hide fade" tabindex="-1" 
	role="dialog" aria-labelledby="myModalLabel" 
	aria-hidden="true"
	style="">

	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="myModalLabel"><i class="icon-exclamation"></i> Warning !</h3>
	</div>

	<div class="span6" style="padding-top: 10px; padding-bottom: 20px;">
		
		<h5 id="pass-alert"></h5>

		<button class="btn btn-google cancel" data-dismiss='modal' type="button">Close</button>
	</div>

</div> 

<a href="#pass-alert" data-toggle="modal" id="pass-alert"></a>

<style>

	.equalto, .required, .type{
		color: red;
		font-size: 12px !important;
		list-style-type: none;
	}

	.parsley-error-list{
		margin: 0px !important;
		padding: 0px !important;
	}

</style>


<script type="text/javascript">

    $(document).ready(function(){


		var checkIEBrowser = msieversion();
	
		function msieversion() {

	            var ua = window.navigator.userAgent;
	            var msie = ua.indexOf("MSIE ");
	            var ieversion;

	            if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer, return version number
	                return parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)));
	            else                 // If another browser, return 0
	                return false;
	    }


    	$("#MemberUserConfirmPassword").blur( function (){
	       	return $(this).parsley( 'validate');
    	});

    	$("#MemberUserProvinceCode").change( function (){
			var me = $(this);

			var citySelect = $("#MemberUserCityCode");

			var pro_code = me.val();

			var url = '<?php echo $this->webroot ?>' + 'member/MemberUsers/get_city_by_province/' + pro_code ;

			if( pro_code != '' ){
				$.ajax({
		            type: 'POST',
		            url: url,
		            data: { pro_code : pro_code },
		            dataType: 'json',
		            
		            beforeSend: function(){
		                citySelect.html('<option>Loading...</option>');
		            },

		            success: function (data){
		               	// console.log('success');

		               	var data = data.data;

		               	citySelect.html("<option value=''>select a city</option>")
		               	$.each( data, function (ind, val ){
		               		var city = val.City;

		               		citySelect.append("<option value='" + city.city_code + "'>" + city.city_code + " - " + city.city_name + "</option>" )
		               	});

		            },

		            error: function ( err ){
		            	// console.log(err);
		            }

		        });

			}else{

				citySelect.html("<option value=''>select an option</option>");

			}

		});
	

		function validateEmail(){

			var email = $("#MemberUserEmail").val();
			var id = null;

			var result = true;

			var url = '<?php echo $this->webroot ?>' + 'member/MemberUsers/checkDuplicateEmail/' + email + "/" + id  ;

    		$.ajax({
	            type: 'POST',
	            url: url,
	            data: { email: email, id:id },
	            async: false,
	            dataType: 'json',

	           	success: function (data){
		               	// console.log(data);

		               	if( data.status == 1 ){
		               		result = true;
		               	}else{
		               		result = false;
		               	}
		            },

	            error: function ( err ){
	            	// console.log(err);
	            }

	        });

    		return result;   
		};


		$("#save").click( function(){

			if( checkIEBrowser ){
				var fName = $('#MemberUserFirstName').val();
				var lName = $('#MemberUserLastName').val();
				var province = $('select#MemberUserProvinceCode').val();
				var city = $('select#MemberUserCityCode').val();
				var phone = $('#MemberUserPhone').val();
				var email = $('#MemberUserEmail').val();
				var pass = $('#MemberUserPassword').val();
				var cnfpass = $('#MemberUserConfirmPassword').val();


				if( fName == "" || lName == "" ){ alert("Please enter both First Name and Last Name."); return false; }
				if( province == "" || city == "" ){ alert("Please Province and City."); return false; }
				if( phone == "" ){ alert("Please enter Phone number."); return false; }
				if( email == "" ){ alert("Please enter email."); return false; }
				if( pass == "" || cnfpass == "" ){ alert("Please enter password and confirm password."); return false; }
			}

			if(validateEmail()){

				var pass = $("#MemberUserPassword").val().length;

				if( pass > 0 && pass < 6 ){
					$("h5#pass-alert").text("Password must be at least 6 characters.");
					$("a#pass-alert").click();
					return false;
				}
				$("#MemberUserAddForm").submit();
			}else{
				$("h5#message").text("This email has already been registered. Please try another email.");
				$("a#message").click();
				return false;	
			}
			return false;
		})


	});


</script>