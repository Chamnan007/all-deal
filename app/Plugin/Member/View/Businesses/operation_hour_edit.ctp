<?php 
	if( !empty($operationHour) ){

		$monday 	= $operationHour[0]['OperationHour'] ;
		$tuesday 	= $operationHour[1]['OperationHour'] ;
		$wednesday 	= $operationHour[2]['OperationHour'] ;
		$thursday 	= $operationHour[3]['OperationHour'] ;
		$friday 	= $operationHour[4]['OperationHour'] ;
		$saturday 	= $operationHour[5]['OperationHour'] ;
		$sunday 	= $operationHour[6]['OperationHour'] ; 

	}
?>

<div class="businesses form">
	<div class="row-fluid">		
		<!-- Pie: Box -->
		<div class="span12">

			<!-- Pie: Top Bar -->
			<div class="top-bar">
				<h3><i class="icon-list"></i> Edit Operation Hours</h3>
			</div>
			<!-- / Pie: Top Bar -->

			<!-- Pie: Content -->
			<div class="well">

			<a href="<?php echo $this->Html->url( array('action' => 'index', 'controller' => 'dashboards', 'plugin' => 'member' )); ?>"> 
				<button class="btn btn-skype" type="button"><i class="icon-list-alt"></i> Merchant Detail</button>
			</a>
			

			<div class="clearfix"></div>
				
				<form action="<?php echo $this->Html->url(array('action' => 'operationHourEdit')); ?>" 
					  style="padding: 20px;" method="POST" id="operationHourEditForm" >
					
					<div class="span6">
						<table width="100% !important">
							<tr>
								<td>Monday</td>
								<td>
									<input type="hidden" name="OperationHour[mon][id]" id="monID" value="<?php echo(isset($monday))?$monday['id']:'' ?>" >
									<select name="OperationHour[mon][f]" style="width:150px !important;" id="MonFrom" class="from">
										<?php foreach($operation_hours as $key => $hour ): ?>
											<option value="<?php echo $key ?>"
													<?php echo(isset($monday) && $monday['from'] == $key)?" selected='selected'":"" ?>><?php echo $hour ?></option>
										<?php endforeach; ?>
									</select>
								</td>
								<td>
									<select name="OperationHour[mon][t]" style="width:150px !important;" id="MonTo" class="to">
										<?php foreach($operation_hours as $key => $hour ): ?>
											<option value="<?php echo $key ?>"
													<?php echo(isset($monday) && $monday['to'] == $key)?" selected='selected'":"" ?>><?php echo $hour ?></option>
										<?php endforeach; ?>
									</select>
								</td>
							</tr>

							<tr>
								<td>Tuesday</td>
								<td>
									<input type="hidden" name="OperationHour[tues][id]" id="tuesID" value="<?php echo(isset($tuesday))?$tuesday['id']:'' ?>" >
									<select name="OperationHour[tues][f]" style="width:150px !important;" id="TuesFrom" class="from">
										<?php foreach($operation_hours as $key => $hour ): ?>
											<option value="<?php echo $key ?>"
													<?php echo(isset($tuesday) && $tuesday['from'] == $key)?" selected='selected'":"" ?>><?php echo $hour ?></option>
										<?php endforeach; ?>
									</select>
								</td>
								<td>
									<select name="OperationHour[tues][t]" style="width:150px !important;" id="TuesTo" class="to">
										<?php foreach($operation_hours as $key => $hour ): ?>
											<option value="<?php echo $key ?>"
													<?php echo(isset($tuesday) && $tuesday['to'] == $key)?" selected='selected'":"" ?>><?php echo $hour ?></option>
										<?php endforeach; ?>
									</select>
								</td>
							</tr>
							
							<tr>
								<td>Wednesday</td>
								<td>
									<input type="hidden" name="OperationHour[wed][id]" id="wedID" value="<?php echo(isset($wednesday))?$wednesday['id']:'' ?>" >
									<select name="OperationHour[wed][f]" style="width:150px !important;" id="WedFrom" class="from">
										<?php foreach($operation_hours as $key => $hour ): ?>
											<option value="<?php echo $key ?>"
													<?php echo(isset($wednesday) && $wednesday['from'] == $key)?" selected='selected'":"" ?>><?php echo $hour ?></option>
										<?php endforeach; ?>
									</select>
								</td>
								<td>
									<select name="OperationHour[wed][t]" style="width:150px !important;" id="WedTo" class="to">
										<?php foreach($operation_hours as $key => $hour ): ?>
											<option value="<?php echo $key ?>"
													<?php echo(isset($wednesday) && $wednesday['to'] == $key)?" selected='selected'":"" ?>><?php echo $hour ?></option>
										<?php endforeach; ?>
									</select>
								</td>
							</tr>
							
							<tr>
								<td>Thursday</td>
								<td>
									<input type="hidden" name="OperationHour[thur][id]" id="thurID" value="<?php echo(isset($thursday))?$thursday['id']:'' ?>" >
									<select name="OperationHour[thur][f]" style="width:150px !important;" id="ThurFrom" class="from">
										<?php foreach($operation_hours as $key => $hour ): ?>
											<option value="<?php echo $key ?>"
													<?php echo(isset($thursday) && $thursday['from'] == $key)?" selected='selected'":"" ?>><?php echo $hour ?></option>
										<?php endforeach; ?>
									</select>
								</td>
								<td>
									<select name="OperationHour[thur][t]" style="width:150px !important;" id="ThurTo" class="to">
										<?php foreach($operation_hours as $key => $hour ): ?>
											<option value="<?php echo $key ?>"
													<?php echo(isset($thursday) && $thursday['to'] == $key)?" selected='selected'":"" ?>><?php echo $hour ?></option>
										<?php endforeach; ?>
									</select>
								</td>
							</tr>
							
							<tr>
								<td>Friday</td>
								<td>
									<input type="hidden" name="OperationHour[fri][id]" id="friID" value="<?php echo(isset($friday))?$friday['id']:'' ?>" >
									<select name="OperationHour[fri][f]" style="width:150px !important;" id="FriFrom" class="from">
										<?php foreach($operation_hours as $key => $hour ): ?>
											<option value="<?php echo $key ?>"
													<?php echo(isset($friday) && $friday['from'] == $key)?" selected='selected'":"" ?>><?php echo $hour ?></option>
										<?php endforeach; ?>
									</select>
								</td>
								<td>
									<select name="OperationHour[fri][t]" style="width:150px !important;" id="FriTo" class="to">
										<?php foreach($operation_hours as $key => $hour ): ?>
											<option value="<?php echo $key ?>"
													<?php echo(isset($friday) && $friday['to'] == $key)?" selected='selected'":"" ?>><?php echo $hour ?></option>
										<?php endforeach; ?>
									</select>
								</td>
							</tr>
							
							<tr>
								<td>Saturday</td>
								<td>
									<input type="hidden" name="OperationHour[sat][id]" id="satID" value="<?php echo(isset($saturday))?$saturday['id']:'' ?>" >
									<select name="OperationHour[sat][f]" style="width:150px !important;" id="SatFrom" class="from">
										<?php foreach($operation_hours as $key => $hour ): ?>
											<option value="<?php echo $key ?>"
													<?php echo(isset($saturday) && $saturday['from'] == $key)?" selected='selected'":"" ?>><?php echo $hour ?></option>
										<?php endforeach; ?>
									</select>
								</td>
								<td>
									<select name="OperationHour[sat][t]" style="width:150px !important;" id="SatTo" class="to">
										<?php foreach($operation_hours as $key => $hour ): ?>
											<option value="<?php echo $key ?>"
													<?php echo(isset($saturday) && $saturday['to'] == $key)?" selected='selected'":"" ?>><?php echo $hour ?></option>
										<?php endforeach; ?>
									</select>
								</td>
							</tr>
							
							<tr>
								<td>Sunday</td>
								<td>
									<input type="hidden" name="OperationHour[sun][id]" id="sunID" value="<?php echo(isset($monday))?$sunday['id']:'' ?>" >
									<select name="OperationHour[sun][f]" style="width:150px !important;" id="SunFrom" class="from">
										<?php foreach($operation_hours as $key => $hour ): ?>
											<option value="<?php echo $key ?>"
													<?php echo(isset($sunday) && $sunday['from'] == $key)?" selected='selected'":"" ?>><?php echo $hour ?></option>
										<?php endforeach; ?>
									</select>
								</td>
								<td>
									<select name="OperationHour[sun][t]" style="width:150px !important;" id="SunTo" class="to">
										<?php foreach($operation_hours as $key => $hour ): ?>
											<option value="<?php echo $key ?>"
													<?php echo(isset($sunday) && $sunday['to'] == $key)?" selected='selected'":"" ?>><?php echo $hour ?></option>
										<?php endforeach; ?>
									</select>
								</td>
							</tr>
							
						</table>
					</div>

					<div class="submit" style="margin-top: 20px; clear: both; padding-top:20px; ">
					 	<a class="btn btn-google cancel" href="<?php echo $this->Html->url( array('action' => 'index', 'controller' => 'dashboards', 'plugin' => 'member' )); ?>">Cancel</a>
					 	<input class="btn btn-primary" type="submit" value="Save">
					</div>
						
				</form>
			</div>
			<!-- / Pie: Content -->

		</div>
		<!-- / Pie -->
		
	</div>

</div>

<script>
	$('select#MonFrom').change( function(){
		var val = $(this).val();

		$('select.from').val(val);

	})

	$('select#MonTo').change( function(){
		var val = $(this).val();

		$('select.to').val(val);

	})
</script>