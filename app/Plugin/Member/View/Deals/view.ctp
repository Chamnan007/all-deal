<?php 

 $goods_category = array();

  if( !empty($goods) ){
    foreach( $goods as $key => $value ){
      $goods_category[$value['GoodsCategory']['id']] = urldecode($value['GoodsCategory']['category']);
    }
  }


$dealCategories = $deal['Deal']['goods_category'];
$dealCategories = json_decode($dealCategories);

$cate_display = "";
foreach( $dealCategories as $val ){
    if( isset($goods_category[$val]) ){
      $cate_display .= $goods_category[$val] . ", ";
    }else{
      $cate_display .= $val . ", ";
    }
}

$cate_display = rtrim($cate_display, ", ");
 ?>

<style>
  tr{
    vertical-align: top;
  } 

</style>

<div class="business form">
  
  <div class="row-fluid">

    <div class="top-bar">
      <h3><i class="icon-eye-open"></i> Detail Deal Information</h3>
    </div>

    <div class="well" style="padding-bottom: 50px;">

      <a href="<?php echo $this->Html->url( array('action' => 'index')); ?>"> 
        <button class="btn btn-linkedin" type="button"><i class="icon-list"></i> Back to List</button>
      </a>

      <div style="clear: both; padding-top: 20px;" ></div>

        <div class="span6" style="margin-left:0px; ">
            <legend>Deal Information</legend>
            <table style="width:100%;color: #333 !important;">
                <tr>
                    <td width="100px;">Deal Title</td>
                    <td width="20px">:</td>
                    <td style="font-weight:bold;"><?php echo $deal['Deal']['title'] ?></td>
                </tr>
                <tr>
                    <td>Categories</td>
                    <td>:</td>
                    <td style="color: #06F"><?php echo $cate_display ?></td>
                </tr>
                <tr>
                    <td>Valid From</td>
                    <td>:</td>
                    <td><?php echo date('d-F-Y h:i:s A', strtotime($deal['Deal']['valid_from']) ) ?></td>
                </tr>
                <tr>
                    <td>Valid To</td>
                    <td>:</td>
                    <td><?php echo date('d-F-Y h:i:s A', strtotime($deal['Deal']['valid_to']) ) ?></td>
                </tr>

                <?php

                    $last_minute = 'No';
                    $color = 'red' ;

                    if( $deal['Deal']['is_last_minute_deal'] == 1 ){
                        $last_minute = 'Yes';
                        $color = "#06F";
                    }

                 ?>

                <tr>
                    <td>Last Minute Deal</td>
                    <td>:</td>
                    <td style="color:<?php echo @$color; ?>"><?php echo @$last_minute ?></td>
                </tr>

                <tr>
                    <td style="padding-top:10px;"></td>
                </tr>

                <?php 
                    $branch = "Any Branch";

                    if( $deal['BranchInfo']['branch_name'] ){
                        $branch = $deal['BranchInfo']['branch_name'];
                    }
                 ?>

                <tr>
                    <td>Availabel Branch</td>
                    <td>:</td>
                    <td><?php echo $branch ?></td>
                </tr>

                <tr>
                    <td style="padding-top:10px;"></td>
                </tr>

                <tr>
                    <td>Posted Date</td>
                    <td>:</td>
                    <td><?php echo date('d-F-Y h:i:s A', strtotime($deal['Deal']['created']) ) ?></td>
                </tr>

                <tr>
                    <td>Posted By</td>
                    <td>:</td>
                    <td><?php echo $deal['CreatedBy']['last_name'] . " " . $deal['CreatedBy']['first_name'] ?></td>
                </tr>

                <tr>
                    <td>Updated Date</td>
                    <td>:</td>
                    <td><?php echo date('d-F-Y h:i:s A', strtotime($deal['Deal']['modified']) ) ?></td>
                </tr>

                <tr>
                    <td>Updated By</td>
                    <td>:</td>
                    <td><?php echo $deal['UpdatedBy']['last_name'] . " " . $deal['UpdatedBy']['first_name'] ?></td>
                </tr>

                <tr>
                    <td style="padding-top:10px;"></td>
                </tr>

                <?php  
                    $state = $deal['Deal']['status'];
                        if( $state != 0 && (strtotime($deal['Deal']['valid_to']) < time()) ){
                          $state = -1;
                        } 
                ?>

                <tr>
                    <td>Deal Status</td>
                    <td>:</td>
                    <td>
                        <span class="label label-<?php echo $deal_status[$state]['color'] ?>">
                            <?php echo h($deal_status[$state]['status']); ?>
                        </span>
                    </td>
                </tr>

                <?php if( $deal['Deal']['status'] == 1 ){ ?>
                    <tr>
                        <td>Approved Date</td>
                        <td>:</td>
                        <td><?php echo date('d-F-Y h:i:s A', strtotime($deal['Deal']['publish_date']) ) ?></td>
                    </tr>
                <?php } ?>

            </table>
        </div>

        <div class="span6">
            <legend>Deal Description</legend>
            <?php echo $deal['Deal']['description'] ?>

            <legend>Deal Conditions</legend>
            <?php echo $deal['Deal']['deal_condition'] ?>
        </div>

        <div class="clearfix"></div>
        <legend>Deal Images</legend>

        <?php

            $image = $deal['Deal']['image'];
            if( $other_images = $deal['Deal']['other_images']) {
                $other_images = json_decode($other_images);
                $images = array();
                foreach( $other_images as $k => $v ){
                    if( $v != "img/deals/default.jpg" ){
                        $images[] = $v;
                    }
                }
            }else{
                $images[] = $image;
            }

            foreach( $images as $ind => $img ){

        ?>
            <div class="deal-image">
                <!-- <img src="<?php echo $this->webroot . $img ?>" alt="<?php echo $img ?>"> -->
                <a href="<?php echo $this->webroot . $img ?>"  rel="shadowbox">
                    <img src="<?php echo $this->webroot . $img ?>" alt="<?php echo $img ?>">
                  </a>
            </div>
        <?php } ?>

        <!-- <div class="clearfix"></div> -->
        <legend>Deal Items</legend>
        <?php 
            $dealItemLinks = $deal['DealItemLink'];            
        ?>
        <table class="table-list" id="table-item-listing" >
            <thead>
                <th width="80px;"><?php echo __('Image'); ?></th>
                <th><?php echo __('Item Name'); ?></th>
                <th width="150px;"><?php echo __('Original Price'); ?></th>
                <th width="150px;"><?php echo __('Discounted Price'); ?></th>
                <th width="150px;"><?php echo __('Available Qty'); ?></th>
            </thead>    

            <tbody>
            <?php 
                foreach( $dealItemLinks as $k => $val ){
                    $original_price = ($val['original_price']!=0)?$val['original_price']:$val['ItemDetail']['price'];
            ?>
                <tr>
                    <td>
                        <a href="<?php echo $this->webroot . $val['ItemDetail']['image'] ?>"  rel="shadowbox">
                            <img src="<?php echo $this->webroot . $val['ItemDetail']['image'] ?>" style="width:100%; border: 1px solid #DDD">
                        </a>
                    </td>
                    <td><?php echo $val['ItemDetail']['title'] ?></td>
                    <td>$ <?php echo number_format($original_price, 2) ?></td>
                    <td>$ <?php echo number_format($val['discount'],2) ?></span></td>
                    <td style="text-align:right"><?php echo number_format($val['available_qty'],2) ?></td>
                </tr>
            <?php 
                }
             ?>
            </tbody>
        </table>
    </div>
  </div>

</div>

<div id="warning-modal" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true"
  style="">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel" style="color:red"><i class="icon-exclamation"></i> Warning !</h3>
  </div>

  <div class="span6" style="padding-top: 10px; padding-bottom: 20px;">
    
    <h5 id="warning-modal"></h5>
    <button class="btn btn-google cancel" data-dismiss='modal' type="button">Close</button>

  </div>
</div> 

<style>
    
    .item-price{
        text-align: right;
        font-size: 13px;
        font-weight: bold;
    }

    .item-discount-price{
        text-align: right;
        color: blue;
        font-weight: bold;
        font-size: 13px;
    }

</style>


<script>

  function numericAndDotOnly(elementRef) {
      
      var keyCodeEntered = (event.which) ? event.which : (window.event.keyCode) ? window.event.keyCode : -1;

    if ((keyCodeEntered >= 48) && (keyCodeEntered <= 57)) {
      return true;
    }   
    // '.' decimal point...
    else if (keyCodeEntered == 46) {
      // Allow only 1 decimal point ('.')...
      if ((elementRef.value) && (elementRef.value.indexOf('.') >= 0))
        return false;
      else
        return true;
    }

    return false;

  }

  $(document).ready(function(){

    Shadowbox.init();

    var checkIEBrowser = msieversion();
  
    function msieversion() {

            var ua = window.navigator.userAgent;
            var msie = ua.indexOf("MSIE ");
            var ieversion;

            if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer, return version number
                return parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)));
            else                 // If another browser, return 0
                return false;
    }

  });

</script>