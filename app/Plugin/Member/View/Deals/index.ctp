<?php 
     /********************************************************************************

      File Name: index.ctp (Deal Listing in Member Part)
      Description: for business registration form

      Powered By: ABi Investment Group Co., Ltd,

      Changed History:

          Date                Author              Description
          2014/06/10          Rattana Phea        Initial Version
          2015/05/15          Rattana Phea        Disable Permission of edit, delete after approved
          2015/06/21          Rattana Phea        Expand Deal Validity and need approval before update

      *********************************************************************************/ 

$goods_category = array();

if( !empty($goods) ){
  foreach( $goods as $key => $value ){
    $goods_category[$value['GoodsCategory']['id']] = urldecode($value['GoodsCategory']['category']);
  }
}

 ?>

<style>

  tr{
    vertical-align: top;
  } 

  .deal-image{
      width:223px; 
      height: 133px; 
      border: 1px solid #CCC; 
      padding:3px; 
      float:left; 
      margin-right:20px;
      margin-top:10px;
      margin-bottom: 10px;
      position: relative;
  }

  .deal-image:hover > .remove-img{
      display: block;    
  }

  .deal-image .remove-img{
      position: absolute;
      display: none;
      background: rgb(255, 101, 81);
      padding: 5px 5px 5px 7px ;
      text-align: center;
      border: rgb(255, 101, 81);
      border-radius: 3px ;
      left:44%;
      top:40%;
  }
  .deal-image .remove-img i{
      color:white;
      font-size:15px;
  }
  .deal-image .remove-img:hover{
      background: rgb(217, 64, 44);
      cursor: pointer;
  }


  .deal-image-upload{
      background: #DDD;
      width:223px; 
      height: 133px; 
      border: 1px solid #CCC; 
      padding:3px; 
      float:left; 
      margin-right:20px;
      margin-top:10px;
      margin-bottom: 10px;
      position: relative;
  }
  .deal-image-upload .upload{
      position: absolute;
      background: rgb(54, 132, 252);
      padding: 5px 5px 5px 7px ;
      text-align: center;
      border: rgb(54, 132, 252);
      border-radius: 3px ;
      left:40%;
      top:40%;
  }
  .deal-image-upload .upload i{
      color:white;
      font-size:15px;
  }
  .deal-image-upload .upload:hover{
      background: rgb(14, 93, 216);
      cursor: pointer;
  }
</style>

<div class="business form">
  
  <div class="row-fluid">

    <div class="top-bar">
      <h3><i class="icon-list"></i> Manage Merchant Deals</h3>
    </div>

    <div class="well" style="padding-bottom: 100px;">

      <a href="<?php echo $this->Html->url( array('action' => 'add')); ?>" data-toggle="modal"> 
        <button class="btn btn-linkedin" type="button"><i class="icon-plus"></i> Create New Deal</button>
      </a>

      <a class="btn btn-google pull-right" href="#" id="clear-search-btn" style="margin-right:0px;">
        <i class="icon icon-trash"></i>
        Clear Search
      </a>

      <a class="btn btn-skype pull-right" href="#" id="filter-btn">
        <i class="icon icon-search"></i>
        Deals Search
      </a>

      <div style="clear: both; padding-top: 20px;" ></div>

      <table class="table-list" >
        <tr>
          <th width="80px;'">Image</th>
          <th>Deal Title</th>
          <th width="200px">Detail</th>
          <th width="220px">Validity</th>
          <th width="80px">Status</th>
          <th>Action</th>
        </tr>

        <?php 

          if($data){

          foreach( $data as $key => $value ){ 

            $index = 0;

            $split = end(explode("/", $value['Deal']['image']));
            $thumb_img =  "img/deals/thumbs/" . $split ;

            $other_images = $value['Deal']['other_images'];
            if( $other_images ){
                
                $other_images = json_decode($other_images);
                foreach( $other_images as $k => $img ){

                  if( $img != "img/deals/default.jpg" ){
                    $index ++;
                    $spt    = end(explode("/", $img ));
                    $thumb  = "img/deals/thumbs/" . $spt;

                    $data_images[$value['Deal']['id']][$index]['img'] = $img;
                    $data_images[$value['Deal']['id']][$index]['thumb'] = $thumb;
                  }
                }

            }else if($value['Deal']['image'] != "img/deals/default.jpg" ) {
              $data_images[$value['Deal']['id']][$index]['img']   = $value['Deal']['image'];
              $data_images[$value['Deal']['id']][$index]['thumb']  = $thumb_img;
            }

            $selected = @$data_images[$value['Deal']['id']][array_rand($data_images[$value['Deal']['id']])];

            if( $selected ){ 
                $selected_img   = $selected['img'];
                $selected_thumb = $selected['thumb']; 
            }else{
                $selected_img   = "img/deals/default.jpg" ;
                $selected_thumb = "img/deals/thumbs/default.jpg" ;
            }

            $dealCategories = $value['Deal']['goods_category'];
            $dealCategories = json_decode($dealCategories);

            $cate_display = "";
            foreach( $dealCategories as $val ){
                if( isset($goods_category[$val]) ){
                  $cate_display .= $goods_category[$val] . ", ";
                }else{
                  $cate_display .= $val . ", ";
                }
            }

            $cate_display = rtrim($cate_display, ", ");

            $last_minute = "";

            if( $value['Deal']['is_last_minute_deal']){
                $last_minute = "Yes";
            }

          ?>
          <tr>
            <td>
              <a href="<?php echo $this->webroot . $selected_img ?>"  rel="shadowbox">
                <img src="<?php echo $this->webroot . $selected_thumb ?>" alt=""
                 style="max-width:90px; max-height:80px; height: auto; padding: 3px; border: 1px solid #CCC;" >
              </a>
            </td>
            <td>
              Code : <strong><?php echo $value['Deal']['deal_code'] ?></strong><br>
              Title :
                <strong>
                  <a href="<?php echo $this->Html->url( array('action' => 'view', $value['Deal']['id'])); ?>" 
                    data-toggle="modal"
                    title="View Deal Detail"
                    style="margin-left:0px;"> 
                    <?php echo urldecode($value['Deal']['title']) ?>
                  </a>
                </strong><br>
              <strong>Category : </strong><?php echo $cate_display ?>
              <?php if( $last_minute != "" ){ ?>
                  <br>Is Last Minute Deal : <strong style="color:#06F"><?php echo $last_minute ?></strong>
              <?php  } ?>
            </td>
            <td>
              <span><strong>Created Date : </strong><?php echo date( 'd-F-Y', strtotime($value['Deal']['created']) ) ?> at <?php echo date( 'h:i:s A', strtotime($value['Deal']['created']) ) ?></span>, By : <?php echo urldecode($value['CreatedBy']['last_name'] . " " . $value['CreatedBy']['first_name']) ?><br>

              <span><strong>Last Updated : </strong><?php echo date( 'd-F-Y', strtotime($value['Deal']['modified']) ) ?> at <?php echo date( 'h:i:s A', strtotime($value['Deal']['modified']) ) ?></span>, By : <?php echo urldecode($value['UpdatedBy']['last_name'] . " " . $value['UpdatedBy']['first_name']) ?>
            </td>
            <td>
              <strong>From :</strong> <?php echo date( 'd-F-Y', strtotime($value['Deal']['valid_from']) ) ?> at <?php echo date( 'h:i:s A', strtotime($value['Deal']['valid_from']) ) ?><br>
              <strong>To :</strong> <?php echo date( 'd-F-Y', strtotime($value['Deal']['valid_to']) ) ?> at <?php echo date( 'h:i:s A', strtotime($value['Deal']['valid_to']) ) ?>
            </td>
            <td style="text-align: center; vertical-align: top">
              <?php 
                
                if( $value['Deal']['status'] != 0 && (strtotime($value['Deal']['valid_to']) < time()) ){
                  $value['Deal']['status'] = -1;
                } 

              ?>
              <span class="label label-<?php echo $deal_status[$value['Deal']['status']]['color'] ?>">
                <?php echo h($deal_status[$value['Deal']['status']]['status']); ?>
              </span>
            </td>            

            <td class="actions" style="vertical-align: top; text-align:right">

              <a href="<?php echo $this->Html->url( array('action' => 'view', $value['Deal']['id'])); ?>" 
                data-toggle="modal"
                class="btn btn-linkedin"
                title="View Deal Detail"
                style="margin-left:0px;"> 
                <i class="icon icon-eye-open" style="padding-right:0px;"></i>
              </a>

              <?php if( $value['Deal']['status'] != 0 ){ ?>

                  <a href="<?php echo $this->Html->url( array('action' => 'repost', $value['Deal']['id'])); ?>" 
                    data-toggle="modal"
                    class="btn btn-skype"
                    title="Repost Deal"
                    style="margin-left:0px;"> 
                    <i class="icon icon-refresh" style="padding-right:0px;"></i>
                  </a>
              <?php } ?>

              <?php if( $value['Deal']['status'] == 0 ){ ?>

                <a href="<?php echo $this->Html->url( array('action' => 'edit', $value['Deal']['id'])); ?>" 
                  data-toggle="modal"
                  class="btn btn-foursquare"
                  title="Edit Deal"
                  style="margin-left:0px;"> 
                  <i class="icon icon-edit" style="padding-right:0px;"></i>
                </a>
              
                <?php if( isset($business_member_level) && 
                          in_array($business_member_level, array(1,3)) && 
                          $access_level == 1 ){ ?>
                  <a  href="#deal_delete"
                    data-toggle="modal"
                    class="deleteDeal btn btn-google"
                    data-id="<?php echo $value['Deal']['id'] ?>"
                    title="Delete Deal" 
                    style="margin-left:0px;">
                    <i class="icon icon-trash" style="padding-right:0px;"></i>
                  </a>
                <?php } ?>

                  <div style="clear:both; padding-top:4px;"></div>

                  <a  href="#"
                      data-bisid ="<?php echo $business_id ?>"
                      data-id="<?php echo $value['Deal']['id'] ?>"
                      class="deal_images pull-right"
                      data-action="" 
                      title="View Deal Images"
                      style="margin-left:0px;">
                      <button class="btn btn-foursquare" type="button">                          
                        Deal Images
                      </button>
                  </a>

            <?php } ?>

            </td>
          </tr>
        <?php 
          } 

        }else{

        ?>
          <tr>
            <td colspan="10"><i>No deal found !</i></td>
          </tr>

        <?php
          }
        ?>
       </table>

       <p class="page">
          <?php
          echo $this->Paginator->counter(array(
          'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total')
          ));
          ?>  
        </p>

        <div class="paging">
        <?php
          echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
          echo $this->Paginator->numbers(array('separator' => ''));
          echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
        ?>
        </div>

    </div>
  </div>

</div>


<div id="search-deal-modal" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel"><i class="icon-search"></i> Search Filter</h3>
  </div>

  <!-- Any Info-customization -->
  <form action="<?php echo $this->Html->url(array('action' => 'index')); ?>" 
        style="padding: 20px;" method="POST" id="searchDealForm"
        enctype = "multipart/form-data" >

    <div style="width:100%;">

    <fieldset>

      <div style="display:block; clear:both">
        <label for="dealTitle" style="float:left; width:100px; padding-top:5px;">Deal Title</label>
        <input  name="deal-title" 
            placeholder="Deal Title" 
            style="float:left; width:390px;" 
            id="dealTitle" 
            value="<?php echo @$states['deal-title'] ?>"
            maxlength="100" type="text" >
      </div>

      <div style="clear:both; ">
        <label style="float:left; width:100px; padding-top:5px;">Category</label>
        <select name="deal-category" id="deal-category" style="width:410px;">
          <option value="">Any category</option>
          <?php foreach( $goods as $k => $v ){ 
              $selected = ( isset($states['deal-category']) && $states['deal-category'] == $v['GoodsCategory']['id'] )?" selected='selected'":"" ;
          ?>
            <option value="<?php echo $v['GoodsCategory']['id'] ?>" <?php echo $selected ?>><?php echo $v['GoodsCategory']['category'] ?></option>
          <?php } ?>
        </select>
      </div>

      <div style="display:block; clear:both">
        <label for="BuyerTransactionPurchaseDate" style="float:left; width:100px; padding-top:5px;">Posted Date</label>
        <div id="dealValidFromPicker" class="input-append date datetimepicker" style="float:left">
                  <input data-format="dd-MM-yyyy" type="text" 
                      name="postedFrom"
                      style="width: 140px"
                      value="<?php echo @$states['postedFrom'] ?>"
                      placeholder="From">
                  <span class="add-on">
                    <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                    </i>
                  </span>
              </div>
        <span style="float:left; margin-left:8px; margin-right:7px; padding-top:5px;">TO</span>
        <div id="dealValidFromPicker" class="input-append date datetimepicker" style="float:left">
                  <input data-format="dd-MM-yyyy" type="text" 
                      name="postedTo"
                      style="width: 133px"
                      value="<?php echo @$states['postedTo'] ?>"
                      placeholder="To">
                  <span class="add-on">
                    <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                    </i>
                  </span>
              </div>
        
      </div>

      <div style="display:block; clear:both">
        <label for="BuyerTransactionReceivedDate" style="float:left; width:100px; padding-top:5px;">Validity</label>
        <div id="dealValidFromPicker" class="input-append date datetimepicker" style="float:left">
                  <input data-format="dd-MM-yyyy" type="text" 
                      name="valideFrom"
                      style="width: 140px"
                      value="<?php echo @$states['valideFrom'] ?>"
                      placeholder="From">
                  <span class="add-on">
                    <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                    </i>
                  </span>
              </div>
        <span style="float:left; margin-left:8px; margin-right:7px; padding-top:5px;">TO</span>
        <div id="dealValidFromPicker" class="input-append date datetimepicker" style="float:left">
              <input data-format="dd-MM-yyyy" type="text" 
                  name="validTo"
                  style="width: 133px"
                  value="<?php echo @$states['validTo'] ?>"
                  placeholder="To">
              <span class="add-on">
                <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                </i>
              </span>
          </div>
      </div>

      <div style="display:block; clear:both">
        <label for="BuyerTransactionPurchaseDate" style="float:left; width:100px; padding-top:5px;">Publisehd Date</label>
        <div id="dealValidFromPicker" class="input-append date datetimepicker" style="float:left">
                  <input data-format="dd-MM-yyyy" type="text" 
                      name="publishedFrom"
                      style="width: 140px"
                      value="<?php echo @$states['publishedFrom'] ?>"
                      placeholder="From">
                  <span class="add-on">
                    <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                    </i>
                  </span>
              </div>
        <span style="float:left; margin-left:8px; margin-right:7px; padding-top:5px;">TO</span>
        <div id="dealValidFromPicker" class="input-append date datetimepicker" style="float:left">
                  <input data-format="dd-MM-yyyy" type="text" 
                      name="publishedTo"
                      style="width: 133px"
                      value="<?php echo @$states['publishedTo'] ?>"
                      placeholder="To">
                  <span class="add-on">
                    <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                    </i>
                  </span>
              </div>
        
      </div>

      <div style="display:block; clear:both">
        <label style="float:left; width:100px; padding-top:5px;">Deal Status</label>
        <select name="status" id="deal-status" style="width:410px;">
          <option value="all">Any status</option>
          <?php foreach( $deal_status as $k => $v ){ 
              $selected = ( isset($states['status']) && $states['status'] != 'all' && $states['status'] == $k )?" selected='selected'":"" ;
          ?>
            <option value="<?php echo $k ?>" <?php echo $selected ?>><?php echo $v['status'] ?></option>
          <?php } ?>
        </select>
      </div>


      <div style="display:block; clear:both">
        <label for='last-minute' style="float:left; width:100px; padding-top:3px;">Last Minute Deal</label>
        <?php 

          $checked = (isset($states['last-minute']) && $states['last-minute'] == 1)?" checked='checked'":"" ;
         ?>
        <input id="last-minute" type="checkbox" value="1" name="last-minute" style="flaot:left; margin-left:10px;" <?php echo $checked ?>/>
      </div>

    </fieldset>

    </div>

    <div class="submit" style="margin-top: 40px; clear: both;">
      <input class="btn btn-primary" type="submit" value="Submit Search">
      <button type="button" class="btn btn-linkedin reset-btn">Reset</button>
      <button class="btn btn-google cancel" data-dismiss='modal' type="button">Close</button>
    </div>
      
  </form>

</div> 

<!--Deal Images Modal -->
<div id="deal_images" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true"
  style="width:800px !important; left:40% !important; min-height:300px; max-height: 500px;">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel"><i class="icon-picture"></i> Deal Images</h3>
  </div>
    
    <div class="clearfix"></div>
    <span id="error-msg" style="margin-left:20px; font-size:16px; color:red; display: none">
        
    </span>

  <div class="span9" style="margin-top:20px; 
                            width:775px; 
                            position:relative;
                            max-height: 380px; min-height:180px;
                            overflow-y:scroll" id='deal-image-container'>
        
      <div  class='deal-image' >
            
      </div>

      <div style="width:32px; height:32px; margin-left:48%; margin-top:8%; position:absolute;" id="loading-img">
            <img src="<?php echo $this->webroot . 'img/ajax-loader.gif' ?>" alt="">
      </div>

      <div  class='deal-image-upload' style="text-align:center; display:none;">
            <div class="upload" title="Upload Deal Image">
                <i class='icon-plus'></i>
                <img src="<?php echo $this->webroot . 'img/ajax-loader.gif' ?>" alt="" style="display:none;">
            </div>
            Size: 705 x 422 pixels <br>Allow File Types: (JPG, JPEG, PNG) 
      </div>

  </div>

  <div style="clear:both;margin-bottom:20px; margin-left:20px;">
      <button class="btn btn-google cancel" data-dismiss='modal' type="button">Close</button>

  </div>

</div>


<div id="deal_delete" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel" style="color:red"><i class="icon-question"></i> Are You Sure ?</h3>
  </div>

  <form   action="#" 
      id="dealDeleteForm"
        style="padding: 20px;" method="POST" >

    <div>
      <h5>Are you sure you want to delete this deal?</h5>
    </div>

    <div class="submit" style="margin-top: 20px; clear: both; width:100%">
      <input class="btn btn-primary" type="submit" value="Yes">
      <button class="btn btn-google cancel" data-dismiss='modal' type="button">No</button>
    </div>
      
  </form>

</div>

<!-- Remove Deal Image -->
<div id="remove-deal-image" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true"
  style="z-index:999999">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel" style="color:red"><i class="icon-question"></i> Are you sure ?</h3>
  </div>

  <div class="span6" style="padding-top: 10px; padding-bottom: 20px;">
    
    <h5>Are you sure you want to remove this image?</h5>

    <button class="btn btn-primary" type="button" id='confirm-delete'>Yes</button>
    <button class="btn btn-google cancel" data-dismiss='modal' type="button">No</button>

  </div>

</div> 

<form action="<?php echo $this->Html->url(array('action' => 'uploadDealImageAjax__', $business_id)); ?>"
        method="POST"  enctype="multipart/form-data"
        id="frm-upload-deal-image">
    <input type="file" name='upload_deal_image' id="upload_deal_image" style="display:none;">
</form>


<div id="warning-modal" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true"
  style="">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel" style="color:red"><i class="icon-exclamation"></i> Warning !</h3>
  </div>

  <div class="span6" style="padding-top: 10px; padding-bottom: 20px;">
    
    <h5 id="warning-modal"></h5>
    <button class="btn btn-google cancel" data-dismiss='modal' type="button">Close</button>

  </div>
</div> 


<script>

  $(document).ready(function(){

    $('.datetimepicker').datetimepicker();
    $('.datetimepicker').find('input').attr('readonly', 'readonly');
    $('.datetimepicker').find('input').css('cursor', 'default');

    $('#expand-valid-to').removeAttr('readonly');

    function msieversion() {

            var ua = window.navigator.userAgent;
            var msie = ua.indexOf("MSIE ");
            var ieversion;

            if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer, return version number
                return parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)));
            else                 // If another browser, return 0
                return false;
    }

    var checkIEBrowser = msieversion();
    Shadowbox.init();

    $(".deleteDeal").click( function(){

      var id = $(this).data('id');
      var action = '<?php echo $this->webroot ?>' + 'member/deals/deleteDeal/' + id ;
      $("#dealDeleteForm").attr('action', action) ;

    });

    $(".change_image_deal").click( function(){

      var me    = $(this),
        deal_id = me.data('id');

      var action = '<?php echo $this->webroot ?>' + 'member/deals/changeDealImage/' + deal_id ;

      var form = $("#changeImageDeal");

      form.attr('action', action);

    })

    if( checkIEBrowser ){

       $("#save_upload").click( function(){

          var img = $('#deal_image_change').val();

          if( img == "" ){
            alert("Please select new deal image"); return false;
          }

      }) 
    };

    // ============================================
    var div_image = $('.deal-image');
    $('.deal-image').remove();
    var div_upload = $('div.deal-image-upload');

    // Deal Images View
    $('.deal_images').click ( function(event){

        event.preventDefault();

        $('#deal-image-container').find('.deal-image').remove();

        var image_modal     = $('div#deal_images');
        var deal_id         = $(this).data('id');
        
        var getDealImageURL__ = "<?php echo $this->Html->url(array('action' => 'getDealImagesAjax__')); ?>";
        getDealImageURL__ += "/" + deal_id ;

        image_modal.modal('show');
        var root = '<?php echo $this->webroot ?>';

        var loading_img = $('div#loading-img');
        var time = new Date().getTime();
        loading_img.show();

        $('div.deal-image-upload').hide();

        if( deal_id ){
            $.ajax({

                url: getDealImageURL__,
                type: "POST",
                data: {},
                async: false,
                dataType: 'json',

                success: function(data){
                    if( data.status == true ){

                        setTimeout( function(){
                            loading_img.hide();

                            var images = data.deal_images;

                            $.each( images, function(ind, val){
                                time += 300;

                                var img_name = val.split("/");
                                img_name = img_name[img_name.length - 1];

                                var img = root + "img/deals/" + img_name;
                                var thb = root + "img/deals/thumbs/" + img_name;

                                div_image.attr( 'data-id' , deal_id + "_" + time );
                                div_image.html('<img alt="" src="' + thb + '"><div title="Remove Image" class="remove-img" data-img="'+ img +'" data-thumb="'+ thb +'" data-dealid="' + deal_id + '"><i class="icon icon-trash"></i></div>');

                                $('#deal-image-container').prepend(div_image.clone());
                            });

                            checkUploadButton(deal_id);

                        }, 1000);
                    }
                },
            
                error: function(err){
                    
                }

            });
        }

    });

    function checkUploadButton( deal_id ){

        var imageCount = $('#deal-image-container').find('.deal-image').length;
        var allowImagesSize = <?php echo $dealImagesSize ?>;

        if( imageCount < allowImagesSize ){
            $('div.deal-image-upload').find('.upload').attr('data-dealid', deal_id);
            $('div.deal-image-upload').find('i').show();
            $('div.deal-image-upload').find('img').hide();
            $('div.deal-image-upload').show();
        }else{
            $('div.deal-image-upload').hide();
        }
    }

    $('#deal-image-container').on('click', '.upload', function(){
        $('#upload_deal_image').trigger('click');
    })

    $('input#upload_deal_image').on( 'change', function(){
        var max_size = <?php echo $__MAXIMUM_IMAGE_SIZE ?>;
        var files = $($(this)[0].files);

        var size = files[0].size ;

        if( size > max_size ){
          $('input#upload_deal_image').val('');
          $('div#warning-modal').find('h5').text( "Images reached size limitation. Images should be less then 2MB." );
          $('div#warning-modal').modal('show');

          return false;
        }

        $('form#frm-upload-deal-image').submit();
    })

    $('form#frm-upload-deal-image').submit( function(event){

        event.preventDefault();

        var div_container   = $('div.deal-image-upload').find('.upload');
        var icon_add        = div_container.find('i');
        var loading_img     = div_container.find('img');

        var deal_id = div_container.data('dealid');

        var form = $(this);
        var action = form.attr('action');
        var root = '<?php echo $this->webroot ?>';
        
        loading_img.show();

        action += "/" + deal_id;

        $.ajax({

            url: action,
            type: "POST",
            data:  new FormData(this),
            contentType: false,
            cache: false,
            async: false,
            dataType: 'json',
            processData:false,
            
            beforeSend: function(){
                icon_add.hide();
            },

            success: function(data){

                setTimeout( function(){

                    icon_add.show();
                    loading_img.hide();    

                    if( data.status == true ){

                        var img_name = data.uploaded_image.split("/");
                        img_name = img_name[img_name.length - 1];

                        var img = root + 'img/deals/' + img_name ;
                        var thb = root + 'img/deals/thumbs/' +  img_name;

                        var time = new Date().getTime();

                        div_image.attr( 'data-id' , deal_id + "_" + time );

                        div_image.html('<img alt="" src="' + thb + '"><div title="Remove Image" class="remove-img" data-img="'+ img +'" data-thumb="'+ thb +'" data-dealid="' + deal_id + '"><i class="icon icon-trash"></i></div>');

                        $('#deal-image-container').prepend(div_image.clone()).fadeIn();
                        checkUploadButton(deal_id);
                        $('form#frm-upload-deal-image')[0].reset();
                    }else{
                        var msg = data.message;
                        $('span#error-msg').text(msg).fadeIn();
                        setTimeout(function(){
                            $('span#error-msg').fadeOut();
                        },2000);
                    }

                }, 1000);
                          
            },
        
            error: function(){             
            
            }

        });

        return false;

    });

    // Remvoe Deal
    $('#deal-image-container').on('click', '.remove-img', function(){
        var me      = $(this),
            img     = me.data('img'),
            deal_id = me.data('dealid'),
            img_box = me.closest(".deal-image").attr("data-id") ;

        var remove_image_modal   = $('div#remove-deal-image');

        remove_image_modal.attr('box-id', img_box);
        remove_image_modal.attr('deal-id', deal_id);
        remove_image_modal.attr('data-img', img);

        remove_image_modal.modal('show');

    });


    $('div#remove-deal-image').on('click', '#confirm-delete', function(){
        
        var modal   = $('div#remove-deal-image');
        var box_id  = modal.attr('box-id');
        var deal_id = modal.attr('deal-id');
        var img     = modal.attr('data-img');

        var img_name = img.split("/");

        img_name = img_name[img_name.length - 1];
        
        var deleteURL__   = "<?php echo $this->Html->url(array('action' => 'deleteDealImageAjax__')); ?>";

        deleteURL__ += "/" + deal_id + "/" + img_name;

        if( deal_id && img ){

            $.ajax({

                url: deleteURL__,
                type: "POST",
                data: {},
                async: false,
                dataType: 'json',

                success: function(data){
                    if(data.status == true ){
                        modal.modal('hide');
                        var remove_box = $('div#deal_images').find('.deal-image[data-id="'+ box_id +'"]');
                        remove_box.fadeOut('slow').remove();
                        checkUploadButton(deal_id);
                    }else{
                        var msg = data.message;
                        $('span#error-msg').text(msg).fadeIn();
                        setTimeout(function(){
                            $('span#error-msg').fadeOut();
                        },3000);
                    }
                },
            
                error: function(){
                }

            });
        }

        return false;

    });

    $('#filter-btn').click( function(event){
        event.preventDefault();
        var modal = $('#search-deal-modal');

        modal.modal('show');
    });

    $('#clear-search-btn').click(function(event){
        event.preventDefault();

        var modal = $('div#search-deal-modal');

        $('.reset-btn').click();

        modal.find('form').submit();

    });

    $('.reset-btn').click( function(){

      var modal = $('div#search-deal-modal');

      modal.find('input[type="text"]').val("");
      modal.find('select#deal-status').val("all");
      modal.find('select#deal-category').val("");
      modal.find('input[type="checkbox"]').removeAttr('checked');

    })


  });

</script>