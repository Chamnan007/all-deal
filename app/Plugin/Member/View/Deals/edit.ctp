<?php 

  $goods_category = array();

  if( !empty($goods) ){
    foreach( $goods as $key => $value ){
      $goods_category[$value['GoodsCategory']['id']] = urldecode($value['GoodsCategory']['category']);
    }
  }

 ?>

<style>
  tr{
    vertical-align: top;
  } 

</style>

<div class="business form">
  
  <div class="row-fluid">

    <div class="top-bar">
      <h3><i class="icon-edit"></i> Edit Deal Information</h3>
    </div>

    <div class="well" style="padding-bottom: 50px;">

      <a href="<?php echo $this->Html->url( array('action' => 'index')); ?>"> 
        <button class="btn btn-linkedin" type="button"><i class="icon-list"></i> Deals List</button>
      </a>

      <div style="clear: both; padding-top: 20px;" ></div>

      <form action="<?php echo $this->Html->url(array('action' => 'edit', $deal['Deal']['id'])); ?>" 
          style="padding: 20px; padding-top:10px;" method="POST" id="dealAddForm"
          enctype = "multipart/form-data" >

         <div class="span3">
            <?php

              echo $this->Form->input('title', array(
                              'placeholder' => 'Deal Title',
                              'label' => 'Deal Title',
                              'required'=>'required',
                              'type' => 'textarea',
                               'name' => 'data[Deal][title]',
                               'id' => 'DealTitle',
                               'style' => 'height:100px;',
                              'maxLength' => 255,
                              'value' => urldecode($deal['Deal']['title']))) ;    


            ?>  

            <div id="dealValidFromPicker" class="input-append date">
              <label for="">Valid From</label>
                <input data-format="dd-MM-yyyy hh:mm:ss" type="text" 
                    name="data[Deal][valid_from]" id="DealValidFrom"
                    style="width: 165px !important"
                    placeholder="valid From" required='required'
                    value="<?php echo date("d-m-Y H:i:s", strtotime($deal['Deal']['valid_from'])) ?>">
                <span class="add-on">
                  <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                  </i>
                </span>
            </div>

            <div id="dealValidToPicker" class="input-append date">
              <label for="">Valid To</label>
                <input data-format="dd-MM-yyyy hh:mm:ss" type="text" 
                    name="data[Deal][valid_to]" id="DealValidTo"
                    style="width: 165px !important"
                    placeholder="valid To" required='required'
                    value="<?php echo date("d-m-Y H:i:s", strtotime($deal['Deal']['valid_to'])) ?>">
                <span class="add-on">
                  <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                  </i>
                </span>
            </div>

            <div class="input-append date">
              <label for=""><b>Branch</b></label>
              <select name="data[Deal][branch_id]" id="merchant-branch" >
                  <?php foreach( $merchant_branches as $k => $val ){ 
                        $selected = ($deal['Deal']['branch_id'] == $val['id'])?" selected='selected'":"";
                  ?>
                      <option value="<?php echo $val['id'] ?>" 
                              data-lat="<?php echo $val['latitude'] ?>"
                              data-lng="<?php echo $val['longitude'] ?>"
                              <?php echo $selected ?>><?php echo $val['branch_name'] ?></option>
                  <?php } ?>
              </select>              
              <input type="hidden" value="<?php echo $deal['Deal']['latitude'] ?>" name="data[Deal][latitude]"   id="latitude" />
              <input type="hidden" value="<?php echo $deal['Deal']['longitude'] ?>" name="data[Deal][longitude]"  id="longitude" />

            </div>

            <?php 
              $checked = ($deal['Deal']['is_last_minute_deal'])?" checked='checked'":"";
            ?>
            <div>
              <label for="last-minute-deal" style="float:left"><b>Set as Last Minute Deal</b></label>
              <input  type="checkbox" id="last-minute-deal" 
                      <?php echo $checked ?>
                      value="1"
                      style="float:left; margin-left:10px;" name="data[Deal][is_last_minute_deal]"/>
            </div>

        </div>

         <div class="span3">
            <label for=""><strong>Category</strong></label>
          <div style="width:200px; 
                max-height:275px; 
                height: 275px; 
                border: 1px solid #CCC; 
                padding:10px; 
                padding-top:5px;
                overflow-y: auto;
                margin-bottom:5px;" 
              id="DealGoodsCategory">

            <?php foreach( $goods_category as $key => $value ){ 

                $g_cates = json_decode($deal['Deal']['goods_category']);

                $checked = ( !empty($g_cates) && in_array($key, $g_cates))?" checked='checked'":"";
            ?>

                 <div class="input checkbox" style="width:80%;">
                  <input type="checkbox" name="data[Deal][goods_category][]" id='agc<?php echo $value ?>' value="<?php echo $key ?>"
                      <?php echo $checked ?> >
                  <label for="agc<?php echo $value ?>"><?php echo $value ?></label>
                 </div>

            <?php } ?>
          </div>
        </div>

        <div class="span3"  style="margin-left:0px;">
          <?php 

            echo $this->Form->input('description', array('placeholder' => 'Deals Description',
                                   'type'       => 'textarea',
                                   'class'      => 'ckeditor',
                                   'label'      => '<b>Deal Description</b>',
                                   'name'       => 'data[Deal][description]',
                                   'id'         => 'DealDescriptionAdd',
                                   'maxlength'  => '65535',
                                   'value'      => urldecode($deal['Deal']['description'])));
           ?>

        </div>

        <div class="span3">
           <?php 

            echo $this->Form->input('deal_condition', array('placeholder' => 'Deal Conditions',
                                   'type' => 'textarea',
                                   'label' => '<b>Deal Conditions</b>',
                                   'class' => 'ckeditor ckeditor-small',
                                   'name' => 'data[Deal][deal_condition]',
                                   'id' => 'DealDescriptionAdd',
                                   'maxlength' => '65535',
                                   'value'      => urldecode($deal['Deal']['deal_condition'])));
           ?>
          
        </div>

        <div class="clearfix"></div>
        <legend style="font-size:16px;"><h5>Related Items</h5></legend>

        <a href="#" class="btn btn-linkedin" id="add-related-item-btn"><i class="icon-plus"></i> Add Related Item</a>

        <div class="span12" style="margin-left:0px; padding-top:20px;" id="div-related-item">

            <table class="table-list" id="table-item-listing" >
                <thead>
                    <th width="200px;"><?php echo __('Menu Category'); ?></th>
                    <th><?php echo __('Item Name'); ?></th>
                    <th width="150px;"><?php echo __('Original Price'); ?></th>
                    <th width="150px;"><?php echo __('Discounted Price'); ?></th>
                    <th width="150px;"><?php echo __('Available Qty'); ?></th>
                    <th width="60px;"><?php echo __('Action'); ?></th>
                </thead>    

                <tbody>
                    
                    <tr class="item-row" id="item-row-model">
                        <input type="hidden" name="DealItemLink[item_id][]" class='hidden-item-id' >
                        <input type="hidden" name="DealItemLink[original_price][]" class='hidden-item-original-price' >
                        <input type="hidden" name="DealItemLink[discount][]" class='hidden-item-discount' >
                        <input type="hidden" name="DealItemLink[availble_qty][]" class='hidden-item-qty' >
                        <td class='item-menu' style="font-weight: bold;"></td>
                        <td class='item-title'></td>
                        <td class='item-price'></td>
                        <td class='item-discount-price'>
                            <span></span>
                            <input style="width:100px; text-align:center; display:none;" 
                                     onkeypress="return numericAndDotOnly(this)"
                                    value="" />
                        </td>
                        <td class='item-available-qty' style="text-align:right">
                            <span></span>
                            <input style="width:100px; text-align:center; display:none;" 
                                     onkeypress="return numericAndDotOnly(this)"
                                    value="" />
                        </td>
                        <td class='item-action' style="text-align:center">

                            <a  href="#" class='btn btn-success save-change' 
                                style="display:none;"
                                data-price="" title="Save Change">
                                <i class='icon-save' style='font-size:12px; padding-right:0px;'></i></a>

                            <a  href="#" class='btn btn-linkedin edit-selected-item' 
                                title="Edit">
                                <i class='icon-edit' style='font-size:12px; padding-right:0px;'></i></a>

                            <a href="#" class='btn btn-google remove-selected-item'>
                                <i class='icon-trash' style='font-size:12px; padding-right:0px;'></i></a>
                        </td>
                    </tr>

                    <?php 

                        if( isset($deal['DealItemLink']) && $deal['DealItemLink'] ){
                          foreach( $deal['DealItemLink'] as $k => $val ){

                            $original_price = ($val['original_price']!=0)?$val['original_price']:$val['ItemDetail']['price'];

                    ?>  
                      <tr class="item-row" data-id="<?php echo $val['item_id'] ?>">
                        <input type="hidden" name="DealItemLink[item_id][]" class='hidden-item-id' value="<?php echo $val['item_id'] ?>" >
                        <input type="hidden" name="DealItemLink[original_price][]" class='hidden-item-original-price' 
                          value="<?php echo $original_price ?>" >
                        <input type="hidden" name="DealItemLink[discount][]" class='hidden-item-discount' value="<?php echo $val['discount'] ?>" >
                        <input type="hidden" name="DealItemLink[availble_qty][]" class='hidden-item-qty' value="<?php echo $val['available_qty'] ?>" >
                        <td class='item-menu' style="font-weight: bold;"><?php echo $val['ItemDetail']['MenuDetail']['name'] ?></td>
                        <td class='item-title'><?php echo $val['ItemDetail']['title'] ?></td>
                        <td class='item-price'>$ <?php echo number_format($original_price, 2) ?></td>
                        <td class='item-discount-price'>
                            <span>$ <?php echo number_format($val['discount'],2) ?></span>
                            <input style="width:100px; text-align:center; display:none;" 
                                     onkeypress="return numericAndDotOnly(this)"
                                    value="<?php echo $val['discount'] ?>" />
                        </td>
                        <td class='item-available-qty' style="text-align:right">                        
                            <span><?php echo number_format($val['available_qty'],2) ?></span>
                            <input style="width:100px; text-align:center; display:none;" 
                                 onkeypress="return numericAndDotOnly(this)"
                                value="<?php echo ($val['available_qty'])?$val['available_qty']:0 ?>" />
                        </td>
                        <td class='item-action' style="text-align:center">

                            <a href="#" class='btn btn-success save-change' 
                                style="display:none;"
                                data-price="<?php echo $original_price ?>"
                                data-id="<?php echo $val['item_id'] ?>" title="Save Change">
                                <i class='icon-save' style='font-size:12px; padding-right:0px;'></i></a>

                            <a href="#" class='btn btn-linkedin edit-selected-item' 
                                data-id="<?php echo $val['item_id'] ?>" title="Edit">
                                <i class='icon-edit' style='font-size:12px; padding-right:0px;'></i></a>

                            <a href="#" class='btn btn-google remove-selected-item' 
                                data-id="<?php echo $val['item_id'] ?>" title="Remove" >
                                <i class='icon-trash' style='font-size:12px; padding-right:0px;'></i></a>
                        </td>
                    </tr>

                    <?php }
                      }
                     ?>

                    <tr id="row-empty-item">
                      <td colspan="10"><i>Empty Items !</i></td>
                    </tr>

                </tbody>      
            </table>

        </div>
  
        <div class="clearfix"></div>

        <div class="submit" style="margin-top: 40px; clear: both;">
          <a class="btn btn-google cancel"  style="float:left" href="<?php echo $this->Html->url( array('action' => 'index')); ?>">Cancel</a>
          <input class="btn btn-primary" type="submit" value="Save" id="save" style="float:left">
            <div id="loading" class="pull-left" style="display:none;">
                <img src="<?php echo $this->webroot . 'img/ajax-loader.gif' ?>" alt="" style="width:30px;">
                <span style="font-size:15px;">saving...</span>
            </div>
        </div>
          
      </form>

    </div>
  </div>

</div>


<!-- Detail View Menu Item -->
<div id="related-item-modal" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true"
  style=" width:820px; left:38%">
  
    <form action="#">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h3 id="myModalLabel"><i class="icon-list"></i> Item List</h3>
        </div>
        
        <div class="clearfix"></div>

        <div class="span4">
            <label for="business-menu"><?php echo __('Business Menu') ?></label>
            <select name="" id="business-menu">
                <option value=""><?php echo __('-- select a menu --'); ?></option>
                <?php foreach( $business_menus as $key => $val ){ ?>
                    <option value="<?php echo $val['BusinessMenu']['id'] ?>"
                            data-name="<?php echo $val['BusinessMenu']['name'] ?>"><?php echo $val['BusinessMenu']['name'] ?></option>
                <?php } ?>
            </select>


            <label for="business-menu-item"><?php echo __('Menu Item') ?></label>
            <select name="" id="business-menu-item" >
                <option value=""><?php echo __('-- select an item --'); ?></option>
            </select>

        </div>

        <div class="span6">
            
            <div style="float:left; width:215px;">

                <label for="original-price"><?php echo __('Original Price (USD)') ?></label>
                <input type="text" id="original-price" 
                        placeholder="Original Price" readonly="readonly" 
                        style="width:160px; font-weight: bold; font-size:15px;"
                        value="0.00" >
            </div>

            <div style="float:left; width:200px;">
                <label for="discounted-price"><?php echo __('Discounted Price (USD)') ?></label>
                <input type="text" id="discounted-price" placeholder="Discounted Price" onkeypress="return numericAndDotOnly(this)"
                        style="width:160px; font-size:15px; font-weight: bold; color:#06F">
            </div>

            <div style="width:200px;">
                <label for="available-qty"><?php echo __('Available Qty') ?></label>
                <input type="text" id="available-qty" placeholder="Available Qty" onkeypress="return numericAndDotOnly(this)"
                        style="width:160px; font-size:15px; font-weight: bold;">
            </div>
            
        </div>
        
        <div class="clearfix"></div>
        <div class="submit" style=" margin-left:20px; clear: both;">
          <button class="btn btn-google cancel" data-dismiss='modal' type="button">Close</button>
          <button class="btn btn-primary" data-dismiss='modal' type="button" id="select-item-btn">Select Item</button>
        </div>

    </form>

</div>


<div id="remove-item-modal" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true"
  style="">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel" style="color:red"><i class="icon-exclamation"></i> Warning !</h3>
  </div>

  <div class="span6" style="padding-top: 10px; padding-bottom: 20px;">
    
    <h5>Are you sure you want to delete this item?</h5>


    <button class="btn btn-google cancel" data-dismiss='modal' type="button">No</button>
    <button class="btn btn-primary" data-dismiss='modal' type="button" id="confirm-remove">Yes</button>

  </div>
</div> 


<div id="warning-modal" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true"
  style="">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel" style="color:red"><i class="icon-exclamation"></i> Warning !</h3>
  </div>

  <div class="span6" style="padding-top: 10px; padding-bottom: 20px;">
    
    <h5 id="warning-modal"></h5>
    <button class="btn btn-google cancel" data-dismiss='modal' type="button">Close</button>

  </div>
</div> 

<style>
    
    .item-price{
        text-align: right;
        font-size: 13px;
        font-weight: bold;
    }

    .item-discount-price{
        text-align: right;
        color: blue;
        font-weight: bold;
        font-size: 13px;
    }

</style>




<script>

  function numericAndDotOnly(elementRef) {
      
      var keyCodeEntered = (event.which) ? event.which : (window.event.keyCode) ? window.event.keyCode : -1;

    if ((keyCodeEntered >= 48) && (keyCodeEntered <= 57)) {
      return true;
    }   
    // '.' decimal point...
    else if (keyCodeEntered == 46) {
      // Allow only 1 decimal point ('.')...
      if ((elementRef.value) && (elementRef.value.indexOf('.') >= 0))
        return false;
      else
        return true;
    }

    return false;

  }

  $(document).ready(function(){

    var checkIEBrowser = msieversion();
  
    function msieversion() {

            var ua = window.navigator.userAgent;
            var msie = ua.indexOf("MSIE ");
            var ieversion;

            if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer, return version number
                return parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)));
            else                 // If another browser, return 0
                return false;
    }


    $('#save').click( function(){

      var div_loading = $(this).parent().find('div#loading');

      div_loading.show();

      var title = $('#DealTitle').val().trim();
      var from = $('#DealValidFrom').val();
      var to = $('#DealValidTo').val();
      var image = $('#deal_image_add').val();

      if( checkIEBrowser ){

          var error = new Array();

          if( title == "" ){
             error.push('Please enter deal title.');
          }

          if( from == "" ){
            error.push('Please select valid from date.');
          }

          if( to == "" ){
            error.push('Please select valid to date.');
          } 

          if( error.length > 0 ){
            var msg = "";
            $.each( error, function(ind, val){
                msg += val + "<br>";
            }) 

            div_loading.hide();

            $('div#warning-modal').find('h5').html(msg);
            $('div#warning-modal').modal('show');
            return false;

          }
      }

      var max_day_validity = "<?php echo $deal_max_validity ?>";

      if( from != "" && to != "" ){

        var valid_from  = moment(from,'D/M/YYYY');
        var valid_to    = moment(to,'D/M/YYYY');
        var diffDays    = valid_to.diff(valid_from, 'days');

        if( diffDays > max_day_validity ){

          var msg = "Deal Validity is only " + max_day_validity + " days longest." ;

          div_loading.hide();
          $('div#warning-modal').find('h5').text(msg);
          $('div#warning-modal').modal('show');
          return false;

        }

      }

      var div_category      = $('div#DealGoodsCategory');
      var checked  = div_category.find('input[type="checkbox"]').is(':checked');

      if( checked == false ){
          div_loading.hide();
          $('div#warning-modal').find('h5').text("Please select at least one category !");
          $('div#warning-modal').modal('show');
          return false;
      }

      if( totalSelectedItem() == 0 ){
          div_loading.hide();
          $('div#warning-modal').find('h5').text("Please select related items !");
          $('div#warning-modal').modal('show');
          return false;
      }

    })

      $('#datetimepicker1, #datetimepicker2').datetimepicker();
      $('#dealValidFromPicker, #dealValidToPicker').datetimepicker();
      $('#dealValidFromPicker, #dealValidToPicker').find('input').css('cursor', 'pointer');
      $('#dealValidFromPicker, #dealValidToPicker').find('input').attr('readonly','readonly') ;

      Shadowbox.init();

  });

  $('a#add-related-item-btn').click( function(event){

        event.preventDefault();

        var related_item_modal = $('div#related-item-modal');

        related_item_modal.find('select#business-menu-item').val("");
        $('select#business-menu-item').change();

        related_item_modal.modal('show');

    });

    $('select#business-menu').change( function(){
        var me      = $(this),
            menu_id = me.val(),
            limit   = null,
            offset  = 0;

        var select_item         = $('select#business-menu-item'),
            original_price      = $('input#original-price'),
            discounted_price    = $('input#discounted-price');

        var empty_option        = "<option value=''>-- select an item --</option>";
        var loading             = "<option value=''>loading...</option>";

        var getItemURL__ = "<?php echo $this->Html->url( array('controller' => 'Dashboards', 'action' => 'getMenuItemByMenuIDAjax__')); ?>" ;


        if( menu_id != '' ){
            $.ajax({

                url: getItemURL__,
                type: "POST",
                data: { menu_id: menu_id, limit: limit , offset: offset },
                async: false,
                dataType: 'json',

                beforeSend: function(){
                    select_item.html(loading);
                    original_price.val('0.00');
                    discounted_price.val("");
                },

                success: function(data){

                    select_item.html(empty_option);

                    if( data.status == true ){

                        var result = data.result;
                        $.each( result, function(ind, val){
                            var price  = parseFloat(val.BusinessMenuItem.price).toFixed(2);
                            var option = "<option value='" + val.BusinessMenuItem.id + "' data-price='" + price + "' data-title='" + val.BusinessMenuItem.title + "'>"+ val.BusinessMenuItem.title +"</option>"; 


                            select_item.append(option);
                        });

                    }

                },

                error: function(err, msg){
                    console.log(msg)
                }

            });
        }else{
            select_item.html(empty_option);
            original_price.val('0.00');
            discounted_price.val("");
        }

    });

    $('select#business-menu-item').change( function(){
        var me         = $(this),
            item_id    = me.val();

        var original_price      = $('input#original-price'),
            discounted_price    = $('input#discounted-price'),
            available_qty       = $('input#available-qty');

        original_price.val('0.00')
        discounted_price.val('');
        available_qty.val(0);

        if( item_id != '' ){

            var selected   = $('option:selected', me),
                price      = selected.attr('data-price');

            original_price.val(price);
        }
        
    });

    var table_listing  = $('table#table-item-listing').find('tbody');
    var item_row_model = table_listing.find('#item-row-model');
    $('table#table-item-listing').find('tbody').find('#item-row-model').remove();

    var row_empty = $('tr#row-empty-item');
    $('tr#row-empty-item').hide();

    $('#select-item-btn').click( function(){

        var item_id           = $('select#business-menu-item').val(),
            original_price    = $('input#original-price').val(),
            discounted_price  = $('input#discounted-price').val(),
            available_qty     = $('input#available-qty').val() ;

        var err = new Array();

        if( item_id == '' ){
            err.push("Please select an item !");
        }

        if( discounted_price == '' || isNaN(parseFloat(discounted_price)) || parseFloat(discounted_price) == 0 ){
            err.push("Please enter discounted price !");
        }else if( parseFloat(discounted_price) >= parseFloat(original_price) ){
            err.push("Discounted Price must be less than original price !");
        }

        if( table_listing.find('tr[data-id="' + item_id + '"]').length > 0 ){
            err.push("This item is already selected !");
        }       

        if( available_qty == 0 || isNaN(parseFloat(available_qty)) ){
            err.push('Please enter available quantity !') ;
        }

        if( err.length == 0 ){
            row_empty.hide();

            item_row_model.find('.hidden-item-id').val(item_id);
            item_row_model.find('.hidden-item-original-price').val(original_price);
            item_row_model.find('.hidden-item-discount').val(discounted_price);
            item_row_model.find('.hidden-item-qty').val(available_qty);

            // Biz Menu
            var menu_name = $('option:selected', $("select#business-menu")).attr('data-name');
            var item_name = $('option:selected', $('select#business-menu-item')).attr('data-title');

            item_row_model.attr('data-id', item_id);
            item_row_model.find('.item-menu').text(menu_name);
            item_row_model.find('.item-title').text(item_name);
            item_row_model.find('.item-price').text("$ " + parseFloat(original_price).toFixed(2));

            item_row_model.find('.item-discount-price').find('span').text("$ " + parseFloat(discounted_price).toFixed(2));
            item_row_model.find('.item-discount-price').find('input').val(discounted_price);
            
            item_row_model.find('.item-available-qty').find('span').text(parseFloat(available_qty).toFixed(2));
            item_row_model.find('.item-available-qty').find('input').val(available_qty);

            item_row_model.find('.remove-selected-item').attr('data-id', item_id);

            item_row_model.find('.save-change').attr('data-price', original_price)

            table_listing.append(item_row_model.show().clone());

        }else{

            $('div#warning-modal').find('h5').html("");
            $.each( err, function(ind, val){
                $('div#warning-modal').find('h5').append(val + "<br/>" );
            })

            $('div#warning-modal').modal('show');
            return false;
        }        

    })

    $('table#table-item-listing').on('click', '.remove-selected-item', function(event){

        event.preventDefault();

        var confirm_modal = $('div#remove-item-modal');

        confirm_modal.find('#confirm-remove').attr('data-id', $(this).attr('data-id'));

        confirm_modal.modal('show');

    });


    $('table#table-item-listing').on('click', '.edit-selected-item', function(event){

        event.preventDefault();

        var me              = $(this),
            row_tr          = me.closest('tr'),
            
            row_discount    = row_tr.find('.item-discount-price'),
            discount_span   = row_discount.find('span'),
            discount_input  = row_discount.find('input'),

            row_qty         = row_tr.find('.item-available-qty'),
            qty_span        = row_qty.find('span'),
            qty_input       = row_qty.find('input'),

            hidden_discount = row_tr.find('input#hidden-item-discount'),
            hidden_qty      = row_tr.find('input#hidden-item-qty') ;

        var save_btn = row_tr.find('.save-change');

        me.hide();
        save_btn.fadeIn();

        discount_span.hide();
        discount_input.fadeIn();

        qty_span.hide();
        qty_input.fadeIn();

    });

    $('table#table-item-listing').on('click', '.save-change', function(event){
        event.preventDefault();

        var me              = $(this),
            row_tr          = me.closest('tr'),
            
            row_discount    = row_tr.find('.item-discount-price'),
            discount_span   = row_discount.find('span'),
            discount_input  = row_discount.find('input'),

            row_qty         = row_tr.find('.item-available-qty'),
            qty_span        = row_qty.find('span'),
            qty_input       = row_qty.find('input'),

            hidden_discount = row_tr.find('input.hidden-item-discount'),
            hidden_qty      = row_tr.find('input.hidden-item-qty') ;

        var edit_btn = row_tr.find('.edit-selected-item');

        var new_discount    = discount_input.val();
        var new_qty         = qty_input.val();

        var original_price  = me.attr('data-price'); 
        
        var err = new Array();

        if( new_discount == '' || isNaN(parseFloat(new_discount)) || parseFloat(new_discount) == 0 ){
            err.push("Please enter discounted price !");
        }else if( parseFloat(new_discount) >= parseFloat(original_price) ){
            err.push("Discounted Price must be less than original price !");
        }      

        if( new_qty == 0 || isNaN(parseFloat(new_qty)) ){
            err.push('Please enter available quantity !');
        }

        if( err.length == 0 ){
            hidden_discount.val(new_discount);
            hidden_qty.val(new_qty);

            qty_span.text(parseFloat(new_qty).toFixed(2));
            discount_span.text("$ " + parseFloat(new_discount).toFixed(2));

            me.hide();
            edit_btn.fadeIn();

            discount_input.hide();
            discount_span.fadeIn();

            qty_input.hide();
            qty_span.fadeIn();

        }else{

            $('div#warning-modal').find('h5').html("");
            $.each( err, function(ind, val){
                $('div#warning-modal').find('h5').append(val + "<br/>" );
            })

            $('div#warning-modal').modal('show');
            return false;
        } 

    });

    $('#confirm-remove').click( function(){
        var id = $(this).attr('data-id');
        $('#table-item-listing').find('tbody').find('tr[data-id="' + id + '"]').fadeOut().remove();
        countTotalItemSelected();
    });

    function countTotalItemSelected(){
        var count = $('table#table-item-listing').find('tbody').find('tr.item-row').length;
        if(count == 0){
            row_empty.show();
        }
    }

    function totalSelectedItem(){
      return count = $('table#table-item-listing').find('tbody').find('tr.item-row').length;
    }


    $('select#merchant-branch').change( function(event) {
        event.preventDefault();

        var me    = $(this),
            lat   = $(":selected", me).attr('data-lat'),
            lng   = $(":selected", me).attr('data-lng');

        me.closest('div').find('input#latitude').val(lat);
        me.closest('div').find('input#longitude').val(lng);

    }).trigger('change');

</script>