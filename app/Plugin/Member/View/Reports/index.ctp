<?php 
	/********************************************************************************
	    File: Report
	    Author: PHEA RATTANA
	
	    Confidential ABi Technologies property.
	
	  	Changed History:
	  	Date 					Author				Description
	  	2014/June/13 			PHEA RATTANA		Initial
	*********************************************************************************/



	$deal_report = array();
	$deal_report[] = array('Member', 'Total', array('role'=>'style'));

	if( isset($deal_result) && !empty($deal_result) ){
		foreach($deal_result as $key => $data){
			$deal_report[] = array( $data['user']['last_name'] . ' ' . $data['user']['first_name'], intval($data[0]['total']), 'blue' );
		}
	}

	$deal_report = json_encode($deal_report);

	
 ?>

<div class="reports index">
	<div class="row-fluid">		
		<!-- Pie: Box -->
		<div class="span12">

			<!-- Pie: Top Bar -->
			<div class="top-bar">
				<h3><i class="icon-list"></i> Report</h3>
			</div>
			<!-- / Pie: Top Bar -->

			<!-- Pie: Content -->
			<div class="well">


			<?php if ($user_access_level == 1){ ?>

				<legend>Report Filter</legend>

				<form action="" method="POST">

					<div class="span6" style="margin-left:0px;">

						<div class="control-group" style="padding-bottom:15px;">
							<label class="control-label" for="inputNormal"><i class="icon-user"></i>Select Users</label>
							<div class="controls"  style="	width:350px; 
															max-height:80px; 
															height: 80px; 
															border: 1px solid #CCC; 
															padding:10px; 
															padding-top:5px;
															overflow-y: auto;
															margin-bottom:5px;">
								
								 <div class="input checkbox" style="width:80%;">
								 	<input style="float:left; margin-right: 6px;" type="checkbox" name="user[]" 
								 			id='all' 
								 			value="all"
								 			<?php echo(!empty($user_id) && in_array('all', $user_id))?" checked='checked'":"" ?> >
								 	<label for="all">All Members</label>
								 </div>

								  <div class="input checkbox" style="width:80%;">
								 	<input style="float:left; margin-right: 6px;" type="checkbox" name="user[]" 
								 			id='my_report' 
								 			value="<?php echo $my_id ?>" 
								 			<?php echo(!empty($user_id) && in_array( $my_id, $user_id))?" checked='checked'":"" ?>>
								 	<label for="my_report">My Report</label>
								 </div>

								<?php foreach( $users as $key => $user){ ?>

								 <div class="input checkbox" style="width:80%;">
								 	<input style="float:left; margin-right: 6px;" type="checkbox" name="user[]" 
								 			id='<?php echo $user['MemberUser']['id'] ?>' 
								 			value="<?php echo $user['MemberUser']['id'] ?>"
								 			<?php echo(!empty($user_id) && in_array( $user['MemberUser']['id'] , $user_id))?" checked='checked'":"" ?> >
								 	<label for="<?php echo $user['MemberUser']['id'] ?>"><?php echo urldecode($user['MemberUser']['last_name'] . " " . $user['MemberUser']['first_name'] ) ?></label>
								 </div>

								<?php } ?>
							</div>
						</div>	

					</div>
				

					<div class="span6" style="margin-left:0px;">

						<div class="control-group" id="datePickerFrom">
							<label class="control-label" for="inputNormal"><i class="icon-time"></i>From Date</label>
							<div class="controls" style="margin-left:115px;">
								<input data-format="dd/MM/yyyy" type="text" 
							    		name="from_date" id="DealsValidFrom"
							    		style="width: 165px !important"
							    		placeholder="From Date" 
							    		value="<?php echo($from != "")?date('m/d/Y', strtotime($from)):""; ?>"></input>
							    <span class="add-on">
							      <i data-time-icon="icon-time" data-date-icon="icon-calendar">
							      </i>
							    </span>
							</div>
						</div>	

						<div class="control-group" id="datePickerTo">
							<label class="control-label" for="inputNormal"><i class="icon-time"></i>To Date</label>
							<div class="controls" style="margin-left:115px;">
								<input data-format="dd/MM/yyyy" type="text" 
							    		name="to_date" id="DealsValidFrom"
							    		style="width: 165px !important"
							    		placeholder="To Date"
							    		value="<?php echo($to != "")?date('m/d/Y', strtotime($to)):"" ?>" ></input>
							    <span class="add-on">
							      <i data-time-icon="icon-time" data-date-icon="icon-calendar">
							      </i>
							    </span>
							</div>
						</div>	

					</div>


					<div class="span4" style="margin-left:0px;">

						


					</div>

					<div class="submit" style="margin-top: 20px; clear: both; float:right; padding-right:70px;">
					 	<input class="btn btn-primary" type="submit" value="Submit Filter">
					</div>	

				</form>

			<?php }else{ ?>

				<legend>Report Filter</legend>

				<form action="" method="POST">
				

					<div class="span5" style="margin-left:0px;">

						<div class="control-group" id="datePickerFrom">
							<label class="control-label" for="inputNormal"><i class="icon-time"></i>From Date</label>
							<div class="controls" style="margin-left:115px;">
								<input data-format="dd/MM/yyyy" type="text" 
							    		name="from_date" id="DealsValidFrom"
							    		style="width: 165px !important"
							    		placeholder="From Date" 
							    		value="<?php echo($from != "")?date('m/d/Y', strtotime($from)):""; ?>"></input>
							    <span class="add-on">
							      <i data-time-icon="icon-time" data-date-icon="icon-calendar">
							      </i>
							    </span>
							</div>
						</div>	

					</div>


					<div class="span5" style="margin-left:0px;">

						<div class="control-group" id="datePickerTo">
							<label class="control-label" for="inputNormal"><i class="icon-time"></i>To Date</label>
							<div class="controls" style="margin-left:115px;">
								<input data-format="dd/MM/yyyy" type="text" 
							    		name="to_date" id="DealsValidFrom"
							    		style="width: 165px !important"
							    		placeholder="To Date"
							    		value="<?php echo($to != "")?date('m/d/Y', strtotime($to)):"" ?>" ></input>
							    <span class="add-on">
							      <i data-time-icon="icon-time" data-date-icon="icon-calendar">
							      </i>
							    </span>
							</div>
						</div>	


					</div>

					<div class="span2" style="margin-left:0px;">
						<div class="control-group" style="padding-bottom:22px;">
							<input class="btn btn-primary" type="submit" value="Submit Filter">
						</div>
					</div>	

				</form>
			<?php } ?>
	
			<?php if( isset($deal_result) ){ ?>
				
				<legend>Result</legend>

				<div class="span11" id="chart_div_deal" style="height:300px;">
				<?php echo(empty($deal_result))?"<h5>No Report Available.</h5>":"" ?>	
				</div>

			<?php } ?>
				
			</div>
			<!-- / Pie: Content -->

		</div>
		<!-- / Pie -->
		
	</div>

</div>


<script type="text/javascript" src="https://www.google.com/jsapi"></script>

<script>

	$(document).ready(function (){

		$('#datePickerTo, #datePickerFrom').datetimepicker();

	});

	google.load("visualization", "1", {packages:["corechart"]});
  	

  	function drawChartDeal() {

  		var report_daeal = <?php echo $deal_report ?> ;

        var data1 = google.visualization.arrayToDataTable(report_daeal) ;
        var options1 = {
          title: 'Deal Created',
          hAxis: {title: "User's Name", titleTextStyle: {color: 'blue'}},
          colors: ['blue']
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('chart_div_deal'));
        chart.draw(data1, options1);

  	}

</script>


<?php if(isset($deal_result) && !empty($deal_result) ){  ?>
	
	<script>
		if(document.getElementById("chart_div_deal")){
	  		google.setOnLoadCallback(drawChartDeal);	
		}
	</script>

<?php } ?>