<?php
App::uses('MemberAppController', 'Member.Controller');

/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class BuyerTransactionsController extends MemberAppController {

/**
 * Components
 *
 * @var array
 */

  var $context = 'BuyerTransaction' ;

  var $uses = array(
                    "Member.BuyerTransaction",
              );

  var $business_id = 0;
  var $userID = 0;

/**
 * index method
 *
 * @return void
 */
    

    public function beforeFilter(){
      parent::beforeFilter();

        $this->set('status', $this->status);   

        $user = CakeSession::read("Auth.MemberUser");

        $this->userID = $user['id'];
        $biz_id = $user['business_id'];
        $this->business_id = $biz_id;
        $this->set('business_id', $biz_id);

        $this->Business->recursive = -1;
        $info = $this->Business->find('first', array('conditions' => array('Business.id' => $biz_id), 'fields' => array('Business.id', 'Business.member_level') )) ;

        $this->set('business_member_level', $info['Business']['member_level']);

        $this->set('purchase_status', $this->buy_transaction_status);
        $this->set('payment_method',  $this->payment_method);
        $this->set('__CREDIT_TYPE', $this->__CREDIT_PAYMENT );

    }

    public function setCondition(){

        parent::setCondition();

        $status = $this->getState('status', 'all');

        if( isset($this->request->data['status']) ){
            $status = $this->request->data['status'];
        }

        if( $status ){
            $this->setState('status', $status);
        }

        if( isset($this->request->data['code']) ){
            $this->setState('code', $this->request->data['code'] );
        }

        if( isset($this->request->data['purchaseDateFrom']) ){
            $this->setState('purchaseDateFrom', $this->request->data['purchaseDateFrom'] );
        }
        if( isset($this->request->data['purchaseDateTo']) ){
            $this->setState('purchaseDateTo', $this->request->data['purchaseDateTo'] );
        }

        if( isset($this->request->data['receivedDateFrom']) ){
            $this->setState('receivedDateFrom', $this->request->data['receivedDateFrom'] );
        }

        if( isset($this->request->data['receivedDateTo']) ){
            $this->setState('receivedDateTo', $this->request->data['receivedDateTo'] );
        }

        if( $code = $this->getState('code', NULL )){
            $this->conditionFilter['BuyerTransaction.code'] = $code ;            
        }

        if( $purchaseFrom = $this->getState('purchaseDateFrom', NULL ) ){
            $purchaseFrom = date('Y-m-d 00:00:00', strtotime($purchaseFrom));
            $this->conditionFilter['BuyerTransaction.created >='] = $purchaseFrom;
        }

        if( $purchaseTo = $this->getState('purchaseDateTo', NULL ) ){
            $purchaseTo = date('Y-m-d 23:59:59', strtotime($purchaseTo));
            $this->conditionFilter['BuyerTransaction.created <='] = $purchaseTo;
        }

        if( $receivedFrom = $this->getState('receivedDateFrom', NULL ) ){
            $receivedFrom = date('Y-m-d 00:00:00', strtotime($receivedFrom));
            $this->conditionFilter['BuyerTransaction.received_date >='] = $receivedFrom;
        }

        if( $receivedTo = $this->getState('receivedDateTo', NULL ) ){
            $receivedTo = date('Y-m-d 23:59:59', strtotime($receivedTo));
            $this->conditionFilter['BuyerTransaction.received_date <='] = $receivedTo;
        }

        $this->conditionFilter['BuyerTransaction.business_id'] = $this->business_id;

        if( $status != 'all' ){
            $this->conditionFilter['BuyerTransaction.status'] = $status;
        }

        $this->conditionFilter['BuyerTransaction.type'] = 0;
        $this->paginate['conditions'] = $this->conditionFilter;
        $this->paginate['order']      = array( 
                                          'BuyerTransaction.created' => 'DESC',
                                          'TransactionDetail.code' => 'DESC'
                                        );

        $this->set('states', $this->getState());

    }

    public function index(){

        $this->setCondition();

        $data = $this->paginate($this->getContext());  

        // Count Pending
        $pending = $this->BuyerTransaction->find('count', array('conditions' => array('BuyerTransaction.status' => 0, 
                                                                                        'BuyerTransaction.type' => 0, 
                                                                                        'BuyerTransaction.business_id' => $this->business_id)) );

        $this->set('totalPendingPurchase', $pending);
        $this->set('data', $data);

    }

    public function view( $id = 0 ){

        $this->BuyerTransaction->recursive = 2;
        $conds['conditions'] = array('BuyerTransaction.id' => $id, 'BuyerTransaction.business_id' => $this->business_id );
        $data = $this->BuyerTransaction->find('first', $conds);

        if( !$data || empty($data) ){

            $this->Session->setFlash(__( "Invalid Request !" )
                      .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                    'default',
                    array('class'=>'alert alert-dismissable alert-danger '));
        }

        $this->set('data', $data);   
    }


    public function redeem(){

        $search = false;
        $data = array();
        $code = "";

        if( $this->request->is('post') ){
            $code = $this->request->data['coupon-code'];
            $conds['conditions'] = array( 'BuyerTransaction.code' => $code,
                                          'BuyerTransaction.business_id' => $this->business_id );

            $this->BuyerTransaction->recursive = 3;
            $data = $this->BuyerTransaction->find('first', $conds);
            $search = true;
        }

        $this->set('code', $code);
        $this->set('data', $data);
        $this->set('search', $search);
    }

    public function markRedeem(){

        if( $this->request->is('post') ){
            
            $data = $this->request->data;

            $data['BuyerTransaction']['status'] = 1;
            $data['BuyerTransaction']['mark_received_by'] = $this->userID;
            $data['BuyerTransaction']['received_date']    = date('Y-m-d H:i:s');

            if( $this->BuyerTransaction->save($data) ){

                $id = $data['BuyerTransaction']['id'];

                $this->Session->setFlash(__( "Purchase has been redeem !" )
                      .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                    'default',
                    array('class'=>'alert alert-dismissable alert-success '));

                return $this->redirect(array('action' => 'view', $id));

            }

        }

        return $this->redirect(array('action' => 'index'));
    }


}