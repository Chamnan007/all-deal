<?php
App::uses('MemberAppController', 'Member.Controller');
App::uses('Business', 'Member.Model');
App::uses('Deal', 'Member.Model');
App::uses('User', 'Member.Model');

/**
 * Locations Controller
 *
 * @property Location $Location
 * @property PaginatorComponent $Paginator
 */
class ReportsController extends MemberAppController {


  var $uses = array( 'Member.MemberUser', 'Member.Business', 'Member.Deal' ) ;

  public function index(){

    $user = CakeSession::read("Auth.MemberUser");

    $this->set('user_access_level', $user['access_level']);

    $this->set('my_id', $user['id']) ;

    $this->set('user_id', '' ) ;
    $this->set('from', '' ) ;
    $this->set('to', '') ;

    if( $user['access_level'] == 1 ){
      
      $user_conditions = array( 'conditions' => array( 
                    'MemberUser.id != ' . $user['id'],
                    'MemberUser.business_id' => $user['business_id']
                    ),
                  'order' => array(
                      'MemberUser.last_name'  => 'ASC'
                      )
                );

      $this->set("users", $this->MemberUser->find('all', $user_conditions ) );

    }

    if( $this->request->is('post')){

      $user_id = "";

      if( $user['access_level'] == 1 ){
        if(isset($this->request->data['user'])){
          $user_id = $this->request->data['user'];
        }
      }else{
        $user_id = array($user['id']);
      }

      $arr_user = $user_id;

      if( !empty($user_id) && in_array("all", $user_id) ){
        $user_id = "";
      }

      $from   = $this->request->data['from_date'];
      $to   = $this->request->data['to_date'];

      $deal_conditions = array();

      $deal_conditions['conditions']['user.business_id'] = $user['business_id'];
      $deal_conditions['conditions']['Deal.status !='] = "0" ;
      $deal_conditions['conditions']['OR'] = array( 'user.access_level = 2','user.access_level = 1' ) ;

      if( $user_id != "" ){
        $deal_conditions['conditions']['Deal.created_by'] = $user_id;
      }

      if( $from != "" && $to == "" ){
        $from = date('Y-m-d 00:00:00', strtotime($from) );
        $deal_conditions['conditions'][] = array( "Deal.created >='" . $from . "' " );

      }else if ( $from == "" && $to != "" ) {
        $to = date('Y-m-d 23:59:59', strtotime($to) );
        $deal_conditions['conditions'][] = array( "Deal.created <='" . $to . "' " );

      }else if( $from != "" && $to != "" ){ 
        $from = date('Y-m-d 00:00:00', strtotime($from) );
        $to = date('Y-m-d 23:59:59', strtotime($to) );
        $deal_conditions['conditions'][] = array( "Deal.created BETWEEN '" . $from . "' AND '" . $to . "' " );
      } 

      $this->set('user_id', $arr_user);
      $this->set('from', $from);
      $this->set('to', $to);

      $deal_conditions['group'] = array("Deal.created_by");
      $deal_conditions['fields'] = array("Deal.created_by", "COUNT(Deal.id) AS total", "user.first_name", "user.last_name");
      $deal_conditions['order'] = array("user.last_name ASC");


      $deal_conditions['joins'] = array(
              array( 'table' => 'users',
                    'alias' => 'user',
                    'type' => 'LEFT',
                    'conditions' => array(
                              'user.id = Deal.created_by'
                          ),
                    'fields' => array("first_name", 'last_name', 'access_level', 'bsuiness_id')
                )
            );

      $this->Deal->recursive = -1;
      $deal = $this->Deal->find( 'all', $deal_conditions );
      $this->set('deal_result', $deal);

    }

  }

}
