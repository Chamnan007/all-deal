<?php
App::uses('MemberAppController', 'Member.Controller');

/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class BusinessRevenuesController extends MemberAppController {

/**
 * Components
 *
 * @var array
 */
  // public $components = array('Paginator');

  var $context = 'BusinessRevenue' ;

  var $uses = array(
                    "Member.BusinessRevenue",
                    "Member.BuyerTransaction",
              );

  var $business_id = 0;
  var $revenueConditions = array();
  var $balanceConditions = array();

/**
 * index method
 *
 * @return void
 */

  public function beforeFilter(){
      parent::beforeFilter();

      $this->set('status', $this->status);   

      $user = CakeSession::read("Auth.MemberUser");
      $biz_id = $user['business_id'];
      $this->business_id = $biz_id;
      $this->set('business_id', $biz_id);

      $this->Business->recursive = -1;
      $info = $this->Business->find('first', array('conditions' => array('Business.id' => $biz_id) )) ;

      $this->set('business_member_level', $info['Business']['member_level']);
      $this->set('ending_balance', floatval($info['Business']['ending_balance']));
      $this->set('pay_method', $this->pay_method);

  }

  public function setRevenueConditions(){

      $this->context = "BuyerTransaction";

      $default_from = date('Y-m-01');
      $default_to   = date('Y-m-t');

      $from = $default_from;
      $to   = $default_to;
      $default = true;

      if( isset($this->request->data['DateFrom']) ){
          $this->setState('DateFrom', $this->request->data['DateFrom']);
      }
      if( isset($this->request->data['DateTo']) ){
          $this->setState('DateTo', $this->request->data['DateTo']);
      }

      if( $date_from = $this->getState('DateFrom', NULL ) ){
         $from = date('Y-m-d', strtotime($date_from));
      }

      if( $date_to = $this->getState('DateTo', NULL) ){
         $to = date('Y-m-d', strtotime($date_to));
      }

      if( $from != $default_from || $to != $default_to ){
        $default = false;
      }

      $this->revenueConditions['BuyerTransaction.created >='] = $from . " 00:00:00";
      $this->revenueConditions['BuyerTransaction.created <='] = $to . " 23:59:59";
      $this->revenueConditions['BuyerTransaction.business_id']= $this->business_id;

      $this->paginate['conditions'] = $this->revenueConditions;
      $this->paginate['order']      = array( 
                                              'BuyerTransaction.created' => 'DESC',
                                              'DealInfo.created' => 'DESC'
                                      );

      $countConditions['conditions'] = $this->revenueConditions;
      $countConditions['fields'] = array( 'SUM(BuyerTransaction.amount * (100 - BuyerTransaction.commission_percent) / 100 ) AS total' );

      $total = $this->BuyerTransaction->find('first', $countConditions);

      $period_string = "This month revenue";
      
      if( !$default ){
        $period_string = "Revenue from " . date('d-F-Y', strtotime($from)) . " to " . date('d-F-Y', strtotime($to)) ;
      }

      $this->set('period_string', $period_string);
      $this->set('total_revenue', $total[0]['total']);

      $this->set('revenueStates', $this->getState());

  }

  public function index(){

      $this->setRevenueConditions();

      unset($this->paginate['limit']);
      $this->BuyerTransaction->recursive = 3;
      $data = $this->BuyerTransaction->find('all', $this->paginate);
      $this->set(compact('data'));
  }



  public function setBalanceConditions(){

      $this->context = "BusinessRevenue";

      $default_from = date('Y-m-01');
      $default_to   = date('Y-m-t');

      $from = $default_from;
      $to   = $default_to;
      $default = true;

      if( isset($this->request->data['DateFrom']) ){
          $this->setState('DateFrom', $this->request->data['DateFrom']);
      }
      if( isset($this->request->data['DateTo']) ){
          $this->setState('DateTo', $this->request->data['DateTo']);
      }

      if( $date_from = $this->getState('DateFrom', NULL ) ){
         $from = date('Y-m-d', strtotime($date_from));
      }

      if( $date_to = $this->getState('DateTo', NULL) ){
         $to = date('Y-m-d', strtotime($date_to));
      }

      $this->balanceConditions['BusinessRevenue.created >='] = $from . " 00:00:00";
      $this->balanceConditions['BusinessRevenue.created <='] = $to . " 23:59:59";

      $this->balanceConditions['BusinessRevenue.business_id']= $this->business_id;

      $this->paginate['conditions'] = $this->balanceConditions;
      $this->paginate['order']      = array( 
                                              'BusinessRevenue.created' => 'DESC',
                                              // 'DealInfo.created' => 'DESC'
                                      );

      $this->set('balanceStates', $this->getState());

  }

  public function balance(){

      $this->setBalanceConditions();

      unset($this->paginate['limit']);
      $this->BusinessRevenue->recursive = 2;
      $data = $this->BusinessRevenue->find('all', $this->paginate);

      $this->set(compact('data'));

      $deal_ids = array();
      foreach( $data as $ind => $val ){
        if( $val['BusinessRevenue']['payment_id']){
          $ids = explode(",", $val['PaymentInfo']['deal_ids']);
          foreach( $ids as $id ){
            $deal_ids[] = $id;
          }
        }
      }

      // Get Deal Info
      $deaInfo = $this->Deal->find('all', array('conditions' => array('Deal.id' => $deal_ids)) );
      $deal_data = array();
      if( $deaInfo ){
        foreach( $deaInfo as $k => $deal ){
          $deal_data[$deal['Deal']['id']] = $deal['Deal'];
        }
      }

      $this->set('deal_data', $deal_data);
  }


}