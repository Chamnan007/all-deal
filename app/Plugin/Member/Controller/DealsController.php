<?php
App::uses('MemberAppController', 'Member.Controller');
App::uses('Deal', 'Member.Model');

App::uses('Province', 'Member.Model');
App::uses('City', 'Member.Model');
App::uses('Location', 'Member.Model');

App::uses('InterestsCategory', 'Member.Model');
App::uses('ServicesCategory', 'Member.Model');
App::uses('GoodsCategory', 'Member.Model');
App::uses('DiscountCategory', 'Member.Model');
App::uses('Destination', 'Member.Model');
App::uses('Business', 'Member.Model');
App::uses('BusinessMenu', 'Member.Model');

App::uses('DealItemLink', 'Member.Model');

/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class DealsController extends MemberAppController {

/**
 * Components
 *
 * @var array
 */
	// public $components = array('Paginator');

	var $context = 'Deal' ;

	var $uses = array(	"Member.Deal",
						"Member.MemberUser",
						"Member.DealItemLink"
					);


	var $dealWidth 			= 705;
	var $dealHeight 		= 422;
	var $dealThumbWidth 	= 223;
	var $dealThumbHeight 	= 133;
	var $dealImages     	= 5;
	var $business_id 		= 0;
	var $__GETAWAYS 		= 14; // Getaways Category

	var $max_expired_day 	= 7;

/**
 * index method
 *
 * @return void
*/

	public function beforeFilter(){

		parent::beforeFilter();
		
		$user = CakeSession::read("Auth.MemberUser");
		$biz_id = $user['business_id'];
		$this->business_id = $biz_id;
		$this->set('business_id', $biz_id);

		$this->Business->recursive = 2;
		$info = $this->Business->find('first', array('conditions' => array('Business.id' => $biz_id) )) ;

		$this->set('business_member_level', $info['Business']['member_level']);
		$this->set('lat', $info['Business']['latitude']);
		$this->set('lng', $info['Business']['longitude']);

		$this->set('dealImagesSize', $this->dealImages);
		$this->set('deal_status', $this->deal_status);

		$data_branches = array();
		$data_branches[0]['id'] 		 = 0;
		$data_branches[0]['branch_name'] = "Any branch";
		$data_branches[0]['latitude']	 = $info['Business']['latitude'];
		$data_branches[0]['longitude']	 = $info['Business']['longitude'];

		if( $branches = $info['Branches'] ){
			foreach( $branches as $k => $val ){
				$index = $k + 1;
				$data_branches[$index]['id'] 			= $val['id'];
				$data_branches[$index]['branch_name'] 	= $val['branch_name'];
				$data_branches[$index]['latitude']		= $val['latitude'];
				$data_branches[$index]['longitude']		= $val['longitude'];
			}
		}

		$this->set('merchant_branches', $data_branches);
		$this->set('deal_max_validity', $this->max_expried_day);

      $this->set('__DEAL_WIDTH', $this->dealWidth);
      $this->set('__DEAL_HEIGHT', $this->dealHeight);
	}

	public function setCondition(){

		parent::setCondition();

		$data = $this->request->data;

		$status = $this->getState('status', 'all');

        if( isset($data['status']) ){
            $status = $data['status'];
        }

       	$this->setState('status', $status);

       	$last_minute = 0;

       	if( isset($data['last-minute']) ){	
       		$last_minute = 1;
       	}

       	$this->setState('last-minute', $last_minute);

        if( isset($data['deal-title']) ){
        	$this->setState('deal-title', $data['deal-title']);
        }

        if( isset($data['deal-category']) ){
        	$this->setState('deal-category', $data['deal-category']);
        }

        if( isset($data['postedFrom']) ){
        	$this->setState('postedFrom', $data['postedFrom']);
        }
        if( isset($data['postedTo']) ){
        	$this->setState('postedTo', $data['postedTo']);
        }

        if( isset($data['validFrom']) ){
        	$this->setState('validFrom', $data['validFrom']);
        }
        if( isset($data['validTo']) ){
        	$this->setState('validTo', $data['validTo']);
        }

        if( isset($data['publishedFrom']) ){
        	$this->setState('publishedFrom', $data['publishedFrom']);
        }
        if( isset($data['publishedTo']) ){
        	$this->setState('publishedTo', $data['publishedTo']);
        }

        $statusCondtions = array();
        $now = date('Y-m-d 23:59:59');
        
        if( $status != 'all' ){
        	$statusCondtions['Deal.status'] = $status;
        }

        if( $status == -1 ){
        	$statusCondtions['OR']['Deal.valid_to <'] = $now ;
        	$this->conditionFilter['OR'] = $statusCondtions;

        }else if( $status == 1 ){
        	$now = date('Y-m-d 23:59:59');
        	$statusCondtions['Deal.valid_to >='] 	= $now ;
        	$this->conditionFilter['AND'] = $statusCondtions;
        }else if( !is_null($status) && $status == 0 ){
        	$this->conditionFilter['AND'] = $statusCondtions;
        }

        if( $title = $this->getState('deal-title', NULL )){
        	$this->conditionFilter['Deal.title LIKE'] = '%' . $title .'%'; 
        }

        if( $deal_cate = $this->getState('deal-category', NULL ) ) {
        	$this->conditionFilter['Deal.goods_category LIKE'] = '%"' . $deal_cate . '"%';
        }

        if( $postedFrom = $this->getState('postedFrom') ){
        	$postedFrom = date('Y-m-d 00:00:00', strtotime($postedFrom));
        	$this->conditionFilter['Deal.created >='] = $postedFrom;
        }

        if( $postedTo = $this->getState('postedTo') ){
        	$postedTo = date('Y-m-d 23:59:59', strtotime($postedTo));
        	$this->conditionFilter['Deal.created <='] = $postedTo;
        }

        if( $validFrom = $this->getState('validFrom') ){
        	$validFrom = date('Y-m-d 00:00:00', strtotime($validFrom));
        	$this->conditionFilter['Deal.valid_from >='] = $validFrom;
        }
        if( $validTo = $this->getState('validTo') ){
        	$validTo = date('Y-m-d 23:59:59', strtotime($validTo));
        	$this->conditionFilter['Deal.valid_to <='] = $validTo;
        }

        if( $publishedFrom = $this->getState('publishedFrom') ){
        	$publishedFrom = date('Y-m-d 00:00:00', strtotime($publishedFrom));
        	$this->conditionFilter['Deal.publish_date >='] = $publishedFrom;
        }
        if( $publishedTo = $this->getState('publishedTo') ){
        	$publishedTo = date('Y-m-d 23:59:59', strtotime($publishedTo));
        	$this->conditionFilter['Deal.publishedTo <='] = $publishedTo;
        }

        $last_minute = $this->getState('last-minute', 0);

        if( $last_minute == 1 ){
        	$this->conditionFilter['Deal.is_last_minute_deal'] = $last_minute;
        }

		$this->conditionFilter['Deal.business_id'] 	= $this->business_id;
		$this->conditionFilter['Deal.isdeleted'] 	= 0 ;

	 	$this->paginate['conditions'] = $this->conditionFilter;
	 	$this->paginate['order'] 	  = array( 'Deal.created' => 'DESC' );
	 	
		$this->set('states', $this->getState());

	}

	public function index(){

		$this->setCondition();

		$user = CakeSession::read("Auth.MemberUser");
		$this->set('access_level', $user['access_level']);

		$this->set('expand_request_status', $this->expand_request_status);

		$business_id = $this->business_id;

		parent::index();
		
		// Goods Category
		$goods = new GoodsCategory();
		$this->set('goods', $goods->find('all', array(  'conditions'=> array('GoodsCategory.status' => 1), 
														'order' => array('GoodsCategory.category' => 'ASC' ) )));	

	}

	public function add(){

		$error = "";
		
		// Goods Category
		$goods = new GoodsCategory();
		$this->set('goods', $goods->find('all', array(  'conditions'=> array('GoodsCategory.status' => 1), 
																'order' => array('GoodsCategory.category' => 'ASC' ) )));
		// Business Menu Category
		$menuObj = new BusinessMenu();
		$this->set('business_menus', $menuObj->find('all', array('conditions'=> array('status' => 1, 'business_id' => $this->business_id), 'order' => array('name' => 'ASC'))));

		$user = CakeSession::read("Auth.MemberUser");
		$bis_id = $user['business_id'];

		if ($this->request->is('post')) {

			$data = $this->request->data;

			$this->request->data['Deal']['business_id'] = $bis_id ;
			$this->request->data['Deal']['title_2']   = "";
			$this->request->data['Deal']['en_barcode']  = 0 ;
			$this->request->data['Deal']['status']      = 0 ;
			$this->request->data['Deal']['isdeleted']    = 0 ;

			if(!isset($this->request->data['Deal']['qr_code'])){
				$this->request->data['Deal']['qr_code']   = 0 ;
			}

			$this->request->data['Deal']['created_by']    = $user['id'];
			$this->request->data['Deal']['updated_by']    = $user['id'];

			$this->request->data['Deal']['discount_category'] = "";
			$this->request->data['Deal']['interest_category'] = json_encode((isset($this->request->data['Deal']['interest_category']))?$this->request->data['Deal']['interest_category']:"" );
			$this->request->data['Deal']['services_category'] = json_encode((isset($this->request->data['Deal']['services_category']))?$this->request->data['Deal']['services_category']:"" );
			$this->request->data['Deal']['goods_category'] 	  = json_encode((isset($this->request->data['Deal']['goods_category']))?$this->request->data['Deal']['goods_category']:"" );


			$this->request->data['Deal']['valid_from'] = date('Y-m-d H:i:s', strtotime($this->request->data['Deal']['valid_from']));
			$this->request->data['Deal']['valid_to'] = date('Y-m-d H:i:s', strtotime($this->request->data['Deal']['valid_to']));

			if( isset($this->request->data['Deal']['travel_destination']) ){
				$this->request->data['Deal']['travel_destination'] = json_encode($this->request->data['Deal']['travel_destination']);
			}else{
				$this->request->data['Deal']['travel_destination'] = '[""]';
			}

			if( isset($this->request->data['Deal']['is_last_minute_deal']) ) {
				$this->request->data['Deal']['is_last_minute_deal'] = 1;
			}else{
				$this->request->data['Deal']['is_last_minute_deal'] = 0;
			}

			$image = $_FILES['deal_image'] ;

			if( $image['name'][0] != "" ){

				$dir = 'img/deals' ;

				if( !is_dir($dir) ){
					mkdir($dir, 0700);
				}

				$data_img = array();

				foreach( $image['name'] as $k => $img ){

						$images = array();
						$images['name']     = $img;
						$images['type']     = $image['type'][$k];
						$images['tmp_name'] = $image['tmp_name'][$k];
						$images['error']    = $image['error'][$k];
						$images['size']     = $image['size'][$k];

						$fileName = $this->saveMultiple($images, $dir, array() ,  $this->dealWidth, $this->dealHeight);

						if( is_array($fileName) && $fileName['status'] ==  false ){
							$error = $fileName['msg'];
							$fileName = "img/deals/default.jpg";
						}else{
							$thumb_dir = 'img/deals/thumbs' ;

							if( !is_dir($thumb_dir) ){
								mkdir($thumb_dir, 0755);
							}

							$split = end(explode("/", $fileName));
							$thumb_img =  $thumb_dir . "/" . $split ;
							copy ( $fileName , $thumb_img ) ; 

							// Resize Images
							$this->Image->prepare( $fileName );
							$this->Image->resize( $this->dealWidth , $this->dealHeight );
							$this->Image->save( $fileName );

							// Copy a thumbnail
							$this->Image->prepare( $thumb_img );
							$this->Image->resizeThumbnail($this->dealThumbWidth, $this->dealThumbHeight);
							$this->Image->save( $thumb_img );

						}

						if( $k == 0 ){
							$this->request->data['Deal']['image'] = $fileName;
						}
						
						$data_img[] = $fileName;						
				}

				$this->request->data['Deal']['other_images'] = json_encode($data_img);

			}else{
				$this->request->data['Deal']['image']        = 'img/deals/default.jpg' ;
				$this->request->data['Deal']['other_images'] = NULL;
			}

			$this->Deal->create();			
			if( $this->Deal->save($this->request->data)){

				if( isset($this->request->data['DealItemLink']) ){
					$deal_id = $this->Deal->getLastInsertID();

					$deal_code 	= "D" . str_pad($deal_id, 6, 0, STR_PAD_LEFT);
					$updateData = array();
					$updateData['Deal']['id'] 		= $deal_id;
					$updateData['Deal']['deal_code']= $deal_code;
					$this->Deal->save($updateData);

					$datas = $this->request->data['DealItemLink']['item_id'];
					$item_data = array();
					foreach( $datas as $key => $val ){
						$temp = array();
						$temp['DealItemLink']['deal_id']        = $deal_id;
						$temp['DealItemLink']['item_id']        = $val;
						$temp['DealItemLink']['original_price'] = $this->request->data['DealItemLink']['original_price'][$key];
						$temp['DealItemLink']['discount']       = $this->request->data['DealItemLink']['discount'][$key];
						$temp['DealItemLink']['available_qty'] 	= $this->request->data['DealItemLink']['availble_qty'][$key];
						$temp['DealItemLink']['status']         = 1;

						$item_data[] = $temp;
					}

					$this->DealItemLink->saveAll($item_data);
				}

				$this->Session->setFlash(__( 'New Deal has been created. ' . $error )
											.'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
										'default',
										array('class'=>'alert alert-dismissable alert-success '));

				$logMessage = json_encode($this->request->data);

				$this->generateLog($logMessage,' CREATE NEW ');       
				
			}else{
				$this->Session->setFlash(__( $this->context.' could not be created. ' . $error )
											.'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
										'default',
										array('class'=>'alert alert-dismissable alert-danger '));
			}
			return $this->redirect(array('action' =>'index'));
		}

	}


	public function edit( $id = 0 ){

		$user = CakeSession::read("Auth.MemberUser");

		$bis_id = $user['business_id'];

		$this->Deal->id = $id;

		$this->Deal->recursive = 3;
		$detail = $this->Deal->find('first', array('conditions' => array('Deal.id' => $id, 'Deal.business_id' => $bis_id) ) );

		if (!$detail || empty($detail) ) {
			$this->Session->setFlash(__( "Invalid Deal Request !" )
											.'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
										'default',
										array('class'=>'alert alert-dismissable alert-danger '));

			return $this->redirect(array('action' =>'index'));
		}

		if( $detail['Deal']['status'] == 1 ){
			return $this->redirect(array('action' =>'index'));
		}

		$old_image = $detail['Deal']['image'];

		if ($this->request->is('post')) {

			$this->request->data['Deal']['id'] = $id;
			$this->request->data['Deal']['updated_by']  = $user['id'];
			$this->request->data['Deal']['interest_category'] = json_encode((isset($this->request->data['Deal']['interest_category']))?$this->request->data['Deal']['interest_category']:"" );
			$this->request->data['Deal']['services_category'] = json_encode((isset($this->request->data['Deal']['services_category']))?$this->request->data['Deal']['services_category']:"" );
			$this->request->data['Deal']['goods_category'] = json_encode((isset($this->request->data['Deal']['goods_category']))?$this->request->data['Deal']['goods_category']:"" );

			// Get Business
			$bizConds['conditions'] = array('Business.id' => $bis_id);
			$bizConds['fields'] 	= array('Business.business_main_category');
			$bisInfo = $this->Business->find('first', $bizConds);
			
			$this->request->data['Deal']['valid_from'] = date('Y-m-d H:i:s', strtotime($this->request->data['Deal']['valid_from']));
			$this->request->data['Deal']['valid_to'] = date('Y-m-d H:i:s', strtotime($this->request->data['Deal']['valid_to']));

			if( isset($this->request->data['Deal']['travel_destination']) ){
				$this->request->data['Deal']['travel_destination'] = json_encode($this->request->data['Deal']['travel_destination']);
			}else{
				$this->request->data['Deal']['travel_destination'] = '[""]';
			}

			if(!isset($this->request->data['Deal']['qr_code'])){
				$this->request->data['Deal']['qr_code']   = 0 ;
			}

			if( isset($this->request->data['Deal']['is_last_minute_deal']) ) {
				$this->request->data['Deal']['is_last_minute_deal'] = 1;
			}else{
				$this->request->data['Deal']['is_last_minute_deal'] = 0;
			}

			$err = "";

			if( $this->Deal->save($this->request->data)){

				// Delete Old relate and add new related
				$deal_id = $id;
				
				if( $this->DealItemLink->deleteAll(array('DealItemLink.deal_id' => $deal_id))){
						if( isset($this->request->data['DealItemLink']) ){

								$datas = $this->request->data['DealItemLink']['item_id'];
								$item_data = array();
								foreach( $datas as $key => $val ){
									$temp = array();
									$temp['DealItemLink']['deal_id']  		= $deal_id;
									$temp['DealItemLink']['item_id']  		= $val;
									$temp['DealItemLink']['original_price'] = $this->request->data['DealItemLink']['original_price'][$key];
									$temp['DealItemLink']['discount'] 		= $this->request->data['DealItemLink']['discount'][$key];
									$temp['DealItemLink']['available_qty'] 	= $this->request->data['DealItemLink']['availble_qty'][$key];
									$temp['DealItemLink']['status'] 		= 1;

									$item_data[] = $temp;
								}

								$this->DealItemLink->saveAll($item_data);
						}
				}

				$this->Session->setFlash(__( 'Deal Information has been saved. ' . $err)
											.'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
										'default',
										array('class'=>'alert alert-dismissable alert-success '));

				$logMessage = json_encode($this->request->data);

				$this->generateLog($logMessage,' CREATE NEW ');       
				
			}else{
				$this->Session->setFlash(__( 'Deal Information could not be saved. ' . $err)
											.'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
										'default',
										array('class'=>'alert alert-dismissable alert-danger '));
			}
			return $this->redirect(array('action' =>'index'));

		}else{


			// Business Menu Category
			$menuObj = new BusinessMenu();
			$this->set('business_menus', $menuObj->find('all', array('conditions'=> array('status' => 1, 'business_id' => $this->business_id), 'order' => array('name' => 'ASC'))));

			$discount = new DiscountCategory();
			$this->set('discounts', $discount->find('all', array( 'conditions'=> array('DiscountCategory.status' => 1), 
																	'order' => array('DiscountCategory.category' => 'ASC' ) )));
			$interest = new InterestsCategory();
			$this->set('interests', $interest->find('all', array( 'conditions'=> array('InterestsCategory.status' => 1), 
																	'order' => array('InterestsCategory.category' => 'ASC' ) )));
			$goods = new GoodsCategory();
			$this->set('goods', $goods->find('all', array(  'conditions'=> array('GoodsCategory.status' => 1), 
																	'order' => array('GoodsCategory.category' => 'ASC' ) )));
			$services = new ServicesCategory();
			$this->set('services', $services->find('all', array(  'conditions'=> array('ServicesCategory.status' => 1),
																	'order' => array('ServicesCategory.category' => 'ASC' ) )));

			
			$destination = new Destination();
			$this->set('travelDestination', $destination->find('all', array('conditions'=> array('status' => 1), 'order' => 'destination ASC' )));

			$this->set('deal', $detail);
		}

	}



	public function repost( $id = 0 ){

		$user = CakeSession::read("Auth.MemberUser");

		$bis_id = $user['business_id'];

		$this->Deal->id = $id;

		$this->Deal->recursive = 3;
		$detail = $this->Deal->find('first', array('conditions' => array('Deal.id' => $id, 'Deal.business_id' => $bis_id, 'Deal.isdeleted' => 0) ) );

		if (!$detail || empty($detail) ) {
			$this->Session->setFlash(__( "Invalid Deal Request !" )
											.'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
										'default',
										array('class'=>'alert alert-dismissable alert-danger '));

			return $this->redirect(array('action' =>'add'));
		}

		if ($this->request->is('post')) {

			$data = $this->request->data;

			$this->request->data['Deal']['business_id'] = $bis_id ;
			$this->request->data['Deal']['title_2']   	= "";
			$this->request->data['Deal']['en_barcode']  = 0 ;
			$this->request->data['Deal']['status']      = 0 ;
			$this->request->data['Deal']['isdeleted']   = 0 ;

			if(!isset($this->request->data['Deal']['qr_code'])){
				$this->request->data['Deal']['qr_code']   = 0 ;
			}

			$this->request->data['Deal']['created_by']    = $user['id'];
			$this->request->data['Deal']['updated_by']    = $user['id'];

			$this->request->data['Deal']['discount_category'] = "";
			$this->request->data['Deal']['interest_category'] = json_encode((isset($this->request->data['Deal']['interest_category']))?$this->request->data['Deal']['interest_category']:"" );
			$this->request->data['Deal']['services_category'] = json_encode((isset($this->request->data['Deal']['services_category']))?$this->request->data['Deal']['services_category']:"" );
			$this->request->data['Deal']['goods_category'] = json_encode((isset($this->request->data['Deal']['goods_category']))?$this->request->data['Deal']['goods_category']:"" );


			$this->request->data['Deal']['valid_from'] = date('Y-m-d H:i:s', strtotime($this->request->data['Deal']['valid_from']));
			$this->request->data['Deal']['valid_to'] = date('Y-m-d H:i:s', strtotime($this->request->data['Deal']['valid_to']));

			if( isset($this->request->data['Deal']['travel_destination']) ){
				$this->request->data['Deal']['travel_destination'] = json_encode($this->request->data['Deal']['travel_destination']);
			}else{
				$this->request->data['Deal']['travel_destination'] = '[""]';
			}

			if( isset($this->request->data['Deal']['is_last_minute_deal']) ) {
				$this->request->data['Deal']['is_last_minute_deal'] = 1;
			}else{
				$this->request->data['Deal']['is_last_minute_deal'] = 0;
			}

			$image = $_FILES['deal_image'] ;

			$dir = 'img/deals' ;
			$thumb_dir = 'img/deals/thumbs' ;

			if( !is_dir($thumb_dir) ){
				mkdir($thumb_dir, 0700);
			}

			if( $image['name'][0] != "" ){

				$dir = 'img/deals' ;

				if( !is_dir($dir) ){
					mkdir($dir, 0700);
				}

				$data_img = array();

				foreach( $image['name'] as $k => $img ){

						$images = array();
						$images['name']     = $img;
						$images['type']     = $image['type'][$k];
						$images['tmp_name'] = $image['tmp_name'][$k];
						$images['error']    = $image['error'][$k];
						$images['size']     = $image['size'][$k];

						$fileName = $this->saveMultiple($images, $dir, array() ,  $this->dealWidth, $this->dealHeight);

						if( is_array($fileName) && $fileName['status'] ==  false ){
							$error = $fileName['msg'];
							$fileName = "img/deals/default.jpg";
						}else{
							$thumb_dir = 'img/deals/thumbs' ;

							if( !is_dir($thumb_dir) ){
								mkdir($thumb_dir, 0755);
							}

							$split = end(explode("/", $fileName));
							$thumb_img =  $thumb_dir . "/" . $split ;
							copy ( $fileName , $thumb_img ) ; 

							// Resize Images
							$this->Image->prepare( $fileName );
							$this->Image->resize( $this->dealWidth , $this->dealHeight );
							$this->Image->save( $fileName );

							// Copy a thumbnail
							$this->Image->prepare( $thumb_img );
							$this->Image->resizeThumbnail($this->dealThumbWidth, $this->dealThumbHeight);
							$this->Image->save( $thumb_img );

						}

						if( $k == 0 ){
								$this->request->data['Deal']['image'] = $fileName;
						}
						
						$data_img[] = $fileName;						
				}

				$this->request->data['Deal']['other_images'] = json_encode($data_img);


			}else{

				if( isset($this->request->data['existed_images']) ){

					$existed_imgs = $this->request->data['existed_images'];
					unset($this->request->data['existed_images']);
					$imgs = array();

					foreach( $existed_imgs as $k => $img ){
						$pos = strpos($img, ".");
						$ext = substr($img, $pos + 1, strlen($img));

						$name = substr($img, 0, $pos);
						$name = $name . time();
						$file = $name . "." . $ext;

						$this->request->data['Deal']['image'] = $file;

						// Copy Big Image
						copy ( $img , $file ) ; 

						$thumb_img 		= str_replace("/deals/", "/deals/thumbs/", $img ) ;
						$new_thmb_img  	= str_replace("/deals/", "/deals/thumbs/", $file ) ;
						// Copy Thumb Image
						copy ( $thumb_img , $new_thmb_img ) ; 

						$imgs[] = $file;
					}

					$this->request->data['Deal']['other_images'] = json_encode($imgs);
				}else{
					$this->request->data['Deal']['image'] = 'img/deals/default.jpg' ;
				}

			}

			$err = "";
			$this->Deal->create();

			if( $this->Deal->save($this->request->data)){

				$deal_id = $this->Deal->getLastInsertID();

				$deal_code 	= "D" . str_pad($deal_id, 6, 0, STR_PAD_LEFT);
				$updateData = array();
				$updateData['Deal']['id'] 		= $deal_id;
				$updateData['Deal']['deal_code']= $deal_code;
				$this->Deal->save($updateData);				

				// Delete Old relate and add new related
				if( isset($this->request->data['DealItemLink']) ){

					$datas = $this->request->data['DealItemLink']['item_id'];
					$item_data = array();
					foreach( $datas as $key => $val ){
						$temp = array();
						$temp['DealItemLink']['deal_id']  		= $deal_id;
						$temp['DealItemLink']['item_id']  		= $val;
						$temp['DealItemLink']['original_price'] = $this->request->data['DealItemLink']['original_price'][$key];
						$temp['DealItemLink']['discount'] 		= $this->request->data['DealItemLink']['discount'][$key];
						$temp['DealItemLink']['available_qty'] 	= $this->request->data['DealItemLink']['availble_qty'][$key];
						$temp['DealItemLink']['status'] 		= 1;

						$item_data[] = $temp;
					}

					$this->DealItemLink->saveAll($item_data);
				}
				

				$this->Session->setFlash(__( 'Deal Information has been saved. ' . $err)
											.'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
										'default',
										array('class'=>'alert alert-dismissable alert-success '));

				$logMessage = json_encode($this->request->data);

				$this->generateLog($logMessage,' CREATE NEW ');       
				
			}else{
				$this->Session->setFlash(__( 'Deal Information could not be saved. ' . $err)
											.'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
										'default',
										array('class'=>'alert alert-dismissable alert-danger '));
			}
			return $this->redirect(array('action' =>'index'));

		}else{


			// Business Menu Category
			$menuObj = new BusinessMenu();
			$this->set('business_menus', $menuObj->find('all', array('conditions'=> array('status' => 1, 'business_id' => $this->business_id), 'order' => array('name' => 'ASC'))));

			$discount = new DiscountCategory();
			$this->set('discounts', $discount->find('all', array( 'conditions'=> array('DiscountCategory.status' => 1), 
																	'order' => array('DiscountCategory.category' => 'ASC' ) )));
			$interest = new InterestsCategory();
			$this->set('interests', $interest->find('all', array( 'conditions'=> array('InterestsCategory.status' => 1), 
																	'order' => array('InterestsCategory.category' => 'ASC' ) )));
			$goods = new GoodsCategory();
			$this->set('goods', $goods->find('all', array(  'conditions'=> array('GoodsCategory.status' => 1), 
																	'order' => array('GoodsCategory.category' => 'ASC' ) )));
			$services = new ServicesCategory();
			$this->set('services', $services->find('all', array(  'conditions'=> array('ServicesCategory.status' => 1),
																	'order' => array('ServicesCategory.category' => 'ASC' ) )));

			
			$destination = new Destination();
			$this->set('travelDestination', $destination->find('all', array('conditions'=> array('status' => 1), 'order' => 'destination ASC' )));

			$this->set('deal', $detail);
		}

	}

	public function view( $id = 0 ){

		$user = CakeSession::read("Auth.MemberUser");

		$bis_id = $user['business_id'];
		$this->Deal->id = $id;

		$this->Deal->recursive = 2;
		$detail = $this->Deal->find('first', array('conditions' => array('Deal.id' => $id, 'Deal.business_id' => $bis_id) ) );

		if (!$detail || empty($detail) ) {
			// throw new NotFoundException(__('Invalid business'));
			$this->Session->setFlash(__( "Invalid Deal Request !" )
											.'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
										'default',
										array('class'=>'alert alert-dismissable alert-danger '));

			return $this->redirect(array('action' =>'index'));
		}

		$goods = new GoodsCategory();
		$this->set('goods', $goods->find('all', array(  'conditions'=> array('GoodsCategory.status' => 1), 
																'order' => array('GoodsCategory.category' => 'ASC' ) )));

		// $deal = $this->Deal->findById($id);
		$this->set('deal', $detail);

	}

	public function deleteDeal( $id = 0 ){


		$user = CakeSession::read("Auth.MemberUser");

		if( $user['access_level'] != 1 ){
			$this->redirect( array('action' => 'index') );
		}

		$bis_id = $user['business_id'];

		$this->Deal->id = $id;

		if( $this->request->is('post') ){
			if (!$this->Deal->find('first', array('conditions' => array('Deal.id' => $id, 'Deal.business_id' => $bis_id) ) )) {
				
				$this->Session->setFlash(__( "Invalid Deal Request !" )
												.'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
											'default',
											array('class'=>'alert alert-dismissable alert-danger '));

				return $this->redirect(array('action' =>'index'));
			}


			$this->Deal->id = $id;

			$detail = $this->Deal->findById($id);

			if( !$detail || empty($detail) ){
				 $this->Session->setFlash(__( 'Invalid Request.')
												.'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
											'default',
											array('class'=>'alert alert-dismissable alert-danger '));
					$this->redirect(array('action' => 'index')); 
			}

			$img = $detail['Deal']['image'];

			if( $this->Deal->delete($id)){

				$thumb_dir = "img/deals/thumbs";

				if( $img != "img/deals/default.jpg" && file_exists($img) ){
						unlink($img);

						$split = end(explode("/", $img));
						$thumb_img =  $thumb_dir . "/" . $split ;

						if( file_exists($thumb_img)){
							unlink($thumb_img);
						} 

				}

				$this->Session->setFlash(__( 'Deal has been deleted.')
												.'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
											'default',
											array('class'=>'alert alert-dismissable alert-success '));
			} else {
				$this->Session->setFlash(__( 'Deal could not be deleted.')
												.'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
											'default',
											array('class'=>'alert alert-dismissable alert-danger '));
			}
		}else{
			$this->Session->setFlash(__( 'Invalid Request !')
												.'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
											'default',
											array('class'=>'alert alert-dismissable alert-danger '));
		}

		return $this->redirect(array('action' => 'index' ));
		
	}

	public function changeDealImage( $id = 0 ){

		$this->Deal->id = $id;

		if (!$this->Deal->exists($id)) {
			// throw new NotFoundException(__('Invalid business'));
			$this->Session->setFlash(__( "Invalid Deal Request !" )
											.'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
										'default',
										array('class'=>'alert alert-dismissable alert-danger '));

			return $this->redirect(array('action' =>'index'));
		}

		$old = $this->Deal->findById($id);
		$old_image = $old['Deal']['image'];

		$user = CakeSession::read("Auth.MemberUser");

		$data['Deal']['id'] = $id;
		$data['Deal']['updated_by'] = $user['id'];

		$image = $_FILES['deal_image'] ;

		$dir = 'img/deals' ;
		$thumb_dir = 'img/deals/thumbs' ;

		if( $image['name'] != "" ){

			if( !is_dir($dir) ){
				mkdir($dir, 0700);
			}

			$fileName = $this->saveFile('deal_image', $dir , array() ,  $this->dealWidth, $this->dealHeight );

			if( is_array($fileName) && $fileName['status'] ==  false ){
				$this->Session->setFlash(__( 'Image could not be changed. ' . $fileName['msg'] )
											.'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
										'default',
										array('class'=>'alert alert-dismissable alert-danger '));
				return $this->redirect(array('action' =>'index'));
			}


			$data['Deal']['image'] = $fileName;

			if( !is_dir($thumb_dir) ){
				mkdir($thumb_dir, 0700);
			}

			$split = end(explode("/", $fileName));
			$thumb_img =  $thumb_dir . "/" . $split ;
			copy ( $fileName , $thumb_img ) ; 

			// Resize Images
			$this->Image->prepare( $fileName );
			$this->Image->resize( $this->dealWidth, $this->dealHeight );
			$this->Image->save( $fileName );

			// Copy a thumbnail
				$this->Image->prepare( $thumb_img );
			$this->Image->resizeThumbnail($this->dealThumbWidth, $this->dealThumbHeight);
			$this->Image->save( $thumb_img );
			
		}else{
			$data['Deal']['image'] = 'img/deals/default.jpg' ;
		}

		if( $this->Deal->save($data)){

			if( $old_image != "img/deals/default.jpg" ){
				unlink($old_image);

				$split = end(explode("/", $old_image));
				$thumb_img =  $thumb_dir . "/" . $split ;

				if( file_exists($thumb_img)){
					unlink($thumb_img);
				}       
			}

			$this->generateLog("" ,' CHANGE IMAGE TO DEAL');

			$this->Session->setFlash(__( 'Image has been changed.')
											.'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
										'default',
										array('class'=>'alert alert-dismissable alert-success '));

		}else{
			$this->Session->setFlash(__( 'Image could not be changed.')
											.'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
										'default',
										array('class'=>'alert alert-dismissable alert-danger '));
		}

		return $this->redirect(array('action' =>'index'));


	}


	// Get Deal Images
	public function getDealImagesAjax__ ( $deal_id = 0 ){
		if( $deal_id == 0 ){
				$data['status'] = false;
				$data['message'] = 'Something went wrong ! Please try again.';

				echo json_encode($data); exit;
		}

		$this->Deal->recursive = -1;
		$deal_data = $this->Deal->findById($deal_id);

		if( $deal_data ){
				$data_image = array();
				$images = $deal_data['Deal']['other_images'];

				if( $images ){
					$other_images = json_decode($images);
					foreach( $other_images as $k => $val ){
						if( $val != $this->dealDeafultImage ){
								$data_image[] = $val;
						}
					}
				}else if($deal_data['Deal']['image'] != $this->dealDeafultImage ) {
					$data_image[] = $deal_data['Deal']['image'];
				}

				$data['status'] = true ;
				$data['deal_images'] = $data_image;
				
		}else{
				$data['status']     = false;
				$data['message']    = "Invalid Request !";
		}

		echo json_encode($data); exit;

}


public function uploadDealImageAjax__( $bis_id = 0, $deal_id = 0 ){

	$data = array();

	$conds['conditions'] = array( 'Deal.id' => $deal_id );
	$deal_data = $this->Deal->find('first', $conds);
	
	if( $deal_data ){
		$data_image = array();

		$images = $deal_data['Deal']['other_images'];
		if( $images ){
			$other_images = json_decode($images);
			foreach( $other_images as $k => $val ){
				if( $val != $this->dealDeafultImage ){
					$data_image[] = $val;
				}
			}
		}else{
			$data_image[] = $deal_data['Deal']['image'];
		}
		
		$img = $_FILES['upload_deal_image'];
		if( $img && $img['name'] != "" ){
				
				$dir = 'img/deals' ;

				if( !is_dir($dir) ){
					mkdir($dir, 0755);
				}

				$fileName = $this->saveFile('upload_deal_image', $dir , array() , $this->dealWidth, $this->dealHeight );

				if( is_array($fileName) && $fileName['status'] ==  false ){
					$data['status']   = false;
					$data['message']  = $fileName['msg'];
					
					echo json_encode($data); exit;

				}

				$data_image[] = $fileName;

				$thumb_dir = 'img/deals/thumbs' ;

				if( !is_dir($thumb_dir) ){
					mkdir($thumb_dir, 0755);
				}

				$split = end(explode("/", $fileName));
				$thumb_img =  $thumb_dir . "/" . $split ;
				copy ( $fileName , $thumb_img ) ; 

				// Resize Images
				$this->Image->prepare( $fileName );
				$this->Image->resize($this->dealWidth, $this->dealHeight );
				$this->Image->save( $fileName );

				// Copy a thumbnail
				$this->Image->prepare( $thumb_img );
				$this->Image->resizeThumbnail($this->dealThumbWidth, $this->dealThumbHeight );
				$this->Image->save( $thumb_img );

				$user = CakeSession::read("Auth.User");
				$save_data['Deal']['id'] = $deal_id;
				$save_data['Deal']['updated_by'] = $user['id'];
				$save_data['Deal']['image'] = $fileName;
				$save_data['Deal']['other_images'] = json_encode($data_image);

				// $data['data'] = $save_data;

				// save data
				$this->Deal->save($save_data);

				$data['status'] = true;
				$data['message'] = "Deal image is uploaded successfully !";
				$data['uploaded_image'] = $fileName;

				echo json_encode($data);
				exit;

		} 
	}else{
		$data['status']   = false;
		$data['message']  = "Something went wrong ! Please try again.";
	}

	echo json_encode( $data);
	$this->autoRender = false;

}


	public function deleteDealImageAjax__ ( $deal_id = 0, $img = NULL ) {

		if( $deal_id == 0 || $img == NULL ){
				$data['status']   = false ;
				$data['message']  = "Something went wrong. Please try again !";
				echo json_encode($data); exit;
		}

		$deal_data = $this->Deal->findById($deal_id);

		if( $deal_data){
			$data_image = array();
			$images = $deal_data['Deal']['other_images'];

			$img_path       = "img/deals/" . $img ;
			$img_thumb_path = "img/deals/thumbs/" . $img ;

			if( $images ){
				$other_images = json_decode($images);
				foreach( $other_images as $k => $val ){
					$data_image[] = $val;
				}
			}else{
				$data_image[] = $deal_data['Deal']['image'];
			}

			$data_image = array_flip($data_image);

			if( in_array( $img_path , $data_image )){
					unset($data_image[$img_path]);
					$data_image = array_flip($data_image);
			}

			if( empty($data_image) ){
				 $data_image[] = $this->dealDeafultImage;
			}

			// Save Data, Remove Image
			$save_data['Deal']['id']    = $deal_id;
			$save_data['Deal']['image'] = reset($data_image);
			$save_data['Deal']['other_images'] = json_encode($data_image);

			if( $this->Deal->save($save_data)){
				
				if( $img_path != $this->dealDeafultImage ){
					unlink($img_path);
					unlink($img_thumb_path);
				}

				$data['status']      = true;
				$data['data_images'] = $data_image;

			}else{
					$data['status'] = false;
					$data['message'] = "Something went wrong ! Please try again.";
			}

			echo json_encode($data); exit;

		}else{          
				$data['status']   = false;
				$data['message']  = "Invalid Request ! Please try again.";
				echo json_encode($data); exit;
		}
	}



	public function expandValidity( $deal_id = 0 ){

		if( $this->request->is('post') ){

			$data = $this->request->data;

			$expand_valid_to = date( 'Y-m-d H:i:s', strtotime($data['expand-valid-to']));
			$expand_note 	 = $data['expand-note'];

			$dealData = array();

			$dealData['DealExpandRequest']['deal_id'] 		= $deal_id;
			$dealData['DealExpandRequest']['expand_to'] 	= $expand_valid_to;
			$dealData['DealExpandRequest']['expand_note'] 	= $expand_note;
			$dealData['DealExpandRequest']['request_date'] 	= date('Y-m-d H:i:s') ;
			$dealData['DealExpandRequest']['request_status']= 0 ;
			$dealData['DealExpandRequest']['status_note'] 	= "" ;

			// Check if there is exist pending request
			$conds['conditions'] = array('DealExpandRequest.deal_id' => $deal_id, "DealExpandRequest.request_status !=" => 1 );
			$info = $this->DealExpandRequest->find('first', $conds);

			if( $info && !empty($info) ){
				$dealData['DealExpandRequest']['id'] = $info['DealExpandRequest']['id'];
				$this->DealExpandRequest->id 		 = $info['DealExpandRequest']['id'];
			}			

			$this->DealExpandRequest->create();

			if( $this->DealExpandRequest->save($dealData) ) {
				$this->Session->setFlash(__( 'Deal Extend Request has been submitted. Your deal will be updated after the approval !')
											.'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
										'default',
										array('class'=>'alert alert-dismissable alert-success '));
			}else{
				$this->Session->setFlash(__( 'Deal Extend Request has not been submitted. Please try again!')
											.'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
										'default',
										array('class'=>'alert alert-dismissable alert-warning '));
			}

		}

		return $this->redirect(array('action' =>'index'));

	}

}