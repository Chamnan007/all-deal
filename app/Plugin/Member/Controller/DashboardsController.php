<?php
App::uses('MemberAppController', 'Member.Controller');
App::uses('Business', 'Member.Model');
App::uses('Province', 'Member.Model');
App::uses('City', 'Member.Model');
App::uses('Sangkat', 'Member.Model');
App::uses('BusinessMenu', 'Member.Model');
App::uses('BusinessMenuItem', 'Member.Model');
/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */

class DashboardsController extends MemberAppController {

/**
 * Components
 *
 * @var array
 */
  public $components = array('Paginator');

  var $context = 'Business';

  var $uses = array( 
            "Member.Business", 
            "Member.BusinessesMedia", 
            "Member.BusinessMenu", 
            "Member.BusinessMenuItem", 
            "Member.Province", 
            "Member.City", 
            "Member.Location", 
            "Member.Sangkat"
          );


  var $dealWidth = 705;
  var $dealHeight = 422;
  var $dealThumbWidth = 223;
  var $dealThumbHeight = 133;
  var $user = NULL;

  var $dealDeafultImage = "img/deals/default.jpg";
/**
 * index method
 *
 * @return void
 */


  public function beforeFilter(){
      parent::beforeFilter();

      $user = CakeSession::read("Auth.MemberUser");
      $biz_id = $user['business_id'];
      $this->user = $user;

      $this->Business->recursive = -1;
      $info = $this->Business->find('first', array('conditions' => array('Business.id' => $biz_id), 'fields' => array('Business.id', 'Business.member_level') )) ;

      $this->set('business_member_level', $info['Business']['member_level']);


  }

  public function index(){

    $user = CakeSession::read("Auth.MemberUser");

    $this->set('user', $user);

    $business_id = $user['business_id'];

    $this->set('status', $this->status);

    $options = array( 'joins' => array(
                              array('table' => 'cities',
                                  'alias' => 'City',
                                  'type' => 'LEFT',
                                  'conditions' => array('Business.city = City.city_code')
                                ),

                              array('table' => 'provinces',
                                  'alias' => 'Province',
                                  'type' => 'LEFT',
                                  'conditions' => array('Business.province = Province.province_code')
                                )

                        ),
                    'conditions' => array(
                                'Business.id' => $business_id
                              ),

                    'fields' => array('Business.*', 
                                      'City.*', 
                                      'Province.*', 
                                      'RegisteredBy.last_name', 
                                      'RegisteredBy.first_name',
                                      'ApprovedBy.last_name', 
                                      'ApprovedBy.first_name',
                                      'MainCategory.*',
                                      'SangkatInfo.*',
                                      'SubCategory.*'),
                  );



    $business = new Business(); 
    $data = $business->find('first', $options);
    $this->set('business', $data );

    // $this->set('access_level', $this->member_biz_level);
    $this->set('access_level', $this->biz_access_level);
    // Count Menu Item
    $count = $this->countTotalMenuItems($business_id);
    $this->set('count_menu_items', $count);
    
  }


  public function countTotalMenuItems( $business_id = 0 ){

      $conds['conditions'] = array(   "BusinessMenuItem.business_id" => $business_id,
                                      "BusinessMenuItem.status" => 1 );

      $conds['group'] = array( "BusinessMenuItem.menu_id" ) ;
      $conds['fields'] = array( 'BusinessMenuItem.menu_id', 'COUNT(BusinessMenuItem.id) as total' );

      return $this->BusinessMenuItem->find('all', $conds);
  }

  public function uploadMedia( $type = NULL ){

    $user = CakeSession::read("Auth.MemberUser");

    $bis_id = $user['business_id'];

    $this->Business->id = $bis_id;
    if (!$this->Business->exists()) {
      $this->Session->setFlash(__( 'Invalid Request.')
                          .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                        'default',
                        array('class'=>'alert alert-dismissable alert-danger '));
      exit;
    }

    $picWidth = 300;
    $picHeight = 300;

    if( $type == 0 ){
      
      $data_array = array();

      if( !empty($_FILES['images']['name']) ){

        foreach( $_FILES['images']['name'] as $key => $value ){
          $data = array();
          $images = array();
          $images['name'] = $value;
          $images['type'] = $_FILES['images']['type'][$key];
          $images['tmp_name'] = $_FILES['images']['tmp_name'][$key];
          $images['error'] = $_FILES['images']['error'][$key];
          $images['size'] = $_FILES['images']['size'][$key];

          $dir = 'img/business/pictures' ;
          if( !is_dir($dir) ){
            mkdir($dir, 0700);
          }

          $fileName = $this->saveMultiple($images, $dir, array(), $picWidth, $picHeight );

          if( is_array($fileName) && $fileName['status'] ==  false ){

            $this->Session->setFlash(__( 'Images could not be uploaded. ' . $fileName['msg'] )
                          .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                        'default',
                        array('class'=>'alert alert-dismissable alert-danger '));

            return $this->redirect(array('action' =>'index/media'));
          }

          $max_width = $max_height = 800 ;

          $img_size    = getimagesize($fileName);
          $img_width    = $img_size[0];
          $img_height   = $img_size[1];

          if( $img_width > $img_height ){
            $max = $img_width - 1;
          }else{
            $max = $img_height - 1;
          }


          if( $img_width > $max_width || $img_height > $max_height ){
            // Resize Images
            $this->Image->prepare( $fileName );
            $this->Image->resize( $max_width, $max_height );
            $this->Image->save( $fileName );
          }else{
            // Resize Images
            $this->Image->prepare( $fileName );
            $this->Image->resize( $max, $max );
            $this->Image->save( $fileName );
          }


          $data['BusinessesMedia']['business_id']   = $bis_id;
          $data['BusinessesMedia']['media_type']    = $type;
          $data['BusinessesMedia']['media_path']    = $fileName;
          $data['BusinessesMedia']['media_embeded']   = NULL;
          $data['BusinessesMedia']['status']      = 1;

          $data_array[] = $data;
        }

        // var_dump($data_array); exit;

        $this->BusinessesMedia->create(); 

        if( $this->BusinessesMedia->saveAll($data_array)){
          $message = json_encode($data_array);

          $this->generateLog($message ,' UPLOAD PICTURES TO BUSINESS  '.$bis_id);

          $this->Session->setFlash(__( 'Images have been uploaded.')
                          .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                        'default',
                        array('class'=>'alert alert-dismissable alert-success '));

        }else{
          $this->Session->setFlash(__( 'Images could not be uploaded.')
                          .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                        'default',
                        array('class'=>'alert alert-dismissable alert-danger '));
        }
      }

      return $this->redirect(array('action' =>'index/media'));

    }else if( $type == 1 ){

      $data['BusinessesMedia']['business_id']   = $bis_id;
      $data['BusinessesMedia']['media_type']    = $type;
      $data['BusinessesMedia']['media_path']    = NULL;
      $data['BusinessesMedia']['media_embeded']   = $this->request->data['video_url'];
      $data['BusinessesMedia']['status']      = 1;

      if( $this->BusinessesMedia->save($data)){
        $message = json_encode($data);

        $this->generateLog($message ,' UPLOAD VIDEO TO BUSINESS  '.$bis_id);

        $this->Session->setFlash(__( 'Video has been uploaded.')
                        .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                      'default',
                      array('class'=>'alert alert-dismissable alert-success '));

      }else{
        $this->Session->setFlash(__( 'Video could not be uploaded.')
                        .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                      'default',
                      array('class'=>'alert alert-dismissable alert-danger '));
      }
      return $this->redirect(array('action' =>'index/media'));
    }

  }
  
  public function removeLogo(){

    $user = CakeSession::read("Auth.MemberUser");

    $bis_id = $user['business_id'];

    if( $user['access_level'] != 1 ){
      return $this->redirect(array('action' =>'index'));
    }

    $old = $this->Business->findById($bis_id);
    $old_logo = $old['Business']['logo'];
    $data['Business']['id'] = $bis_id;
    $data['Business']['logo'] = 'img/business/logos/default.jpg' ;

    if( $this->Business->save($data)){

      if( $old_logo != "img/business/logos/default.jpg" ){
        unlink($old_logo);
      }

      $this->generateLog("" ,' REMOVE LOGO TO BUSINESS  '.$bis_id);

      $this->Session->setFlash(__( 'Logo has been removed.')
                      .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                    'default',
                    array('class'=>'alert alert-dismissable alert-success '));

    }else{
      $this->Session->setFlash(__( 'Logo could not be removed.')
                      .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                    'default',
                    array('class'=>'alert alert-dismissable alert-danger '));
    }

    return $this->redirect(array('action' =>'index'));
  
  }

  public function changeLogo(){

    $user = CakeSession::read("Auth.MemberUser");

    if( $user['access_level'] != 1 ){
      return $this->redirect(array('action' =>'index'));
    }

    $bis_id = $user['business_id'];

    $old = $this->Business->findById($bis_id);
    $old_logo = $old['Business']['logo'];

    $data['Business']['id'] = $bis_id;

    $logo = $_FILES['logo'] ;

    if( $logo['name'] != "" ){

      $dir = 'img/business/logos' ;

      if( !is_dir($dir) ){
        mkdir($dir, 0700);
      }

       $minWidth = 225;

      $fileName = $this->saveFile('logo', $dir, array(), $minWidth, $minWidth  );

      if( is_array($fileName) && $fileName['status'] ==  false ){
        $this->Session->setFlash(__( 'Logo could not be changed. ' . $fileName['msg'] )
                      .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                    'default',
                    array('class'=>'alert alert-dismissable alert-danger '));
        return $this->redirect(array('action' =>'view', $bis_id));
      }

      // Resize Images
      $this->Image->prepare( $fileName );
      $this->Image->resize($minWidth, $minWidth);
      $this->Image->save( $fileName, 90, true );

      $data['Business']['logo'] = $fileName;
    }else{
      $data['Business']['logo'] = 'img/business/logos/default.jpg' ;
    }

    if( $this->Business->save($data)){

      if( $old_logo != "img/business/logos/default.jpg" ){
        unlink($old_logo);
      }

      $this->generateLog("" ,' CHANGE LOGO TO BUSINESS  '.$bis_id);

      $this->Session->setFlash(__( 'Logo has been changed.')
                      .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                    'default',
                    array('class'=>'alert alert-dismissable alert-success '));

    }else{
      $this->Session->setFlash(__( 'Logo could not be changed.')
                      .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                    'default',
                    array('class'=>'alert alert-dismissable alert-danger '));
    }

    return $this->redirect(array('action' =>'index'));

  }



// Business Menu Category
public function saveMenuCategory( $business_id = 0, $id =  0 ){

    $this->Business->id = $business_id;
    if ( !$this->request->is('post') || !$this->Business->exists()) {
        $this->Session->setFlash(__( 'Invalid Request !')
                      .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                    'default',
                    array('class'=>'alert alert-dismissable alert-danger '));
        $this->redirect(array('action' => 'index', 'menu'));
    }

    $data = $this->request->data;

    $user =  CakeSession::read("Auth.MemberUser"); 

    $data['BusinessMenu']['business_id']    = $business_id;
    if( !$id ){
        $data['BusinessMenu']['created_by']     = $user['id'] ;
    }else{
        $data['BusinessMenu']['id'] = $id;
    }
    $data['BusinessMenu']['modified_by']    = $user['id'] ;
    $data['BusinessMenu']['status']         = 1;

    if( $this->BusinessMenu->save($data) ){
        $this->Session->setFlash(__( 'Menu Category has been saved.')
                      .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                    'default',
                    array('class'=>'alert alert-dismissable alert-success '));
    }else{
        $this->Session->setFlash(__( 'Faild to save menu category !')
                      .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                    'default',
                    array('class'=>'alert alert-dismissable alert-danger '));
    }

    if( !$id ){
        $id = $this->BusinessMenu->getLastInsertID();
    }

    $this->redirect(array('action' => 'index', 'menu', $id));

}

// Delete Menu Category
public function deleteMenuCategory( $biz_id = 0, $id = 0 ){

    $this->BusinessMenu->id = $id;
    $this->Business->id = $biz_id;
    if ( !$this->Business->exists() || !$this->BusinessMenu->exists()) {
        $this->Session->setFlash(__( 'Invalid Request !')
                      .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                    'default',
                    array('class'=>'alert alert-dismissable alert-danger '));
        $this->redirect(array('action' => 'index', 'menu'));
    }

    $user =  CakeSession::read("Auth.MemberUser"); 

    $data['BusinessMenu']['id'] = $id;
    $data['BusinessMenu']['status'] = 0;
    $data['BusinessMenu']['modified_by'] = $user['id'];

    if( $this->BusinessMenu->save($data) ){
        $this->Session->setFlash(__( 'Menu Category has been deleted.')
                      .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                    'default',
                    array('class'=>'alert alert-dismissable alert-success '));
    }else{
        $this->Session->setFlash(__( 'Faild to delete menu category !')
                      .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                    'default',
                    array('class'=>'alert alert-dismissable alert-danger '));
    }

    $this->redirect(array('action' => 'index', 'menu', $id));

}

public function getMenuItemByMenuIDAjax__(){

    $menu_id    = $this->request->data['menu_id'];
    $limit      = $this->request->data['limit'];
    $offset     = $this->request->data['offset'];

    if( $menu_id == 0 ){
        $data['status'] = false;
        $data['message'] = 'Loading failed ! Please try again.' ;
        echo json_encode($data); exit;
    }

    $conditions['conditions'] = array(  'BusinessMenuItem.menu_id'  => $menu_id,
                                        'BusinessMenuItem.status'   => 1 
                                    );
    $conditions['order'] = array("BusinessMenuItem.title" => "ASC");
    if( $limit != 0 || $limit != null ){
      $conditions['limit'] = $limit;
      $conditions['offset'] = $offset;
    }

    $result = $this->BusinessMenuItem->find('all', $conditions);

    if( $result ){
        $data['status'] = true;
        $data['result'] = $result;
    }else{
        $data['status'] = false;
        $data['message']= "";
    }

    echo json_encode($data); exit;

}

public function saveMenuItem( $bis_id = 0, $menu_id = 0, $id = 0 ){

    if( $bis_id == 0 || $menu_id == 0 ){
       
        $this->Session->setFlash(__( 'Invalid Request ! Failed to save data.')
                  .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                'default',
                array('class'=>'alert alert-dismissable alert-danger '));

        return $this->redirect(array('action' =>'index', 'menu', $menu_id ));
    }

    $data = $this->request->data;
    
    $dir = 'img/menus' ;
    $thumb_dir = 'img/menus/thumbs' ;

    if( !is_dir($dir) ){
      mkdir($dir, 0755);
    }

    if( !is_dir($thumb_dir) ){
      mkdir($thumb_dir, 0755);
    }

    if( !file_exists($dir . "/default.jpg" )){
        copy ( $this->dealDeafultImage , $dir . "/default.jpg" ) ;     
    }

    if( !file_exists($thumb_dir . "/default.jpg" )){
        copy ( $this->dealDeafultImage , $thumb_dir . "/default.jpg" ) ;     
    }

    $msg = "";

    $img = $_FILES['menuItemImage']['name'];

    if( $img != "" ){
        $fileName = $this->saveFile('menuItemImage', $dir , array() , $this->dealWidth, $this->dealHeight );

        if( is_array($fileName) && $fileName['status'] ==  false ){
            $msg = $fileName['msg'];
            $fileName = $this->dealDeafultImage;
        }else{

            $split = end(explode("/", $fileName));
            $thumb_img =  $thumb_dir . "/" . $split ;

            copy ( $fileName , $thumb_img ) ; 

            // Resize Images
            $this->Image->prepare( $fileName );
            $this->Image->resize($this->dealWidth, $this->dealHeight );
            $this->Image->save( $fileName );

            // Copy a thumbnail
            $this->Image->prepare( $thumb_img );
            $this->Image->resizeThumbnail($this->dealThumbWidth, $this->dealThumbHeight );
            $this->Image->save( $thumb_img );

            // Remove Old Image
            if( $data['data_old_img'] && $data['data_old_img'] != "" && $data['data_old_img'] != $dir . "/default.jpg" ){
                $old_img    = $data['data_old_img'];
                $old_thumb  = str_replace("/menus/", '/menus/thumbs/', $old_img);

                if( file_exists($old_img ))     unlink($old_img);
                if( file_exists($old_thumb))    unlink($old_thumb);
            }
        }

        $data['BusinessMenuItem']['image'] = $fileName; 
    }

    $data['BusinessMenuItem']['business_id'] = $bis_id;
    $data['BusinessMenuItem']['menu_id'] = $menu_id;

    if( !$id ){
        $data['BusinessMenuItem']['created_by'] = $this->user['id']  ;
    }else{
        $data['BusinessMenuItem']['id'] = $id;
    }

    $data['BusinessMenuItem']['modified_by'] = $this->user['id']  ;
    $data['BusinessMenuItem']['status'] = 1  ;

    unset($data['menuItemImage']);

    if( $this->BusinessMenuItem->save($data) ){
        $this->Session->setFlash(__( 'Item has been save successfully. ' . $msg )
                  .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                'default',
                array('class'=>'alert alert-dismissable alert-success '));
    }else{
        $this->Session->setFlash(__( 'Failed to save . Please try again. ' . $msg )
                  .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                'default',
                array('class'=>'alert alert-dismissable alert-dagner '));
    }

    return $this->redirect(array('action' =>'index', 'menu', $menu_id ));

}

public function getMenuItemDetailAjax__(){

    $item_id = $this->request->data['item_id'] ;

    if( $item_id == 0 ){
        $data['status']     = false;
        $data['message']    = "Failed to load data ! Please try again." ;

        echo json_decode($data); exit;
    }

    $detail = $this->BusinessMenuItem->findById($item_id);

    if( $detail && !empty($detail) ){
        $data['status'] = true;
        $data['detail'] = $detail['BusinessMenuItem'];
    }else{
        $data['status'] = false;
        $data['message'] = "Failed to load data ! Please try again." ;
    }

    echo json_encode($data); exit;

}


public function deleteMenuItemAjax__(){

    $item_id = $this->request->data['item_id'];

    if( $item_id == 0 ){
        $data['status'] = false;
        $data['message'] = "Invalid Request ! Please try again.";
        echo json_encode($data); exit;
    }

    $conds['conditions'] = array( 'BusinessMenuItem.id' => $item_id );

    $ItemData['BusinessMenuItem']['id'] = $item_id;
    $ItemData['BusinessMenuItem']['status'] = 0;

    $default_img = 'img/menus/default.jpg' ;
    $default_thm = 'img/menus/thumbs/default.jpg' ;

    if( $this->BusinessMenuItem->save($ItemData) ){

        $detail = $this->BusinessMenuItem->findById($item_id);
        if( $detail ){
            $image = $detail['BusinessMenuItem']['image'];
            $thumb = str_replace('/menus/' , '/menus/thumbs/' , $image);

            if( file_exists($image) && $image != $default_img ){ unlink($image); }
            if( file_exists($thumb) && $thumb != $default_thm ){ unlink($thumb); }
        }

        $data['status'] = true;
        $data['message'] = "Item has been deleted.";
    }else{
        $data['status'] = false;
        $data['message'] = "Failed to delete item. Please try again.";
    }

    echo json_encode($data); exit;

}



  public function get_location_by_city_code( $city_code = null ){

    if( $city_code != null ){

      $location = new Location();
      $locations =  $location->find( 'all', array( 'conditions' => array('Location.city_code' => $city_code ))) ;

      $data = array();
      $data['data'] =  $locations;
      echo json_encode( $data);

      $this->autoRender = false;

    }
  }

  
  public function getSangkatByLocation( $location_id = 0 ){

    if( $location_id != null ){
      $sangkats =  $this->Sangkat->find( 'all', array( 'conditions' => array('Sangkat.location_id' => $location_id ))) ;

      $data = array();
      $data['data'] =  $sangkats;
      echo json_encode( $data);

      $this->autoRender = false;

    }
  }

}