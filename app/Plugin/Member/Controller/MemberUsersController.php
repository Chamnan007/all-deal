<?php
App::uses('MemberAppController', 'Member.Controller');
App::uses('Province', 'Member.Model');
App::uses('City', 'Member.Model');
App::uses('Location', 'Member.Model');
App::uses('MailConfigure', 'Member.Model');
App::uses('Business', 'Member.Model');
App::uses('BusinessCategory','Member.Model');

/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */

class MemberUsersController extends MemberAppController {

/**
 * Components
 *
 * @var array
 */
  var $context = 'MemberUser';

  var $uses = array('Member.MemberUser', 'Member.Province', 'Member.City', 'Member.MailConfigure', 'Member.Business', 'Member.Location', 'Member.BusinessCategory');

  var $components = array('Email');

/**
 * index method
 *
 * @return void
 */
  public function index() {
    
    $user = CakeSession::read("Auth.MemberUser");

    if( $user && $user['access_level'] != 1 ){
      return $this->redirect(array( 'controller' => 'dashboards' ,'action' => 'index' ));
    }

    $bis_id = $user['business_id'];

    $info  = $this->Business->find( "first", array("conditions" => array( "Business.id" => $bis_id ))) ;
    $this->set('business_member_level' , $info['Business']['member_level'] );


    $this->set('status', $this->status);
    $this->set('access_level' , $this->member_biz_level);


    $this->MemberUser->recursive = 0;

    $this->paginate['conditions'] = array("MemberUser.id != " . $user['id'], 'MemberUser.business_id' => $user['business_id'] ) ;
    $this->paginate['order'] = array("MemberUser.last_name" => 'ASC', "MemberUser.access_level" => 'ASC' ) ;

    parent::index() ;

  }

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
  public function view($id = null) {
    
    $user = CakeSession::read("Auth.MemberUser");

    if( $user && $user['access_level'] != 1 ){
      return $this->redirect(array( 'controller' => 'dashboards' ,'action' => 'index' ));
    }

    $bis_id = $user['business_id'];

    $info  = $this->Business->find( "first", array("conditions" => array( "Business.id" => $bis_id ))) ;
    $this->set('business_member_level' , $info['Business']['member_level'] );

    if (!$this->MemberUser->find("first", array('conditions' => array('MemberUser.id' => $id, 'MemberUser.business_id' => $bis_id) ) )) {
      $this->Session->setFlash(__( "Invalid Request !" )
                      .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                    'default',
                    array('class'=>'alert alert-dismissable alert-danger '));

      return $this->redirect(array('action' =>'index'));
    }

    $city = new City();
    $this->set('cities', $city->find('all', array('conditions' => array('City.status' => 1) )));

    $province = new Province();
    $this->set('provinces', $province->find('all', array('conditions' => array('Province.status' => 1) )));


    $this->set('access_level', $this->member_biz_level);
    $this->set('status', $this->status);

    $options = array('conditions' => array('MemberUser.' . $this->MemberUser->primaryKey => $id));
    $this->set('user', $this->MemberUser->find('first', $options));

  }

  public function profile() {

    $user = CakeSession::read("Auth.MemberUser"); 

    $bis_id = $user['business_id'];

    $info  = $this->Business->find( "first", array("conditions" => array( "Business.id" => $bis_id ))) ;
    $this->set('business_member_level' , $info['Business']['member_level'] );

    $city = new City();
    $this->set('cities', $city->find('all', array('conditions' => array('City.status' => 1) )));

    $province = new Province();
    $this->set('provinces', $province->find('all', array('conditions' => array('Province.status' => 1) )));


    $this->set('access_level', $this->member_biz_level);
    $this->set('status', $this->status);

    $options = array('conditions' => array('MemberUser.' . $this->MemberUser->primaryKey => $user['id']));
    $this->set('user', $this->MemberUser->find('first', $options));

  }

/**
 * add method
 *
 * @return void
 */


  public function add() {
    
    $user = CakeSession::read("Auth.MemberUser");

    if( $user && $user['access_level'] != 1 ){
      return $this->redirect(array( 'controller' => 'dashboards' ,'action' => 'index' ));
    } 

    $bis_id = $user['business_id'];

    $info  = $this->Business->find( "first", array("conditions" => array( "Business.id" => $bis_id ))) ;
    $this->set('business_member_level' , $info['Business']['member_level'] );

    $this->set('gender', $this->gender);

    $city = new City();
    $this->set('cities', $city->find('all', array('conditions' => array('City.status' => 1) ) ));

    $province = new Province();
    $this->set('provinces', $province->find('all', array('conditions' => array('Province.status' => 1) ) ));

    
    $this->set('access_level', $this->member_biz_level);
    $this->set('status', $this->status);


    if ($this->request->is('post')) {


      unset( $this->request->data['MemberUser']['confirm_password'] );

      $password = $this->request->data['MemberUser']['password'] ;

      if($this->request->data['MemberUser']['password'] == "" ){
        $this->request->data['MemberUser']['confirm_password'] = NULL;
      }

      $Y = $this->request->data['MemberUser']['dob'][2];
      $m = $this->request->data['MemberUser']['dob'][1];
      $d = $this->request->data['MemberUser']['dob'][0];

      $this->request->data['MemberUser']['dob'] = $Y . '-' . $m . '-' . $d;

      $this->request->data['MemberUser']['user_id'] = uniqid() ;
      $this->request->data['MemberUser']['business_id'] = $user['business_id'] ;

      $this->request->data['MemberUser']['registered_date'] = DboSource::expression('NOW()');

      $this->MemberUser->create();

      parent::save();
    }
  }

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */

  public function beforeFilter() {

    parent::beforeFilter();

    $this->MemberAuth->allow('forgot', 'activate' );
  }


  public function forgot(){

    if ($this->request->is('post')) { 
      $mail = $this->request->data['User']['email'];

      $conditions = array('conditions' => array('MemberUser.email' => $mail, 'MemberUser.status = 1') );

      $info = $this->MemberUser->find( 'first', $conditions );

      if( !empty($info) ){

        $token = $this->randomPassword() . $mail ;

        for( $i =1; $i <= 10; $i++ ){
          $token = sha1($token);
        }

        $activate_link = Router::url("/member/MemberUsers/activate/"  . $token , true);

        $data['MemberUser']['id'] = $info['MemberUser']['id'];
        $data['MemberUser']['token'] = $token;

        $subject = "Password Reset Verification Message";

        $content = "<h4>Dear " . $info['MemberUser']['last_name'] . " " . $info['MemberUser']['first_name'] . ",</h4><p>Please click on verification link below to complete your password reset.</p>";
        $content .= "<p><a href='" . $activate_link . "'>" . $activate_link . "</a></p><br/>";
        // $content .= "<p>Thanks,</p><p>Admin</p>";

        if( $this->sendEmail($mail, $subject, $content) ){

          if( $this->MemberUser->save($data)){
              $this->Session->setFlash(__( "Verification email has been sent to your email: " . $mail )
                      .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                    'default',
                    array('class'=>'alert alert-dismissable alert-success '));
            return $this->redirect('/merchants');
          }
        }else{
          $this->Session->setFlash(__( "Something went wrong. Please try again." )
                      .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                    'default',
                    array('class'=>'alert alert-dismissable alert-danger '));
          return $this->redirect(array('action' =>'forgot'));
        }
  
      }else{
        $this->Session->setFlash(__( "Reset Password Failed. Make sure you account is activated." )
                      .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                    'default',
                    array('class'=>'alert alert-dismissable alert-danger '));

        return $this->redirect(array('action' =>'forgot'));

      }

    }

    $this->layout = 'login';
  }


  public function activate( $token = NULL ){

    if( !$token ){
      $this->Session->setFlash(__( "Verification Failed. Try again!" )
                      .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                    'default',
                    array('class'=>'alert alert-dismissable alert-danger '));

      return $this->redirect('/merchants');

    }

    $info = $this->MemberUser->findByToken($token);

    if( !empty($info) ){

      $mail = $info['MemberUser']['email'];

      $new_pass = $this->randomPassword();

      $user_id = $info['MemberUser']['id'];
      $this->MemberUser->id = $user_id;
      $data['MemberUser']['id'] = $user_id ;
      $data['MemberUser']['password'] = $new_pass;
      $data['MemberUser']['token'] = "";

      $Login = Router::url("/merchants" , true);

      $subject = "Reset Password";

      $content = "<h4>Dear " . $info['MemberUser']['last_name'] . " " . $info['MemberUser']['first_name'] . ", </h4><p>Your password is successfully reset.</p><p>Your new password: $new_pass</p>";
      $content .= "<p><a href='" . $Login . "'>Goto Login</a></p><br/>";
      // $content .= "<p>Thanks,</p><p>Admin</p>";

      if( $this->sendEmail($mail, $subject, $content) ){

        if( $this->MemberUser->save($data)){
            $this->Session->setFlash(__( "Your new password has been sent to your email : " . $mail )
                    .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                  'default',
                  array('class'=>'alert alert-dismissable alert-success '));
          return $this->redirect('/merchants');
        }
      }else{
        $this->Session->setFlash(__( "Something went wrong. Please try again." )
                    .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                  'default',
                  array('class'=>'alert alert-dismissable alert-danger '));
        return $this->redirect(array('action' =>'forgot'));
      }

    }else{
      $this->Session->setFlash(__( "Verification Failed. Try again!" )
                      .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                    'default',
                    array('class'=>'alert alert-dismissable alert-danger '));

      return $this->redirect('/merchants');
    }


  }

public function sendEmail( $recipients = NULL, $subject = NULL , $content = NULL ) {

    return parent::sendMail( $recipients, $subject, $content);

}

  public function randomPassword() {

      $alphabet = "0123456789abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
      $pass = array(); 
      $alphaLength = strlen($alphabet) - 1; 

      for ($i = 0; $i < 10; $i++) {
          $n = rand(0, $alphaLength);
          $pass[] = $alphabet[$n];
      }

      return implode($pass);
  }
  
  public function edit($id = null) {
    
    
    $user = CakeSession::read("Auth.MemberUser");

    if( $user && $user['access_level'] != 1 ){
      return $this->redirect(array( 'controller' => 'dashboards' ,'action' => 'index' ));
    }

    $bis_id = $user['business_id'];

    $info  = $this->Business->find( "first", array("conditions" => array( "Business.id" => $bis_id ))) ;
    $this->set('business_member_level' , $info['Business']['member_level'] );

    if (!$this->MemberUser->find("first", array('conditions' => array('MemberUser.id' => $id, 'MemberUser.business_id' => $bis_id) ) )) {
      $this->Session->setFlash(__( "Invalid Request !" )
                      .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                    'default',
                    array('class'=>'alert alert-dismissable alert-danger '));

      return $this->redirect(array('action' =>'index'));
    }

    $this->set('gender', $this->gender);

    $province = new Province();
    $this->set('provinces', $province->find('all', array('conditions' => array('Province.status' => 1) )));

    $this->set('access_level', $this->member_biz_level);

    $this->set('status', $this->status);

    if ($this->request->is(array('post', 'put'))) {

      $this->request->data['MemberUser']['dob'] = $this->request->data['MemberUser']['dob']['year'] . "-" .
                                                  $this->request->data['MemberUser']['dob']['month'] . "-" .
                                                  $this->request->data['MemberUser']['dob']['day'] ;

      $this->request->data['MemberUser']['inactive_date'] = DboSource::expression('NOW()');

      parent::save($id);

    } else {

      $options = array('conditions' => array('MemberUser.' . $this->MemberUser->primaryKey => $id));
      $this->request->data = $this->MemberUser->find('first', $options);  

      // var_dump($this->request->data);
      $city = new City();
      $this->set('cities', $city->find('all', array('conditions' => array("City.province_code" => $this->request->data['MemberUser']['province_code'], 'City.status' => 1 ) ) ));
      
    }
  }

  public function editProfile() {

    $user = CakeSession::read("Auth.MemberUser");
    $this->set("user_ac_lv", $user['access_level']);
    $bis_id = $user['business_id'];

    $info  = $this->Business->find( "first", array("conditions" => array( "Business.id" => $bis_id ))) ;
    $this->set('business_member_level' , $info['Business']['member_level'] );

    $this->set('gender', $this->gender);

    $province = new Province();
    $this->set('provinces', $province->find('all', array('conditions' => array('Province.status' => 1) )));

    $this->set('access_level', $this->member_biz_level);
    // $this->set('business_member_level' , $this->business_level);

    $this->set('status', $this->status);

    if ($this->request->is(array('post', 'put'))) {

      $this->request->data['MemberUser']['inactive_date'] = DboSource::expression('NOW()');
      $this->request->data['MemberUser']['id'] = $user['id'];

      
      $this->request->data['MemberUser']['dob'] = $this->request->data['MemberUser']['dob']['year'] . "-" .
                                                  $this->request->data['MemberUser']['dob']['month'] . "-" .
                                                  $this->request->data['MemberUser']['dob']['day'] ;

      if($this->MemberUser->save($this->request->data)){

        $options = array('conditions' => array('MemberUser.' . $this->MemberUser->primaryKey => $user['id']));
        $new_info = $this->MemberUser->find('first', $options); 

        $this->Session->write("Auth.MemberUser", $new_info['MemberUser']);

        $this->Session->setFlash(__( "Profile is updated successfully !" )
                      .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                    'default',
                    array('class'=>'alert alert-dismissable alert-success '));

        return $this->redirect(array('action' =>'profile'));

      }else{
        $this->Session->setFlash(__( "Updating Failed !" )
                      .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                    'default',
                    array('class'=>'alert alert-dismissable alert-danger '));

      }

    } else {

      $options = array('conditions' => array('MemberUser.' . $this->MemberUser->primaryKey => $user['id']));
      $this->request->data = $this->MemberUser->find('first', $options);  

      // var_dump($this->request->data);
      $city = new City();
      $this->set('cities', $city->find('all', array('conditions' => array("City.province_code" => $this->request->data['MemberUser']['province_code'], 'City.status' => 1 ) ) ));
      
    }
  }


  public function change_password( $id = null){
    
    $user = CakeSession::read("Auth.MemberUser");

    if( $user && $user['access_level'] != 1 ){
      return $this->redirect(array( 'controller' => 'dashboards' ,'action' => 'index' ));
    }

    if (!$this->MemberUser->exists($id)) {
      $this->Session->setFlash(__( "Invalid Request !" )
                      .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                    'default',
                    array('class'=>'alert alert-dismissable alert-danger '));

      return $this->redirect(array('action' =>'index'));
    }

    unset($this->request->data['MemberUser']['confirm_password']);

    if ($this->MemberUser->save($this->request->data)) {
      $this->Session->setFlash(__('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button><b>Success!!!</b> Password is successfully changed.</div>'));

      $obj  = $this->MemberUser->findById($id);
      $logMessage = json_encode($obj);
      parent::generateLog($logMessage,' RESET PASSWORD :'.$id);
      
      return $this->redirect(array( 'controller' => 'MemberUsers' , 'action' => 'view/' .  $id, 'plugin' => 'member' ));
    } else {
      $this->Session->setFlash(__('<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button><b>Failed!!!</b> Password cound not be changed. Please, try again.</div>'));
    }

  }

  public function p_change_password(){

    $user = CakeSession::read("Auth.MemberUser");
    $id = $user['id'];

    unset($this->request->data['MemberUser']['confirm_password']);

    if ($this->MemberUser->save($this->request->data)) {
      $this->Session->setFlash(__('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button><b>Success!!!</b> Password is successfully changed.</div>'));

      $obj  = $this->MemberUser->findById($id);
      $logMessage = json_encode($obj);
      parent::generateLog($logMessage,' RESET PASSWORD');
      
      return $this->redirect(array( 'controller' => 'MemberUsers' , 'action' => 'profile', 'plugin' => 'member' ));
    } else {
      $this->Session->setFlash(__('<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button><b>Failed!!!</b> Password cound not be changed. Please, try again.</div>'));
    }

  }



/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
  public function delete($id = null) {
    
    $user = CakeSession::read("Auth.MemberUser");

    if( $user && $user['access_level'] != 1 ){
      return $this->redirect(array( 'controller' => 'dashboards' ,'action' => 'index' ));
    }
    
    $this->MemberUser->id = $id;

    if (!$this->MemberUser->exists()) {
      $this->Session->setFlash(__( "Invalid Request !" )
                      .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                    'default',
                    array('class'=>'alert alert-dismissable alert-danger '));

      return $this->redirect(array('action' =>'index'));
    }
    $this->request->onlyAllow('post', 'delete');
    if ($this->MemberUser->delete()) {
      $this->Session->setFlash(__('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button><b>Success!!!</b> The user has been deleted.</div>'));
    } else {
      $this->Session->setFlash(__('<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button><b>Failed!!!</b> The user could not be deleted. Please, try again.</div>'));
    }
    return $this->redirect(array('action' => 'index'));
  }


  public function logout() {  
    
    return $this->redirect($this->MemberAuth->logout());
  }

  public function login(){

    if ($this->request->is('post')) { 

      $data = $this->request->data;

      // var_dump($data); exit;
      
      $obj  = $this->MemberAuth->login();   
      
      if ($obj) {

        $user = CakeSession::read("Auth.MemberUser");

        $biz_id = $user['business_id'];

        $this->Business->recursive = -1;

        $bis_info = $this->Business->find('first', array('conditions' => array('Business.id' => $biz_id), 'fields' => 'Business.status')) ;

        if($bis_info['Business']['status'] != 1 ){
        		$this->Session->setFlash(__('<span style="color:red;">Invalid username or password. Try again!</span>'));
        		$this->logout();
        }

          return $this->redirect(array('controller' =>'deals'  ,
                      'action'=>'index',
                      'plugin'=>'member',
                       ));

      }

      $this->Session->setFlash(__('<span style="color:red;">Invalid username or password. Try again!</span>'));
      
      if( isset($data['from-website']) ){

          $redirect_url = Router::url('/merchants', true);

          return $this->redirect($redirect_url);
      }

    }
        
    $this->layout = 'login';
  }



  public function checkDuplicateEmail( $email = NULL, $userid = NULL ){
    
    if( $userid != NULL ){
      $conditions = array('conditions'=> array('MemberUser.email' => $email, 'MemberUser.id !=' => $userid )) ;
    }else{
      $conditions = array('conditions'=> array('MemberUser.email' => $email )) ;
    }


    if( $this->MemberUser->find('first', $conditions ) ){
      $data['status'] = 0;
      $data['msg'] = "This email is already registered. Please use another email." ;
    }else{
      $data['status'] = 1;
    }

    echo json_encode($data); exit;

  }

  public function get_city_by_province( $pro_code = null ){

      if( $pro_code != null ){

        $city = new City();
        $cities =  $city->find('all',array( 'conditions' => array('province_code' => $pro_code, 'status' => 1 ))) ;

        $data = array();
        $data['data'] =  $cities;
        echo json_encode( $data);

        $this->autoRender = false;

      }
  }

  public function get_location_by_city_code( $city_code = null ){

    if( $city_code != null ){

      $location = new Location();
      $locations =  $location->find( 'all', array( 'conditions' => array('Location.city_code' => $city_code ))) ;

      $data = array();
      $data['data'] =  $locations;
      echo json_encode( $data);

      $this->autoRender = false;

    }
  }

public function getCategoryByParentID( $parent_id = 0 ){

    $options = array('conditions' => array('parent_id'=>$parent_id, 'status' => 1 ), 'order' => array('BusinessCategory.category' => 'ASC' ) );

    $result  = $this->BusinessCategory->find('all', $options);

    $data = array();

    $data['data'] =  $result;
    echo json_encode( $data);

    $this->autoRender = false;
  }

}