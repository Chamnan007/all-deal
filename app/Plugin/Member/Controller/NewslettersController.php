<?php
App::uses('MemberAppController', 'Member.Controller');

App::uses('Business', 'Member.Model');
App::uses('MemberUser', 'Member.Model');
App::uses('MailConfigure', 'Member.Model');
App::uses('NewletterSubscriber', 'Member.Model');

/**
 * Cities Controller
 *
 * @property City $City
 * @property PaginatorComponent $Paginator
*/

class NewslettersController extends MemberAppController {

/**
 * Components
 *
 * @var array
 */
  var $context = 'Newsletter';

  var $uses = array(  'Member.Newsletter',
            'Member.Business',
            'Member.MemberUser',
            'Member.MailConfigure',
            'Member.NewletterSubscriber' );

  var $components = array('Email');

/**
 * index method
 *
 * @return void
 */
  
  public function index() {

    $user_data = CakeSession::read("Auth.MemberUser");
    
    $this->paginate['order'] = array("created" => 'DESC' ) ;
    $this->paginate['conditions'] = array( "Newsletter.business_id" => $user_data['business_id']);
    parent::index();
  }


  public function compose(){

    $user_data = CakeSession::read("Auth.MemberUser");

    $this->set('user_data', $user_data);

    $config = new MailConfigure();
    $configuration = $config->find('first', array( 'conditions' => array('MailConfigure.business_id' => $user_data['business_id']) )) ;
    $this->set('configure', $configuration );

    $biz_acc_level = array();

    foreach($this->biz_access_level as $key => $value ){
      $biz_acc_level['OR']['access_level'][] = $key ;
    }

    $emails = array();
      $bis_email = array();

    $busniess = new Business();
    $this->Business->recursive = - 1;
    $conditions = array('fields' => array('DISTINCT email') );
    $business_email = $busniess->find('all', $conditions) ;

    foreach( $business_email as $key => $mail ){
      $emails[] = $mail['Business']['email'];
          $bis_email[] = $mail['Business']['email'];
    }


    $user = new MemberUser();
    $this->MemberUser->recursive = -1;
    $conditions = array('fields' => array('DISTINCT email', 'access_level'), 'conditions' => $biz_acc_level );

    $user_email = $user->find('all', $conditions);

    foreach( $user_email as $key => $mail ){
      $emails[] = $mail['MemberUser']['email'];
          $bis_email[] = $mail['MemberUser']['email'];
    }

    array_unique($emails);
      sort($emails);
      array_unique($bis_email);
      sort($bis_email);

    $this->set('biz_email', $bis_email);

      $subscribe_mails = $this->NewletterSubscriber->find('all', array('conditions' => array('status' => 1), 
                                                                        'fields' => array('email', 'status')) );
      $sub_emails = array();

      foreach($subscribe_mails as $k => $m){
         $emails[] = $m['NewletterSubscriber']['email'];
         $sub_emails[] = $m['NewletterSubscriber']['email'];
      }

      array_unique($emails);
      sort($emails);
      array_unique($sub_emails);
      sort($sub_emails);

      $this->set('email', $emails);
      $this->set('sub_email', $sub_emails);

      $to = $emails ;

      // Testing
      // $to = array('rattana.phea@abi-technologies.com');
      $recipients = array();
      $bis = array();
      $sub = array();

    // Testing
    // $to = array('rattana.phea@abi-technologies.com');

    if ($this->request->is('post')) { 

      $data_email = json_decode($this->request->data['Newsletter']['recipient']);

      if( in_array('all_biz', $data_email) ){
            $bis = $bis_email;
        }

        if( in_array('all_sub', $data_email) ){
            $sub = $sub_emails;
        }

        if( !in_array('all_biz', $data_email) && !in_array('all_sub', $data_email)){
              $recipients = $data_email;
        }else{
              $recipients = array_merge($bis, $sub);
        }

        $this->request->data['Newsletter']['recipient'] = json_encode($recipients) ;

          $to = $recipients;

      $other = $this->request->data['Newsletter']['other_recipient'] ;

      $other = split(";", $other);
      $other_re = array();

      if( !empty($other) ){
        foreach($other as $k => $v ){
          $to[] = $v;
          $other_re[] = $v;
        }
      }

      array_unique($to);
          sort($to);

      $this->request->data['Newsletter']['other_recipient'] = json_encode($other_re);

      $this->request->data['Newsletter']['status'] = 1 ;
      $this->request->data['Newsletter']['sent_by'] = $user_data['id'] ;
      $this->request->data['Newsletter']['business_id'] = $user_data['business_id'] ;

      $subject = $this->request->data['Newsletter']['subject'];
      $message = $this->request->data['Newsletter']['message'];


      if(isset($this->request->data['signature'])){
        $message .= "<div style='clear: both; padding-top:20px;'></div>" . $configuration['MailConfigure']['signature'];
      }
      
      $this->request->data['Newsletter']['message'] = $message ;

      // unset($this->request->data['attachement']);

      if( !empty($to) ){
        
            $this->Newsletter->create();
            $saving = $this->Newsletter->save($this->request->data);

            if( $saving ){

              $insert_id = $this->Newsletter->getInsertId();

              foreach( $to as $key => $email ){
                 if( !$this->sendMail( trim($email) , $subject, $message )){
                    $this->Session->setFlash('Sending Failed. Please Email configuration and try again.','default',array('class'=>'alert alert-danger'));

                      $this->Newsletter->delete($insert_id);

                      return $this->redirect(array('controller'=>'newsletters','action'=>'compose')); 
                 }
              }
              
              $this->Session->setFlash('Newsetter has been sent.','default',array('class'=>'alert alert-success'));
                  return $this->redirect(array('controller'=>'newsletters','action'=>'index'));

            }else{
              $this->Session->setFlash('Sending Failed. Please Email configuration.','default',array('class'=>'alert alert-danger'));
                  return $this->redirect(array('controller'=>'newsletters','action'=>'compose'));
            }

        }else{
        $this->Session->setFlash('Something went wrong. Please check email configuration.','default',array('class'=>'alert alert-danger'));
            return $this->redirect(array('controller'=>'newsletters','action'=>'compose'));
      }
    }

  }

  public function sendMail( $recipients = NULL, $subject = NULL , $content = NULL ) {

    $user_data = CakeSession::read("Auth.MemberUser");

    $config = new MailConfigure();
    $configuration = $config->find('first', array( 'conditions' => array('MailConfigure.business_id' => $user_data['business_id']) )) ;

    $port = $configuration['MailConfigure']['port'];
    $host = $configuration['MailConfigure']['host'];
    $from = $configuration['MailConfigure']['user_name'];
    $password = $configuration['MailConfigure']['password'];

    $this->Email->from = $from;
    $this->Email->to = $recipients;
    $this->Email->subject = $subject;
    $this->Email->sendAs = 'html';
    $this->Email->smtpOptions = array(
                    'port'    => $port ,
                    'timeout' => '60',
                    'host'    => $host,
                    'username'  => $from,
                    'password'  => $password,
                    'log'     => true,
                      'ssl'   => true,
                  );

    $this->Email->delivery = 'smtp';
    $this->Email->debug= false;
    $this->Email->sendAs = 'html';  

    try {
          if ( $this->Email->send($content) ) {
              return true;
          } else {
              return false;
          }
      } catch ( Exception $e ) {
          return false;
      }

  }


  public function config(){

    $user_data = CakeSession::read("Auth.MemberUser");
    if( $user_data['access_level'] != 1 ){
      $this->redirect(array('action' => 'index'));
    }

    $config = new MailConfigure();
    $configuration = $config->find('first', array( 'conditions' => array('MailConfigure.business_id' => $user_data['business_id']) )) ;

    $this->set('configuration', $configuration );


    if ($this->request->is('post')) {

      if(!empty($configuration)){
        $this->MailConfigure->id = $configuration['MailConfigure']['id'];
        $this->request->data['MailConfigure']['id'] = $configuration['MailConfigure']['id'];
      }else{
        $this->MailConfigure->create();
      }

      $this->request->data['MailConfigure']['user_id'] = $user_data['id'];
      $this->request->data['MailConfigure']['business_id'] = $user_data['business_id'] ;


      if( $this->MailConfigure->save($this->request->data) ){
        $this->Session->setFlash(__('Configuration has been set successfully.')
                    .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                  'default',
                  array('class'=>'alert alert-dismissable alert-success '));
        $config = new MailConfigure();
        $configuration = $config->find('first');
        $this->set('configuration', $configuration );

      }else{
        $this->Session->setFlash(__('Failed to save configuration.')
                    .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                  'default',
                  array('class'=>'alert alert-dismissable alert-danger '));
      }

      $this->redirect(array('action' => 'config'));

    }

  }


  public function upload_images(){

    if ($this->request->is('post')) {

      $dir = 'img/newsletter' ;

      if( !is_dir($dir) ){
        mkdir($dir, 0700);
      }

      $fileName = $this->saveFile('ImageFile', $dir );
      $error = "";

      if( is_array($fileName) && $fileName['status'] ==  false ){
        $error = $fileName['msg'];
        $fileName = "img/business/logos/default.jpg";

        echo "Error uploading. Please try again.";exit;
      }
      
      echo $fileName;
      exit;
    }
    
  }


  public function removeImage( $img = NULL ){

    $data = array();

    $data['status'] = $img;

    if( !$img ){
      $data['status'] = false;
      echo json_encode($data); exit;
    }

    $url = "img/newsletter/" . $img ;

    if( file_exists( $url ) ){
      unlink($url);
      $data['status'] = true;
    }else{
      $data['status'] = false;
    }

    echo json_encode($data); exit;

  }


  public function view( $id = NULL ){

    if( !$this->Newsletter->exists($id) ){
      $this->Session->setFlash(__('Invalid Request.')
                    .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                  'default',
                  array('class'=>'alert alert-dismissable alert-danger '));
      $this->redirect(array("action" => 'index'));
    }

    $conditions = array('conditions' => array('Newsletter.id' => $id, 'Newsletter.status' => 1 ));

    $this->set('data', $this->Newsletter->find('first', $conditions));
  }

}
