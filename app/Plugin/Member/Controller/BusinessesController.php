<?php
App::uses('MemberAppController', 'Member.Controller');

App::uses('Province', 'Member.Model');
App::uses('City', 'Member.Model');
App::uses('Location', 'Member.Model');
App::uses('BusinessCategory', 'Member.Model');
App::uses('OperationHour', 'Member.Model');
App::uses('BusinessBranch', 'Member.Model');


/**
 * Businesses Controller
 *
 * @property Business $Business
 * @property PaginatorComponent $Paginator
 */

class BusinessesController extends MemberAppController {

/**
 * Components
 *
 * @var array
 */
  var $context = 'Business';
  var $uses = array(  
            "Member.BusinessCategory",
            "Member.BusinessesMedia",
            "Member.BusinessBranch",
            "Member.Business",
            "Member.Province",
            "Member.City",
            "Member.Location",
            "Member.Sangkat",
            "Member.OperationHour"
           );


  public function edit() {

    $user = CakeSession::read("Auth.MemberUser");

    if( in_array($this->business_level, array(1,3)) && $user['access_level'] != 1 ){
      return $this->redirect(array('action' => 'index', 'controller' => 'dashboards', 'plugin' => 'member' ));
    }

    $id = $user['business_id'];

    if (!$this->Business->exists($id)) {
      // throw new NotFoundException(__('Invalid business'));
      $this->Session->setFlash(__( "Invalid Request !" )
                      .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                    'default',
                    array('class'=>'alert alert-dismissable alert-danger '));

      return $this->redirect(array('controller' => 'dashboards', 'action' =>'index', 'plugin' => 'member' ));
    }

    $this->set('access_level', $user['access_level']);

    $bizCategory = new BusinessCategory();
    $this->set('businessCategories', $bizCategory->find('all', array('conditions'=> array('parent_id'=>0, 'status' => 1) )));
    
    $this->set('status', $this->status);
    $this->Business->id = $id;

    if ($this->request->is(array('post', 'put'))) {

      if( $this->request->data['Business']['status'] == 1 && !isset($this->request->data['done-activated']) ){
        $this->request->data['Business']['approved_date'] = DboSource::expression('NOW()');
        $this->request->data['Business']['approved_by']   = $user['id'];
      }

      if( $this->request->data['Business']['status'] == -1 ){
        $this->request->data['Business']['inactive_date'] = DboSource::expression('NOW()');
      }

      if( $this->request->data['Business']['status'] == 2 ){
        $this->request->data['Business']['suspend_date'] = DboSource::expression('NOW()');
      }

      $this->request->data['Business']['accept_payment'] = json_encode(isset($this->request->data['Business']['accept_payment'])?$this->request->data['Business']['accept_payment']:"");
      $this->request->data['Business']['updated_by']  = $user['id'];

      if ($this->Business->save($this->request->data)) {

        // Remove Old Branches
        $delete_conds = array( 'BusinessBranch.business_id' => $id );

        if( $this->BusinessBranch->deleteAll( $delete_conds) ){

            // Add New Branches (Location)
            $bis_branches_data = array();
            if( isset($this->request->data['BusinessBranch']) && !empty($this->request->data['BusinessBranch']) ){
                $branches = $this->request->data['BusinessBranch']['branch_name'];
                foreach( $branches as $k => $val ){
                    $temp = array();
                    $temp['BusinessBranch']['business_id']  = $id;
                    $temp['BusinessBranch']['branch_name']  = $val;
                    $temp['BusinessBranch']['latitude']     = $this->request->data['BusinessBranch']['latitude'][$k];
                    $temp['BusinessBranch']['longitude']    = $this->request->data['BusinessBranch']['longitude'][$k];
                    $temp['BusinessBranch']['status']  = 1;

                    $bis_branches_data[] = $temp;
                }

                $this->BusinessBranch->saveAll($bis_branches_data);

            }

        }

        $this->Session->setFlash(__( 'Information has been saved.')
                      .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                    'default',
                    array('class'=>'alert alert-dismissable alert-success '));

        $logMessage = json_encode($this->Business->findById($id));

        $this->generateLog($logMessage,' EDIT BUSINESS ');    


      } else {
        $this->Session->setFlash(__( 'Information could not be save.')
                      .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                    'default',
                    array('class'=>'alert alert-dismissable alert-danger '));
      }

      return $this->redirect(array( 'controller' => 'dashboards', 'action' =>'index', 'plugin' => 'member' ));

    } else {

      $this->Business->recursive = -1;
      $options = array('conditions' => array('Business.' . $this->Business->primaryKey => $id));
      $this->request->data = $this->Business->find('first', $options);
      

      $pro_code   = $this->request->data['Business']['province']  ;
      $city_code  = $this->request->data['Business']['city'] ;
      $main  = $this->request->data['Business']['business_main_category']  ;

      // Get Branch Data
      $conds['conditions'] = array( 'BusinessBranch.business_id' => $id );
      $branches = $this->BusinessBranch->find('all', $conds);
      $this->set('branches', $branches);

      $province = new Province();
      $this->set('provinces', $province->find('all', array('conditions'=> array('Province.status' => 1) )));

      $city = new City();
      $this->set('cities', $city->find('all', array('conditions' => array('City.status' => 1 ), 
                'order' => array('City.province_code' => 'ASC') ) ));
      
      $location = new Location();
      $this->set('locations', $location->find('all', array('conditions' => array("Location.city_code" => $city_code, 'Location.status' => 1 ) ) ));

      $location_id  = $this->request->data['Business']['location_id'];
      $loc_name     = $this->request->data['Business']['location'];

      $conds = array();
      if( !$location_id ){
         $locInfo = $this->Location->findByLocationName($loc_name);
         if( $locInfo ){
           $location_id = $locInfo['Location']['id'];
         }
      }
      $conds['conditions'] = array( 'Sangkat.location_id' => $location_id, 'Sangkat.status' => 1 );
      $this->set('SangkatData', $this->Sangkat->find('all', $conds));

      $main_cate = new BusinessCategory();
      $this->BusinessCategory->recursive = -1;
      $this->set('subCates', $main_cate->find('all', array('conditions' => array("BusinessCategory.parent_id" => $main, 'BusinessCategory.status' => 1 ) ) ));


    }

  }


  public function operationHourEdit(){

    $user = CakeSession::read("Auth.MemberUser");

    if(  in_array($this->business_level, array(1,3)) && $user['access_level'] != 1 ){
      return $this->redirect(array('action' => 'index', 'controller' => 'dashboards', 'plugin' => 'member' ));
    }

    $id = $user['business_id'];

    if (!$this->Business->exists($id)) {
      // throw new NotFoundException(__('Invalid business'));
      $this->Session->setFlash(__( "Invalid Request !" )
                      .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                    'default',
                    array('class'=>'alert alert-dismissable alert-danger '));

      return $this->redirect(array('controller' => 'dashboards', 'action' =>'index', 'plugin' => 'member' ));
    }

    if ($this->request->is(array('post', 'put'))) {

      $o_hours = $this->request->data['OperationHour'];

      $arr_hours = array();

      foreach( $o_hours as $key => $value ){
        
        $oh_arr = array();
        
        $oh_arr['OperationHour']['id'] = $value['id'];
        $oh_arr['OperationHour']['business_id'] = $id;
        $oh_arr['OperationHour']['day'] = $key;
        $oh_arr['OperationHour']['from'] = $value['f'];
        $oh_arr['OperationHour']['to'] = $value['t'];

        $arr_hours[] = $oh_arr;

      }

      // var_dump($arr_hours); exit;

      $this->OperationHour->create();

      if( $this->OperationHour->saveAll($arr_hours)){

        $this->Session->setFlash(__( 'Operation Hours have been updated.')
                        .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                      'default',
                      array('class'=>'alert alert-dismissable alert-success '));

          $logMessage = json_encode($arr_hours);

          $this->generateLog($logMessage,' EDIT OPERATION HOURS ');
      }else{
        $this->Session->setFlash(__( 'Operation hours could not be updated, try again !!!')
                        .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                      'default',
                      array('class'=>'alert alert-dismissable alert-danger '));
      }

      return $this->redirect(array('controller' => 'dashboards', 'action' =>'index', 'plugin' => 'member' ));

    }else{

      $oh = $this->OperationHour->find('all', array( 'conditions' => array('business_id' => $id) ) );

      $this->set('operationHour', $oh );
      $this->set('operation_hours', $this->operation_hours);

    }

  }

  public function removeMedia( $media_id = 0 ){

    if (!$this->BusinessesMedia->exists($media_id)) {
      throw new NotFoundException(__('Invalid Request'));
    }

    $old_data = $this->BusinessesMedia->findById($media_id);

    if( $this->BusinessesMedia->delete($media_id) ){

      $st = "";
      if( $old_data['BusinessesMedia']['media_type'] == 0 ){
        $img = $old_data['BusinessesMedia']['media_path'];

        if( file_exists($img) ){
          $st = 1;
          unlink($img);
        }else{
          $st = 0;
        }
      }

      $this->generateLog('',' REMOVE IMAGE ');
      $data['status'] = true;
      $data['img'] = $st;

      echo json_encode($data); exit;
    }else{
      $data['status'] = false;
      echo json_encode($data); exit;
    }

  }


}
