<?php

App::uses('AppController', 'Controller');
App::uses('Userlog', 'Member.Model');
App::uses('Business', 'Member.Model');

class MemberAppController extends AppController {

	var $usrLog;

	public $business_level = NULL;

	var $components = array('Session','Cookie' , 'Image',
							'MemberAuth'=>array(
								'loginAction' => array(
						  		                  'controller' => 'login',
						  		                  'plugin' => 'member'
					   			             	),
								'loginRedirect' => array( 'controller' => 'deals', 'action' => 'index','plugin'=>'member'),
		   			         	// 'logoutRedirect' => array( 'controller' => 'MemberUsers', 'action' => 'login', 'plugin'=>'member' ) ,
		   			         	'logoutRedirect' => '/merchants' ,
		   			         	'authenticate' => array(
						            'Form' => array(
						            	'fields' => array('username' => 'email', 'password' => 'password'),
						            	'scope'=>array( 'status' => 1, 'access_level' => array(1,2) ),
						                'passwordHasher' => array(
						                    'className' => 'Simple',
						                    'hashType' => 'sha256'
						                ),					   					

						            )
						        )
							)
						);


	var $context  = '' ;
	
	var $conditionFilter = array();
	/*
	* Get store id mean unique id of object
	*/
	public function getSortId()
	{
		return md5($this->getContext());
	}
	/*
		Set state of the object
	*/

	public function setState($name,$value='')
	{
		$states = $this->getState();
		$states[$name] = $value;
		$this->Session->write(sha1($this->getSortId()),$states);	
	}

	/*
		Get State of the object
	*/
	public function getState($name='',$value='')
	{
		$stores = sha1($this->getSortId());
		$states = array();
		if($this->Session->check($stores))
		{
			$states = $this->Session->read($stores);

		}
		if(!$name) return $states;
		if(isset($states[$name])) return $states[$name];
		return $value;		
		
	}


	/*
		Set coditoin called before listing record method
	*/
		
	function setCondition()
	{
				
		if(isset($this->request->data['business_name']))
		{
			$this->setState('business_name', $this->request->data['business_name']);
		}

		if(isset($this->request->data['province']))
		{
			$this->setState('province',$this->request->data['province']);
		}

		if(isset($this->request->data['city']))
		{
			$this->setState('city',$this->request->data['city']);
		}

		if(isset($this->request->data['main_category']))
		{
			$this->setState('main_category',$this->request->data['main_category']);
		}

		if(isset($this->request->data['sub_category']))
		{
			$this->setState('sub_category',$this->request->data['sub_category']);
		}

		if(isset($this->request->data['status']) )
		{
				$this->setState('status',$this->request->data['status']);
		}

		$this->set('statusArray',$this->states);

	}

	/*
		Default cake php before filter action 
		Call to set anything before render view
	*/

	function beforeFilter(){

		parent::beforeFilter();

		$this->userLog = new Userlog();

		$user = CakeSession::read("Auth.MemberUser");

		$biz_id = $user['business_id'];

		$business = new Business();


		$conditions = array( 'conditions' => array( 'Business.id' => $biz_id ),
							 'fields' => array('Business.id', 'Business.member_level') );

		$this->Business->recursive = -1;
		$info = $business->find('first', $conditions);
		$this->business_level = $info['Business']['member_level'];
		$this->set('business_member_level', $info['Business']['member_level']);

	}

	/*
		Get context of the controller
		or the matched model
	*/
	
	function getContext()
	{
		if(isset($this->context) && $this->context)return $this->context;
		$className = get_class($this);
		$className = substr($className, 0,strlen($className)- strlen('Controller'));
		return $className;
	}	
	
	/*
		Get model by name
	*/

	public function getModel($name=''){		
		// return $name;
		if(!$name) $name= $this->context;
		if(!$name) $name = $this->getContext();
		return $this->$name;
	}

	/*
		Saving record to databsae by id
	*/

	public function save($id=0)
	{

		$model = $this->getModel();

		// var_dump($model); exit;

		if($id==0)
		{
			//add new 
			$model->create();

			if($model->save($this->request->data))
			{
				$this->Session->setFlash(__( $this->context.' has been saved.')
										.'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
									'default',
									array('class'=>'alert alert-dismissable alert-success '));
				$id  = $model->getLastInsertId();
				$this->request->data['id'] = $id;
				$logMessage = json_encode($this->request->data);
				$this->generateLog($logMessage,' CREATE NEW :'.$id);				
				return $this->redirect(array('action' =>'index'));
			}
			
			$this->Session->setFlash(__('Unable to add '.$this->context),
									'default',
									array('class'=>'alert alert-dismissable alert-danger '));

		}
		else
		{
			if($this->request->is(array('post','put')))
			{
				$model->id = $id;
				$obj 	= $model->findById($id);
				if(!count($obj))
				{
					throw new NotFoundException();
					
				}
				$logMessage = json_encode($obj);
				$this->generateLog($logMessage,' EDIT :'.$id);
				
				if($model->save($this->request->data))
				{

					$this->Session->setFlash(__( $this->context.' has been saved.')
									.'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
									'default',
									array('class'=>'alert alert-dismissable alert-success '));
					return $this->redirect(array('action' =>'index'));
				}
			}
			$this->Session->setFlash(__('Unable to save '.$this->context).
										'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
										'default',
										array('class'=>'alert alert-dismissable alert-danger '));
			
		}
		return $this->redirect(array('action' =>'index'));
	}



	public $paginate = array('limit' =>20); //paginate default limit to 20

	/*
		lising the record
	*/

	public function index()
	{	
		$this->setCondition();	

		$data = $this->paginate($this->getContext());	

		$this->set(compact('data'));
		$states = $this->getState();
		$this->set('states',$states);
		
	}


	public function logDeleteList($id=array())
	{
		$model = $this->getModel();
		$data = $model->find('all',array('conditions'=>
										array(
												$this->getContext().'.id'=>$id
											)
										)
						);
		$logMessage = json_encode($data);
		$this->generateLog($logMessage,' DELETE LIST ');

	}

	/*
		Generate log mesage to table users_logs
		Store old data and history by user_id
	*/

	public function generateLog($message,$action)
	{
		$user = CakeSession::read("Auth.MemberUser");		
		$this->Userlog = new Userlog();
		$total = $this->Userlog->find('count');
		if($total>10000)
		{
			$this->Userlog->query('TRUNCATE TABLE '.$this->Userlog->table);
		}
		$this->Userlog->create();
		$this->Userlog->save(
							array('userid'=>$user['id'],							
								'log'=>$message,
								'action'=>$action,
								'context'=>$this->getContext(),
								'ip'=>$this->request->clientIp()
								)
							);

	}
	/*
		delete list of record by id[]
	*/
	public function deleteList()
	{
		$cb = $this->request->data['cb'];
		if(count($cb))
		{
			$lstid = implode(',', $cb);
			$model = $this->getModel();
			$this->logDeleteList($cb);
			$sql 	='DELETE FROM '.$model->tablePrefix.$model->table.
						' WHERE '.$model->primaryKey.' IN ('.($lstid).')';

			$model->query($sql);

			
				$this->Session->setFlash(
									__( '%d '.$this->context.'(s) has been selected',count($cb))
										,'default'
										, array('class'=>'alert alert-dismissable alert-success '));
				
				return $this->redirect(array('action' =>'index'));		


		}
		else
		{
			$this->Session->setFlash(
									__( 'No '. $this->context.' has been selected')
										.'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'
										,'default'
										, array('class'=>'alert alert-dismissable alert-warning '));
				
				return $this->redirect(array('action' =>'index'));
		}
	}

	/*
		Default function edit by id
	*/
	public function edit($id=0)
	{
		if(!$id)
		{
			throw new NotFoundException(__('Invalid '.$this->getContext()));
		}
		$model = $this->getModel();		
		$data   = $model->findById($id);		
		if(!count($data))throw new NotFoundException(__('Invalid '.$this->getContext()));
		$this->set('data',$data);
		
	}

	/*
		Default function index
	*/
	// public function index()
	// {
	// 	return $this->redirect(array('action'=>'index'));
	// }

	/*
		Update state of the record to (0 inactive, 1 active .... etc) 
	*/

	public function updateState($lstId=array(),$state=1) {

		$this->Userlog = new Userlog();
		$model = $this->getModel();

		$data = $model->find('all',array('conditions'=>
										array(
												$this->getContext().'.id'=>$lstId
											)
										)
						);

		$logMessage  = json_encode($data);
		$this->generateLog($logMessage,'UPDATE STATUS LIST : '.$state);
		$sql = ' UPDATE tb_'.$model->table.' SET status = '.$state.
				' WHERE id IN ('.implode(",", $lstId).')';
		$model->query($sql);

	}

	/*
		delete record by id
		log the record for keep as history
	*/
	public function delete($id=0) {

		$model 	= $this->getModel();
		$obj 	= $model->findById($id);

		if(!count($obj)){
			throw new NotFoundException();
		}

		
		$logMessage = json_encode($obj);
		$this->generateLog($logMessage,'DELETE : '.$id);
		// end log

		if($model->delete($id))
		{
			$this->Session->setFlash(__( $this->getContext().' has been deleted')
									.'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>'
										,
									'default',
									array('class'=>'alert alert-dismissable alert-success '));
			return $this->redirect(array('action'=>'index'));
		}
		return $this->redirect(array('action' =>'index'));


	}

	/* function save file */

	public function saveFile($fileName,$path,$fileType=array(), $width = NULL, $height = NULL, $minW = NULL, $thumb = false )
	{
		if(!count($fileType)) {

			$fileType = array("jpg","png","jpeg","PNG","JPG","JPEG");

		}

		if(!isset($_FILES[$fileName])){
			$data['status'] = false;
			$data['msg'] 	= "File not found.";
			return $data;
		}
		
		$name 	= 	$_FILES[$fileName]["name"];
	    $tmp 	=  	$_FILES[$fileName]["tmp_name"];
	    $size 	= 	$_FILES[$fileName]["size"];

	    if($size ==0) {
	    	return "";
	    }	

	    $img_size 		= getimagesize($tmp);
		$img_width 		= $img_size[0];
		$img_height 	= $img_size[1];

	    if( $width && $height ){
	    	if( $img_width < $width && $img_height < $height ){
				$data['status'] = false;
				$data['msg'] 	= "Failed to save image. Image size should be " . $width . "x" . $height . " pixels minimum.";
				return $data;
	    	}
	    }

	    if( $minW && $img_width < $minW ){
	    	if( $img_width < $width && $img_height < $height ){
				$data['status'] = false;
				$data['msg'] 	= "Failed to save image. Image width must be " . $minW . " pixels minimum.";
				return $data;
	    	}
	    }
	    
	    $ext = pathinfo($name);
	    $ext = $ext['extension'];

	    // if(!in_array($ext, $fileType)) throw new Exception(' Invalid file format');
	    
	    if(!in_array( $ext, $fileType)){
			$data['status'] = false;
			$data['msg'] 	= "Invalid File. " ;
			return $data;
	    }
	    
    	$actual_image_name = sha1(time().rand(1,100)).".".$ext;
    	move_uploaded_file($_FILES[$fileName]["tmp_name"], "$path/$actual_image_name");  
    	// copy ( "$path/$actual_image_name" , "$path/thumbs/$actual_image_name" ) ;  
	    return "$path/$actual_image_name";

	}

	public function saveMultiple($fileName,$path,$fileType=array(), $width = NULL, $height = NULL)
	{
		if(!count($fileType))
		{
			$fileType = array("jpg","png","jpeg","PNG","JPG","JPEG");
		}

		if(!isset($fileName)){
			$data['status'] = false;
			$data['msg'] 	= "Some files are not found.";
			return $data;
		}

		
		$name = $fileName["name"];
	    $tmp=  $fileName["tmp_name"];
	    $size=$fileName["size"];

	    $img_size 		= getimagesize($tmp);
		$img_width 		= $img_size[0];
		$img_height 	= $img_size[1];

	    if( $width && $height ){
	    	if( $img_width < $width && $img_height < $height ){
				$data['status'] = false;
				$data['msg'] 	= "Failed to save image. Image size should be " . $width . "x" . $height . " pixels minimum.";
				return $data;
	    	}
	    }
	    
	    if($size ==0)
	    {
	    	return "";
	    }
	    
	    $ext = pathinfo($name);
	    $ext = $ext['extension'];
	  
	    if(!in_array($ext, $fileType)) {
			$data['status'] = false;
			$data['msg'] 	= "Some files are invalid.";
			return $data;
	    }
	    
    	$actual_image_name = sha1(time().rand(1,100))."." . $ext;

    	move_uploaded_file($tmp, "$path/$actual_image_name");
	    return "$path/$actual_image_name";

	}

}