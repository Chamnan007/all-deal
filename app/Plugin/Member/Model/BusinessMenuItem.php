<?php
App::uses('MemberAppModel', 'Member.Model');
/**
 * Business Model
 *
 * @property BusinessCategory $BusinessCategory
 * @property OperationHour $OperationHour
 * @property User $User
 */
class BusinessMenuItem extends MemberAppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'id';


	public $belongsTo = array(

		'MenuDetail' => array(
			'className' => 'Member.BusinessMenu',
			'foreignKey' => 'menu_id',
			'conditions' => '',
			'fields' =>  array("name"),
			'order' => ''
		),

	);


	public $actsAs = array(
	     'Sluggable' => array(
	             'fields' => 'title',
	             'scope' => false,
	             'conditions' => false,
	             'slugfield' => 'slug',
	             'separator' => '-',
	             'overwrite' => true,
	             'length' => 500,
	             'lower' => true
	         )
	   );

}
