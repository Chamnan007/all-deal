<?php
App::uses('MemberAppModel', 'Member.Model');
/**
 * Business Model
 *
 * @property BusinessCategory $BusinessCategory
 * @property OperationHour $OperationHour
 * @property User $User
 */
class Business extends MemberAppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'id';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'MainCategory' => array(
			'className' => 'Member.BusinessCategory',
			'foreignKey' => 'business_main_category',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),

		'SubCategory' => array(
			'className' => 'Member.BusinessCategory',
			'foreignKey' => 'business_sub_category',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),

		'ApprovedBy' => array(
			'className' => 'Member.MemberUser',
			'foreignKey' => 'approved_by',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),

		'RegisteredBy' => array(
			'className' => 'Member.MemberUser',
			'foreignKey' => 'registered_by',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),

		'SangkatInfo' => array(
			'className' => 'Member.Sangkat',
			'foreignKey' => 'sangkat_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
		
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(

		'OperationHour' => array(
			'className' => 'Member.OperationHour',
			'foreignKey' => 'business_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),

		'Pictures' => array(
			'className' => 'Member.BusinessesMedia',
			'foreignKey' => 'business_id',
			'dependent' => false,
			'conditions' => array("media_type = 0") ,
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),

		'Videos' => array(
			'className' => 'Member.BusinessesMedia',
			'foreignKey' => 'business_id',
			'dependent' => false,
			'conditions' => array("media_type = 1")  ,
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),

		'Branches' => array(
			'className' => 'Member.BusinessBranch',
			'foreignKey' => 'business_id',
			'dependent' => false,
			'conditions' => array( 'Branches.status' => 1 ) ,
			'fields' => '',
			'order' => array( 'Branches.branch_name' => 'ASC' ),
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),

		'MenuCategories' => array(
			'className' => 'Member.BusinessMenu',
			'foreignKey' => 'business_id',
			'dependent' => false,
			'conditions' => array( 'MenuCategories.status' => 1 ) ,
			'fields' => '',
			'order' => array( 'MenuCategories.name' => 'ASC' ),
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
