<?php
App::uses('MemberAppModel', 'Member.Model');
/**
 * Business Model
 *
 * @property BusinessCategory $BusinessCategory
 * @property OperationHour $OperationHour
 * @property User $User
 */

class BusinessRevenue extends MemberAppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'id';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */

	
public $belongsTo = array(

		'TransactionInfo' => array(
			'className' => 'Member.BuyerTransaction',
			'foreignKey' => 'transaction_id',
			'conditions' => '',
			'fields' =>  '',
			'order' => ''
		),

		'PaymentInfo' => array(
			'className' => 'Member.PaymentHistory',
			'foreignKey' => 'payment_id',
			'conditions' => '',
			'fields' =>  '',
			'order' => ''
		),

		'BusinessDetail' => array(
			'className' => 'Business',
			'foreignKey' => 'business_id',
			'conditions' => '',
			'fields' =>  '',
			'order' => ''
		),
	);

	

}
