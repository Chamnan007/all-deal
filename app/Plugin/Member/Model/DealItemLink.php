<?php
App::uses('MemberAppModel', 'Member.Model');
/**
 * Location Model
 *
 */

class DealItemLink extends MemberAppModel {

	public $displayField = 'id';

	public $belongsTo = array(

		'ItemDetail' => array(
			'className' => 'Member.BusinessMenuItem',
			'foreignKey' => 'item_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),

	);
	
}
