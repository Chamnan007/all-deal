<?php
App::uses('MemberAppModel', 'Member.Model');
/**
 * BusinessCategory Model
 *
 */
class BusinessCategory extends MemberAppModel {

/**
 * Validation rules
 *
 * @var array
 */

    public $hasMany = array(
        'ChildCategory'    => array(
            'className'     => 'Member.BusinessCategory',
            'foreignKey'    => 'parent_id',
            'dependent'     => false,
            'conditions'    => array('ChildCategory.status' => 1) ,
            'fields'        => '',
            'order'         => array('ChildCategory.category' => 'ASC' ),
            'limit'         => '',
            'offset'        => '',
            'exclusive'     => '',
            'finderQuery'   => '',
            'counterQuery'  => ''
            )
        );

}
