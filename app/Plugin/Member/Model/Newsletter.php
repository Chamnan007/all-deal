<?php
App::uses('AdministratorAppModel', 'Administrator.Model');
/**
 * Business Model
 *
 * @property BusinessCategory $BusinessCategory
 * @property OperationHour $OperationHour
 * @property User $User
 */
class Newsletter extends AdministratorAppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'id';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */

	public $actsAs = array(
	     'Sluggable' => array(
	             'fields' => 'subject',
	             'scope' => false,
	             'conditions' => false,
	             'slugfield' => 'slug',
	             'separator' => '-',
	             'overwrite' => true,
	             'length' => 500,
	             'lower' => true
	         )
	   );

	public $belongsTo = array(
				'SentBy' => array(
					'className' => 'Member.MemberUser',
					'foreignKey' => 'sent_by',
					'conditions' => '',
					'fields' => array('first_name', 'last_name' ) ,
					'order' => ''
				) 
		);
	

}
