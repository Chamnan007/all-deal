<?php
App::uses('MemberAppModel', 'Member.Model');
App::uses('SimplePasswordHasher', 'Controller/Component/Auth');
/**
 * User Model
 *
 */
class MemberUser extends MemberAppModel {

/**
 * Validation rules
 *
 * @var array
 */
var $useTable = 'users';

	public function beforeSave($options = array()) {
        
	    /* password hashing */

	    if ( isset($this->data[$this->alias]['password']) && $this->data[$this->alias]['password'] != "" ) {
	    	$passwordHasher = new SimplePasswordHasher(array('hashType'=>'sha256'));

	  		$hashedPassword = $passwordHasher->hash(
                $this->data[$this->alias]['password']
            );

	     	$this->data[$this->alias]['password'] = $hashedPassword;
	    }
	    
	    return parent::beforeSave($options);


	}

public $belongsTo = array(

		'Business' => array(
			'className' => 'Businesses',
			'foreignKey' => 'business_id',
			'conditions' => '' ,
			'fields' => array('Business.status') ,
			'order' => ''
			)
);




}