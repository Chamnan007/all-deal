<?php
App::uses('MemberAppModel', 'Member.Model');
/**
 * Business Model
 *
 * @property BusinessCategory $BusinessCategory
 * @property OperationHour $OperationHour
 * @property User $User
 */

class Deal extends MemberAppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'id';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */

	public $belongsTo = array(

		'CreatedBy' => array(
			'className' => 'Member.MemberUser',
			'foreignKey' => 'created_by',
			'conditions' => '',
			'fields' =>  array('first_name', 'last_name'),
			'order' => ''
		),

		'UpdatedBy' => array(
			'className' => 'Member.MemberUser',
			'foreignKey' => 'updated_by',
			'conditions' => '',
			'fields' =>  array('first_name', 'last_name'),
			'order' => ''
		),

		'BranchInfo' => array(
			'className' => 'Member.BusinessBranch',
			'foreignKey' => 'branch_id',
			'conditions' => '',
			'fields' =>  "",
			'order' => ''
		),

		'BusinessInfo' => array(
			'className' => 'Member.Business',
			'foreignKey' => 'business_id',
			'conditions' => '',
			'fields' =>  '',
			'order' => ''
		),
	);

	public $actsAs = array(
     'Sluggable' => array(
             'fields' => 'title',
             'scope' => false,
             'conditions' => false,
             'slugfield' => 'slug',
             'separator' => '-',
             'overwrite' => true,
             'length' => 500,
             'lower' => true
         )
   );

	public $hasMany = array(

		'DealItemLink' => array(
			'className' => 'Member.DealItemLink',
			'foreignKey' => 'deal_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)

	);
	

}
