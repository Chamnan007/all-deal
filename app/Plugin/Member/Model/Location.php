<?php
App::uses('MemberAppModel', 'Member.Model');
/**
 * Location Model
 *
 */
class Location extends MemberAppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $belongsTo = array(
		'City' => array(
			'className' => 'Member.City',
			'foreignKey' => 'city_code',
			'conditions' => 'City.status = 1',
			'fields' => '',
			'order' => ''
		)
	);
}
