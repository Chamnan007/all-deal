<?php
App::uses('MemberAppModel', 'Member.Model');
/**
 * Business Model
 *
 * @property BusinessCategory $BusinessCategory
 * @property OperationHour $OperationHour
 * @property User $User
 */
class BusinessMenu extends MemberAppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'id';


public $actsAs = array(
	     'Sluggable' => array(
	             'fields' => 'name',
	             'scope' => false,
	             'conditions' => false,
	             'slugfield' => 'slug',
	             'separator' => '-',
	             'overwrite' => true,
	             'length' => 500,
	             'lower' => true
	         )
	   );


}
