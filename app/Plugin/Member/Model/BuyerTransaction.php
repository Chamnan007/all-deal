<?php

/********************************************************************************

File Name: Buyer Model
Description: Model file for user

Powered By: ABi Investment Group Co., Ltd,

Changed History:

  	Date				Author				Description
  	2015/04/09    		Phea Rattana		Initial Version

*********************************************************************************/


App::uses('MemberAppModel', 'Member.Model');
/**
 * User Model
 *
 */
class BuyerTransaction extends MemberAppModel {

/**
 * Validation rules
 *
 * @var array
 */

	public $belongsTo = array(

		'BusinessDetail' => array(
			'className' => 'Member.Business',
			'foreignKey' => 'business_id',
			'conditions' => '',
			'fields' =>  '' ,
			'order' => ''
		),

		'BuyerInfo' => array(
			'className' => 'Member.Buyer',
			'foreignKey' => 'buyer_id',
			'conditions' => '',
			'fields' =>  '' ,
			'order' => ''
		),

		'MarkReceivedBy' => array(
			'className' => 'Member.MemberUser',
			'foreignKey' => 'mark_received_by',
			'conditions' => '',
			'fields' =>  '' ,
			'order' => ''
		),

		'DealInfo' => array(
			'className' => 'Member.Deal',
			'foreignKey' => 'deal_id',
			'conditions' => '',
			'fields' =>  "" ,
			'order' => ''
		)
	);
	
	public $hasMany = array(

		'TransactionDetail' => array(
			'className' => 'BuyerTransactionDetail',
			'foreignKey' => 'transaction_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),

	);


}
