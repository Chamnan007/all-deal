<?php
/********************************************************************************
    File: Model Province
    Author: PHEA RATTANA

    Confidential ABi Technologies property.

  	Changed History:
  	Date			Author			Description
  	2014/02/04    	PHEA RATTANA	
*********************************************************************************/



App::uses('MemberAppModel', 'Member.Model');
/**
 * Province Model
 *
 */
class Province extends MemberAppModel {

/**
 * Validation rules
 *
 * @var array
 */

	// public $hasMany = array(
	// 	'City' => array(
	// 		'className' => 'City',
	// 		'foreignKey' => 'city_code',
	// 		'dependent' => false,
	// 		'conditions' => '',
	// 		'fields' => '',
	// 		'order' => '',
	// 		'limit' => '',
	// 		'offset' => '',
	// 		'exclusive' => '',
	// 		'finderQuery' => '',
	// 		'counterQuery' => ''
	// 	)
	// );
}
