<?php
	App::uses('WebservicesAppController', 'Webservices.Controller');
	// App::uses('CakeEmail', 'Network/Email');
	App::uses('SimplePasswordHasher', 'Controller/Component/Auth');
	// App::import('Helper', 'Html');
	/**
	 * Abouts Controller
	 *
	 * @property About $About
	 * @property PaginatorComponent $Paginator
	 */
	class FunctionsController extends WebservicesAppController {

	/**
	 * Components
	 *
	 * @var array
	 */
		// var $context = 'About';
		
		var $uses = array(	'OperationHour', 
							'Deal', 
							'DealOfTheDay',
							'BusinessesMedia', 
							'Province', 
							'Location', 
							'Sangkat', 
							'City', 
							'Destination', 
							'BusinessCategory', 
							'BusinessBranch',
							'InterestsCategory', 
							'Buyer', 
							'MailConfigure',
							'Commission',
							'BuyerTransaction',
							'DealItemLink',
							'BusinessMenu',
							'GoodsCategory',
							'User',
							'Buyer',
							'ReferalToken',
							'BuyerTransaction'
						);

		var $path = "";

		var $sortDeals = array( 
							'title' => 'Deal.title',
							'valid_to' => 'Deal.valid_to',
							'publish_date' => 'Deal.publish_date'
		 				);

		var $sortBusinesses = array( 
							'business_name' => 'Business.business_name',
							'created_date' => 'Business.created',
							'member_level' => 'Business.member_level'
		 				);

		var $senderEmail = '';

		var $bonus_id = 2; // Sign Up Bonus
		var $referer_bonus	= 3;
		var $referee_bonus	= 4;

		var $type_in  = 1;
		var $type_out = 0;


		public function index(){
			echo "Invalid request !!!";
			exit;
		}

		public function beforeFilter(){
			$this->path = Router::url('/', true) . "app/webroot/";

			$this->senderEmail = $this->admin_email;

			date_default_timezone_set('Asia/Phnom_Penh');
		}

		public function convertDeals($deals){
			if( is_array($deals) && !empty($deals) ){

				$preparedData = array();

				foreach ($deals as $key => $value) {

					// Prepare Deal Images
					$deal_img 		= $value['Deal']['image'];
					$other_images 	= $value['Deal']['other_images'];
					$data_images 	= array();
				    if( $other_images ){
				        $other_images = json_decode($other_images);
				        foreach( $other_images as $k => $img ){
				            if( $img != "img/deals/default.jpg" ){
				                $data_images[] = $this->path . $img;
				            }
				        }
				        $selected = @$data_images[array_rand($data_images)];

				        if( $selected ){ 
				            $deal_img = $selected ;
				        }
				    }

					$cate_ids = ($value['Deal']['goods_category'] == '""')?array():json_decode($value['Deal']['goods_category']);

					$this->GoodsCategory->recursive = -1;
					$categories = $this->GoodsCategory->find('all', array('conditions' => array('GoodsCategory.id' => $cate_ids)) );
					$str_cate = "";
					foreach( $categories as $cateKey => $cateValue ){
						$str_cate .= $cateValue['GoodsCategory']['category'] . ", ";
					}

					$str_cate = rtrim($str_cate, ", ");

					// Deal Item
					$prev_original  = 0;
	              	$prev_discount  = 0;
	              	$max_percentage = 0;

	              	$original = $discount = 0;
	              	$itemList = $value['DealItemLink'];
					if( $itemList ){
						foreach( $itemList as $k => $item ){
							$ori        = ($item['original_price'] != 0 )?$item['original_price']:$item['ItemDetail']['price'];
	                        $disc       = $item['discount'];

	                        $percentage = 100 - ( $disc * 100 / $ori );
	                        if( $percentage > $max_percentage ){
	                            $max_percentage = $percentage;
	                            $prev_original  = $ori;
	                            $prev_discount  = $disc;
	                        }else if( $percentage == $max_percentage ){
	                            if( $disc < $prev_discount ){
	                                $prev_original  = $ori;
	                                $prev_discount  = $disc;
	                            }
	                        }
						}

						$original = $prev_original;
						$discount = $prev_discount;
					}

					$preparedData[$key]['Deal']['id'] 				= $value['Deal']['id'];
					$preparedData[$key]['Deal']['title'] 			= $value['Deal']['title'];
					$preparedData[$key]['Deal']['description'] 		= $value['Deal']['description'];
					$preparedData[$key]['Deal']['term_condition'] 	= $value['Deal']['deal_condition'];
					$preparedData[$key]['Deal']['category'] 		= $str_cate;
					$preparedData[$key]['Deal']['original_price'] 	= $original;
					$preparedData[$key]['Deal']['discounted_price'] = $discount;
					$preparedData[$key]['Deal']['image'] 			= $deal_img;
					$preparedData[$key]['Deal']['other_images'] 	= $data_images;

					$preparedData[$key]['Merchant']['id']		= $value['Business']['id'];
					$preparedData[$key]['Merchant']['name'] 	= $value['Business']['business_name'];
					$preparedData[$key]['Merchant']['logo'] 	= $this->path . $value['Business']['logo'];
					$preparedData[$key]['Merchant']['phone1'] 	= $value['Business']['phone1'] ;
					$preparedData[$key]['Merchant']['phone2'] 	= $value['Business']['phone2'] ;
					$preparedData[$key]['Merchant']['latitude'] = $value['Business']['latitude'] ;
					$preparedData[$key]['Merchant']['longitude']= $value['Business']['longitude'];

					$address = ($value['Business']['street'])?$value['Business']['street'] . ", ":"";
					$address .= ($value['BusinessInfo']['SangkatInfo'])?$value['BusinessInfo']['sangkatInfo']['name'] . ", ":"";
					$address .= ($value['Business']['location'])?$value['Business']['location'] . ", ":"";
					$address .= ($value['City']['city_name'])?$value['City']['city_name']:"";

					$preparedData[$key]['Merchant']['address'] = $address;

				}
				return $preparedData;
			}
			return false;
		}

		public function convertBusinesses($businesses){

			if( is_array($businesses) && !empty($businesses) ){

				$preparedData = array();

				foreach ($businesses as $key => $value) {

					$arrPictures = $value['Pictures'];
					$images = array();
					if( !empty($arrPictures) && is_array($arrPictures) ){
						foreach ($arrPictures as $pKey => $pValue) {
							$images[] = $this->path . $pValue['media_path'];
						}
					}

					$mainCategoryInfo = $this->BusinessCategory->findById($value['Business']['business_main_category']);

            		$preparedData[$key]['id'] 		= $value['Business']['id'];
            		$preparedData[$key]['name'] 	= $value['Business']['business_name'];
            		$preparedData[$key]['description'] 	= $value['Business']['description'];
            		$preparedData[$key]['category'] = $mainCategoryInfo['BusinessCategory']['category'];
            		$preparedData[$key]['logo'] 	= $this->path . $value['Business']['logo'];
            		$preparedData[$key]['images'] 	= $images;
            		$preparedData[$key]['location']	= $value['Business']['location'];

            		$address = ($value['Business']['street'])?$value['Business']['street'] . ", ":"";
					$address .= ($value['Business']['SangkatInfo']['name'])?$value['Business']['sangkatInfo']['name'] . ", ":"";
					$address .= ($value['Business']['location'])?$value['Business']['location'] . ", ":"";
					$address .= ($value['City']['city_name'])?$value['City']['city_name']:"";

					$preparedData[$key]['address'] 	= $address;
            		$preparedData[$key]['phone1'] 	= $value['Business']['phone1'];
            		$preparedData[$key]['phone2'] 	= $value['Business']['phone2'];
            		$preparedData[$key]['email'] 	= $value['Business']['email'];

            		$website = $value['Business']['website'];
            		$website = str_replace("http://", "" , $website);
            		$website = str_replace("https://", "" , $website);
            		$website = ($website)?"http://" . $website:"" ;

            		$preparedData[$key]['website'] 	= $website;
            		$preparedData[$key]['latitude'] 	= $value['Business']['latitude'];
            		$preparedData[$key]['longitude'] 	= $value['Business']['longitude'];

				}

				return $preparedData;
			}
			return false;
		}

		public function getCity(){

			$getParam = $this->request->query;
			$secureToken = (!empty($getParam['token'])) ? $getParam['token'] : "";
			$provinceCode = (!empty($getParam['province_code'])) ? $getParam['province_code'] : "";

			if( $this->validateToken( $this->appId, $this->appKey, $secureToken ) ){

				$arrCondition[0] = array('City.status' => 1);
				if( !empty($provinceCode) ){
					$arrCondition[1] = array('City.province_code' => $provinceCode);
				}

				$conditions = $arrCondition;

				$cities = $this->City->find('all', array('conditions' => $conditions, 'order' => array('City.city_name ASC')));
				
				if( $cities ){
					$result['status'] = true;
					$result['data'] = $cities;
				}else{
					$result['status'] = false;
					$result['data'] = array();
				}

			}else{
				$result['status'] = false;
				$result['data'] = "Invalid Token";
			}

		    // no view to render
		    $this->autoRender = false;
		    $this->response->type('json');
		    $json = json_encode($result,JSON_FORCE_OBJECT);
		    $this->response->body($json);

		}

		public function getProvinceList(){

			$getParam 		= $this->request->query;
			$secureToken 	= (!empty($getParam['token'])) ? $getParam['token'] : "";

			if( $this->validateToken( $this->appId, $this->appKey, $secureToken ) ){

				$arrCondition[0] = array('City.status' => 1);

				$conditions = $arrCondition;
				$cities = $this->City->find('all', array('conditions' => $conditions, 'order' => array('City.city_name ASC')));
				
				if( $cities && !empty($cities) ){
					$result['status'] = true;

					$preparedData = array();
					foreach( $cities as $k => $val ){
						$preparedData[$k]['id'] 			= $val['City']['id'];
						$preparedData[$k]['province_code'] 	= $val['City']['city_code'];
						$preparedData[$k]['province_name'] 	= $val['City']['city_name'];
					}

					$result['data'] = $preparedData;

				}else{
					$result['status'] = false;
					$result['data'] = array();
				}

			}else{
				$result['status'] = false;
				$result['data'] = "Invalid Token";
			}

		    // no view to render
		    $this->autoRender = false;
		    $this->response->type('json');
		    $json = json_encode($result,JSON_FORCE_OBJECT);
		    $this->response->body($json);

		}


		public function getLocation(){

			$getParam = $this->request->query;
			$secureToken = (!empty($getParam['token'])) ? $getParam['token'] : "";
			$province_code = (!empty($getParam['province_code'])) ? $getParam['province_code'] : "";

			if( $this->validateToken( $this->appId, $this->appKey, $secureToken ) ){

				if( $province_code ){

					$this->Location->recursive = -1;
					$arrCondition[0] = array('Location.status' => 1);

					if( !empty($province_code) ){
						$arrCondition[1] = array('Location.city_code' => $province_code);
					}

					$conditions = $arrCondition;

					$locations = $this->Location->find('all', array('conditions' => $conditions, 'order' => array('Location.location_name ASC')));
					
					if( $locations ){
						$result['status'] = true;

						$preparedData = array();
						foreach( $locations as $k => $v ){
							$preparedData[$k]['id'] 	= $v['Location']['id'];
							$preparedData[$k]['location_name'] 	= $v['Location']['location_name'];
						}
						$result['data'] = $preparedData;
					}else{
						$result['status'] = false;
						$result['data'] = array();
					}

				}else{
					$result['status'] = false;
					$result['data'] = "No province is selected.";
				}
			}else{
				$result['status'] = false;
				$result['data'] = "Invalid Token";
			}

		    // no view to render
		    $this->autoRender = false;
		    $this->response->type('json');
		    $json = json_encode($result,JSON_FORCE_OBJECT);
		    $this->response->body($json);

		}

		public function getProvince(){

			$getParam = $this->request->query;
			$secureToken = (!empty($getParam['token'])) ? $getParam['token'] : "";

			if( $this->validateToken( $this->appId, $this->appKey, $secureToken ) ){

				$provinces = $this->Province->find('all', array('conditions' => array('Province.status'=>1), 'order' => array('province_name ASC')));
				
				if( $provinces ){
					$result['status'] = true;
					$result['data'] = $provinces;
				}else{
					$result['status'] = false;
					$result['data'] = array();
				}

			}else{
				$result['status'] = false;
				$result['data'] = "Invalid Token";
			}

		    // no view to render
		    $this->autoRender = false;
		    $this->response->type('json');
		    $json = json_encode($result,JSON_FORCE_OBJECT);
		    $this->response->body($json);

		}

		public function getSangkatByLocation(){

			$getParam 		= $this->request->query;
			$secureToken 	= (!empty($getParam['token'])) ? $getParam['token'] : "";
			$locationID 	= (!empty($getParam['location_id'])) ? $getParam['location_id'] : "";
			$locationName 	= (!empty($getParam['location_name'])) ? $getParam['location_name'] : "";

			if( $this->validateToken( $this->appId, $this->appKey, $secureToken ) ){

				if( $locationID || $locationName ){

					$arrCondition[0]['Sangkat.status'] = 1;
					$arrCondition[1]['OR']['Sangkat.location_id'] = $locationID;
					$arrCondition[1]['OR']['Location.location_name'] = $locationName;
					
					$conditions = $arrCondition;
					$info = $this->Sangkat->find('all', array('conditions' => $conditions, 'order' => array('Sangkat.name ASC')));

					if( $info ){
						$result['status'] = true;
						$preparedData = array();

						foreach( $info as $k => $val ){
							$preparedData[$k]['id'] 			= $val['Sangkat']['id'];
							$preparedData[$k]['sangkat_name'] 	= $val['Sangkat']['name'];
						}

						$result['data'] = $preparedData;
					}else{
						$result['status'] = false;
						$result['data'] = array();
					}

				}else{
					$result['status'] = false;
					$result['data'] = "No location is selected.";
				}
			}else{
				$result['status'] = false;
				$result['data'] = "Invalid Token";
			}

		    // no view to render
		    $this->autoRender = false;
		    $this->response->type('json');
		    $json = json_encode($result,JSON_FORCE_OBJECT);
		    $this->response->body($json);
		}

		public function getBusinessCategory(){

			$getParam = $this->request->query;
			$secureToken = (!empty($getParam['token'])) ? $getParam['token'] : "";

			if( $this->validateToken( $this->appId, $this->appKey, $secureToken ) ){

				$categories = $this->BusinessCategory->find('all', array('conditions' => array('status'=>1, 'parent_id'=>0), 'order' => array('category ASC')));
				
				if( $categories ){
					$result['status'] = true;
					$result['data'] = $categories;
				}else{
					$result['status'] = false;
					$result['data'] = array();
				}

			}else{
				$result['status'] = false;
				$result['data'] = "Invalid Token";
			}

		    // no view to render
		    $this->autoRender = false;
		    $this->response->type('json');
		    $json = json_encode($result,JSON_FORCE_OBJECT);
		    $this->response->body($json);

		}

		public function getGoodsCategory(){

			$getParam = $this->request->query;
			$secureToken = (!empty($getParam['token'])) ? $getParam['token'] : "";

			if( $this->validateToken( $this->appId, $this->appKey, $secureToken ) ){

				$goodCate = $this->GoodsCategory->find('all', array('conditions' => array('GoodsCategory.status'=>1), 'order' => array('category ASC')));
				
				if( $goodCate ){
					$result['status'] = true;
					$result['data'] = $goodCate;
				}else{
					$result['status'] = false;
					$result['data'] = array();
				}

			}else{
				$result['status'] = false;
				$result['data'] = "Invalid Token";
			}

		    // no view to render
		    $this->autoRender = false;
		    $this->response->type('json');
		    $json = json_encode($result,JSON_FORCE_OBJECT);
		    $this->response->body($json);

		}

		public function getDestination(){

			$getParam = $this->request->query;
			$secureToken = (!empty($getParam['token'])) ? $getParam['token'] : "";

			if( $this->validateToken( $this->appId, $this->appKey, $secureToken ) ){

				$destination = $this->Destination->find('all', array('conditions'=>array('Destination.status'=>1), 'order'=>array('Destination.destination ASC')));
				
				if( $destination ){
					$result['status'] = true;
					$result['data'] = $destination;
				}else{
					$result['status'] = false;
					$result['data'] = array();
				}

			}else{
				$result['status'] = false;
				$result['data'] = "Invalid Token";
			}

		    // no view to render
		    $this->autoRender = false;
		    $this->response->type('json');
		    $json = json_encode($result,JSON_FORCE_OBJECT);
		    $this->response->body($json);

		}

		public function getInterestCategory(){

			$getParam = $this->request->query;
			$secureToken = (!empty($getParam['token'])) ? $getParam['token'] : "";

			if( $this->validateToken( $this->appId, $this->appKey, $secureToken ) ){

				$interestCategory = $this->InterestsCategory->find('all', array('conditions'=>array('InterestsCategory.status'=>1), 'order'=>array('InterestsCategory.category ASC')));
				
				if( $interestCategory ){
					$result['status'] = true;
					$result['data'] = $interestCategory;
				}else{
					$result['status'] = false;
					$result['data'] = array();
				}

			}else{
				$result['status'] = false;
				$result['data'] = "Invalid Token";
			}

		    // no view to render
		    $this->autoRender = false;
		    $this->response->type('json');
		    $json = json_encode($result,JSON_FORCE_OBJECT);
		    $this->response->body($json);

		}

		public function getNewDeals( $type = NULL, $p_secureToken = NULL, $p_per_page = 10, $p_page = 1, $merchant_id ){

			$getParam 		= $this->request->query;
			$secureToken 	= (!empty($getParam['token'])) ? $getParam['token'] : $p_secureToken;
			$per_page 		= (!empty($getParam['per_page'])) ? $getParam['per_page'] : $p_per_page;
			$page 			= (!empty($getParam['page'])) ? $getParam['page'] : $p_page ;

			$conditions = array();
			$conditions = array(	'Deal.status' 		=> 1, 
	        						'Deal.isdeleted' 	=> 0, 
	        						'Deal.valid_from <='=> date("Y-m-d H:i:s"), 
	        						'Deal.valid_to >'	=> date("Y-m-d H:i:s"), 
	        						'Business.status'	=> 1
	        					);

			if( $type == 'last-minute' ){
				$conditions['Deal.is_last_minute_deal'] = 1;
			}else if( $type == 'get-by-merchant' ){
				$conditions['Deal.business_id'] = $merchant_id;
			}

			$arrayCondition = array();
			$arrayCondition['conditions'] = $conditions;
			$arrayCondition['fields'] 	= array('Deal.*', 'Business.*', 'City.*');
			$arrayCondition['order'] 	= array('Deal.created' => 'DESC');
			$arrayCondition['limit'] 	= $per_page;
			$arrayCondition['page'] 	= $page;
			$arrayCondition['joins']    = array(
				                                array(
				                                    'table' => 'tb_businesses',
				                                    'alias' => 'Business',
				                                    'type' => 'LEFT',
				                                    'conditions' => array(
				                                        'Deal.business_id = Business.id'
				                                    )
				                                ),
				                                array(
				                                    'table' => 'tb_cities',
				                                    'alias' => 'City',
				                                    'type' => 'LEFT',
				                                    'conditions' => array(
				                                        'Business.city = City.city_code'
				                                    )
		                                		)
		                                	);

			if( $type == 'get-by-merchant' ){
				unset($arrayCondition['limit']);
				unset($arrayCondition['page']);
			}

			if( $this->validateToken( $this->appId, $this->appKey, $secureToken ) ){

		        $this->Deal->recursive = 3;
		        $newDeals = $this->Deal->find('all', $arrayCondition );


				if( !empty($newDeals) ){
					$newDeals = $this->convertDeals($newDeals);
				}
				
				if( $newDeals ){
					$result['status'] 	= true;
					$result['page'] 	= $page;
					$result['per_page'] = (isset($arrayCondition['limit']))?$per_page:NULL;
					$result['data'] 	= $newDeals;
				}else{
					$result['status'] 	= false;
					$result['data'] 	= array();
				}

			}else{
				$result['status'] 	= false;
				$result['data'] 	= "Invalid Token";
			}

		    // no view to render
		    $this->autoRender = false;
		    $this->response->type('json');
		    $json = json_encode($result,JSON_FORCE_OBJECT);
		    $this->response->body($json);
		}

		public function getLastMinuteDeals(){

			$getParam = $this->request->query;
			$secureToken = (!empty($getParam['token'])) ? $getParam['token'] : "";
			$per_page = (!empty($getParam['per_page'])) ? $getParam['per_page'] : "10";
			$page = (!empty($getParam['page'])) ? $getParam['page'] : "1" ;

			$this->getNewDeals('last-minute', $secureToken, $per_page, $page );
		}

		public function getDealPackages( $deal_id = 0 ){

			$getParam 		= $this->request->query;
			$secureToken 	= (!empty($getParam['token'])) ? $getParam['token'] : "";
			$deal_id 		= (isset($getParam['deal_id']) && $getParam['deal_id'] != "" )?$getParam['deal_id']:0;

			if( $deal_id ){

				if( $this->validateToken( $this->appId, $this->appKey, $secureToken ) ){
					$conditions = array( 	'DealItemLink.deal_id' => $deal_id,
											'DealItemLink.status'  => 1 );

					$data = $this->DealItemLink->find('all', array('conditions' => $conditions) );

					if( $data ){
						$returnVal = array();
						foreach($data as $k => $val ){
							// $returnVal[$k]['Items'] = $val['DealItemLink'];

							$returnVal[$k]['Package']['id'] 				= $val['DealItemLink']['id'];
							$returnVal[$k]['Package']['item_id']  		= $val['ItemDetail']['id'];
							$returnVal[$k]['Package']['item_name']  		= $val['ItemDetail']['title'];
							$returnVal[$k]['Package']['item_description']  	= $val['ItemDetail']['description'];
							$returnVal[$k]['Package']['item_image'] 		= $this->path . $val['ItemDetail']['image'];

							$returnVal[$k]['Package']['original_price']  	= $val['DealItemLink']['original_price'];
							$returnVal[$k]['Package']['discounted_price']  	= $val['DealItemLink']['discount'];
							$returnVal[$k]['Package']['available_qty']  	= $val['DealItemLink']['available_qty'];
						}

						$result['status'] 	= true;
						$result['data'] 	= $returnVal;
					}else{
						$result['status'] 	= false;
						$result['data'] 	= array();
					}
				}else{
					$result['status'] 	= false;
					$result['data'] 	= "Invalid Token";
				}

			}else{
				$result['status'] 	= false;
				$result['data'] 	= array();
			}

			// no view to render
		    $this->autoRender = false;
		    $this->response->type('json');
		    $json = json_encode($result,JSON_FORCE_OBJECT);
		    $this->response->body($json);

		}

		public function getDealsByMerchant( $merchant_id = 0 ){

			$getParam 		= $this->request->query;
			$secureToken 	= (!empty($getParam['token'])) ? $getParam['token'] : "";
			$per_page 		= (!empty($getParam['per_page'])) ? $getParam['per_page'] : "10";
			$page 			= (!empty($getParam['page'])) ? $getParam['page'] : "1" ;
			

			if( isset($getParam['merchant_id']) && $getParam['merchant_id'] != "" ){
				$merchant_id 	= $getParam['merchant_id'];
				$this->getNewDeals('get-by-merchant', $secureToken, $per_page, $page, $merchant_id );
			}else{

				$result['status'] 	= false;
				$result['data'] 	= array();
				// no view to render
			    $this->autoRender = false;
			    $this->response->type('json');
			    $json = json_encode($result,JSON_FORCE_OBJECT);
			    $this->response->body($json);
			    return false;
			}

		}

		public function getNearByDeals(){

			$getParam 		= $this->request->query;
			$secureToken 	= (!empty($getParam['token'])) ? $getParam['token'] : "";
			$per_page 		= (!empty($getParam['per_page'])) ? $getParam['per_page'] : "10";
			$page 			= (!empty($getParam['page'])) ? $getParam['page'] : "1" ;
			$latitude 		= (!empty($getParam['lat']) && $getParam['lat'] != "" ) ? $getParam['lat'] : "11.556918" ;
			$longitude 		= (!empty($getParam['lng']) && $getParam['lat'] != "" ) ? $getParam['lng'] : "104.928238" ;

			$conditions = array();
			$conditions = array(	'Deal.status' 		=> 1, 
	        						'Deal.isdeleted' 	=> 0, 
	        						'Deal.valid_from <='=> date("Y-m-d H:i:s"), 
	        						'Deal.valid_to >'	=> date("Y-m-d H:i:s"), 
	        						'Business.status'	=> 1
	        					);

			if( $this->validateToken( $this->appId, $this->appKey, $secureToken ) ){

	 			$distance_field = '( 6371 * acos ( cos ( radians(' . $latitude . ') ) * cos( radians( Deal.latitude ) ) * 
								 cos( radians( Deal.longitude ) - radians(' . $longitude . ') ) + sin ( radians(' . $latitude . ') ) * 
								 sin( radians( Deal.latitude ) ) ) ) AS distance';

		        $this->Deal->recursive = 3;
		        $dealInfo = $this->Deal->find('all', array(
		                            'joins' => array(
		                                array(
		                                    'table' => 'tb_businesses',
		                                    'alias' => 'Business',
		                                    'type' => 'LEFT',
		                                    'conditions' => array(
		                                        'Deal.business_id = Business.id'
		                                    )
		                                ),
		                                array(
		                                    'table' => 'tb_cities',
		                                    'alias' => 'City',
		                                    'type' => 'LEFT',
		                                    'conditions' => array(
		                                        'Business.city = City.city_code'
		                                    )
		                                )
		                            ),
		                            'conditions' => $conditions,
		                            'fields' => array(	'Deal.*',  
		                            					'Business.*',
		                            					'City.*',
		                            					$distance_field
		                            					 ),
		                            'order' => array('distance' => 'ASC', 'Deal.created' => 'DESC'),
		                            'limit' => $per_page,
		                            'page' => $page
		                        ));

				if( !empty($dealInfo) ){
					$dealInfo = $this->convertDeals($dealInfo);
				}
				
				if( $dealInfo ){
					$result['status'] = true;
					$result['page'] = $page;
					$result['per_page'] = $per_page;
					$result['data'] = $dealInfo;
				}else{
					$result['status'] = false;
					$result['data'] = array();
				}

			}else{
				$result['status'] = false;
				$result['data'] = "Invalid Token";
			}

			$result['token'] = $secureToken;

		    // no view to render
		    $this->autoRender = false;
		    $this->response->type('json');
		    $json = json_encode($result,JSON_FORCE_OBJECT);
		    $this->response->body($json);

		}

		public function getDealOfTheDay(){

			$getParam = $this->request->query;
			$secureToken = (!empty($getParam['token'])) ? $getParam['token'] : "";
			$per_page = (!empty($getParam['per_page'])) ? $getParam['per_page'] : "10";
			$page = (!empty($getParam['page'])) ? $getParam['page'] : "1";

			$conditions = array();

			if( $this->validateToken( $this->appId, $this->appKey, $secureToken ) ){

				$conds 	= array('DealOfTheDay.available_date' => date('Y-m-d'));

	    		$dodConditions['conditions']	= $conds;
	    		$dodConditions['order'] 		= array('DealOfTheDay.created' => 'DESC', 'DealDetail.created' => 'DESC' );
	    		$dodConditions['limit']			= $this->limit;

	    		// $this->DealOfTheDay->recursive = -1;
	    		$dod = $this->DealOfTheDay->find('all', $dodConditions);

	    		// Prepare Data Deals
	            $deal_ids = array();
	            if( $dod ){
	            	foreach( $dod as $k => $val ){
	            		$deal_ids[] = $val['DealOfTheDay']['deal_id'];
	            	}

	            	$conditionFilter['Deal.id'] 		= $deal_ids;

			        $this->Deal->recursive = 3;
			        $newDeals = $this->Deal->find('all', array(
			                            'joins' => array(
			                                array(
			                                    'table' => 'tb_businesses',
			                                    'alias' => 'Business',
			                                    'type' => 'LEFT',
			                                    'conditions' => array(
			                                        'Deal.business_id = Business.id'
			                                    )
			                                ),
			                                array(
			                                    'table' => 'tb_cities',
			                                    'alias' => 'City',
			                                    'type' => 'LEFT',
			                                    'conditions' => array(
			                                        'Business.city = City.city_code'
			                                    )
			                                )
			                            ),
			                            'conditions' => array(	'Deal.status' 		=> 1, 
			                            						'Deal.isdeleted' 	=> 0, 
					                    						'Deal.valid_from <='=> date("Y-m-d H:i:s"), 
					                    						'Deal.valid_to >'	=> date("Y-m-d H:i:s"), 
					                    						'Business.status'	=> 1,
					                    						'Deal.id'			=> $deal_ids
					                    					),
			                            'fields' => array('Deal.*', 'Business.*', 'City.*'),
			                            'order' => array('Deal.created DESC'),
			                            'limit' => $per_page,
			                            'page' => $page
			                        ));

					if( !empty($newDeals) ){
						$newDeals = $this->convertDeals($newDeals);
					}
					
					if( $newDeals ){
						$result['status'] = true;
						$result['page'] = $page;
						$result['per_page'] = $per_page;
						$result['data'] = $newDeals;
					}else{
						$result['status'] = false;
						$result['data'] = array();
					}

	            }else{
					$result['status'] = false;
					$result['data'] = array();
				}
	           

			}else{
				$result['status'] = false;
				$result['data'] = "Invalid Token";
			}

			$result['type'] = $type;

		    // no view to render
		    $this->autoRender = false;
		    $this->response->type('json');
		    $json = json_encode($result,JSON_FORCE_OBJECT);
		    $this->response->body($json);

		}

		public function getDeals(){
			
			$getParam = $this->request->query;
			$secureToken = (!empty($getParam['token'])) ? $getParam['token'] : "";
			$per_page = (!empty($getParam['per_page'])) ? $getParam['per_page'] : "20";
			$page = (!empty($getParam['page'])) ? $getParam['page'] : "1";
			$order_by = (!empty($getParam['order_by'])) ? $getParam['order_by'] : "ASC";
			if( $order_by != 'DESC' && $order_by != 'ASC' ){
				$order_by = 'ASC';
			}
			$sort = (!empty($getParam['sort'])) ? $getParam['sort'] : "publish_date";
			if( isset($this->sortDeals[$sort]) ){
				$order = $this->sortDeals[$sort] . " " . $order_by;
			}else{
				$order = $this->sortDeals['publish_date'] . " " . $order_by;
			}

			$dealID = (!empty($getParam['deal_id'])) ? $getParam['deal_id'] : "";
			$categoryID = (!empty($getParam['category_id'])) ? $getParam['category_id'] : "";
			$cityCode = (!empty($getParam['city_code'])) ? $getParam['city_code'] : "";
			$interestID = (!empty($getParam['interest_id'])) ? $getParam['interest_id'] : "";
			$serviceID = (!empty($getParam['service_id'])) ? $getParam['service_id'] : "";
			$destinationID = (!empty($getParam['destination_id'])) ? $getParam['destination_id'] : "";
			$businessID = (!empty($getParam['business_id'])) ? $getParam['business_id'] : "";
			$businessMainCategoryID = (!empty($getParam['business_main_category_id'])) ? $getParam['business_main_category_id'] : "";
			$businessSubCategoryID = (!empty($getParam['business_sub_category_id'])) ? $getParam['business_sub_category_id'] : "";

			if( $this->validateToken( $this->appId, $this->appKey, $secureToken ) ){

		        $this->Deal->recursive = -1;
	            $arrCondition[0] = array('Deal.status' => 1, 'Deal.valid_to >' => date("Y-m-d H:i:s"), 'Business.status'=>1);

	            // search by categories
		        if( !empty($categoryID) ){
		        	$jsonCateID = json_decode($categoryID, true);
		        	if( !empty($jsonCateID) && is_array($jsonCateID) ){
		                foreach ($jsonCateID as $key) {
		                    $cateInfo = $this->GoodsCategory->findById($key);
		                    if(!empty($cateInfo)){
		                        $cateName = $cateInfo['GoodsCategory']['category'];    
		                    }else{
		                        $cateName = "XXX-XXX";
		                    }
		                    $arrCondition[1]["OR"][] = array('Deal.goods_category LIKE' => "%$cateName%");
		                }
		        	}
	            }

	            // search by cities
	            if( !empty($cityCode) ){
	            	$jsonCitCode = json_decode($cityCode, true);
	            	if( !empty($jsonCitCode) && is_array($jsonCitCode) ){
		                foreach ($jsonCitCode as $key) {
		                    $arrCondition[2]["OR"][] = array('Business.city' => $key);
		                }
	            	}
	            }

	            // search by interest
	            if( !empty($interestID) ){
	            	$jsonIntID = json_decode($interestID, true);
	            	if( !empty($jsonIntID) && is_array($jsonIntID) ){
		                foreach ($jsonIntID as $key) {
		                    $interestCateInfo = $this->InterestsCategory->findById($key);
		                    if( !empty($interestCateInfo) ){
			                    $interestCate = $interestCateInfo['InterestsCategory']['category'];
		                    }else{
			                    $interestCate = "XXX-XXX";
		                    }
			                $arrCondition[3]["OR"][] = array('Deal.interest_category LIKE' => "%$interestCate%");
		                }
	            	}
	            }

	            // search by services
	            if( !empty($serviceID) ){
	            	$jsonSerID = json_decode($serviceID, true);
	            	if( !empty($jsonSerID) && is_array($jsonSerID) ){
		                foreach ($jsonSerID as $key) {
		                    $serviceCateInfo = $this->ServicesCategory->findById($key);
		                    if( !empty($serviceCateInfo) ){
			                    $serviceCate = $serviceCateInfo['ServicesCategory']['category'];
		                    }else{
			                    $serviceCate = "XXX-XXX";
		                    }
			                $arrCondition[4]["OR"][] = array('Deal.services_category LIKE' => "%$serviceCate%");
		                }
	            	}
	            }

	            // search by deal id
	            if( !empty($dealID) ){
	            	$arrCondition[5] = array('Deal.id' => $dealID);
	            }

	            // search by business id
	            if( !empty($businessID) ){
	            	$arrCondition[6] = array('Deal.business_id' => $businessID);
	            }

	            // search by business main categories
	            if( !empty($businessMainCategoryID) ){
	            	$jsonBusinessMainCatID = json_decode($businessMainCategoryID, true);
	            	if( !empty($jsonBusinessMainCatID) && is_array($jsonBusinessMainCatID) ){
		                foreach ($jsonBusinessMainCatID as $key) {
		                	$arrCondition[7]["OR"][] = array('Business.business_main_category' => $key);
		                }
	            	}
	            }

	            // search by business sub categories
	            if( !empty($businessSubCategoryID) ){
	            	$jsonBusinessSubCatID = json_decode($businessSubCategoryID, true);
	            	if( !empty($jsonBusinessSubCatID) && is_array($jsonBusinessSubCatID) ){
		                foreach ($jsonBusinessSubCatID as $key) {
		                	$arrCondition[8]["OR"][] = array('Business.business_sub_category' => $key);
		                }
	            	}
	            }

	            // search by destination
	            if( !empty($destinationID) ){
	            	$jsonDesID = json_decode($destinationID, true);
	            	if( !empty($jsonDesID) && is_array($jsonDesID) ){
	            		$arrCondition[9]["OR"][] = array('Deal.travel_destination LIKE' =>'%"All"%');
		                foreach ($jsonDesID as $key) {
		                    $arrCondition[9]["OR"][] = array('Deal.travel_destination LIKE' =>"%$key%");
		                }
	            	}
	            }

	            $conditions = $arrCondition;

	            // print_r($conditions);
	            // exit();

	            $deals = $this->Deal->find('all', array(
	                        'joins' => array(
	                            array(
	                                'table' => 'tb_businesses',
	                                'alias' => 'Business',
	                                'type' => 'LEFT',
	                                'conditions' => array(
	                                    'Deal.business_id = Business.id'
	                                )
	                            ),
	                            array(
	                                'table' => 'tb_cities',
	                                'alias' => 'City',
	                                'type' => 'LEFT',
	                                'conditions' => array(
	                                    'Business.city = City.city_code'
	                                )
	                            )
	                        ),
	                        'conditions' => $conditions,
	                        'fields' => array('Deal.*', 'Business.*', 'City.*'),
	                        'order' => array($order),
	                        'limit' => $per_page,
	                        'page' => $page
	                    ));

				if( !empty($deals) ){
					$deals = $this->convertDeals($deals);
				}
				
				if( $deals ){
					$result['status'] = true;
					$result['page'] = $page;
					$result['per_page'] = $per_page;
					$result['data'] = $deals;
				}else{
					$result['status'] = false;
					$result['data'] = array();
				}

			}else{
				$result['status'] = false;
				$result['data'] = "Invalid Token";
			}

		    // no view to render
		    $this->autoRender = false;
		    $this->response->type('json');
		    $json = json_encode($result,JSON_FORCE_OBJECT);
		    $this->response->body($json);

		}

		public function getBusinesses(){

			$getParam = $this->request->query;
			$secureToken = (!empty($getParam['token'])) ? $getParam['token'] : "";
			$per_page = (!empty($getParam['per_page'])) ? $getParam['per_page'] : "20";
			$page = (!empty($getParam['page'])) ? $getParam['page'] : "1";
			$order_by = (!empty($getParam['order_by'])) ? $getParam['order_by'] : "ASC";
			if( $order_by != 'DESC' && $order_by != 'ASC' ){
				$order_by = 'ASC';
			}
			$sort = (!empty($getParam['sort'])) ? $getParam['sort'] : "business_name";
			if( isset($this->sortBusinesses[$sort]) ){
				$order = $this->sortBusinesses[$sort] . " " . $order_by;
			}else{
				$order = $this->sortBusinesses['business_name'] . " " . $order_by;
			}
			$locationName = (!empty($getParam['location_name'])) ? $getParam['location_name'] : "";
			$cityCode = (!empty($getParam['city_code'])) ? $getParam['city_code'] : "";
			$provinceCode = (!empty($getParam['province_code'])) ? $getParam['province_code'] : "";
			$businessID = (!empty($getParam['business_id'])) ? $getParam['business_id'] : "";
			$businessMainCategoryID = (!empty($getParam['business_main_category_id'])) ? $getParam['business_main_category_id'] : "";
			$businessSubCategoryID = (!empty($getParam['business_sub_category_id'])) ? $getParam['business_sub_category_id'] : "";
			$businessName = (!empty($getParam['business_name'])) ? $getParam['business_name'] : "";

			if( $this->validateToken( $this->appId, $this->appKey, $secureToken ) ){

		        // $this->Deal->recursive = -1;
	            $arrCondition[0] = array('Business.status' => 1);

	            // search by business main category
		        if( !empty($businessMainCategoryID) ){
	            	$jsonBusinessMainCatID = json_decode($businessMainCategoryID, true);
	            	if( !empty($jsonBusinessMainCatID) && is_array($jsonBusinessMainCatID) ){
		                foreach ($jsonBusinessMainCatID as $key) {
		                	$arrCondition[1]["OR"][] = array('Business.business_main_category' => $key);
		                }
	            	}
	            }

	            // search by business sub category
	            if( !empty($businessSubCategoryID) ){
	            	$jsonBusinessSubCatID = json_decode($businessSubCategoryID, true);
	            	if( !empty($jsonBusinessSubCatID) && is_array($jsonBusinessSubCatID) ){
		                foreach ($jsonBusinessSubCatID as $key) {
		                	$arrCondition[2]["OR"][] = array('Business.business_sub_category' => $key);
		                }
	            	}
	            }

	            // search by cities
	            if( !empty($cityCode) ){
	            	$jsonCitID = json_decode($cityCode, true);
	            	if( !empty($jsonCitID) && is_array($jsonCitID) ){
		                foreach ($jsonCitID as $key) {
		                    $arrCondition[3]["OR"][] = array('Business.city' => $key);
		                }
	            	}
	            }

	            // search by business name
	            if( !empty($businessName) ){
	                $businessName = trim($businessName);
	                $arrName = explode(" ", $businessName);
	                foreach ($arrName as $key => $value) {
	                    if(!empty($value)){
	                        $arrCondition[4]["OR"][] = array('Business.business_name LIKE' => "%$value%");        
	                    }
	                }
	                
	            }

	            // search by business id
	            if( !empty($businessID) ){
	            	$arrCondition[5] = array('Business.id' => $businessID);
	            }

	            // search by location name
	            if( !empty($locationName) ){
	            	$jsonLocName = json_decode($locationName, true);
	            	if( !empty($jsonLocName) && is_array($jsonLocName) ){
		                foreach ($jsonLocName as $key) {
		                    $arrCondition[6]["OR"][] = array('Business.location LIKE' => "%$key%");
		                }
	            	}
	            }

	            // search by provinces
	            if( !empty($provinceCode) ){
	            	$jsonProCode = json_decode($provinceCode, true);
	            	if( !empty($jsonProCode) && is_array($jsonProCode) ){
		                foreach ($jsonProCode as $key) {
		                    $arrCondition[7]["OR"][] = array('Business.province' => $key);
		                }
	            	}
	            }

	            $conditions = $arrCondition;

	            // $this->Business->recursive = 2;
	            $businesses = $this->Business->find('all', array(
	                            'joins' => array(
		                                array(
		                                    'table' => 'tb_provinces',
		                                    'alias' => 'Provinces',
		                                    'type' => 'LEFT',
		                                    'conditions' => array(
		                                        'Business.province = Provinces.province_code'
		                                    )
		                                ),
		                                array(
		                                    'table' => 'tb_cities',
		                                    'alias' => 'City',
		                                    'type' => 'LEFT',
		                                    'conditions' => array(
		                                        'Business.city = City.city_code'
		                                    )
		                                )
	                            	),
	                            'conditions'=> $conditions, 
	                            'fields' => array('Business.*', 'Provinces.*', 'City.*', 'SangkatInfo.*'),
	                            'order'=>array($order),
	                            'limit'=>$per_page,
	                            'page' => $page));


				if( !empty($businesses) ){
					$businesses = $this->convertBusinesses($businesses);
				}
				
				if( $businesses ){
					$result['status'] = true;
					$result['page'] = $page;
					$result['per_page'] = $per_page;
					$result['data'] = $businesses;
				}else{
					$result['status'] = false;
					$result['data'] = array();
				}

			}else{
				$result['status'] = false;
				$result['data'] = "Invalid Token";
			}

		    // no view to render
		    $this->autoRender = false;
		    $this->response->type('json');
		    $json = json_encode($result,JSON_FORCE_OBJECT);
		    $this->response->body($json);

		}

		public function signup(){

			$msg_error = array();
			$getParam = $this->request->query;
			$secureToken = (!empty($getParam['token'])) ? $getParam['token'] : "";
			$getData = (!empty($getParam['data'])) ? $getParam['data'] : "";

			if( $this->validateToken( $this->appId, $this->appKey, $secureToken ) ){

				$json_data = json_decode($getData, true);

				if(!empty($json_data) && is_array($json_data)){

					$name 			= $data['Buyer']['full_name'] 		= isset($json_data['full_name']) ? $json_data['full_name'] : NULL;
			 		$gender 		= $data['Buyer']['gender'] 			= isset($json_data['gender']) ? $json_data['gender'] : NULL;
			 		$dob 			= $data['Buyer']['dob'] 			= isset($json_data['dob']) ? date('Y-m-d', strtotime($json_data['dob'])) : NULL;
			 		$province_code 	= $data['Buyer']['province_code'] 	= isset($json_data['province_code']) ? $json_data['province_code'] : NULL;
			 		$city_code 		= $data['Buyer']['city_code'] 		= isset($json_data['city_code']) ? $json_data['city_code'] : NULL;
			 		$location 		= $data['Buyer']['location'] 		= isset($json_data['location']) ? $json_data['location'] : NULL;
			 		$location_id 	= $data['Buyer']['location_id'] 	= isset($json_data['location_id']) ? $json_data['location_id'] : NULL;
			 		$sangkat_id 	= $data['Buyer']['sangkat_id'] 		= isset($json_data['sangkat_id']) ? $json_data['sangkat_id'] : NULL;
			 		$street 		= $data['Buyer']['street'] 			= isset($json_data['street']) ? $json_data['street'] : NULL;
			 		$phone1 		= $data['Buyer']['phone1'] 			= isset($json_data['phone1']) ? $json_data['phone1'] : NULL;
			 		$phone2 		= $data['Buyer']['phone2'] 			= isset($json_data['phone2']) ? $json_data['phone2'] : NULL;
			 		$email 			= $data['Buyer']['email'] 			= isset($json_data['email']) ? $json_data['email'] : NULL;
			 		$password 		= $data['Buyer']['password'] 		= isset($json_data['password']) ? $json_data['password'] : NULL;

			 		$cnf_password  = isset($json_data['confirm_password'])?$json_data['confirm_password']:NULL;

					if( empty($name) ){
						array_push($msg_error, 'Please enter your name !');
					}
					if( empty($gender) ){
						array_push($msg_error, "Please enter your gender !");
					}
					if( empty($dob) ){
						array_push($msg_error, "Invalid date of birth !");
					}

					if( empty($city_code) ){
						array_push($msg_error, "Please select province !");
					}
					if( empty($location) ){
						array_push($msg_error, "Please select location !");
					}

					if( empty($phone1) ){
						array_push($msg_error, "Please enter your phone number !");
					}

					if( empty($email) ){
						array_push($msg_error, "Please enter your email address !");
					}else{
						if (!filter_var($email , FILTER_VALIDATE_EMAIL)) {
		                    array_push($msg_error, "Invalid Email Address !");
		                }else{
		                	$checkEmail = $this->Buyer->findByEmail($email);
		                	if( !empty($checkEmail) ){
		                		array_push($msg_error, "Duplicate Email Address ! Please use another email address.");
		                	}
		                }
					}
					if( strlen($password) < 6 ){
						array_push($msg_error, "Please enter password and must be at least 6 characters !");
					}else{

						if( $cnf_password != $password ){
							array_push($msg_error, "Password and Confirm Password does not match !" );
						}
					}

					if( count($msg_error) > 0 ){
						foreach( $msg_error as $k => $val ){
							$result['status'] = false;
							$result['message'][$k] = $val;
						}
						$result['data'] = $data;
					    // no view to render
					    $this->autoRender = false;
					    $this->response->type('json');
					    $json = json_encode($result,JSON_FORCE_OBJECT);
					    $this->response->body($json);

					}else{

						// save
						$token = $email . $password ;
				        for ($i=0; $i < 10 ; $i++) { 
				            $token = sha1($token);
				        }
				        $data['Buyer']['status'] 	= 0;
				        $data['Buyer']['token'] 	= $token;
						if( $this->Buyer->save($data) ){

							$configuration = $this->MailConfigure->find('first', array( 'conditions' => array('business_id' => NULL) ));
			    			$signature = $configuration['MailConfigure']['signature'];

			    			// Send Verification Mail
			                $receiver   = $email;
			                $subject    = "Sign Up Verification";
			                $content    = "";
			                $url        = Router::url("signUpVerification?token=" . $token, true );
			                $content    .= "<html><body>";
			                $content    .= "<p>Dear " . $name. ", </p>";
			                $content    .= "<p>Click on link below to verify your sign up:</p>";
			                $content    .= "<a href='" . $url . "'>" . $url . "</a><br>";
			                $content    .= "<p>Thanks for helping us maintain the security of your account.</p><br>";
			                $content    .= $signature;
			                $content    .= "</body></html>";

			                $sendMail =  $this->sendMail($receiver, $subject, $content);
			                // $sendMail = true;

							$result['status'] = true;
							$result['data'] = "Successfully signed up.";

						}else{
							$result['status'] = false;
							$result['data'] = "Failed to sign up.";
						}
					}
				}else{
					$result['status'] = false;
					$result['data'] = "Posted empty data!";
				}
			}else{
				$result['status'] = false;
				$result['data'] = "Invalid Token";
			}

			// exit();

		    // no view to render
		    $this->autoRender = false;
		    $this->response->type('json');
		    $json = json_encode($result,JSON_FORCE_OBJECT);
		    $this->response->body($json);

		}

		public function signUpVerification(){

			$getParam = $this->request->query;
			$secureToken = (!empty($getParam['token'])) ? $getParam['token'] : "";

			if( !empty($secureToken) ){

				$info = $this->Buyer->findByToken($secureToken);

				if( $info ){

					$id = $info['Buyer']['id'];
					$data['Buyer']['id'] = $id;
					$data['Buyer']['status'] = 1;
					$data['Buyer']['token'] = NULL;
					$data['Buyer']['activated_date'] = date('Y-m-d H:i:s');

					// Get Bonus Commission
					$conds = array(	'conditions' => array('status' => 1, 'type' => $this->bonus_id),
									'fields' => array('commission')
								);
					$bonus = $this->Commission->find('first', $conds);

					if( $bonus && !empty($bonus) ){

						// Save bonus to transactions
						$transactionData['BuyerTransaction']['code'] 		= "";
						$transactionData['BuyerTransaction']['buyer_id'] 	= $id;
						$transactionData['BuyerTransaction']['type'] 		= $this->type_in;
						$transactionData['BuyerTransaction']['description']	= 'Sign Up Bonus';
						$transactionData['BuyerTransaction']['amount']		= $bonus['Commission']['commission'];
						$transactionData['BuyerTransaction']['created']		= date("Y-m-d H:i:s");
						$transactionData['BuyerTransaction']['modified']	= date("Y-m-d H:i:s");

						$this->BuyerTransaction->save($transactionData);

					}

					$update = $this->Buyer->save($data);
					
					if( $update ){
						$this->Session->setFlash(__( 'Your account is now activated.')
	                                                    .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
	                                                'default',
	                                                array('class'=>'alert alert-dismissable alert-success align-center '));
						return $this->redirect('/shops/signup/verified');
					}
				}else{
					$this->Session->setFlash(__( 'Invalid request.')
	                                                .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
	                                            'default',
	                                            array('class'=>'alert alert-dismissable alert-danger align-center '));
					return $this->redirect('/shops/signup/verified');

				}
			}
			$this->Session->setFlash(__( 'Invalid request.')
	                                        .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
	                                    'default',
	                                    array('class'=>'alert alert-dismissable alert-danger align-center '));
			return $this->redirect('/shops/signup/verified');

		}
		
		public function signin(){

			$getParam = $this->request->query;

			$secureToken = (!empty($getParam['token'])) ? $getParam['token'] : "";
			$getEmail = (!empty($getParam['email'])) ? $getParam['email'] : "";
			$getPassword = (!empty($getParam['password'])) ? $getParam['password'] : "";

			if( $this->validateToken( $this->appId, $this->appKey, $secureToken ) ){

				if( !empty($getEmail) && !empty($getPassword) ){

					$passwordHasher = new SimplePasswordHasher(array('hashType'=>'sha256'));
			  		$hashedPassword = $passwordHasher->hash($getPassword);

			     	$getPassword = $hashedPassword;

			     	$conds = array( 'conditions' => array('Buyer.email' => $getEmail, 'Buyer.password' => $getPassword));

			     	$conds['joins']    = array(
			                                array(
			                                    'table' => 'tb_cities',
			                                    'alias' => 'City',
			                                    'type' => 'LEFT',
			                                    'conditions' => array(
			                                        'Buyer.city_code = City.city_code'
			                                    )
	                                		)
	                                	);
			     	$conds['fields'] = array( 'Buyer.*', 'SangkatInfo.*', 'City.*' );

			     	// $this->Buyer->recursive = 2;
			     	$getInfo = $this->Buyer->find('first', $conds);

			     	if( $getInfo && !empty($getInfo) ){

			     		if( $getInfo['Buyer']['status'] == 1 ){

			     			$preparedData = array();

			     			$userData = array();
			     			$userData['id'] 			= $getInfo['Buyer']['id'];
			     			$userData['code'] 			= strtoupper($getInfo['Buyer']['buyer_code']);
			     			$userData['name'] 			= $getInfo['Buyer']['full_name'];
			     			$userData['gender'] 		= $getInfo['Buyer']['gender'];
			     			$userData['dob'] 			= $getInfo['Buyer']['dob'];
			     			$userData['phone1'] 		= $getInfo['Buyer']['phone1'];
			     			$userData['phone2'] 		= $getInfo['Buyer']['phone2'];
			     			$userData['email'] 			= $getInfo['Buyer']['email'];
			     			$userData['province_code'] 	= $getInfo['Buyer']['city_code'];
			     			$userData['city_code'] 		= $getInfo['Buyer']['city_code'];
			     			$userData['city_name'] 		= $getInfo['City']['city_name'];
			     			$userData['location_id'] 	= $getInfo['Buyer']['location_id'];
			     			$userData['location_name'] 	= $getInfo['Buyer']['location'];
			     			$userData['sangkat_id'] 	= $getInfo['Buyer']['sangkat_id'];
			     			$userData['sangkat_name'] 	= $getInfo['SangkatInfo']['name'];
			     			$userData['street'] 		= $getInfo['Buyer']['street'];
			     			$userData['amount_balance'] = $getInfo['Buyer']['amount_balance'];

			     			$preparedData['User'] = $userData;
			     			
							$result['status'] = true;
							$result['data'] = $preparedData;
			     		}else{
							$result['status'] = false;
							$result['data'] = "This account is deactivated !";
			     		}

			     	}else{
						$result['status'] = false;
						$result['data'] = "Login failed. Invalid Email or Password !";
			     	}

				}else{

					$result['status'] = false;
					$result['data'] = "Please enter Email and Password";

				}

			}else{

				$result['status'] = false;
				$result['data'] = "Invalid Token";

			}

		    // no view to render
		    $this->autoRender = false;
		    $this->response->type('json');
		    $json = json_encode($result,JSON_FORCE_OBJECT);
		    $this->response->body($json);

		}

		public function resetMerchantPassword(){

			$getParam = $this->request->query;
			$secureToken = (!empty($getParam['token'])) ? $getParam['token'] : "";
			$getEmail 	 = (!empty($getParam['email'])) ? $getParam['email'] : "";

			if( $this->validateToken( $this->appId, $this->appKey, $secureToken ) ){

				$conds['conditions'] = array( 'User.email' => $getEmail );
				$getInfo = $this->User->find('first', $conds);

				if( $getEmail == "" ){
					$result['status'] = false;
					$result['data'] = "Please enter email address.";
				}else if(!filter_var($getEmail , FILTER_VALIDATE_EMAIL)){
					$result['status'] = false;
					$result['data'] = "Invalid email address.";
				}else{

					if( $getInfo ){
						if( $getInfo['User']['status'] == 1 ){
							$token = $this->randomPassword() . $getEmail ;
							$n = date('s');
					        for( $i =1; $i <= $n; $i++ ){
					          $token = sha1($token);
					        }

					        $activate_link = Router::url("/member/MemberUsers/activate/"  . $token , true);

					        $data['User']['id'] 	= $getInfo['User']['id'];
					        $data['User']['token'] 	= $token;

					        $subject = "Password Reset Verification";

					        $content = "<h4>Dear " . $getInfo['User']['last_name'] . " " . $getInfo['User']['first_name'] . ",</h4><p>Please click on verification link below to complete your password reset.</p>";
					        $content .= "<p><a href='" . $activate_link . "'>" . $activate_link . "</a></p><br/>";

					        if( $this->sendMail($getEmail, $subject, $content) ){
					        	if( $this->User->save($data) ){
									$result['status'] 	= true;
									$result['data'] 	= "Reset link has been sent to your email: " . $getEmail . ". Check you email inbox and click on the reset link."  ;	
					        	}
					        }else{
								$result['status'] 	= false;
								$result['data'] 	= "Sorry, something went wrong. Please try again!";
					        }

						}else{
							$result['status'] 	= false;
							$result['data'] 	= "Sorry, this account is deactivated.";	
						}
					}else{
						$result['status'] 	= false;
						$result['data'] 	= "Sorry, email address not found.";
					}
				}

			}else{

				$result['status'] = false;
				$result['data'] = "Invalid Token !";

			}

		    // no view to render
		    $this->autoRender = false;
		    $this->response->type('json');
		    $json = json_encode($result,JSON_FORCE_OBJECT);
		    $this->response->body($json);

		}

		public function sendMail( $recipients = NULL, $subject = NULL , $content = NULL ) {

	        return parent::sendMail( $recipients, $subject, $content );

	    }


		public function merchantLogin(){

			$getParam = $this->request->query;

			$secureToken = (!empty($getParam['token'])) ? $getParam['token'] : "";
			$getEmail = (!empty($getParam['email'])) ? $getParam['email'] : "";
			$getPassword = (!empty($getParam['password'])) ? $getParam['password'] : "";

			if( $this->validateToken( $this->appId, $this->appKey, $secureToken ) ){

				if( !empty($getEmail) && !empty($getPassword) ){

					$passwordHasher = new SimplePasswordHasher(array('hashType'=>'sha256'));

			  		$hashedPassword = $passwordHasher->hash($getPassword);

			     	$getPassword = $hashedPassword;

			     	$conds = array( 'conditions' => array(	'User.status' 			=> 1, 
			     											'User.business_id !=' 	=> NULL, 
			     											'User.email' 			=> $getEmail, 
			     											'User.password' 		=> $getPassword,
			     											'BusinessInfo.status' 	=> 1));

			     	$conds['joins']    = array(
			                                array(
			                                    'table' => 'tb_businesses',
			                                    'alias' => 'BusinessInfo',
			                                    'type' => 'LEFT',
			                                    'conditions' => array(
			                                        'User.business_id = BusinessInfo.id'
			                                    )
	                                		)
	                                	);

			     	$conds['fields'] = array( 	'User.id',
			     								'User.first_name',
			     								'User.last_name',
			     								'User.business_id',
			     								'BusinessInfo.id',
			     								'BusinessInfo.business_name'
			     							 );

			     	$this->User->recursive = -1;
			     	$getInfo = $this->User->find('first', $conds);

			     	if( $getInfo && !empty($getInfo) ){
			     		$tmp = array();

			     		unset($getInfo['User']['business_id']);

			     		$tmp['User'] 			= $getInfo['User'];
			     		$tmp['Merchant']['id'] 	= $getInfo['BusinessInfo']['id'];
			     		$tmp['Merchant']['name']= $getInfo['BusinessInfo']['business_name'];

		     			$result['status'] = true;
						$result['data'] = $tmp;

			     	}else{
						$result['status'] = false;
						$result['data'] = "Login failed. Invalid Email or Password !";
			     	}

				}else{

					$result['status'] = false;
					$result['data'] = "Please enter Email and Password !";

				}

			}else{

				$result['status'] = false;
				$result['data'] = "Invalid Token !";

			}

		    // no view to render
		    $this->autoRender = false;
		    $this->response->type('json');
		    $json = json_encode($result,JSON_FORCE_OBJECT);
		    $this->response->body($json);

		}

		public function randomPassword() {

		    $alphabet = "0123456789abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
		    $pass = array(); 
		    $alphaLength = strlen($alphabet) - 1; 

		    for ($i = 0; $i < 10; $i++) {
		        $n = rand(0, $alphaLength);
		        $pass[] = $alphabet[$n];
		    }
		    return implode($pass);
		}

		public function getVerifyCouponByCode(){

			$getParam 		= $this->request->query;
			$secureToken 	= (!empty($getParam['token'])) ? $getParam['token'] : "";
			$couponCode 	= (!empty($getParam['code'])) ? $getParam['code'] : "";
			$merchant_id    = (!empty($getParam['merchant_id'])) ? $getParam['merchant_id'] : "";

			if( $this->validateToken( $this->appId, $this->appKey, $secureToken ) ){

				if( $couponCode && $merchant_id ){
					$conds = array();
					$conds['conditions'] = array( 	'BuyerTransaction.code' 		=> $couponCode,
	                                          		'BuyerTransaction.business_id' 	=> $merchant_id);

					$conds['joins']    = array(
			                                array(
			                                    'table' => 'tb_businesses',
			                                    'alias' => 'BusinessDetails',
			                                    'type' => 'LEFT',
			                                    'conditions' => array(
			                                        'BuyerTransaction.business_id = BusinessDetails.id'
			                                    )
	                                		),

			                                array(
			                                    'table' => 'tb_cities',
			                                    'alias' => 'CityInfo',
			                                    'type' => 'LEFT',
			                                    'conditions' => array(
			                                        'BusinessDetails.city = CityInfo.city_code'
			                                    )
	                                		)
	                                	);

					$conds['fields'] = array( 'BuyerTransaction.*', 'BusinessDetails.*', 'CityInfo.*' );

					$this->BuyerTransaction->recursive = 2;
	            	$getInfo = $this->BuyerTransaction->find('first', $conds);

	            	if( $getInfo ){

	            		$sangkatInfo = $getInfo['BusinessDetail']['SangkatInfo'];
	            		$cityInfo 	 = $getInfo['CityInfo'];

	            		unset($getInfo['BusinessDetail']);
	            		unset($getInfo['BusinessDetails']);
	            		unset($getInfo['CityInfo']);

	            		$bizDetail = $getInfo['DealInfo']['BusinessInfo'];

	            		$deal_img 		= $getInfo['DealInfo']['image'];
						$other_images 	= $getInfo['DealInfo']['other_images'];
						$data_images 	= array();
					    if( $other_images ){
					        $other_images = json_decode($other_images);
					        foreach( $other_images as $k => $img ){
					            if( $img != "img/deals/default.jpg" ){
					                $data_images[] = $this->path . $img;
					            }
					        }
					        $selected = @$data_images[array_rand($data_images)];

					        if( $selected ){ 
					            $deal_img = $selected ;
					        }			        
					    }

					    $getInfo['DealInfo']['other_images'] 	= $data_images;
						$getInfo['DealInfo']['image'] 			= $deal_img;
						$getInfo['DealInfo']['city'] 			= $cityInfo['city_name'];

						$bizDetail['logo'] 			= $this->path . $bizDetail['logo'];

	            		$getInfo['BusinessDetail'] = $bizDetail;
	            		$getInfo['BusinessDetail']['SangkatInfo'] = $sangkatInfo;
						$getInfo['BusinessDetail']['city'] 		  = $cityInfo['city_name'];

	            		$expired_period = $this->__COUPON_EXPIRATION_PERIOD;
	            		$purchase_date 	= $getInfo['BuyerTransaction']['created'];
	            		$now 			= date('Y-m-d');
	            		$expired_date 	= date('Y-m-d', strtotime('+6 month', strtotime($purchase_date)));

		            	$result['status'] 		= true;
		            	$result['redeemed'] 	= false;
		            	$result['expired'] 		= false;
		            	$result['date'] 		= date("d-F-Y", strtotime($expired_date));

	            		if( $getInfo['BuyerTransaction']['status'] == 1 ){
	            			$result['redeemed'] = true;
	            			$result['date'] 	= date('d-F-Y', strtotime($getInfo['BuyerTransaction']['received_date']));
	            		}else if( $now > $expired_date ){
	            			$result['expired']  = true;
	            		}

	            		$preparedData = array();
	            		$preparedData['Coupon']['id'] 			= $getInfo['BuyerTransaction']['id'];
	            		$preparedData['Coupon']['code'] 		= strtoupper($getInfo['BuyerTransaction']['code']);
	            		$preparedData['Coupon']['title'] 		= $getInfo['BuyerTransaction']['description'];
	            		$preparedData['Coupon']['images']		= $getInfo['DealInfo']['other_images'];

	            		$detail = $getInfo['TransactionDetail'];
	            		foreach( $detail as $k => $val ){
	            			$preparedData['CouponDetail'][$k]['item_name'] 	= $val['ItemDetail']['title'];
	            			$preparedData['CouponDetail'][$k]['unit_price'] = $val['unit_price'];
	            			$preparedData['CouponDetail'][$k]['qty'] 		= $val['qty'];
	            			$preparedData['CouponDetail'][$k]['amount'] 	= $val['amount'];
	            		}

	            		$preparedData['Coupon']['description']	= $getInfo['DealInfo']['description'];
	            		$preparedData['Coupon']['term_condition']	= $getInfo['DealInfo']['deal_condition'];

	            		$preparedData['Merchant']['id'] 		= $getInfo['BusinessDetail']['id'];
	            		$preparedData['Merchant']['name'] 		= $getInfo['BusinessDetail']['business_name'];
	            		$preparedData['Merchant']['logo']		= $getInfo['BusinessDetail']['logo'];

	            		$address = ($getInfo['BusinessDetail']['street'])?$getInfo['BusinessDetail']['street'] . ", ":"";
	            		$address .= $getInfo['BusinessDetail']['location'] . ", ";
	            		$address .= ($getInfo['SangkatInfo'])?$getInfo['SangkatInfo']['name'] . ", ":"";
	            		$address .= $getInfo['BusinessDetail']['city'];

	            		$preparedData['Merchant']['address'] 	= $address;
	            		$preparedData['Merchant']['phone1']		= $getInfo['BusinessDetail']['phone1'];
	            		$preparedData['Merchant']['phone2']		= $getInfo['BusinessDetail']['phone2'];

						$result['data'] = $preparedData;
						// $result['raw']  = $getInfo;
	            	}else{
		            	$result['status'] = false;
						$result['data'] = array();
	            	}
				}else{
					$result['status'] = false;
					$result['data'] = array();
				}

			}else{

				$result['status'] = false;
				$result['data'] = "Invalid Token !";

			}

		    // no view to render
		    $this->autoRender = false;
		    $this->response->type('json');
		    $json = json_encode($result,JSON_FORCE_OBJECT);
		    $this->response->body($json);
		
		}

		public function redeemPurchasedTransaction(){

			$getParam 		= $this->request->query;

			$secureToken 	= (!empty($getParam['token'])) ? $getParam['token'] : "";
			$transaction_id = (!empty($getParam['coupon_id'])) ? $getParam['coupon_id'] : "";
			$user_id 		= (!empty($getParam['user_id'])) ? $getParam['user_id'] : "";

			if( $this->validateToken( $this->appId, $this->appKey, $secureToken ) ){
				if( $transaction_id && $user_id ){

					$conds = array();
					$conds['conditions'] = array( 	'BuyerTransaction.id' 	 	=> $transaction_id,
													'BuyerTransaction.status' 	=> 0,
												);

					$this->BuyerTransaction->recursive = -1;
					$getInfo = $this->BuyerTransaction->find('first', $conds);

					if( $getInfo ){

						$expired_period = $this->__COUPON_EXPIRATION_PERIOD;
	            		$purchase_date 	= $getInfo['BuyerTransaction']['created'];
	            		$now 			= date('Y-m-d');
	            		$expired_date 	= date('Y-m-d', strtotime('+6 month', strtotime($purchase_date)));

	            		if( $now <= $expired_date ){

	            			$newData = array();
	            			$newData['BuyerTransaction']['id'] 				 = $transaction_id;
	            			$newData['BuyerTransaction']['status'] 			 = 1;
				            $newData['BuyerTransaction']['mark_received_by'] = $user_id ;
				            $newData['BuyerTransaction']['received_note'] 	 = 'Redeemed by Merchant App.' ;
				            $newData['BuyerTransaction']['received_date']    = date('Y-m-d H:i:s');

				            if( $this->BuyerTransaction->save($newData) ){
				            	$result['status'] 	= true;
								$result['data'] 	= "Coupon has been redeemed!";	
				            }else{
				            	$result['status'] 	= false;
								$result['data'] 	= "Redemption failed!";
				            }

	            		}else{
	            			$result['status'] 	= false;
							$result['data'] 	= "This coupon is expired!";
	            		}

					}else{
						$result['status'] 	= false;
						$result['data'] 	= "Invalid Request! No coupon found.";	
					}

				}else{
					$result['status'] = false;
					$result['data'] = "Invalid Request! No coupon found.";
				}
			}else{
				$result['status'] = false;
				$result['data'] = "Invalid Token !";
			}

			// no view to render
		    $this->autoRender = false;
		    $this->response->type('json');
		    $json = json_encode($result,JSON_FORCE_OBJECT);
		    $this->response->body($json);

		}


		public function resetUserPassword(){

			$getParam = $this->request->query;
			$secureToken = (!empty($getParam['token'])) ? $getParam['token'] : "";
			$getEmail 	 = (!empty($getParam['email'])) ? $getParam['email'] : "";

			if( $this->validateToken( $this->appId, $this->appKey, $secureToken ) ){

				$conds = array('conditions' => array('Buyer.email' => $getEmail) );
				$info = $this->Buyer->find('first', $conds);

				if( $getEmail == "" ){
					$result['status'] = false;
					$result['data'] = "Please enter email address.";
				}else if(!filter_var($getEmail , FILTER_VALIDATE_EMAIL)){
					$result['status'] = false;
					$result['data'] = "Invalid email address.";
				}else{

					if( $info && !empty($info) ){

						if( $info['Buyer']['status'] == 1 ){
							$token = $this->randomPassword() . $getEmail ;
							$loop = date('s');

					        for( $i =1; $i <= $loop ; $i++ ){
					          $token = sha1($token);
					        }

					        $activate_link = Router::url("/users/activatereset/"  . $token , true);

					        $data['Buyer']['id'] 	= $info['Buyer']['id'];
					        $data['Buyer']['token'] = $token;

					        $subject = "Password Reset Verification";

					        $content = "<h4>Dear " . ucwords($info['Buyer']['full_name']) . ",</h4><p> Please click on verification link below to complete your password reset.</p>";
					        $content .= "<p><a href='" . $activate_link . "'>" . $activate_link . "</a></p><br/>";

					        if( $this->sendMail($getEmail, $subject, $content) ){
					          	if( $this->Buyer->save($data) ){
									$result['status'] 	= true;
									$result['data'] 	= "Reset link has been sent to your email: " . $getEmail . ". Check you email inbox and click on the reset link."  ;	
					        	}
					        }else{
								$result['status'] 	= false;
								$result['data'] 	= "Sorry, something went wrong. Please try again!";
					        }
						}else{
							$result['status'] 	= false;
							$result['data'] 	= "Sorry, this account is deactivated.";	
						}
					}else{
						$result['status'] 	= false;
						$result['data'] 	= "Sorry, this email do not exist.";
					}

				}

			}else{

				$result['status'] = false;
				$result['data'] = "Invalid Token !";

			}

		    // no view to render
		    $this->autoRender = false;
		    $this->response->type('json');
		    $json = json_encode($result,JSON_FORCE_OBJECT);
		    $this->response->body($json);



			if( $this->request->is('post') ){
				$data = $this->request->data;

				$email = $data['Buyer']['email'];

				$conds = array('conditions' => array('Buyer.email' => $email, 'Buyer.status' => 1) );
				$info = $this->Buyer->find('first', $conds);

				if( $info && !empty($info) ){

					$token = $this->randomPassword() . $email ;
					$loop = date('s');

			        for( $i =1; $i <= $loop ; $i++ ){
			          $token = sha1($token);
			        }

			        $activate_link = Router::url("/Shops/activatereset/"  . $token , true);

			        $data['Buyer']['id'] 	= $info['Buyer']['id'];
			        $data['Buyer']['token'] = $token;

			        $subject = "Password Reset Verification Message";

			        $content = "<h4>Dear " . ucwords($info['Buyer']['full_name']) . ",</h4><p> Please click on verification link below to complete your password reset.</p>";
			        $content .= "<p><a href='" . $activate_link . "'>" . $activate_link . "</a></p><br/>";

			        if( $this->sendMail($email, $subject, $content) ){

			          	if( $this->Buyer->save($data)){
			            	$this->Session->setFlash(__( "Verification email has been sent to your email: " . $email )
			                      .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
			                    'default',
			                    array('class'=>'alert alert-dismissable alert-success align-center  '));
			            	return $this->redirect(array('action' =>'signin'));
			          	}

			        }else{

			          	$this->Session->setFlash(__( "Something went wrong. Please try again." )
			                      .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
			                    'default',
			                    array('class'=>'alert alert-dismissable alert-danger '));
			          	return $this->redirect(array('action' =>'forgotpassword'));

			        }

				}else{
					$this->Session->setFlash(__( "Reset Password Failed. This email does not exist in our system." )
	                      .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
	                    'default',
	                    array('class'=>'alert alert-dismissable alert-danger '));

	        		return $this->redirect(array('action' =>'forgotpassword'));
				}

			}
		
		}

		public function referFriend(){

			$getParam = $this->request->query;
			$secureToken = (!empty($getParam['token'])) ? $getParam['token'] : "";
			$getEmail 	 = (!empty($getParam['email'])) ? $getParam['email'] : "";
			$getUserID 	 = (!empty($getParam['user_id'])) ? $getParam['user_id'] : "";

			if( $this->validateToken( $this->appId, $this->appKey, $secureToken ) ){

				if (filter_var($getEmail, FILTER_VALIDATE_EMAIL)) {
					// Get Referer Bonus
					$conds 	= array('conditions' => array('type' => $this->referer_bonus, 'status' => 1));
					$info 	= $this->Commission->find('first', $conds);
					$referer_bonus 	= 0;

					if( $info ){
						$referer_bonus = $info['Commission']['commission'];
					}

					$this->set('bonus', $referer_bonus);

					// Get Referee Bonus
					$conds 	= array('conditions' => array('type' => $this->referee_bonus, 'status' => 1));
					$info 	= $this->Commission->find('first', $conds);
					$referee_bonus 	= 0;

					if( $info ){
						$referee_bonus = $info['Commission']['commission'];
					}

					// User Info
					$userInfo = $this->Buyer->find('first', array( 'conditions' => array('Buyer.id' => $getUserID, 'Buyer.status' => 1) ));

					if( $userInfo ){
						$user_id 	= $userInfo['Buyer']['id'];
						$email 		= $getEmail;

						$timestamp = time();

						$token_str 	= $email . "-" . $user_id . "-" . $timestamp ;
						$token 		= parent::generateReferalToken( $token_str );

						// Send Verification Mail
			            $receiver   = $email;
			            $subject    = "All Deal Invitation";
			            $content    = "";
			            $url        = Router::url("signup/?ref=" . $token , true );

			            $senderName = ucwords($userInfo['Buyer']['full_name']);

			        	$expired_date = date('Y-m-d');
						$expired_date = strtotime($expired_date);
					   	$expired_date = strtotime("+" . $this->__EXPIRATION_PERIOD . " day", $expired_date);

			            $content    .= "<html><body>";
			            $content 	.= "<p>" . $senderName . " has invited you to join All Deal.</p>";
			            $content    .= "<p>Sign Up and make your first purchase now to receive credit bonus in All Deal.</p>";
			            $content    .= "<br><p>Sign up throgh link below :</p>";
			            $content    .= "<a href='" . $url . "'>" . $url . "</a><br>";
			            $content    .= "<br><p>Bonus Expiration Date : " . date('d-F-Y', $expired_date) . "</p>";
			            $content    .= "</body></html>";

			            $sendMail =  $this->sendMail($receiver, $subject, $content);

			            if($sendMail){
			            	// Add Referer Token
			            	$tokenData = array();
			            	$tokenData['ReferalToken']['referer_id'] 	= $user_id ;
			            	$tokenData['ReferalToken']['email'] 		= $email ;
			            	$tokenData['ReferalToken']['timestamp']		= $timestamp ;
			            	$tokenData['ReferalToken']['referer_bonus']	= $referer_bonus ;
			            	$tokenData['ReferalToken']['referee_bonus']	= $referee_bonus ;
			            	$tokenData['ReferalToken']['token']			= $token ;
			            	$tokenData['ReferalToken']['expired_date']	= date( "Y-m-d", $expired_date ) ;

			            	// Check if email is already invited
						   	$conds['conditions'] = array( 'ReferalToken.referer_id' => $user_id, 'ReferalToken.email' => $email );
						   	$checkData = $this->ReferalToken->find('first', $conds );

						   	if( $checkData ){
						   		$tokenData['ReferalToken']['id'] = $checkData['ReferalToken']['id'];
						   	}

						   	$this->ReferalToken->create();
			            	$this->ReferalToken->save($tokenData);

			            	$result['status'] = true;
							$result['data'] = "Signup invitation has been sent to email: " . $getEmail ;

				        }else{

							$result['status'] = false;
							$result['data'] = "Something went wrong. Please try again.";
				        }
					}else{					
						$result['status'] = false;
						$result['data'] = "Something went wrong. Please try again.";
					}
				}else{					
					$result['status'] = false;
					$result['data'] = "Invalid Email Address !";					
				}

			}else{

				$result['status'] = false;
				$result['data'] = "Invalid Token !";
			}

			// no view to render
		    $this->autoRender = false;
		    $this->response->type('json');
		    $json = json_encode($result,JSON_FORCE_OBJECT);
		    $this->response->body($json);

		}

		public function getAllDealCreditTransaction(){

			$getParam 		= $this->request->query;
			$secureToken 	= (!empty($getParam['token'])) ? $getParam['token'] : "";
			$limit 			= $getParam['limit'];
			$page 			= $getParam['page'];
			$userID 		= $getParam['user_id'];

			// Array of payment type as All Deal Credit
			// 0: All Deal Credit Bonus, 2: Pay with All Deal Credit, 3: Top up all deal credit			
			$allDealCredit  = array( 0, 2, 3 );

			if( $this->validateToken( $this->appId, $this->appKey, $secureToken ) ){
				
				$conds = array();
				$conds['conditions'] 	= array( 'BuyerTransaction.buyer_id' => $userID, 'BuyerTransaction.payment_type' => $allDealCredit );
				$conds['order'] 		= array( 'BuyerTransaction.created' => 'DESC' );

				$this->BuyerTransaction->recursive = -1;
				$list = $this->BuyerTransaction->find('all', $conds);

				if( $list ){
					$dataList = array();
					foreach( $list as $k => $val ){
						$list[$k]['BuyerTransaction']['payment_type'] = $this->payment_method[$val['BuyerTransaction']['payment_type']];

						$dataList[$k]['transaction_date'] 	= date('d-M-Y', strtotime($val['BuyerTransaction']['created'])) ;
						$dataList[$k]['transaction_time'] 	= date('h:i:s A', strtotime($val['BuyerTransaction']['created'])) ;
						$dataList[$k]['type'] 				= $val['BuyerTransaction']['type'];
						$dataList[$k]['description'] 		= $val['BuyerTransaction']['description'];
						$dataList[$k]['code'] 				= ($val['BuyerTransaction']['code'])?strtoupper($val['BuyerTransaction']['code']):"";
						$dataList[$k]['amount'] 			= "$" . number_format($val['BuyerTransaction']['amount'], 2);
					}

					$result['status'] = true;
					$result['result'] = $dataList;

				}else{
					$result['status'] = false;
					$result['data']   = "Empty history!";
				}

			}else{
				$result['status'] = false;
				$result['data'] = "Invalid Token !";
			}

			// no view to render
		    $this->autoRender = false;
		    $this->response->type('json');
		    $json = json_encode($result,JSON_FORCE_OBJECT);
		    $this->response->body($json);

		}

		public function getTransactionDetail(){

			$getParam 		= $this->request->query;
			$secureToken 	= (!empty($getParam['token'])) ? $getParam['token'] : "";
			$transaction_id 	= $getParam['transaction_id'];

			if( $this->validateToken( $this->appId, $this->appKey, $secureToken ) ){

				if( $transaction_id ){
					$conds = array();
					$conds = array( 'conditions' => array('BuyerTransaction.id' => $transaction_id ) );

					$conds['joins']    = array(
			                                array(
			                                    'table' => 'tb_businesses',
			                                    'alias' => 'BusinessDetails',
			                                    'type' => 'LEFT',
			                                    'conditions' => array(
			                                        'BuyerTransaction.business_id = BusinessDetails.id'
			                                    )
	                                		),

			                                array(
			                                    'table' => 'tb_cities',
			                                    'alias' => 'CityInfo',
			                                    'type' => 'LEFT',
			                                    'conditions' => array(
			                                        'BusinessDetails.city = CityInfo.city_code'
			                                    )
	                                		)
	                                	);

					$conds['fields'] = array( 'BuyerTransaction.*', 'BusinessDetails.*', 'CityInfo.*' );

					$this->BuyerTransaction->recursive = 2;
	            	$getInfo = $this->BuyerTransaction->find('first', $conds);

	            	$preparedData = array();

	            	if( $getInfo ){
	            		$sangkatInfo = $getInfo['BusinessDetail']['SangkatInfo'];
	            		$cityInfo 	 = $getInfo['CityInfo'];
	            		$bizDetail = $getInfo['DealInfo']['BusinessInfo'];

	            		$deal_img 		= $getInfo['DealInfo']['image'];
						$other_images 	= $getInfo['DealInfo']['other_images'];
						$data_images 	= array();
					    if( $other_images ){
					        $other_images = json_decode($other_images);
					        foreach( $other_images as $k => $img ){
					            if( $img != "img/deals/default.jpg" ){
					                $data_images[] = $this->path . $img;
					            }
					        }
					        $selected = @$data_images[array_rand($data_images)];

					        if( $selected ){ 
					            $deal_img = $selected ;
					        }			        
					    }

					    $getInfo['DealInfo']['other_images'] 	= $data_images;
						$getInfo['DealInfo']['image'] 			= $deal_img;
						$getInfo['DealInfo']['city'] 			= $cityInfo['city_name'];

						$bizDetail['logo'] 			= $this->path . $bizDetail['logo'];

	            		$getInfo['BusinessDetail'] = $bizDetail;
	            		$getInfo['BusinessDetail']['SangkatInfo'] = $sangkatInfo;
						$getInfo['BusinessDetail']['city'] 		  = $cityInfo['city_name'];

	            		$expired_period = $this->__COUPON_EXPIRATION_PERIOD;
	            		$purchase_date 	= $getInfo['BuyerTransaction']['created'];
	            		$now 			= date('Y-m-d');
	            		$expired_date 	= date('Y-m-d', strtotime('+6 month', strtotime($purchase_date)));

		            	$result['status'] 		= true;
		            	$result['redeemed'] 	= false;
		            	$result['expired'] 		= false;
		            	$result['date'] 		= date("d-F-Y", strtotime($expired_date));

	            		if( $getInfo['BuyerTransaction']['status'] == 1 ){
	            			$result['redeemed'] = true;
	            			$result['date'] 	= date('d-F-Y', strtotime($getInfo['BuyerTransaction']['received_date']));
	            		}else if( $now > $expired_date ){
	            			$result['expired']  = true;
	            		}

	            		$preparedData = array();
	            		$preparedData['Coupon']['id'] 			= $getInfo['BuyerTransaction']['id'];
	            		$preparedData['Coupon']['code'] 		= strtoupper($getInfo['BuyerTransaction']['code']);
	            		$preparedData['Coupon']['title'] 		= $getInfo['BuyerTransaction']['description'];
	            		$preparedData['Coupon']['images']		= $data_images;

	            		$detail = $getInfo['TransactionDetail'];
	            		foreach( $detail as $k => $val ){
	            			$preparedData['CouponDetail'][$k]['item_name'] 	= $val['ItemDetail']['title'];
	            			$preparedData['CouponDetail'][$k]['unit_price'] = $val['unit_price'];
	            			$preparedData['CouponDetail'][$k]['qty'] 		= $val['qty'];
	            			$preparedData['CouponDetail'][$k]['amount'] 	= $val['amount'];
	            		}

	            		$preparedData['Coupon']['description']	= $getInfo['DealInfo']['description'];
	            		$preparedData['Coupon']['term_condition']	= $getInfo['DealInfo']['deal_condition'];

	            		$preparedData['Merchant']['id'] 		= $getInfo['BusinessDetail']['id'];
	            		$preparedData['Merchant']['name'] 		= $getInfo['BusinessDetail']['business_name'];
	            		$preparedData['Merchant']['logo']		= $getInfo['BusinessDetail']['logo'];

	            		$address = ($getInfo['BusinessDetail']['street'])?$getInfo['BusinessDetail']['street'] . ", ":"";
	            		$address .= $getInfo['BusinessDetail']['location'] . ", ";
	            		$address .= ($getInfo['SangkatInfo'])?$getInfo['SangkatInfo']['name'] . ", ":"";
	            		$address .= $getInfo['BusinessDetail']['city'];

	            		$preparedData['Merchant']['address'] 	= $address;
	            		$preparedData['Merchant']['phone1']		= $getInfo['BusinessDetail']['phone1'];
	            		$preparedData['Merchant']['phone2']		= $getInfo['BusinessDetail']['phone2'];

						$result['data'] = $preparedData;
	            	}else{
		            	$result['status'] = false;
						$result['data'] = array();
	            	}
				}else{
					$result['status'] = false;
					$result['data'] = "Invalid Request !";
				}
					
			}else{
				$result['status'] = false;
				$result['data'] = "Invalid Token !";
			}

			// no view to render
		    $this->autoRender = false;
		    $this->response->type('json');
		    $json = json_encode($result,JSON_FORCE_OBJECT);
		    $this->response->body($json);
		}

		public function getUserDetailInformation(){
			$getParam 		= $this->request->query;
			$secureToken 	= (!empty($getParam['token'])) ? $getParam['token'] : "";
			$user_id 		= $getParam['user_id'];

			if( $this->validateToken( $this->appId, $this->appKey, $secureToken ) ){

				if( $user_id ){

					$conds = array( 'conditions' => array( 'Buyer.id' => $user_id ));
			     	$conds['joins']    = array(
			                                array(
			                                    'table' => 'tb_cities',
			                                    'alias' => 'City',
			                                    'type' => 'LEFT',
			                                    'conditions' => array(
			                                        'Buyer.city_code = City.city_code'
			                                    )
	                                		)
	                                	);

			     	$conds['fields'] = array( 'Buyer.*', 'SangkatInfo.*', 'City.*' );
			     	// $this->Buyer->recursive = 2;
			     	$getInfo = $this->Buyer->find('first', $conds);

			     	if( $getInfo && !empty($getInfo) ){

						if( $getInfo['Buyer']['status'] == 1 ){

			     			$preparedData = array();

			     			$userData = array();
			     			$userData['id'] 			= $getInfo['Buyer']['id'];
			     			$userData['code'] 			= strtoupper($getInfo['Buyer']['buyer_code']);
			     			$userData['name'] 			= $getInfo['Buyer']['full_name'];
			     			$userData['gender'] 		= $getInfo['Buyer']['gender'];
			     			$userData['dob'] 			= $getInfo['Buyer']['dob'];
			     			$userData['phone1'] 		= $getInfo['Buyer']['phone1'];
			     			$userData['phone2'] 		= $getInfo['Buyer']['phone2'];
			     			$userData['email'] 			= $getInfo['Buyer']['email'];
			     			$userData['province_code'] 	= $getInfo['Buyer']['city_code'];
			     			$userData['city_code'] 		= $getInfo['Buyer']['city_code'];
			     			$userData['city_name'] 		= $getInfo['City']['city_name'];
			     			$userData['location_id'] 	= $getInfo['Buyer']['location_id'];
			     			$userData['location_name'] 	= $getInfo['Buyer']['location'];
			     			$userData['sangkat_id'] 	= $getInfo['Buyer']['sangkat_id'];
			     			$userData['sangkat_name'] 	= $getInfo['SangkatInfo']['name'];
			     			$userData['street'] 		= $getInfo['Buyer']['street'];
			     			$userData['amount_balance'] = $getInfo['Buyer']['amount_balance'];

			     			$preparedData['User'] = $userData;
			     			
							$result['status'] = true;
							$result['data'] = $preparedData;
			     		}else{
							$result['status'] = false;
							$result['data'] = "This account is deactivated !";
			     		}

			     	}else{

						$result['status'] = false;
						$result['data'] = "No user found !";

			     	}
				}else{
					$result['status'] = false;
					$result['data'] = "Invalid request!";
				}

			}else{
				$result['status'] = false;
				$result['data'] = "Invalid Token !";
			}

			// no view to render
		    $this->autoRender = false;
		    $this->response->type('json');
		    $json = json_encode($result,JSON_FORCE_OBJECT);
		    $this->response->body($json);
		
		}


		public function saveMyProfile(){

			$getParam 		= $this->request->query;
			$secureToken 	= (!empty($getParam['token'])) ? $getParam['token'] : "";
			$getData 		= (!empty($getParam['data'])) ? $getParam['data'] : "";
			$getUserID  	= (!empty($getParam['user_id'])) ? $getParam['user_id'] : "";

			$msg_error 		= array();

			if( $this->validateToken( $this->appId, $this->appKey, $secureToken ) ){

				$json_data = json_decode($getData, true);

				if(!empty($json_data) && is_array($json_data) && $getUserID){

					$id 			= $data['Buyer']['id'] 				= $getUserID;
					$name 			= $data['Buyer']['full_name'] 		= isset($json_data['full_name']) ? $json_data['full_name'] : NULL;
			 		$gender 		= $data['Buyer']['gender'] 			= isset($json_data['gender']) ? $json_data['gender'] : NULL;
			 		$dob 			= $data['Buyer']['dob'] 			= isset($json_data['dob']) ? date('Y-m-d', strtotime($json_data['dob'])) : NULL;
			 		$province_code 	= $data['Buyer']['province_code'] 	= isset($json_data['province_code']) ? $json_data['province_code'] : NULL;
			 		$city_code 		= $data['Buyer']['city_code'] 		= isset($json_data['city_code']) ? $json_data['city_code'] : NULL;
			 		$location 		= $data['Buyer']['location'] 		= isset($json_data['location']) ? $json_data['location'] : NULL;
			 		$location_id 	= $data['Buyer']['location_id'] 	= isset($json_data['location_id']) ? $json_data['location_id'] : NULL;
			 		$sangkat_id 	= $data['Buyer']['sangkat_id'] 		= isset($json_data['sangkat_id']) ? $json_data['sangkat_id'] : NULL;
			 		$street 		= $data['Buyer']['street'] 			= isset($json_data['street']) ? $json_data['street'] : NULL;
			 		$phone1 		= $data['Buyer']['phone1'] 			= isset($json_data['phone1']) ? $json_data['phone1'] : NULL;
			 		$phone2 		= $data['Buyer']['phone2'] 			= isset($json_data['phone2']) ? $json_data['phone2'] : NULL;
			 		$email 			= $data['Buyer']['email'] 			= isset($json_data['email']) ? $json_data['email'] : NULL;
			 		$password 		= $data['Buyer']['password'] 		= isset($json_data['password']) ? $json_data['password'] : NULL;

			 		$cnf_password = isset($json_data['confirm_password']) ? $json_data['confirm_password'] : NULL;

			 		$current_password = isset($json_data['current_password']) ? $json_data['current_password'] : NULL;

					if( $name == "" ){ array_push($msg_error, 'Please enter your name !'); }
					if( $dob_d == "" || $dob_m == "" || $dob_y == "" ){ array_push($msg_error, "Invalid date of birth !"); }
					if( $city == "" ){ array_push($msg_error, "Please select province !"); }
					if( $location == "" ){ array_push($msg_error, "Please select location !"); }
					if( $phone1 == "" ){ array_push($msg_error, "Please enter your phone number !"); }
					
					if( $email == "" ){ 
						array_push($msg_error, "Please enter your email address !");
					}else{

						if (!filter_var($email , FILTER_VALIDATE_EMAIL)) {
		                    array_push($msg_error, "Invalid Email Address !");
		                }else{
		                	$conds = array('conditions' => array('Buyer.id !=' => $data['Buyer']['id'], 'Buyer.email' => $email) );
		                	$checkEmail = $this->Buyer->find('first',$conds);
		                	if( !empty($checkEmail) ){
		                		array_push($msg_error, "Duplicate Email Address ! Please use another email address.");
		                	}
		                }
					}

					if( $password != "" ){
						if( strlen($password) < 6 ){
							array_push($msg_error, "Please enter password and must be at least 6 characters !");
						}else{

							if( $cnf_password != $password ){
								array_push($msg_error, "Password and Confirm Password does not match !" );
							}
						}

					}else{

						unset($data['Buyer']['password']);
						unset($this->request->data['Buyer']['password']);
					}

					if( $current_password == "" ){
						array_push($msg_error, "Please enter your current password to save change !");
					}else{
						$old_password = $info['Buyer']['password'];
						$passwordHasher = new SimplePasswordHasher(array('hashType'=>'sha256'));
			  			$current_pass = $passwordHasher->hash($current_password);

			  			if( $current_pass != $old_password ){
			  				array_push($msg_error, "Your current password is not correct !");
			  			}
					}

					unset($this->request->data['Buyer']['confirm_password']);
					unset($data['Buyer']['confirm_password']);

					unset($this->request->data['Buyer']['current_password']);
					unset($data['Buyer']['current_password']);

					if( count($msg_error) > 0 ){
						foreach( $msg_error as $k => $val ){
							$result['status'] = false;
							$result['message'][$k] = $val;
						}
						$result['data'] = $data;
					    // no view to render
					    $this->autoRender = false;
					    $this->response->type('json');
					    $json = json_encode($result,JSON_FORCE_OBJECT);
					    $this->response->body($json);
					}else{

						if( $this->Buyer->save($data) ){
							
							$result['status'] = true;
							$result['data'] = "Profile has been save !";

						}else{
							$result['status'] = false;
							$result['data'] = "Saving information failed !";
						}

					}
				}else{
					$result['status'] = false;
					$result['data'] = "Empty data posted!";
				}

			}else{
				$result['status'] = false;
				$result['data'] = "Invalid Token !";
			}
			// no view to render
		    $this->autoRender = false;
		    $this->response->type('json');
		    $json = json_encode($result,JSON_FORCE_OBJECT);
		    $this->response->body($json);
			
		}


		
		public function getTransactionHistory(){

			$getParam 		= $this->request->query;
			$secureToken 	= (!empty($getParam['token'])) ? $getParam['token']:"" ;
			$user_id 		= $getParam['user_id'];
			// $limit 			= ($getParam['limit']) ;
			// $page 			= $getParam['page'];

			$redeemed 		= (!empty($getParam['redeemed'])) ? $getParam['redeemed']:0 ;

			if( $this->validateToken( $this->appId, $this->appKey, $secureToken ) ){
				
				$conds = array();
				$conds['conditions'] 	= array( 'BuyerTransaction.buyer_id' => $userID, 'BuyerTransaction.type' => 0, 'BuyerTransaction.buyer_id' => $user_id );

				if( $redeemed == 1 ){
					$conds['conditions']['BuyerTransaction.status'] = 1;
				}else{
					$conds['conditions']['BuyerTransaction.status'] = 0;
				}

				$conds['order'] = array( 'BuyerTransaction.created' => 'DESC' );

				// $this->BuyerTransaction->recursive = 2;
				$list = $this->BuyerTransaction->find('all', $conds);

				$expired_period = $this->__COUPON_EXPIRATION_PERIOD;

				if( $list ){

					$dataList = array();
					foreach( $list as $k => $val ){

		            	$dealInfo = $val['DealInfo'];

		            	$deal_img 		= $dealInfo['image'];
						$other_images 	= $dealInfo['other_images'];
						$data_images 	= array();
					    if( $other_images ){
					        $other_images = json_decode($other_images);
					        foreach( $other_images as $key => $img ){
					            if( $img != "img/deals/default.jpg" ){
					                $data_images[] = $this->path . $img;
					            }
					        }
					        $selected = @$data_images[array_rand($data_images)];

					        if( $selected ){ 
					            $deal_img = $selected ;
					        }			        
					    }

					    $bizDetail = $val['BusinessDetail'];
					    $list[$k]['BusinessDetail']['logo'] = $this->path . $val['BusinessDetail']['logo'];

					    $dataList[$k]['id'] 			= $val['BuyerTransaction']['id'];
					    $dataList[$k]['image'] 			= $deal_img;
					    $dataList[$k]['description']	= $val['BuyerTransaction']['description'];
					    $dataList[$k]['merchant_name']	= $val['BusinessDetail']['business_name'];
					    $dataList[$k]['amount']			= "$" . number_format($val['BuyerTransaction']['amount'], 2);

					}

					$result['status'] = true;
					$result['result'] = $dataList;

				}else{
					$result['status'] = false;
					$result['data']   = "Empty history!";
				}

			}else{
				$result['status'] = false;
				$result['data'] = "Invalid Token !";
			}

			// no view to render
		    $this->autoRender = false;
		    $this->response->type('json');
		    $json = json_encode($result,JSON_FORCE_OBJECT);
		    $this->response->body($json);

		}
	}
