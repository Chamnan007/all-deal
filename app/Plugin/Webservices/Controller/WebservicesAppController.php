<?php

App::uses('AppController', 'Controller');

class WebservicesAppController extends AppController {
	
	function validateToken( $appId = "", $appKey = "", $token=""){

 		// for testing
 		return true;

		$secretKey = array(	"Techno",
							"Secret",
							"Phnom Penh",
				   			"Nervous",
				   			"Green Light",
				   			"Intelligent",
							"Fast",
							"Be Smart",
				   			"Easy",
				   			"Buy Now"
				   		);
				   
		$data = explode("@", $token);

		if( count($data) < 2 ) return false;
		
		$index 		= substr($data[1], 0, 1);
		$datetime 	= substr($data[1], 1);
		$dateitem 	= explode(":", $datetime);
		$count 		= $dateitem[2];
		
		$submit_time = strtotime($datetime);
		$current_time = time();
		
		$diff = round(($current_time - $submit_time)/(60*60) );
		
		if ($diff >= 24) {
			return false;
		}
		
		$hash = sha1($appId . ":" . $appKey . ":" . $secretKey[$index] . " " . $datetime);
		
		for ($i = 0; $i < $count; $i++) {
			$hash = sha1($hash);
		}
		
		if ($data[0] == $hash) {
			return true;
		}
		
		return false;
 	}
 	
}
