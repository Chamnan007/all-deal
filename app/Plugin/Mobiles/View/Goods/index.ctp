<?php 

/********************************************************************************

File Name: index.ctp
Description: displaying goods UI

Powered By: ABi Investment Group Co., Ltd,

Changed History:

    Date                Author              Description
    2014/06/10          Sim Chhayrambo      Initial Version
    2014/06/13          Sim Chhayrambo      Check if no deals found
    2014/06/19          Sim Chhayrambo      Add meta description, add reset all button
    2014/07/13          Sim Chhayrambo      Remove space in thumbnail, limit Title
    2014/07/16          Sim Chhayrambo      Add token
    2015/02/25          Phea Rattana        check for deal image randomly to display,
                                            check price discount if available

*********************************************************************************/


if(!empty($featureDeal['Deal'])){
    $featureDealInfo = $featureDeal['Deal'];
    $featureID = $featureDealInfo['id'];
    $featureTitle = $featureDealInfo['title'];
    $featureImage = $featureDealInfo['image'];
    $featureDesc = $featureDealInfo['description'];

    $other_images = $featureDealInfo['other_images'];
    if( $other_images ){
        $other_images = json_decode($other_images);
        foreach( $other_images as $k => $img ){
            if( $img != "img/deals/default.jpg" ){
                $data_images[] = $img;
            }
        }
        $selected = @$data_images[array_rand($data_images)];

        if( $selected ){ 
            $featureImage   = $selected;
        }
        
    }

    $featureBusinessName = $featureDeal['Business']['business_name'];
    $featureBusinessStreet = $featureDeal['Business']['street'];
    $featureBusinessLocation = $featureDeal['Business']['location'];
    $featureBusinessProvince = $featureDeal['Province']['province_name'];

    $featureSlug = $featureDealInfo['slug'];

    $original = "";
    $featureDiscount = $featureDealInfo['discount_category'];

    $prev_original  = 0;
    $prev_discount  = 0;

    if( $items = $featureDeal['DealItemLink'] ){

        $max_percentage = 0;

        foreach( $items as $k => $v ){

            $ori        = ($v['original_price'] != 0 )?$v['original_price']:$v['ItemDetail']['price'];
            $disc       = $v['discount'];
            $item_id    = $v['item_id'];

            $percentage = 100 - ( $disc * 100 / $ori );

            if( $percentage > $max_percentage ){
                $max_percentage = $percentage;
                $prev_original  = $ori;
                $prev_discount  = $disc;
            }else if( $percentage == $max_percentage ){
                if( $disc < $prev_discount ){
                    $prev_original  = $ori;
                    $prev_discount  = $disc;
                }
            }

        }

        $original = "$" . number_format($prev_original, 2) ;
        $featureDiscount = "$" . number_format($prev_discount, 2);                        
    }

}else{

    $featureDealInfo = NULL;
    $featureID = NULL;
    $featureTitle = NULL;
    $featureImage = NULL;
    $featureDesc = NULL;

    $featureBusinessName = NULL ;
    $featureBusinessStreet = NULL ;
    $featureBusinessLocation = NULL ;
    $featureBusinessProvince = NULL ;

    $featureDiscount = NULL ;
    $featureSlug = NULL ;
}


// SEARCH PARAMS
$arrCategory = array();
$arrLocation = array();
$arrInterest = array();
$arrService = array();
$strDescription = "";

if(!empty($filterCategory)){
    $arrCategory = $filterCategory;
}

if(!empty($filterLocation)){
    $arrLocation = $filterLocation;
}

if(!empty($filterInterest)){
    $arrInterest = $filterInterest;
}

if(!empty($filterService)){
    $arrService = $filterService;
}

// 2014-08-11
// if (strpos($title,'-') !== false) {
//     $newArray = explode("-", $title);
//     $title = ucwords($newArray[0]) . " & " . ucwords($newArray[1]);
// }else{
//     $title = ucwords($title);
// }

// replace '-' with empty
$title = str_replace("-", " ", $title);
$title = ucwords($title);
if($title == "Food Drink"){
    $title = "Food & Drink";
}elseif($title == "Health Beauty"){
    $title = "Health & Beauty";
}elseif($title == "Lock Keys"){
    $title = "Lock & Keys";
}

// first we make everything lowercase, and then make the first letter if the entire string capitalized
// $title = ucfirst(strtolower($title));

// now we run the function to capitalize every letter AFTER a full-stop (period).
// $title = preg_replace_callback('/[.!?].*?\w/', create_function('$matches', 'return strtoupper($matches[0]);'),$title);


if(!empty($this->request->params['named'])){
    $param = $this->request->params['named'];

    foreach($param as $k => $v ){
        $params[$k][] = $v;
    }
    $jParams = json_encode($params);
}else{
    $jParams = json_encode($this->params['url']);  
}


if(!empty($arrCategory)){
    foreach ($arrCategory as $key => $value) {
        $strDescription .= ", " . $value;
    }
}

if(!empty($arrLocation)){
    foreach ($arrLocation as $key => $value) {
        $strDescription .= ", " . $value;
    }
}

if(!empty($arrInterest)){
    foreach ($arrInterest as $key => $value) {
        $strDescription .= ", " . $value;
    }
}

if(!empty($arrService)){
    foreach ($arrService as $key => $value) {
        $strDescription .= ", " . $value;
    }
}

if(empty($strDescription)){
    $strDescription = $title;
}

$metaDescription = substr("All Deal: " . ltrim($strDescription, ","),0,300).'...';
$this->Html->meta('description', $metaDescription, array('inline'=>false));

// var_dump($jParams);

?>

<div class="container">
    <input type="hidden" name="secure_token" id="secure_token" value="<?php echo $token ?>">
    <div style="height: 20px;"></div>
    <div class="col-sm-12 no-padding" style="margin: 10px 0; border-bottom: 1px dotted #999;">
        <h1 style="line-height: 40px; font-size: 28px;">
            <?php 
            if(!empty($searchCity) && !empty($searchText)){
                echo 'Searching for ' . '"' . $searchText . '"';
            }else{
                echo $title;
            } ?>
        </h1>
    </div>
    <div class="col-sm-12 no-padding clearfix" style="margin: 20px 0;">
        <div class="col-sm-3 no-padding-left">
            <form method="get" name="frmFilter" id="frmFilter" action="<?php echo $this->Html->url(array('controller'=>'goods', 'action'=>'index')) ?>">
                <div class="categories-wrapper">
                    <h3 style="margin-bottom: 5px; font-weight: bold;">
                        <?php echo __('Goods') ?>
                    </h3>
                    <ul>
                        <?php 
                        foreach ($goodCate as $gCatKey => $gCatValue) { 
                            $strChecked = "";
                            if(in_array($gCatValue['GoodsCategory']['slug'], $arrCategory) || $gCatValue['GoodsCategory']['slug'] == $searchCategory){
                                $strChecked = "checked='checked'";
                            }
                        ?>
                        <!-- <li class="category">
                            <div class="category-name">
                                <span class="name"><a href="#"><?php echo $gCatValue['GoodsCategory']['category'] ?></a></span>
                            </div>
                        </li> -->
                        <li class="category">
                            <div class="category-name">
                                <input value="<?php echo $gCatValue['GoodsCategory']['slug'] ?>" type="checkbox" id="<?php echo "category_" . $gCatValue['GoodsCategory']['id'] ?>" class="category" name="category[]" <?php echo $strChecked ?>>
                                <label for="<?php echo "category_" . $gCatValue['GoodsCategory']['id'] ?>">
                                    <span class="name truncated"><?php echo $gCatValue['GoodsCategory']['category'] ?></span>
                                    <!-- <span class="deal-counts">(18)</span> -->
                                </label>
                            </div>
                        </li>
                        <?php } ?>
                    </ul>
                </div>
                <div class="categories-wrapper">
                    <h3 style="margin-bottom: 5px; font-weight: bold;"><?php echo __('Location') ?></h3>
                    <ul>
                        <?php 
                        $strChecked = "";
                        if($searchCity == "all" || in_array("all_location", $arrLocation) || $searchLocation == "All") $strChecked = "checked='checked'";
                        ?>
                        <li class="category">
                            <div class="category-name">
                                <input type="checkbox" class="mainLocation" id="all_location" value="all_location" name="location[]" <?php echo $strChecked ?>>
                                <label for="all_location">
                                    <span class="name truncated"><?php echo __('All') ?></span>
                                    <!-- <span class="deal-counts">(23)</span> -->
                                </label>
                                
                            </div>
                        </li>
                        <?php 
                        foreach ($cities as $cityKey => $cityValue) {
                            $strChecked = "";
                            if (in_array($cityValue['City']['slug'], $arrLocation) || $cityValue['City']['slug'] == $searchLocation || $cityValue['City']['slug'] == $searchCity){
                                $strChecked = "checked='checked'";
                            }
                        ?>
                        <li class="category">
                            <div class="category-name">
                                <input value="<?php echo $cityValue['City']['slug'] ?>" type="checkbox" id="<?php echo "province_" . $cityValue['City']['id'] ?>" class="subLocation" name="location[]" <?php echo $strChecked ?>>
                                <label for="<?php echo "province_" . $cityValue['City']['id'] ?>">
                                    <span class="name truncated"><?php echo $cityValue['City']['city_name'] ?></span>
                                    <!-- <span class="deal-counts">(18)</span> -->
                                </label>
                            </div>
                        </li>
                        <?php } ?>
                    </ul>
                </div>
               
                <div class="categories-wrapper">
                    <div class="category-reset">
                        <span>
                            <a href="<?php echo $this->Html->url(array('action'=>'index')) ?>">
                                <i class="icon-cancel" style="color: #428bca;"></i>
                                <?php echo __('Reset All'); ?>
                            </a>
                        </span>
                        <button name="filter" class="pull-right btn-green" style="border: 1px solid #95b959; border-radius: 3px;">
                            <i class="icon-eye" style="color: #fff;"></i>
                            <?php echo __('Search') ?>
                        </button>
                    </div>
                </div>
                
            </form>
        </div>
        <div class="col-sm-9 no-padding" id="dealContainer">
            <?php if(!empty($deals)){ ?>
            <div class="feature-deal">
                <a href="<?php echo $this->Html->url(array('controller'=>'deals', 'action'=>'detail', $featureSlug)) ?>">
                    <img src="<?php echo $this->webroot . $featureImage?>" width="465" height="264" style="float: left;">
                </a>
                <figcaption class="featured-deal">
                    <div class="new-project add-margin-bottom">
                        <div id="bx-pager">
                            <a data-slide-index="0" href="<?php echo $this->Html->url(array('controller'=>'deals', 'action'=>'detail', $featureSlug)) ?>" class="active">
                                <div class="new-project-body" style="padding: 0;">
                                    <h4 style="font-size: 21px; font-weight: bold;"><?php echo $featureTitle ?></h4>
                                    <p style="font-size: 14px;"><?php echo $featureBusinessName ?></p>
                                    <p style="font-size: 12px;"><?php echo $featureBusinessLocation . ", " . $featureBusinessProvince ?></p>
                                    <h4 class="pull-left" style="font-size: 14px; color: #444; margin-top:2px; text-decoration: line-through;<?php echo($original!="")?' margin-right: 10px; margin-left: 7px;':'' ?>"><?php echo $original ?></h4>
                                    <h4 class="price-off" style="margin-bottom: 10px; text-align:left;"><?php echo $featureDiscount ?></h4>
                                    <a href="<?php echo $this->Html->url(array('controller'=>'deals', 'action'=>'detail', $featureSlug)) ?>" class="button btn-green pull-right"><i class="icon-search" style="color: #fff;"></i> <?php echo __('View Detail') ?></a>
                                </div>
                            </a>
                        </div>
                    </div>
                </figcaption>
            </div>
            <div class="col-sm-12 no-padding clearfix" style="margin-top: 20px;">
                <div class="recent-works col-sm-12 no-padding">
                    <div class="recent-works-carousel">
                        <div class="carousel slide" id="Recentworks">
                            <div class="carousel-inner">
                                <div class="item active">
                                    <ul class="thumbnails">
                                        <div class="row" id="deal-container">
                                            <?php 
                                            foreach ($deals as $dKey => $dValue) { 

                                                $split = end(explode("/", $dValue['Deal']['image']));
                                                $thumb_img =  "img/deals/thumbs/" . $split ;
                                                $business_name = $dValue['Business']['business_name'];
                                                $business_street = $dValue['Business']['street'];
                                                $business_location = $dValue['Business']['location'];
                                                $deal_title = $dValue['Deal']['title'];
                                                
                                                if(strlen($deal_title) > $limit_deal_title){
                                                    $deal_title = substr($dValue['Deal']['title'],0,$limit_deal_title-3).'...';   
                                                }

                                                if(strlen($business_name) > $limit_business_title){
                                                    $business_name = substr($business_name,0,$limit_business_title-3).'...';
                                                }

                                                if( $other_deal_imgs = $dValue['Deal']['other_images'] ){
                                                    $imgs = json_decode($other_deal_imgs);
                                                    $data_img = array();

                                                    foreach( $imgs as $k => $img ){
                                                        $data_img[] = $img;
                                                    }

                                                    $selected = array_rand($data_img);

                                                    $thumb_img = str_replace("/deals/", "/deals/thumbs/", $data_img[$selected] );
                                                }


                                                $original = "";
                                                $discount = $dValue['Deal']['discount_category'];

                                                $prev_original  = 0;
                                                $prev_discount  = 0;

                                                if( $items = $dValue['DealItemLink'] ){

                                                    $max_percentage = 0;

                                                    foreach( $items as $k => $v ){

                                                        $ori        = ($v['original_price'] != 0 )?$v['original_price']:$v['ItemDetail']['price'];
                                                        $disc       = $v['discount'];
                                                        $item_id    = $v['item_id'];

                                                        $percentage = 100 - ( $disc * 100 / $ori );

                                                        if( $percentage > $max_percentage ){
                                                            $max_percentage = $percentage;
                                                            $prev_original  = $ori;
                                                            $prev_discount  = $disc;
                                                        }else if( $percentage == $max_percentage ){
                                                            if( $disc < $prev_discount ){
                                                                $prev_original  = $ori;
                                                                $prev_discount  = $disc;
                                                            }
                                                        }

                                                    }

                                                    $original = "$" . number_format($prev_original, 2) ;
                                                    $discount = "$" . number_format($prev_discount, 2);                        
                                                }

                                            ?>
                                                <li class="col-sm-4 col-xs-4">
                                                    <div class="new-project bg-shadow add-margin-bottom deal-img-container">
                                                        <div>
                                                            <a href="<?php echo $this->Html->url(array('controller'=>'deals', 'action'=>'detail', $dValue['Deal']['slug'])) ?>">
                                                                <img src="<?php echo $this->webroot . $thumb_img ?>" title="<?php echo $dValue['Deal']['title'] ?>">
                                                            </a>
                                                        </div>
                                                        <div id="bx-pager">
                                                            <a data-slide-index="0" href="<?php echo $this->Html->url(array('controller'=>'deals', 'action'=>'detail', $dValue['Deal']['slug'])) ?>" class="active">
                                                                <div class="new-project-body">
                                                                    <h4><?php echo $deal_title ?></h4>
                                                                    <i><?php echo $business_name ?></i><br>
                                                                    <i class="icon-location"><?php echo $business_location . ", " . $dValue['Province']['province_name'] ?></i>
                                                                    <div class="clearfix"></div>
                                                                    <h4 class="price-off pull-right"><?php echo $discount ?></h4>
                                                                    <h4 style="color:#666; float:right; margin-right: 10px; text-decoration: line-through;"><?php echo $original ?></h4>
                                                                </div>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </li>
                                            <?php } ?>
                                        </div>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="pagination_footer" style="width: 100%; text-align: center;">
                    <h4 id="showNumber"></h4>
                    <a href="#" id="view-more" class="button btn-green" style="width: 140px; margin: 0 auto; margin-top: 10px; padding: 10px; display: none;"> <?php echo __('VIEW') . " " . $limit . " " . __('MORE') ?> <i class="icon-angle-down"></i></a>
                </div>
                <div class="ajax-loading" style="width: 100%; text-align: center; display:none;">
                    <img src="<?php echo $this->webroot . "img/bx_loader.gif" ?>">
                </div>
            </div>
            <?php }else{?>
            <div class="not-found">
                <h1><?php echo __('No Deals Found!') ?></h1>
            </div>
            <?php } ?>
        </div>
    </div>
</div>

<script type="text/javascript">

$(function(){

    var track_click = 1,
        total_deals = <?php echo $total_deals; ?>,
        total_pages = <?php echo $total_pages; ?>,
        limit = <?php echo $limit; ?>,
        current_deal = limit * track_click;
        params = <?php echo $jParams; ?>,
        divContainer = $("div#deal-container"),
        secure_token = $("input#secure_token").val();

    if(current_deal > total_deals){
        current_deal = total_deals;
    }

    if(total_deals <= limit){
        $("a#view-more").hide();
    }else{
        $("a#view-more").show();
    }

    $("h4#showNumber").text("Show "+current_deal+" of "+total_deals);

    $("a#view-more").click(function() {
        
        event.preventDefault();

        var me = $(this);

        track_click++;

        $.ajax({
            url:"<?php echo $this->Html->url(array('controller'=>'goods', 'action'=>'getMore')) ?>"+"/"+track_click+"/"+secure_token,
            data:{params:params},
            type:'post',       
            beforeSend:function(){
                me.hide(); //hide load more button on click
                $('.ajax-loading').show(); //show loading image
            },
            success:function(data){
                data = $.parseJSON(data);
                strDeal = data.strDeal;
                // console.log(data.all_deals);
                if(track_click <= total_pages) {
                    divContainer.append(strDeal)
                    me.show(); //bring back load more button
                    $('.ajax-loading').hide(); //hide loading image once data is received
                    // $("html, body").animate({scrollTop: me.offset().top}, 500); //scroll page to button element
                    if(track_click > total_pages-1){
                        me.hide();
                    }
                }
            },
            error: function(error){
            }
        });

        current_deal = limit * track_click;

        if(current_deal > total_deals){
            current_deal = total_deals;
        }

        $("h4#showNumber").text("Show "+current_deal+" of "+total_deals);

    });
    
    $("input.subLocation").click(function(){
        $("input.mainLocation").attr("checked",false);  
    });

    $("input.mainLocation").click(function(){
        $("input.subLocation").attr("checked",false);
    });
    
    $("input.subInterest").click(function(){
        $("input.mainInterest").attr("checked",false);  
    });

    $("input.mainInterest").click(function(){
        $("input.subInterest").attr("checked",false);
    });
    
    $("input.subService").click(function(){
        $("input.mainService").attr("checked",false);  
    });

    $("input.mainService").click(function(){
        $("input.subService").attr("checked",false);
    });
    
});

</script>