<?php

/********************************************************************************

File Name: index.ctp
Description:

Powered By: ABi Investment Group Co., Ltd,

Changed History:

    Date                Author              Description
    2014/07/10          Sim Chhayrambo      Initial Version

*********************************************************************************/
?>

<style type="text/css">
.bold{
    font-weight: bold;
}
article#article .article-entry h3{
    padding: 20px 0 4px;
    font-weight: bold;
}
</style>

<div class="container" style="margin-top: 30px;">
    <div class="col-sm-12">
        <article id="article">
            <div class="article-entry">
                <h2 style="text-align:center;">Privacy Policy</h2>
                <strong>ALL DEAL is operated by IDMS (Cambodia) Co., Ltd.</strong>
                <p>Your privacy is very important to us, below explains the information we collect from you, what we do with that information and our information security policy.</p>

                <h3>Your Information</h3>
                <p>When you sign up for our services we will ask you to provide contact information this will include name, phone number, home address, and e­mail. When you visit <strong>All Deal website and Applications</strong> we do record certain information using Google Analytics such as your location, browser version, ip address, screen resolution, operating system and flash support. This is so we can continue to provide the most convenient web site for all our visitors. Our website is protected by a Secure Sockets Layer (SSL) and verified. This helps prevent eavesdropping on our website, just look for the padlock.</p>

                <h3>How Your Information Is Used</h3>
                <p>We will use your personal identifiable information in the following ways. </p>
                <p>- To provide convenient and targeted support.</p>
                <p>- To contact you and announce special offers, general announcements and news.</p>
                <p>- To improve our service and the marketing of our service. This could include using your 
                demographic location to improve targeting of our website and customize your visit.</p>

                <h3>Cookies</h3>
                <p>A cookie is a small text file that is stored on a users computer for record­keeping purposes. We use cookies on this site. We do not link the information we store in cookies to any personally identifiable information you submit while on our site. We use both session ID cookies and persistent cookies. We use session cookies to make it easier for you to navigate our site. A session ID cookie expires when you close you browser. A persistent cookie remains on your hard drive for an extended period of time much like the cookie used for our affiliate program. You can remove persistent cookies by following directions provided in your Internet browsers 'help' file. We set a persistent cookie to store your passwords, so you do not have to enter it more than once. Persistent cookies also enable us to track and target the interests of our users to enhance the experience on our site. If you reject cookies, you may still use our site, but you will be asked to enter your username and password again every time you closed your browser or your session has timed out.</p>

                <h3>Log Files</h3>
                <p>As is true of most Web sites, we gather certain information automatically and store it in log files. This information includes Internet protocol (IP) addresses, browser type, internet service provider (ISP), referring/exit pages, operating system, date/time stamp, and clickstream data. We use this information, which does not identify individual users, to analyze trends, to administer the site, to track users movements around the site and to gather demographic information about our user base as a whole. We do not link this automatically­collected data to personally identifiable information. IP addresses are tied to personally identifiable information to prevent and block abusers to keep using our services such as members who violates our terms of use. We use a third­party tracking service that uses cookies and log files to track non-personally identifiable information about visitors to our site in the aggregate of usage and volume statistics to determine how our users navigate through our website and to know how many users are using our services demographically. We use the information you provide about yourself or others to complete the transaction for which it is intended. This may include administering a service such as registration, email, forum, etc., or contacting you.</p>

                <h3>Surveys</h3>
                <p>Upon using our free services, you may be requested to participate in our periodic internal surveys that are conducted by <strong>IDMS (Cambodia) Co., Ltd.</strong> to determine your personal interests. These surveys are strictly voluntary. The data collected through these surveys will only be used to improve our products and services.</p>

                <h3>Newsletter</h3>
                <p>We provide you the opportunity to 'opt-in' of our newsletter at any time. Our newsletter is sent periodically so it will certainly contain allot of useful information and any offers we may have.</p>

                <h3>Specific Terms and Conditions</h3>
                <p><strong>IDMS (Cambodia) Co., Ltd.</strong> considers email transmitted via our service to be the private correspondence between the sender and recipient. We will not monitor, edit or disclose the contents of a user`s private communications, except as required by law, to comply with legal process, if necessary to enforce the Service Agreement, to respond to claims that such contents violate the rights of third­parties, or to protect the rights or property of <strong>IDMS (Cambodia) Co., Ltd.</strong> By using the service you agree that technical processing of email communications is and may be required to send and receive messages, to conform to the technical requirements of connecting networks, to conform to the limitations of the Service, or to conform to other, similar technical requirements. By using the service you acknowledge and agree that <strong>IDMS (Cambodia) Co., Ltd.</strong> does not endorse the content of any user communications and are not responsible or liable for any unlawful, harassing, libelous, privacy invading, abusive, threatening, harmful, vulgar, obscene, tortuous, or otherwise objectionable content, or content that infringes or may infringe the intellectual property or other rights of another.</p>

                <h3>Legal Disclaimer</h3>
                <p>We reserve the right to disclose your personally identifiable information as required by law and when we believe that disclosure is necessary to protect our rights and/or to comply with a judicial proceeding, court order, or legal process served on our Web site.</p>

                <h3>Security</h3>
                <p>The security of your personal information is important to us. We follow generally accepted industry standards to protect the personal information submitted to us, both during transmission and once we receive it. No method of transmission over the Internet, or method of electronic storage, is 100% secure, however we have taken every step possible to make your information safe. This website is protected by Secure Sockets Layer (SSL) to protect your information. Therefore, while we strive to use commercially acceptable means to protect your personal information, we cannot guarantee its absolute security.</p>

                <h3>Links To Other Sites</h3>
                <p>This Web site contains links to other sites thatopts are not owned or controlled by <strong>IDMS (Cambodia) Co., Ltd.</strong> Please be aware that we, <strong>IDMS (Cambodia) Co., Ltd.</strong> is not responsible for the privacy practices of such other sites. We encourage you to be aware when you leave our site and to read the privacy statements of each and every Web site that collects personally identifiable information. This privacy statement applies only to information collected by this Web site.</p>

                <h3>Changes To This Privacy Statement</h3>
                P   If we decide to change our privacy policy, we will post those changes to this privacy statement, the home page, and other places we deem appropriate so that you are aware of what information we collect, how we use it, and under what circumstances, if any, we disclose it. We reserve the right to modify this privacy statement at any time, so please review it frequently. If we make material changes to this policy, we will notify you here, by email, or by means of a notice on our home page. For more information please do not hesitate to send your questions via our contact form.

            </div>
        </article>
    </div>
</div>