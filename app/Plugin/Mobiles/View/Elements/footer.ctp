<?php 

/********************************************************************************

File Name: footer.ctp
Description: UI for footer page

Powered By: ABi Investment Group Co., Ltd,

Changed History:

    Date                Author              Description
    2014/06/10          Sim Chhayrambo      Initial Version
    2014/06/20          Sim Chhayrambo      Add about us, contact and subscribe
    2014/06/24          Sim Chhayrambo      Add subscribe form
    2014/06/25          Sim Chhayrambo      Change UI for subscribe form
    2014-07-10          Sim Chhayrambo      Add Link for Term of Use and Privacy Policy
    2014-08-01          Sim Chhayrambo      Check subscribe form in IE9


*********************************************************************************/

?>

<!-- SUBSCRIBE -->
<div class="subscribe-form modal fade" id="subcribe-form" style="display:none;">
    
    <!-- <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <h1><?php echo __('Subcribe Form'); ?></h1>
    </div> -->

    <h1>
        <?php echo __('Subscribe to Newsletters'); ?>
        <button type="button" 
                class="close" 
                style="border-radius: 6px; padding: 5px 10px; font-size: 16px;" 
                data-dismiss="modal">×</button>
    </h1>
    
    <p style="font-weight: bold;"><?php echo __('All fields are required.'); ?></p>

    <div class="modal-body">
        <div class="box-content">
            <form id="frmSubscribe" method="post" action="" enctype="multipart/form-data">
                <div class="col-sm-7 clearfix">
                    <fieldset>
                        <ol>
                            <li>
                                <label class="required"><?php echo __('Name') ?></label>
                                <input type="text" id="subscriber-name" name="data[subscriber][name]" placeholder="<?php echo __('Name') ?>" required value="">
                            </li>
                            <li>
                                <label class="required"><?php echo __('Phone') ?></label>
                                <input type="text" id="subscriber-phone" name="data[subscriber][phone]" placeholder="<?php echo __('Phone') ?>" required>
                            </li>
                            <li>
                                <label class="required"><?php echo __('Email') ?></label>
                                <input type="email" id="subscriber-email" name="data[subscriber][email]" placeholder="<?php echo __('Email') ?>" required>
                            </li>
                        </ol>
                    </fieldset>
                    <button type="submit" 
                            class="btn btn-green pull-right" 
                            style="margin-right: 10px;" 
                            id="btn-subscribe">
                            <?php echo __('Subcribe') ?>
                    </button>
                    <div class="ajax-loading" style="width: 100%; text-align: right; display:none; padding-right: 10px;">
                        <img src="<?php echo $this->webroot . "img/bx_loader.gif" ?>">
                    </div>
                </div>

                <div class="col-sm-5" style="padding: 50px 0 50px 50px; border-left: 1px solid #ddd">

                    <div class="fb-loading">
                        <!-- Facebook like button -->
                        <fb:login-button scope="email,public_profile,publish_actions,user_birthday,user_location" onlogin="checkLoginState();">Subscribe with facebook</fb:login-button>

                        <div id="status"></div>
                        <!-- End -->
                    </div>
                    <div class="fb-ajax-loading" style="width: 100%; text-align: center; display:none;">
                        <img src="<?php echo $this->webroot . "img/bx_loader.gif" ?>">
                    </div>

                </div>
            </form>
        </div>
    </div>

</div>

<div class="divider">
    <div class="container">
        <div class="contact-info">
            <a href="#"><?php echo __('ABOUT US'); ?></a>
        </div>
    </div>
</div>

<div class="footer-section">
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <div class="about-us">
                    <div class="footer-logo">
                        <a href="<?php echo $this->Html->url(array('controller'=>'index', 'action'=>'index')); ?>">
                            <img src="<?php echo $this->webroot . "img/logo/AD_LOGO.png"?>" width="100" style="margin-left: 65px;">
                        </a>
                        
                    </div>
                    <p>All Deal is the easiest and most exciting ways to find the best deals in Phnom Penh.</p><br>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="contact-us">
                    <ul class="media-list">
                        <li class="media">
                            <a class="pull-left" href="#">
                                <i class="icon-location"></i>
                            </a>
                            <div class="media-body">
                                <h4 class="media-heading">Head Office Address</h4>
                                #25AE1 Suite 201, Street 302, Boeung Keng Kang 1, Phnom Penh, Cambodia
                            </div>
                        </li>
                        <li class="media">
                            <a class="pull-left" href="#">
                                <i class="icon-phone"></i>
                            </a>
                            <div class="media-body">
                                <h4 class="media-heading"><?php echo __('Phone Number'); ?></h4>
                                <table>
                                    <tr>
                                        <td>For Users</td>
                                        <td style="padding:0 5px;">:</td>
                                        <td>(+855) 98 799 699</td>
                                    </tr>
                                    <tr>
                                        <td>For Merchants</td>
                                        <td style="padding:0 5px;">:</td>
                                        <td>(+855) 99 799 699</td>
                                    </tr>
                                </table>
                            </div>
                        </li>
                        <li class="media">
                            <a class="pull-left" href="#">
                                <i class="icon-paper-plane"></i>
                            </a>
                            <div class="media-body">
                                <h4 class="media-heading"><?php echo __('Email Address'); ?></h4>
                                <a href="mailto:info@alldeal.asia" style="color:#c5c5c5;">info@alldeal.asia</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="contact-us">
                    <ul class="media-list">
                        <li class="media">
                            <div class="media-body" style="padding-bottom:20px;">
                                <!-- <div class="fb-page" data-href="https://www.facebook.com/alldealcambodia" data-tabs="timeline" data-height="300" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/alldealcambodia"><a href="https://www.facebook.com/alldealcambodia">All Deal</a></blockquote></div></div> -->
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="clearfix" style="padding-bottom:20px;"></div>
        </div>
    </div>
</div>

<div class="copyright-section">
    <div class="container">
        <div class="row">
            <div class="copyright">
                <span>&copy <?php echo __(' 2015 All Deal, All Rights Reserved.') ?></span>
                <span class='footer-copyright-links'>
                    <a href="<?php echo $this->Html->url(array('controller'=>'termOfUses', 'action'=>'index')) ?>" target="_blank"><?php echo __('Terms of Use'); ?></a>
                    <a href="<?php echo $this->Html->url(array('controller'=>'privacyPolicies', 'action'=>'index')) ?>" target="_blank"><?php echo __('Privacy Policy'); ?></a>
                </span>
            </div>
        </div>
    </div>
</div>

<!-- <script language="javascript" type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script src="https://fonts.googleapi.jquery.com/jquery-1.11.0.min.js"></script> -->
<script type="text/javascript">

$(function(){

    $('form#frmSubscribe label.required').append("<span class='red-star'> * </span>"); //add red star for all required fields
    $('input#subscriber-name').attr({'maxlength': 20}); //set maxlength for user name
    $('input#subscriber-phone').attr({'maxlength': 11}); //set maxlength for phone
    $('input#subscriber-email').attr({'maxlength': 50}); //set maxlength for email

    $("input#subscriber-name").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190, 32]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) || 
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a alphabet and stop the keypress
        if (e.keyCode < 65 || e.keyCode > 90) {
            e.preventDefault();
        }
    });

    $("input#subscriber-phone").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) || 
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

    function validateEmail(email) {
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        return emailReg.test( email );
    }

    $( "button#btn-subscribe" ).click(function() {

        // event.preventDefault();

        // 2014-08-01
        if (event.preventDefault) { event.preventDefault(); } else { event.returnValue = false; }

        var me = $(this);

        var subscriber_name = $("input#subscriber-name").val(),
            subscriber_phone = $("input#subscriber-phone").val(),
            subscriber_email = $("input#subscriber-email").val();

        subscriber_name = $.trim(subscriber_name);

        if(subscriber_name != "" && subscriber_phone != "" && subscriber_email != ""){

            if(validateEmail(subscriber_email)){

                // CHECK EXISTING EMAIL
                var url = '<?php echo $this->Html->url(array("controller"=>"newsLetters", "action"=>"check_existing_email")) ?>' + "/"  + subscriber_email ;

                $.ajax({
                    type: 'POST',
                    url: url,
                    data: { email : subscriber_email },
                    dataType: 'json',
                    
                    beforeSend: function(){

                        me.hide();
                        $('.ajax-loading').show(); //show loading image

                    },
                    success: function (data){

                        me.show();
                        $('.ajax-loading').hide(); //hide loading image once data is received

                        var result = data.result;

                        if(result){

                            $.msgBox({
                                title: "Failed",
                                content: "Email Address is Already Registered!",
                                type: "error",
                                buttons: [{ value: "Ok" }],
                            });
                            
                        }else{
                            
                            var url = '<?php echo $this->Html->url(array("controller"=>"newsLetters", "action"=>"subscribe")) ?>' + "/"  + subscriber_name + "/" + subscriber_phone + "/" + subscriber_email ;

                            $.ajax({
                                type: 'POST',
                                url: url,
                                data: { name:subscriber_name, phone:subscriber_phone, email:subscriber_email },
                                dataType: 'json',
                                
                                beforeSend: function(){
                                },
                                success: function (data){

                                    // console.log(data);

                                    var result = data.result;

                                    if(result){

                                        // $("input#subscriber-name").val("");
                                        // $("input#subscriber-phone").val("");
                                        // $("input#subscriber-email").val("");
                                        $("div#subcribe-form").hide();
                                        $("div.modal-backdrop").hide();

                                        $.msgBox({
                                            title:"Success",
                                            content:"You have successfully subscribed to our newsletter!"
                                        });
                                        
                                    }else{

                                        $.msgBox({
                                            title: "Failed",
                                            content: "Failed to add!",
                                            type: "error",
                                            buttons: [{ value: "Ok" }],
                                        });

                                    }

                                },

                                error: function(xhr, status, error) {
                                    var err = eval("(" + xhr.responseText + ")");
                                    console.log(err);
                                }

                            });
                        }

                    },

                    error: function(xhr, status, error) {
                        var err = eval("(" + xhr.responseText + ")");
                        console.log(err);
                    }

                });
                // END

            }else{

                $.msgBox({
                    title: "Failed",
                    content: "Invalid Email !",
                    type: "error",
                    buttons: [{ value: "Ok" }],
                });

            }

        }else{

            $.msgBox({
                title: "Failed",
                content: "All fields are required!",
                type: "error",
                buttons: [{ value: "Ok" }],
            });

        }

    });

    $("a.subcribe-form").click(function(){
        $("div#subcribe-form").show();
        $("div.modal-backdrop").show();
    });

    $('a#user-name-menu, #user-drop-menu').mouseover( function(){
        $('#user-drop-menu').show();
    })

    $('#user-drop-menu, a#user-name-menu').mouseleave( function(){
        $('#user-drop-menu').hide();
    });


});

</script>