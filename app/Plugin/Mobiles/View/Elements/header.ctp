<?php 

/********************************************************************************

File Name: header.ctp
Description: header file

Powered By: ABi Investment Group Co., Ltd,

Changed History:

    Date                Author              Description
    2014/06/10          Sim Chhayrambo      Initial Version
    2014/06/24          Sim Chhayrambo      Add Subcribe Form
    2014/06/30          Sim Chhayrambo      Add maximum characters to search box
    2014/07/09          Sim Chhayrambo      Check empty string
    2014/07/12          Sim Chhayrambo      Check Menu Deal
    2015/July/08        Rattana Phea        Change Menu and Deal order Listing

*********************************************************************************/

if(!empty($menuBusinessInfo)){
    $businessName = $menuBusinessInfo['Business']['business_name'];
    $businessLogo = $menuBusinessInfo['Business']['logo'];
    $businessSlug = $menuBusinessInfo['Business']['slug'];    
}

$cart = false;
$signin = false;
$signup = false;

if( $title_for_layout == "Shops" ){
    
    if( $this->params['action'] == 'index' ){
        $cart = true; 
    }elseif( $this->params['action'] == 'signin' ){
        $signin = true;
    }elseif( $this->params['action'] == 'signup' ){
        $signup = true;
    }

}

?>

<?php 

?>
<!--
  Below we include the Login Button social plugin. This button uses
  the JavaScript SDK to present a graphical Login button that triggers
  the FB.login() function when clicked.
-->

<!-- <fb:login-button scope="public_profile,email" onlogin="checkLoginState();">Subscribe with facebook
</fb:login-button>

<div id="status">
</div> -->

<!-- <div class="fb-login-button" data-max-rows="1" data-size="medium" data-show-faces="false" data-auto-logout-link="false">Login with facebook</div> -->

<div class="header center bg-header padding-20">
    <div class="search">
        <button type="button" id="toggle-sidebar" style="background-color:#8ec641"><i class="fa fa-bars color-white font-25"></i></button>
        <span class="color-white padding-left-20">Near By</span>
        <button type="button" id="btnSearch" class=" pull-right"><i class="fa fa-search pull-right color-white font-25"></i></button>
    </div>
    <nav class="main-menu navbar navbar-default">
        <div class="container-fluid">
            <?php //var_dump($type);exit(); ?>
           
            
            <ul class="no-padding-lr col-xs-12 no-margin nav navbar-nav">                      
                <li class="active col-xs-3 text-center padding-bottom-10">
                    <a href="<?php echo $this->Html->url(array('controller'=>'deals','action'=>'index'))?>?type=near-by"><i class="fa fa-map-marker font-25 color-white "></i></a>
                </li>
                <li class="col-xs-3 text-center padding-bottom-10">
                    <a href="<?php echo $this->Html->url(array('controller'=>'/','action'=>'/'))?>?type=deal-of-the-day"><i class="fa fa-star font-25 color-white"></i></a>
                </li>
                <li class="col-xs-3 text-center padding-bottom-10">
                    <a href="<?php echo $this->Html->url(array('controller'=>'/','action'=>'/')) ?>?type=last-minute"><i class="fa fa-clock-o font-25 color-white"></i></a>
                </li>
                <li class="col-xs-3 text-center padding-bottom-10">
                    <a href="<?php echo $this->Html->url(array('controller'=>'merchant','action'=>'index')) ?>"><i class="fa fa-home font-25 color-white"></i></a>
                </li>
            </ul>
        </div>
    </nav>
</div>
<div id="main-sidebar" class="main-sidebar main-sidebar-left">
    <div id="main-sidebar-wrapper" class="main-sidebar-wrapper">
        <!-- animate nav page -->
        <div class="wrap-setting">
            <div class="setting-head">
                <a href="#">Sign In</a>
                <span class="color-white">/</span>
                <a href="#">Sign Up</a>
                <div class="cost">
                    <span class="cost-price circle"><i class="fa fa-usd "></i></span>
                    <span class="cost-price padding-left-25">$0.0</span>
                </div>
            </div>
            <div class="setting-content">
                <div class="user-info">
                    <div class="row">
                        <div class="myCoupons">
                            <div class="col-xs-1"><a href=""><img src="<?php echo $this->webroot. 'img/mobile/hdpi_coupon.png' ?>" class="setting-image"></i></a></div>
                            <div class="col-xs-11 setting-menu"><a href="">My Coupons</a></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="myProfile">
                            <div class="col-xs-1"><a href=""><img src="<?php echo $this->webroot. 'img/mobile/hdpi_person.png' ?>" class="setting-image"></a></div>
                            <div class="col-xs-11 setting-menu"><a href="">My Profile</a></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="myFriend">
                            <div class="col-xs-1"><a href=""><img src="<?php echo $this->webroot. 'img/mobile/hdpi_add_person.png' ?>" class="setting-image"></a></div>
                            <div class="col-xs-11 setting-menu"><a href="">Refer Friends</a></div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="more-info full-height">
                    <span style="color:#BCBCBC;">More Info</span>
                    <div class="padding-bottom-20"></div>
                    <div class="row margin-top-30">
                        <div class="myCoupons">
                            <div class="col-xs-1"><a href=""><img src="<?php echo $this->webroot. 'img/mobile/hdpi_contact.png' ?>" class="setting-image"></a></div>
                            <div class="col-xs-11 setting-menu"><a href="">Contact Us</div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="myProfile">
                            <div class="col-xs-1"><a href=""><img src="<?php echo $this->webroot. 'img/mobile/hdpi_info.png' ?>" class="setting-image"></a></div>
                            <div class="col-xs-11 setting-menu"><a href="">About All Deal</a></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="myFriend">
                            <div class="col-xs-1"><a href=""><img src="<?php echo $this->webroot. 'img/mobile/Exit (log out)hdpi.png' ?>" class="setting-image"></a></div>
                            <div class="col-xs-11 setting-menu"><a href="">Sign Out</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end animate nav page -->
    </div>
</div>                
<script>
    $(document).ready(function() {
        $('#main-sidebar').simplerSidebar({
            opener: '#toggle-sidebar',
            animation: {
                easing: "easeOutQuint"
            },
            sidebar: {
                align: 'left',
                closingLinks: '.close-sb'
            }
        });
    });
   $("button").click(function(){
    $("#main-sidebar").width("75%");
    }); 
   
</script>



