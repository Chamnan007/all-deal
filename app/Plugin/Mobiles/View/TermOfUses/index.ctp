<?php

/********************************************************************************

File Name: index.ctp
Description:

Powered By: ABi Investment Group Co., Ltd,

Changed History:

    Date                Author              Description
    2014/07/10          Sim Chhayrambo      Initial Version

*********************************************************************************/
?>

<style type="text/css">
.bold{
    font-weight: bold;
}
article#article .article-entry h3{
    padding: 20px 0 4px;
    font-weight: bold;
}

article#article .article-entry p{
    margin-top: 10px;
}
</style>

<div class="container" style="margin-top: 30px;">
    <div class="col-sm-12">
        <article id="article">
            <div class="article-entry">
                <h2 style="text-align:center;">Terms of Use</h2>
                <p class="bold">All Deal Website and Applications are operated by <strong>IDMS (Cambodia) Co., Ltd.</strong></p>
                <p>These terms and conditions are between <strong>IDMS (Cambodia) Co., Ltd</strong>. and the person/persons who use All Deal website and applications. Please read carefully to ensure you understand our terms before using any of our products or services.</p>
                
                <h3>1. Definitions</h3>
                <p><strong>All Deal</strong>: refers to <strong>All Deal Website and Applications</strong> created and operated by <strong>IDMS (Cambodia) Co., Ltd</strong>.<br>
                <strong>Merchants</strong>: refer to business owners who use post business and deal information on <strong>All Deal Websites and Applications</strong>. <br>
                <strong>Users</strong>: refer to people who use <strong>All Deal Websites and Applications</strong> to view or purchase products and/or services. <br>
                <strong>Customers</strong>: refer people who use <strong>All Deal websites and applications</strong> including merchants and users.</p>

                <h3>2. Product and Service Quality</h3>
                <p><strong>IDMS (Cambodia) Co., Ltd.</strong> is not responsible for any problems related to the quality of the products and services listed in our website. In case of dissatisfaction with any products or services listed in our website and applications, users have to contact the merchants provide the products and services directly.</p>

                <h3>3. Disclaimer of Warranties</h3>
                <p><strong>IDMS (Cambodia) Co., Ltd.</strong> does not warrant or hold any responsibility on the products or services posted on All Deal Website and Applications.</p> 
                
                <h3>4. Terms</h3>
                <p><strong>Customers</strong> are responsible for monitoring their profile and information.</p>

                <h3>5. Coupon Cancellation and Refund</h3>
                <p>The coupon cancellation policy is set by the merchants who create the deal. Each merchant has their own cancellation policy. The users must read the coupon terms and conditions carefully before purchasing any deals on <strong>All Deal Website and Applications. IDMS (Cambodia) Co., Ltd.</strong> is not responsible for managing the cancellation of coupon purchase.</p>

                <h3>6. Server Abuse</h3>
                <p>Any attempt to undermine or cause harm to a server or other customer of <strong>IDMS (Cambodia) Co., Ltd.</strong> is strictly prohibited.</p>
                
                <p><strong>IDMS (Cambodia) Co., Ltd.</strong> will strongly react to any use or attempted use of an account or computer without the owner's authorization. Such attempts include "Internet scamming" (tricking other people into releasing their passwords), posting any images or text that is prohibited, password theft, security hole scanning, etc.</p>

                <p>Any unauthorized use of accounts or computers by <strong>Customer</strong>, whether or not the attacked account or computer belongs to <strong>IDMS (Cambodia) Co., Ltd.</strong>, will result in action against him or her. Possible actions include warnings, account suspension or cancelation, as well as civil or criminal legal action, depending on the seriousness of the attack.</p>

                <p>IMPORTANT NOTE – <strong>IDMS (Cambodia) Co., Ltd.</strong> has the right to discontinue service, or deny access to anyone who violates our policies or the terms and conditions shown below WITHOUT WARNING or PRIOR NOTICE. No refunds of fees paid will be made if account termination is due to violation of the terms outlined below.</p>

                <p><strong>Customer</strong> may not upload scripts (Rappidleach), pornographic content, political content, illegal content, copyright infringement, trademark infringement, link to or from insecure site or inappropriate site, cracks, software serial numbers, proxy­relaying, link farming (the act of or by use of scripts), link grinding, link­only sites, spamdexing and/or anything else determined by <strong>IDMS (Cambodia) Co., Ltd.</strong> to be unacceptable use of our services including abuse of server resources.</p>                
                
                <p>Further, accounts may also be terminated if it includes the following content or have links to the following content: Providing material that is grossly offensive to the Web community including blatant expressions of bigotry, racism, hatred, or profanity; promoting or providing instructional information about illegal activities; promoting physical harm or injury against any group or individual; displaying material containing obscene nudity or pornographic material (not applicable to managed dedicated servers); displaying material that exploits children under 18­years of age; acts of copyright infringement including offering pirated computer programs or links to such programs; information used to circumvent manufacturer­installed copy­protect devices, including serial or registration numbers for software programs, or any type of cracker utilities.</p>

                <h3>7. Customer Information</h3>
                <p><strong>Customer</strong> represents and warrant to <strong>IDMS (Cambodia) Co., Ltd.</strong> that the information he, she or it has provided and will provide to <strong>IDMS (Cambodia) Co., Ltd.</strong> for purposes of establishing and maintaining the service is accurate. If Customer is an individual,  he/she represents and warrants to <strong>IDMS (Cambodia) Co., Ltd.</strong> that he or she is at least 18 years of age. <strong>IDMS (Cambodia) Co., Ltd.</strong> may rely on the instructions of the person listed as the Primary Customer Contact with regard to Customer's account until Customer has provided a written notice changing the Primary Customer Contract.</p>

                <h3>8. TECHNICAL SUPPORT BOUNDARIES</h3>
                <p><strong>IDMS (Cambodia) Co., Ltd.</strong> provides technical support for <strong>Customers</strong> that encompasses within our area of expertise only. Such expertise includes assistance, troubleshooting, and debugging of our control panel interface, servers within our immediate responsibility and any other hosting related issues.</p>

                <h3>9. Indemnification</h3>
                <p><strong>Customer</strong> agrees to indemnify and hold harmless <strong>IDMS (Cambodia) Co., Ltd.</strong>, <strong>IDMS (Cambodia) Co., Ltd.</strong>’s affiliates, and each of their respective officers, directors, agents, and employees from and against any and all claims, demands, liabilities, obligations, losses, damages, penalties, fines, punitive damages, amounts in interest, expenses and disbursements of any kind and nature whatsoever (including reasonable attorneys fees) brought by a third party under any theory of legal liability arising out of or related to the actual or alleged use of <strong>Customer</strong>'s services in violation of applicable law by <strong>Customer</strong> or any person using <strong>Customer</strong>'s log on information, regardless of whether such person has been authorized to use the services by <strong>Customer</strong>. <br>
                   
                <p>YOU AGREE TO DEFEND,INDEMNIFY AND HOLD HARMLESS ALL DEAL AGAINST LIABILITIES ARISING OF:br<br>
                (1) ANY INJURY TO PERSON OR PROPERTY CAUSED BY ANY PRODUCTS/SERVICES SOLD OR OTHERWISE DISTRIBUTED IN CONNECTION WITH ALL DEAL’s WEBSITE AND APPLICATIONS.<br>
                (2) ANY MATERIAL SUPPLIED BY THE <strong>CUSTOMER</strong> INFRINGING OR ALLEGEDLY INFRINGING ON THE PROPRIETARY RIGHTS OF A THIRD PARTY.b<br>
                (3) COPYRIGHT INFRINGEMENT AND(4) ANY DEFECTIVE PRODUCTS SOLD TO <strong>CUSTOMER</strong> FROM ALL DEAL’s WEBSITE AND APPLICATIONS.</p>

                <h3>10. Suspension of Services/Termination</h3>
                <p><strong>Customer</strong> agrees that <strong>IDMS (Cambodia) Co., Ltd.</strong> may suspend services to <strong>Customer</strong> without notice and without liability if: (i) <strong>IDMS (Cambodia) Co., Ltd.</strong> reasonably believes that the services are being used in violation of the term of use; (ii) <strong>Customer</strong> fails to cooperate with any reasonable investigation of any suspected violation of the term of use; (iii) <strong>IDMS (Cambodia) Co., Ltd.</strong> reasonably believes that the suspension of service is necessary to protect its network or its other customers, or (iv) as requested by a law enforcement or regulatory agency.If you break our terms of service we hold the right to cancel any services. We will never cancel any services without trying our best to resolve the problem with you. However in extreme cases such as an account containing child pornography we hold the right to cancel services without any prior warning.</p>

                <h3>11. Request For Customer Information</h3>
                <p><strong>Customer</strong> agrees that <strong>IDMS (Cambodia) Co., Ltd.</strong> may, without notice to <strong>Customer</strong>, (i) report to the appropriate authorities any conduct by <strong>Customer</strong> or any of <strong>Customer</strong>'s <strong>customer</strong>s or end users that <strong>IDMS (Cambodia) Co., Ltd.</strong> believes violates applicable law, and (ii) provide any information that it has about <strong>Customer</strong> or any of its customers or end users in response to a formal or informal request from a law enforcement or regulatory agency or in response to a formal request in a civil action that on its face meets the requirements for such a request.</p>

                <h3>12. Changes To ALL DEAL’s Network</h3>
                <p>Upgrades and other changes in ALL DEAL’s network, including, but not limited to changes in its software, hardware, and service providers, may affect the display or operation of <strong>Customer</strong>'s hosted content and/or applications. <strong>IDMS (Cambodia) Co., Ltd.</strong> reserves the right to change its network in its commercially reasonable discretion, and <strong>IDMS (Cambodia) Co., Ltd.</strong> shall not be liable for any resulting harm to <strong>Customer</strong>. 13. NoticesNotices to <strong>IDMS (Cambodia) Co., Ltd.</strong> under the Agreement shall be given via electronic mail to the e­mail address posted for <strong>customer</strong> support. Notices to <strong>Customer</strong> shall be given via electronic mail to the individual listed as the Primary <strong>Customer</strong> Contact. Notices are deemed received on the day transmitted, or if that day is not a business day, on the first business day following the day delivered. <strong>Customer</strong> may change his, her or its notice address by a notice given in accordance with this Section.</p>

                <h3>14. Force Majored</h3>
                <p><strong>IDMS (Cambodia) Co., Ltd.</strong> shall not be in default of any obligation under the Agreement if the failure to perform the obligation is due to any event beyond <strong>IDMS (Cambodia) Co., Ltd.</strong>’s control, including, without limitation, significant failure of a portion of the power grid, significant failure of the Internet, natural disaster, war, riot, insurrection, epidemic, strikes or other organized labour action, terrorist activity, or other events of a magnitude or type for which precautions are not generally taken in the industry.</p>

                <h3>15. Governing/Law Disputes</h3>
                <p>The Agreement shall be governed by the laws of the Kingdom of Cambodia.</p>

                <h3>16. Copyright Files</h3>
                <p>All files stored on <strong>IDMS (Cambodia) Co., Ltd.</strong>’s servers/websites must be legally­owned and be accompanied with a valid license and/or copyright. This include and is not limiting to MP3, AVI, MID, MIDI, MPG, MPEG, MOV, EXE, ISO, JPG, PNG and so on. Should we discover any unlicensed and/or illegal files within <strong>Customer</strong>’s account, the files will be subjected to deletion.</p>

                <h3>17. INTELLECTUAL PROPERTY RIGHTS</h3>
                <p>Material accessible to you through <strong>IDMS (Cambodia) Co., Ltd.</strong>’s services may be subject to protection under Cambodia or other copyright laws, or laws protecting trademarks, trade secrets and proprietary information. Except when expressly permitted by the owner of such rights, <strong>Customer</strong> must not use ALL DEAL website and applications or its servers and network in a manner that would infringe, violate, dilute or misappropriate any such rights, with respect to any material that you access or receive through the ALL DEAL network. If <strong>Customer</strong> uses a domain name in connection with ALL DEAL or similar service, <strong>Customer</strong> must not use that domain name in violation of any trademark, service mark, or similar rights of any third party.</p>

                <h3>18. NETWORK SECURITY</h3>
                <p><strong>Customer</strong>s may not use the ALL DEAL network with an attempt to circumvent user authentication or security of any host, network, or account. This includes, but is not limited to, accessing data not intended for <strong>Customer</strong>, logging into a server or account <strong>Customer</strong> is not expressly authorized to access, password cracking, probing the security of other networks in search of weakness, or violation of any other organization's security policy. <strong>Customer</strong> may not attempt to interfere or deny service to any user, host, or network. This includes, but is not limited to, flooding, mail bombing, or other deliberate attempts to overload or crash a host or network. <br>
                <strong>IDMS (Cambodia) Co., Ltd.</strong> will cooperate fully with investigations for violations of systems or network security at other sites, including cooperating with law enforcement authorities in the investigation of suspected criminal violations. <strong>Customer</strong>s who violate system or network security may incur criminal or civil liability.</p>

                <h3>19. ELECTRONIC COMMERCE</h3>
                <strong>Customer</strong> will be solely responsible for the development, operation and maintenance of his/her online store and products along with all content and materials appearing online or on his/her products, including without limitation: <br>
                (a.) the accuracy and appropriateness of content and materials appearing within the store or related to his/her products, <br>
                (b.) ensuring that the content and materials appearing within the store or related to his/her products do not violate or infringe upon the rights of any third party, and <br>
                (c.) ensuring that the content and materials appearing within the store or related to his/her products are not libelous or otherwise illegal. <strong>Customer</strong> will be solely responsible for the final calculation and application of shipping and sales tax. <strong>Customer</strong> will also be solely responsible for accepting, processing, and filling any customer orders, and for handling any customer inquiries or complaints arising there from.

                <h3>20. STATIC & DYNAMIC CONTENT CACHING</h3>
                <p>YOU expressly <br>
                (i) grant to <strong>IDMS (Cambodia) Co., Ltd.</strong> a license to cache the entirety of YOUR content, including content supplied by third parties, hosted by <strong>IDMS (Cambodia) Co., Ltd.</strong> under this Agreement and <br>
                </b>(ii) agree that such caching is not an infringement of any of <strong>Customer</strong>’s intellectual property rights or any third party's intellectual property rights.</p>

                <h3>21. LAWFUL PURPOSE</h3>
                <p><strong>IDMS (Cambodia) Co., Ltd.</strong> reserves the right to refuse service to anyone. <strong>Customer</strong> may only use <strong>IDMS (Cambodia) Co., Ltd.</strong>’s service for lawful purposes and our services may not be used for illegal purposes or in support of illegal activities. We reserve the right to cooperate with legal authorities and/or injured third parties in the investigation of any suspected crime or civil wrongdoing. If anything is not legal in Cambodia or your country of residence, it is not permitted to reside on our servers or websites. Transmission, distribution or storage of any material in violation of any applicable law or regulation is prohibited. This includes but not limiting material protected by copyright, trademark, trade secret or other intellectual property right used without proper authorization, and material that is obscene, defamatory, constitutes an illegal threat, or violates export control laws. <br>
                <strong>IDMS (Cambodia) Co., Ltd.</strong> reserves the right to refuse service to anyone. <strong>Customer</strong> may only use <strong>IDMS (Cambodia) Co., Ltd.</strong>’s service for lawful purposes and our services may not be used for illegal purposes or in support of illegal activities. We reserve the right to cooperate with legal authorities and/or injured third parties in the investigation of any suspected crime or civil wrongdoing. If anything is not legal in Cambodia or your country of residence, it is not permitted to reside on our servers or websites. Transmission, distribution or storage of any material in violation of any applicable law or regulation is prohibited. This includes but not limiting material protected by copyright, trademark, trade secret or other intellectual property right used without proper authorization, and material that is obscene, defamatory, constitutes an illegal threat, or violates export control laws.</p>

                <h3>22. PRIVACY STATEMENT</h3>
                <p><strong>IDMS (Cambodia) Co., Ltd.</strong> follows the strict guidelines of our customer privacy statement. Please make sure you understand this statement fully.</p>

                <h3>23. CHANGE TO TERMS OF USE</h3>
                <p>We reserved the right to change this Terms of Use at any time without prior notice.</p>

            </div>
        </article>
    </div>
</div>