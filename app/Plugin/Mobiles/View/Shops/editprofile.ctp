<?php 

/********************************************************************************

File Name: index.ctp (Your Cart Shopping)
Description: for business registration form

Powered By: ABi Investment Group Co., Ltd,

Changed History:

    Date                Author              Description
    2015/04/06          Phea Rattana        Initial Version

*********************************************************************************/

$currentYear = date("Y");
$fromYear = $currentYear - 120;
$toYear = $currentYear;

$name   = "";
$gedner = "";
$dob_d  = "";
$dob_m  = "";
$dob_y  = "";

$province   = "";
$city       = "";
$sangkat_id = "";
$location_id = "";
$location   = "";
$street     = "";
$phone1     = "";
$phone2     = "";
$email      = "";

$current_password = "";

if( $user_info ){
    $name       = $user_info['full_name'];
    $gender     = $user_info['gender'];

    $dob        = $user_info['dob'];

    $dob_d      = date('d', strtotime($dob));
    $dob_m      = date('m', strtotime($dob));
    $dob_y      = date('Y', strtotime($dob));

    $province   = $user_info['province_code'];
    $city       = $user_info['city_code'];
    $location   = $user_info['location'];
    $location_id = $user_info['location'];
    $sangkat_id   = $user_info['sangkat_id'];
    $street     = $user_info['street'];

    $phone1     = $user_info['phone1'];
    $phone2     = $user_info['phone2'];
    $email      = $user_info['email'];

    $current_password = $user_info['password'];
}

?>

<style type="text/css">
.register-form label{
    width: 140px;
}
.alert-error{
    color: red;
    background: rgb(200, 54, 54); /* The Fallback */
    background: rgba(200, 54, 54, 0.5);
}
.alert-dismissable .close{
    top: -35px;
    right: -30px;
    color: #666;
}
</style>

<?php $selected_menu = "profile"; ?>

<div class="register-form clearfix">
    <form id="frmAdd" method="post" action="<?php echo $this->Html->url(array("action"=>"saveprofile")) ?>">
        <h1 style="text-transform: uppercase;"><?php echo __('Edit Profile'); ?></h1>
        
        <div class="col-sm-12 no-padding clearfix" style="min-height: 150px;">

            <?php include('account-nav.ctp') ?>
            <div class="col-sm-6 no-padding" style="border-right: 1px solid #DDD">
                
                <legend>General Information</legend>
                <ol>
                    <li>

                        <input type="hidden" class="input_email" id="id" name="Buyer[id]" required value="<?php echo $user_info['id']; ?>">

                        <label class="required"><?php echo __('Name') ?></label>
                        <input type="text" class="input_email" id="fullname" name="Buyer[full_name]" placeholder="<?php echo __('Name') ?>" required value="<?php echo $name; ?>" maxLength="50">
                    </li>

                    <li>
                        <label class="required"><?php echo __('Gender') ?></label>

                        <select name="Buyer[gender]" id="gender">
                            <?php foreach( $genders as $key => $val ){ 
                                    $selected = ($key == $gender )?" selected='selected'":"";
                            ?>
                                <option value="<?php echo $key ?>" <?php echo $selected ?>><?php echo $val ?></option>
                            <?php } ?>
                        </select>

                    </li>

                    <li>
                        <label class="required"><?php echo __('Date of Birth') ?></label>
                        <select style="width:100px;" id="dob-day" name="Buyer[dob][day]" required>
                            <option value=""><?php echo __('Day') ?></option>
                            <?php for ($i=1; $i < 32 ; $i++) { 

                                $selected = ($i == $dob_d )?" selected='selected'":"";
                                echo "<option value='".$i."' " . $selected . ">".$i."</option>";
                            } ?>
                        </select>
                        <select style="width:100px;" id="dob-month" name="Buyer[dob][month]" required>
                            <option value=""><?php echo __('Month') ?></option>
                            <?php foreach( $mons as $key => $value ){
                                $selected = ( $key == $dob_m )?" selected='selected'":"";

                                echo "<option value='".$key."'" . $selected . ">".$value."</option>";
                            } ?>
                        </select>
                        <select style="width:105px;" id="dob-year" name="Buyer[dob][year]" required>
                            <option value=""><?php echo __('Year') ?></option>

                            <?php for ( $i=$fromYear; $i <= $toYear ; $i++ ) { 
                                $selected = ($i == $dob_y )?" selected='selected'":"";
                                echo "<option value='".$i."'" . $selected . ">".$i."</option>";
                            } ?>

                        </select>
                    </li>

                    <li>
                        <label class="required"><?php echo __('Province/ City') ?></label>
                        <select name="Buyer[city_code]" id="city" required>
                            <option value=""><?php echo __('Select Province/ City') ?></option>
                            <?php foreach ($provinceList as $proKey => $proValue) {
                                $selected = ( $proValue['City']['city_code'] == $city )?" selected='selected'":"";

                                echo "<option value='".$proValue['City']['city_code']."'" . $selected . ">".$proValue['City']['city_name']."</option>";
                            } ?>
                        </select>
                    </li>
                    <li>
                        <label class="required"><?php echo __('Location/ Khan') ?></label>
                        <select name="Buyer[location]" id="location" required>
                            <option value="" data-id=""><?php echo __('Select Location') ?></option>
                        </select>
                        <input type="hidden" name="Buyer[location_id]" id="location-id">
                    </li>

                    <li>
                        <label><?php echo __('Sangkat') ?></label>
                        <select name="Buyer[sangkat_id]" id="sangkat-select">
                            <option value=""><?php echo __('Select Sangkat') ?></option>
                        </select>
                    </li>
                    <li>
                        <label><?php echo __('Street') ?></label>
                        <input type="text" id="street" name="Buyer[street]" placeholder="<?php echo __('Street') ?>" pattern=".{1,100}" required title="100 characters maximum" maxLength='100' value="<?php echo $street ?>">
                    </li>

                    <li>
                        <label class="required"><?php echo __('Phone 1') ?></label>
                        <input type="text" class="input_phone" id="phone-1" name="Buyer[phone1]" placeholder="<?php echo __('Phone 1') ?>" pattern=".{1,11}" required  maxLength="20" value="<?php echo $phone1 ?>">
                    </li>

                    <li>
                        <label><?php echo __('Phone 2') ?></label>
                        <input type="text" class="input_phone" id="phone-2" name="Buyer[phone2]" placeholder="<?php echo __('Phone 2') ?>" pattern=".{1,11}" maxLength="20" value="<?php echo $phone2 ?>">
                    </li>
                </ol>
            </div>
            
            <div class="col-sm-5" style="width: 478px">
                <legend>Sign In Information</legend>
                <ol>

                    <li>
                        <label class="required" style="width:150px;"><?php echo __('Email') ?></label>
                        <input type="email" class="input_email" id="email" name="Buyer[email]" placeholder="<?php echo __('Email') ?>" required maxLength='50' value="<?php echo $email ?>">
                    </li>
                
                    <li>
                        <label style="width:150px;"><?php echo __('New Password') ?></label>
                        <input type="password" name="Buyer[password]" class="input_password" id="password" placeholder="<?php echo __('Password') ?>" pattern=".{6,30}" required title="Password must contain 6-30 characters" >
                    </li>
                    <li>
                        <label style="width:150px;"><?php echo __('Confirm New password') ?></label>
                        <input type="password" name="Buyer[confirm_password]" class="input_password" id="confirm-password" placeholder="<?php echo __('Confirm Password') ?>" pattern=".{6,30}" required title="Password must contain 6-30 characters">
                    </li>

                    <li>
                        <label class="required" style="width:150px;"><?php echo __('Current Password') ?></label>
                        <input type="password" name="Buyer[current_password]" class="input_password" id="current-password" placeholder="<?php echo __('Current Password') ?>" pattern=".{6,30}" required title="Password must contain 6-30 characters" >
                    </li>

                </ol>
            </div>

            <div class="col-sm-12 no-padding clearfix" style="text-align:center;">
                <button type="submit" id="btn-save-change"
                    style="font-size: 12px"><?php echo __('SAVE CHANGES') ?></button>
                <div class="ajax-loading" style="width: 100%; text-align: center; display:none;">
                    <img src="<?php echo $this->webroot . "img/bx_loader.gif" ?>">
                </div>
            </div>             
        </div>
        
    </form>
</div>

<script type="text/javascript">
    function msieversion() {

            var ua = window.navigator.userAgent;
            var msie = ua.indexOf("MSIE ");
            var ieversion;

            if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer, return version number
                return parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)));
            else                 // If another browser, return 0
                return false;
    }

    var checkIEBrowser = msieversion();

    var location_code    = "<?php echo $location ?>";    
    var location_id      = "<?php echo $location_id ?>";    
    var sangkat_id       = "<?php echo $sangkat_id ?>";
   
    $("select#city").change( function (){
        var me = $(this);

        var locationSelect  = $("select#location");
        var sangkatSelect   = $('select#sangkat-select')
        var city_code       = me.val();

        var url = '<?php echo $this->Html->url(array("controller"=>"Locations", "action"=>"get_location_by_city_code")) ?>' + "/"  + city_code ;

        if( city_code != '' ){
            $.ajax({
                type: 'POST',
                url: url,
                data: { city_code : city_code },
                dataType: 'json',
                
                beforeSend: function(){
                    locationSelect.html("<option value=''>Loading...</option>");
                    sangkatSelect.html("<option value=''>Loading...</option>");
                },
                success: function (data){

                    var data = data.data;

                    locationSelect.html("<option value=''>Select location/Khan</option>");
                    sangkatSelect.html("<option value=''>Select Sangkat</option>");
                    
                    $.each( data, function (ind, val ){
                        var loc = val.Location;

                        var selected = "";
                        if( loc.location_name == location_code || loc.id == location_id ){
                            selected = " selected='selected'";
                        }

                        locationSelect.append("<option value='" + loc.location_name + "' data-id='"+ loc.id +"'" + selected + ">" + loc.location_name + "</option>" )
                    });

                    $('select#location').change();
                },
                error: function ( err ){
                    console.log(err);
                }

            });

        }else{

            locationSelect.html("<option value=''>select an option</option>");
            sangkatSelect.html("<option value=''>select an option</option>");

        }

    }).trigger('change');


    $('select#location').change( function(){

        var me      = $(this);
        
        var optoin_selected = $('select#location option:selected');
        var location_id     = optoin_selected.attr('data-id');
        var sangkatSelect   = $('select#sangkat-select');

        $('input#location-id').val(location_id);

        var url = '<?php echo $this->webroot ?>' + 'sangkats/getSangkatByLocation/' + location_id ;

        if( location_id != '' ){
            $.ajax({
                type: 'POST',
                url: url,
                data: { location_id : location_id },
                dataType: 'json',
                
                beforeSend: function(){
                    sangkatSelect.html('<option>Loading...</option>');
                },

                success: function (result){
                    var data = result.data;
                    sangkatSelect.html("<option value=''>select an option</option>");

                    $.each( data, function (ind, val ){
                      var sk = val.Sangkat;
                        var selected = "";
                        if( sangkat_id == sk.id ){
                            selected = " selected='selected'";
                        }
                        sangkatSelect.append("<option value='" + sk.id + "'"+ selected +">" + sk.name + "</option>" );
                    });

                },

                error: function(xhr, status, error) {
                  var err = eval("(" + xhr.responseText + ")");
                    console.log(err);
                }

            });

        }else{
            sangkatSelect.html("<option value=''>select an option</option>");
        }
    })


    $('form#frmAdd label.required').append("<span class='red-star'> * </span>");

    $("input.input_phone").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) || 
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

    $('button#btn-save-change').click ( function(){

        event.preventDefault();
        var msg = new Array();

        var btn         = $(this),
            loading     = $('div.ajax-loading');

        btn.hide();
        loading.show();

        var id          = $('input#id').val(),
            name        = $('input#fullname').val().trim(),
            dob_day     = $('select#dob-day').val(),
            dob_month   = $('select#dob-month').val(),
            dob_year    = $('select#dob-year').val(),
            province    = $('select#province').val(),
            location    = $('select#location').val(),
            phone1      = $('input#phone-1').val().trim(),
            email       = $('input#email').val().trim(),
            password    = $('input#password').val(),
            cnf_password= $('input#confirm-password').val(),

            current_password = $('input#current-password').val();

        var old_password = "<?php echo $current_password ?>";

        if( name == "" ){ 
            msg.push('Please enter name !'); 
        }

        if( dob_day == "" || dob_month == "" || dob_year == "" ){  
            msg.push('Please select date of birth !') 
        }

        if( province == "" ) {
            msg.push('Please select province !');
        }

        if( location == ""){
            msg.push('Please select location !');
        }

        if( phone1 == ''){
            msg.push('Please enter phone number !');
        }

        if( email == ""){
            msg.push('Please enter email address !');
        }else{
            if( !validateEmail(email) ){
                msg.push('Invalid Email Adress !');  
            }
        }

        if( password != "" ){
            if( password.length < 6 ){
                msg.push('Password must be 6 characters at least !');
            }else{
                if( cnf_password == "" ){
                    msg.push('Please enter confirm password !');
                }

                if( password != "" && cnf_password != "" && password != cnf_password ){
                    msg.push('Password & Confirm password does not match !');
                }   
            }
        }

        if( msg.length > 0 ){
            var msg_error = "";
            $.each( msg, function(ind,val){
                msg_error += "<br>" + val;
            })

            loading.hide();
            btn.show();

            $.msgBox({
                title: "Failed",
                content: msg_error,
                type: "error",
                buttons: [{ value: "Ok" }],
            });

        }else{

                
            var url = '<?php echo $this->Html->url(array("controller"=>"shops", "action"=>"checkDuplicateEmail")) ?>' + "/"  + email + "/" + id ;

            $.ajax({
                type: 'POST',
                url: url,
                data: { email : email },
                dataType: 'json',

                success: function( data ){
                    if( data.status == true ){
                       
                        if( current_password == "" ){
                            loading.hide();
                            btn.show();

                            $.msgBox({
                                title: "Failed",
                                content: "Please enter your current password to save change !",
                                type: "error",
                                buttons: [{ value: "Ok" }],
                            }); 
                            return false;
                        }else{
                            var hash_url = '<?php echo $this->Html->url(array("controller"=>"shops", "action"=>"hashPassword")) ?>' + "/"  + current_password ;

                            var old_password = "<?php echo $current_password ?>";

                            $.ajax({
                                type: 'POST',
                                url: hash_url,
                                data: { password : current_password },
                                dataType: 'json',

                                success: function( data ){
                                    if(data.password == old_password ){
                                       $('form#frmAdd').submit(); 
                                    }else{
                                        loading.hide();
                                        btn.show();

                                        $.msgBox({
                                            title: "Failed",
                                            content: "Your current password is not correct !",
                                            type: "error",
                                            buttons: [{ value: "Ok" }],
                                        }); 
                                    }
                                }

                            });

                        }

                        
                    }else{

                        loading.hide();
                        btn.show();

                        $.msgBox({
                            title: "Failed",
                            content: "This email is already registered. Please try with another email address !",
                            type: "error",
                            buttons: [{ value: "Ok" }],
                        }); 
                    }
                },

                error: function(err, message ){
                    console.log(message);
                }
            });

            

        }
        

        return false;
    })

    function validateEmail(email) {
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        return emailReg.test( email );
    }
</script>
