<ul class="account-menu">
    <li <?php echo(@$selected_menu == 'account')?' class="active"':'' ?>>
    	<a href="<?php echo $this->Html->url(array("action"=>"myaccount")) ?>">Account</a>
    </li>
    <li <?php echo(@$selected_menu == 'profile')?' class="active"':'' ?>>
    	<a href="<?php echo $this->Html->url(array("action"=>"myprofile")) ?>">Profile</a>
    </li>
    <li <?php echo(@$selected_menu == 'refer')?' class="active"':'' ?>>
    	<a href="<?php echo $this->Html->url(array("action"=>"refer")) ?>">Refer Friends</a>
    </li>
</ul> 

<div class="clearfix" style="padding-top: 50px;"></div>
