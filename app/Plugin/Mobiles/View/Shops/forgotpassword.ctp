<?php 

/********************************************************************************

File Name: index.ctp (Your Cart Shopping)
Description: for business registration form

Powered By: ABi Investment Group Co., Ltd,

Changed History:

    Date                Author              Description
    2015/04/06          Phea Rattana        Initial Version

*********************************************************************************/
?>

<style type="text/css">
.register-form label{
    width: 140px;
}
.alert-error{
    color: red;
    background: rgb(200, 54, 54); /* The Fallback */
    background: rgba(200, 54, 54, 0.5);
}
.alert-dismissable .close{
    top: -35px;
    right: -30px;
    color: #666;
}
</style>

<div class="register-form clearfix">
    <form id="frmAdd" method="post" action="<?php echo $this->Html->url(array('controller'=>'shops', 'action'=>'forgotpassword')) ?>"enctype="multipart/form-data">
        <h1 style="text-transform: uppercase;"><?php echo __('Reset Your Password'); ?></h1>
        
        <div class="col-sm-12 no-padding clearfix" style="min-height: 150px;">
            <div class="col-sm-4 no-padding" style="min-height: 100px;">
                <div class="clearfix" style="padding-top: 20px;"></div>
                <p>
                    Not yet a member ?  
                    <a href="<?php echo $this->Html->url(array('controller'=>'shops', 'action'=>'signup')) ?>">SIGN UP NOW !</a>
                </p>
                <p>
                    Already a member ?  
                    <a href="<?php echo $this->Html->url(array('controller'=>'shops', 'action'=>'signin')) ?>">SIGN IN NOW !</a>
                </p>
            </div>

            <div class="col-sm-8" style="border-left: 1px solid #DDD">
                <legend>Reset Your Password</legend>
                <ol>
                    <li>
                        <label class="required"><?php echo __('Email Address') ?></label>
                        <input type="email" class="input_email" id="" name="Buyer[email]" placeholder="<?php echo __('Email') ?>" required value="">
                    </li>
                    
                </ol>

                <div class="col-sm-12 no-padding clearfix" style="text-align:center;">
                    <button type="submit" id="btn-register"><?php echo __('RESET') ?></button>
                    <div class="ajax-loading" style="width: 100%; text-align: center; display:none;">
                        <img src="<?php echo $this->webroot . "img/bx_loader.gif" ?>">
                    </div>
                </div>

            </div>
           
        </div>
        
    </form>
</div>

<script type="text/javascript">

</script>
