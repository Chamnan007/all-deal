<?php 
/********************************************************************************

File Name: index.ctp (Your Cart Shopping)
Description: for business registration form

Powered By: ABi Investment Group Co., Ltd,

Changed History:

    Date                Author              Description
    2015/04/06          Phea Rattana        Initial Version

*********************************************************************************/

?>

<style type="text/css">
    .register-form label{
        width: 140px;
    }
    .alert-error{
        color: red;
        background: rgb(200, 54, 54); /* The Fallback */
        background: rgba(200, 54, 54, 0.5);
    }
    .alert-dismissable .close{
        top: -35px;
        right: -30px;
        color: #666;
    }

    #history-list td{
        font-size: 15px !important;
    }

    #history-list td{
        padding-left: 10px;
    }

    .center{
        text-align: center !important;
    }

    .right{
        text-align: right !important;
    }

    #history-list input[type='text']{
        border: 1px solid #CCC !important;
        height: 25px !important;
        border-radius: 0px !important;
    }

    #history-list input[type='text']:focus{
        border: 1px solid #8dc63f  !important;
    }

    #history-list tr:hover{
        background: none !important;
    }

    .total{
        background: rgba(169, 228, 123, 0.35);
        width: 100%;
        padding: 20px 0;
        overflow: hidden;
    }

    .total .title{
        width: 80%;
        text-align: right;
        font-size: 20px;
        float: left;
    }

    .total .grand-total{
        float: right;
        font-size: 20px;
        padding-right: 50px;
    }

    .remove-item{
        float: right;
        width: 18px;
        height: 18px;
        background: url(img/remove.png);
        background-size: 18px 18px;
        cursor: pointer;
    }
    .remove-item:hover{
        background: url(img/remove-hover.png);
        background-size: 18px 18px;
    }
    

    .my-btn{
        border: 1px solid #8dc63f ;
        color: #8dc63f  !important;
        font-weight: bold;
        background: white;
    }

    .my-btn:hover{
        cursor: pointer;
        color: white !important;
        background: #8dc63f  !important;
    }
    .my-btn-active{
        color: white !important;
        background: #8dc63f  !important;
    }

    .btn-payment{
        width: 150px;
        height: 50px;
        border: 1px solid #CCC ;
        float: left;
        margin-right: 5px;

        -webkit-border-radius: 5px;
        -moz-border-radius: 5px;
        border-radius: 5px;

    }

    .btn-payment img{
        width: 100%;
        height: 100%;
    }

    .btn-payment:hover{
        border: 1px solid #8dc63f  !important;

        -webkit-box-shadow: 3px 6px 42px -9px rgba(173,173,173,1);
        -moz-box-shadow: 3px 6px 42px -9px rgba(173,173,173,1);
        box-shadow: 3px 6px 42px -9px rgba(173,173,173,1);

    }

</style>

<div class="register-form clearfix">
    <form   id="frmCheckOut" method="post" 
            action="<?php echo $this->Html->url(array("action"=>"checkout")) ?>" 
            enctype="multipart/form-data">
        <h1 style="text-transform: uppercase;"><?php echo __('Your Cart'); ?></h1>
        
        <div class="col-sm-12 no-padding clearfix" style="min-height: 150px;">


            <?php
                $grand_total = 0;
                $purchase_description = array();

                if( count($items) == 0){
                    echo "<h2>Uh oh... Your cart is empty!</h2>";
                }else{
            ?>

                <a href="#">
                    <button type="button" 
                            class="clear-cart-btn" 
                            style=" font-size:12px; 
                                    text-transform: none; 
                                    margin-top:0px;
                                    float:right; width:120px;">
                        <?php echo __('Clear Cart') ?>
                    </button>
                </a>

                <input type="hidden" name="token" value="<?php echo $token ?>">

                <table style="margin-top: 10px; width:100%" id="history-list">
                    <tr>
                        <th style="padding-left: 10px;" colspan="2">Item Description</th>
                        <th class="center" width="100px;">Unit Price</th>
                        <th width="30px"></th>
                        <th class="center" width="100px">QTY</th>
                        <th width="150px" style="text-align:right">Subtotal</th>
                    </tr>

                    <?php foreach( $items as $key => $val ){ 

                            $item_id        = $val['item_id'];
                            $item_qty       = $val['qty'];
                            $item_price     = $val['unit_price'];

                            $amount = $item_price * $item_qty;
                            $grand_total += $amount;

                            $detail = $val['ItemDetail'];

                            $thumb_img  = str_replace('/menus/', '/menus/thumbs/', $detail['image']);

                            $purchase_description[] = $detail['title'];

                        ?>
                        <tr class="item">
                            <input type="hidden" name="deal_id[]" value="<?php echo $val['deal_id'] ?>" />
                            <input type="hidden" name="item_id[]" value="<?php echo $val['item_id'] ?>" />
                            <input type="hidden" name="unit_price[]" value="<?php echo $item_price ?>" />

                            <td width="100px;">
                                <img src="<?php echo $this->webroot . $thumb_img ?>"
                                     style="max-width: 100px; max-height:100px; border: 1px solid #DDD"   >
                            </td>
                            <td>
                                <strong><?php echo $detail['title'] ?></strong><br/>
                            </td>
                            <td class="center item-price" data-val="<?php echo $item_price ?>">$ <?php echo number_format($item_price, 2) ?></td>
                            <td class="center">x</td>
                            <td class="center">
                                <input type="text" 
                                    class="item-qty number-only" 
                                    style="width:50px; text-align:center" 
                                    value="<?php echo $item_qty  ?>" maxLength="1" 
                                    data-available-qty="<?php echo $detail['DealItemLink']['available_qty'] ?>"
                                    readonly="readonly"
                                    name="qty[]" />
                            </td>

                            <td class="right item-amount" data-val="<?php echo $amount ?>">$ <?php echo number_format($amount, 2) ?></td>
                        </tr>

                    <?php } ?>

                </table>
                <div class="clearfix" style="padding-top:20px;"></div>

                <div class="total">
                    <div class="title">
                        Total Amount
                    </div>
                    <div class="grand-total" data-val="<?php echo $grand_total ?>" >
                        <input type="hidden" name="total_amount" value="<?php echo $grand_total ?>"/>
                        <span style="font-weight:bold;" >$ <?php echo number_format( $grand_total, 2 ) ?></span>
                    </div>
                </div>

                <div class="clearfix" style="padding-top:20px;"></div>
                <span style="color:red;">* </span><i>By clicking on "Check Out" button, you agreed with 
                 <a href="<?php echo $this->Html->url(array('controller'=>'termOfUses', 'action'=>'index')) ?>" target="_blank"><?php echo __('Terms of Use'); ?></a> and 
                    <a href="<?php echo $this->Html->url(array('controller'=>'privacyPolicies', 'action'=>'index')) ?>" target="_blank"><?php echo __('Privacy Policy'); ?></a> of our deals and services.</i>

                <div class="clearfix" style="padding-top:20px;"></div>
                <h2>Choose Payment Method</h2><hr>

                <a  href="#"
                    id="alldeal-credit-btn"
                    title="AllDeal Credit">
                    <div class="btn-payment">
                        <img src="<?php echo $this->webroot . 'img/payment-buttons/all-deal.png' ?>" alt="">
                    </div>
                </a>

                <a  href="#"
                    id="instore-payment"
                    title="In-Store">
                    <div class="btn-payment">
                        <img src="<?php echo $this->webroot . 'img/payment-buttons/in-store.png' ?>" alt="InStore Payment">
                    </div>
                </a>

                <a  href="#"
                    id="paypal-btn"
                    title="Credit Card" >
                    <div class="btn-payment">
                        <img src="<?php echo $this->webroot . 'img/payment-buttons/paypal.png' ?>" alt="">
                    </div>
                </a>

                <a  href="<?php echo $this->Html->url(array("action"=>"paygo")) ?>?ref=<?php echo $token ?>"
                    id="paygo-btn"
                    title="Pay & Go" >
                    <div class="btn-payment">
                        <img src="<?php echo $this->webroot . 'img/payment-buttons/paygo.png' ?>" alt="">
                    </div>
                </a>

                <div class="clearfix" style="padding-bottom:50px;"></div>

            <?php } ?>
            
        </div>
        
    </form>
    
    <div class="paypal-pay-block">        
        <!-- Live Button -->
        <form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" style="display:none;" id="paypal-form">
            <input type="hidden" name="cmd" value="_xclick">
            <input type="hidden" name="business" value="rattana.phea-facilitator@abi-technologies.com">

            <input type="hidden" name="amount" value="<?php echo $grand_total; ?>">
            <input type="hidden" name="item_name" value="<?php echo implode($purchase_description, ", "); ?>">
            <input type="hidden" name="currency_code" value="USD">

            <input  type="hidden" name="return" 
                    value="http://www.alldeal.idms.asia/success?ref=<?php echo $token ?>&m_ref=<?php echo sha1($token) ?>">

            <input  type="hidden" name="cancel_return"
                    value="http://www.alldeal.idms.asia/cart?ref=<?php echo $token ?>&m_ref=<?php echo sha1($token) ?>">

            <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!" style="width:150px; height:50px;">
            <img alt="" border="0" src="https://www.paypal.com/en_US/i/scr/pixel.gif" width="1" height="1">
        </form>

    </div>

</div>

<div id="blur-background"></div>

<div class="confirm-modal" id="clear-cart-modal">
    
    <h4 style="float:left; font-size: 20px;" id="item-title">Are you sure ?</h4>
    <div id="close" title="Close">X</div>
    <div class="clearfix"></div>
    <hr>

    <form action="<?php echo $this->Html->url(array('controller'=>'shops', 'action'=>'clearcart', $ref )) ?>" method="POST" >
        <div id="content-show" style="padding-top:10px;"> 
            <p class="msg">Are you sure you want to clear your carts?</p>
        </div>

        <div class="footer-action" style="text-align:right">
            <input type="submit" class="btn btn-primary" value="Yes" />
            <input type="button" class="close-btn btn btn-danger" value="Cancel" />
        </div>
    </form>

</div>

<div class="confirm-modal" id="remove-from-cart-modal">
    
    <h4 style="float:left; font-size: 20px;" id="item-title">Are you sure ?</h4>
    <div id="close" title="Close">X</div>
    <div class="clearfix"></div>
    <hr>

    <form action="<?php echo $this->Html->url(array('controller'=>'shops', 'action'=>'removefromcart')) ?>" method="POST" >
        <div id="content-show" style="padding-top:10px;"> 
            <p class="msg">Are you sure you want to delete this item from your cart?</p>

            <input type="hidden" name="deal-id" value="" id="deal-id" />
            <input type="hidden" name="item-id" value="" id="item-id" />

        </div>

        <div class="footer-action" style="text-align:right">
            <input type="submit" class="btn btn-primary" value="Yes" id="yes-remove"/>
            <input type="button" class="close-btn btn btn-danger" value="Cancel" />
        </div>
    </form>

</div>

<div id="item-list-modal" style="height:520px;">
    
    <h4 style="float:left; font-size: 22px;" id="item-title">Purchase Confirmation</h4>
    <div id="close" title="Close">X</div>
    <div class="clearfix" style="border-bottom:1px solid #EEE; padding-bottom:10px;"></div>
    <!-- <hr> -->

    <div id="content-show" style="padding-top:10px; padding-bottom:20px; ">
        <div id="item-content" style=" height:400px;max-height:400px; padding-bottom:20px;">
            <table style="width:100%">
                <tr>
                    <td width="120px">User ID</td>
                    <td width="20px;">:</td>
                    <td><strong><?php echo strtoupper($userInfo['Buyer']['buyer_code']) ?></strong></td>
                </tr>

                <tr>
                    <td>User Name</td>
                    <td>:</td>
                    <td><strong><?php echo $userInfo['Buyer']['full_name'] ?></strong></td>
                </tr>

                <tr id="available-credit">
                    <td>Available Credit</td>
                    <td>:</td>
                    <td style="color: #8dc63f !important; font-size:18px;"><strong><?php echo "$ " . number_format($balance, 2) ?></strong></td>
                </tr>
            </table>
            
            <div class="clearfix" style="padding-bottom:20px;"></div>
            <h3><strong>Purchase includes:</strong></h3>

            <table style="margin-top: 10px; width:100%" id="history-list">
                <tr>
                    <th width="50px;">N<sup>o</sup></th>
                    <th>Item Description</th>
                    <th class="center" width="100px;">Unit Price</th>
                    <th width="30px"></th>
                    <th class="center" width="100px">QTY</th>
                    <th width="150px" style="text-align:right">Subtotal</th>
                </tr>

                <?php 
                    $grand_total = 0;
                    
                    foreach( $items as $key => $val ){ 

                        $item_id        = $val['item_id'];
                        $item_qty       = $val['qty'];
                        $item_price     = $val['unit_price'];

                        $amount = $item_price * $item_qty;
                        $grand_total += $amount;

                        $detail = $val['ItemDetail'];

                        $thumb_img  = str_replace('/menus/', '/menus/thumbs/', $detail['image']);

                        $purchase_description[] = $detail['title'];

                    ?>
                    <tr class="item">
                        <td><?php echo $key + 1 ?></td>
                        <td><?php echo $detail['title'] ?></td>
                        <td class="center item-price">$ <?php echo number_format($item_price, 2) ?></td>
                        <td class="center">x</td>
                        <td class="center"><?php echo $item_qty ?></td>
                        <td class="right item-amount" style="padding-right:20px;" data-val="<?php echo $amount ?>">$ <?php echo number_format($amount, 2) ?></td>
                    </tr>

                <?php } ?>

            </table>
            <div class="total" style="maring-top:10px;">
                <div class="title">
                    Total Amount
                </div>
                <div class="grand-total" style=" padding-right: 20px !important;" >
                    <span style="font-weight:bold;" >$ <?php echo number_format( $grand_total, 2 ) ?></span>
                </div>
            </div>
        </div>
    </div>


    <input type="button" class="close-btn btn" value="Close"  style="float:right; font-size:18px; margin-left:10px;"/>

    <input  type="button" class="btn my-btn my-btn-active" value="Check Out" 
            id="checkout-btn" data-type="2"
            style="float:right; font-size:18px; width:150px;"/>

    <input  type="button" class="btn my-btn my-btn-active" value="Check Out" 
            id="instore-checkout-btn" data-type="5"
            style="float:right; font-size:18px; width:150px; display:none;"/>


</div>

<div class="confirm-modal" id="message-warning-modal">
    
    <h4 style="float:left; font-size: 20px;" id="item-title">Warning</h4>
    <div id="close" title="Close">X</div>
    <div class="clearfix"></div>
    <hr>

    <form action="<?php echo $this->Html->url(array('controller'=>'shops', 'action'=>'removefromcart')) ?>" method="POST" >
        <div id="content-show" style="padding-top:10px;"> 
            
        </div>

        <div class="footer-action" style="text-align:right">
            <input type="button" class="close-btn btn btn-danger" value="Close" />
        </div>
    </form>

</div>


<script type="text/javascript">
    
    $(".number-only").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) || 
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

    function calculateGrandTotal(){

        var items = $('table#history-list').find('tr.item');

        var grand_total = 0;
        $.each( items, function(ind, val){
            var amount = $(val).find('.item-amount').attr('data-val');

            grand_total = parseFloat(grand_total) + parseFloat(amount);
        })

        return grand_total;
    }

   
    $('.clear-cart-btn').click(function(event){
        event.preventDefault();

        $('#clear-cart-modal').animate({'top':'60px'});
        $('#clear-cart-modal').animate({'top':'50px'});
        $('#blur-background').show();

    });

    var span_error = $('span#msg-error');
    span_error.hide();

    var loading = $('div.ajax-loading');
    loading.hide();

    $('a#paypal-btn').click( function(event){

        event.preventDefault();
        $('form#paypal-form').submit();

    });

    $('a#alldeal-credit').click( function(event){

        span_error.hide();
        loading.hide();

        event.preventDefault();
        var me      = $(this);
        var balance = parseFloat(me.closest('.confirm-modal').find('td#amount-balance').attr('data-val'));
        var total   = parseFloat(me.closest('.confirm-modal').find('td#amount-to-pay').attr('data-val'));

        var msg = "";

        if( balance < total ){
            msg = "All-Deal credit is not enough !";
            span_error.text(msg);

            loading.show();

            setTimeout( function(){                    
                span_error.show();
                loading.hide();

                setTimeout( function(){
                    span_error.text('').fadeOut();
                }, 3000);  
            }, 1000)
        }

    });

    $(document).keydown(function(e) {
        
        // ESCAPE key pressed
        if (e.keyCode == 27) {
            $('.confirm-modal').animate({'top':'-200%'});

            $('div#item-list-modal').animate({'top':'60px'});
            $('div#item-list-modal').animate({'top':'-200%'});

            $('#blur-background').hide();
            document.onmousewheel = start;

        }

    });

    $('#close, .close-btn').click( function(){
        $(this).closest('.confirm-modal').animate({'top':'60px'});
        $(this).closest('.confirm-modal').animate({'top':'-200%'});

        $('div#item-list-modal').animate({'top':'60px'});
        $('div#item-list-modal').animate({'top':'-200%'});

        $('#blur-background').hide();

        document.onmousewheel = start;
    });

    function stop(){
        return false;
    }

    function start(){
        return true;
    }


    $('form#frmCheckOut label.required').append("<span class='red-star'> * </span>");

    var block_credit_card       = $('.paypal-pay-block');
    var block_alldeal_credit    = $('#alldeal-credit-block');

    var alldeal_credit_btn      = $('#alldeal-credit-btn');
    var credit_card_btn         = $('a#paypal-btn');

    block_alldeal_credit.show();

    $('a#alldeal-credit-btn').click( function(event){
        event.preventDefault();

        $('input#checkout-btn').attr('data-type', 2).show();
        $('input#instore-checkout-btn').hide();

        var confirm_modal = $('div#item-list-modal');

        confirm_modal.find('tr#available-credit').show();

        confirm_modal.show();
        confirm_modal.animate({'top':'60px'});
        confirm_modal.animate({'top':'50px'});
        $('#blur-background').show();

    });

    $('a#instore-payment').click( function( event ){
        event.preventDefault();

        $('input#checkout-btn').hide();
        $('input#instore-checkout-btn').attr('data-type', 5).show();

        var confirm_modal = $('div#item-list-modal');

        confirm_modal.show();

        confirm_modal.find('tr#available-credit').hide();
        confirm_modal.animate({'top':'60px'});
        confirm_modal.animate({'top':'50px'});
        $('#blur-background').show();
    })

    $('input#checkout-btn').click(function(){

        var me      = $(this),
            type    = me.attr('data-type') ;
        var form    = $('form#frmCheckOut');

        var msg = "";
        var waring_modal = $('div#message-warning-modal');

        var action = form.attr('action');
        action += "/" + type;

        form.attr('action', action);

        if( type == 2 ){
            var balance = parseFloat(<?php echo $balance ?>);
            var total   = parseFloat($('.grand-total').attr('data-val'));

            if( balance < total ){

                msg = "<p>Your credit is not enough !</p>";

                waring_modal.find('#content-show').html(msg);
                
                waring_modal.animate({'top':'60px'});
                waring_modal.animate({'top':'50px'});

            }else{
                form.submit();
            }

        }

        return false;
    })

    $('input#instore-checkout-btn').click(function(){

        var me      = $(this),
            type    = me.attr('data-type') ;
        var form    = $('form#frmCheckOut');

        var action = form.attr('action');
        action += "/" + type;

        form.attr('action', action);

        form.submit();

    })

</script>
