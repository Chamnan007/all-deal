<?php 

/********************************************************************************

File Name: index.ctp (Your Cart Shopping)
Description: for business registration form

Powered By: ABi Investment Group Co., Ltd,

Changed History:

    Date                Author              Description
    2015/04/06          Phea Rattana        Initial Version

*********************************************************************************/
?>

<style type="text/css">

    .register-form label{
        width: 140px;
    }

    .alert-error{
        color: red;
        background: rgb(200, 54, 54); /* The Fallback */
        background: rgba(200, 54, 54, 0.5);
    }

    .alert-dismissable .close{
        top: -35px;
        right: -30px;
        color: #666;
    }

</style>

<?php 
    
    $slug = "";

    if( isset($_GET['ref']) && $_GET['ref'] != "" ){
        $slug = $_GET['ref'];
    }

    $selected_menu = "refer";
    
?>

<div class="register-form clearfix">
    <form id="frmAdd" method="post" action="<?php echo $this->Html->url(array('controller'=>'shops', 'action'=>'refer')) ?>"enctype="multipart/form-data">
        <h1 style="text-transform: uppercase;"><?php echo __('Refer Friends'); ?></h1>
        
        <div class="col-sm-12 no-padding clearfix" style="min-height: 200px; padding-bottom:50px !important;">
            <?php include('account-nav.ctp') ?>     

            <div class="clearfix" style="padding-top: 0px;"></div> 
            
            <h1 style="border-bottom:none">Refer a friend and earn $<?php echo $bonus ?> in All-Deal Credit !</h1>

            <h3>You can earn $<?php echo $bonus ?> in All-Deal Credit whenever you refer a friend to All-Deal and they buy their first deal.</h3>

            <div class="clearfix" style="padding-top: 50px;"></div> 
            <div class="col-sm-8 no-padding">
                <legend>Send Referal Email</legend>

                <div class="pull-left" style="width:75%">
                    <ol>
                        <li>
                            <label class="required" style="text-align:left; float:left !important">
                                <?php echo __('Email Address : ') ?>
                            </label>
                            <input type="email" class="input_email" id="user_email" name="email" placeholder="<?php echo __('Email') ?>" required value="" style="width:65%; float:left !important;">
                        </li>
                    </ol>
                </div>

                <div style="float: right">
                    <button type="submit" id="btn-send-mail" style="font-size:15px; margin-top:8px;"><?php echo __('SEND EMAIL') ?></button>
                    <div class="ajax-loading" style="width: 100%; text-align: center; display:none;">
                        <img src="<?php echo $this->webroot . "img/bx_loader.gif" ?>">
                    </div>
                </div>

            </div>
           
        </div>
        
    </form>
</div>

<script type="text/javascript">

</script>
