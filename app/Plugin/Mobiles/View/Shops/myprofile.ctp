<?php 

/********************************************************************************

File Name: index.ctp (Your Cart Shopping)
Description: for business registration form

Powered By: ABi Investment Group Co., Ltd,

Changed History:

    Date                Author              Description
    2015/04/06          Phea Rattana        Initial Version

*********************************************************************************/

$selected_menu = 'profile';
?>

<style type="text/css">
.register-form label{
    width: 140px;
}
.alert-error{
    color: red;
    background: rgb(200, 54, 54); /* The Fallback */
    background: rgba(200, 54, 54, 0.5);
}
.alert-dismissable .close{
    top: -35px;
    right: -30px;
    color: #666;
}
</style>

<div class="register-form clearfix">
    <form id="frmAdd" method="post" action="<?php echo $this->Html->url(array("action"=>"saveprofile")) ?>">
        <h1 style="text-transform: uppercase;"><?php echo __('My Profile'); ?></h1>
        
        <div class="col-sm-12 no-padding clearfix" style="min-height: 150px; margin-bottom: 50px;">

            <?php include('account-nav.ctp') ?>


            <div class="col-sm-6 no-padding" style="border-right: 1px solid #DDD">
                
                <legend>General Information</legend>
                
                <table width="100%">
                    <tr>
                        <td width="100px">ID</td>
                        <td width="20px">:</td>
                        <td><strong><?php echo strtoupper($detail['Buyer']['buyer_code']) ?></strong></td>

                    </tr>

                    <tr>
                        <td width="100px">Name</td>
                        <td width="20px">:</td>
                        <td><strong><?php echo $detail['Buyer']['full_name'] ?></strong></td>

                    </tr>

                    <tr>
                        <td>Gender</td>
                        <td>:</td>
                        <td><?php echo $genders[$detail['Buyer']['gender']] ?></td>
                    </tr>
                    <tr>
                        <td>DOB</td>
                        <td>:</td>
                        <td><?php echo date('d-F-Y', strtotime($detail['Buyer']['dob'])) ?></td>
                    </tr>

                    <tr>
                        <td style="padding-top:20px;"></td>
                    </tr>


                    <tr>
                        <td>Sign Up Date</td>
                        <td>:</td>
                        <td><?php echo date('d-F-Y h:i:s A', strtotime($detail['Buyer']['created'])) ?></td>
                    </tr>

                </table>
            </div>
            
            <div class="col-sm-5" style="width: 478px;">
                <legend>Contact Information</legend>

                <table width="100%">
                    <tr>
                        <td width="100px">Address</td>
                        <td width="20px">:</td>
                        <td>
                            <?php echo ($detail['Buyer']['street']!= "")?$detail['Buyer']['street'] . ",":"" ?>
                            <?php echo ($detail['SangkatInfo']['name'])?$detail['SangkatInfo']['name']. ", ":"" ?>
                            <?php echo $detail['Buyer']['location'] . ", " ?>
                            <?php echo $detail['Cities']['city_name'] ?>
                        </td>
                    </tr>

                    <tr>
                        <td>Phone1</td>
                        <td>:</td>
                        <td><?php echo $detail['Buyer']['phone1'] ?></td>
                    </tr>

                    <tr>
                        <td>Phone2</td>
                        <td>:</td>
                        <td><?php echo $detail['Buyer']['phone2'] ?></td>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td>:</td>
                        <td><?php echo $detail['Buyer']['email'] ?></td>
                    </tr>
                </table>
            </div>

            <div class="col-sm-12 no-padding clearfix" style="text-align:left;">
                
                <a href="<?php echo $this->Html->url(array("action"=>"editprofile")) ?>">
                  <button type="button" style="font-size:12px; text-transform: none; float:left"><?php echo __('Edit Information') ?></button>
                </a>

            </div>             
        </div>
        
    </form>
</div>

</script>
