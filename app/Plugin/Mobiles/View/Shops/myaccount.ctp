<?php 

/********************************************************************************

File Name: index.ctp (Your Cart Shopping)
Description: for business registration form

Powered By: ABi Investment Group Co., Ltd,

Changed History:

    Date                Author              Description
    2015/04/06          Phea Rattana        Initial Version

*********************************************************************************/
?>

<style type="text/css">
.register-form label{
    width: 140px;
}
.alert-error{
    color: red;
    background: rgb(200, 54, 54); /* The Fallback */
    background: rgba(200, 54, 54, 0.5);
}
.alert-dismissable .close{
    top: -35px;
    right: -30px;
    color: #666;
}

</style>

<?php 
    $selected_menu = "account";

    $type[1] = "In";
    $type[0] = "Out";

?>

<div class="register-form clearfix">
        <h1 style="text-transform: uppercase;"><?php echo __('My Account'); ?></h1>
        
        <div class="col-sm-12 no-padding clearfix" style="min-height: 150px;">
            <?php include('account-nav.ctp') ?>     

            <div class="clearfix" style="padding-top: 0px;"></div> 
            
            <div style="width: 100%; border-bottom: 1px solid #AAA; padding-bottom: 5px; overflow:hidden;">
                <p style=" font-size: 20px; float: left;">ALL DEAL Credits</p>
                <p style=" font-size: 20px; float: right;">Balance: 
                    <font style="color:#8dc63f ">$ <?php echo number_format($balance,2) ?></font>
                </p>
            </div>

            <div class="clearfix" style="padding-top: 10px;"></div>

            <form id="history-search" class="form-horizontal" method="POST" action="<?php echo $this->Html->url(array('action'=>'myaccount')) ?>">
               
                <fieldset>

                   <!--  <div class="control-group" style="float:left; width:150px;">
                        <div class="controls">
                            <input type="text" class="form-contrl" name="code" id="code" placeholder="Code" style="width:140px;"
                                value="<?php echo @$states['code'] ?>">
                        </div>
                    </div> -->

                    <div class="control-group" style="float:left; width:245px;">
                        <div class="controls">
                            <div class="input-prepend input-group">
                                <span class="add-on input-group-addon">
                                    <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                                </span>
                                <input type="text" style="width: 200px" name="daterange" id="daterange" class="form-control" placeholder="Date Range" value="<?php echo @$states['daterange'] ?>"/>
                            </div>
                        </div>
                    </div>

                    <div class="control-group" style="float:left; width:150px;">
                        <div class="controls">
                            <select name="status" id="status"  style="width:100%;" class="form-control">
                                <option value="all">All</option>
                                <?php foreach( $status_arr as $k => $val ){ 
                                    $selected = (@$states['status'] != "all" && $states['status'] == $k )?" selected='selected'":"";
                                ?>
                                    <option value="<?php echo $k ?>" <?php echo $selected ?>><?php echo $val['status'] ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <input type="submit" class="header-button" value="Filter" style="float:left; width: 60px; font-size:13px;">
                    <input type="button" class="close-btn btn-danger" id="clear-search" value="Clear" style="float:left; width: 60px; font-size:13px; color:white;">

                
                </fieldset>

            </form>

            <div class="clearfix" style="padding-top: 10px;"></div>
            <table style="margin-top: 10px; width:100%" id="history-list">
                <tr>
                    <th width="30px"></th>
                    <th width="170px;">Date</th>
                    <th width="100px">Deal Code</th>
                    <th>Merchant</th>
                    <th width="120px;" style="text-align:right; padding-right:20px;">Amount</th>
                    <th width="150px;">Payment Method</th>
                    <th width="100px;" style="text-align:right">Status</th>
                    <th width="50px;"></th>
                </tr>

                <?php if($histories){ ?>

                <?php foreach( $histories as $k => $val ){ 

                        $status = $val['BuyerTransaction']['status'];

                        $status_text    = $TransactionStatus[$status]['status'];
                        $status_color   = $TransactionStatus[$status]['color'];

                        $pay_method     = $payment_method[$val['BuyerTransaction']['payment_type']] ;
                ?>

                    <tr class="history-row" <?php echo($val['BuyerTransaction']['type'] == 1)?" style='color:blue;'":"" ?>>
                        <td style="text-align:center">
                           <?php if( $val['BuyerTransaction']['type'] == 0 ){ ?>
                                <span class="list-expand" title="click to see detail" data-id="<?php echo $val['BuyerTransaction']['id'] ?>" >+</span>
                           <?php } ?> 
                        </td>
                        <td><?php echo date('d-M-Y h:i:s A', strtotime($val['BuyerTransaction']['created'])) ?></td>
                        <td>
                            <strong>
                                <?php echo $val['DealInfo']['deal_code'] ?>
                            </strong>
                        </td>
                        <td>
                            <a href="<?php echo $this->Html->url(array('controller'=>'businesses', 'action'=>'detail', $val['BusinessDetail']['slug'])) ?>" target="_blank">
                                <?php echo $val['BusinessDetail']['business_name'] ?>
                            </a>
                        </td>
                        <td style="padding-right:20px; text-align:right; font-weight:bold; <?php echo($val['BuyerTransaction']['type'] == 1)?' color:blue;':'' ?>">$ <?php echo number_format($val['BuyerTransaction']['amount'], 2) ?></td>
                        
                        <td>
                            <?php echo $pay_method ?>
                        </td>

                        <td style="text-align:right; padding-right:5px;">
                            <?php if( $val['BuyerTransaction']['type'] == 0 ){ ?>
                                <span class="label label-<?php echo $status_color ?>"><?php echo $status_text ?></span>
                            <?php } ?>
                        </td>

                        <td style="text-align:right">
                            <?php if( $val['BuyerTransaction']['type'] == 0 ){ ?>
                                <a  href="#" class="view-detail-transaction" 
                                    title="View Detail"
                                    data-creditcard="<?php echo str_pad($val['BuyerTransaction']['credit_card_number'], 12, "*", STR_PAD_LEFT) ?>"
                                    data-id="<?php echo $val['BuyerTransaction']['id'] ?>"
                                    data-date="<?php echo date("d-F-Y", strtotime($val['BuyerTransaction']['created'])) ?>"
                                    data-received-date="<?php echo date("d-F-Y | h:i:s A", strtotime($val['BuyerTransaction']['received_date'])) ?>"
                                    data-time="<?php echo date("h:i:s A", strtotime($val['BuyerTransaction']['created'])) ?>"
                                    data-code="<?php echo $val['BuyerTransaction']['code'] ?>"><strong>view</strong></a>
                            <?php } ?>
                        </td>

                    </tr>

                <?php 

                    $tranDetail = $val['TransactionDetail'];


                        foreach( $tranDetail as $ind => $item ){

                            $thumb_img  = str_replace('/menus/', '/menus/thumbs/', $item['ItemDetail']['image']);
                ?>
                    <tr class="sub-row sub-row-<?php echo $item['transaction_id'] ?>" >
                        <td></td>
                        <td colspan="2">
                            <img src="<?php echo $this->webroot . $thumb_img ?>"
                                     style="max-width: 80px; max-height:80px; margin-right:10px;"   >
                            <?php echo $item['ItemDetail']['title'] ?>
                        </td>
                        <td colspan="4">
                            <span style="padding-right:10px;"><?php echo $item['qty'] ?></span>
                            <span style="padding-right:10px;">x</span>
                            <span style="padding-right:10px;"><?php echo "$ " . number_format($item['unit_price'],2) ?></span>
                            <span style="padding-right:10px;">=</span>
                            <span>$ <?php echo number_format($item['amount'], 2 ) ?></span>
                        </td>
                    </tr>
                <?php
                    }
                }
            }else{
            ?>
                
                <tr>
                    <td colspan="10"><i>There is no transaction yet.</i></td>
                </tr>
            <?php    
                }
            ?>
            </table>

            <div class="clearfix" style="padding-top: 15px;"></div>
            
            <?php if($prev){ ?>
                <a class="pull-left header-button"  
                    href="<?php echo $this->Html->url(array('action'=>'myaccount', $page - 1)) ?>">Previous</a>
            <?php } ?>

            <?php if($next){ ?>
                <a class="pull-right header-button" 
                    href="<?php echo $this->Html->url(array('action'=>'myaccount', $page + 1)) ?>">Next</a>
            <?php } ?>

        </div>
        
</div>

<div id="blur-background"></div>

<div id="item-list-modal" style="height:500px;">
    
    <h4 style="float:left; font-size: 22px;" id="item-title">Purchase Detail</h4>
    <div id="close" title="Close">X</div>
    <div class="clearfix" style="border-bottom:1px solid #EEE; padding-bottom:10px;"></div>
    <!-- <hr> -->

    <div id="content-show" style="padding-top:10px; ">
        
        <div id="item-content" style=" height:400px;max-height:400px;">

            <div style="width:48%; float:left">        
                <legend style="font-weight: bold; border-bottom: 1px solid #DDD; margin-bottom: 5px;">Purchase Information</legend>        
                <table id="purchase-detail-info" style="width:100%; font-size:12px; float:left">
                    <tr>
                        <td width="100px;">Purchased Date</td>
                        <td width="20px;">:</td>
                        <td id="purchase-date" style="font-weight:bold;"></td>
                    </tr>

                    <tr>
                        <td>Payment Type</td>
                        <td>:</td>
                        <td id="purchase-payment-type" style="font-weight:bold;"></td>
                    </tr>

                    <tr id="if-credit-card" style="display:none;">
                        <td>Card Number (Last 4 numbers)</td>
                        <td>:</td>
                        <td id="card-number"></td>
                    </tr>

                    <tr>
                        <td>Purchase Status</td>
                        <td>:</td>
                        <td id="purchase-status" style="font-weight:bold;"></td>
                    </tr>

                    <tr class="if-received" style="display:none;">
                        <td>Received Date</td>
                        <td>:</td>
                        <td id="received-date" style="font-weight:bold"></td>
                    </tr>

                    <tr class="if-received" style="display:none;">
                        <td>Received Note</td>
                        <td>:</td>
                        <td id="received-note"></td>
                    </tr>

                </table>
            </div>

            <div style="width:48%; float:left; margin-left:20px;">
                <legend style="font-weight: bold; border-bottom: 1px solid #DDD; margin-bottom: 5px;">Merchant Information</legend>        
                <table id="merchant-info" style="width:100%; font-size:12px;">
                    <tr>
                        <td width="100px;">Code</td> 
                        <td width="20px;">:</td>
                        <td id="merchant-code" style="font-weight:bold;"></td>
                    </tr>
                    <tr>
                        <td width="100px;">Name</td> 
                        <td width="20px;">:</td>
                        <td id="merchant-name"></td>
                    </tr>
                    <tr>
                        <td>Phone</td>
                        <td>:</td>
                        <td id="merchant-phone"></td>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td>:</td>
                        <td id="merchant-email"></td>
                    </tr>
                    <tr>
                        <td>Address</td>
                        <td>:</td>
                        <td id="merchant-address"></td>
                    </tr>
                </table>
            </div>          

            <div class="clearfix" style="padding-top:20px;"></div>

            <table id="detail-table" style="font-size:12px; width:99%; margin-top:20px;">
                <thead>
                    <tr>
                        <th width="50px;">N<sup>o</sup></th>
                        <th style="text-align:left"><?php echo __('Name') ?></th>
                        <th width="120px;"><?php echo __('Unit Price') ?></th>
                        <th width="100px;"><?php echo __('Qty') ?></th>
                        <th width="150px;"  style="text-align:right;"><?php echo __('Amount') ?></th>
                    </tr>
                </thead>

                <tbody id="item-detial-list">
                    <tr class="row-model">
                        <td class="no" style="text-align:center"></td>
                        <td class="name"></td>
                        <td class="unit-price" style="text-align:center;"></td>
                        <td class="qty" style="text-align:center;"></td>
                        <td class="amount"  style="text-align:right;"></td>
                    </tr>
                </tbody>

                <tfoot>
                    <tr style="border: none !important;">
                        <td style="padding-top:5px;border: none !important;"></td>
                    </tr>
                    <tr style="border: none !important;">
                        <td colspan="3" style="border:none !important;"></td>
                        <td style="text-align:center; font-size: 15px; font-weight:bold; border:none !important; border: 1px solid #CCC; background: #DDD">TOTAL :</td>
                        <td style="text-align:right; font-size:15px; font-weight:bold; border:none !important; border: 1px solid #CCC; background: #DDD" class="total"></td>
                    </tr>

                </tfoot>

            </table>


            <div class="clearfix" style="padding-top:0px;"></div>

            <table style="width:100%; font-size:13px;" >
                <tr>
                    <th width="100px;">Description</td>
                    <td width="20px;">:</td>
                    <td id="purchase-desc"></td>
                </tr>
                <tr>
                    <th width="width: 100px;">Conditions</td>
                    <td width="20px;">:</td>
                    <td id="deal-conditions"></td>
                </tr>
            </table>

        </div>

    </div>

    <div id="loading" style="position: absolute; top:30%; left: 45%; text-align: center; display:none;">
        <img src="<?php echo $this->webroot . "img/ajax-loader.gif" ?>" alt="">
        <h3>Loading...</h3>
    </div>
    
    <a  href="<?php echo $this->Html->url(array('controller'=>'shops', 'action'=>'downloadClaim')) ?>" 
        id="download-purchase-claim-btn" style="text-decoration:underline">Download Purchase Claim</a>

    <input type="button" id="close-btn" class="btn btn-google" value="Close"
            style="position: absolute; bottom:10px; right:20px;" >
</div>

<script type="text/javascript">

    $(document).ready(function() {
       
        $('#daterange').daterangepicker(null, function(start, end, label) {
            // console.log(start.toISOString(), end.toISOString(), label);
        }).attr('readonly', 'readonly');

        $("span.list-expand").click( function( event ){

            event.preventDefault();
            var me  = $(this),
                row = me.closest('tr'),
                id  = me.attr('data-id');

            id = "sub-row-" + id;

            if( row.hasClass("row-expand") ){
                row.removeClass('row-expand');
                row.find('span.list-expand').text('+');                
                row.closest('table#history-list').find('tr.sub-row').fadeOut();

            }else{

                row.closest('table#history-list').find('tr.history-row').removeClass('row-expand');
                row.closest('table#history-list').find('tr.history-row').find('span.list-expand').text('+');

                row.closest('table#history-list').find('tr.sub-row').fadeOut();
                row.closest('table#history-list').find('tr.' + id ).fadeIn();

                row.addClass('row-expand');
                row.find('span.list-expand').text('-');
            }

        });

        $('#clear-search').click( function(){
            $('input#code').val("");
            $('#daterange').val('');
            $('select#status').val('all');

            $('form#history-search').submit();
        })

        var content_show = $('div#content-show');
        content_show.hide();

        var loading     = $('div#loading');
        var modal       = $('div#item-list-modal');  

        var tbody       = modal.find('tbody#item-detial-list');
        var row_model   = tbody.find('tr.row-model');

        tbody.find('tr.row-model').remove();
        
        var download_btn = modal.find('#download-purchase-claim-btn');
        var download_url = download_btn.attr('href');


        $('.view-detail-transaction').click( function(e){
            e.preventDefault();

            tbody.html('');

            var me      = $(this); 

            var code    = me.attr('data-code');
            var date    = me.attr('data-date');
            var time    = me.attr('data-time');
            var if_credit_card = '<?php echo $__CREDIT_PAYMENT_TYPE ?>' ;

            loading.show();
            content_show.hide();
            
            $('#blur-background').show();
            modal.show();
            modal.animate({'top':'60px'});
            modal.animate({'top':'50px'});

            var url     = '<?php echo $this->Html->url(array( "controller" => "shops", "action"=>"getTransactionDetailAjax__")) ?>' ;
            var id      = me.attr('data-id');

            download_btn.hide();
            
            $.ajax({

                type: 'POST',
                url: url,
                data: { tranID: id },
                dataType: 'json',
                
                success: function (data){

                    if( data.status == true ){

                        loading.hide();
                        content_show.show();

                        var result = data.data;

                        var desc = result.BuyerTransaction.description;
                        // desc = desc.substring(0, desc.length - 2);
                        modal.find('#purchase-desc').text(desc);
                        modal.find('#deal-conditions').text(result.BuyerTransaction.deal_condition);

                        modal.find('#purchase-date').text(date + " at " + time);

                        var payment_type    = result.BuyerTransaction.payment_type;
                        var payment_methods = <?php echo json_encode($payment_method) ?>;
                        var type_text       = payment_methods[payment_type];
                        modal.find('#purchase-payment-type').text(type_text);

                        modal.find('tr#if-credit-card').hide();
                        var card_number = result.BuyerTransaction.credit_card_number;

                        if( payment_type == if_credit_card && card_number != null && card_number != '' ){
                            modal.find('tr#if-credit-card').find('td#card-number').text(me.attr('data-creditcard'));
                            modal.find('tr#if-credit-card').show();
                        }

                        var purchase_status = result.BuyerTransaction.status;
                        var status_arr      = <?php echo json_encode($status_arr) ?>;
                        var status_text     = status_arr[purchase_status]['status'];
                        modal.find('#purchase-status').text(status_text);

                        download_btn.show();
                        download_btn.attr('href', download_url + "/" + id);

                        modal.find('tr.if-received').hide();

                        if( purchase_status == 1 ){

                            download_btn.hide();

                            modal.find('#received-date').text(me.attr('data-received-date')) ;
                            modal.find('#received-note').text(result.BuyerTransaction.received_note);
                            modal.find('tr.if-received').show();

                        }
                        
                        // // Set Biz Info

                        modal.find('#merchant-code').text(result.BusinessDetail.business_code);
                        modal.find('#merchant-name').text(result.BusinessDetail.business_name);

                        var tel = result.BusinessDetail.phone1;
                        
                        if( result.BusinessDetail.phone2 != "" ){
                            tel += " / " + result.BusinessDetail.phone2;
                        }
                        modal.find('#merchant-phone').text(tel);
                        modal.find('#merchant-email').text(result.BusinessDetail.email);

                        var address = result.BusinessDetail.street + ", " + result.BusinessDetail.location + ", " + result.BusinessDetail.city ;
                        modal.find('#merchant-address').text(address);

                        // =========================

                        var itemDetail = result.TransactionDetail;

                        var total = 0;

                        $.each( itemDetail, function( ind, val ){

                            var price  = val.unit_price;
                            var qty    = val.qty;
                            var amount = price * qty;

                            total += amount;

                            price = parseFloat(price).toFixed(2);
                            amount= parseFloat(amount).toFixed(2);

                            row_model.find(".no").text(ind+1);
                            row_model.find('.name').text(val.ItemDetail.title);
                            row_model.find('.unit-price').text( "$" + price );
                            row_model.find('.qty').text( qty );
                            row_model.find('.amount').text( "$" + amount );

                            tbody.append(row_model.clone());
                        })

                        modal.find('tfoot').find('.total').text("$" + parseFloat(total).toFixed(2));
                        
                    }

                },

                error: function( err, msg ){
                    console.log(msg);
                }

            })

            document.onmousewheel = stop;

        });

    });

    $(document).keydown(function(e) {
        
        // ESCAPE key pressed
        if (e.keyCode == 27) {

            ('#item-list-modal').show();
            $('#item-list-modal').animate({'top':'60px'});
            $('#item-list-modal').animate({'top':'-200%'}, function(){
                $(this).hide();
                $('#blur-background').hide();
            });

            document.onmousewheel = start;
        }


    });

    $('#close, #close-btn').click( function(){
        
        // $('#item-list-modal').fadeOut();

        $('#item-list-modal').animate({'top':'60px'});
        $('#item-list-modal').animate({'top':'-200%'}, function(){
            $(this).hide();
            $('#blur-background').hide();
        });

        document.onmousewheel = start;
    });
    
    function stop(){
        return false;
    }

    function start(){
        return true;
    }
</script>
