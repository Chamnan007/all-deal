<?php 

/********************************************************************************

File Name: missing_action.ctp
Description: Layout for missing action error

Powered By: ABi Investment Group Co., Ltd,

Changed History:

    Date                Author              Description
    2014/06/19          Sim Chhayrambo      Initial Version

*********************************************************************************/
?>

<div class="col-sm-12 no-padding">
    <div class="container" style="padding: 50px; text-align: center;">
        <img src="<?php echo $this->webroot . "img/404-error-page.jpg" ?>">
    </div>
</div>