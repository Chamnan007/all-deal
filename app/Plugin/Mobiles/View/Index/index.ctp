<?php 

/********************************************************************************

File Name: index.ctp
Description: UI for home page

Powered By: ABi Investment Group Co., Ltd,

Changed History:

    Date                Author              Description
    2014/06/10          Sim Chhayrambo      Initial Version
    2014/06/19          Sim Chhayrambo      add meta description
    2014/06/22          Sim Chhayrambo      add margin top
    2014/07/12          Sim Chhayrambo      Change Image Size and background transparent
    2014/07/13          Sim Chhayrambo      Reduce more char in Title and remove space
    2014/07/14          Sim Chhayrambo      Fix style for feature image
    2014/08/11          Sim Chhayrambo      Remove "Date Night Deal" and add "Food & Drink"
    2015/02/25          Phea Rattana        check for deal image randomly to display,
                                            check price discount if available
    2105/07/10          Rattana             List Deal by Category Type

*********************************************************************************/
$about_us = "All Deal is a place where you can easily discover great local deal in your city such as shopping, food & drink, travel, things to do, accommodation, massages and more. Our mission is to connect buyers and sellers together through deal offering and discover fun activities.";
$metaDescription = substr($about_us,0,300).'...';
$this->Html->meta('description', $metaDescription, array('inline'=>false));

?>

<style type="text/css">

    .feature-deal figcaption{
        background: rgba(0,0,0,.8);
        bottom: 5px;
        left: 0;
        padding: 15px 20px;
        position: absolute;
        width: 100%;
    }
    .feature-deal h2{
        color: #fff;
        margin: 0;
    }
    .feature-deal p{
        color: #aaa;
        font-size: 14px;
        margin: 2px 0;
    }

</style>

<div class="container">
    <div style="height: 20px;"></div>
    <div class="col-sm-12 no-padding clearfix">
        <div class="col-sm-3 no-padding-left">
            <div class="side-title">
                <img src="<?php echo $this->webroot . "img/glass-icon.png"?>" style="float: left; margin-right: 10px;">
                <h3 style="font-weight: bold;"><?php echo __('Find Deals') ?></h3>
            </div>
            <div class="special-widget">
                <div class="swidget-body">
                    <ul>                        
                        <li>
                            <a href="<?php echo $this->Html->url(array('controller' => 'deals', 'action' => 'index')) ?>?type=deal-of-the-day"><?php echo __('Deal of the Day') ?>
                            </a>
                        </li>

                        <li>
                            <a href="<?php echo $this->Html->url(array('controller'=>'deals', 'action'=>'index')) ?>?type=last-minute-deal"><?php echo __('Last Minute Deal') ?>
                            </a>
                        </li>

                        <li>
                            <a href="<?php echo $this->Html->url(array('controller'=>'deals', 'action'=>'index')) ?>?type=food-and-drink"><?php echo __('Food & Drink') ?>
                            </a>
                        </li>
                        
                        <li>
                            <a href="<?php echo $this->Html->url(array('controller' => 'deals', 'action' => 'index' )) ?>?type=beauty-and-spa"><?php echo __('Beauty & Spa') ?>
                            </a>
                        </li>
                        
                        <li>
                            <a href="<?php echo $this->Html->url(array('controller' => 'deals', 'action' => 'index')) ?>?type=shopping"><?php echo __('Shopping') ?>
                            </a>
                        </li>

                        <li>
                            <a href="<?php echo $this->Html->url(array('controller'=>'deals', 'action' => 'index')) ?>?type=things-to-do"><?php echo __('Things to do') ?>
                            </a>
                        </li>

                        <li>
                            <a href="<?php echo $this->Html->url(array('controller'=>'deals', 'action'=>'index')) ?>?type=getaways"><?php echo __('Getaways') ?>
                            </a>
                        </li>

                    </ul>
                </div>
            </div>
        </div>

        <div class="col-sm-9 no-padding feature-deal">
            <div class="col-sm-12 no-padding">
                <?php 

                if( $featureDeal ){
                    $image = $featureDeal[0]['Deal']['image'];
                    $other_images = $featureDeal[0]['Deal']['other_images'];

                    if( $other_images ){
                        
                        $other_images = json_decode($other_images);
                        foreach( $other_images as $k => $img ){

                          if( $img != "img/deals/default.jpg" ){
                            $data_images[]['img'] = $img;
                          }
                        }

                    }else if( $image != "img/deals/default.jpg" ) {
                      $data_images[]['img']   = $image;
                    }

                    $selected = @$data_images[array_rand($data_images)];

                    if( $selected ){ 
                        $image   = $selected['img'];
                    }

                    $original = "";
                    $discount = $featureDeal[0]['Deal']['discount_category'];

                    $prev_original  = 0;
                    $prev_discount  = 0;

                    if( $items = $featureDeal[0]['DealItemLink'] ){

                        $max_percentage = 0;

                        foreach( $items as $k => $v ){

                            $ori        = ($v['original_price'] != 0 )?$v['original_price']:$v['ItemDetail']['price'];
                            $disc       = $v['discount'];
                            $item_id    = $v['item_id'];

                            $percentage = 100 - ( $disc * 100 / $ori );

                            if( $percentage > $max_percentage ){
                                $max_percentage = $percentage;
                                $prev_original  = $ori;
                                $prev_discount  = $disc;
                            }else if( $percentage == $max_percentage ){
                                if( $disc < $prev_discount ){
                                    $prev_original  = $ori;
                                    $prev_discount  = $disc;
                                }
                            }

                        }

                        $original = "$" . number_format($prev_original, 2) ;
                        $discount = "$" . number_format($prev_discount, 2);                        
                    }

                ?>

                <a href="<?php echo $this->Html->url(array('controller'=>'deals', 'action'=>'detail', $featureDeal[0]['Deal']['slug'])) ?>">
                    <img src="<?php echo $this->webroot . $image ?>" width="705" height="422" title="<?php echo $featureDeal[0]['Deal']['title'] ?>">
                </a>
                <figcaption>
                    <h2><?php echo $featureDeal[0]['Deal']['title'] ?></h2>
                    <p><?php echo $featureDeal[0]['Business']['business_name'] ?></p>
                    <p style="font-size: 12px;" ><i class="icon-location"></i><?php echo $featureDeal[0]['Business']['location'] . ", " . $featureDeal[0]['Province']['province_name']  ?></p>
                    <h4 style="color:#CCC; float:left; margin-right: 10px; text-decoration: line-through"><?php echo $original ?></h4>
                    <h4 class="price-off" style="float: left;"><?php echo $discount ?></h4>
                    <a href="<?php echo $this->Html->url(array('controller'=>'deals', 'action'=>'detail', $featureDeal[0]['Deal']['slug'])) ?>" class="button btn-green pull-right"><i class="icon-search"></i> <?php echo __('View Detail') ?></a>
                </figcaption>

                <?php }else{ ?>
                    <div class="not-found">
                        <h1><?php echo __('No Deals Available!') ?></h1>
                    </div>
                <?php } ?>
            </div>

        </div>

    </div>

    <div class="clearfix" style="padding-bottom: 20px;"></div>

    <!-- Deal of the Day -->
    
    
    <?php if($deal_of_the_day): ?>
        <div class="container no-padding">
            <div class="recent-works col-sm-12 no-padding-left" style="margin-top: 20px;">
                <div class="recent-works-header clearfix">
                    <h2 style="width: 100%;">
                        <span style="float: left;">
                            <?php echo __('Deal of the Day') ?></span>
                        <span>
                            <a href="<?php echo $this->Html->url(array('controller'=>'deals', 'action' => 'index')) ?>?type=deal-of-the-day" class="button btn-green pull-right" style="padding:10px; !important;"><i class="icon-list"></i> <?php echo __('View All') ?></a>
                        </span>
                    </h2>
                </div>
                <div class="recent-works-carousel">
                    <div class="carousel slide" id="Recentworks">
                        <div class="carousel-inner">
                            <div class="item active">
                                <ul class="thumbnails">
                                    <div class="row">
                                        <?php 
                                        foreach ($deal_of_the_day as $dealKey => $dealValue) { 
                                            $split = end(explode("/", $dealValue['Deal']['image']));
                                            $thumb_img =  "img/deals/thumbs/" . $split ;
                                            $business_name = $dealValue['Business']['business_name'];
                                            $business_street = $dealValue['Business']['street'];
                                            $business_location = $dealValue['Business']['location'];
                                            $deal_title = $dealValue['Deal']['title'];
                                            if(strlen($deal_title) > $limit_deal_title){
                                                $deal_title = substr($dealValue['Deal']['title'],0,$limit_deal_title-3).'...';   
                                            }
                                            if(strlen($business_name) > $limit_business_title){
                                                $business_name = substr($business_name,0,$limit_business_title-3).'...';   
                                            }

                                            $image = $dealValue['Deal']['image'];
                                            $other_images = $dealValue['Deal']['other_images'];
                                            $data_images = array();

                                            if( $other_images ){
                                                
                                                $other_images = json_decode($other_images);
                                                foreach( $other_images as $k => $img ){

                                                  if( $img != "img/deals/default.jpg" ){
                                                    $data_images[]['img'] = $img;
                                                  }
                                                }

                                            }else if( $image != "img/deals/default.jpg" ) {
                                              $data_images[]['img']   = $image;
                                            }

                                            $selected = @$data_images[array_rand($data_images)];

                                            if( $selected ){ 
                                                $image          = $selected['img'];
                                                $image_thumb    = str_replace("/deals/", "/deals/thumbs/", $image);
                                            }

                                            $original = "";
                                            $discount = $dealValue['Deal']['discount_category'];

                                            $prev_original  = 0;
                                            $prev_discount  = 0;

                                            if( $items = $dealValue['DealItemLink'] ){

                                                $max_percentage = 0;

                                                foreach( $items as $k => $v ){

                                                    $ori        = ($v['original_price'] != 0 )?$v['original_price']:$v['ItemDetail']['price'];
                                                    $disc       = $v['discount'];
                                                    $item_id    = $v['item_id'];

                                                    $percentage = 100 - ( $disc * 100 / $ori );

                                                    if( $percentage > $max_percentage ){
                                                        $max_percentage = $percentage;
                                                        $prev_original  = $ori;
                                                        $prev_discount  = $disc;
                                                    }else if( $percentage == $max_percentage ){
                                                        if( $disc < $prev_discount ){
                                                            $prev_original  = $ori;
                                                            $prev_discount  = $disc;
                                                        }
                                                    }

                                                }

                                                $original = "$" . number_format($prev_original, 2) ;
                                                $discount = "$" . number_format($prev_discount, 2);                        
                                            }

                                        ?>
                                            <li class="col-sm-3 col-xs-3">
                                                <div class="new-project bg-shadow add-margin-bottom deal-img-container">
                                                    <div>
                                                        <a href="<?php echo $this->Html->url(array('controller'=>'deals', 'action'=>'detail', $dealValue['Deal']['slug'])) ?>">
                                                            <img src="<?php echo $this->webroot . $image_thumb ?>" title="<?php echo $dealValue['Deal']['title'] ?>">
                                                        </a>
                                                    </div>
                                                    <div id="bx-pager">
                                                        <a data-slide-index="0" href="<?php echo $this->Html->url(array('controller'=>'deals', 'action'=>'detail', $dealValue['Deal']['slug'])) ?>" class="active">
                                                            <div class="new-project-body">
                                                                <h4><?php echo $deal_title ?></h4>
                                                                <i><?php echo $business_name ?></i><br>
                                                                <i class="icon-location"><?php echo $business_location . ", " . $dealValue['Province']['province_name'] ?></i>
                                                                <div class="clearfix"></div>
                                                                <h4 class="price-off pull-right"><?php echo $discount ?></h4>
                                                                <h4 style="color:#666; float:right; margin-right: 10px; text-decoration: line-through;"><?php echo $original ?></h4>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </li>
                                        <?php } ?>
                                    </div>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
        </div>
    <?php endif ?>
    <!-- Last Minute Deals -->
    <?php if($last_minute_deals): ?>
        <div class="container no-padding">
            <div class="recent-works col-sm-12 no-padding-left">
                <div class="recent-works-header clearfix">
                    <h2 style="width: 100%;">
                        <span style="float: left;"><?php echo __('Last Minute Deal') ?></span>
                        <span>
                            <a href="<?php echo $this->Html->url(array('controller'=>'deals', 'action' => 'index')) ?>?type=last-minute-deal" class="button btn-green pull-right" style="padding:10px; !important;"><i class="icon-list"></i> <?php echo __('View All') ?></a>
                        </span>
                    </h2>
                </div>
                <div class="recent-works-carousel">
                    <div class="carousel slide" id="Recentworks">
                        <div class="carousel-inner">
                            <div class="item active">
                                <ul class="thumbnails">
                                    <div class="row">
                                        <?php 
                                        foreach ($last_minute_deals as $dnKey => $dnValue) { 
                                            $split = end(explode("/", $dnValue['Deal']['image']));
                                            $thumb_img =  "img/deals/thumbs/" . $split ;
                                            $business_name = $dnValue['Business']['business_name'];
                                            $business_street = $dnValue['Business']['street'];
                                            $business_location = $dnValue['Business']['location'];
                                            $deal_title = $dnValue['Deal']['title'];
                                            if(strlen($deal_title) > $limit_deal_title){
                                                $deal_title = substr($dnValue['Deal']['title'],0,$limit_deal_title-3).'...';   
                                            }
                                            if(strlen($business_name) > $limit_business_title){
                                                $business_name = substr($business_name,0,$limit_business_title-3).'...';   
                                            }

                                            $image = $dnValue['Deal']['image'];
                                            $other_images = $dnValue['Deal']['other_images'];
                                            $data_images = array();

                                            if( $other_images ){
                                                
                                                $other_images = json_decode($other_images);
                                                foreach( $other_images as $k => $img ){

                                                  if( $img != "img/deals/default.jpg" ){
                                                    $data_images[]['img'] = $img;
                                                  }
                                                }

                                            }else if( $image != "img/deals/default.jpg" ) {
                                              $data_images[]['img']   = $image;
                                            }

                                            $selected = @$data_images[array_rand($data_images)];

                                            if( $selected ){ 
                                                $image          = $selected['img'];
                                                $image_thumb    = str_replace("/deals/", "/deals/thumbs/", $image);
                                            }

                                            $original = "";
                                            $discount = $dnValue['Deal']['discount_category'];

                                            $prev_original  = 0;
                                            $prev_discount  = 0;

                                            if( $items = $dnValue['DealItemLink'] ){

                                                $max_percentage = 0;

                                                foreach( $items as $k => $v ){

                                                    $ori        = ($v['original_price'] != 0 )?$v['original_price']:$v['ItemDetail']['price'];
                                                    $disc       = $v['discount'];

                                                    $percentage = 100 - ( $disc * 100 / $ori );

                                                    if( $percentage > $max_percentage ){
                                                        $max_percentage = $percentage;
                                                        $prev_original  = $ori;
                                                        $prev_discount  = $disc;
                                                    }else if( $percentage == $max_percentage ){
                                                        if( $disc < $prev_discount ){
                                                            $prev_original  = $ori;
                                                            $prev_discount  = $disc;
                                                        }
                                                    }

                                                }

                                                $original = "$" . number_format($prev_original, 2) ;
                                                $discount = "$" . number_format($prev_discount, 2);                        
                                            }
                                        ?>
                                            <li class="col-sm-3 col-xs-3">
                                                <div class="new-project bg-shadow add-margin-bottom deal-img-container">
                                                    <div>
                                                        <a href="<?php echo $this->Html->url(array('controller'=>'deals', 'action'=>'detail', $dnValue['Deal']['slug'])) ?>">
                                                            <img src="<?php echo $this->webroot . $image_thumb ?>" title="<?php echo $dnValue['Deal']['title'] ?>">
                                                        </a>
                                                    </div>
                                                    <div id="bx-pager">
                                                        <a data-slide-index="0" href="<?php echo $this->Html->url(array('controller'=>'deals', 'action'=>'detail', $dnValue['Deal']['slug'])) ?>" class="active">
                                                            <div class="new-project-body">
                                                                <h4><?php echo $deal_title ?></h4>
                                                                <i><?php echo $business_name ?></i><br>
                                                                <i class="icon-location"><?php echo $business_location . ", " . $dnValue['Province']['province_name'] ?></i>
                                                                <div class="clearfix"></div>
                                                                <h4 class="price-off pull-right"><?php echo $discount ?></h4>
                                                                <h4 style="color:#666; float:right; margin-right: 10px; text-decoration: line-through;"><?php echo $original ?></h4>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </li>

                                        <?php } ?>
                                    </div>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
        </div>
    <?php endif ?>

    <!-- Food & Drink -->
    <?php if($food_and_drink_deals): ?>
        <div class="container no-padding">
            <div class="recent-works col-sm-12 no-padding-left">
                <div class="recent-works-header clearfix">
                    <h2 style="width: 100%;">
                        <span style="float: left;"><?php echo __('Food & Drink') ?></span>
                        <span>
                            <a href="<?php echo $this->Html->url(array('controller'=>'deals', 'action' => 'index')) ?>?type=food-and-drink" class="button btn-green pull-right" style="padding:10px; !important;"><i class="icon-list"></i> <?php echo __('View All') ?></a>
                        </span>
                    </h2>
                </div>
                <div class="recent-works-carousel">
                    <div class="carousel slide" id="Recentworks">
                        <div class="carousel-inner">
                            <div class="item active">
                                <ul class="thumbnails">
                                    <div class="row">
                                        <?php 
                                        foreach ($food_and_drink_deals as $dnKey => $dnValue) { 
                                            $split = end(explode("/", $dnValue['Deal']['image']));
                                            $thumb_img =  "img/deals/thumbs/" . $split ;
                                            $business_name = $dnValue['Business']['business_name'];
                                            $business_street = $dnValue['Business']['street'];
                                            $business_location = $dnValue['Business']['location'];
                                            $deal_title = $dnValue['Deal']['title'];
                                            if(strlen($deal_title) > $limit_deal_title){
                                                $deal_title = substr($dnValue['Deal']['title'],0,$limit_deal_title-3).'...';   
                                            }
                                            if(strlen($business_name) > $limit_business_title){
                                                $business_name = substr($business_name,0,$limit_business_title-3).'...';   
                                            }

                                            $image = $dnValue['Deal']['image'];
                                            $other_images = $dnValue['Deal']['other_images'];
                                            $data_images = array();

                                            if( $other_images ){
                                                
                                                $other_images = json_decode($other_images);
                                                foreach( $other_images as $k => $img ){

                                                  if( $img != "img/deals/default.jpg" ){
                                                    $data_images[]['img'] = $img;
                                                  }
                                                }

                                            }else if( $image != "img/deals/default.jpg" ) {
                                              $data_images[]['img']   = $image;
                                            }

                                            $selected = @$data_images[array_rand($data_images)];

                                            if( $selected ){ 
                                                $image          = $selected['img'];
                                                $image_thumb    = str_replace("/deals/", "/deals/thumbs/", $image);
                                            }

                                            $original = "";
                                            $discount = $dnValue['Deal']['discount_category'];

                                            $prev_original  = 0;
                                            $prev_discount  = 0;

                                            if( $items = $dnValue['DealItemLink'] ){

                                                $max_percentage = 0;

                                                foreach( $items as $k => $v ){

                                                    $ori        = ($v['original_price'] != 0 )?$v['original_price']:$v['ItemDetail']['price'];
                                                    $disc       = $v['discount'];
                                                    $item_id    = $v['item_id'];

                                                    $percentage = 100 - ( $disc * 100 / $ori );

                                                    if( $percentage > $max_percentage ){
                                                        $max_percentage = $percentage;
                                                        $prev_original  = $ori;
                                                        $prev_discount  = $disc;
                                                    }else if( $percentage == $max_percentage ){
                                                        if( $disc < $prev_discount ){
                                                            $prev_original  = $ori;
                                                            $prev_discount  = $disc;
                                                        }
                                                    }

                                                }

                                                $original = "$" . number_format($prev_original, 2) ;
                                                $discount = "$" . number_format($prev_discount, 2);                        
                                            }
                                        ?>
                                            <li class="col-sm-3 col-xs-3">
                                                <div class="new-project bg-shadow add-margin-bottom deal-img-container">
                                                    <div>
                                                        <a href="<?php echo $this->Html->url(array('controller'=>'deals', 'action'=>'detail', $dnValue['Deal']['slug'])) ?>">
                                                            <img src="<?php echo $this->webroot . $image_thumb ?>" title="<?php echo $dnValue['Deal']['title'] ?>">
                                                        </a>
                                                    </div>
                                                    <div id="bx-pager">
                                                        <a data-slide-index="0" href="<?php echo $this->Html->url(array('controller'=>'deals', 'action'=>'detail', $dnValue['Deal']['slug'])) ?>" class="active">
                                                            <div class="new-project-body">
                                                                <h4><?php echo $deal_title ?></h4>
                                                                <i><?php echo $business_name ?></i><br>
                                                                <i class="icon-location"><?php echo $business_location . ", " . $dnValue['Province']['province_name'] ?></i>
                                                                <div class="clearfix"></div>
                                                                <h4 class="price-off pull-right"><?php echo $discount ?></h4>
                                                                <h4 style="color:#666; float:right; margin-right: 10px; text-decoration: line-through;"><?php echo $original ?></h4>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </li>
                                        <?php } ?>
                                    </div>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
        </div>
    <?php endif ?>

    <!-- Beauty & Spa Deals -->
    <?php if($beauty_and_spa_deals): ?>
        <div class="container no-padding">
            <div class="recent-works col-sm-12 no-padding-left">
                <div class="recent-works-header clearfix">
                    <h2 style="width: 100%;">
                        <span style="float: left;"><?php echo __('Beauty & Spa') ?></span>
                        <span>
                            <a href="<?php echo $this->Html->url(array('controller'=>'deals', 'action' => 'index')) ?>?type=beauty-and-spa" class="button btn-green pull-right" style="padding:10px; !important;"><i class="icon-list"></i> <?php echo __('View All') ?></a>
                        </span>
                    </h2>
                </div>
                <div class="recent-works-carousel">
                    <div class="carousel slide" id="Recentworks">
                        <div class="carousel-inner">
                            <div class="item active">
                                <ul class="thumbnails">
                                    <div class="row">
                                        <?php 
                                        foreach ($beauty_and_spa_deals as $tKey => $tValue) { 
                                            $split = end(explode("/", $tValue['Deal']['image']));
                                            $thumb_img =  "img/deals/thumbs/" . $split ;
                                            $business_name = $tValue['Business']['business_name'];
                                            $business_street = $tValue['Business']['street'];
                                            $business_location = $tValue['Business']['location'];
                                            $deal_title = $tValue['Deal']['title'];
                                            if(strlen($deal_title) > $limit_deal_title){
                                                $deal_title = substr($tValue['Deal']['title'],0,$limit_deal_title-3).'...';   
                                            }
                                            if(strlen($business_name) > $limit_business_title){
                                                $business_name = substr($business_name,0,$limit_business_title-3).'...';   
                                            }

                                            $image = $tValue['Deal']['image'];
                                            $other_images = $tValue['Deal']['other_images'];
                                            $data_images = array();

                                            if( $other_images ){
                                                
                                                $other_images = json_decode($other_images);
                                                foreach( $other_images as $k => $img ){

                                                  if( $img != "img/deals/default.jpg" ){
                                                    $data_images[]['img'] = $img;
                                                  }
                                                }

                                            }else if( $image != "img/deals/default.jpg" ) {
                                              $data_images[]['img']   = $image;
                                            }

                                            $selected = @$data_images[array_rand($data_images)];

                                            if( $selected ){ 
                                                $image          = $selected['img'];
                                                $image_thumb    = str_replace("/deals/", "/deals/thumbs/", $image);
                                            }

                                            $original = "";
                                            $discount = $tValue['Deal']['discount_category'];

                                            $prev_original  = 0;
                                            $prev_discount  = 0;

                                            if( $items = $tValue['DealItemLink'] ){

                                                $max_percentage = 0;

                                                foreach( $items as $k => $v ){

                                                    $ori        = ($v['original_price'] != 0 )?$v['original_price']:$v['ItemDetail']['price'];
                                                    $disc       = $v['discount'];
                                                    $item_id    = $v['item_id'];

                                                    $percentage = 100 - ( $disc * 100 / $ori );

                                                    if( $percentage > $max_percentage ){
                                                        $max_percentage = $percentage;
                                                        $prev_original  = $ori;
                                                        $prev_discount  = $disc;
                                                    }else if( $percentage == $max_percentage ){
                                                        if( $disc < $prev_discount ){
                                                            $prev_original  = $ori;
                                                            $prev_discount  = $disc;
                                                        }
                                                    }

                                                }

                                                $original = "$" . number_format($prev_original, 2) ;
                                                $discount = "$" . number_format($prev_discount, 2);                        
                                            }

                                        ?>
                                            <li class="col-sm-3 col-xs-3">
                                                <div class="new-project bg-shadow add-margin-bottom deal-img-container">
                                                    <div>
                                                        <a href="<?php echo $this->Html->url(array('controller'=>'deals', 'action'=>'detail', $tValue['Deal']['slug'])) ?>">
                                                            <img src="<?php echo $this->webroot . $image_thumb ?>" title="<?php echo $tValue['Deal']['title'] ?>">
                                                        </a>
                                                    </div>
                                                    <div id="bx-pager">
                                                        <a data-slide-index="0" href="<?php echo $this->Html->url(array('controller'=>'deals', 'action'=>'detail', $tValue['Deal']['slug'])) ?>" class="active">
                                                            <div class="new-project-body">
                                                                <h4><?php echo $deal_title ?></h4>
                                                                <i><?php echo $business_name ?></i><br>
                                                                <i class="icon-location"><?php echo $business_location . ", " . $tValue['Province']['province_name'] ?></i>
                                                                <div class="clearfix"></div>
                                                                <h4 class="price-off pull-right"><?php echo $discount ?></h4>
                                                                <h4 style="color:#666; float:right; margin-right: 10px; text-decoration: line-through;"><?php echo $original ?></h4>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </li>
                                        <?php } ?>
                                    </div>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
        </div>
    <?php endif ?>

    <!-- Shopping Deal -->
    <?php if($shopping_deals): ?>
        <div class="container no-padding">
            <div class="recent-works col-sm-12 no-padding-left">
                <div class="recent-works-header clearfix">
                    <h2 style="width: 100%;">
                        <span style="float: left;"><?php echo __('Shopping') ?></span>
                        <span>
                            <a href="<?php echo $this->Html->url(array('controller'=>'deals', 'action' => 'index')) ?>?type=shopping" class="button btn-green pull-right" style="padding:10px; !important;"><i class="icon-list"></i> <?php echo __('View All') ?></a>
                        </span>
                    </h2>
                </div>
                <div class="recent-works-carousel">
                    <div class="carousel slide" id="Recentworks">
                        <div class="carousel-inner">
                            <div class="item active">
                                <ul class="thumbnails">
                                    <div class="row">
                                        <?php 
                                        foreach ($shopping_deals as $dnKey => $dnValue) { 
                                            $split = end(explode("/", $dnValue['Deal']['image']));
                                            $thumb_img =  "img/deals/thumbs/" . $split ;
                                            $business_name = $dnValue['Business']['business_name'];
                                            $business_street = $dnValue['Business']['street'];
                                            $business_location = $dnValue['Business']['location'];
                                            $deal_title = $dnValue['Deal']['title'];
                                            if(strlen($deal_title) > $limit_deal_title){
                                                $deal_title = substr($dnValue['Deal']['title'],0,$limit_deal_title-3).'...';   
                                            }
                                            if(strlen($business_name) > $limit_business_title){
                                                $business_name = substr($business_name,0,$limit_business_title-3).'...';   
                                            }

                                            $image = $dnValue['Deal']['image'];
                                            $other_images = $dnValue['Deal']['other_images'];
                                            $data_images = array();

                                            if( $other_images ){
                                                
                                                $other_images = json_decode($other_images);
                                                foreach( $other_images as $k => $img ){

                                                  if( $img != "img/deals/default.jpg" ){
                                                    $data_images[]['img'] = $img;
                                                  }
                                                }

                                            }else if( $image != "img/deals/default.jpg" ) {
                                              $data_images[]['img']   = $image;
                                            }

                                            $selected = @$data_images[array_rand($data_images)];

                                            if( $selected ){ 
                                                $image          = $selected['img'];
                                                $image_thumb    = str_replace("/deals/", "/deals/thumbs/", $image);
                                            }

                                            $original = "";
                                            $discount = $dnValue['Deal']['discount_category'];

                                            $prev_original  = 0;
                                            $prev_discount  = 0;

                                            if( $items = $dnValue['DealItemLink'] ){

                                                $max_percentage = 0;

                                                foreach( $items as $k => $v ){

                                                    $ori        = ($v['original_price'] != 0 )?$v['original_price']:$v['ItemDetail']['price'];
                                                    $disc       = $v['discount'];
                                                    $item_id    = $v['item_id'];

                                                    $percentage = 100 - ( $disc * 100 / $ori );

                                                    if( $percentage > $max_percentage ){
                                                        $max_percentage = $percentage;
                                                        $prev_original  = $ori;
                                                        $prev_discount  = $disc;
                                                    }else if( $percentage == $max_percentage ){
                                                        if( $disc < $prev_discount ){
                                                            $prev_original  = $ori;
                                                            $prev_discount  = $disc;
                                                        }
                                                    }

                                                }

                                                $original = "$" . number_format($prev_original, 2) ;
                                                $discount = "$" . number_format($prev_discount, 2);                        
                                            }
                                        ?>
                                            <li class="col-sm-3 col-xs-3">
                                                <div class="new-project bg-shadow add-margin-bottom deal-img-container">
                                                    <div>
                                                        <a href="<?php echo $this->Html->url(array('controller'=>'deals', 'action'=>'detail', $dnValue['Deal']['slug'])) ?>">
                                                            <img src="<?php echo $this->webroot . $image_thumb ?>" title="<?php echo $dnValue['Deal']['title'] ?>">
                                                        </a>
                                                    </div>
                                                    <div id="bx-pager">
                                                        <a data-slide-index="0" href="<?php echo $this->Html->url(array('controller'=>'deals', 'action'=>'detail', $dnValue['Deal']['slug'])) ?>" class="active">
                                                            <div class="new-project-body">
                                                                <h4><?php echo $deal_title ?></h4>
                                                                <i><?php echo $business_name ?></i><br>
                                                                <i class="icon-location"><?php echo $business_location . ", " . $dnValue['Province']['province_name'] ?></i>
                                                                <div class="clearfix"></div>
                                                                <h4 class="price-off pull-right"><?php echo $discount ?></h4>
                                                                <h4 style="color:#666; float:right; margin-right: 10px; text-decoration: line-through;"><?php echo $original ?></h4>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </li>
                                        <?php } ?>
                                    </div>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
        </div>
    <?php endif ?>

    <!-- Things to do Deal -->
    <?php if($things_to_do_deals): ?>
        <div class="container no-padding">
            <div class="recent-works col-sm-12 no-padding-left">
                <div class="recent-works-header clearfix">
                    <h2 style="width: 100%;">
                        <span style="float: left;"><?php echo __('Things to do') ?></span>
                        <span>
                            <a href="<?php echo $this->Html->url(array('controller'=>'deals', 'action' => 'index')) ?>?type=things-to-do" class="button btn-green pull-right" style="padding:10px; !important;"><i class="icon-list"></i> <?php echo __('View All') ?></a>
                        </span>
                    </h2>
                </div>
                <div class="recent-works-carousel">
                    <div class="carousel slide" id="Recentworks">
                        <div class="carousel-inner">
                            <div class="item active">
                                <ul class="thumbnails">
                                    <div class="row">
                                        <?php 
                                        foreach ($things_to_do_deals as $dnKey => $dnValue) { 
                                            $split = end(explode("/", $dnValue['Deal']['image']));
                                            $thumb_img =  "img/deals/thumbs/" . $split ;
                                            $business_name = $dnValue['Business']['business_name'];
                                            $business_street = $dnValue['Business']['street'];
                                            $business_location = $dnValue['Business']['location'];
                                            $deal_title = $dnValue['Deal']['title'];
                                            if(strlen($deal_title) > $limit_deal_title){
                                                $deal_title = substr($dnValue['Deal']['title'],0,$limit_deal_title-3).'...';   
                                            }
                                            if(strlen($business_name) > $limit_business_title){
                                                $business_name = substr($business_name,0,$limit_business_title-3).'...';   
                                            }

                                            $image = $dnValue['Deal']['image'];
                                            $other_images = $dnValue['Deal']['other_images'];
                                            $data_images = array();

                                            if( $other_images ){
                                                
                                                $other_images = json_decode($other_images);
                                                foreach( $other_images as $k => $img ){

                                                  if( $img != "img/deals/default.jpg" ){
                                                    $data_images[]['img'] = $img;
                                                  }
                                                }

                                            }else if( $image != "img/deals/default.jpg" ) {
                                              $data_images[]['img']   = $image;
                                            }

                                            $selected = @$data_images[array_rand($data_images)];

                                            if( $selected ){ 
                                                $image          = $selected['img'];
                                                $image_thumb    = str_replace("/deals/", "/deals/thumbs/", $image);
                                            }

                                            $original = "";
                                            $discount = $dnValue['Deal']['discount_category'];

                                            $prev_original  = 0;
                                            $prev_discount  = 0;

                                            if( $items = $dnValue['DealItemLink'] ){

                                                $max_percentage = 0;

                                                foreach( $items as $k => $v ){

                                                    $ori        = ($v['original_price'] != 0 )?$v['original_price']:$v['ItemDetail']['price'];
                                                    $disc       = $v['discount'];
                                                    $item_id    = $v['item_id'];

                                                    $percentage = 100 - ( $disc * 100 / $ori );

                                                    if( $percentage > $max_percentage ){
                                                        $max_percentage = $percentage;
                                                        $prev_original  = $ori;
                                                        $prev_discount  = $disc;
                                                    }else if( $percentage == $max_percentage ){
                                                        if( $disc < $prev_discount ){
                                                            $prev_original  = $ori;
                                                            $prev_discount  = $disc;
                                                        }
                                                    }

                                                }

                                                $original = "$" . number_format($prev_original, 2) ;
                                                $discount = "$" . number_format($prev_discount, 2);                        
                                            }
                                        ?>
                                            <li class="col-sm-3 col-xs-3">
                                                <div class="new-project bg-shadow add-margin-bottom deal-img-container">
                                                    <div>
                                                        <a href="<?php echo $this->Html->url(array('controller'=>'deals', 'action'=>'detail', $dnValue['Deal']['slug'])) ?>">
                                                            <img src="<?php echo $this->webroot . $image_thumb ?>" title="<?php echo $dnValue['Deal']['title'] ?>">
                                                        </a>
                                                    </div>
                                                    <div id="bx-pager">
                                                        <a data-slide-index="0" href="<?php echo $this->Html->url(array('controller'=>'deals', 'action'=>'detail', $dnValue['Deal']['slug'])) ?>" class="active">
                                                            <div class="new-project-body">
                                                                <h4><?php echo $deal_title ?></h4>
                                                                <i><?php echo $business_name ?></i><br>
                                                                <i class="icon-location"><?php echo $business_location . ", " . $dnValue['Province']['province_name'] ?></i>
                                                                <div class="clearfix"></div>
                                                                <h4 class="price-off pull-right"><?php echo $discount ?></h4>
                                                                <h4 style="color:#666; float:right; margin-right: 10px; text-decoration: line-through;"><?php echo $original ?></h4>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </li>
                                        <?php } ?>
                                    </div>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
        </div>
    <?php endif ?>

    <!-- Getaways Deal -->
    <?php if($getaways_deals): ?>
        <div class="container no-padding">
            <div class="recent-works col-sm-12 no-padding-left">
                <div class="recent-works-header clearfix">
                    <h2 style="width: 100%;">
                        <span style="float: left;"><?php echo __('Getaways') ?></span>
                        <span>
                            <a href="<?php echo $this->Html->url(array('controller'=>'deals', 'action' => 'index')) ?>?type=getaways" class="button btn-green pull-right" style="padding:10px; !important;"><i class="icon-list"></i> <?php echo __('View All') ?></a>
                        </span>
                    </h2>
                </div>
                <div class="recent-works-carousel">
                    <div class="carousel slide" id="Recentworks">
                        <div class="carousel-inner">
                            <div class="item active">
                                <ul class="thumbnails">
                                    <div class="row">
                                        <?php 
                                        foreach ($getaways_deals as $tKey => $tValue) { 
                                            $split = end(explode("/", $tValue['Deal']['image']));
                                            $thumb_img =  "img/deals/thumbs/" . $split ;
                                            $business_name = $tValue['Business']['business_name'];
                                            $business_street = $tValue['Business']['street'];
                                            $business_location = $tValue['Business']['location'];
                                            $deal_title = $tValue['Deal']['title'];
                                            if(strlen($deal_title) > $limit_deal_title){
                                                $deal_title = substr($tValue['Deal']['title'],0,$limit_deal_title-3).'...';   
                                            }
                                            if(strlen($business_name) > $limit_business_title){
                                                $business_name = substr($business_name,0,$limit_business_title-3).'...';   
                                            }

                                            $image = $tValue['Deal']['image'];
                                            $other_images = $tValue['Deal']['other_images'];
                                            $data_images = array();

                                            if( $other_images ){
                                                
                                                $other_images = json_decode($other_images);
                                                foreach( $other_images as $k => $img ){

                                                  if( $img != "img/deals/default.jpg" ){
                                                    $data_images[]['img'] = $img;
                                                  }
                                                }

                                            }else if( $image != "img/deals/default.jpg" ) {
                                              $data_images[]['img']   = $image;
                                            }

                                            $selected = @$data_images[array_rand($data_images)];

                                            if( $selected ){ 
                                                $image          = $selected['img'];
                                                $image_thumb    = str_replace("/deals/", "/deals/thumbs/", $image);
                                            }

                                            $original = "";
                                            $discount = $tValue['Deal']['discount_category'];

                                            $prev_original  = 0;
                                            $prev_discount  = 0;

                                            if( $items = $tValue['DealItemLink'] ){

                                                $max_percentage = 0;

                                                foreach( $items as $k => $v ){

                                                    $ori        = ($v['original_price'] != 0 )?$v['original_price']:$v['ItemDetail']['price'];
                                                    $disc       = $v['discount'];
                                                    $item_id    = $v['item_id'];

                                                    $percentage = 100 - ( $disc * 100 / $ori );

                                                    if( $percentage > $max_percentage ){
                                                        $max_percentage = $percentage;
                                                        $prev_original  = $ori;
                                                        $prev_discount  = $disc;
                                                    }else if( $percentage == $max_percentage ){
                                                        if( $disc < $prev_discount ){
                                                            $prev_original  = $ori;
                                                            $prev_discount  = $disc;
                                                        }
                                                    }

                                                }

                                                $original = "$" . number_format($prev_original, 2) ;
                                                $discount = "$" . number_format($prev_discount, 2);                        
                                            }

                                        ?>
                                            <li class="col-sm-3 col-xs-3">
                                                <div class="new-project bg-shadow add-margin-bottom deal-img-container">
                                                    <div>
                                                        <a href="<?php echo $this->Html->url(array('controller'=>'deals', 'action'=>'detail', $tValue['Deal']['slug'])) ?>">
                                                            <img src="<?php echo $this->webroot . $image_thumb ?>" title="<?php echo $tValue['Deal']['title'] ?>">
                                                        </a>
                                                    </div>
                                                    <div id="bx-pager">
                                                        <a data-slide-index="0" href="<?php echo $this->Html->url(array('controller'=>'deals', 'action'=>'detail', $tValue['Deal']['slug'])) ?>" class="active">
                                                            <div class="new-project-body">
                                                                <h4><?php echo $deal_title ?></h4>
                                                                <i><?php echo $business_name ?></i><br>
                                                                <i class="icon-location"><?php echo $business_location . ", " . $tValue['Province']['province_name'] ?></i>
                                                                <div class="clearfix"></div>
                                                                <h4 class="price-off pull-right"><?php echo $discount ?></h4>
                                                                <h4 style="color:#666; float:right; margin-right: 10px; text-decoration: line-through;"><?php echo $original ?></h4>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </li>
                                        <?php } ?>
                                    </div>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
        </div>
    <?php endif ?>
</div>
