<?php 

/********************************************************************************

File Name: detail.ctp
Description: displaying detail for business

Powered By: ABi Investment Group Co., Ltd,

Changed History:

    Date                Author              Description
    2014/06/10          Sim Chhayrambo      Initial Version
    2014/06/13          Sim Chhayrambo      Change UI
    2014/06/16          Sim Chhayrambo      Add marker to google map
    2014/06/19          Sim Chhayrambo      Add meta description
    2014/07/16          Sim Chhayrambo      Show Accept Payment
    2014/07/22          Sim Chhayrambo      Remove words "all deal" from meta description
    2015/02/25          Phea Rattana        check for deal image randomly to display,
                                            check price discount if available

*********************************************************************************/

if(!empty($businessInfo['Business'])){

$bussDealTitle = $limit_deal_title - 5;
$bussTitle = $limit_business_title - 5;
$strCategory = "";
$strPayment = "";

$arrMainCategory = $businessInfo['main_category'];
$arrSubCategory = $businessInfo['sub_category'];

$businessDeal = $businessInfo['Deals'];

$businessCity = $businessInfo['Cities']['city_name'];
$businessProvince = $businessInfo['Provinces']['province_name'];

$opHours = $businessInfo['OperationHour'];

$businessInfo = $businessInfo['Business'];
$businessName = $businessInfo['business_name'];
$businessStreet = $businessInfo['street'];
$businessLocation = $businessInfo['location'];
$businessEmail = $businessInfo['email'];
$businessPhone1 = $businessInfo['phone1'];
$businessPhone2 = $businessInfo['phone2'];
if(!empty($businessPhone2)){
    $businessPhone2 = " / " . $businessPhone2;
}
$businessLogo = $businessInfo['logo'];
// $businessWebsite = $businessInfo['website'];
$businessWebsite = str_replace("http://", "", $businessInfo['website']);
$businessWebsite = str_replace("https://", "", $businessInfo['website']);
$businessWebsite = str_replace("www.", "", $businessWebsite);
$businessWebsite = "www." . $businessWebsite;
$businessWebsite = trim($businessWebsite, '/');

$businessDescription = $businessInfo['description'];
$businessLat = $businessInfo['latitude'];
$businessLong = $businessInfo['longitude'];
$payment = json_decode($businessInfo['accept_payment']);
if(!empty($payment)){
    foreach( $payment as $key => $value ){
        $strPayment .= $this->Html->image( $value . '.jpg', array(  'alt' => ucfirst($value),
              'title'=>ucfirst($value),
              'style'=>'width:40px; margin-right:5px;'));
    }
}

if(!empty($arrMainCategory)){
    $businessMainCategory = $arrMainCategory['BusinessCategory']['category'];
}else{
    $businessMainCategory = "";
}

if(!empty($businessMainCategory)){
    if(!empty($arrSubCategory)){
        $businessSubCategory = $arrSubCategory['BusinessCategory']['category'];
    }else{
        $businessSubCategory = "";    
    }
}else{
    $businessSubCategory = "";
}

if(!empty($businessMainCategory)){
    $strCategory = $businessSubCategory . " - " . $businessMainCategory;
}else{
    $strCategory = $businessMainCategory;
}

$metaTitle = trim($businessName);
$metaInfo = strip_tags($businessDescription);
$newMetaDescription = $metaTitle . ". " . $metaInfo;

if(strlen($newMetaDescription) >= 300){
    $newMetaDescription = substr($newMetaDescription,0,300) . '...';
}

$metaDescription = $newMetaDescription;
$this->Html->meta('description', $metaDescription, array('inline'=>false));


$data_braches = array();
$time = time();

if( isset($branches) && !empty($branches) ){
  foreach( $branches as $k => $val ){
     $time += 300;

     $data_braches[$k]['index']         = $time;
     $data_braches[$k]['branch_name']   = $val['BusinessBranch']['branch_name'];
     $data_braches[$k]['latitude']      = $val['BusinessBranch']['latitude'];
     $data_braches[$k]['longitude']     = $val['BusinessBranch']['longitude'];
  }

}

?>

<style type="text/css">
.bussiness-container{
    width: 197px;
}
.bussiness-container img{
    width: 197px;
    height: 117px;
    
}
.info-container{

}
.info-container p{
    margin: 2px 0;
}
.info-container p span{
    float: left;
}
</style>

<div class="container">
    <div style="height: 20px;"></div>
    <div class="col-sm-12 no-padding clearfix">
        <div class="col-sm-3 no-padding">
            <img src="<?php echo $this->webroot . $businessLogo ?>" width="220">
        </div>
        <div class="col-sm-5 no-padding info-container">
            <h2 style="margin: 10px 0;"><?php echo $businessName; ?></h2>
            <div class="col-sm-12 no-padding">
                <p><?php echo __('Address') . " : " . $businessStreet . ", " . $businessLocation . ", " . $businessCity;?></p>
                <p><?php echo __('Phone') . " : " . $businessPhone1 . $businessPhone2 ?></p>
                <p><?php echo __('Email') . " : " . $businessEmail ?></p>
                <p>
                    <?php echo __('Website') . " : ";?>
                    <a href="http://<?php echo $businessWebsite ?>" target="_blank" style="color: #ff9311; text-decoration: underline;"><?php echo $businessWebsite ?></a>
                </p>
                <p>
                    <?php 
                        echo __('Category') . " : " . $strCategory; 
                    ?>
                </p>
                <p>
                    <span style="margin-top: 3px;"><?php echo __('Accept Payment') . " : ";  ?></span>
                    <span style="margin-left:3px;"><?php echo $strPayment ?></span>
                </p>
            </div>
        </div>
        <div class="col-sm-4">
            <h2 style="margin: 10px 0;"><?php echo __('Operation Hours') ?></h2>
            <?php 
            if(!empty($opHours)){
                foreach ($opHours as $opKey => $opValue) { 
                    $opDay = $opValue['day'];
                    $opDay = substr($opDay, 0, 3);
                    if(!empty($opValue['from'])){
                        $opFrom = date("h:i A", strtotime($opValue['from']));
                    }else{
                        $opFrom = "";
                    }
                    
                    if(!empty($opValue['to'])){
                        $opTo = date("h:i A", strtotime($opValue['to']));   
                    }else{
                        $opTo = "";
                    }

                    if( !$opFrom && !$opTo ){
                        $opFrom = "Closed";
                        $opTo = "";
                    }else if( $opFrom == $opTo ){
                        $opFrom = "24 Hours";
                        $opTo   = "";
                    }else{
                        if( !$opFrom ){
                            $opFrom = "<i>Empty</i>";
                        }
                        if( !$opTo ){
                            $opTo = "<i>Empty</i>";
                        }
                    }

                    if( $opTo ){
                        $opTo = " - " . $opTo ;
                    }

                    echo "<p>";
                    echo "<span style='display:inline-block; width:48px; color: orange;'>" . strtoupper($opDay) . " : " . "</span>";
                    echo "<span>" . $opFrom . $opTo . "</span>";
                    echo "</p>";

                } 
            }else{
                echo "No Operation Hour";
            } ?>
        </div>
    </div>
    <div style="height: 20px;"></div>
    <div class="col-sm-8 no-padding">
        <div class="col-sm-12 no-padding" style="margin:10px 0 20px 0;">
            <h2><?php echo __('About') . " " . $businessName ?></h2>
            <?php 
                if(!empty($businessDescription)){
                    echo $businessDescription;
                }else{
                    echo __('No description for this business.');
                }
            ?>
        </div>
        <?php 
        if (!empty($businessDeal)) { ?>
        <div class="container no-padding-left">
            <div class="recent-works col-sm-8 no-padding-left">
                <div class="recent-works-header clearfix">
                    <h2 style="width: 100%;">
                        <span style="float: left;"><?php echo __('Current Deals') ?></span>
                        <!-- <span>
                            <a href="#" class="button btn-green pull-right"><i class="icon-plus"></i> View All</a>
                        </span> -->
                    </h2>
                </div>
                <div class="recent-works-carousel">
                    <div class="carousel slide" id="Recentworks">
                        <div class="carousel-inner">
                            <div class="item active">
                                <ul class="thumbnails">
                                    <div class="row">
                                        <?php 
                                        $now = date("Y-m-d H:i:s");

                                        foreach($businessDeal as $busKey => $busValue) { 
                                        
                                            $split = end(explode("/", $busValue['Deal']['image']));
                                            $thumb_img =  "img/deals/thumbs/" . $split ;
                                            $deal_title = $busValue['Deal']['title'];
                                            if(strlen($deal_title) > $bussDealTitle){
                                                $deal_title = substr($busValue['Deal']['title'],0,$bussDealTitle-3).'...';   
                                            }
                                            if(strlen($businessName) > $bussTitle){
                                                $businessName = substr($businessName,0,$bussTitle-3).'...';   
                                            }

                                            if( $other_deal_imgs = $busValue['Deal']['other_images'] ){
                                                $imgs = json_decode($other_deal_imgs);
                                                $data_img = array();

                                                foreach( $imgs as $k => $img ){
                                                    $data_img[] = $img;
                                                }

                                                $selected = array_rand($data_img);

                                                $thumb_img = str_replace("/deals/", "/deals/thumbs/", $data_img[$selected] );
                                            }
                                        
                                            $original = "";
                                            $discount = $busValue['Deal']['discount_category'];

                                            $prev_original  = 0;
                                            $prev_discount  = 0;

                                            if( $items = $busValue['DealItemLink'] ){

                                                $max_percentage = 0;

                                                foreach( $items as $k => $v ){

                                                    $ori        = ($v['original_price'] != 0 )?$v['original_price']:$v['ItemDetail']['price'];
                                                    $disc       = $v['discount'];
                                                    $item_id    = $v['item_id'];

                                                    $percentage = 100 - ( $disc * 100 / $ori );

                                                    if( $percentage > $max_percentage ){
                                                        $max_percentage = $percentage;
                                                        $prev_original  = $ori;
                                                        $prev_discount  = $disc;
                                                    }else if( $percentage == $max_percentage ){
                                                        if( $disc < $prev_discount ){
                                                            $prev_original  = $ori;
                                                            $prev_discount  = $disc;
                                                        }
                                                    }

                                                }

                                                $original = "$" . number_format($prev_original, 2) ;
                                                $discount = "$" . number_format($prev_discount, 2);                        
                                            }

                                        ?>
                                            <li class="col-sm-4 col-xs-4">
                                                <div class="new-project bg-shadow add-margin-bottom bussiness-container">
                                                    <div>
                                                        <a href="<?php echo $this->Html->url(array('controller'=>'deals', 'action'=>'detail', $busValue['Deal']['slug'])) ?>">
                                                            <img src="<?php echo $this->webroot . $thumb_img ?>" title="<?php echo $busValue['Deal']['title'] ?>">
                                                        </a>
                                                    </div>
                                                    <div id="bx-pager">
                                                        <a data-slide-index="0" href="<?php echo $this->Html->url(array('controller'=>'deals', 'action'=>'detail', $busValue['Deal']['slug'])) ?>" class="active">
                                                            <div class="new-project-body">
                                                                <h4><?php echo $deal_title ?></h4>
                                                                <i><?php echo $businessName ?></i><br>
                                                                <i class="icon-location"><?php echo $businessLocation . ", " . $businessProvince ?></i>
                                                                <div class="clearfix"></div>
                                                                <h4 class="price-off pull-right"><?php echo $discount ?></h4>
                                                                <h4 style="color:#666; float:right; margin-right: 10px; text-decoration: line-through;"><?php echo $original ?></h4>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </li>
                                        <?php } ?>
                                    </div>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
            <!-- <div class="recent-works col-sm-3 no-padding"></div> -->
        </div>
        <?php } ?>

        <div class="col-sm-12 no-padding">
            <?php 
            if(!empty($businessMedia)){ ?>
            <div class="col-sm-12 no-padding">
                <article id="article">
                    <div class="article-entry">
                        <div class="recent-works">
                            <div class="recent-works-header clearfix">
                                <h2><?php echo __('Image Gallery') ?></h2>
                            </div>
                            <div class="recent-works-carousel">
                                <div class="carousel slide" id="Recentworks">
                                    <div class="carousel-inner">
                                        <div class="item active">
                                            <ul class="thumbnails">
                                                <div class="row">
                                                    <?php
                                                    foreach($businessMedia as $bKey => $bValue) { 
                                                        $strLeft = "";
                                                        if($bKey == 0) $strLeft = "style='left:70%;'";
                                                        $media_path = $bValue['BusinessesMedia']['media_path'];
                                                        if(!empty($media_path)){
                                                    ?>
                                                    <li class="col-sm-3 col-xs-3">
                                                        <div class="img-overlay">
                                                            <img src="<?php echo $this->webroot . $media_path ?>" width="100%" height="137">
                                                            <div class="recent-icons" <?php echo $strLeft; ?>>
                                                                <a href="<?php echo $this->webroot . $media_path ?>" class="fancybox"><i class="icon-search"></i></a>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <?php } } ?>
                                                </div>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </div>
            <?php if(!empty($businessMedia[0]['BusinessesMedia']['media_embeded'])) { ?>
            <div class="col-sm-12 clearfix" style="border: 1px solid #f2f2f2; padding: 10px 0; margin-bottom: 20px;">
                <h2 style="margin: 10px 15px; font-weight: bold;"><?php echo __('Videos') ?></h2>
                <?php 
                foreach ($businessMedia as $buKey => $buValue) { 

                    $media_embeded = $buValue['BusinessesMedia']['media_embeded'];
                    $media_embeded = explode("?v=", $media_embeded);

                    $video_id = end($media_embeded);

                    if(!empty($video_id)){
                ?>
                    <div class="col-sm-6">
                        <iframe 
                            width="100%" height="220"
                            src="<?php echo "http://www.youtube.com/embed/" . $video_id ?>" allowfullscreen>
                        </iframe>
                    </div>
                <?php } } ?>
            </div>
            <?php } } ?>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="col-sm-12" style="width:100%; height:250px; margin:0 auto;" id="map-canvas"></div>
    </div>
</div>



<?php if(!empty($businessLat) && !empty($businessLong)){ 
    // $mapBusinessName = str_replace("'", "", $businessName);
?>

<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
<script src="https://google-maps-utility-library-v3.googlecode.com/svn/tags/markerwithlabel/1.1.9/src/markerwithlabel.js" type="text/javascript"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

<script>

    
    var listBranchMarker = {};
    var map = null;
    var pin_icon_blue = '<?php echo $this->webroot ?>' + 'img/pin-blue.png';
    var iw = null;

    function addBranchMarker(index,latLng, brandName, latLngText) {

        listBranchMarker[index] = new MarkerWithLabel({
                map:map,
                position:latLng,
                icon: pin_icon_blue,
                labelContent: brandName  ,
                title:brandName,

                labelAnchor : new google.maps.Point(45, 67),
                labelClass : "labels-branch",
                labelInBackground : false
            });

        var b_name = new google.maps.InfoWindow({ content: brandName + latLngText });

        google.maps.event.addListener( listBranchMarker[index], "click", function (e) { b_name.open(map, this); });
        
    }

    function initialize() {


      var  latitude  = <?php echo $businessLat ?>,
           longitude = <?php echo $businessLong ?>,
           myLatlng  = new google.maps.LatLng( latitude, longitude ),
           businessesName = "<?php echo $businessName ?>";

      var image = '<?php echo $this->webroot ?>' + 'img/pin-yellow.png';

      var mapOptions = {
        zoom: 12,
        center: myLatlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      }

      map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

      var marker = new MarkerWithLabel({
              map: map,
              position: myLatlng,
              icon : image,
              labelContent : businessesName,
              labelAnchor : new google.maps.Point(26, 64),
              labelClass : "labels",
              labelColor : "red",
              labelInBackground : false
          });

        var iw = new google.maps.InfoWindow({
                       content: businessesName
                     });


        google.maps.event.addListener(marker, "click", function (e) { iw.open(map, this); });

        var data_branch = <?php echo json_encode($data_braches) ?>;
        $.each( data_branch, function( ind, val ){
            var name  = val.branch_name,
                lat   = val.latitude,
                lng   = val.longitude,
                index = val.index;

            var newLatLng  = new google.maps.LatLng(lat, lng );

            latLngText = " (" + lat + ", " + lng + ")";
            addBranchMarker( index ,newLatLng, name, latLngText );
        })

    }

    google.maps.event.addDomListener(window, 'load', initialize);

</script>

<?php } ?>

<?php 

}else{ require_once(dirname(__FILE__).'/../Elements/not_found.ctp'); }

?>