<?php 


/********************************************************************************

File Name: File Name
Description: Description

Powered By: ABi Investment Group Co., Ltd,

Changed History:

    Date                            Author              Description
    31/Aug/2015                     Phea Rattana          Initialize Version

*********************************************************************************/

?>

<style>
	
</style>

<div class="full-width" style="z-index:9999;">	
	<div class="background"></div>
	<div class="overlay"></div>
	<div class="merchant-top-bar" style="z-index: 999">
		<div style="margin:0 auto; width: 970px;">
			<a href="<?php echo $this->Html->url(array('controller'=>'index', 'action'=>'index')); ?>">
			    <h1 style="width: 100px; margin-top:12px; float:left;">
			        <img src="<?php echo $this->webroot . "img/logo/AD_LOGO.png"?>" width="100%">
			    </h1>
			</a>
		</div>
	</div>
	<div class="welcome-merchant" style="margin-bottom: 100px;">
		<div class="clearfix" style="margin-bottom: 100px;"></div>

		<div class="col-sm-7 welcome">
			<h1>Welcome to All Deal,</h1>
			<br>
			<p>All Deal is a set of mobile phone applications and websites that attract customers to visit and try your products and services.</p><br>

			<h2>Why All Deal?</h2><br>
			<ul>
				<li>A chance to meet a lot more customers, a chance to let them try your products or services, and a chance to keep them as your loyal customers. We bring customers to you. You have focus on your products and services, managing your store and staffs.</li><br>

				<li>No upfront cost: you do not pay anything beforehand. You will pay the commission fee to us based on how many deals we can sell for you through All Deal.</li><br>

				<li>We bring you online for free. By working with All Deal, you will be able to have a merchant profile for free. Merchant Profile can be used as a subdomain for your business. All Deal users also can subscribe to your Merchant Profile and your subscribers will automatically receive notifications from their smartphone whenever you have a new deal.</li><br>

				<li>You can easily track the performance of your promotion through our website Merchant Management Page.</li> 

				<li>We have a graphic design to make sure your promotion pictures will look effortless on our website and smartphone application.</li><br>

				<li>We know that every business has their high and low season. All Deal is the best way to bring customers in your store in the low season or time of the day that has less customers. Attract them with your great deals. </li><br>

				<li>Our team has worked with a lot of merchants to create the most successful promotions to promote their business. We will sit down and give you ideas on how to make a successful promotions that will turn out tons of customers. </li>

			</ul>

		</div>

		<div class="col-sm-5">
			<div id="log-in-form">
				<form action="<?php echo $this->webroot . 'member/login' ?>" class="form-horizental" method="POST">
					<legend style="color: #8dc63f "><h2>Merchant Login</h2></legend><hr>

					<?php echo $this->Session->flash(); ?>
					
					<input type="email" class="form-control" placeholder="Email" required="required" name="data[User][email]">
					<div class="clearfix" style="padding-bottom:10px;"></div>
					<input type="password" class="form-control pull-left" name="data[User][password]" placeholder="Password" style="width:75%" required="required">

					<input type="hidden" name="from-website" >
					<button type="submit" class="btn btn-primary pull-right">LOG IN</button>

					<div class="clearfix" style="padding-bottom:20px;"></div>
					<a href="<?php echo $this->Html->url( array('action' => 'forgot', 'plugin' => 'member', 'controller' => 'MemberUsers' )); ?>" style="float:right; color: #8dc63f ; font-size:13px;">Forgot your password ?</a>
					<div class="clearfix" style="paddig-bottom:20px;"></div>
				</form>
			</div>
			
			<div class="clearfix" style="margin-bottom: 20px;"></div>
			<div id="log-in-form">
				<form action="" class="form-horizental" method="POST">
					<legend style="color: #8dc63f "><h2>SIGN UP</h2></legend><hr>
					<p style="color:#000;">Get a free merchant account, sign up now !</p><br>
					<a href="<?php echo $this->Html->url( array('controller' => 'businesses', 'action' => 'register')); ?>" class="btn btn-primary pull-right">SIGN UP NOW !</a>					
					<div class="clearfix" style="padding-bottom:20px;"></div>

				</form>
			</div>

		</div>

	</div>
	<div class="clearfix" style="margin-bottom: 100px;"></div>

	<div style="position: fixed; bottom:0px; left:0px; background: #222; height: 30px; width: 100%;">
		<div style="font-size: 13px; color: white; text-align:center; padding-top:5px;">
            <span>&copy <?php echo __(' 2015 All Deal, All Rights Reserved.') ?></span>
            <span class='footer-copyright-links'>
                <a href="<?php echo $this->Html->url(array('controller'=>'termOfUses', 'action'=>'index')) ?>" target="_blank"><?php echo __('Terms of Use'); ?></a> and 
                <a href="<?php echo $this->Html->url(array('controller'=>'privacyPolicies', 'action'=>'index')) ?>" target="_blank"><?php echo __('Privacy Policy'); ?></a>
            </span>
        </div>
	</div>

</div>

<script>
	
	var imgs = ["img/merchant-bgs/1.jpg", "img/merchant-bgs/2.jpg", "img/merchant-bgs/3.jpg", "img/merchant-bgs/4.jpg"];

	var count = imgs.length;
	var i = 1;

	setInterval(function() { 
	    if(i == count) i=0;
	    img = imgs[i];
	   	
	   	var div = $('div.background');
	   	div.fadeOut(500, function () {
	        div.css( {"background": "url(" + img + ") no-repeat center center fixed", 
					    "-webkit-background-size": "cover",
					    "-moz-background-size": "cover",
					    "-o-background-size": "cover",
					    "background-size": "cover",
					} );

	        div.fadeIn(1000);
	    });	    
	    i++;
	}, 5000);

</script>
