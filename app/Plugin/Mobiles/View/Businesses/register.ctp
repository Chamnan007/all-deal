<?php 

/********************************************************************************

File Name: register.ctp
Description: for business registration form

Powered By: ABi Investment Group Co., Ltd,

Changed History:

    Date                Author              Description
    2014/06/10          Sim Chhayrambo      Initial Version
    2014/06/11          Sim Chhayrambo      Change UI
    2014/06/12          Sim Chhayrambo      Add "*" to required fields
    2014/06/19          Sim Chhayrambo      Add meta description
    2014/06/27          Sim Chhayrambo      Check Validation
    2014/07/10          Sim Chhayrambo      Add link for Terms of Use and Privacy Policy
    2014/07/29          Sim Chhayrambo      Allow space in first name and last name
    2014/08/01          Sim Chhayrambo      Fix style in ie9

*********************************************************************************/

$currentYear = date("Y");
$fromYear = $currentYear - 120;
$toYear = $currentYear;

$metaDescription = substr("All Deal: Merchant Registration",0,300).'...';
$this->Html->meta('description', $metaDescription, array('inline'=>false));

if(!empty($this->params['pass'][0])){
    $paramVerified = $this->params['pass'][0];
}else{
    $paramVerified = "";
}

?>

<?php 
if($paramVerified != "verified"){
?>

<style type="text/css">
.register-form label{
    width: 140px;
}
.alert-error{
    color: red;
    background: rgb(200, 54, 54); /* The Fallback */
    background: rgba(200, 54, 54, 0.5);
}
.alert-dismissable .close{
    top: -35px;
    right: -30px;
    color: #666;
}
</style>

<div class="register-form clearfix">
    <form id="frmAdd" method="post" action="<?php echo $this->Html->url(array('controller'=>'users', 'action'=>'add')) ?>" enctype="multipart/form-data">
        <h1><?php echo __('Registration Form (Merchant Only)'); ?></h1>
        <input type="hidden" name="check_email" id="check_email">
        <div class="col-sm-12 no-padding clearfix">
            <div class="col-sm-6 no-padding" style="border-right: 1px solid #DDD">
                <fieldset>
                    <legend><?php echo __('Merchant Info') ?></legend>
                    <ol>
                        <li>
                            <label class="required"><?php echo __('Name') ?></label>
                            <input type="text" class="required" id="businessName" name="data[Business][business_name]" placeholder="<?php echo __('Merchant Name') ?>" pattern=".{1,30}" required title="30 characters maximum" value="">
                        </li>
                        <li>
                            <label class="required"><?php echo __('Main Category') ?></label>
                            <select name="data[Business][business_main_category]" id="mainCategory" required>
                                <option value=""><?php echo __('Main Category') ?></option>
                                <?php 
                                foreach ($categories as $catKey => $catValue) { ?>
                                <option value="<?php echo $catValue['BusinessCategory']['id'] ?>"><?php echo $catValue['BusinessCategory']['category'] ?></option>
                                <?php } ?>
                            </select>
                        </li>
                        <!-- <li>
                            <label><?php echo __('Sub Category') ?></label>
                            <select name="data[Business][business_sub_category]" id="subCategory">
                                <option value=""><?php echo __('Sub Category') ?></option>
                            </select>
                        </li> -->
                        <li>
                            <label class="required"><?php echo __('Phone') ?></label>
                            <input type="text" class="input_phone" id="businessPhone" name="data[Business][phone]" placeholder="<?php echo __('Phone') ?>" pattern=".{1,11}" required title="11 characters maximum, number only">
                        </li>
                        <li>
                            <label class="required"><?php echo __('Email') ?></label>
                            <input type="email" class="input_email" id="businessEmail" name="data[Business][email]" placeholder="<?php echo __('Email') ?>" required>
                        </li>
                        <li>
                            <label class="required"><?php echo __('Street') ?></label>
                            <input type="text" id="street" name="data[Business][street]" placeholder="<?php echo __('Street') ?>" pattern=".{1,30}" required title="30 characters maximum">
                        </li>
                        <li>
                            <label class="required"><?php echo __('Province/ City') ?></label>
                            <select name="data[Business][city]" id="province" required>
                                <option value=""><?php echo __('Select Province') ?></option>
                                <?php foreach ($provinceList as $proKey => $proValue) {
                                    echo "<option value='".$proValue['City']['city_code']."'>".$proValue['City']['city_name']."</option>";
                                } ?>
                            </select>
                        </li>
                        <li>
                            <label class="required"><?php echo __('Location/ Khan') ?></label>
                            <select name="data[Business][location]" id="location" required>
                                <option value="" data-id=""><?php echo __('Select Location') ?></option>
                            </select>
                            <input type="hidden" name="data[Business][location_id]" id="location-id">
                        </li>

                        <li>
                            <label><?php echo __('Sangkat') ?></label>
                            <select name="data[Business][sangkat_id]" id="sangkat-select">
                                <option value=""><?php echo __('Select Sangkat') ?></option>
                            </select>
                        </li>
                    </ol>
                </fieldset>

            </div>
            <div class="col-sm-6">
                <fieldset>
                    <legend><?php echo __('Login Info') ?></legend>
                    <ol>
                        <li>
                            <label class="required"><?php echo __('First Name') ?></label>
                            <input type="text" id="userFirstName" class="user_name" name="User[first_name]" placeholder="<?php echo __('First Name') ?>" pattern=".{1,15}" required title="15 characters maximum" value="">
                        </li>
                        <li>
                            <label class="required"><?php echo __('Last Name') ?></label>
                            <input type="text" id="userLastName" name="User[last_name]" class="user_name" placeholder="<?php echo __('Last Name') ?>" pattern=".{1,15}" required title="15 characters maximum" value="">
                        </li>
                        <li>
                            <label class="required"><?php echo __('Date of Birth') ?></label>
                            <select style="width:75px;" id="userDOBDay" name="User[dob][day]" required>
                                <option value=""><?php echo __('Day') ?></option>
                                <?php for ($i=1; $i < 32 ; $i++) { 
                                    echo "<option value='".$i."'>".$i."</option>";
                                } ?>
                            </select>
                            <select style="width:80px;" id="userDOBMonth" name="User[dob][month]" required>
                                <option value=""><?php echo __('Month') ?></option>
                                <?php foreach($mons as $key => $value){
                                    echo "<option value='".$key."'>".$value."</option>";
                                } ?>
                            </select>
                            <select style="width:75px;" id="userDOBYear" name="User[dob][year]" required>
                                <option value=""><?php echo __('Year') ?></option>
                                <?php 
                                for ($i=$fromYear; $i <= $toYear ; $i++) { 
                                    echo "<option value='".$i."'>".$i."</option>";
                                } ?>
                            </select>
                        </li>
                        <li>
                            <label><?php echo __('Phone') ?></label>
                            <input type="text" class="input_phone" id="user_phone" name="User[phone]" placeholder="<?php echo __('Phone') ?>" title="11 characters maximum, number only">
                        </li>
                        <li>
                            <label class="required"><?php echo __('Email') ?></label>
                            <input type="email" class="input_email" id="user_email" name="User[email]" placeholder="<?php echo __('Email') ?>" required value="">
                            <div class="show-error">
                                <p><?php echo __('This email is already registered.'); ?></p>
                            </div>
                        </li>
                        <li>
                            <label class="required"><?php echo __('Password') ?></label>
                            <input type="password" name="User[password]" class="input_password" id="password" placeholder="<?php echo __('Password') ?>" pattern=".{6,30}" required title="Password must contain 6-30 characters" >
                            <div class="show-error">
                                <p><?php echo __('Password must contain 6-30 characters.'); ?></p>
                            </div>
                        </li>
                        <li>
                            <label class="required"><?php echo __('Confirm password') ?></label>
                            <input type="password" name="User[confirm_password]" class="input_password" id="confirm_password" placeholder="<?php echo __('Confirm Password') ?>" pattern=".{6,30}" required title="Password must contain 6-30 characters">
                            <div class="show-error">
                                <p><?php echo __('Password does not match.'); ?></p>
                            </div>
                        </li>
                        <li>
                            <span style="float:left; width:100%;">
                                <input type="checkbox" name="agreed" id="agreed" style="width: 20px; margin-top: 9px;" value="1">
                                <label for="agreed" style="float:right; width:93%; text-align:left;">I have read and agreed to the <a href="<?php echo $this->Html->url(array('controller'=>'termOfUses', 'action'=>'index')) ?>" target="_blank">Terms of Use</a> and <a href="<?php echo $this->Html->url(array('controller'=>'privacyPolicies', 'action'=>'index')) ?>" target="_blank">Privacy Policy</a></label>
                            </span>
                        </li>
                    </ol>
                </fieldset>
            </div>
        </div>
        <!-- <fieldset>
            <button type="submit" id="btn-register"><?php echo __('REGISTER') ?></button>
            <div class="ajax-loading" style="width: 100%; text-align: center; display:none;">
                <img src="<?php echo $this->webroot . "img/bx_loader.gif" ?>">
            </div>
        </fieldset> -->
        <div class="col-sm-12 no-padding clearfix" style="text-align:center;">
            <button type="submit" id="btn-register"><?php echo __('REGISTER') ?></button>
            <div class="ajax-loading" style="width: 100%; text-align: center; display:none;">
                <img src="<?php echo $this->webroot . "img/bx_loader.gif" ?>">
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">

$(function(){

    // 2014-08-08
    function msieversion() {

            var ua = window.navigator.userAgent;
            var msie = ua.indexOf("MSIE ");
            var ieversion;

            if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer, return version number
                return parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)));
            else                 // If another browser, return 0
                return false;
    }

    var checkIEBrowser = msieversion();
    // END 2014-08-08

    $("select#province").change( function (){
        var me = $(this);

        var locationSelect  = $("select#location");
        var sangkatSelect   = $('select#sangkat-select')
        var city_code       = me.val();

        var url = '<?php echo $this->Html->url(array("controller"=>"Locations", "action"=>"get_location_by_city_code")) ?>' + "/"  + city_code ;

        if( city_code != '' ){
            $.ajax({
                type: 'POST',
                url: url,
                data: { city_code : city_code },
                dataType: 'json',
                
                beforeSend: function(){
                    locationSelect.html("<option value=''>Loading...</option>");
                    sangkatSelect.html("<option value=''>Loading...</option>");
                },
                success: function (data){

                    var data = data.data;

                    locationSelect.html("<option value=''>Select location/Khan</option>");
                    sangkatSelect.html("<option value=''>Select Sangkat</option>");
                    
                    $.each( data, function (ind, val ){
                        var loc = val.Location;

                        locationSelect.append("<option value='" + loc.location_name + "' data-id='"+ loc.id +"'>" + loc.location_name + "</option>" )
                    });
                },
                error: function ( err ){
                    console.log(err);
                }

            });

        }else{

            locationSelect.html("<option value=''>select an option</option>");
            sangkatSelect.html("<option value=''>select an option</option>");

        }

    });


    $('select#location').change( function(){

        var me      = $(this);
        
        var optoin_selected = $('select#location option:selected');
        var location_id     = optoin_selected.attr('data-id');
        var sangkatSelect   = $('select#sangkat-select');

        $('input#location-id').val(location_id);

        var url = '<?php echo $this->webroot ?>' + 'sangkats/getSangkatByLocation/' + location_id ;

        if( location_id != '' ){
            $.ajax({
                type: 'POST',
                url: url,
                data: { location_id : location_id },
                dataType: 'json',
                
                beforeSend: function(){
                    sangkatSelect.html('<option>Loading...</option>');
                },
                success: function (result){
                    var data = result.data;

                    console.log(data);

                    sangkatSelect.html("<option value=''>select an option</option>");

                    $.each( data, function (ind, val ){
                      var sk = val.Sangkat;
                      sangkatSelect.append("<option value='" + sk.id + "'>" + sk.name + "</option>" );

                    });

                },

                error: function(xhr, status, error) {
                  var err = eval("(" + xhr.responseText + ")");
                    console.log(err);
                }

            });

        }else{
            sangkatSelect.html("<option value=''>select an option</option>");
        }
    })


    $('form#frmAdd label.required').append("<span class='red-star'> * </span>"); //add red star for all required fields

    $('input.input_phone').attr({'maxlength': 11}); //set maxlength for phone

    $("input#user_email").blur(function(){

        var me = $(this),
            user_email = me.val();

        if(user_email != ''){

            if(validateEmail(user_email)){

                var url = '<?php echo $this->Html->url(array("controller"=>"users", "action"=>"check_existing_email")) ?>' + "/"  + user_email ;

                if( user_email != '' ){
                    $.ajax({
                        type: 'POST',
                        url: url,
                        data: { user_email : user_email },
                        dataType: 'json',
                        
                        beforeSend: function(){
                        },
                        success: function (data){

                            var result = data.result;

                            if(result){

                                // $.msgBox({
                                //     title: "Failed",
                                //     content: "Email is already registered !",
                                //     type: "error",
                                //     buttons: [{ value: "Ok" }],
                                // });
                                // me.parent().find("div.show-error").fadeIn();
                                // me.focus();
                                $('input#check_email').val(1);
                                
                            }else{
                                // me.parent().find("div.show-error").fadeOut();
                                $('input#check_email').val(0);
                            }

                        },

                        error: function(xhr, status, error) {
                            var err = eval("(" + xhr.responseText + ")");
                            console.log(err);
                        }

                    });

                }
            }else{

                
                // $.msgBox({
                //     title: "Failed",
                //     content: "Invalid email !",
                //     type: "error",
                //     buttons: [{ value: "Ok" }],
                // });
                // me.focus();

            }

        }else{

            
            // $.msgBox({
            //     title: "Failed",
            //     content: "Email can not be empty !",
            //     type: "error",
            //     buttons: [{ value: "Ok" }],
            // });
            // me.focus();

        }

    });

    $("input.input_phone").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) || 
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });

    $("input.user_name").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190, 32]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) || 
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a alphabet and stop the keypress
        if (e.keyCode < 65 || e.keyCode > 90) {
            e.preventDefault();
        }
    });


    $( "#frmAdd" ).submit(function( event ) {

        var check_email = $("input#check_email").val(),
            password = $("input#password").val(),
            confirm_password = $("input#confirm_password").val();

        // 2014-08-08
        var business_name = $("input#businessName").val(),
            mainCategory = $("select#mainCategory").val(),
            businessPhone = $("input#businessPhone").val(),
            businessEmail = $("input#businessEmail").val(),
            street = $("input#street").val(),
            province = $("select#province").val(),
            city = $("select#city").val(),
            location = $("select#location").val(),
            userFirstName = $("input#userFirstName").val(),
            userLastName = $("input#userLastName").val(),
            userDOBDay = $("select#userDOBDay").val(),
            userDOBMonth = $("select#userDOBMonth").val(),
            userDOBYear = $("select#userDOBYear").val(),
            user_phone = $("input#user_phone").val(),
            user_email = $("input#user_email").val();
        // END



        var btnRegister = $('button#btn-register'),
            divAjax = $('div.ajax-loading'),
            chkAgreed = $('input#agreed');

        // 2014-08-08
        if(checkIEBrowser){
            if(business_name == "" || business_name.length > 30){
                $.msgBox({
                    title: "Failed",
                    content: "Business Name can not be empty and must be less than 30 characters !",
                    type: "error",
                    buttons: [{ value: "Ok" }],
                });
                event.preventDefault();
            }else if(mainCategory == ""){
                $.msgBox({
                    title: "Failed",
                    content: "Main Category can not be empty !",
                    type: "error",
                    buttons: [{ value: "Ok" }],
                });
                event.preventDefault();   
            }else if(businessPhone == ""){
                    $.msgBox({
                        title: "Failed",
                        content: "Business Phone can not be empty !",
                        type: "error",
                        buttons: [{ value: "Ok" }],
                    });
                    event.preventDefault();
            }else if(businessEmail == "" || !validateEmail(businessEmail)){
                    $.msgBox({
                        title: "Failed",
                        content: "Business Email can not be empty and must be valid !",
                        type: "error",
                        buttons: [{ value: "Ok" }],
                    });
                    event.preventDefault();
            }else if(street == ""){
                    $.msgBox({
                        title: "Failed",
                        content: "Street can not be empty !",
                        type: "error",
                        buttons: [{ value: "Ok" }],
                    });
                    event.preventDefault();
            }else if(province == ""){
                    $.msgBox({
                        title: "Failed",
                        content: "Province can not be empty !",
                        type: "error",
                        buttons: [{ value: "Ok" }],
                    });
                    event.preventDefault();
            }else if(city == ""){
                    $.msgBox({
                        title: "Failed",
                        content: "City can not be empty !",
                        type: "error",
                        buttons: [{ value: "Ok" }],
                    });
                    event.preventDefault();
            }else if(location == ""){
                    $.msgBox({
                        title: "Failed",
                        content: "Location can not be empty !",
                        type: "error",
                        buttons: [{ value: "Ok" }],
                    });
                    event.preventDefault();
            }else if(userFirstName == "" || userFirstName.length > 15){
                    $.msgBox({
                        title: "Failed",
                        content: "First Name can not be empty and must be less than 15 characters !",
                        type: "error",
                        buttons: [{ value: "Ok" }],
                    });
                    event.preventDefault();
            }else if(userLastName == "" || userLastName.length >15){
                    $.msgBox({
                        title: "Failed",
                        content: "Last Name can not be empty and must be less than 15 characters !",
                        type: "error",
                        buttons: [{ value: "Ok" }],
                    });
                    event.preventDefault();
            }else if(userDOBDay == ""){
                    $.msgBox({
                        title: "Failed",
                        content: "Day can not be empty !",
                        type: "error",
                        buttons: [{ value: "Ok" }],
                    });
                    event.preventDefault();
            }else if(userDOBMonth == ""){
                    $.msgBox({
                        title: "Failed",
                        content: "Month can not be empty !",
                        type: "error",
                        buttons: [{ value: "Ok" }],
                    });
                    event.preventDefault();
            }else if(userDOBYear == ""){
                    $.msgBox({
                        title: "Failed",
                        content: "Year can not be empty !",
                        type: "error",
                        buttons: [{ value: "Ok" }],
                    });
                    event.preventDefault();
            }else if(user_email == "" || !validateEmail(user_email)){
                    $.msgBox({
                        title: "Failed",
                        content: "Login Email can not be empty and must be valid !",
                        type: "error",
                        buttons: [{ value: "Ok" }],
                    });
                    event.preventDefault();
            }else if(password == "" || password.length <6){
                    $.msgBox({
                        title: "Failed",
                        content: "Password can not be empty and must be at least 6 characters !",
                        type: "error",
                        buttons: [{ value: "Ok" }],
                    });
                    event.preventDefault();
            }else if(confirm_password == "" || confirm_password.length < 6){
                    $.msgBox({
                        title: "Failed",
                        content: "Confirm Password can not be empty and must be at least 6 characters !",
                        type: "error",
                        buttons: [{ value: "Ok" }],
                    });
                    event.preventDefault();
            }else{
                if(chkAgreed.is(':checked')){

                    btnRegister.hide();
                    divAjax.show();

                    if(check_email == 1){

                        btnRegister.show();
                        divAjax.hide();

                        $.msgBox({
                            title: "Failed",
                            content: "Email is already registered !",
                            type: "error",
                            buttons: [{ value: "Ok" }],
                        });
                        // me.parent().find("div.show-error").fadeIn();
                        // me.focus();

                        event.preventDefault();

                    }else{

                        if(password != confirm_password){

                            btnRegister.show();
                            divAjax.hide();
                            
                            $.msgBox({
                                title: "Failed",
                                content: "Password does not match !",
                                type: "error",
                                buttons: [{ value: "Ok" }],
                            });
                            event.preventDefault();

                        }
                    }

                }else{

                    $.msgBox({
                        title: "Failed",
                        content: "You must agree to the Term of Use and Privacy Policy !",
                        type: "error",
                        buttons: [{ value: "Ok" }],
                    });
                    event.preventDefault();

                }
            }
        }else{

            if(chkAgreed.is(':checked')){

                btnRegister.hide();
                divAjax.show();

                if(check_email == 1){

                    btnRegister.show();
                    divAjax.hide();

                    $.msgBox({
                        title: "Failed",
                        content: "Email is already registered !",
                        type: "error",
                        buttons: [{ value: "Ok" }],
                    });
                    // me.parent().find("div.show-error").fadeIn();
                    // me.focus();

                    event.preventDefault();

                }else{

                    if(password != confirm_password){

                        btnRegister.show();
                        divAjax.hide();
                        
                        $.msgBox({
                            title: "Failed",
                            content: "Password does not match !",
                            type: "error",
                            buttons: [{ value: "Ok" }],
                        });
                        event.preventDefault();

                    }
                }

            }else{

                $.msgBox({
                    title: "Failed",
                    content: "You must agree to the Term of Use and Privacy Policy !",
                    type: "error",
                    buttons: [{ value: "Ok" }],
                });
                event.preventDefault();

            }

        }
        // END

    });

    function validateEmail(email) {
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        return emailReg.test( email );
    }
    
});

</script>

<?php }else{ ?>
<div style="height: 200px;"></div>
<?php } ?>