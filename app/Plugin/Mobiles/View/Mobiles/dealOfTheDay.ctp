<!-- content -->
		<div class="block-item">
			<img src="picture/food_650_380.jpg" class="img-responsive">
			<div class="item-detail">
				<div class="item-name">itme-name</div>
				<div class="row">
					<div class="col-xs-6">
						<div class="item-type original-price">item type</div>
					</div>
					<div class="col-xs-3 ">
						<div class="original-price text-right"><strike>$420.00</strike></div>
					</div>
					<div class="col-xs-3 text-right">
						<div class="discount-price">$270.00</div>
					</div>
					<div class="clearfix"></div>
				</div>	
			</div>
		</div>
		<div class="block-item">
			<img src="picture/food_650_380.jpg" class="img-responsive">
			<div class="item-detail">
				<div class="item-name">itme-name</div>
				<div class="row">
					<div class="col-xs-6">
						<div class="item-type original-price">item type</div>
					</div>
					<div class="col-xs-3 ">
						<div class="original-price text-right"><strike>$420.00</strike></div>
					</div>
					<div class="col-xs-3 text-right">
						<div class="discount-price">$270.00</div>
					</div>
					<div class="clearfix"></div>
				</div>	
			</div>
		</div>
		<div class="block-item">
			<img src="picture/food_650_380.jpg" class="img-responsive">
			<div class="item-detail">
				<div class="item-name color-black">itme-name</div>
				<div class="row">
					<div class="col-xs-6">
						<div class="item-type original-price">item type</div>
					</div>
					<div class="col-xs-3 ">
						<div class="original-price text-right"><strike>$420.00</strike></div>
					</div>
					<div class="col-xs-3 text-right">
						<div class="discount-price">$270.00</div>
					</div>
					<div class="clearfix"></div>
				</div>	
			</div>
		</div>
<!-- end content -->