<!-- merchant detail -->
<?php  

//var_dump($businessInfo);exit(); 
if(!empty($businessInfo)){
	$BbusinessInfo = $businessInfo;
}
// var_dump($BbusinessInfo);exit();

?>
<div class="page-detail">
	<span class="arrow-back">
    	<button type="button" onClick="goBack()" class="button-back"><i class="fa fa-arrow-left"></i></button>
	</span>
	<div class="row header-background no-margin">
		<div class="col-xs-3">
			<div class="merchant-logo">
				<img src="<?php echo $this->webroot . $BbusinessInfo['Business']['logo'] ?>" class="img-responsive">
			</div>
		</div>	

	</div>
	<div class="row header-background no-margin">
		<div class="col-xs-12">	
			<div class="branch-name text-center"><?php echo $BbusinessInfo['Business']['business_name']; ?></div>
			<div class="merchant-type text-center"><?php echo $BbusinessInfo['MainCategory']['category']; ?></div>
		</div>
	</div>
</div>
<div class="merchant-des">
	<span><?php echo $BbusinessInfo['Business']['description'] ;?></span>
</div>
<div class="contact-caption">
	Contact Info
</div>
<div class="merchant-info">
	<div class="row">
		<div class="col-xs-2"><i class="fa fa-phone"></i></div>
		<div class="col-xs-9 no-padding-left">
			<?php echo $BbusinessInfo['Business']['phone1']; ?> &nbsp; &nbsp;
			<?php if(!empty($BbusinessInfo['Business']['phone2'])) 
					echo $BbusinessInfo['Business']['phone2'];
			?>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-2"><i class="fa fa-envelope"></i></div>
		<div class="col-xs-9 no-padding-left"><?php echo $BbusinessInfo['Business']['email']; ?></div>
	</div>
	<div class="row">
		<div class="col-xs-2"><i class="fa fa-fax"></i></div>
		<div class="col-xs-9 no-padding-left"><?php echo $BbusinessInfo['Business']['website']; ?></div>
	</div>
	<div class="row">
		<div class="col-xs-2"><i class="fa fa-map-marker"></i></div>
		<div class="col-xs-9 no-padding-left no-border"><?php echo $BbusinessInfo['Business']['location'] .",  ". $BbusinessInfo['Cities']['city_name']  ?></div>
	</div>
</div>

<!-- style -->
<style type="text/css">
.header ,.main-sidebar{
    display: none;
}
</style>
<!-- script -->
<script>
function goBack(){
	window.history.back();
}

</script>