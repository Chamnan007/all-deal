<!-- content -->	
<div class="full-bg">		
<?php foreach ($businesses as $key => $bvalue): 
?>
	<div class="merchant-block">
		<a href="<?php echo $this->Html->url(array('controller'=>'merchant','action'=>'detail',$bvalue['Business']['slug'])) ?>">
		<div class="row no-margin padding-top-20">
			<div class="col-xs-8">
				<div class="merchant-name"><?php echo $bvalue['Business']['business_name']; ?></div>
				<div class="merchant-type"><?php echo $bvalue['CategoryInfo']['mainCategory'];?></div>
				<div class="merchant-address"><?php echo $bvalue['Business']['location'] ?></div>
			</div>
			
			<div class="col-xs-4">
				<div class="merchant-logo-index">
					<img src="<?php echo $this->webroot . $bvalue['Business']['logo'] ?>" class="img-responsive">
				</div>
			</div>
		</div>
		</a>
		<hr class="line">
		<div class="merchant-subscribe">
			<button type="button" id="btnSubscribe" class="btnSearch">SUBSCRIBE</button>
		</div>
	</div>
<?php endforeach ?>
</div>
<!-- end content -->