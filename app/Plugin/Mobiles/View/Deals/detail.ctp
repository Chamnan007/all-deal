
<?php 

/********************************************************************************

File Name: detail.ctp
Description: displaying detail for deals

Powered By: ABi Investment Group Co., Ltd,

Changed History:

    Date                Author              Description
    2014/06/10          Sim Chhayrambo      Initial Version
    2014/06/16          Sim Chhayrambo      Add marker to google map
    2014/06/19          Sim Chhayrambo      Check empty data and add meta description
    2014/06/24          Sim Chhayrambo      Change UI, add business website
    2014/07/13          Sim Chhayrambo      Remove space in thumbnail, limit title
    2014/07/22          Sim Chhayrambo      Remove words "all deal" from meta description
    2015/02/23          Phea Rattana        Add Multiple Locations on Map, at related item link, 
                                            Random Images deal, check for operation hour

*********************************************************************************/

// var_dump("expression");exit();
$dealInfo = $data['Deal'];
// var_dump($dealInfo);exit();
if(!empty($data)){

$strPayment = "";

$dealInfo = $data['Deal'];

$id = $dealInfo['id'];
$title = $dealInfo['title'];
$image = $dealInfo['image'];
$desc = $dealInfo['description'];
$conditions = $dealInfo['deal_condition'];
// var_dump($conditions);exit();
$discount = $dealInfo['discount_category'];
$validTo = $dealInfo['valid_to'];
// var_dump($title);exit();
if ( $validTo >= date("Y-m-d H:i:s") ) {
 
$image = $dealInfo['image'];
if( $other_images = $dealInfo['other_images']) {
    $other_images = json_decode($other_images);
    $images = array();
    foreach( $other_images as $k => $v ){
        if( $v != "img/deals/default.jpg" ){
            $images[] = $v;
        }
    }
}else{
    $images[] = $image;
    }
}
// Biz Info
// ================================
$businessInfo = $data['Business'];

$businessName = $businessInfo['business_name'];
// var_dump($businessName);exit();
$businessStreet = $businessInfo['street'];
$businessLocation = $businessInfo['location'];
$businessProvince = $data['Province']['province_name'];
$businessCity   = $data['City']['city_name'];
$businessSangkat = ($data['BusinessInfo']['SangkatInfo']['name'])?$data['BusinessInfo']['SangkatInfo']['name']:"";

$businessEmail  = $businessInfo['email'];
$businessPhone1 = $businessInfo['phone1'];
$businessPhone2 = ($businessInfo['phone2'])?" / " . $businessInfo['phone2']:""; ;
$businessLogo = $businessInfo['logo'];
$businessDescription = $businessInfo['description'];
$businessLat = $businessInfo['latitude'];
//var_dump($businessLat);exit();
$businessLong = $businessInfo['longitude'];
$businessSlug = $businessInfo['slug'];
$payment = json_decode($businessInfo['accept_payment']);

if(!empty($payment)){
    foreach( $payment as $key => $value ){
        $strPayment .= $this->Html->image( $value . '.jpg', array(  'alt' => ucfirst($value),
              'title'=>ucfirst($value),
              'style'=>'width:40px; margin-right:5px;'));
    }
}

$strCategory = "";
$businessMainCategory = $businessCategory['main_category'];
$businessSubCategory = $businessCategory['sub_category'];
if(!empty($businessSubCategory)){
    $strCategory = $businessSubCategory . " - " . $businessMainCategory;
}else{
    $strCategory = $businessMainCategory;
}

$businessWebsite = str_replace("http://", "", $businessInfo['website']);
$businessWebsite = str_replace("https://", "", $businessInfo['website']);
$businessWebsite = str_replace("www.", "", $businessWebsite);
$businessWebsite = "www." . $businessWebsite;
$businessWebsite = trim($businessWebsite, "/");
// var_dump($businessWebsite);exit();
$full_url = Router::url( $this->here, true );

$metaTitle = trim($title);
$metaInfo = strip_tags($desc);
$newMetaDescription = $metaTitle . ". " . $metaInfo;

if(strlen($newMetaDescription) >= 300){
    $newMetaDescription = substr($newMetaDescription,0,300) . '...';
}

$metaDescription = $newMetaDescription;
$this->Html->meta('description', $metaDescription, array('inline'=>false));

$data_braches = array();
$time = time();

if( isset($branches) && !empty($branches) ){
  foreach( $branches as $k => $val ){
     $time += 300;

     $data_braches[$k]['index']         = $time;
     $data_braches[$k]['branch_name']   = $val['BusinessBranch']['branch_name'];
     $data_braches[$k]['latitude']      = $val['BusinessBranch']['latitude'];
     $data_braches[$k]['longitude']     = $val['BusinessBranch']['longitude'];
  }

}

$deal_items = $data['DealItemLink'];
$prev_original  = 0;
$prev_discount  = 0;
// var_dump($deal_items);exit();
if( $deal_items ){

    $max_percentage = 0;

    foreach( $deal_items as $k => $v ){

        $ori        = ($v['original_price'] != 0 )?$v['original_price']:$v['ItemDetail']['price'];
        $disc       = $v['discount'];
        $item_id    = $v['item_id'];

        $percentage = 100 - ( $disc * 100 / $ori );

        if( $percentage > $max_percentage ){
            $max_percentage = $percentage;
            $prev_original  = $ori;
            $prev_discount  = $disc;
        }else if( $percentage == $max_percentage ){
            if( $disc < $prev_discount ){
                $prev_original  = $ori;
                $prev_discount  = $disc;
            }
        }

    }

    $original =  number_format($prev_original, 2);
    $discount =  number_format($prev_discount, 2); 

    $realDiscount = $original-$discount;
    
    $percentSaveCost = number_format((($original-$discount)*100)/$original,2);                       
}
}


?>


<!-- content -->
<div class="page-detail">
	<span class="arrow-back">
    <button type="button" onClick="goBack()" class="button-back"><i class="fa fa-arrow-left"></i></button>
  </span>
	<div class="single-item">
		<?php  foreach ($images as $key => $img): ?>
		<div>
			<img src="<?php echo $this->webroot . $img ?>" class="img-responsive">
		</div>	
		<?php  endforeach ?>
	</div>
	
	<div class="item-detail-bg">
		<div class="itme-detail-name">
			<div><?php echo $title; ?></div>
		</div>
        <div class="full-bg">
    		<div class="detial-description">
    			<div class="calculate">
                    <div class="item-type original-price padding-left-20"><?php echo $businessName; ?></div>
    				<span class="original-price"><strike><?php echo "$" . $original; ?></strike></span>
    				<span class="discount-price"><?php echo "$" . $discount ; ?></span>
    				<div class="result-calculat original-price">
    					<span>You save </span> <span><?php echo $percentSaveCost . "%" ?></span>
    				</div>
            <!-- item list -->
            <div class="item-list">
                <div class="item-list-header">
                    <div class="pull-left">Item List</div>
                    <div class="pull-right">
                      <a href="#" 
                         data-toggle="modal"
                         data-target="#myModal">Buy</a>
                      <!-- purchase items -->
                        <?php foreach ($deal_items as $key => $value): ?>
                        <?php 
                        //var_dump($deal_items);exit();
                        ?>
                       <div class="modal fade" id="myModal" role="dialog">
                        <div class="modal-dialog custom-modal">
                          <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close cros" data-dismiss="modal">&times;</button>
                                <h2 class="modal-title" style="font-size:35px">List Items</h2>
                            </div>
                            <div class="modal-body">
                              <div class="block-purchase-header">
                                  <div class="row purchase-header-style">
                                      <div class="col-xs-2">
                                        <div class="purchase-header">Image</div>
                                      </div>
                                      <div class="col-xs-2">
                                        <div class="purchase-header">Name</div>
                                      </div>
                                      <div class="col-xs-2">
                                        <div class="purchase-header">Original Price</div>
                                      </div>
                                      <div class="col-xs-2">
                                        <div class="purchase-header">Discounted Price</div>
                                      </div>
                                      <div class="col-xs-2">
                                        <div class="purchase-header">QTY</div>
                                      </div>
                                      <div class="col-xs-2">
                                        <div class="purchase-header">Amount</div>
                                      </div>
                                  </div>
                              </div>
                              <div class="purchase-listing">
                                  
                                  <?php foreach ($deal_items as $key => $value): ?>

                                  <div class="row">
                                      <div class="col-xs-2">
                                          <img src="<?php echo $this->webroot. $value['ItemDetail']['image']; ?>" class="img-responsive">
                                      </div>
                                      <div class="col-xs-2">
                                          <div class="perchase-text-align"><?php echo $value['ItemDetail']['title']; ?></div>
                                      </div>
                                      <div class="col-xs-2">
                                          <div class="perchase-text-align"><strike>$<?php echo number_format($value['original_price'],2) ?></strike></div>
                                      </div>
                                      <div class="col-xs-2">
                                           <div class="perchase-text-align">$<?php echo number_format($value['discount'],2) ?></div>
                                      </div>
                                      <div class="col-xs-2">
                                          <select>
                                              <option value="1">1</option>
                                              <option value="2">2</option>
                                              <option value="3">3</option>
                                              <option value="4">4</option>
                                              <option value="5">5</option>
                                          </select>
                                      </div>
                                      <div class="col-xs-2">
                                          <div class="perchase-text-align"><label id="amount">$0.00</label></div>
                                      </div>
                                  </div><hr>
                                  <?php endforeach ?>
                                  
                              </div>  
                            </div>
                            <div class="modal-footer no-padding-top">
                                <div class="total-amount">Total Amout: </div>
                                <button type="button" class="btn btn-default btn-lg" data-dismiss="modal" style="position:absolute;bottom:10px;right:40px;">Close</button>
                            </div>
                          </div>
                        </div>
                      </div>
                      <?php endforeach ?>
                      <!-- end -->
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="item-list-theader">
                    <div class="row">
                      
                        <div class="col-xs-3 row-bg">
                          <div class="list-pakack-item-head">Image</div>
                        </div>
                        <div class="col-xs-3 row-bg">
                            <div class="list-pakack-item-head">Name</div>
                        </div>
                        <div class="col-xs-3 row-bg">
                            <div class="list-pakack-item-head">Original Price</div>
                        </div>
                        <div class="col-xs-3 row-bg">
                            <div class="list-pakack-item-head">Discount Price</div>
                        </div>
                        
                    </div>
                    <div class="line-bottom"></div>
                    <div class="row padding-top-20 ">
                    <?php foreach ($deal_items as $key => $item): 
                            $full_img   = $item['ItemDetail']['image'];
                            $thumb_img  = str_replace('/menus/', '/menus/thumbs/', $item['ItemDetail']['image']);

                            $item_desc = strip_tags($item['ItemDetail']['description']);

                            $limit_length = 43;
                            if(strlen($item_desc) > $limit_length){
                                $item_desc = substr($item_desc,0, $limit_length - 3 ) .'...';   
                            }

                            $ori_price = ($item['original_price'] != 0 )?$item['original_price']:$item['ItemDetail']['price'] ;

                            $saved_price = $ori_price - $item['discount'];

                            $saved_percentage = $saved_price * 100 / $ori_price ;
                        ?>

                      <div class="col-xs-3">
                      <img src="<?php echo $this->webroot . $thumb_img ?>" 
                                     alt="<?php echo $item['ItemDetail']['title'] ?>"
                                     title="<?php echo $item['ItemDetail']['title'] ?>"
                                     class="item-img"
                                     data-img = "<?php echo $full_img ?>"
                                     style=" padding: 2px; border: 1px solid #EEE; max-height: 150px; max-width:215px ;width:215px"
                                     data-toggle="modal"
                                     data-target="#myModal<?php echo $key; ?>">
                            <!-- show item detail -->   
                            <?php foreach ($deal_items as $key => $value): ?>
                             <?php 

                              //var_dump($deal_items);exit();
                              ?>
                             <div class="modal fade" id="myModal<?php echo $key; ?>" role="dialog">
                              <div class="modal-dialog custom-modal">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <button type="button" class="close cros" data-dismiss="modal">&times;</button>
                                    <h1 class="modal-title"><?php echo $value['ItemDetail']['title'] ?></h1>
                                  </div>
                                  <div class="modal-body">
                                    <div class="col-xs-4">
                                        <img src="<?php echo $this->webroot . $value['ItemDetail']['image']; ?>" class="img-responsive">
                                    </div>
                                    <div class="col-xs-8">
                                        <?php echo $value['ItemDetail']['description'] ?>
                                    </div>
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-default btn-lg" data-dismiss="modal" style="position:absolute;bottom:10px;right:40px;">Close</button>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <?php endforeach ?>
                            <!-- end item detail -->
                      </div>
                      <div class="col-xs-3">
                          <strong style="font-size:30px"><?php echo $item['ItemDetail']['title'] ?></strong><br>
                              <p style="font-size:29px;padding-top:10px; color:#555"><?php echo $item_desc ?></p>
                      </div>
                      <div class="col-xs-3">
                       <span class="original-price"><strike> $ <?php echo number_format($ori_price, 2) ?></strike></span>
                      </div>
                      <div class="col-xs-3">
                         <span class="discount-price">$ <?php echo number_format($item['discount'], 2) ?></span>
                      </div>
                      <div class="clearfix"></div><hr style="margin-left:15px !important;margin-right:15px !important;">
                    <?php endforeach ?>
                    </div>
                </div>
            </div>

            <!-- Description -->
    				<div class="item-description">
    					<div class="des-caption">Description</div>
              <span><?php echo $desc; ?></span>
    				</div>
            <!-- Condition -->
            <div class="condition">
                <div class="des-caption">Conditions</div>
                <span><?php echo $conditions; ?></span>
            </div>
            <p>
                <span style="font-size:25px;">
                    <strong>Note:</strong> Cancellation policy is according to each store's policy.<br/>
                    <span>IDMS (Cambodia), All Deal and its affiliates doesn’t warrant any products or services listed in this coupon.</span>
                </span>
            </p>
            <!-- deal Company -->
            <div class="deal-company">
               <div class="des-caption"><?php echo __('About') . " " . $businessName; ?></div>
               <span><?php echo $businessDescription ?></span>
            </div>
            <div class="clearfix"></div>
    			</div>
    		</div>
    		<div class="map">
                <div class="map-header">
                    <div class="col-xs-4">
                        
                    </div>
                    <div class="col-xs-8"></div>
                </div>
    			<div style="width:100%; height:500px; margin:0 auto;" id="map-canvas"></div>
    		</div>
    		<div class="term-and-condition">
    			<div class="des-caption">term and condition</div><br>
    			<span style="font-size:28px">description</span>
    		</div>
        </div>
	</div>
  <!-- <div class="shopping-cart">
      <button class="btn-shopping-cart">
        <img src="<?php // echo  $this->webroot. 'img/mobile/ic_shopping_cart_black_24px-01.png' ?>">
      </button>
  </div> -->
</div>

<!-- end content -->
<!-- script -->
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
<script src="https://google-maps-utility-library-v3.googlecode.com/svn/tags/markerwithlabel/1.1.9/src/markerwithlabel.js" type="text/javascript"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script>

$(document).ready(function(){
  $('.single-item').slick({dots:true});
});

//google map
var listBranchMarker = {};
    var map = null;
    var pin_icon_blue = '<?php echo $this->webroot ?>' + 'img/pin-blue.png';
    var iw = null;

    function addBranchMarker(index,latLng, brandName, latLngText) {

        listBranchMarker[index] = new MarkerWithLabel({
                map:map,
                position:latLng,
                icon: pin_icon_blue,
                labelContent: brandName  ,
                title:brandName,

                labelAnchor : new google.maps.Point(45, 67),
                labelClass : "labels-branch",
                labelInBackground : false
            });

        var b_name = new google.maps.InfoWindow({ content: brandName + latLngText });

        google.maps.event.addListener( listBranchMarker[index], "click", function (e) { b_name.open(map, this); });
        
    }
function initialize() {


      var  latitude  = <?php echo $businessLat ?>,
           longitude = <?php echo $businessLong ?>,
           myLatlng  = new google.maps.LatLng( latitude, longitude ),
           businessesName = "<?php echo $businessName ?>";

      var image = '<?php echo $this->webroot ?>' + 'img/pin-yellow.png';

      var mapOptions = {
        zoom: 12,
        center: myLatlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      }

      map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

      var marker = new MarkerWithLabel({
              map: map,
              position: myLatlng,
              // title: businessesName,
              icon : image,
              labelContent : businessesName,
              labelAnchor : new google.maps.Point(26, 64),
              labelClass : "labels",
              labelColor : "red",
              labelInBackground : false
          });

        var iw = new google.maps.InfoWindow({
                       content: businessesName
                     });


        google.maps.event.addListener(marker, "click", function (e) { iw.open(map, this); });

        var data_branch = <?php echo json_encode($data_braches) ?>;
        $.each( data_branch, function( ind, val ){
            var name  = val.branch_name,
                lat   = val.latitude,
                lng   = val.longitude,
                index = val.index;

            var newLatLng  = new google.maps.LatLng(lat, lng );

            latLngText = " (" + lat + ", " + lng + ")";
            addBranchMarker( index ,newLatLng, name, latLngText );
        })
    }
    google.maps.event.addDomListener(window, 'load', initialize);

    function goBack(){
      window.history.back();
    }
</script>

<!-- end script -->
<!-- style -->
<style type="text/css">
.header ,.main-sidebar{
    display: none;
}
</style>
<!-- end style -->