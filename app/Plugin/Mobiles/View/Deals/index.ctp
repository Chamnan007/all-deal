<div class="full-bg">
<?php
foreach ($deals as $dKey => $dValue) { 
    $split = end(explode("/", $dValue['Deal']['image']));
    $thumb_img =  "img/deals/thumbs/" . $split ;
    $business_name = $dValue['Business']['business_name'];
    $business_street = $dValue['Business']['street'];
    $business_location = $dValue['Business']['location'];
    $deal_title = $dValue['Deal']['title'];
    if(strlen($deal_title) > $limit_deal_title){
        $deal_title = substr($dValue['Deal']['title'],0,$limit_deal_title-3).'...';   
    }
    if(strlen($business_name) > $limit_business_title){
        $business_name = substr($business_name,0,$limit_business_title-3).'...';   
    }

    if( $other_deal_imgs = $dValue['Deal']['other_images'] ){
        $imgs = json_decode($other_deal_imgs);
        $data_img = array();

        foreach( $imgs as $k => $img ){
            $data_img[] = $img;
        }

        $selected = array_rand($data_img);

        $thumb_img = str_replace("/deals/", "/deals/", $data_img[$selected] );
    }

    $original = "";
    $discount = $dValue['Deal']['discount_category'];

    $prev_original  = 0;
    $prev_discount  = 0;

    if( $items = $dValue['DealItemLink'] ){

        $max_percentage = 0;

        foreach( $items as $k => $v ){

            $ori        = ($v['original_price'] != 0 )?$v['original_price']:$v['ItemDetail']['price'];
            $disc       = $v['discount'];
            $item_id    = $v['item_id'];

            $percentage = 100 - ( $disc * 100 / $ori );

            if( $percentage > $max_percentage ){
                $max_percentage = $percentage;
                $prev_original  = $ori;
                $prev_discount  = $disc;
            }else if( $percentage == $max_percentage ){
                if( $disc < $prev_discount ){
                    $prev_original  = $ori;
                    $prev_discount  = $disc;
                }
            }

        }

        $original = "$" . number_format($prev_original, 2) ;
        $discount = "$" . number_format($prev_discount, 2);                        
    }
     // var_dump($type);exit();
?>

<div class="main-bg">
    <div class="block-item">
        <a href="<?php echo $this->Html->url(array('controller'=>'deals','action'=>'detail',$dValue['Deal']['slug'])) ?>">
        <img src="<?php echo $this->webroot . $thumb_img ?>" class="img-responsive">
        <div class="item-detail">
            <div class="item-name"><?php echo $deal_title ?></div>
            <div class="col-xs-8">
                <div class="item-type original-price"><?php echo $business_name; ?></div>
            </div>
            <div class="col-xs-2">
                <div class="original-price text-right"><strike><?php echo $original; ?></strike></div>
            </div>
            <div class="col-xs-2 text-right">
                <div class="discount-price"><?php echo $discount; ?></div>
            </div>
            <div class="clearfix"></div>  
        </div>
        </a>
    </div>
</div>
<?php } ?>

</div>
