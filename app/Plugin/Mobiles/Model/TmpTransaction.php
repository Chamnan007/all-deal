<?php

/********************************************************************************

File Name: Buyer Model
Description: Model file for user

Powered By: ABi Investment Group Co., Ltd,

Changed History:

  	Date				Author				Description
  	2015/04/09    		Phea Rattana		Initial Version

*********************************************************************************/


App::uses('AdministratorAppModel', 'Administrator.Model');
/**
 * User Model
 *
 */
class TmpTransaction extends AdministratorAppModel {

/**
 * Validation rules
 *
 * @var array
 */

	public $belongsTo = array(

		'BusinessDetail' => array(
			'className' => 'Business',
			'foreignKey' => 'business_id',
			'conditions' => '',
			'fields' =>  '' ,
			'order' => ''
		),

		'DealInfo' => array(
			'className' => 'Deal',
			'foreignKey' => 'deal_id',
			'conditions' => '',
			'fields' =>  '' ,
			'order' => ''
		)
	);
	
	public $hasMany = array(

		'TransactionDetail' => array(
			'className' => 'TmpTransactionDetail',
			'foreignKey' => 'transaction_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),

	);


}
