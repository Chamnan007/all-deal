<?php
App::uses('AdministratorAppController', 'Administrator.Controller');
/**
 * DiscountCategories Controller
 *
 * @property DiscountCategory $DiscountCategory
 * @property PaginatorComponent $Paginator
 */
class DiscountCategoriesController extends AdministratorAppController {

/**
 * Components
 *
 * @var array
 */
	var $context = 'DiscountCategory';

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->DiscountCategory->recursive = 0;
		
		$this->conditionFilter['status'] = 1;
		$this->paginate['conditions'] = $this->conditionFilter;
		$this->paginate['order'] = array("DiscountCategory.category" => 'ASC' ) ;
		parent::index();
		
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->DiscountCategory->exists($id)) {
			throw new NotFoundException(__('Invalid discount category'));
		}
		$options = array('conditions' => array('DiscountCategory.' . $this->DiscountCategory->primaryKey => $id));
		$this->set('discountCategory', $this->DiscountCategory->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			parent::save();
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->DiscountCategory->exists($id)) {
			throw new NotFoundException(__('Invalid discount category'));
		}
		if ($this->request->is(array('post', 'put'))) {
			parent::save($id);
		} else {
			$options = array('conditions' => array('DiscountCategory.' . $this->DiscountCategory->primaryKey => $id));
			$this->request->data = $this->DiscountCategory->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {

		if (!$this->DiscountCategory->exists($id)) {
			throw new NotFoundException(__('Invalid Category'));
		}

		$this->DiscountCategory->id = $id;
		
		if ($this->DiscountCategory->save(array('status'=>0))) 
		{
			$this->Session->setFlash(__('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button><b>Success!!!</b> The Goods Category has been deleted.</div>'));

			$obj 	= $this->DiscountCategory->findById($id);
			$logMessage = json_encode($obj);
			parent::generateLog($logMessage,' DELETE :'.$id);
		} else {
			$this->Session->setFlash(__('<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button><b>Failed!!!</b> The Goods Category could not be deleted. Please, try again.</div>'));
		}

		// var_dump($this->request->data); exit;
	
		return $this->redirect(array('action' => 'index'));
	}
	
}
