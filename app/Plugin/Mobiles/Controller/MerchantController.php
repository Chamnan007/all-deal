<?php  
	class MerchantController extends MobilesAppController{
	
    	public function index() {

            $params = $this->request->query;
            $paramsMainCategory = (!empty($params['mainCategory'])) ? $params['mainCategory'] : "";
            $paramsLocation = (!empty($params['location'])) ? $params['location'] : "";
            $paramsCategory = (!empty($params['businessCategory'])) ? $params['businessCategory'] : "";
            $paramsName = (!empty($params['businessName'])) ? $params['businessName'] : "";
            $conditions = array();
            $now = date('Y-m-d H:i:s');

            $getCategory = (!empty($params['category'])) ? $params['category'] : "";

            if(!empty($getCategory)){

                $mainCategory = $getCategory;

                $cateInfo = $this->BusinessCategory->findBySlug($getCategory);
                $this->set('businessCategoryInfo', $cateInfo);
                if(!empty($cateInfo)){
                    $cateID = $cateInfo['BusinessCategory']['id'];    
                }else{
                    $cateID = "XXX-XXX";
                }
                $arrCondition[0]["OR"][] = array('Business.business_main_category' => $cateID);

            }else{

                if(!empty($paramsMainCategory) ){
                    $mainCategory = $paramsMainCategory;
                    $this->set('filterCategory', $paramsMainCategory);
                    $cateInfo = $this->BusinessCategory->findBySlug($paramsMainCategory);
                    $this->set('businessCategoryInfo', $cateInfo);
                    if(!empty($cateInfo)){
                        $cateID = $cateInfo['BusinessCategory']['id'];    
                    }else{
                        $cateID = "XXX-XXX";
                    }
                    // $cateID = $cateInfo['BusinessCategory']['id'];
                    $arrCondition[4] = array('Business.business_main_category' => $cateID);
                }else{
                    $mainCategory = "";
                }

                if(!empty($paramsCategory)){
                    $this->set('filterCategory', $paramsCategory);
                    foreach ($paramsCategory as $key) {

                        $cateInfo = $this->BusinessCategory->findBySlug($key);
                        if(!empty($cateInfo)){
                            $cateID = $cateInfo['BusinessCategory']['id'];    
                        }else{
                            $cateID = "XXX-XXX";
                        }
                        // $cateID = $cateInfo['BusinessCategory']['id'];
                        $arrCondition[0]["OR"][] = array('Business.business_sub_category' => $cateID);
                    }
                }

                if(!empty($paramsLocation)){
                    $this->set('filterLocation', $paramsLocation);
                    foreach ($paramsLocation as $key) {
                        if(!empty($key)){
                            $city = $this->City->findBySlug($key);
                            $cityCode = $city['City']['city_code'];
                            $arrCondition[1]["OR"][] = array('Business.city' => $cityCode);
                        }
                    }
                }

                if(!empty($paramsName)){
                    $this->set('filterName', $paramsName);
                    $paramsName = trim($paramsName);
                    $arrName = explode(" ", $paramsName);
                    foreach ($arrName as $key => $value) {
                        if(!empty($value)){
                            $arrCondition[3]["OR"][] = array('Business.business_name LIKE' => "%$value%");        
                        }
                    }
                    
                }

            }

            $arrCondition[2] = array('Business.status' => 1);

            $conditions = $arrCondition;

            $businesses = $this->Business->find('all', array(
                            'joins' => array(
                                array(
                                    'table' => 'tb_provinces',
                                    'alias' => 'Provinces',
                                    'type' => 'LEFT',
                                    'conditions' => array(
                                        'Business.province = Provinces.province_code'
                                    )
                                )
                            ),
                            'conditions'=>$conditions, 
                            'fields' => array('Business.*', 'Provinces.*'),
                            'order'=>array('Business.business_name'),
                            'limit'=>$this->limit));

            foreach ($businesses as $key => $busValue) {
                $mainCategoryInfo = $this->BusinessCategory->findById($busValue['Business']['business_main_category']);
                $businesses[$key]['CategoryInfo']['mainCategory'] = $mainCategoryInfo['BusinessCategory']['category'];
            }

            $this->set('businesses', $businesses);
            // var_dump($businesses);exit();
            // GET TOTAL DEALS
            $total_business = $this->Business->find('count', array(
                            'joins' => array(
                                array(
                                    'table' => 'tb_provinces',
                                    'alias' => 'Provinces',
                                    'type' => 'LEFT',
                                    'conditions' => array(
                                        'Business.province = Provinces.province_code'
                                    )
                                )
                            ),
                            'conditions'=>$conditions, 
                            'fields' => array('Business.*', 'Provinces.*'),
                            'order'=>array('Business.business_name')));

            $total_pages = ceil($total_business/$this->limit);  
            $this->set('total_business', $total_business);
            $this->set('total_pages', $total_pages);

            if(!empty($paramsCategory)){

                $this->set('subCategory', $paramsCategory);

            }

            $this->set('getCategory', $getCategory);
            $this->set('mainCategory', $mainCategory);
            $this->set('limit', $this->limit);

        }	
         public function detail($slug = ""){

        // $businessInfo = $this->Business->findBySlug($slug);
        $this->Business->recursive = 3;
        $businessInfo = $this->Business->find('first', array(
                    'joins' => array(
                        array(
                            'table' => 'tb_provinces',
                            'alias' => 'Provinces',
                            'type' => 'LEFT',
                            'conditions' => array(
                                'Business.province = Provinces.province_code'
                            )
                        ),
                        array(
                            'table' => 'tb_cities',
                            'alias' => 'Cities',
                            'type' => 'LEFT',
                            'conditions' => array(
                                'Business.city = Cities.city_code'
                            )
                        )
                    ),
                    'conditions' => array(
                        'Business.slug' => $slug
                    ),
                    'fields' => array('Business.*', 'Provinces.*', 'Cities.*')
                ));

        $businessInfo['main_category'] = $this->BusinessCategory->findById($businessInfo['Business']['business_main_category']);
        $businessInfo['sub_category'] = $this->BusinessCategory->findById($businessInfo['Business']['business_sub_category']);

        $businessMedia = array();

        if(!empty($businessInfo)){

            $businessID = $businessInfo['Business']['id'];    
            $businessMedia = $this->BusinessesMedia->find('all', array('conditions'=>array('BusinessesMedia.business_id'=>$businessID)));

            // Get Current Deal
            $dealConditions = array( 'conditions' => array( 'Deal.status' => 1,
                                                            'Deal.business_id' => $businessID,
                                                            'Deal.valid_from <=' => date('Y-m-d H:i:s') ,
                                                            'Deal.valid_to >' => date('Y-m-d H:i:s') 
                                                        ),
                                     'order' => array('Deal.created' => 'DESC') );
            $this->Deal->recursive = 2;
            $deals = $this->Deal->find('all', $dealConditions);
            $businessInfo['Deals'] = $deals;

            $branches = $this->BusinessBranch->find('all', 
                                                    array( 
                                                        "conditions" => 
                                                            array( "BusinessBranch.status" => 1,
                                                                    "BusinessBranch.business_id" => $businessID ),
                                                        'order' => array( 'BusinessBranch.branch_name' => 'ASC' )
                                                    )
                                                );
            $this->set('branches', $branches);

        }
        
        $this->set('businessInfo', $businessInfo);
        $this->set('businessMedia', $businessMedia);

         // var_dump($businessInfo);exit();

    }

	}
?>