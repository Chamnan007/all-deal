<?php

/********************************************************************************

File Name: NewsLettersController.php
Description: Controller for NewsLetters

Powered By: ABi Investment Group Co., Ltd,

Changed History:

    Date                Author              Description
    2014/06/25          Sim Chhayrambo      Initial Version

*********************************************************************************/

App::uses('AppController', 'Controller');
App::uses('NewletterSubscriber', 'Model');

/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class NewsLettersController extends AppController {

/**
 * Components
 *
 * @var array
 */
    public $components = array('Paginator');
    var $uses = array("NewletterSubscriber");

/**
 * index method
 *
 * @return void
 */
    public  function index(){
    }

    public  function subscribe($name = "", $phone = "", $email = ""){

        if(isset($_POST['name'])){

            $params = "" ;

            if(!empty($name) && !empty($email)){

                $data = array(
                            'NewletterSubscriber' => array(
                                'name' => $name,
                                'email' => $email,
                                'phone' => $phone,
                                'contry' => NULL,
                                'status' => 1,
                                'province' => NULL,
                                'city' => NULL
                            )
                        );

                // Create: id isn't set or is null
                $this->NewletterSubscriber->create();

                $result = $this->NewletterSubscriber->save($data);

                // $result = 1;

            }else{

                $result = false;
            }

            $data['params'] = $params;
            $data['result'] = $result;
            echo json_encode($data);
            exit();

        }else{
            return $this->redirect(array('controller'=>'error-page'));
        }

    }

    public function check_existing_email($email = ""){

        if(!empty($email)){
            $getSubscriber = $this->NewletterSubscriber->findByEmail($email);
            if(!empty($getSubscriber)){
                $result = true;
            }else{
                $result = false;
            }
        }else{
            $result = false;
        }

        $data['result'] = $result;
        echo json_encode($data);
        exit();

    }
}