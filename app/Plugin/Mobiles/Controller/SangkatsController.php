<?php
/**
 * Locations Controller
 *
 * @property Location $Location
 * @property PaginatorComponent $Paginator
 */

App::uses('AppController', 'Controller');

class SangkatsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	var $context = 'Sangkat';

	var $uses  = array(	'City' , 
						'Location' , 
						'Province', 
						'Sangkat' );

/**
 * index method
 *
 * @return void
 */

	public function getSangkatByLocation( $location_id = 0 ){

		if( $location_id != null ){
			$sangkats =  $this->Sangkat->find( 'all', array( 'conditions' => array('Sangkat.location_id' => $location_id ))) ;



			$data = array();
			$data['data'] =  $sangkats;
			echo json_encode( $data);

			$this->autoRender = false;

		}
	}

}
