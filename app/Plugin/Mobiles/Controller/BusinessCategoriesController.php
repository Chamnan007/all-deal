<?php

/********************************************************************************

File Name: BusinessCategoriesController.php
Description: managing business category module

Powered By: ABi Investment Group Co., Ltd,

Changed History:

    Date                Author              Description
    2014/06/10          Sim Chhayrambo      Initial Version

*********************************************************************************/

App::uses('AppController', 'Controller');
/**
 * BusinessCategories Controller
 *
 * @property BusinessCategory $BusinessCategory
 * @property PaginatorComponent $Paginator
 */
class BusinessCategoriesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	var $context = 'BusinessCategory';
	// public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */


	public function index() {
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */

	public function getCategoryByParentID( $parent_id = 0 ){

		if(isset($_POST['parent_id'])){

			$options = array('conditions' => array('parent_id'=>$parent_id, 'status' => 1 ), 'order' => array('BusinessCategory.category' => 'ASC' ) );

			$result  = $this->BusinessCategory->find('all', $options);

			$data = array();

			$data['data'] =  $result;
			echo json_encode( $data);

			$this->autoRender = false;

		}else{

			return $this->redirect(array('controller'=>'error-page'));

		}
	}

}
