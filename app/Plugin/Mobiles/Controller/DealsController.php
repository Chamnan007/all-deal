<?php

/********************************************************************************

File Name: DealsController.php
Description: controlling deals module function

Powered By: ABi Investment Group Co., Ltd,

Changed History:

    Date                Author              Description
    2014/06/10          Sim Chhayrambo      Initial Version
    2014/06/24          Sim Chhayrambo      Join with tb_province, add order
    2014/07/03          Sim Chhayrambo      Check for tavel deal
    2015/07/09          Phea Rattana        All Types of deal listing are here

*********************************************************************************/

App::uses('AppController', 'Controller');

/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class DealsController extends AppController {

/**
 * Components
 *
 * @var array
 */
    public $components = array('Paginator');
    
    var $uses = array(  'OperationHour', 
                        'Deal', 
                        'BusinessesMedia', 
                        'City', 
                        'Destination', 
                        'GoodsCategory',
                        'BusinessCategory', 
                        'BusinessBranch',
                        'DealOfTheDay' );

    var $categories_checked     = array();

    public function beforeFilter(){

        parent::beforeFilter();
        $this->set('maxItem', $this->__NUMBER_OF_ITEM_LIMIT);

        // Get List of Deal Category
        $categoryConds['conditions']    = array( 'GoodsCategory.status'     => 1 );
        $categoryConds['order']         = array( 'GoodsCategory.category'   => 'ASC' );
        $this->set('dealCategories', $this->GoodsCategory->find('all', $categoryConds) );
    }
    
    public  function index(){

        $this->Deal->recursive = 3;
        $getForm = $this->request->query;
        
        $type = (isset($getForm['type']))?strtolower($getForm['type']):"";
        $location = (isset($getForm['location']))?strtolower($getForm['location']):"";

        $now = date("Y-m-d H:i:s");

        $conditionFilter    = array() ;
        $order              = "";
        $pageTitle          = "All Deals";
        $deals              = array();

        $conditionFilter = array(   'Deal.status'           => 1, 
                                    'Deal.isdeleted'        => 0, 
                                    'Deal.valid_from <='    => $now, 
                                    'Deal.valid_to >'       => $now,
                                    'Business.status'       => 1 
                                );
           
        $order = array( 'Deal.created' => 'DESC');
        
        if( $type ){
             // var_dump($type);exit();
            $pageTitle = str_replace("-", " ", $type);
            $pageTitle = str_replace(" and ", " & ", $pageTitle);
            $pageTitle = ucwords($pageTitle);

            if( $type == "deal-of-the-day" ){

                $conds  = array('DealOfTheDay.available_date' => date('Y-m-d'));

                $dodConditions['conditions']    = $conds;
                $dodConditions['order']         = array('DealOfTheDay.created' => 'DESC', 'DealDetail.created' => 'DESC' );
                $dodConditions['limit']         = $this->limit;

                // $this->DealOfTheDay->recursive = -1;
                $dod = $this->DealOfTheDay->find('all', $dodConditions);
                // Prepare Data Deals
                $deal_ids = array();
                if( $dod ){
                    foreach( $dod as $k => $val ){
                        $deal_ids[] = $val['DealOfTheDay']['deal_id'];
                    }
                }

                $conditionFilter['Deal.id']         = $deal_ids;

            }else if( $type == 'last-minute-deal' ){

                $conditionFilter['Deal.is_last_minute_deal'] = 1;
            
            }else if( $type == 'food-and-drink' ) {
                $cate_ids = $this->food_drink_categories;
                $this->categories_checked = $this->food_drink_categories;

                foreach( $cate_ids as $id ){
                    $conditionFilter['OR'][] = array( 'Deal.goods_category LIKE' => '%"' . $id . '"%' ) ;
                }
            }else if( $type == 'beauty-and-spa' ){
                $cate_ids = $this->beauty_spa_categories;
                $this->categories_checked = $this->beauty_spa_categories;

                foreach( $cate_ids as $id ){
                    $conditionFilter['OR'][] = array( 'Deal.goods_category LIKE' => '%"' . $id . '"%' ) ;
                }
            }else if( $type == "shopping" ){
                $cate_ids = $this->shopping_categories;
                $this->categories_checked = $this->shopping_categories;

                foreach( $cate_ids as $id ){
                    $conditionFilter['OR'][] = array( 'Deal.goods_category LIKE' => '%"' . $id . '"%' ) ;
                }
            }else if( $type == "things-to-do" ){
                $cate_ids = $this->things_to_do_categories;
                $this->categories_checked = $this->things_to_do_categories;

                foreach( $cate_ids as $id ){
                    $conditionFilter['OR'][] = array( 'Deal.goods_category LIKE' => '%"' . $id . '"%' ) ;
                }
            }else if( $type == "getaways" ){
                $cate_ids = $this->getaways_categories;
                $this->categories_checked = $this->getaways_categories;

                foreach( $cate_ids as $id ){
                    $conditionFilter['OR'][] = array( 'Deal.goods_category LIKE' => '%"' . $id . '"%' ) ;
                }
            }

        }else if( $location ) {

            $pageTitle = str_replace("-", " ", $location);
            $pageTitle = str_replace(" and ", " & ", $pageTitle);
            $pageTitle = ucwords($pageTitle);

            if( $location_name = $this->Location->findBySlug($location) ){
                $conditionFilter['Business.location'] = $location_name['Location']['location_name'];
            }
        }

        // Get Deal Information
        $deals = $this->Deal->find('all', array(
            'joins' => array(
                array(
                    'table' => 'tb_businesses',
                    'alias' => 'Business',
                    'type' => 'LEFT',
                    'conditions' => array(
                        'Deal.business_id = Business.id'
                    )
                ),
                array(
                    'table' => 'tb_provinces',
                    'alias' => 'Province',
                    'type' => 'LEFT',
                    'conditions' => array(
                        'Business.province = Province.province_code'
                    )
                )
            ),

            'conditions' => $conditionFilter,
            'fields' => array('Deal.*', 'Business.*', 'Province.*'),
            'order' => $order,
            'limit' => $this->limit
        ));

        $this->set('deals', $deals);

        // COUNT TOTAL DEAL
        $total_deals = $this->Deal->find('count', array(
            'joins' => array(
                array(
                    'table' => 'tb_businesses',
                    'alias' => 'Business',
                    'type' => 'LEFT',
                    'conditions' => array(
                        'Deal.business_id = Business.id'
                    )
                ),
                array(
                    'table' => 'tb_provinces',
                    'alias' => 'Province',
                    'type' => 'LEFT',
                    'conditions' => array(
                        'Business.province = Province.province_code'
                    )
                )
            ),

            'conditions' => $conditionFilter,
            'fields' => array('Deal.*', 'Business.*', 'Province.*')
        ));

        $total_pages = ceil($total_deals/$this->limit); 
        $this->set('total_deals', $total_deals);
        $this->set('total_pages', $total_pages);

        if(!empty($deals)){             
            $randKey = array_rand($deals);
            $featureDeal = $deals[$randKey];
            $this->set('featureDeal', $featureDeal);
        }

        $this->set('type', $type);
        $this->set('limit', $this->limit);
        $this->set('pageTitle', $pageTitle);
        $this->set('param_location', $location);
        $this->set('categories_checked', $this->categories_checked);
        

    }


    public  function getMore($page = 1, $secure_token = "") {   
        
        $this->Deal->recursive = 3;

        if( !empty($secure_token) && $secure_token == $this->token ){

            $now = date("Y-m-d H:i:s");
            $params = $_REQUEST['params'];

            $type       = (!empty($params['type']))?$params['type'] : "";
            $type       = strtolower($type);
            $location   = (!empty($params['location']))?$params['location'] : "";

            $strDeal = "";

            $conditionFilter    = array() ;
            $order              = "";
            $deals              = array();

            $conditionFilter = array(   'Deal.status'           => 1, 
                                        'Deal.isdeleted'        => 0, 
                                        'Deal.valid_from <='    => $now, 
                                        'Deal.valid_to >'       =>$now 
                                    );

            $order = array( 'Deal.created' => 'DESC' );

            if( $type ){

                if( $type == "deal-of-the-day" ){

                    $conds  = array('DealOfTheDay.available_date' => date('Y-m-d'));

                    $dodConditions['conditions']    = $conds;
                    $dodConditions['order']         = array('DealOfTheDay.created' => 'DESC', 'DealDetail.created' => 'DESC' );
                    $dodConditions['limit']         = $this->limit;

                    // $this->DealOfTheDay->recursive = -1;
                    $dod = $this->DealOfTheDay->find('all', $dodConditions);

                    // Prepare Data Deals
                    $deal_ids = array();
                    if( $dod ){
                        foreach( $dod as $k => $val ){
                            $deal_ids[] = $val['DealOfTheDay']['deal_id'];
                        }
                    }

                    $conditionFilter['Deal.id']         = $deal_ids;
                    $conditionFilter['Business.status'] = 1;

                }else if( $type == 'last-minute-deal' ){

                    $conditionFilter['Deal.is_last_minute_deal'] = 1;
                
                }else if( $type == 'food-and-drink' ) {
                    $cate_ids = $this->food_drink_categories;

                    foreach( $cate_ids as $id ){
                        $conditionFilter['OR'][] = array( 'Deal.goods_category LIKE' => '%"' . $id . '"%' ) ;
                    }
                }else if( $type == 'beauty-and-spa' ){
                    $cate_ids = $this->beauty_spa_categories;

                    foreach( $cate_ids as $id ){
                        $conditionFilter['OR'][] = array( 'Deal.goods_category LIKE' => '%"' . $id . '"%' ) ;
                    }
                }else if( $type == "shopping" ){
                    $cate_ids = $this->shopping_categories;

                    foreach( $cate_ids as $id ){
                        $conditionFilter['OR'][] = array( 'Deal.goods_category LIKE' => '%"' . $id . '"%' ) ;
                    }
                }else if( $type == "things-to-do" ){
                    $cate_ids = $this->things_to_do_categories;

                    foreach( $cate_ids as $id ){
                        $conditionFilter['OR'][] = array( 'Deal.goods_category LIKE' => '%"' . $id . '"%' ) ;
                    }
                }else if( $type == "getaways" ){
                    $cate_ids = $this->getaways_categories;

                    foreach( $cate_ids as $id ){
                        $conditionFilter['OR'][] = array( 'Deal.goods_category LIKE' => '%"' . $id . '"%' ) ;
                    }
                }

            }else if( $location ) {

                if( $location_name = $this->Location->findBySlug($location) ){
                    $conditionFilter['Business.location'] = $location_name['Location']['location_name'];
                }
            }

            $deals = $this->Deal->find('all', array(
                        'joins' => array(
                            array(
                                'table' => 'tb_businesses',
                                'alias' => 'Business',
                                'type' => 'LEFT',
                                'conditions' => array(
                                    'Deal.business_id = Business.id'
                                )
                            ),
                            array(
                                'table' => 'tb_provinces',
                                'alias' => 'Province',
                                'type' => 'LEFT',
                                'conditions' => array(
                                    'Business.province = Province.province_code'
                                )
                            )
                        ),
                        'conditions' => $conditionFilter,
                        'fields' => array('Deal.*', 'Business.*', 'Province.*'),
                        'order' => array('Deal.created DESC'),
                        'limit' => $this->limit,
                        'page' => $page
                    ));
            
            foreach ($deals as $key => $value) {

                $limit_deal_title = $this->limit_deal_title;
                $limit_business_title = $this->limit_business_title;

                $split = end(explode("/", $value['Deal']['image']));
                $thumb_img =  "img/deals/thumbs/" . $split ;
                $url = $this->webroot;
                $business_name = $value['Business']['business_name'];
                $business_street = $value['Business']['street'];
                $business_location = $value['Business']['location'];
                $deal_title = $value['Deal']['title'];
                if(strlen($deal_title) > $limit_deal_title){
                    $deal_title = substr($value['Deal']['title'],0,$limit_deal_title-3).'...';   
                }
                if(strlen($business_name) > $limit_business_title){
                    $business_name = substr($business_name,0,$limit_business_title-3).'...';   
                }

                if( $other_deal_imgs = $value['Deal']['other_images'] ){
                    $imgs = json_decode($other_deal_imgs);
                    $data_img = array();

                    foreach( $imgs as $k => $img ){
                        $data_img[] = $img;
                    }

                    $selected = array_rand($data_img);

                    $thumb_img = str_replace("/deals/", "/deals/thumbs/", $data_img[$selected] );
                }

                $original = "";
                $discount = $value['Deal']['discount_category'];

                $prev_original  = 0;
                $prev_discount  = 0;

                if( $items = $value['DealItemLink'] ){

                    $max_percentage = 0;

                    foreach( $items as $k => $v ){

                        $ori        = ($v['original_price'] != 0 )?$v['original_price']:$v['ItemDetail']['price'];
                        $disc       = $v['discount'];
                        $item_id    = $v['item_id'];

                        $percentage = 100 - ( $disc * 100 / $ori );

                        if( $percentage > $max_percentage ){
                            $max_percentage = $percentage;
                            $prev_original  = $ori;
                            $prev_discount  = $disc;
                        }else if( $percentage == $max_percentage ){
                            if( $disc < $prev_discount ){
                                $prev_original  = $ori;
                                $prev_discount  = $disc;
                            }
                        }

                    }

                    $original = "$" . number_format($prev_original, 2) ;
                    $discount = "$" . number_format($prev_discount, 2);                        
                }

                $strDeal .= '<li class="col-sm-4 col-xs-4">' .
                                '<div class="new-project bg-shadow add-margin-bottom deal-img-container">' .  
                                    '<div>' . 
                                        '<a href="' . $url . 'deal/' . $value['Deal']['slug'] . '">' . 
                                            '<img src="' . $this->webroot . $thumb_img . '" title="' . $value['Deal']['title'] . '">' .
                                        '</a>' . 
                                    '</div>' . 
                                    '<div id="bx-pager">' . 
                                        '<a data-slide-index="0" href="' . $url . 'deal/' . $value['Deal']['slug'] . '"' . ' class="active">' . 
                                            '<div class="new-project-body">' . 
                                                '<h4>' . 
                                                    $deal_title . 
                                                '</h4>' . 
                                                '<i>' . 
                                                    $business_name .
                                                '</i><br>' .
                                                '<i class="icon-location">' . 
                                                    $business_location . ', ' . $value['Province']['province_name'] .
                                                '</i>' .
                                                '<div class="clearfix"></div>' .
                                                '<h4 class="price-off pull-right">'. $discount . '</h4>' .
                                                '<h4 style="color:#666; float:right; margin-right: 10px; text-decoration: line-through;">'. $original . '</h4>' .
                                            '</div>' . 
                                        '</a>' . 
                                    '</div>' . 
                                '</div>' . 
                            '</li>';

            }

            $result['params'] = $conditions;
            $result['strDeal'] = $strDeal;
            echo json_encode($result);

            exit();

        }else{
            return $this->redirect(array('controller'=>'error-page'));
        }
    }

    public  function detail($slug = "") {

        $now = date("Y-m-d H:i:s");

        $this->Deal->recursive = 3;

        $data = $this->Deal->find('first', array(
                    'joins' => array(
                        array(
                            'table' => 'tb_businesses',
                            'alias' => 'Business',
                            'type' => 'LEFT',
                            'conditions' => array(
                                'Deal.business_id = Business.id'
                            )
                        ),
                        array(
                            'table' => 'tb_provinces',
                            'alias' => 'Province',
                            'type' => 'LEFT',
                            'conditions' => array(
                                'Business.province = Province.province_code'
                            )
                        ),
                        array(
                            'table' => 'tb_cities',
                            'alias' => 'City',
                            'type' => 'LEFT',
                            'conditions' => array(
                                'Business.city = City.city_code'
                            )
                        )
                    ),
                    'conditions' => array(
                        'Deal.slug' => $slug,
                        'Deal.valid_from <=' => $now,
                        'Deal.valid_to >' => $now,
                        'Deal.status' => 1,
                        'Deal.isdeleted' => 0
                    ),
                    'fields' => array('Deal.*', 'Business.*', 'Province.*', 'City.*')
                ));

        $this->set('data', $data);

        $this->set('slug', $slug);

        if( $data ){
            $mainCategoryInfo = $this->BusinessCategory->findById($data['Business']['business_main_category']);
            $businessCategory['main_category'] = $mainCategoryInfo['BusinessCategory']['category'];
        }

        if(!empty($data['Business']['business_sub_category'])){
            $subCategoryInfo = $this->BusinessCategory->findById($data['Business']['business_sub_category']);
            $businessCategory['sub_category'] = $subCategoryInfo['BusinessCategory']['category'];
        }else{
            $businessCategory['sub_category'] = "";
        }

        $this->set('businessCategory', $businessCategory);

        $travelInfo = array();

        if(!empty($data)){

            $arrServices = array();

            $businessID = $data['Business']['id'];  

            if(!empty($data['Deal']['services_category'])){
                $arrServices = json_decode($data['Deal']['services_category']);
                if(empty($arrServices)){
                    $arrServices = array();
                }
            }

            if(in_array("Travel", $arrServices)){
                $arrTravel = json_decode($data['Deal']['travel_destination']);
                foreach ($arrTravel as $key => $value) {
                    if(!empty($value)){
                        if($value != "All"){
                            $travelInfo[] = $this->Destination->findById($value);   
                        }else{
                            $travelInfo[] = array('Destination'=>array('destination'=>'All'));
                        }
                    }
                }
            }

            $branches = $this->BusinessBranch->find('all', 
                                                    array( 
                                                        "conditions" => 
                                                            array( "BusinessBranch.status" => 1,
                                                                    "BusinessBranch.business_id" => $businessID ),
                                                        'order' => array( 'BusinessBranch.branch_name' => 'ASC' )
                                                    )
                                                );
            $this->set('branches', $branches);

        }else{
            $businessID = "XXX-XXX";
        }

        $this->set('travelDestination', $travelInfo);

        if(!empty($data)){

            $opHours = $this->OperationHour->find('all', array('conditions'=>array('OperationHour.business_id'=>$businessID)));
            $this->set('opHours', $opHours);

            $businessMedia = $this->BusinessesMedia->find('all', array('conditions'=>array('BusinessesMedia.business_id'=>$businessID)));
            $this->set('businessMedia', $businessMedia);

            $deal_info = $data['Deal'];
            $interest_category = json_decode($deal_info['interest_category']);
            $goods_category = json_decode($deal_info['goods_category']);
            $services_category = json_decode($deal_info['services_category']);

        }

        if(!empty($interest_category)){

            foreach ($interest_category as $key) {
                $arrCondition["OR"][] = array('Deal.interest_category LIKE' => "%$key%");
            }

            $conditions = $arrCondition;

        }

        if(!empty($goods_category)){

            foreach ($goods_category as $key) {
                $arrCondition["OR"][] = array('Deal.goods_category LIKE' => "%$key%");
            }

            $conditions = $arrCondition;

        }

        if(!empty($services_category)){

            foreach ($services_category as $key) {
                $arrCondition["OR"][] = array('Deal.services_category LIKE' => "%$key%");
            }

            $conditions = $arrCondition;

        }

        if(!empty($deal_info)){
            $dealID = $deal_info['id'];
        }else{
            $dealID = "XXX-XXX";
        }

        $conditions["NOT"] = array('Deal.id' => $dealID);

        $conditions["AND"] = array('Deal.status'=>1, 'Deal.valid_from <=' => $now, 'Deal.valid_to >' => $now, 'Business.status'=>1);

        // $relatedDeals = $this->Deal->find('all', array('conditions' => $conditions, 'limit' => 8));

        $relatedDeals = $this->Deal->find('all', array(
                        'joins' => array(
                            array(
                                'table' => 'tb_businesses',
                                'alias' => 'Business',
                                'type' => 'LEFT',
                                'conditions' => array(
                                    'Deal.business_id = Business.id'
                                )
                            ),
                            array(
                                'table' => 'tb_provinces',
                                'alias' => 'Province',
                                'type' => 'LEFT',
                                'conditions' => array(
                                    'Business.province = Province.province_code'
                                )
                            )
                        ),
                        'conditions' => $conditions,
                        'fields' => array('Deal.*', 'Business.*', 'Province.*'),
                        'order' => array('Deal.created DESC'),
                        'limit' => 16
                    ));
        $this->set('relatedDeals', $relatedDeals);
        // var_dump($relatedDeals);exit();
        $info = NULL;
        if( $this->Session->read('Buyer.__SES_LOGGED_INFO') ) {
            $info   = $this->Session->read('Buyer.__SES_LOGGED_INFO');
        }
        
        $this->set('__USER_LOGGED', $info);

        if( $type ){
             // var_dump($type);exit();
            $pageTitle = str_replace("-", " ", $type);
            $pageTitle = str_replace(" and ", " & ", $pageTitle);
            $pageTitle = ucwords($pageTitle);

            if( $type == "deal-of-the-day" ){

                $conds  = array('DealOfTheDay.available_date' => date('Y-m-d'));

                $dodConditions['conditions']    = $conds;
                $dodConditions['order']         = array('DealOfTheDay.created' => 'DESC', 'DealDetail.created' => 'DESC' );
                $dodConditions['limit']         = $this->limit;

                // $this->DealOfTheDay->recursive = -1;
                $dod = $this->DealOfTheDay->find('all', $dodConditions);
                // Prepare Data Deals
                $deal_ids = array();
                if( $dod ){
                    foreach( $dod as $k => $val ){
                        $deal_ids[] = $val['DealOfTheDay']['deal_id'];
                    }
                }

                $conditionFilter['Deal.id']         = $deal_ids;

            }else if( $type == 'last-minute-deal' ){

                $conditionFilter['Deal.is_last_minute_deal'] = 1;
            
            }else if( $type == 'food-and-drink' ) {
                $cate_ids = $this->food_drink_categories;
                $this->categories_checked = $this->food_drink_categories;

                foreach( $cate_ids as $id ){
                    $conditionFilter['OR'][] = array( 'Deal.goods_category LIKE' => '%"' . $id . '"%' ) ;
                }
            }else if( $type == 'beauty-and-spa' ){
                $cate_ids = $this->beauty_spa_categories;
                $this->categories_checked = $this->beauty_spa_categories;

                foreach( $cate_ids as $id ){
                    $conditionFilter['OR'][] = array( 'Deal.goods_category LIKE' => '%"' . $id . '"%' ) ;
                }
            }else if( $type == "shopping" ){
                $cate_ids = $this->shopping_categories;
                $this->categories_checked = $this->shopping_categories;

                foreach( $cate_ids as $id ){
                    $conditionFilter['OR'][] = array( 'Deal.goods_category LIKE' => '%"' . $id . '"%' ) ;
                }
            }else if( $type == "things-to-do" ){
                $cate_ids = $this->things_to_do_categories;
                $this->categories_checked = $this->things_to_do_categories;

                foreach( $cate_ids as $id ){
                    $conditionFilter['OR'][] = array( 'Deal.goods_category LIKE' => '%"' . $id . '"%' ) ;
                }
            }else if( $type == "getaways" ){
                $cate_ids = $this->getaways_categories;
                $this->categories_checked = $this->getaways_categories;

                foreach( $cate_ids as $id ){
                    $conditionFilter['OR'][] = array( 'Deal.goods_category LIKE' => '%"' . $id . '"%' ) ;
                }
            }

        }else if( $location ) {

            $pageTitle = str_replace("-", " ", $location);
            $pageTitle = str_replace(" and ", " & ", $pageTitle);
            $pageTitle = ucwords($pageTitle);

            if( $location_name = $this->Location->findBySlug($location) ){
                $conditionFilter['Business.location'] = $location_name['Location']['location_name'];
            }
        }
        // var_dump($type);exit();
    }

    public function search(){

        $getForm = $this->request->query;

        $searchText = (!empty($getForm['searchText'])) ? $getForm['searchText'] : "";
        $searchCity = (!empty($getForm['searchCity'])) ? $getForm['searchCity'] : "";
        $now = date('Y-m-d H:i:s');

        if(!empty($searchCity)){

            $arrCondition[0] = array('Deal.status' => 1, 'Deal.valid_from <=' => $now, 'Deal.valid_to >' => $now);

            if(!empty($searchText)){
                $arrCondition[1] = array('Deal.title LIKE' => "%$searchText%");
            }

            if($searchCity != "all"){

                $city = $this->City->findByCityName($searchCity);
                $cityCode = $city['City']['city_code'];
                $arrCondition[2] = array('Business.city' => $cityCode);
            }

            $conditions = $arrCondition;

            $deals = $this->Deal->find('all', array(
                        'joins' => array(
                            array(
                                'table' => 'tb_businesses',
                                'alias' => 'Business',
                                'type' => 'LEFT',
                                'conditions' => array(
                                    'Deal.business_id = Business.id'
                                )
                            )
                        ),
                        'conditions' => $conditions,
                        'fields' => array('Deal.*', 'Business.*'),
                        'limit' => 30
                    ));

            $this->set('deals', $deals);

            $this->set('searchText', $searchText);

            $this->set('searchCity', $searchCity);

        }

    }

    public function checkIfDealAvailableAjax__( $deal_id = 0 ){

        if( !$deal_id ){
            $data['status'] = false;
            $data['message']    = "Invalid Request !";
            echo json_encode($data); exit;
        }

        $now = date('Y-m-d H:i:s');
        $conds  = array(    'Deal.status'           => 1, 
                            'Deal.isdeleted'        => 0, 
                            'Deal.valid_from <='    => $now, 
                            'Deal.valid_to >'       => $now,
                            'Business.status'       => 1,
                            'Deal.id'               => $deal_id
                        );

        $deals = $this->Deal->find('count', array(
                        'joins' => array(
                            array(
                                'table' => 'tb_businesses',
                                'alias' => 'Business',
                                'type' => 'LEFT',
                                'conditions' => array(
                                    'Deal.business_id = Business.id'
                                )
                            )
                        ),
                        'conditions' => $conds,
                        'fields' => array('Deal.*', 'Business.*'),
            ));

        if( $deals ){
            $data['status'] = true;

        }else{
            $data['status']     = false;
            $data['message']    = "This deal is no longer available!";

            $this->Session->setFlash(__( 'This Deal is no longer available !' )
                                            .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                                            'default',
                                            array('class'=>'alert alert-dismissable alert-error align-center '));
        
        }

        echo json_encode($data); exit;
    }

}