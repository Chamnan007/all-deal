<?php
App::uses('AppController', 'Controller');
/**
 * Locations Controller
 *
 * @property Location $Location
 * @property PaginatorComponent $Paginator
 */
class LocationsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	// var $context = 'Location';

/**
 * index method
 *
 * @return void
 */
	public function index() {

	}
	

	public function get_location_by_city_code( $city_code = null ){

		if( $city_code != null ){

			// $location = new Location();
			$locations =  $this->Location->find('all', array( 'conditions' => array('Location.city_code' => $city_code ))) ;

			$data = array();
			$data['data'] =  $locations;
			echo json_encode( $data);

			$this->autoRender = false;

		}
	}

}
