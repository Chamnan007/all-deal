<?php

/********************************************************************************

File Name: CitiesController.php
Description: controlling city module function

Powered By: ABi Investment Group Co., Ltd,

Changed History:

    Date                Author              Description
    2014/06/10          Sim Chhayrambo      Initial Version

*********************************************************************************/

App::uses('AdministratorAppController', 'Administrator.Controller');
App::uses('Province', 'Administrator.Model');

/**
 * Cities Controller
 *
 * @property City $City
 * @property PaginatorComponent $Paginator
*/

class CitiesController extends AdministratorAppController {

/**
 * Components
 *
 * @var array
 */
	var $context = 'City';
	var $uses  = array( 'Administrator.Province' ,'Administrator.City' );

/**
 * index method
 *
 * @return void
 */
	public function index() {
		// $this->City->recursive = 1;
		// $this->set('cities', $this->Paginator->paginate());
		$this->Province  = new Province();
		$this->set('provinces', $this->Province->find('all'));

		$this->conditionFilter['City.status'] = 1 ;
		$this->paginate['conditions'] = $this->conditionFilter;
		$this->paginate['order'] = array("City.city_code" => 'ASC' ) ;
		parent::index();

	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->City->exists($id)) {
			throw new NotFoundException(__('Invalid city'));
		}
		$options = array('conditions' => array('City.' . $this->City->primaryKey => $id));
		$this->set('city', $this->City->find('first', $options));

	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		
		$this->Province  = new Province();
		$this->set('provinces', $this->Province->find('all', array('conditions' => array('Province.status=1') ) ));

		if ($this->request->is('post')) {
			parent::save();
		}
		
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->Province  = new Province();
		$this->set('provinces', $this->Province->find('all', array('conditions' => array('Province.status=1') ) ));
		if (!$this->City->exists($id)) {
			throw new NotFoundException(__('Invalid city'));
		}
		if ($this->request->is(array('post', 'put'))) {
			parent::save($id);
		} else {
			$options = array('conditions' => array('City.' . $this->City->primaryKey => $id));
			$this->request->data = $this->City->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {

		if (!$this->City->exists($id)) {
			throw new NotFoundException(__('Invalid City'));
		}

		$this->City->id = $id;
		$this->request->data['City']['status'] = 0;
		
		if ($this->City->save(array('status'=>0))) 
		{
			$this->Session->setFlash(__('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button><b>Success!!!</b> The city has been deleted.</div>'));

			$obj 	= $this->City->findById($id);
			$logMessage = json_encode($obj);
			parent::generateLog($logMessage,' DELETE :'.$id);
		} else {

			$this->Session->setFlash(__('<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button><b>Failed!!!</b> The city could not be deleted. Please, try again.</div>'));
		}
	
		return $this->redirect(array('action' => 'index'));
	}

}
