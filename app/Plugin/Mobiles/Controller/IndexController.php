<?php

/********************************************************************************

File Name: IndexController.php
Description: Controller for index page

Powered By: ABi Investment Group Co., Ltd,

Changed History:

    Date                Author              Description
    2014/06/13          Sim Chhayrambo      Check deals by business status
    2014/08/11          Sim Chhayrambo      Get Food & Drink deals
    2015/06/01          Rattana             Check Discount, Deal Items
    2105/07/10          Rattana             List Deal by Category Type

*********************************************************************************/

App::uses('AppController', 'Controller');

/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class IndexController extends AppController {

/**
 * Components
 *
 * @var array
 */
    public $components = array('Paginator');
    
    var $uses = array(  'OperationHour', 
                        'Deal', 
                        'BusinessesMedia', 
                        'City', 
                        'Destination', 
                        'GoodsCategory',
                        'BusinessCategory', 
                        'BusinessBranch',
                        'DealOfTheDay' );

/**
 * index method
 *
 * @return void
 */
    public  function index(){   

        $now = date("Y-m-d H:i:s");

        $this->Deal->recursive = 3;
        $now = date("Y-m-d H:i:s");

        $pageTitle          = "";
        $deals              = array();

        $order = array( 'Deal.created' => 'DESC' );

        // Deal of the day
        $conds  = array('DealOfTheDay.available_date' => date('Y-m-d'));
        $dodConditions['conditions']    = $conds;
        $dodConditions['order']         = array('DealOfTheDay.created' => 'DESC', 'DealDetail.created' => 'DESC' );
        $dodConditions['limit']         = $this->limit;

        $dod = $this->DealOfTheDay->find('all', $dodConditions);

        // Prepare Data Deals
        $deal_ids = array();
        if( $dod ){
            foreach( $dod as $k => $val ){
                $deal_ids[] = $val['DealOfTheDay']['deal_id'];
            }
        }

        $conditionFilter    = array() ;
        $conditionFilter    = array(    'Deal.id'               => $deal_ids,
                                        'Deal.status'           => 1, 
                                        'Deal.isdeleted'        => 0, 
                                        'Deal.valid_from <='    => $now, 
                                        'Deal.valid_to >'       =>$now ,
                                        'Business.status'       => 1
                                );

        // Get Deal Information
        $deal_of_the_day = $this->Deal->find('all', array(
            'joins' => array(
                array(
                    'table' => 'tb_businesses',
                    'alias' => 'Business',
                    'type' => 'LEFT',
                    'conditions' => array(
                        'Deal.business_id = Business.id'
                    )
                ),
                array(
                    'table' => 'tb_provinces',
                    'alias' => 'Province',
                    'type' => 'LEFT',
                    'conditions' => array(
                        'Business.province = Province.province_code'
                    )
                )
            ),

            'conditions' => $conditionFilter,
            'fields' => array('Deal.*', 'Business.*', 'Province.*'),
            'order' => $order,
            'limit' => 4
        ));

        $this->set('deal_of_the_day', $deal_of_the_day);

        // Last Minute Deal
        $conditionFilter    = array() ;
        $conditionFilter    = array(    'Deal.status'               => 1, 
                                        'Deal.isdeleted'            => 0, 
                                        'Deal.valid_from <='        => $now, 
                                        'Deal.valid_to >'           =>$now ,
                                        'Business.status'           => 1,
                                        'Deal.is_last_minute_deal'  => 1
                                );

        $last_minute_deals = $this->Deal->find('all', array(
            'joins' => array(
                array(
                    'table' => 'tb_businesses',
                    'alias' => 'Business',
                    'type' => 'LEFT',
                    'conditions' => array(
                        'Deal.business_id = Business.id'
                    )
                ),
                array(
                    'table' => 'tb_provinces',
                    'alias' => 'Province',
                    'type' => 'LEFT',
                    'conditions' => array(
                        'Business.province = Province.province_code'
                    )
                )
            ),

            'conditions' => $conditionFilter,
            'fields' => array('Deal.*', 'Business.*', 'Province.*'),
            'order' => $order,
            'limit' => 4
        ));
        
        $this->set('last_minute_deals', $last_minute_deals);

        // Food & Drink Deal
        $conditionFilter    = array() ;
        $conditionFilter    = array(    'Deal.status'               => 1, 
                                        'Deal.isdeleted'            => 0, 
                                        'Deal.valid_from <='        => $now, 
                                        'Deal.valid_to >'           =>$now ,
                                        'Business.status'           => 1
                                );
        $cate_ids = $this->food_drink_categories;

        foreach( $cate_ids as $id ){
            $conditionFilter['OR'][] = array( 'Deal.goods_category LIKE' => '%"' . $id . '"%' ) ;
        }

        $food_and_drink_deals = $this->Deal->find('all', array(
            'joins' => array(
                array(
                    'table' => 'tb_businesses',
                    'alias' => 'Business',
                    'type' => 'LEFT',
                    'conditions' => array(
                        'Deal.business_id = Business.id'
                    )
                ),
                array(
                    'table' => 'tb_provinces',
                    'alias' => 'Province',
                    'type' => 'LEFT',
                    'conditions' => array(
                        'Business.province = Province.province_code'
                    )
                )
            ),

            'conditions' => $conditionFilter,
            'fields' => array('Deal.*', 'Business.*', 'Province.*'),
            'order' => $order,
            'limit' => 4
        ));
        $this->set('food_and_drink_deals', $food_and_drink_deals);

        // Beauty & Spa Deal
        $conditionFilter    = array() ;
        $conditionFilter    = array(    'Deal.status'               => 1, 
                                        'Deal.isdeleted'            => 0, 
                                        'Deal.valid_from <='        => $now, 
                                        'Deal.valid_to >'           =>$now ,
                                        'Business.status'           => 1
                                );
        $cate_ids = $this->beauty_spa_categories;

        foreach( $cate_ids as $id ){
            $conditionFilter['OR'][] = array( 'Deal.goods_category LIKE' => '%"' . $id . '"%' ) ;
        }

        $beauty_and_spa_deals = $this->Deal->find('all', array(
            'joins' => array(
                array(
                    'table' => 'tb_businesses',
                    'alias' => 'Business',
                    'type' => 'LEFT',
                    'conditions' => array(
                        'Deal.business_id = Business.id'
                    )
                ),
                array(
                    'table' => 'tb_provinces',
                    'alias' => 'Province',
                    'type' => 'LEFT',
                    'conditions' => array(
                        'Business.province = Province.province_code'
                    )
                )
            ),

            'conditions' => $conditionFilter,
            'fields' => array('Deal.*', 'Business.*', 'Province.*'),
            'order' => $order,
            'limit' => 4
        ));
        $this->set('beauty_and_spa_deals', $beauty_and_spa_deals);


        // Shopping Deal
        $conditionFilter    = array() ;
        $conditionFilter    = array(    'Deal.status'               => 1, 
                                        'Deal.isdeleted'            => 0, 
                                        'Deal.valid_from <='        => $now, 
                                        'Deal.valid_to >'           =>$now ,
                                        'Business.status'           => 1
                                );
        $cate_ids = $this->shopping_categories;

        foreach( $cate_ids as $id ){
            $conditionFilter['OR'][] = array( 'Deal.goods_category LIKE' => '%"' . $id . '"%' ) ;
        }

        $shopping_deals = $this->Deal->find('all', array(
            'joins' => array(
                array(
                    'table' => 'tb_businesses',
                    'alias' => 'Business',
                    'type' => 'LEFT',
                    'conditions' => array(
                        'Deal.business_id = Business.id'
                    )
                ),
                array(
                    'table' => 'tb_provinces',
                    'alias' => 'Province',
                    'type' => 'LEFT',
                    'conditions' => array(
                        'Business.province = Province.province_code'
                    )
                )
            ),

            'conditions' => $conditionFilter,
            'fields' => array('Deal.*', 'Business.*', 'Province.*'),
            'order' => $order,
            'limit' => 4
        ));
        $this->set('shopping_deals', $shopping_deals);

        // Things to do Deal
        $conditionFilter    = array() ;
        $conditionFilter    = array(    'Deal.status'               => 1, 
                                        'Deal.isdeleted'            => 0, 
                                        'Deal.valid_from <='        => $now, 
                                        'Deal.valid_to >'           => $now ,
                                        'Business.status'           => 1
                                );
        $cate_ids = $this->things_to_do_categories;

        foreach( $cate_ids as $id ){
            $conditionFilter['OR'][] = array( 'Deal.goods_category LIKE' => '%"' . $id . '"%' ) ;
        }

        $things_to_do_deals = $this->Deal->find('all', array(
            'joins' => array(
                array(
                    'table' => 'tb_businesses',
                    'alias' => 'Business',
                    'type' => 'LEFT',
                    'conditions' => array(
                        'Deal.business_id = Business.id'
                    )
                ),
                array(
                    'table' => 'tb_provinces',
                    'alias' => 'Province',
                    'type' => 'LEFT',
                    'conditions' => array(
                        'Business.province = Province.province_code'
                    )
                )
            ),

            'conditions' => $conditionFilter,
            'fields' => array('Deal.*', 'Business.*', 'Province.*'),
            'order' => $order,
            'limit' => 4
        ));
        $this->set('things_to_do_deals', $things_to_do_deals);

        // Getaways Deal
        $conditionFilter    = array() ;
        $conditionFilter    = array(    'Deal.status'               => 1, 
                                        'Deal.isdeleted'            => 0, 
                                        'Deal.valid_from <='        => $now, 
                                        'Deal.valid_to >'           =>$now ,
                                        'Business.status'           => 1
                                );
        $cate_ids = $this->getaways_categories;

        foreach( $cate_ids as $id ){
            $conditionFilter['OR'][] = array( 'Deal.goods_category LIKE' => '%"' . $id . '"%' ) ;
        }

        $getaways_deals = $this->Deal->find('all', array(
            'joins' => array(
                array(
                    'table' => 'tb_businesses',
                    'alias' => 'Business',
                    'type' => 'LEFT',
                    'conditions' => array(
                        'Deal.business_id = Business.id'
                    )
                ),
                array(
                    'table' => 'tb_provinces',
                    'alias' => 'Province',
                    'type' => 'LEFT',
                    'conditions' => array(
                        'Business.province = Province.province_code'
                    )
                )
            ),

            'conditions' => $conditionFilter,
            'fields' => array('Deal.*', 'Business.*', 'Province.*'),
            'order' => $order,
            'limit' => 4
        ));
        $this->set('getaways_deals', $getaways_deals);


        // Random of Last Week Deal to be feature
        $lastWeek = date('Y-m-d H:i:s',strtotime("-7 days"));

        $conditions = array('Deal.created >= ' => $lastWeek,
                            'Deal.created <= ' => $now,
                            'Deal.status'=>1, 
                            'Deal.valid_from <='=> $now,
                            'Deal.valid_to >'=>$now,
                            'Business.status'=>1);

        $featureDeal = $this->Deal->find('all', array(
                            'joins' => array(
                                array(
                                    'table' => 'tb_businesses',
                                    'alias' => 'Business',
                                    'type' => 'LEFT',
                                    'conditions' => array(
                                        'Deal.business_id = Business.id'
                                    )
                                ),
                                array(
                                    'table' => 'tb_provinces',
                                    'alias' => 'Province',
                                    'type' => 'LEFT',
                                    'conditions' => array(
                                        'Business.province = Province.province_code'
                                    )
                                )
                            ),
                            'conditions' => $conditions,
                            'fields' => array('Deal.*', 'Business.*', 'Province.*'),
                            'order' => 'rand()',
                            'limit' => 1
                        ));

        // Random of Last Month Deal to be feature
        if(empty($featureDeal)){

            $lastWeek = date('Y-m-d H:i:s',strtotime("-30 days"));

            $conditions = array('Deal.created >= ' => $lastWeek,
                                'Deal.created <= ' => $now,
                                'Deal.status'=>1, 
                                'Deal.valid_from <='=> $now,
                                'Deal.valid_to >'=>$now,
                                'Business.status'=>1);

            $featureDeal = $this->Deal->find('all', array(
                                'joins' => array(
                                    array(
                                        'table' => 'tb_businesses',
                                        'alias' => 'Business',
                                        'type' => 'LEFT',
                                        'conditions' => array(
                                            'Deal.business_id = Business.id'
                                        )
                                    ),
                                    array(
                                        'table' => 'tb_provinces',
                                        'alias' => 'Province',
                                        'type' => 'LEFT',
                                        'conditions' => array(
                                            'Business.province = Province.province_code'
                                        )
                                    )
                                ),
                                'conditions' => $conditions,
                                'fields' => array('Deal.*', 'Business.*', 'Province.*'),
                                'order' => 'rand()',
                                'limit' => 1
                            ));

            if(empty($featureDeal)){

                $conditions = array('Deal.status'=>1, 
                                    'Deal.valid_from <='=> $now,
                                    'Deal.valid_to >'=>$now,
                                    'Business.status'=>1);

                $featureDeal = $this->Deal->find('all', array(
                                    'joins' => array(
                                        array(
                                            'table' => 'tb_businesses',
                                            'alias' => 'Business',
                                            'type' => 'LEFT',
                                            'conditions' => array(
                                                'Deal.business_id = Business.id'
                                            )
                                        ),
                                        array(
                                            'table' => 'tb_provinces',
                                            'alias' => 'Province',
                                            'type' => 'LEFT',
                                            'conditions' => array(
                                                'Business.province = Province.province_code'
                                            )
                                        )
                                    ),
                                    'conditions' => $conditions,
                                    'fields' => array('Deal.*', 'Business.*', 'Province.*'),
                                    'order' => 'rand()',
                                    'limit' => 1
                                ));

            }

        }

        $this->set('featureDeal', $featureDeal);


        // Count Deal by each type
        // $now = date("Y-m-d H:i:s");

        // $this->Deal->recursive = 3;
        // $now = date("Y-m-d H:i:s");

        // $pageTitle          = "";
        // $deals              = array();

        // $order = array( 'Deal.created' => 'DESC' );

        // // Deal of the day
        // $conds  = array('DealOfTheDay.available_date' => date('Y-m-d'));
        // $dodConditions['conditions']    = $conds;
        // $dodConditions['order']         = array('DealOfTheDay.created' => 'DESC', 'DealDetail.created' => 'DESC' );
        // $dodConditions['limit']         = $this->limit;

        // $dod = $this->DealOfTheDay->find('all', $dodConditions);

        // // Prepare Data Deals
        // $deal_ids = array();
        // if( $dod ){
        //     foreach( $dod as $k => $val ){
        //         $deal_ids[] = $val['DealOfTheDay']['deal_id'];
        //     }
        // }

        // $conditionFilter    = array() ;
        // $conditionFilter    = array(    'Deal.id'               => $deal_ids,
        //                                 'Deal.status'           => 1, 
        //                                 'Deal.isdeleted'        => 0, 
        //                                 'Deal.valid_from <='    => $now, 
        //                                 'Deal.valid_to >'       =>$now ,
        //                                 'Business.status'       => 1
        //                         );

        // // Get Deal Information
        // $deal_of_the_day = $this->Deal->find('count', array(
        //     'joins' => array(
        //         array(
        //             'table' => 'tb_businesses',
        //             'alias' => 'Business',
        //             'type' => 'LEFT',
        //             'conditions' => array(
        //                 'Deal.business_id = Business.id'
        //             )
        //         ),
        //         array(
        //             'table' => 'tb_provinces',
        //             'alias' => 'Province',
        //             'type' => 'LEFT',
        //             'conditions' => array(
        //                 'Business.province = Province.province_code'
        //             )
        //         )
        //     ),

        //     'conditions' => $conditionFilter,
        //     'fields' => array('Deal.*', 'Business.*', 'Province.*'),
        //     'order' => NULL,
        //     'limit' => NULL
        // ));

        // $this->set('total_deal_of_the_day', $deal_of_the_day);

        // // Last Minute Deal
        // $conditionFilter    = array() ;
        // $conditionFilter    = array(    'Deal.status'               => 1, 
        //                                 'Deal.isdeleted'            => 0, 
        //                                 'Deal.valid_from <='        => $now, 
        //                                 'Deal.valid_to >'           =>$now ,
        //                                 'Business.status'           => 1,
        //                                 'Deal.is_last_minute_deal'  => 1
        //                         );

        // $total_last_minute_deals = $this->Deal->find('count', array(
        //     'joins' => array(
        //         array(
        //             'table' => 'tb_businesses',
        //             'alias' => 'Business',
        //             'type' => 'LEFT',
        //             'conditions' => array(
        //                 'Deal.business_id = Business.id'
        //             )
        //         ),
        //         array(
        //             'table' => 'tb_provinces',
        //             'alias' => 'Province',
        //             'type' => 'LEFT',
        //             'conditions' => array(
        //                 'Business.province = Province.province_code'
        //             )
        //         )
        //     ),

        //     'conditions' => $conditionFilter,
        //     'fields' => array('Deal.*', 'Business.*', 'Province.*'),
        //     'order' => $order,
        //     'limit' => NULL
        // ));
        // $this->set('total_last_minute_deals', $total_last_minute_deals);

        // // Food & Drink Deal
        // $conditionFilter    = array() ;
        // $conditionFilter    = array(    'Deal.status'               => 1, 
        //                                 'Deal.isdeleted'            => 0, 
        //                                 'Deal.valid_from <='        => $now, 
        //                                 'Deal.valid_to >'           =>$now ,
        //                                 'Business.status'           => 1
        //                         );
        // $cate_ids = $this->food_drink_categories;

        // foreach( $cate_ids as $id ){
        //     $conditionFilter['OR'][] = array( 'Deal.goods_category LIKE' => '%"' . $id . '"%' ) ;
        // }

        // $total_food_and_drink_deals = $this->Deal->find('count', array(
        //     'joins' => array(
        //         array(
        //             'table' => 'tb_businesses',
        //             'alias' => 'Business',
        //             'type' => 'LEFT',
        //             'conditions' => array(
        //                 'Deal.business_id = Business.id'
        //             )
        //         ),
        //         array(
        //             'table' => 'tb_provinces',
        //             'alias' => 'Province',
        //             'type' => 'LEFT',
        //             'conditions' => array(
        //                 'Business.province = Province.province_code'
        //             )
        //         )
        //     ),

        //     'conditions' => $conditionFilter,
        //     'fields' => array('Deal.*', 'Business.*', 'Province.*'),
        //     'order' => $order,
        //     'limit' => NULL
        // ));
        // $this->set('total_food_and_drink_deals', $total_food_and_drink_deals);

        // // Beauty & Spa Deal
        // $conditionFilter    = array() ;
        // $conditionFilter    = array(    'Deal.status'               => 1, 
        //                                 'Deal.isdeleted'            => 0, 
        //                                 'Deal.valid_from <='        => $now, 
        //                                 'Deal.valid_to >'           =>$now ,
        //                                 'Business.status'           => 1
        //                         );
        // $cate_ids = $this->beauty_spa_categories;

        // foreach( $cate_ids as $id ){
        //     $conditionFilter['OR'][] = array( 'Deal.goods_category LIKE' => '%"' . $id . '"%' ) ;
        // }

        // $total_beauty_and_spa_deals = $this->Deal->find('count', array(
        //     'joins' => array(
        //         array(
        //             'table' => 'tb_businesses',
        //             'alias' => 'Business',
        //             'type' => 'LEFT',
        //             'conditions' => array(
        //                 'Deal.business_id = Business.id'
        //             )
        //         ),
        //         array(
        //             'table' => 'tb_provinces',
        //             'alias' => 'Province',
        //             'type' => 'LEFT',
        //             'conditions' => array(
        //                 'Business.province = Province.province_code'
        //             )
        //         )
        //     ),

        //     'conditions' => $conditionFilter,
        //     'fields' => array('Deal.*', 'Business.*', 'Province.*'),
        //     'order' => $order,
        //     'limit' => NULL
        // ));
        // $this->set('total_beauty_and_spa_deals', $total_beauty_and_spa_deals);


        // // Shopping Deal
        // $conditionFilter    = array() ;
        // $conditionFilter    = array(    'Deal.status'               => 1, 
        //                                 'Deal.isdeleted'            => 0, 
        //                                 'Deal.valid_from <='        => $now, 
        //                                 'Deal.valid_to >'           =>$now ,
        //                                 'Business.status'           => 1
        //                         );
        // $cate_ids = $this->shopping_categories;

        // foreach( $cate_ids as $id ){
        //     $conditionFilter['OR'][] = array( 'Deal.goods_category LIKE' => '%"' . $id . '"%' ) ;
        // }

        // $total_shopping_deals = $this->Deal->find('count', array(
        //     'joins' => array(
        //         array(
        //             'table' => 'tb_businesses',
        //             'alias' => 'Business',
        //             'type' => 'LEFT',
        //             'conditions' => array(
        //                 'Deal.business_id = Business.id'
        //             )
        //         ),
        //         array(
        //             'table' => 'tb_provinces',
        //             'alias' => 'Province',
        //             'type' => 'LEFT',
        //             'conditions' => array(
        //                 'Business.province = Province.province_code'
        //             )
        //         )
        //     ),

        //     'conditions' => $conditionFilter,
        //     'fields' => array('Deal.*', 'Business.*', 'Province.*'),
        //     'order' => $order,
        //     'limit' => NULL
        // ));
        // $this->set('total_shopping_deals', $total_shopping_deals);

        // // Things to do Deal
        // $conditionFilter    = array() ;
        // $conditionFilter    = array(    'Deal.status'               => 1, 
        //                                 'Deal.isdeleted'            => 0, 
        //                                 'Deal.valid_from <='        => $now, 
        //                                 'Deal.valid_to >'           =>$now ,
        //                                 'Business.status'           => 1
        //                         );
        // $cate_ids = $this->things_to_do_categories;

        // foreach( $cate_ids as $id ){
        //     $conditionFilter['OR'][] = array( 'Deal.goods_category LIKE' => '%"' . $id . '"%' ) ;
        // }

        // $total_things_to_do_deals = $this->Deal->find('count', array(
        //     'joins' => array(
        //         array(
        //             'table' => 'tb_businesses',
        //             'alias' => 'Business',
        //             'type' => 'LEFT',
        //             'conditions' => array(
        //                 'Deal.business_id = Business.id'
        //             )
        //         ),
        //         array(
        //             'table' => 'tb_provinces',
        //             'alias' => 'Province',
        //             'type' => 'LEFT',
        //             'conditions' => array(
        //                 'Business.province = Province.province_code'
        //             )
        //         )
        //     ),

        //     'conditions' => $conditionFilter,
        //     'fields' => array('Deal.*', 'Business.*', 'Province.*'),
        //     'order' => $order,
        //     'limit' => NULL
        // ));
        // $this->set('total_things_to_do_deals', $total_things_to_do_deals);

        // // Getaways Deal
        // $conditionFilter    = array() ;
        // $conditionFilter    = array(    'Deal.status'               => 1, 
        //                                 'Deal.isdeleted'            => 0, 
        //                                 'Deal.valid_from <='        => $now, 
        //                                 'Deal.valid_to >'           =>$now ,
        //                                 'Business.status'           => 1
        //                         );
        // $cate_ids = $this->getaways_categories;

        // foreach( $cate_ids as $id ){
        //     $conditionFilter['OR'][] = array( 'Deal.goods_category LIKE' => '%"' . $id . '"%' ) ;
        // }

        // $total_getaways_deals = $this->Deal->find('count', array(
        //     'joins' => array(
        //         array(
        //             'table' => 'tb_businesses',
        //             'alias' => 'Business',
        //             'type' => 'LEFT',
        //             'conditions' => array(
        //                 'Deal.business_id = Business.id'
        //             )
        //         ),
        //         array(
        //             'table' => 'tb_provinces',
        //             'alias' => 'Province',
        //             'type' => 'LEFT',
        //             'conditions' => array(
        //                 'Business.province = Province.province_code'
        //             )
        //         )
        //     ),

        //     'conditions' => $conditionFilter,
        //     'fields' => array('Deal.*', 'Business.*', 'Province.*'),
        //     'order' => $order,
        //     'limit' => NULL
        // ));
        // $this->set('total_getaways_deals', $total_getaways_deals);

    }
}