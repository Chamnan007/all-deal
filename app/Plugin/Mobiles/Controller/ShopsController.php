<?php


/********************************************************************************

File Name: Shops Controller
Description: For Buyer Sign In, Sign Up, Check Out...

Powered By: ABi Investment Group Co., Ltd,

Changed History:

    Date                		Author              Description
    01/May/2015          		Phea Rattana      	Initialize Version

*********************************************************************************/

App::uses('AdministratorAppController', 'Administrator.Controller');
App::uses('Province', 'Administrator.Model');
App::uses('City', 'Administrator.Model');
App::uses('Location', 'Administrator.Model');

class ShopsController extends AppController {

/**
 * Components
 *
 * @var array
*/
	//public $components = array('Email');

	public $uses = array(	'Buyer', 
							'BuyerTransaction', 
							"BuyerTransactionDetail", 
							'TmpTransaction', 
							"TmpTransactionDetail", 
							'MailConfigure', 
							'Commission', 
							'Province', 
							'Deal', 
							'DealItemLink',
							'Business' ,
							'BusinessMenuItem' ,
							'SystemRevenue',
							'BusinessRevenue',
							'ReferalToken',
							'BuyerPayment'
						);

	var $helpers = array('Html', 'Form');

	var $context 			= 'Buyer';
	var $business_table 	= 'tb_businesses';

	var $credit_card_info = NULL;

/**
 * index method
 *
 * @return void
 */

	var $biz_commission_id 	= 1; // Comission when buyer purchased deal
	var $singup_bonus_id 	= 2; // Sing Up Bonus
	var $referer_bonus		= 3;
	var $referee_bonus		= 4; 

	var $type_in  = 1;
	var $type_out = 0;

	var $senderEmail = "" ;
	var $payment_id = NULL;


	public function beforeFilter(){

		parent::beforeFilter();

		$this->senderEmail = $this->admin_email;

		$this->set('status_arr', $this->buy_transaction_status);
		$this->set('payment_method', $this->payment_method );
		$this->set('__CREDIT_PAYMENT_TYPE', $this->__CREDIT_PAYMENT);
	}

	public function index(){

		if( !$this->Session->read('Buyer') && !$this->Session->read('Buyer.__SES_LOGGED_INFO') ){
			return $this->redirect('/users/signin');
		}

		$userInfo = $this->Session->read('Buyer.__SES_LOGGED_INFO');		
		$params = $this->request->query;
		$ref = "";
		if( isset($params['ref']) && $params['ref'] != "" ){
			$ref = $params['ref'];

		}else{
			if($this->Session->read('Buyer.__SES_TMP_TOKEN') ){
				$ref = $this->Session->read('Buyer.__SES_TMP_TOKEN');
			}
		}

		$info = $this->Session->read('Buyer.__SES_LOGGED_INFO');

		$user_id = $info['Buyer']['id'];
		$userInfo = $this->Buyer->find('first', array(
                    'joins' => array(
                        array(
                            'table' => 'tb_cities',
                            'alias' => 'Cities',
                            'type' => 'LEFT',
                            'conditions' => array(
                                'Buyer.city_code = Cities.city_code'
                            )
                        )
                    ),
                    'conditions' => array(
                        'Buyer.id' => $user_id
                    ),
                    'fields' => array('Buyer.*', 'Cities.city_name', 'SangkatInfo.*')
                ));
		
		$this->set('userInfo', $userInfo);

		if( $ref ){

			// Check if ref is exist in TMP table
			$conds = array();
			$conds['conditions'] = array( 'TmpTransaction.token' => $ref, 'TmpTransaction.buyer_id' => $userInfo['Buyer']['id'] );

			$this->TmpTransaction->recursive = 2;
			$data = $this->TmpTransaction->find('first', $conds);
			$item = array();

			if( $data ){
				$this->Session->write('Buyer.__SES_TMP_TOKEN', $ref);
				$item = $data['TransactionDetail'];
			}

			$this->set('items', $item);
			$this->set('token', $ref);

			$detail 	= $this->Buyer->findById($userInfo['Buyer']['id']);
			$this->set('balance', $detail['Buyer']['amount_balance']);

		}else{
			$this->set('items', array());
		}

		// Get List of Province
		// $provinceData = $this->Province->find('all', array('conditions' => array('Province.status' => 1) ));
		// $this->set('provices', $provinceData);

	}

	public function hashPassword( $password = NULL ){
		
		$passwordHasher = new SimplePasswordHasher(array('hashType'=>'sha256'));
	  	$hashedPassword = $passwordHasher->hash($password);

		$data['password'] = $hashedPassword;

		echo json_encode($data); exit;

	}
	
	public function signin( $slug = NULL ){

		if( $this->request->is('post') ){

			$data = $this->request->data;

			$email 		= $data['Buyer']['email'];
			$password  	= $data['Buyer']['password'];

			$passwordHasher = new SimplePasswordHasher(array('hashType'=>'sha256'));

	  		$hashedPassword = $passwordHasher->hash($password);

	     	$password = $hashedPassword;

	     	$conds = array( 'conditions' => array('Buyer.status' => 1, 'Buyer.email' => $email, 'Buyer.password' => $password));

	     	$this->Buyer->recursive = 2;
	     	$getInfo = $this->Buyer->find('first', $conds);

	     	if( $getInfo && !empty($getInfo) ){

	     		// Logged in (success)ful, and then save it to session
	     		$this->Session->write('Buyer.__SES_LOGGED_INFO', $getInfo );

	     		// Save Log
	     		$action = " SIGN IN ";
		            
	            $logMessage = json_encode($getInfo);
	            parent::generateLog( $logMessage, $action, $getInfo['Buyer']['id'] );

	            if( $slug ){
					return $this->redirect('/deal/' . $slug );	            	
	            }

	            return $this->redirect('/');

	     	}else{
	     		$this->Session->setFlash(__( 'Sign In Failed. Invalid email adress or password !' )
                                            .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                                            'default',
                                            array('class'=>'alert alert-dismissable alert-danger align-center '));
	     		if( $slug ){
					return $this->redirect('/users/signin?ref=' . $slug );	            	
	            }
	     	}
	     	
		}

	}

	public function signout(){

		$action = " SIGN OUT ";
		$getInfo = $this->Session->read('Buyer.__SES_LOGGED_INFO');

		$getInfo = $getInfo['Buyer'];      
        $logMessage = json_encode($getInfo);
        parent::generateLog( $logMessage, $action, $getInfo['id'] );

		$this->Session->delete('Buyer');

        return $this->redirect('/');

	}

	public function signup( $verified = "" ){

		$this->set('verified', $verified);
		
		if( !$this->Session->check('Message.flash') ){
			$this->set('verified', "");
		}

		if( isset($_GET['ref']) && $_GET['ref'] != "" ){

			$token = $_GET['ref'];
			$conditions['conditions'] = array('ReferalToken.token' => $token);

			if($referalToken = $this->ReferalToken->find('first', $conditions)){
				$this->set('referal', $referalToken['ReferalToken']);	
			}

		}

		if( $this->request->is('post')){

			$data = $this->request->data;

			$msg_error = array();

			$name 	= $data['Buyer']['full_name'];

			$dob_d 	= $data['Buyer']['dob']['day'];
			$dob_m 	= $data['Buyer']['dob']['month'];
			$dob_y 	= $data['Buyer']['dob']['year'];

			$province = $data['Buyer']['province_code'];
			$city 	  = $data['Buyer']['city_code'];
			$location = $data['Buyer']['location'];

			$phone1   = $data['Buyer']['phone1'];
			$email 	  = $data['Buyer']['email'];
			$password = $data['Buyer']['password'];
			$cnf_password = $data['Buyer']['confirm_password'];

			if( $name == "" ){
				array_push($msg_error, 'Please enter your name !');
			}

			if( $dob_d == "" || $dob_m == "" || $dob_y == "" ){
				array_push($msg_error, "Invalid date of birth !");
			}

			if( $city == "" ){
				array_push($msg_error, "Please select province !");
			}

			if( $location == "" ){
				array_push($msg_error, "Please select location !");
			}

			if( $phone1 == "" ){
				array_push($msg_error, "Please enter your phone number !");
			}

			if( $email == "" ){
				array_push($msg_error, "Please enter your email address !");
			}else{
				if (!filter_var($email , FILTER_VALIDATE_EMAIL)) {
                    array_push($msg_error, "Invalid Email Address !");
                }else{
                	$checkEmail = $this->Buyer->findByEmail($email);
                	if( !empty($checkEmail) ){
                		array_push($msg_error, "Duplicate Email Address ! Please use another email address.");
                	}
                }
			}

			if( $password == "" || strlen($password) < 6 ){
				array_push($msg_error, "Please enter password and must be at least 6 characters !");

				if( $cnf_password != $password ){
					array_push($msg_error, "Password and Confirm Password does not match !" );
				}

			}

			unset($this->request->data['Buyer']['confirm_password']);
			unset($data['Buyer']['confirm_password']);

			if( count($msg_error) > 0 ){
				$msg = "";

				foreach( $msg_error as $k => $val ){
					$msg .= $val . "<br>";
				}

				$this->Session->setFlash(__( $msg )
                                            .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                                            'default',
                                            array('class'=>'alert alert-dismissable alert-danger align-center '));
                
                $this->set('save', false );
                $this->set('data', $this->request->data );

			}else{

				unset($data['agreed']);
				// save
				$dob = $dob_y . "-" . $dob_m . "-" . $dob_d;
				$data['Buyer']['dob'] = date('Y-m-d', strtotime($dob) );

				$token_str = $email . $password ;
		        $token = parent::generateRegisterToken($token_str);

		        $data['Buyer']['status'] 	= 0;
		        $data['Buyer']['token'] 	= $token;

				if( $this->Buyer->save($data) ){

					$id = $this->Buyer->getLastInsertId();

					// generate code
					$code = "U" . str_pad($id, 6, 0 , STR_PAD_LEFT);
					$updateData = array();
					$updateData['Buyer']['id'] = $id;
					$updateData['Buyer']['buyer_code'] = $code;
					$this->Buyer->save($updateData);


	    			// Send Verification Mail
                    $receiver   = $email;
                    $subject    = "Sign Up Verification";
                    $content    = "";
                    $url        = Router::url("signUpVerification/" . $token, true );

                    $content    .= "<html><body>";
                    $content    .= "<p>Dear " . $name. ", </p>";
                    $content    .= "<p>Click on link below to verify your sign up:</p>";
                    $content    .= "<a href='" . $url . "'>" . $url . "</a><br>";
                    $content    .= "<p>Thanks for helping us maintain the security of your account.</p><br>";
                    $content    .= "</body></html>";

                    $sendMail =  $this->sendMail($receiver, $subject, $content);

                    if($sendMail){

                    	$this->set('save', true );
                        $this->Session->setFlash(__(    '<p>Sign Up Successful.</p>'.
                                                        '<p>Verification link has been sent to ' . $email . '</p>'.
                                                        '<p>Please go to email and click on verification link to activate your account.</p>')
                                                        .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                                                        'default',
                                                        array('class'=>'alert alert-dismissable alert-success align-center '));

                        $action = " SIGN UP ";
		            
			            $logMessage = json_encode($data);
			            parent::generateLog( $logMessage, $action, $id );

                    }else{

                    	// Delete data when not sending email
                    	$this->Buyer->delete($id);
                    	$this->Session->setFlash(__(    '<p>Sign Up Failed. Please try again !</p>')
                                                        .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                                                        'default',
                                                        array('class'=>'alert alert-dismissable alert-warning align-center '));
                    }

				}else{
					$this->Session->setFlash(__( "Account Sign Up Failed. Please try again !" )
                                            .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                                            'default',
                                            array('class'=>'alert alert-dismissable alert-danger align-center '));
				}

			}
			
		
			return $this->redirect('/users/signup');

		}

	}


	public function signUpVerification( $token = NULL ){

		if( $token ){
			$info = $this->Buyer->findByToken($token);

			$expired_date = date('Y-m-d');
			$expired_date = strtotime($expired_date);
		   	$expired_date = strtotime("+" . $this->__EXPIRATION_PERIOD . " day", $expired_date);
		   	$expired_date = date("d-F-Y", $expired_date);
			
			if( $info ){

				$id = $info['Buyer']['id'];	

				if( $info['Buyer']['referer_id'] != 0 ){
					$referer_id = $info['Buyer']['referer_id'];
					$email 		= $info['Buyer']['email'];

					$conditions['conditions'] = array(  'ReferalToken.referer_id' => $referer_id,
														'ReferalToken.email'	  => $email
													 );

					$checkReferralToken = $this->ReferalToken->find('first', $conditions );

					if( $checkReferralToken ){
						$expired_date = $checkReferralToken['ReferalToken']['expired_date'];
					}

				}

				$data['Buyer']['id'] 					= $id;
				$data['Buyer']['status'] 				= 1;
				$data['Buyer']['token'] 				= NULL;
				$data['Buyer']['activated_date'] 		= date('Y-m-d H:i:s');
				$data['Buyer']['amount_balance'] 		= 0 ;
				$data['Buyer']['bonus_expiration_date'] = date("Y-m-d", strtotime($expired_date)) ;

				$update = $this->Buyer->save($data);
				
				if( $update ){

					// Send Email
	    			// Send Verification Mail
                    $receiver   = $info['Buyer']['email'];
                    $subject    = "Sign Up Verification";
                    $content    = "";

                    $content    .= "<html><body>";
                    $content    .= "<p>Dear " . $info['Buyer']['full_name'] . ", </p>";
                    $content    .= "<p>Your account is now activated. Please make your first purchase in All Deal to receive All Deal Credit Bonus.</p>";
                    $content 	.= "<p>All Deal Credit Bonus Expiration Date: " . $expired_date . "</p>";
                    
                    $content    .= "</body></html>";

                    $sendMail =  $this->sendMail($receiver, $subject, $content);

					$this->Session->setFlash(__( '<p>Your account is now activated.</p><p>Make your first purchase now to receive All Deal Credit Bonus.</p>.')
                                                    .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                                                'default',
                                                array('class'=>'alert alert-dismissable alert-success align-center '));

					return $this->redirect('/users/signup/verified');
				}
			}else{
				// echo "Invalide Request !"; exit;
			}
		}

		return $this->redirect('/');

	}

	public function forgotPassword(){

		if( $this->request->is('post') ){
			$data = $this->request->data;

			$email = $data['Buyer']['email'];

			$conds = array('conditions' => array('Buyer.email' => $email) );
			$info = $this->Buyer->find('first', $conds);

			if( $info && !empty($info) ){

				if( $info['Buyer']['status'] == 1){

					$token = $this->randomPassword() . $email ;
					$loop = date('s');

			        for( $i =1; $i <= $loop ; $i++ ){
			          $token = sha1($token);
			        }

			        $activate_link = Router::url("/users/activatereset/"  . $token , true);

			        $data['Buyer']['id'] 	= $info['Buyer']['id'];
			        $data['Buyer']['token'] = $token;

			        $subject = "Password Reset Verification Message";

			        $content = "<h4>Dear " . ucwords($info['Buyer']['full_name']) . ",</h4><p> Please click on verification link below to complete your password reset.</p>";
			        $content .= "<p><a href='" . $activate_link . "'>" . $activate_link . "</a></p><br/>";

			        if( $this->sendMail($email, $subject, $content) ){

			          	if( $this->Buyer->save($data)){
			            	$this->Session->setFlash(__( "Verification email has been sent to your email: " . $email )
			                      .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
			                    'default',
			                    array('class'=>'alert alert-dismissable alert-success align-center'));
			            	return $this->redirect(array('action' =>'signin'));
			          	}

			        }else{

			          	$this->Session->setFlash(__( "Something went wrong. Please try again." )
			                      .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
			                    'default',
			                    array('class'=>'alert alert-dismissable alert-danger '));
			          	return $this->redirect(array('action' =>'forgotpassword'));

			        }
				}else{					
					$this->Session->setFlash(__( "Reset Password Failed. This account is now deactivated." )
	                      .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
	                    'default',
	                    array('class'=>'alert alert-dismissable alert-danger align-center'));

	        		return $this->redirect(array('action' =>'forgotpassword'));					
				}

			}else{
				$this->Session->setFlash(__( "Reset Password Failed. This email does not exist in our system." )
                      .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                    'default',
                    array('class'=>'alert alert-dismissable alert-danger align-center'));

        		return $this->redirect(array('action' =>'forgotpassword'));
			}

		}
	}


	public function activateReset( $token = NULL ){

	    if( !$token ){

	      $this->Session->setFlash(__( "Verification Failed. Try again!" )
	                      .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
	                    'default',
	                    array('class'=>'alert alert-dismissable alert-danger '));

	      return $this->redirect(array('action' => 'signin'));

	    }

	    $info = $this->Buyer->findByToken($token);

	    if( !empty($info) ){

	      $mail = $info['Buyer']['email'];

	      $new_pass = $this->randomPassword();

	      $user_id = $info['Buyer']['id'];
	      $this->Buyer->id = $user_id;

	      $data['Buyer']['id'] = $user_id ;
	      $data['Buyer']['password'] = $new_pass;
	      $data['Buyer']['token'] = "";

	      $Login = Router::url("/users/signin" , true);

	      $subject = "Password Reset";

	      $content = "<h4>Dear " . $info['Buyer']['full_name'] . ", </h4><p>Your password is successfully reset.</p><p>Your new password: $new_pass</p>";
	      $content .= "<p><a href='" . $Login . "'>Goto Login</a></p><br/>";
	      // $content .= "<p>Thanks,</p><p>Admin</p>";

	      if( $this->sendMail($mail, $subject, $content) ){

	        if( $this->Buyer->save($data)){
	            $this->Session->setFlash(__( "Your new password has been sent to your email : " . $mail )
	                    .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
	                  'default',
	                  array('class'=>'alert alert-dismissable alert-success align-center  '));
	          return $this->redirect('/users/signin');
	        }
	      }else{
	        $this->Session->setFlash(__( "Something went wrong. Please try again." )
	                    .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
	                  'default',
	                  array('class'=>'alert alert-dismissable alert-danger '));
	        return $this->redirect('/users/forgot-password');
	      }

	    }else{
	      $this->Session->setFlash(__( "Verification Failed." )
	                      .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
	                    'default',
	                    array('class'=>'alert alert-dismissable alert-danger '));

	      return $this->redirect('/users/signin');
	    }

	}

	public function randomPassword() {

      $alphabet = "0123456789abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ";
      $pass = array(); 
      $alphaLength = strlen($alphabet) - 1; 
      for ($i = 0; $i < 10 ; $i++) {
          $n = rand(0, $alphaLength);
          $pass[] = $alphabet[$n];
      }

      return implode($pass);
  	}

	public function sendMail( $recipients = NULL, $subject = NULL , $content = NULL, $attachment = NULL, $sender= NULL ) {

       return parent::sendMail( $recipients , $subject  , $content , $attachment , $sender );

    }

	public function checkDuplicateEmail( $email = NULL, $id = 0 ){

        if(!empty($email)){
        	$conds = array('conditions' => array('Buyer.email' => $email) );

        	if( $id ){
        		$conds = array('conditions' => array('Buyer.email' => $email, 'Buyer.id !=' => $id ) );
        	}

            $getData = $this->Buyer->find("first", $conds);

            if(!empty($getData)){
                $result = false;
            }else{
                $result = true;
            }
        }else{
            $result = false;
        }

        $data['status'] = $result;
        echo json_encode($data);
        exit();

	}

	public function myAccount( $page = 1 ){

		if( !$this->Session->read('Buyer') && !$this->Session->read('Buyer.__SES_LOGGED_INFO') ){
			return $this->redirect('/users/signin');
		}

		$info 		= $this->Session->read('Buyer.__SES_LOGGED_INFO');
		$user_id 	= $info['Buyer']['id'];

		$detail 	= $this->Buyer->findById($user_id);
		$this->set('balance', $detail['Buyer']['amount_balance']);

		// Get Transaction History
		$limit = 30;
		$offset = ( $page - 1 ) * $limit ;

		$next = false;
		$prev = false;

		if($page > 1 ){
			$prev = true;
		}

		$conditions = array();

		$conditions['BuyerTransaction.buyer_id'] = $user_id ;


		if( isset($this->request->data['code']) ){
			$this->setState('code', $this->request->data['code'] );
		}

		if( isset($this->request->data['daterange']) ){
			$this->setState('daterange', $this->request->data['daterange'] );
		}

		if( isset($this->request->data['status']) ){
			$this->setState('status', $this->request->data['status']);
		}

		if( $code = $this->getState('code', NULL )){
			$conditions['BuyerTransaction.code LIKE'] = "%" . $code . "%" ;
		}

		$date = $this->getState('daterange', NULL );
		if( $date ){
			$date = explode(" - ", $date );
			$from = date("Y-m-d 00:00:00", strtotime($date[0]));
			$to = date("Y-m-d 23:59:59", strtotime($date[1]));

			$conditions['BuyerTransaction.created >='] = $from;
			$conditions['BuyerTransaction.created <='] = $to;
		}

		$status = $this->getState('status', 'all');
		$this->setState('status', $status);

		if( $status != "all" ){
			$conditions['BuyerTransaction.status'] = $status;

			if( $status == 0 ){
				$conditions['BuyerTransaction.type'] = 0;
			}
		}

		$conds['conditions'] = $conditions;

		$conds['order']		 = array('BuyerTransaction.created' => 'DESC', "BuyerTransaction.code" => 'DESC' );
		$conds['limit']		 = $limit + 1;
		$conds['offset']	 = $offset;

		$this->set('states', $this->getState());

		$this->BuyerTransaction->recursive = 2;
		$history = $this->BuyerTransaction->find('all', $conds );
		
		if( count($history) > $limit ) {
			$next = true;
			unset($history[$limit]);
		}

		$this->set('histories', $history);
		$this->set('next', $next);
		$this->set('prev', $prev);
		$this->set('page', $page);

		$this->set('TransactionStatus', $this->buy_transaction_status );


	}

	public function editProfile(){
		if( !$this->Session->read('Buyer') && !$this->Session->read('Buyer.__SES_LOGGED_INFO') ){
			return $this->redirect('/users/signin');
		}

		$info = $this->Session->read('Buyer.__SES_LOGGED_INFO');
		$this->set('user_info', $info['Buyer']);
	}

	public function myProfile(){
		if( !$this->Session->read('Buyer') && !$this->Session->read('Buyer.__SES_LOGGED_INFO') ){
			return $this->redirect('/users/signin');
		}

		$info = $this->Session->read('Buyer.__SES_LOGGED_INFO');

		$user_id = $info['Buyer']['id'];
		$detail = $this->Buyer->findById($user_id);

		$detail = $this->Buyer->find('first', array(
                    'joins' => array(
                        array(
                            'table' => 'tb_provinces',
                            'alias' => 'Provinces',
                            'type' => 'LEFT',
                            'conditions' => array(
                                'Buyer.province_code = Provinces.province_code'
                            )
                        ),
                        array(
                            'table' => 'tb_cities',
                            'alias' => 'Cities',
                            'type' => 'LEFT',
                            'conditions' => array(
                                'Buyer.city_code = Cities.city_code'
                            )
                        )
                    ),
                    'conditions' => array(
                        'Buyer.id' => $user_id
                    ),
                    'fields' => array('Buyer.*', 'Provinces.province_name', 'Cities.city_name', 'SangkatInfo.*')
                ));
		
		$this->set('detail', $detail);

	}

	public function saveProfile(){

		if( !$this->Session->read('Buyer') && !$this->Session->read('Buyer.__SES_LOGGED_INFO') ){
			return $this->redirect('/users/signin');
		}

		$info = $this->Session->read('Buyer.__SES_LOGGED_INFO');

		if( $this->request->is('post') ){
			$data = $this->request->data;

			$msg_error = array();

			$name 	= $data['Buyer']['full_name'];

			$dob_d 	= $data['Buyer']['dob']['day'];
			$dob_m 	= $data['Buyer']['dob']['month'];
			$dob_y 	= $data['Buyer']['dob']['year'];

			$province = $data['Buyer']['province_code'];
			$city 	  = $data['Buyer']['city_code'];
			$location = $data['Buyer']['location'];

			$phone1   = $data['Buyer']['phone1'];
			$email 	  = $data['Buyer']['email'];
			$password = $data['Buyer']['password'];
			$cnf_password = $data['Buyer']['confirm_password'];
			$current_password = $data['Buyer']['current_password'];

			if( $name == "" ){
				array_push($msg_error, 'Please enter your name !');
			}

			if( $dob_d == "" || $dob_m == "" || $dob_y == "" ){
				array_push($msg_error, "Invalid date of birth !");
			}

			if( $city == "" ){
				array_push($msg_error, "Please select province !");
			}

			if( $location == "" ){
				array_push($msg_error, "Please select location !");
			}

			if( $phone1 == "" ){
				array_push($msg_error, "Please enter your phone number !");
			}

			if( $email == "" ){
				array_push($msg_error, "Please enter your email address !");
			}else{
				if (!filter_var($email , FILTER_VALIDATE_EMAIL)) {
                    array_push($msg_error, "Invalid Email Address !");
                }else{
                	$conds = array('conditions' => array('Buyer.id !=' => $data['Buyer']['id'], 'Buyer.email' => $email) );
                	$checkEmail = $this->Buyer->find('first',$conds);

                	if( !empty($checkEmail) ){
                		array_push($msg_error, "Duplicate Email Address ! Please use another email address.");
                	}
                }
			}

			if( $password != "" ){
				if( strlen($password) < 6 ){
					array_push($msg_error, "Please enter password and must be at least 6 characters !");
				}else{

					if( $cnf_password != $password ){
						array_push($msg_error, "Password and Confirm Password does not match !" );
					}
				}

			}else{

				unset($data['Buyer']['password']);
				unset($this->request->data['Buyer']['password']);
			}

			if( $current_password == "" ){
				array_push($msg_error, "Please enter your current password to save change !");
			}else{
				$old_password = $info['Buyer']['password'];
				$passwordHasher = new SimplePasswordHasher(array('hashType'=>'sha256'));
	  			$current_pass = $passwordHasher->hash($current_password);

	  			if( $current_pass != $old_password ){
	  				array_push($msg_error, "Your current password is not correct !");
	  			}
			}

			unset($this->request->data['Buyer']['confirm_password']);
			unset($data['Buyer']['confirm_password']);

			unset($this->request->data['Buyer']['current_password']);
			unset($data['Buyer']['current_password']);

			if( count($msg_error) > 0 ){
				$msg = "";

				foreach( $msg_error as $k => $val ){
					$msg .= $val . "<br>";
				}

				$this->Session->setFlash(__( $msg )
                                            .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                                            'default',
                                            array('class'=>'alert alert-dismissable alert-danger align-center '));

			}else{
				// save
				$dob = $dob_y . "-" . $dob_m . "-" . $dob_d;
				$data['Buyer']['dob'] = date('Y-m-d', strtotime($dob) );

				if( $this->Buyer->save($data) ){
					$id = $data['Buyer']['id'];

					$conds = array('conditions' => array('Buyer.id' => $id ) );
					$getInfo = $this->Buyer->find('first', $conds);

					$this->Session->write('Buyer.__SES_LOGGED_INFO', $getInfo );

					$action = " EDIT PROFILE ";
		            
		            $logMessage = json_encode($data);
		            parent::generateLog( $logMessage, $action, $data['Buyer']['id'] );

		            $this->Session->setFlash(__( "Your profile has been changed !" )
                                            .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                                            'default',
                                            array('class'=>'alert alert-dismissable alert-success align-center '));

				}

			}
		}

		return $this->redirect('/myprofile');
	
	}

	public function postAddToCart(){
		
		if( !$this->Session->read('Buyer') && !$this->Session->read('Buyer.__SES_LOGGED_INFO') ){
			// Set Messages
			$this->Session->setFlash(__( 'Session has expired! Please sign in again.' )
	                    .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
	                    'default',
	                    array('class'=>'alert alert-dismissable alert-danger align-center') );

			return $this->redirect('/users/signin');
		}

		$info = $this->Session->read('Buyer.__SES_LOGGED_INFO');

		$user_id 	= $info['Buyer']['id'];
		$email 		= $info['Buyer']['email'];

		if( $this->request->is('post') ){

			$data = $this->request->data['item'];

			$business_ids 	= $data['business_id'];
			$deal_ids 		= $data['deal_id'];
			$item_ids		= $data['item_id'];
			$item_qtys 		= $data['qty'];
			$unit_prices 	= $data['price'];

			$saveData 	= array();
			$old_bis_id = 0;

			$qty_issue 	= false;
			$issue_data = array();

			// Check if chosen deals are still available
			$now = date('Y-m-d H:i:s');
			$dealConds = array();
			$dealConds['conditions'] = array( 
										'Deal.isdeleted' 		=> 0 ,
										'Deal.status'			=> 1,
										'Deal.valid_from <=' 	=> $now,
										'Deal.valid_to >'		=> $now,
										'Deal.id'				=> $deal_ids,
										'BusinessInfo.status'	=> 1
									);

			$deals = $this->Deal->find('count', $dealConds);

			$count_deal_ids = array_unique($deal_ids);
			// If deals == 0, no active deals, Some deal is expired
			if( $deals < count($count_deal_ids) ){

				$this->Session->setFlash(__( 'Items is no longer available!')
	                                    .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
	                                    'default',
	                                    array('class'=>'alert alert-dismissable alert-danger align-center') );

				return $this->redirect( array('action' => 'index') );

			}

			foreach( $deal_ids as $key => $deal_id ){

				$tmp = array();

				$bis_id 	= $business_ids[$key];
				$item_id 	= $item_ids[$key];
				$price 	 	= $unit_prices[$key];
				$qty 	 	= $item_qtys[$key];

				if( $qty > 0 ){

					$checkQty 	= $this->checkAvailableQTY($deal_id, $item_id, $qty);

					if( $checkQty ) {
						$qty_issue = true;
						$issue_data[$key]['available_qty'] 	= $checkQty['available_qty'];
						$issue_data[$key]['item_name'] 		= $checkQty['item_name'];
					}

					$amount  = $qty * $price;

					$tmp['deal_id'] 	= $deal_id;
					$tmp['item_id']		= $item_id;
					$tmp['unit_price'] 	= $price;
					$tmp['qty']			= $qty;
					$tmp['amount']		= $amount;

					$saveData[$deal_id]['detail'][] 		= $tmp;
					$saveData[$deal_id]['type'] 			= 0 ; // 0 = Expense
					$saveData[$deal_id]['description'] 	    = "" ;
					$saveData[$deal_id]['business_id']		= $bis_id;
					$saveData[$deal_id]['deal_id']			= $deal_id;
					$saveData[$deal_id]['amount'] 			= @$saveData[$deal_id]['amount'] + $amount;	
					
				}				

			}

			if( $qty_issue == true ){

				$st = "";
				foreach( $issue_data as $ind => $msg ){
					$st .= $msg['item_name'] . " available only " . $msg['available_qty'] . " unit(s) maximum.<br/>";
				}

				$this->Session->setFlash(__( 'Your cannot make purchase right now.<br/>' . $st )
	                                    .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
	                                    'default',
	                                    array('class'=>'alert alert-dismissable alert-danger align-center') );

				return $this->redirect( array('action' => 'index') );
			}

			if( $saveData ){
				//======= Set Bonus, if available
				// Get Bonus Commission
				$conds 	= array(	'conditions' => array('status' => 1, 'type' => $this->biz_commission_id),
											'fields' => array('commission')
									);

				$bonus_percent = $this->Commission->find('first', $conds);

				$percent = 0;
				if($bonus_percent){
					$percent = $bonus_percent['Commission']['commission'];
				}

				$TransactionCodes = array();

				// Check if exist, then remove
				$exist_tmp = $this->TmpTransaction->find('all', 
																array(
																	'conditions' => array( 'TmpTransaction.buyer_id' => $user_id )
					 										) );
				if( $exist_tmp ){
					$tmp_ids = array();
					foreach( $exist_tmp as $k => $val ){
						$tmp_ids[] = $val['TmpTransaction']['id'];
					}

					// Remove
					$this->TmpTransaction->deleteAll(array('TmpTransaction.id' => $tmp_ids));
					$this->TmpTransactionDetail->deleteAll(array('TmpTransactionDetail.transaction_id' => $tmp_ids));
				}

				foreach( $saveData as $key => $val ){
					// Get Deal Title
					$dealConds = array('conditions' => array('Deal.id' => $key) );
					$this->Deal->recursive = -1;
					$dealData = $this->Deal->find('first', $dealConds);

					$desc = $dealData['Deal']['title'];

					$tmp = array();

					$str = $email . "-" . $user_id;
					$token = parent::generateRegisterToken($str);

					$tmp['TmpTransaction']['token']					= $token;
					$tmp['TmpTransaction']['business_id'] 			= $val['business_id'] ;
					$tmp['TmpTransaction']['deal_id']				= $val['deal_id'];
					$tmp['TmpTransaction']['buyer_id']				= $user_id;
					$tmp['TmpTransaction']['type']					= $val['type'];
					$tmp['TmpTransaction']['description']  			= $desc ;
					$tmp['TmpTransaction']['amount']				= $val['amount'];
					$tmp['TmpTransaction']['commission_percent']	= $percent;

					$this->TmpTransaction->create();

					if( $this->TmpTransaction->save($tmp)){
						$tran_id = $this->TmpTransaction->getLastInsertId();

						$itemDetails = $val['detail'];
						// Save Transaction Detail
						$detailData = array();

						foreach( $itemDetails as $k => $item ){
							$temp = array();
							$temp['TmpTransactionDetail']['transaction_id'] = $tran_id;
							$temp['TmpTransactionDetail']['deal_id']		= $item['deal_id'];
							$temp['TmpTransactionDetail']['item_id']		= $item['item_id'];
							$temp['TmpTransactionDetail']['qty'] 			= $item['qty'];
							$temp['TmpTransactionDetail']['unit_price']		= $item['unit_price'];
							$temp['TmpTransactionDetail']['amount']			= $item['amount'];

							$detailData[] = $temp;
							$itemIDs[] = $item['item_id'];
						}

						$this->TmpTransactionDetail->saveAll($detailData);

						return $this->redirect("/cart?ref=" . $token);
					}

				}

			}

		}

		return $this->redirect("/");

	}

	public function addToCart(){

		if( !$this->Session->read('Buyer') && !$this->Session->read('Buyer.__SES_LOGGED_INFO') ){
			
			$result['status'] = false;

		}else{

			$info = $this->Session->read('Buyer.__SES_LOGGED_INFO');

			$bis_id 	= $this->request->data('business_id');
			$deal_id 	= $this->request->data('deal_id');
			$item_id 	= $this->request->data('item_id');
			$price 		= $this->request->data('price');

			$cartData = array();
			$post_data = array();
			$post_data['business_id'] 	= $bis_id;
			$post_data['deal_id']		= $deal_id;
			$post_data['item_id']		= $item_id;
			$post_data['price']			= $price;

			if( $this->Session->read('Buyer.__SES_CART_DATA') ){
				$cartData = $post_data;
				$session_data = $this->Session->read('Buyer.__SES_CART_DATA');
				array_push($session_data, $cartData);
				$cartData = $session_data;
			}else{
				$cartData[] = $post_data;
			}

			$this->Session->write('Buyer.__SES_CART_DATA', $cartData);

			$result['status'] = true;
		}		

		echo json_encode($result); exit;

	}

	public function clearCart(){

		if( $this->request->is('post') ){
			if( !$this->Session->read('Buyer') && !$this->Session->read('Buyer.__SES_LOGGED_INFO') ){
				return $this->redirect( array('action' => 'index') );
			}else{
				$userInfo = $this->Session->read('Buyer.__SES_LOGGED_INFO');

				$tmp_data = $this->TmpTransaction->find('all', array('conditions' => array('TmpTransaction.buyer_id' => $userInfo['Buyer']['id'])) );
				$tmp_ids = array();

				if( $tmp_data ){
					foreach( $tmp_data as $k => $val ){
						$tmp_ids[] = $val['TmpTransaction']['id'];
					}
				}

				$this->TmpTransaction->deleteAll(array('TmpTransaction.id' => $tmp_ids));
				$this->TmpTransactionDetail->deleteAll(array('TmpTransactionDetail.transaction_id' => $tmp_ids));
			}
		}

		return $this->redirect( array('action' => 'index') );

	}

	public function removeFromCart(){

		if( $this->request->is('post') ){
			$data = $this->request->data;

			$deal_id = $data['deal-id'];
			$item_id = $data['item-id'];

			$data = $this->Session->read('Buyer.__SES_CART_DATA');

			if( $data ){
				foreach( $data as $k => $val ){
					if( $val['deal_id'] == $deal_id && $val['item_id'] == $item_id ){
						unset($data[$k]);
					}
				}
			}

			$this->Session->write('Buyer.__SES_CART_DATA', $data);

		}

		return $this->redirect( array('action' => 'index') );
	}

	public function checkOut( $type = 1 ){

		// type = 1 for PayPal
		// type = 2 for All Deal Credit
		// type = 5 for In-store payment

		$percent = 0;
		
		if( $info = $this->Session->read('Buyer.__SES_LOGGED_INFO') ){

			$user_id 	= $info['Buyer']['id'];
			$user_email = $info['Buyer']['email'];

			if( $this->request->is('post') ){

				$data = $this->request->data;
				$token = $data['token'];

				$saveData 	= array();
				$old_bis_id = 0;

				$qty_issue 	= false;
				$issue_data = array();
				// Get TMP Data
				$conds = array();
				$conds['conditions'] = array( 'TmpTransaction.token' => $token, 'TmpTransaction.buyer_id' => $user_id );

				$this->TmpTransaction->recursive = 2;
				$dataInfo = $this->TmpTransaction->find('first', $conds);

				if( $dataInfo ){

					$deal_ids = $dataInfo['TmpTransaction']['deal_id'];

					// Check if chosen deals are still available
					$now = date('Y-m-d H:i:s');
					$dealConds = array();
					$dealConds['conditions'] = array( 
										'Deal.isdeleted' 		=> 0 ,
										'Deal.status'			=> 1,
										'Deal.valid_from <=' 	=> $now,
										'Deal.valid_to >'		=> $now,
										'Deal.id'				=> $deal_ids
									);

					$deals = $this->Deal->find('all', $dealConds);

					// If deals == 0, no active deals, Some deal is expired
					if( !$deals ){

						$this->Session->setFlash(__( 'The deal is no longer available !')
			                                    .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
			                                    'default',
			                                    array('class'=>'alert alert-dismissable alert-danger align-center') );

						return $this->redirect( array('action' => 'index') );
					}

					$tranDetail = $dataInfo['TransactionDetail'];
					$business_id = $dataInfo['TmpTransaction']['business_id'];

					$total_amount = 0;
					$description = array();
					$tmp_tran_id = $dataInfo['TmpTransaction']['id'];

					foreach( $tranDetail as $key => $val ){

						$description[] = $val['ItemDetail']['title'];

						$tmp = array();

						$deal_id 	= $val['deal_id'];
						$item_id 	= $val['item_id'];
						$price 	 	= $val['unit_price'];
						$qty 	 	= $val['qty'];

						$checkQty 	= $this->checkAvailableQTY($deal_id, $item_id, $qty);

						if( $checkQty ) {
							$qty_issue = true;
							$issue_data[$key]['available_qty'] 	= $checkQty['available_qty'];
							$issue_data[$key]['item_name'] 		= $checkQty['item_name'];
						}

						$amount  = $qty * $price;
						$total_amount += $amount;

						$tmp['deal_id'] 	= $deal_id;
						$tmp['item_id']		= $item_id;
						$tmp['unit_price'] 	= $price;
						$tmp['qty']			= $qty;
						$tmp['amount']		= $amount;

						$saveData[$deal_id]['detail'][] 		= $tmp;
						$saveData[$deal_id]['type'] 			= 0 ; // 0 = Expense
						$saveData[$deal_id]['payment_type'] 	= $type ;
						$saveData[$deal_id]['description'] 	    = "" ;
						$saveData[$deal_id]['business_id']		= $business_id;
						$saveData[$deal_id]['deal_id']			= $deal_id;
						$saveData[$deal_id]['amount'] 			= @$saveData[$deal_id]['amount'] + $amount;					

					}

					if( $qty_issue == true ){

						$st = "";
						foreach( $issue_data as $ind => $msg ){
							$st .= $msg['item_name'] . " available only " . $msg['available_qty'] . " unit(s) maximum.<br/>";
						}

						$this->Session->setFlash(__( 'Your cannot make purchase right now.<br/>' . $st )
			                                    .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
			                                    'default',
			                                    array('class'=>'alert alert-dismissable alert-danger align-center') );

						return $this->redirect( array('action' => 'index'));

					}
				}

				if( $saveData ){

					$save = true;

					if( $type == 2 ){
						// 2: All Deal Credit
						$save = $this->paymentWithAllDealCredit($total_amount);
					}

					if( $save ){

						//======= Set Bonus, if available
						// Get Bonus Commission
						$conds 	= array(	'conditions' => array('status' => 1, 'type' => $this->biz_commission_id),
													'fields' => array('commission')
											);

						$bonus_percent = $this->Commission->find('first', $conds);
						$percent = 0;

						if($bonus_percent){
							$percent = $bonus_percent['Commission']['commission'];
						}

						if( $info['Buyer']['bonus_received'] == 0 && $info['Buyer']['bonus_expiration_date'] >= date('Y-m-d')  ){

							// Get Bonus Commission
							if( $info['Buyer']['referer_id'] != 0 ){

								$email 		= $user_email;
								$referer_id = $info['Buyer']['referer_id'];

								// Get Bonus Amount
								$bonusAmount = $this->ReferalToken->find('first', array( 'conditions' => 
																							array('ReferalToken.referer_id' => $referer_id,
																								  'ReferalToken.email'		=> $email
																								) 
																						)
																		);
								$referer_bonus = 0;
								$referee_bonus = 0;

								if( $bonusAmount ){
									$referee_bonus = $bonusAmount['ReferalToken']['referee_bonus'];
									$referer_bonus = $bonusAmount['ReferalToken']['referer_bonus'];
								}

								$dataReferee = array();

								// Update Plus referee bonus
								$refereeInfo = $this->Buyer->findById($user_id);
								if( $refereeInfo ){
									$dataReferee['Buyer']['amount_balance'] = $refereeInfo['Buyer']['amount_balance'] + $referee_bonus ;
								}else{
									$dataReferee['Buyer']['amount_balance'] = $referee_bonus ;
								}

								$dataReferee['Buyer']['id'] = $user_id;
								$dataReferee['Buyer']['bonus_received'] = 1;

								$this->Buyer->save($dataReferee);

								// Save bonus to transactions
								$tranBonus = array();
								$tranBonus['BuyerTransaction']['code'] 			= "";
								$tranBonus['BuyerTransaction']['buyer_id'] 		= $user_id;
								$tranBonus['BuyerTransaction']['type'] 			= $this->type_in ;
								$tranBonus['BuyerTransaction']['payment_type'] 	= 0 ;
								$tranBonus['BuyerTransaction']['description']	= 'Sign Up Bonus';
								$tranBonus['BuyerTransaction']['amount']		= $referee_bonus ;
								$tranBonus['BuyerTransaction']['created']		= date("Y-m-d H:i:s");
								$tranBonus['BuyerTransaction']['modified']		= date("Y-m-d H:i:s");
								
								$this->BuyerTransaction->create();
								$save = $this->BuyerTransaction->save($tranBonus);

								$dataReferer = array();
								$refererInfo = $this->Buyer->findById($referer_id);
								if( $refererInfo ){
									$dataReferer['Buyer']['amount_balance'] = $refererInfo['Buyer']['amount_balance'] + $referer_bonus ;
								}else{
									$dataReferer['Buyer']['amount_balance'] = $referer_bonus;
								}

								$dataReferer['Buyer']['id'] = $info['Buyer']['referer_id'];

								$this->Buyer->save($dataReferer);

								// Save bonus to transactions
								$referBonus = array();
								$referBonus['BuyerTransaction']['code'] 		= "";
								$referBonus['BuyerTransaction']['buyer_id'] 	= $info['Buyer']['referer_id'];
								$referBonus['BuyerTransaction']['type'] 		= $this->type_in ;
								$referBonus['BuyerTransaction']['payment_type'] = 0 ;
								$referBonus['BuyerTransaction']['description']	= 'Referral Bonus';
								$referBonus['BuyerTransaction']['amount']		= $referer_bonus;
								$referBonus['BuyerTransaction']['created']		= date("Y-m-d H:i:s");
								$referBonus['BuyerTransaction']['modified']		= date("Y-m-d H:i:s");

								$this->BuyerTransaction->create();
								$save1 = $this->BuyerTransaction->save($referBonus);

								// Re-write Session
								$conds = array('conditions' => array('Buyer.id' => $user_id ) );
								$getInfo = $this->Buyer->find('first', $conds);
								$this->Session->write('Buyer.__SES_LOGGED_INFO', $getInfo );						

							}else{

								$conds 	= array( 'conditions' => array('status' => 1, 'type' => $this->singup_bonus_id),
												 'fields' => array('commission')
												);

								$bonus = $this->Commission->find('first', $conds);

								if( $bonus && !empty($bonus) ){

									$dataBuyer = array();
									// Update Buyer

									$dataUser = $this->Buyer->findById($user_id);

									$dataBuyer['Buyer']['id'] = $user_id;
									$dataBuyer['Buyer']['amount_balance'] = $dataUser['Buyer']['amount_balance'] + $bonus['Commission']['commission'];
									$dataBuyer['Buyer']['bonus_received'] = 1;


									$this->Buyer->save($dataBuyer);

									// Save bonus to transactions
									$transactionData = array();
									$transactionData['BuyerTransaction']['code'] 		= "";
									$transactionData['BuyerTransaction']['buyer_id'] 	= $user_id;
									$transactionData['BuyerTransaction']['type'] 		= $this->type_in ;
									$transactionData['BuyerTransaction']['payment_type'] = 0 ;
									$transactionData['BuyerTransaction']['description']	= 'Sign Up Bonus';
									$transactionData['BuyerTransaction']['amount']		= $bonus['Commission']['commission'];
									$transactionData['BuyerTransaction']['created']		= date("Y-m-d H:i:s");
									$transactionData['BuyerTransaction']['modified']	= date("Y-m-d H:i:s");

									$this->BuyerTransaction->create();
									$this->BuyerTransaction->save($transactionData);

									// Re-write Session
									$conds = array('conditions' => array('Buyer.id' => $user_id ) );
									$getInfo = $this->Buyer->find('first', $conds);
									$this->Session->write('Buyer.__SES_LOGGED_INFO', $getInfo );

								}
							
							}
						}

						//======= End Setting Bonus
						$TransactionCodes = array();
						$attachments 	  = array();

						// Remove TMP DATA
						$this->TmpTransaction->deleteAll(array('TmpTransaction.id' => $tmp_tran_id));
						$this->TmpTransactionDetail->deleteAll(array('TmpTransactionDetail.transaction_id' => $tmp_tran_id));

						foreach( $saveData as $key => $val ){

							// Get Deal Title
							$dealConds = array('conditions' => array('Deal.id' => $key) );
							$this->Deal->recursive = -1;
							$dealData = $this->Deal->find('first', $dealConds);

							$desc = $dealData['Deal']['title'];

							$tranData = array();
							$code = $this->generateTransactionCode();

							$TransactionCodes[$code] = $val['amount'] ;

							$tranData['business_id'] 		= $val['business_id'] ;
							$tranData['deal_id']			= $val['deal_id'];
							$tranData['code'] 				= $code ;
							$tranData['buyer_id']			= $user_id;
							$tranData['type']				= $val['type'];
							$tranData['payment_type']		= $val['payment_type'];
							$tranData['description']  		= $desc ;
							$tranData['amount']				= $val['amount'];
							$tranData['commission_percent']	= $percent;
							$tranData['status']				= 0 ;

							$card_number = NULL;

							if( $type == 1 ){
								$card_number = $creditCard_data['card-number'];
								$card_number = substr($card_number, -4);
							}
							
							$tranData['credit_card_number']	= $card_number ;

							$this->BuyerTransaction->create();

							if( $this->BuyerTransaction->save($tranData) ){

								$tran_id = $this->BuyerTransaction->getLastInsertId();	
								$itemDetails = $val['detail'];

								// Save Transaction Detail
								foreach( $itemDetails as $k => $item ){

									$temp = array();
									$temp['transaction_id'] = $tran_id;
									$temp['deal_id']		= $item['deal_id'];
									$temp['item_id']		= $item['item_id'];
									$temp['qty'] 			= $item['qty'];
									$temp['unit_price']		= $item['unit_price'];
									$temp['amount']			= $item['amount'];

									$this->BuyerTransactionDetail->create();
									$this->BuyerTransactionDetail->save($temp);

									// Update Available Qty
									$updateConds['conditions'] = array(	'DealItemLink.deal_id' => $item['deal_id'],
																		'DealItemLink.item_id' => $item['item_id']
																		);

									$this->DealItemLink->recursive = -1;
									$oldData = $this->DealItemLink->find('first', $updateConds);
									if( $oldData ){
										$updateData = array();
										$this->DealItemLink->id = $oldData['DealItemLink']['id'];

										$new_qty = $oldData['DealItemLink']['available_qty'] - $item['qty'];

										$updateData['DealItemLink']['id'] = $oldData['DealItemLink']['id'];
										$updateData['DealItemLink']['available_qty'] = $new_qty;

										$this->DealItemLink->save($updateData);
									}
								}								

								// Calculate Received Amount
								$amount 	 	= $val['amount'];
								$system_revenue	= $amount * $percent / 100 ;
								$biz_revenue 	= $amount - $system_revenue ;

								// Update Ending Balance of Business
								$update_sql = " UPDATE " . $this->business_table . 
											  " SET ending_balance = ( ending_balance + " . $biz_revenue . ") " .
											  " WHERE id = " . $val['business_id']  ;
								$this->Business->query($update_sql);							

								// Save Revenue to Biz and System
								// Business
								$data_bis_revenue = array();
								$data_bis_revenue['transaction_id'] 	= $tran_id;
								$data_bis_revenue['type'] 				= 1; // 1: Credit
								$data_bis_revenue['payment_id'] 		= 0;
								$data_bis_revenue['business_id'] 		= $val['business_id'];
								$data_bis_revenue['transaction_amount'] = $val['amount'];
								$data_bis_revenue['commission_percent'] = $percent;
								$data_bis_revenue['total_amount']		= $biz_revenue;
								$data_bis_revenue['created'] 			= date('Y-m-d H:i:s');
								$data_bis_revenue['modified'] 			= date('Y-m-d H:i:s');

								$this->BusinessRevenue->create();
								$this->BusinessRevenue->save($data_bis_revenue);

								// System
								$data_sys_revenue = array();
								$data_sys_revenue['transaction_id'] 	= $tran_id;
								$data_sys_revenue['business_id']		= $val['business_id'];
								$data_sys_revenue['transaction_amount'] = $val['amount'];
								$data_sys_revenue['commission_percent'] = $percent;
								$data_sys_revenue['total_amount']		= $system_revenue;
								$data_sys_revenue['created'] 			= date('Y-m-d H:i:s');
								$data_sys_revenue['modified'] 			= date('Y-m-d H:i:s');

								$this->SystemRevenue->create();
								$this->SystemRevenue->save($data_sys_revenue);

								// Log
								$action = " Purchased Deal ";
								$logData['Purchase Deal'] = $tranData;
								$logData['Detail'] 		  = $itemDetails;
					            $logMessage = json_encode($logData);
					            parent::generateLog( $logMessage, $action, $user_id );

								// Generate Purchase Claim
								$name = "Purchase Claim-" . date('d-M-Y') . "-" . $tran_id . ".pdf";
								$file = $this->generatePurchaseClaimPDFAttacment( $name, $tran_id );

								$attachments[] = $file;

							}else{

								// Set Messages
								$this->Session->setFlash(__( 'Something went wrong. Please try again !' )
		                                    .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
		                                    'default',
		                                    array('class'=>'alert alert-dismissable alert-danger align-center') );

								return $this->redirect( array('action' => 'index') );
							}
						}

		    			// Send Verification Mail
	                    $receiver   = $user_email;
	                    $subject    = "Purchased Items With All Deal";
	                    $content    = "";
	                    // $url        = Router::url("signUpVerification/" . $token, true );

	                    $content    .= "<html><body>";
	                    $content    .= "<p>Dear " . ucwords($info['Buyer']['full_name']) . ", </p><br>" ;
	                    $content    .= "<p>You have purchased items with amount: <b>$ ". number_format($total_amount,2) . "</b></p>" ;

	                    $content    .= "<p>Please print attached files to claim your purchased items.</p><br>" ;

	                    $content    .= "<p>Thanks for purchasing items with All Deal.</p><br>";
	                    // $content    .= $signature ;
	                    $content    .= "</body></html>";

	                    $sendMail =  $this->sendMail($receiver, $subject, $content, $attachments);

	                    // Remove file after attachments
	                    foreach( $attachments as $val ){
	                    	unlink($val);
	                    }
						
						// Set Messages
						$this->Session->setFlash(__( 'You have successfully purchased the deal. Purchase Claim has been sent to your email address.' )
		                                    .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
		                                    'default',
		                                    array('class'=>'alert alert-dismissable alert-success align-center') );

						return $this->redirect('/myaccount');

					}else{
						$this->Session->setFlash(__( 'Your credit is not enough to purchase right now !' )
		                                    .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
		                                    'default',
		                                    array('class'=>'alert alert-dismissable alert-danger align-center') );

						return $this->redirect( array('action' => 'index') );
					}

				}else{
					$this->Session->setFlash(__( 'Something went wrong. You cannot make purchase right now. Please try again !' )
		                                    .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
		                                    'default',
		                                    array('class'=>'alert alert-dismissable alert-danger align-center') );

					return $this->redirect( array('action' => 'index') );
				}
			}

		}else{

			return $this->redirect('/users/signin');

		}

		return $this->redirect( array('action' => 'index') );

	}

	public function paymentWithAllDealCredit( $total_amount = 0 ){

		$info 		= $this->Session->read('Buyer.__SES_LOGGED_INFO');
		$buyer_id 	= $info['Buyer']['id'];

		$buyerDetail 		= $this->Buyer->findById($buyer_id);
		$available_amount 	= $buyerDetail['Buyer']['amount_balance'];

		if( $available_amount < $total_amount ){
			return false;
		}

		$amount = $available_amount - $total_amount;
		$data['Buyer']['id'] 			= $buyer_id;
		$data['Buyer']['amount_balance']= $amount;

		if( $this->Buyer->save($data) ){
			return true;
		}else{
			return false;
		}

	}

	public function generateTransactionCode(){

		$alphabet = "0123456789abcdefghijklmnopqrstuwxyz";
	    $coupon_code = array(); 
	    $alphaLength = strlen($alphabet) - 1; 

	    for ($i = 0; $i < 7 ; $i++) {
	        $n = rand(0, $alphaLength);
	        $coupon_code[] = $alphabet[$n];
	    }

	    $coupon_code = implode($coupon_code);
	    $coupon_code = "AD" . $coupon_code;

	    $this->BuyerTransaction->recursive = -1;
	    $conds['conditions'] = array( 'LOWER(BuyerTransaction.code)' => strtolower($coupon_code) ) ;
	    $info = $this->BuyerTransaction->find('first', $conds );

		while ( $info ) {
			$this->generateTransactionCode();
		}

      	return $coupon_code;

	}

	public function refer(){

		if( !$this->Session->read('Buyer') && !$this->Session->read('Buyer.__SES_LOGGED_INFO') ){
			return $this->redirect('/users/signin');
		}

		// Get Referer Bonus
		$conds 	= array('conditions' => array('type' => $this->referer_bonus, 'status' => 1));
		$info 	= $this->Commission->find('first', $conds);
		$referer_bonus 	= 0;

		if( $info ){
			$referer_bonus = $info['Commission']['commission'];
		}

		$this->set('bonus', $referer_bonus);

		// Get Referee Bonus
		$conds 	= array('conditions' => array('type' => $this->referee_bonus, 'status' => 1));
		$info 	= $this->Commission->find('first', $conds);
		$referee_bonus 	= 0;

		if( $info ){
			$referee_bonus = $info['Commission']['commission'];
		}

		if( $this->request->is('post') ){

			// User Info
			$userInfo = $this->Session->read('Buyer.__SES_LOGGED_INFO');
			$user_id = $userInfo['Buyer']['id'];

			$email = $this->request->data['email'];

			$timestamp = time();

			$token_str 	= $email . "-" . $user_id . "-" . $timestamp ;
			$token 		= parent::generateReferalToken( $token_str );

			// Send Verification Mail
            $receiver   = $email;
            $subject    = "All Deal Invitation";
            $content    = "";
            $url        = Router::url("signup/?ref=" . $token , true );

            $senderName = ucwords($userInfo['Buyer']['full_name']);

        	$expired_date = date('Y-m-d');
			$expired_date = strtotime($expired_date);
		   	$expired_date = strtotime("+" . $this->__EXPIRATION_PERIOD . " day", $expired_date);

            $content    .= "<html><body>";
            $content 	.= "<p>" . $senderName . " has invited you to join All Deal.</p>";
            $content    .= "<p>Sing Up and make your first purchase now to receive credit bonus in All Deal.</p>";
            $content    .= "<br><p>Sign up throgh link below :</p>";
            $content    .= "<a href='" . $url . "'>" . $url . "</a><br>";
            $content    .= "<br><p>Bonus Expiration Date : " . date('d-F-Y', $expired_date) . "</p>";
            $content    .= "</body></html>";

            $sendMail =  $this->sendMail($receiver, $subject, $content);

            if($sendMail){

            	// Add Referer Token
            	$tokenData = array();
            	$tokenData['ReferalToken']['referer_id'] 	= $user_id ;
            	$tokenData['ReferalToken']['email'] 		= $email ;
            	$tokenData['ReferalToken']['timestamp']		= $timestamp ;
            	$tokenData['ReferalToken']['referer_bonus']	= $referer_bonus ;
            	$tokenData['ReferalToken']['referee_bonus']	= $referee_bonus ;
            	$tokenData['ReferalToken']['token']			= $token ;
            	$tokenData['ReferalToken']['expired_date']	= date( "Y-m-d", $expired_date ) ;

            	// Check if email is already invited
			   	$conds['conditions'] = array( 'ReferalToken.referer_id' => $user_id, 'ReferalToken.email' => $email );
			   	$checkData = $this->ReferalToken->find('first', $conds );

			   	if( $checkData ){
			   		$tokenData['ReferalToken']['id'] = $checkData['ReferalToken']['id'];
			   	}

			   	$this->ReferalToken->create();
            	$this->ReferalToken->save($tokenData);

                $this->Session->setFlash(__(    '<p>You have successfully sent referal sign up to : ' . $email . '</p>') . 
                								'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                                                'default',
                                                array('class'=>'alert alert-dismissable alert-success align-center '));


            }else{

            	$this->Session->setFlash(__(    '<p>Sending Email Failed !</p>') . 
                								'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                                                'default',
                                                array('class'=>'alert alert-dismissable alert-danger align-center '));
            }
		}

	}


	public function generatePurchaseClaimPDFAttacment( $filename = NULL, $transaction_id = 0 ){

		$info = $this->Session->read('Buyer.__SES_LOGGED_INFO');

		$html 	= "<!DOCTYPE html>
					<html>
					<head>
						<meta charset='UTF-8'>
						<title></title>
						
						<style>
							@page {
						    	margin: 10px;
						     	font-size:13px;
						    }

						    .container{
						    	margin: 0 auto;
						    	width: 350px;
						    	background: #EEE;
						    	min-height:600px;
						    	position: relative;

						    	-moz-box-sizing: border-box;
								-webkit-box-sizing: border-box;
								box-sizing: border-box;
						    }

						    .container .image-wrapper{
						    	position: relative;
						    	width: 100%;
						    	height: 196px;
						    	padding: 10px;
						    }

						    .container .image-wrapper .deal-image{
						    	// position: absolute;
						    	width: 330px;
								height: auto;
						    }

						    .container .image-wrapper .deal-image img{
						    	width: 100%;
						    	height: auto;
						    }

						    .container .image-wrapper .qr-code{
								width: 70px;	
								height: 87px;

						    	-moz-box-sizing: border-box;
								-webkit-box-sizing: border-box;
								box-sizing: border-box;		

								background: #666;
						    	font-size: 9px;
						    	color: #FFF;
						    	text-transform: uppercase;
						    	text-align:center;
						    	float: right;
						    	margin-right: 15px;
						    	margin-top: -212px;
						    }

						    .container .image-wrapper .qr-code img{
						    	background: #666;
						    	padding-bottom: 20px;
						    }

						    .container .image-wrapper .qr-code #code-text{
							    background-color: #666;
							    width: 70px;
							    padding-top: 4px;
							    padding-bottom: 4px;
						    }
						    .container .coupon-title{
								width: 400px;
								height: 100px;
								position: absolute;
								top: 160px;
								z-index: 10;
							}

							.container .coupon-title #title-img{
								width: 355px;
								margin-top: -60px;
							}

							.container .coupon-title #title-img img{
								width: 100%;
							}

						    .container .coupon-title .title{
						    	    color: white;
								    width: 248px;
								    height: 60px;
								    margin-top: -55px;
								    margin-left: 96px;
						    }

						    .container .coupon-title .title span{
						    	height: 60px;
								font-size: 15px;
								vertical-align: middle;
						    }

						    .container .content-wrapper{
						    	position: relative;
						    	width: 330px;
						    	height: auto;
						    	margin: 0px 10px 10px 10px;;
								padding: 0px 10px 10px 10px;

						    	-moz-box-sizing: border-box;
								-webkit-box-sizing: border-box;
								box-sizing: border-box;

								color: #333;

								-webkit-box-shadow: 0px 2px 15px 0px rgba(133,133,133,0.2);
								-moz-box-shadow: 0px 2px 15px 0px rgba(133,133,133,0.2);
								box-shadow: 0px 2px 15px 0px rgba(133,133,133,0.2);

								margin-top: -70px;
						    }

						    .content-wrapper h4{
						    	margin-bottom: 10px;
						    	font-size: 15px;
						    }

						    table.item-list{
						    	width: 100%;
						    }
						    table.item-list tr td{
						    	font-size: 11px !important;
						    }

						    #term-conditions{
								font-size: 11px !important;
							}

							#term-conditions p{
								margin-top:0px;
								margin-bottom: 5px;
							}

						</style>

					</head>
					<body style='font-family: Arial;'>";

		$this->BuyerTransaction->recursive = 2;
		$buyer_id = $info['Buyer']['id'];

		$conds['conditions'] = array('BuyerTransaction.id' => $transaction_id, 'BuyerTransaction.buyer_id' => $buyer_id);

		$getData 	= $this->BuyerTransaction->find('first', $conds);

		$tranDesc 	= rtrim($getData['BuyerTransaction']['description'],", ");

		// Deal Information
		$dealInfo = $getData['DealInfo'];

		$deal_image = Router::url('/', true) . $dealInfo['image'];

		$code 				= strtoupper($getData['BuyerTransaction']['code']) ;
		$error 	 			= "Q";
		$border 			= 1;
		$qr_code 			= "<img src=\"http://chart.googleapis.com/chart?chs=70x70&chld=" . $error . "|" . $border . "&cht=qr&chl=" . $code . "\"/>";

		$qr_background 		= "<img src='" . Router::url('/', true ) . "img/coupon/qr-background.jpg" . "'/>" ;
		$title_background 	= Router::url('/', true ) . "img/coupon/title.png";
		$deal_title 		= $dealInfo['title'];
		$term_condition 	= $dealInfo['deal_condition'];

		// Bisiness Information
		$bizDetail 	= $getData['BusinessDetail'];
		$biz_name 	= $bizDetail['business_name'];
		$bizLogo 	= Router::url('/', true) . $bizDetail['logo'] ;
		$bizPhone 	= $bizDetail['phone1'];
		$bizPhone 	.= ($bizDetail['phone2'])?" / " .$bizDetail['phone2']:"";

		// Get Province Name by code
		$city_data 	= $this->City->findByCityCode($bizDetail['city']);
		$bizAddress = $bizDetail['street'] . ", " . $bizDetail['location'] . ", " .  $city_data['City']['city_name'];

		$allDealLogo = Router::url('/', true) . "img/coupon/deal-logo.png";

		$html 	.= "	<div class='container'>
							<div class='image-wrapper'>

								<div class='deal-image'>
									<img src='" . $deal_image . "' alt=''>
								</div>

								<div class='qr-code' style='padding-top:10px;'>" . 
									$qr_background . 
									"<div style='margin-top:-95px;'>" .
									$qr_code . 
									"</div>
									<div id='code-text'>" . $code . "</div>
								</div>
							</div>

							<div class='coupon-title'>
								<div id='title-img'>
									<img src='" . $title_background . "' />
								</div>
								<div class='title'>
									<span>" . $deal_title . "</span>
								</div>
							</div>

							<div class='content-wrapper'>
								<h4>Purchase includes:</h4>
								<table class='item-list'>";

							$itemDetails = $getData['TransactionDetail'];
				            $total = 0;
				            
				            foreach( $itemDetails as $key => $val ){
				            	$no = $key + 1;
				            	$unit_price = $val['unit_price'];
				            	$qty 		= $val['qty'];

				            	$amount 	= $unit_price * $qty;
				            	$total 		+= $amount;

			 	$html 	.= "<tr>
								<td width='150px;'>- " . $val['ItemDetail']['title'] . "</td>
								<td style='text-align:right'>$ " . number_format($unit_price, 2) . "</td>
								<td width='20px;' style='text-align:center;'>x</td>
								<td>" . $qty . "</td>
								<td style='text-align:right'>$ " . number_format($amount, 2) . "</td>
							</tr>";
						}

				$html 	.= "<tfoot>
								<tr>
									<td colspan='4' style='border-top:1px solid #333; width: 180px;'></td>
									<td style='text-align:right; padding-top:10px; border-top:1px solid #333;'>
										<strong style='font-size:15px;'>$ " . number_format($total, 2) . "</strong>
									</td>
								</tr>
							</tfoot>";

				$html 	.="</table>
							</div>

							<div style='clear:both; padding-left:10px; padding-right:10px;'>
								<table width='330px' style='font-size: 11px;'>
									<tr>
										<td width='70px;'>
											<img style='width: 70px; height:70px;' src='" . $bizLogo . "'/>
										</td>
										<td style='vertical-align:top; padding-left:10px;'>
											<h3 style='margin-top:0px; margin-bottom:0px;'>" . strtoupper($biz_name) . "</h3>
											<table style='width:100%;'>
												<tr>
													<td width='15px;'>
														<img src='" . Router::url('/', true) . "img/coupon/location.png' style='width:15px;'/>
													</td>
													<td>" . $bizAddress . "</td>
												</tr>
												<tr>
													<td>
														<img src='" . Router::url('/', true) . "img/coupon/phone.png' style='width:13px;'/>
													</td>
													<td>" . $bizPhone . "</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>

								<div style='clear:both;'></div>
								<h4 style='margin-bottom:0px;'>Terms and Conditions</h4>
								<div id='term-conditions'>
									" . $term_condition . "
								</div>

								<div style='clear:both;'></div>
								<table style='width:100%;'>
									<tr>
										<td width='40px;'>
											<img src='" . $allDealLogo . "' style='width:40px;'/>
										</td>
										<td style='font-size:9px; vertical-align:middle !important;'>
											Note: Cancellation policy is according to the each store’s policy.<br>
											All Deal doesn’t warrant the products or services listed in this coupon.
										</td>
									</tr>
								</table>
								<div style='clear:both; padding-bottom:5px;'></div>
							</div>

						</div>";

		$html 	.= "</body>
				</html>";

		$path = WWW_ROOT . "tmp" ;

		if( !is_dir($path) ){
			mkdir($path, '0755' );
		}

		$filename = $path . "/" . $filename;

		$mpdf 	= new mPDF('c', 'A4');
		$mpdf->WriteHTML($html);
		$mpdf->Output( $filename, 'F' );

		return $filename;

	}
	
	public function downloadClaim( $transaction_id = 0 ){
		
		$info = $this->Session->read('Buyer.__SES_LOGGED_INFO');

		if( !$info  ){
			return $this->redirect('/users/signin');
		}

		if( !$transaction_id ){	
			return $this->redirect('/myaccount');
		}	

		$html 	= "<!DOCTYPE html>
					<html>
					<head>
						<meta charset='UTF-8'>
						<title></title>
						
						<style>
							@page {
						    	margin: 10px;
						     	font-size:13px;
						    }

						    .container{
						    	margin: 0 auto;
						    	width: 350px;
						    	background: #EEE;
						    	min-height:600px;
						    	position: relative;

						    	-moz-box-sizing: border-box;
								-webkit-box-sizing: border-box;
								box-sizing: border-box;
						    }

						    .container .image-wrapper{
						    	position: relative;
						    	width: 100%;
						    	height: 196px;
						    	padding: 10px;
						    }

						    .container .image-wrapper .deal-image{
						    	// position: absolute;
						    	width: 330px;
								height: auto;
						    }

						    .container .image-wrapper .deal-image img{
						    	width: 100%;
						    	height: auto;
						    }

						    .container .image-wrapper .qr-code{
								width: 70px;	
								height: 87px;

						    	-moz-box-sizing: border-box;
								-webkit-box-sizing: border-box;
								box-sizing: border-box;		

								background: #666;
						    	font-size: 9px;
						    	color: #FFF;
						    	text-transform: uppercase;
						    	text-align:center;
						    	float: right;
						    	margin-right: 15px;
						    	margin-top: -212px;
						    }

						    .container .image-wrapper .qr-code img{
						    	background: #666;
						    	padding-bottom: 20px;
						    }

						    .container .image-wrapper .qr-code #code-text{
							    background-color: #666;
							    width: 70px;
							    padding-top: 4px;
							    padding-bottom: 4px;
						    }
						    .container .coupon-title{
								width: 400px;
								height: 100px;
								position: absolute;
								top: 160px;
								z-index: 10;
							}

							.container .coupon-title #title-img{
								width: 355px;
								margin-top: -60px;
							}

							.container .coupon-title #title-img img{
								width: 100%;
							}

						    .container .coupon-title .title{
						    	    color: white;
								    width: 248px;
								    height: 60px;
								    margin-top: -55px;
								    margin-left: 96px;
						    }

						    .container .coupon-title .title span{
						    	height: 60px;
								font-size: 15px;
								vertical-align: middle;
						    }

						    .container .content-wrapper{
						    	position: relative;
						    	width: 330px;
						    	height: auto;
						    	margin: 0px 10px 10px 10px;;
								padding: 0px 10px 10px 10px;

						    	-moz-box-sizing: border-box;
								-webkit-box-sizing: border-box;
								box-sizing: border-box;

								color: #333;

								-webkit-box-shadow: 0px 2px 15px 0px rgba(133,133,133,0.2);
								-moz-box-shadow: 0px 2px 15px 0px rgba(133,133,133,0.2);
								box-shadow: 0px 2px 15px 0px rgba(133,133,133,0.2);

								margin-top: -70px;
						    }

						    .content-wrapper h4{
						    	margin-bottom: 10px;
						    	font-size: 15px;
						    }

						    table.item-list{
						    	width: 100%;
						    }
						    table.item-list tr td{
						    	font-size: 11px !important;
						    }

						    #term-conditions{
								font-size: 11px !important;
							}

							#term-conditions p{
								margin-top:0px;
								margin-bottom: 5px;
							}

						</style>

					</head>
					<body style='font-family: Arial;'>";

		$this->BuyerTransaction->recursive = 2;
		$buyer_id = $info['Buyer']['id'];

		$conds['conditions'] = array('BuyerTransaction.id' => $transaction_id, 'BuyerTransaction.buyer_id' => $buyer_id);

		$getData 	= $this->BuyerTransaction->find('first', $conds);


		$tranDesc 	= rtrim($getData['BuyerTransaction']['description'],", ");

		// Deal Information
		$dealInfo = $getData['DealInfo'];

		$deal_image = Router::url('/', true) . $dealInfo['image'];

		$code 				= strtoupper($getData['BuyerTransaction']['code']) ;
		$error 	 			= "Q";
		$border 			= 1;
		$qr_code 			= "<img src=\"http://chart.googleapis.com/chart?chs=70x70&chld=" . $error . "|" . $border . "&cht=qr&chl=" . $code . "\"/>";

		$qr_background 		= "<img src='" . Router::url('/', true ) . "img/coupon/qr-background.jpg" . "'/>" ;
		$title_background 	= Router::url('/', true ) . "img/coupon/title.png";
		$deal_title 		= $dealInfo['title'];
		$term_condition 	= $dealInfo['deal_condition'];

		// Bisiness Information
		$bizDetail 	= $getData['BusinessDetail'];
		$biz_name 	= $bizDetail['business_name'];
		$bizLogo 	= Router::url('/', true) . $bizDetail['logo'] ;
		$bizPhone 	= $bizDetail['phone1'];
		$bizPhone 	.= ($bizDetail['phone2'])?" / " .$bizDetail['phone2']:"";

		// Get Province Name by code
		$city_data 	= $this->City->findByCityCode($bizDetail['city']);
		$bizAddress = $bizDetail['street'] . ", " . $bizDetail['location'] . ", " .  $city_data['City']['city_name'];

		$allDealLogo = Router::url('/', true) . "img/coupon/deal-logo.png";

		$html 	.= "	<div class='container'>
							<div class='image-wrapper'>

								<div class='deal-image'>
									<img src='" . $deal_image . "' alt=''>
								</div>

								<div class='qr-code' style='padding-top:10px;'>" . 
									$qr_background . 
									"<div style='margin-top:-95px;'>" .
									$qr_code . 
									"</div>
									<div id='code-text'>" . $code . "</div>
								</div>
							</div>

							<div class='coupon-title'>
								<div id='title-img'>
									<img src='" . $title_background . "' />
								</div>
								<div class='title'>
									<span>" . $deal_title . "</span>
								</div>
							</div>

							<div class='content-wrapper'>
								<h4>Purchase includes:</h4>
								<table class='item-list'>";

							$itemDetails = $getData['TransactionDetail'];
				            $total = 0;
				            
				            foreach( $itemDetails as $key => $val ){
				            	$no = $key + 1;
				            	$unit_price = $val['unit_price'];
				            	$qty 		= $val['qty'];

				            	$amount 	= $unit_price * $qty;
				            	$total 		+= $amount;

			 	$html 	.= "<tr>
								<td width='150px;'>- " . $val['ItemDetail']['title'] . "</td>
								<td style='text-align:right'>$ " . number_format($unit_price, 2) . "</td>
								<td width='20px;' style='text-align:center;'>x</td>
								<td>" . $qty . "</td>
								<td style='text-align:right'>$ " . number_format($amount, 2) . "</td>
							</tr>";
						}

				$html 	.= "<tfoot>
								<tr>
									<td colspan='4' style='border-top:1px solid #333; width: 180px;'></td>
									<td style='text-align:right; padding-top:10px; border-top:1px solid #333;'>
										<strong style='font-size:15px;'>$ " . number_format($total, 2) . "</strong>
									</td>
								</tr>
							</tfoot>";

				$html 	.="</table>
							</div>

							<div style='clear:both; padding-left:10px; padding-right:10px;'>
								<table width='330px' style='font-size: 11px;'>
									<tr>
										<td width='70px;'>
											<img style='width: 70px; height:70px;' src='" . $bizLogo . "'/>
										</td>
										<td style='vertical-align:top; padding-left:10px;'>
											<h3 style='margin-top:0px; margin-bottom:0px;'>" . strtoupper($biz_name) . "</h3>
											<table style='width:100%;'>
												<tr>
													<td width='15px;'>
														<img src='" . Router::url('/', true) . "img/coupon/location.png' style='width:15px;'/>
													</td>
													<td>" . $bizAddress . "</td>
												</tr>
												<tr>
													<td>
														<img src='" . Router::url('/', true) . "img/coupon/phone.png' style='width:13px;'/>
													</td>
													<td>" . $bizPhone . "</td>
												</tr>
											</table>
										</td>
									</tr>
								</table>

								<div style='clear:both;'></div>
								<h4 style='margin-bottom:0px;'>Terms and Conditions</h4>
								<div id='term-conditions'>
									" . $term_condition . "
								</div>

								<div style='clear:both;'></div>
								<table style='width:100%;'>
									<tr>
										<td width='40px;'>
											<img src='" . $allDealLogo . "' style='width:40px;'/>
										</td>
										<td style='font-size:9px; vertical-align:middle !important;'>
											Note: Cancellation policy is according to the each store’s policy.<br>
											All Deal doesn’t warrant the products or services listed in this coupon.
										</td>
									</tr>
								</table>
								<div style='clear:both; padding-bottom:5px;'></div>
							</div>

						</div>";

		$html 	.= "</body>
				</html>";


		$filename = "Purchase Claim-" . date('d-M-Y') . "-" . $transaction_id . ".pdf";

		// echo $html; exit;

		$mpdf 	= new mPDF('c', 'A4');
		
		$mpdf->WriteHTML($html);
		$mpdf->Output( $filename, 'D' );
		exit;

	}

	public function setState($name,$value='') {

		$states = $this->getState();
		$states[$name] = $value;
		$this->Session->write(sha1($this->getSortId()),$states);	
	}

	/*
		Get State of the object
	*/

	public function getState( $name='', $value='' ){

		$stores = sha1($this->getSortId());
		$states = array();

		if($this->Session->check($stores)){
			$states = $this->Session->read($stores);
		}

		if(!$name) return $states;

		if(isset($states[$name])) return $states[$name];
		return $value;		
		
	}

	public function getSortId(){

		return md5($this->getContext());

	}

	public function getTransactionDetailAjax__(){
		
		$result['status'] = false;

		if( $this->request->is('post') ){
			$tranID = $this->request->data['tranID'];

			$this->BuyerTransaction->recursive = 3;

			$conds['conditions'] = array('BuyerTransaction.id' => $tranID);
			$data = $this->BuyerTransaction->find('first', $conds);

			if( $data ){
				$result['status'] 	= true;
				$result['data'] 	= $data;
			}

		}

		echo json_encode($result); 
		exit;
		
	}

	public function checkAvailableQTY( $deal_id = 0, $item_id = 0, $purchase_qty = 0 ){
		if( $deal_id == 0 || $item_id == 0 || $purchase_qty <= 0 ){
			return false;
		}

		$conditions['conditions'] = array(  'DealItemLink.deal_id' => $deal_id,
											'DealItemLink.item_id' => $item_id );

		$info = $this->DealItemLink->find('first', $conditions);

		if( $info && $info['DealItemLink']['available_qty'] < $purchase_qty ){
			$data['item_name']		= $info['ItemDetail']['title'];
			$data['available_qty'] 	= $info['DealItemLink']['available_qty'];
			return $data;
		}

		return false;
	
	}

	public function success(){

		$this->layout = "success";

		$info = $this->Session->read('Buyer.__SES_LOGGED_INFO');

		$params = $this->request->query;

		$token = (isset($params['ref']))?$params['ref']:"";

		if( !$info || !$token ){
			return $this->redirect('/');
		}

		$user_id 	= $info['Buyer']['id'];
		$user_email = $info['Buyer']['email'];

		$saveData 	= array();
		$old_bis_id = 0;

		$qty_issue 	= false;
		$issue_data = array();

		// Get TMP Data
		$conds = array();
		$conds['conditions'] = array( 'TmpTransaction.token' => $token, 'TmpTransaction.buyer_id' => $user_id );

		$this->TmpTransaction->recursive = 2;
		$dataInfo = $this->TmpTransaction->find('first', $conds);

		if( $dataInfo ){

			$deal_ids = $dataInfo['TmpTransaction']['deal_id'];

			// Check if chosen deals are still available
			$now = date('Y-m-d H:i:s');
			$dealConds = array();
			$dealConds['conditions'] = array( 
								'Deal.isdeleted' 		=> 0 ,
								'Deal.status'			=> 1,
								'Deal.valid_from <=' 	=> $now,
								'Deal.valid_to >'		=> $now,
								'Deal.id'				=> $deal_ids
							);

			$deals = $this->Deal->find('all', $dealConds);

			// If deals == 0, no active deals, Some deal is expired
			if( !$deals ){

				$this->Session->setFlash(__( 'The deal is no longer available !'),
	                                    'default',
	                                    array('class'=>'alert alert-dismissable alert-danger  align-center ') );

				return $this->redirect( array('action' => 'index') );
			}

			$tranDetail = $dataInfo['TransactionDetail'];
			$business_id = $dataInfo['TmpTransaction']['business_id'];

			$total_amount = 0;
			$description = array();
			$tmp_tran_id = $dataInfo['TmpTransaction']['id'];

			foreach( $tranDetail as $key => $val ){

				$description[] = $val['ItemDetail']['title'];

				$tmp = array();

				$deal_id 	= $val['deal_id'];
				$item_id 	= $val['item_id'];
				$price 	 	= $val['unit_price'];
				$qty 	 	= $val['qty'];

				$amount  = $qty * $price;
				$total_amount += $amount;

				$tmp['deal_id'] 	= $deal_id;
				$tmp['item_id']		= $item_id;
				$tmp['unit_price'] 	= $price;
				$tmp['qty']			= $qty;
				$tmp['amount']		= $amount;

				$saveData[$deal_id]['detail'][] 		= $tmp;
				$saveData[$deal_id]['type'] 			= 0 ; // 0 = Expense
				$saveData[$deal_id]['payment_type'] 	= 1 ;
				$saveData[$deal_id]['description'] 	    = "" ;
				$saveData[$deal_id]['business_id']		= $business_id;
				$saveData[$deal_id]['deal_id']			= $deal_id;
				$saveData[$deal_id]['amount'] 			= @$saveData[$deal_id]['amount'] + $amount;					

			}

		}else{
			return $this->redirect('/');
		}

		if( $saveData ){	

			//======= Set Bonus, if available
			// Get Bonus Commission
			$conds 	= array(	'conditions' => array('status' => 1, 'type' => $this->biz_commission_id),
										'fields' => array('commission')
								);

			$bonus_percent = $this->Commission->find('first', $conds);
			$percent = 0;

			if($bonus_percent){
				$percent = $bonus_percent['Commission']['commission'];
			}

			if( $info['Buyer']['bonus_received'] == 0 && $info['Buyer']['bonus_expiration_date'] >= date('Y-m-d')  ){

				// Get Bonus Commission
				if( $info['Buyer']['referer_id'] != 0 ){

					$email 		= $user_email;
					$referer_id = $info['Buyer']['referer_id'];

					// Get Bonus Amount
					$bonusAmount = $this->ReferalToken->find('first', array( 'conditions' => 
																				array('ReferalToken.referer_id' => $referer_id,
																					  'ReferalToken.email'		=> $email
																					) 
																			)
															);
					$referer_bonus = 0;
					$referee_bonus = 0;

					if( $bonusAmount ){
						$referee_bonus = $bonusAmount['ReferalToken']['referee_bonus'];
						$referer_bonus = $bonusAmount['ReferalToken']['referer_bonus'];
					}

					$dataReferee = array();
					$refereeInfo = $this->Buyer->findById($user_id);
					if( $refereeInfo ){
						$dataReferee['Buyer']['amount_balance'] = $refereeInfo['Buyer']['amount_balance'] + $referee_bonus ;
					}else{
						$dataReferee['Buyer']['amount_balance'] = $referee_bonus ;
					}

					// Update Plus referee bonus
					$dataReferee['Buyer']['id'] = $user_id;
					$dataReferee['Buyer']['bonus_received'] = 1;

					$this->Buyer->save($dataReferee);

					// Save bonus to transactions
					$tranBonus = array();
					$tranBonus['BuyerTransaction']['code'] 			= "";
					$tranBonus['BuyerTransaction']['buyer_id'] 		= $user_id;
					$tranBonus['BuyerTransaction']['type'] 			= $this->type_in ;
					$tranBonus['BuyerTransaction']['payment_type'] 	= 0 ;
					$tranBonus['BuyerTransaction']['description']	= 'Sign Up Bonus';
					$tranBonus['BuyerTransaction']['amount']		= $referee_bonus ;
					$tranBonus['BuyerTransaction']['created']		= date("Y-m-d H:i:s");
					$tranBonus['BuyerTransaction']['modified']		= date("Y-m-d H:i:s");
					
					$this->BuyerTransaction->create();
					$save = $this->BuyerTransaction->save($tranBonus);

					$dataReferer = array();

					$refererInfo = $this->Buyer->findById($info['Buyer']['referer_id']);
					if( $refererInfo ){
						$dataReferer['Buyer']['amount_balance'] = $refererInfo['Buyer']['amount_balance'] + $referer_bonus ;
					}else{
						$dataReferer['Buyer']['amount_balance'] = $referer_bonus;
					}

					$dataReferer['Buyer']['id'] = $info['Buyer']['referer_id'];

					$this->Buyer->save($dataReferer);

					// Save bonus to transactions
					$referBonus = array();
					$referBonus['BuyerTransaction']['code'] 		= "";
					$referBonus['BuyerTransaction']['buyer_id'] 	= $info['Buyer']['referer_id'];
					$referBonus['BuyerTransaction']['type'] 		= $this->type_in ;
					$referBonus['BuyerTransaction']['payment_type'] = 0 ;
					$referBonus['BuyerTransaction']['description']	= 'Referral Bonus';
					$referBonus['BuyerTransaction']['amount']		= $referer_bonus;
					$referBonus['BuyerTransaction']['created']		= date("Y-m-d H:i:s");
					$referBonus['BuyerTransaction']['modified']		= date("Y-m-d H:i:s");

					$this->BuyerTransaction->create();
					$save1 = $this->BuyerTransaction->save($referBonus);

					// Re-write Session
					$conds = array('conditions' => array('Buyer.id' => $user_id ) );
					$getInfo = $this->Buyer->find('first', $conds);
					$this->Session->write('Buyer.__SES_LOGGED_INFO', $getInfo );						

				}else{

					$conds 	= array( 'conditions' => array('status' => 1, 'type' => $this->singup_bonus_id),
									 'fields' => array('commission')
									);
					$bonus = $this->Commission->find('first', $conds);

					if( $bonus && !empty($bonus) ){

						$dataBuyer = array();
						$dataUser = $this->Buyer->findById($user_id);
						// Update Buyer
						$dataBuyer['Buyer']['id'] = $user_id;
						$dataBuyer['Buyer']['amount_balance'] = $dataUser['Buyer']['amount_balance'] + $bonus['Commission']['commission'];
						$dataBuyer['Buyer']['bonus_received'] = 1;

						$this->Buyer->save($dataBuyer);

						// Save bonus to transactions
						$transactionData = array();
						$transactionData['BuyerTransaction']['code'] 		= "";
						$transactionData['BuyerTransaction']['buyer_id'] 	= $user_id;
						$transactionData['BuyerTransaction']['type'] 		= $this->type_in ;
						$transactionData['BuyerTransaction']['payment_type'] = 0 ;
						$transactionData['BuyerTransaction']['description']	= 'Sign Up Bonus';
						$transactionData['BuyerTransaction']['amount']		= $bonus['Commission']['commission'];
						$transactionData['BuyerTransaction']['created']		= date("Y-m-d H:i:s");
						$transactionData['BuyerTransaction']['modified']	= date("Y-m-d H:i:s");

						$this->BuyerTransaction->create();
						$this->BuyerTransaction->save($transactionData);

						// Re-write Session
						$conds = array('conditions' => array('Buyer.id' => $user_id ) );
						$getInfo = $this->Buyer->find('first', $conds);
						$this->Session->write('Buyer.__SES_LOGGED_INFO', $getInfo );

					}
				
				}
			}

			//======= End Setting Bonus
			$TransactionCodes = array();
			$attachments 	  = array();

			// Remove TMP DATA
			$this->TmpTransaction->deleteAll(array('TmpTransaction.id' => $tmp_tran_id));
			$this->TmpTransactionDetail->deleteAll(array('TmpTransactionDetail.transaction_id' => $tmp_tran_id));

			foreach( $saveData as $key => $val ){

				// Get Deal Title
				$dealConds = array('conditions' => array('Deal.id' => $key) );
				$this->Deal->recursive = -1;
				$dealData = $this->Deal->find('first', $dealConds);

				$desc = $dealData['Deal']['title'];

				$tranData = array();
				$code = $this->generateTransactionCode();

				$TransactionCodes[$code] = $val['amount'] ;

				$tranData['business_id'] 		= $val['business_id'] ;
				$tranData['deal_id']			= $val['deal_id'];
				$tranData['code'] 				= $code ;
				$tranData['buyer_id']			= $user_id;
				$tranData['type']				= $val['type'];
				$tranData['payment_type']		= $val['payment_type'];
				$tranData['description']  		= $desc ;
				$tranData['amount']				= $val['amount'];
				$tranData['commission_percent']	= $percent;
				$tranData['status']				= 0 ;

				$card_number = NULL;

				if( $type == 1 ){
					$card_number = $creditCard_data['card-number'];
					$card_number = substr($card_number, -4);
				}
				
				$tranData['credit_card_number']	= $card_number ;

				$this->BuyerTransaction->create();

				if( $this->BuyerTransaction->save($tranData) ){

					$tran_id = $this->BuyerTransaction->getLastInsertId();	
					$itemDetails = $val['detail'];

					// Save Transaction Detail
					foreach( $itemDetails as $k => $item ){

						$temp = array();
						$temp['transaction_id'] = $tran_id;
						$temp['deal_id']		= $item['deal_id'];
						$temp['item_id']		= $item['item_id'];
						$temp['qty'] 			= $item['qty'];
						$temp['unit_price']		= $item['unit_price'];
						$temp['amount']			= $item['amount'];

						$this->BuyerTransactionDetail->create();
						$this->BuyerTransactionDetail->save($temp);

						// Update Available Qty
						$updateConds['conditions'] = array(	'DealItemLink.deal_id' => $item['deal_id'],
															'DealItemLink.item_id' => $item['item_id']
															);

						$this->DealItemLink->recursive = -1;
						$oldData = $this->DealItemLink->find('first', $updateConds);
						if( $oldData ){
							$updateData = array();
							$this->DealItemLink->id = $oldData['DealItemLink']['id'];

							$new_qty = $oldData['DealItemLink']['available_qty'] - $item['qty'];

							$updateData['DealItemLink']['id'] = $oldData['DealItemLink']['id'];
							$updateData['DealItemLink']['available_qty'] = $new_qty;

							$this->DealItemLink->save($updateData);
						}
					}								

					// Calculate Received Amount
					$amount 	 	= $val['amount'];
					$system_revenue	= $amount * $percent / 100 ;
					$biz_revenue 	= $amount - $system_revenue ;

					// Update Ending Balance of Business
					$update_sql = " UPDATE " . $this->business_table . 
								  " SET ending_balance = ( ending_balance + " . $biz_revenue . ") " .
								  " WHERE id = " . $val['business_id'] ;

					$this->Business->query($update_sql);

					// Save Revenue to Biz and System
					// Business
					$data_bis_revenue = array();
					$data_bis_revenue['transaction_id'] 	= $tran_id;
					$data_bis_revenue['type'] 				= 1; // 1: Credit
					$data_bis_revenue['payment_id'] 		= 0;
					$data_bis_revenue['business_id'] 		= $val['business_id'];
					$data_bis_revenue['transaction_amount'] = $val['amount'];
					$data_bis_revenue['commission_percent'] = $percent;
					$data_bis_revenue['total_amount']		= $biz_revenue;
					$data_bis_revenue['created'] 			= date('Y-m-d H:i:s');
					$data_bis_revenue['modified'] 			= date('Y-m-d H:i:s');

					$this->BusinessRevenue->create();
					$this->BusinessRevenue->save($data_bis_revenue);

					// System
					$data_sys_revenue = array();
					$data_sys_revenue['transaction_id'] 	= $tran_id;
					$data_sys_revenue['business_id']		= $val['business_id'];
					$data_sys_revenue['transaction_amount'] = $val['amount'];
					$data_sys_revenue['commission_percent'] = $percent;
					$data_sys_revenue['total_amount']		= $system_revenue;
					$data_sys_revenue['created'] 			= date('Y-m-d H:i:s');
					$data_sys_revenue['modified'] 			= date('Y-m-d H:i:s');

					$this->SystemRevenue->create();
					$this->SystemRevenue->save($data_sys_revenue);

					// Log
					$action = " Purchased Deal ";
					$logData['Purchase Deal'] = $tranData;
					$logData['Detail'] 		  = $itemDetails;
		            $logMessage = json_encode($logData);
		            parent::generateLog( $logMessage, $action, $user_id );

					// Generate Purchase Claim
					$name = "Purchase Claim-" . date('d-M-Y') . "-" . $tran_id . ".pdf";
					$file = $this->generatePurchaseClaimPDFAttacment( $name, $tran_id );

					$attachments[] = $file;

				}else{

					// Set Messages
					$this->Session->setFlash(__( 'Something went wrong. Please try again !' ),
			                                'default',
			                                array('class'=>'alert alert-dismissable alert-danger align-center ') );

					return $this->redirect( array('action' => 'index') );
				}

			}

			// Send Verification Mail
            $receiver   = $user_email;
            $subject    = "Purchased Items With All Deal";
            $content    = "";
            // $url        = Router::url("signUpVerification/" . $token, true );

            $content    .= "<html><body>";
            $content    .= "<p>Dear " . ucwords($info['Buyer']['full_name']) . ", </p><br>" ;
            $content    .= "<p>You have purchased items with amount: <b>$ ". number_format($total_amount,2) . "</b></p>" ;

            $content    .= "<p>Please print attached files to claim your purchased items.</p><br>" ;

            $content    .= "<p>Thanks for purchasing items with All Deal.</p><br>";
            // $content    .= $signature ;
            $content    .= "</body></html>";


            $sendMail =  $this->sendMail($receiver, $subject, $content, $attachments);

            // Remove file after attachments
            foreach( $attachments as $val ){
            	unlink($val);
            }
			
			// Set Messages
			$this->Session->setFlash(__('You have successfully purchased the deal. Purchase Claim has been sent to your email address.'),
                                'default',
                                array('class'=>'alert alert-dismissable alert-success align-center ') );

			return $this->redirect("/myaccount");

		}else{
			$this->Session->setFlash(__( 'Something went wrong. You cannot make purchase right now. Please try again !' ),
	                                'default',
	                                array('class'=>'alert alert-dismissable alert-danger align-center ') );

			return $this->redirect('/myaccount');
		}

	}

	public function paygo(){

		if( !$this->Session->read('Buyer') && !$this->Session->read('Buyer.__SES_LOGGED_INFO') ){
			return $this->redirect('/users/signin');
		}

		$userInfo = $this->Session->read('Buyer.__SES_LOGGED_INFO');		
		$params = $this->request->query;
		$ref = "";
		if( isset($params['ref']) && $params['ref'] != "" ){
			$ref = $params['ref'];

		}else{
			if($this->Session->read('Buyer.__SES_TMP_TOKEN') ){
				$ref = $this->Session->read('Buyer.__SES_TMP_TOKEN');
			}
		}

		$info = $this->Session->read('Buyer.__SES_LOGGED_INFO');

		$user_id = $info['Buyer']['id'];
		$userInfo = $this->Buyer->find('first', array(
                    'joins' => array(
                        array(
                            'table' => 'tb_cities',
                            'alias' => 'Cities',
                            'type' => 'LEFT',
                            'conditions' => array(
                                'Buyer.city_code = Cities.city_code'
                            )
                        )
                    ),
                    'conditions' => array(
                        'Buyer.id' => $user_id
                    ),
                    'fields' => array('Buyer.*', 'Cities.city_name', 'SangkatInfo.*')
                ));
		
		$this->set('userInfo', $userInfo);

		if( $ref ){
			// Check if ref is exist in TMP table
			$conds = array();
			$conds['conditions'] = array( 'TmpTransaction.token' => $ref, 'TmpTransaction.buyer_id' => $userInfo['Buyer']['id'] );

			$this->TmpTransaction->recursive = 2;
			$data = $this->TmpTransaction->find('first', $conds);
			$item = array();
			$order_id = 0;

			if( $data ){
				$this->Session->write('Buyer.__SES_TMP_TOKEN', $ref);
				$item = $data['TransactionDetail'];
				$order_id = $data['TmpTransaction']['id'];
			}else{
				return $this->redirect('/');
			}

			$this->set('items', $item);
			$this->set('token', $ref);
			$this->set('order_id', $order_id);

			$detail 	= $this->Buyer->findById($userInfo['Buyer']['id']);
			$this->set('balance', $detail['Buyer']['amount_balance']);

		}else{
			$this->set('items', array());
		}


		if( $this->request->is('post') ){

			$login 		= $this->__PAYGO_PARTNER_LOGIN ;
			$order_id 	= $this->request->data['order_id'] ;
			$point_id 	= $this->request->data['point_id'];
			$amount 	= number_format($this->request->data['amount'],2, '.', '') . '' ;
			$password 	= $this->__PAYGO_PARTNER_PASSWORD;

			$hash 		= $login . $password . $order_id . $point_id . $amount;
			$hash_str 	= $hash;

			$hash 		= sha1($hash, true );
			$hash 		= base64_encode($hash);
			$hash 		= urlencode($hash);

			$data = array(  'login' 	=> $login,
							'order_id' 	=> $order_id,
							'point_id'	=> $point_id,
							'amount'	=> $amount,
							'hash'		=> $hash
						);

			foreach( $data as $key => $val ){
				$data_string .= $key . "=" . $val . "&";
			}

			$data_string = rtrim($data_string, "&");

			//open connection
			$ch = curl_init();

			$headers = curl_getinfo($ch);
			curl_setopt($ch, CURLOPT_URL, $this->__PAYGO_CREATE_TRANSACTION );
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		    curl_setopt($cURL, CURLOPT_HTTPHEADER, array(
			    'Content-Type: application/json',
			    'Accept: application/json'
			));

			$result = curl_exec($ch);
			$result = json_decode($result, true);
			curl_close($ch);

			if( $result ){
				if( $result['status'] == 0 ){
					// success
					$this->Session->setFlash(__( 'Validation code has been sent by SMS.' )
                                            .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                                            'default',
                                            array('class'=>'alert alert-dismissable alert-success align-center '));

					$this->Session->write($ref, $result['transaction_id']);

					return $this->redirect('/payment/paygo-confirm/?ref' . $ref );

				}else{

				$this->Session->setFlash(__(ucwords($result['description']))
                                            .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                                            'default',
                                            array('class'=>'alert alert-dismissable alert-danger align-center '));

				}
			}

			return $this->redirect('/payment/paygo/?ref' . $ref );

		}

	}


	public function paygoConfirm(){

		if( !$this->Session->read('Buyer') && !$this->Session->read('Buyer.__SES_LOGGED_INFO') ){
			return $this->redirect('/users/signin');
		}

		$userInfo = $this->Session->read('Buyer.__SES_LOGGED_INFO');		
		$params = $this->request->query;
		$ref = "";
		if( isset($params['ref']) && $params['ref'] != "" ){
			$ref = $params['ref'];

		}else{
			if($this->Session->read('Buyer.__SES_TMP_TOKEN') ){
				$ref = $this->Session->read('Buyer.__SES_TMP_TOKEN');
			}
		}

		$info = $this->Session->read('Buyer.__SES_LOGGED_INFO');

		$user_id = $info['Buyer']['id'];
		$userInfo = $this->Buyer->find('first', array(
                    'joins' => array(
                        array(
                            'table' => 'tb_cities',
                            'alias' => 'Cities',
                            'type' => 'LEFT',
                            'conditions' => array(
                                'Buyer.city_code = Cities.city_code'
                            )
                        )
                    ),
                    'conditions' => array(
                        'Buyer.id' => $user_id
                    ),
                    'fields' => array('Buyer.*', 'Cities.city_name', 'SangkatInfo.*')
                ));
		
		$this->set('userInfo', $userInfo);

		if( $ref ){

			// Check if ref is exist in TMP table
			$conds = array();
			$conds['conditions'] = array( 'TmpTransaction.token' => $ref, 'TmpTransaction.buyer_id' => $userInfo['Buyer']['id'] );

			$this->TmpTransaction->recursive = 2;
			$data = $this->TmpTransaction->find('first', $conds);
			$item = array();
			$order_id = 0;
			$transaction_id = 0;

			if( $this->Session->read($ref) ){
				$transaction_id = $this->Session->read($ref);
			}

			if( $data ){
				$this->Session->write('Buyer.__SES_TMP_TOKEN', $ref);
				$item = $data['TransactionDetail'];
				$order_id = $data['TmpTransaction']['id'];

			}else{
				return $this->redirect('/');
			}

			$this->set('items', $item);
			$this->set('token', $ref);
			$this->set('order_id', $order_id);

			$detail 	= $this->Buyer->findById($userInfo['Buyer']['id']);
			$this->set('balance', $detail['Buyer']['amount_balance']);

		}else{
			$this->set('items', array());
		}


		if( $this->request->is('post') ){

			$login 				= $this->__PAYGO_PARTNER_LOGIN ;
			$password 			= $this->__PAYGO_PARTNER_PASSWORD;
			$validation_code 	= $this->request->data['validation_code'] ;

			$hash 		= $login . $password . $transaction_id . $validation_code;

			$hash 		= sha1($hash, true);
			$hash 		= base64_encode($hash);
			$hash 		= urlencode($hash);

			$data = array(  'login' 			=> $login,
							'transaction_id' 	=> intval($transaction_id),
							'validation_code'	=> $validation_code,
							'hash'				=> $hash
						);

			foreach( $data as $key => $val ){
				$data_string .= $key . "=" . $val . "&";
			}

			$data_string = rtrim($data_string, "&");

			//open connection
			$ch = curl_init();

			$headers = curl_getinfo($ch);
			curl_setopt($ch, CURLOPT_URL, $this->__PAYGO_CHECK_TRANSACTION );
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		    curl_setopt($cURL, CURLOPT_HTTPHEADER, array(
			    'Content-Type: application/json',
			    'Accept: application/json'
			));

			$result = curl_exec($ch);
			$result = json_decode($result, true);
			curl_close($ch);

			if( $result ){
				if( $result['status'] == 0 ){
					// success
					// Move Temporary Data to Real Table

					$info = $this->Session->read('Buyer.__SES_LOGGED_INFO');
					$params = $this->request->query;
					$token = $ref;

					if( !$info || !$token ){
						return $this->redirect('/');
					}

					$user_id 	= $info['Buyer']['id'];
					$user_email = $info['Buyer']['email'];

					$saveData 	= array();
					$old_bis_id = 0;

					$qty_issue 	= false;
					$issue_data = array();
					
					// Get TMP Data
					$conds = array();
					$conds['conditions'] = array( 'TmpTransaction.token' => $token, 'TmpTransaction.buyer_id' => $user_id );

					$this->TmpTransaction->recursive = 2;
					$dataInfo = $this->TmpTransaction->find('first', $conds);

					if( $dataInfo ){

						$deal_ids = $dataInfo['TmpTransaction']['deal_id'];

						// Check if chosen deals are still available
						$now = date('Y-m-d H:i:s');
						$dealConds = array();
						$dealConds['conditions'] = array( 
											'Deal.isdeleted' 		=> 0 ,
											'Deal.status'			=> 1,
											'Deal.valid_from <=' 	=> $now,
											'Deal.valid_to >'		=> $now,
											'Deal.id'				=> $deal_ids
										);

						$deals = $this->Deal->find('all', $dealConds);

						// If deals == 0, no active deals, Some deal is expired
						if( !$deals ){

							$this->Session->setFlash(__( 'The deal is no longer available !'),
				                                    'default',
				                                    array('class'=>'alert alert-dismissable alert-danger  align-center ') );

							return $this->redirect( array('action' => 'index') );

						}

						$tranDetail = $dataInfo['TransactionDetail'];
						$business_id = $dataInfo['TmpTransaction']['business_id'];

						$total_amount = 0;
						$description = array();
						$tmp_tran_id = $dataInfo['TmpTransaction']['id'];

						foreach( $tranDetail as $key => $val ){
							$description[] = $val['ItemDetail']['title'];

							$tmp = array();
							$deal_id 	= $val['deal_id'];
							$item_id 	= $val['item_id'];
							$price 	 	= $val['unit_price'];
							$qty 	 	= $val['qty'];

							$amount  = $qty * $price;
							$total_amount += $amount;

							$tmp['deal_id'] 	= $deal_id;
							$tmp['item_id']		= $item_id;
							$tmp['unit_price'] 	= $price;
							$tmp['qty']			= $qty;
							$tmp['amount']		= $amount;

							$saveData[$deal_id]['detail'][] 		= $tmp;
							$saveData[$deal_id]['type'] 			= 0 ; // 0 = Expense
							$saveData[$deal_id]['payment_type'] 	= 4 ; // PayGo
							$saveData[$deal_id]['description'] 	    = "" ;
							$saveData[$deal_id]['business_id']		= $business_id;
							$saveData[$deal_id]['deal_id']			= $deal_id;
							$saveData[$deal_id]['amount'] 			= @$saveData[$deal_id]['amount'] + $amount;					

						}

					}else{
						return $this->redirect('/');
					}

					if( $saveData ){	

						//======= Set Bonus, if available
						// Get Bonus Commission
						$conds 	= array(	'conditions' => array('status' => 1, 'type' => $this->biz_commission_id),
											'fields' => array('commission')
											);

						$bonus_percent = $this->Commission->find('first', $conds);
						$percent = 0;

						if($bonus_percent){
							$percent = $bonus_percent['Commission']['commission'];
						}

						if( $info['Buyer']['bonus_received'] == 0 && $info['Buyer']['bonus_expiration_date'] >= date('Y-m-d')  ){

							// Get Bonus Commission
							if( $info['Buyer']['referer_id'] != 0 ){

								$email 		= $user_email;
								$referer_id = $info['Buyer']['referer_id'];

								// Get Bonus Amount
								$bonusAmount = $this->ReferalToken->find('first', array( 'conditions' => 
																							array('ReferalToken.referer_id' => $referer_id,
																								  'ReferalToken.email'		=> $email
																								) 
																						)
																		);
								$referer_bonus = 0;
								$referee_bonus = 0;

								if( $bonusAmount ){
									$referee_bonus = $bonusAmount['ReferalToken']['referee_bonus'];
									$referer_bonus = $bonusAmount['ReferalToken']['referer_bonus'];
								}

								$dataReferee = array();
								$refereeInfo = $this->Buyer->findById($user_id);
								if( $refereeInfo ){
									$dataReferee['Buyer']['amount_balance'] = $refereeInfo['Buyer']['amount_balance'] + $referee_bonus ;
								}else{
									$dataReferee['Buyer']['amount_balance'] = $referee_bonus ;
								}

								// Update Plus referee bonus
								$dataReferee['Buyer']['id'] = $user_id;
								$dataReferee['Buyer']['bonus_received'] = 1;

								$this->Buyer->save($dataReferee);

								// Save bonus to transactions
								$tranBonus = array();
								$tranBonus['BuyerTransaction']['code'] 			= "";
								$tranBonus['BuyerTransaction']['buyer_id'] 		= $user_id;
								$tranBonus['BuyerTransaction']['type'] 			= $this->type_in ;
								$tranBonus['BuyerTransaction']['payment_type'] 	= 0 ;
								$tranBonus['BuyerTransaction']['description']	= 'Sign Up Bonus';
								$tranBonus['BuyerTransaction']['amount']		= $referee_bonus ;
								$tranBonus['BuyerTransaction']['created']		= date("Y-m-d H:i:s");
								$tranBonus['BuyerTransaction']['modified']		= date("Y-m-d H:i:s");
								
								$this->BuyerTransaction->create();
								$save = $this->BuyerTransaction->save($tranBonus);

								$dataReferer = array();

								$refererInfo = $this->Buyer->findById($info['Buyer']['referer_id']);
								if( $refererInfo ){
									$dataReferer['Buyer']['amount_balance'] = $refererInfo['Buyer']['amount_balance'] + $referer_bonus ;
								}else{
									$dataReferer['Buyer']['amount_balance'] = $referer_bonus;
								}

								$dataReferer['Buyer']['id'] = $info['Buyer']['referer_id'];

								$this->Buyer->save($dataReferer);

								// Save bonus to transactions
								$referBonus = array();
								$referBonus['BuyerTransaction']['code'] 		= "";
								$referBonus['BuyerTransaction']['buyer_id'] 	= $info['Buyer']['referer_id'];
								$referBonus['BuyerTransaction']['type'] 		= $this->type_in ;
								$referBonus['BuyerTransaction']['payment_type'] = 0 ;
								$referBonus['BuyerTransaction']['description']	= 'Referral Bonus';
								$referBonus['BuyerTransaction']['amount']		= $referer_bonus;
								$referBonus['BuyerTransaction']['created']		= date("Y-m-d H:i:s");
								$referBonus['BuyerTransaction']['modified']		= date("Y-m-d H:i:s");

								$this->BuyerTransaction->create();
								$save1 = $this->BuyerTransaction->save($referBonus);

								// Re-write Session
								$conds = array('conditions' => array('Buyer.id' => $user_id ) );
								$getInfo = $this->Buyer->find('first', $conds);
								$this->Session->write('Buyer.__SES_LOGGED_INFO', $getInfo );						

							}else{

								$conds 	= array( 'conditions' => array('status' => 1, 'type' => $this->singup_bonus_id),
												 'fields' => array('commission')
												);
								$bonus = $this->Commission->find('first', $conds);

								if( $bonus && !empty($bonus) ){

									$dataBuyer = array();
									$dataUser = $this->Buyer->findById($user_id);
									// Update Buyer
									$dataBuyer['Buyer']['id'] = $user_id;
									$dataBuyer['Buyer']['amount_balance'] = $dataUser['Buyer']['amount_balance'] + $bonus['Commission']['commission'];
									$dataBuyer['Buyer']['bonus_received'] = 1;

									$this->Buyer->save($dataBuyer);

									// Save bonus to transactions
									$transactionData = array();
									$transactionData['BuyerTransaction']['code'] 		= "";
									$transactionData['BuyerTransaction']['buyer_id'] 	= $user_id;
									$transactionData['BuyerTransaction']['type'] 		= $this->type_in ;
									$transactionData['BuyerTransaction']['payment_type'] = 0 ;
									$transactionData['BuyerTransaction']['description']	= 'Sign Up Bonus';
									$transactionData['BuyerTransaction']['amount']		= $bonus['Commission']['commission'];
									$transactionData['BuyerTransaction']['created']		= date("Y-m-d H:i:s");
									$transactionData['BuyerTransaction']['modified']	= date("Y-m-d H:i:s");

									$this->BuyerTransaction->create();
									$this->BuyerTransaction->save($transactionData);

									// Re-write Session
									$conds = array('conditions' => array('Buyer.id' => $user_id ) );
									$getInfo = $this->Buyer->find('first', $conds);
									$this->Session->write('Buyer.__SES_LOGGED_INFO', $getInfo );

								}
							
							}
						}

						//======= End Setting Bonus
						$TransactionCodes = array();
						$attachments 	  = array();

						// Remove TMP DATA
						$this->TmpTransaction->deleteAll(array('TmpTransaction.id' => $tmp_tran_id));
						$this->TmpTransactionDetail->deleteAll(array('TmpTransactionDetail.transaction_id' => $tmp_tran_id));

						foreach( $saveData as $key => $val ){

							// Get Deal Title
							$dealConds = array('conditions' => array('Deal.id' => $key) );
							$this->Deal->recursive = -1;
							$dealData = $this->Deal->find('first', $dealConds);

							$desc = $dealData['Deal']['title'];

							$tranData = array();
							$code = $this->generateTransactionCode();

							$TransactionCodes[$code] = $val['amount'] ;

							$tranData['business_id'] 		= $val['business_id'] ;
							$tranData['deal_id']			= $val['deal_id'];
							$tranData['code'] 				= $code ;
							$tranData['buyer_id']			= $user_id;
							$tranData['type']				= $val['type'];
							$tranData['payment_type']		= $val['payment_type'];
							$tranData['description']  		= $desc ;
							$tranData['amount']				= $val['amount'];
							$tranData['commission_percent']	= $percent;
							$tranData['status']				= 0 ;

							$card_number = NULL;							
							$tranData['credit_card_number']	= $card_number ;

							$this->BuyerTransaction->create();

							if( $this->BuyerTransaction->save($tranData) ){

								$tran_id = $this->BuyerTransaction->getLastInsertId();	
								$itemDetails = $val['detail'];

								// Save Transaction Detail
								foreach( $itemDetails as $k => $item ){

									$temp = array();
									$temp['transaction_id'] = $tran_id;
									$temp['deal_id']		= $item['deal_id'];
									$temp['item_id']		= $item['item_id'];
									$temp['qty'] 			= $item['qty'];
									$temp['unit_price']		= $item['unit_price'];
									$temp['amount']			= $item['amount'];

									$this->BuyerTransactionDetail->create();
									$this->BuyerTransactionDetail->save($temp);

									// Update Available Qty
									$updateConds['conditions'] = array(	'DealItemLink.deal_id' => $item['deal_id'],
																		'DealItemLink.item_id' => $item['item_id']
																		);

									$this->DealItemLink->recursive = -1;
									$oldData = $this->DealItemLink->find('first', $updateConds);

									if( $oldData ){
										$updateData = array();
										$this->DealItemLink->id = $oldData['DealItemLink']['id'];

										$new_qty = $oldData['DealItemLink']['available_qty'] - $item['qty'];

										$updateData['DealItemLink']['id'] = $oldData['DealItemLink']['id'];
										$updateData['DealItemLink']['available_qty'] = $new_qty;

										$this->DealItemLink->save($updateData);
									}
								}								

								// Calculate Received Amount
								$amount 	 	= $val['amount'];
								$system_revenue	= $amount * $percent / 100 ;
								$biz_revenue 	= $amount - $system_revenue ;

								// Update Ending Balance of Business
								$update_sql = " UPDATE " . $this->business_table . 
											  " SET ending_balance = ( ending_balance + " . $biz_revenue . ") " .
											  " WHERE id = " . $val['business_id'] ;

								$this->Business->query($update_sql);

								// Save Revenue to Biz and System
								// Business
								$data_bis_revenue = array();
								$data_bis_revenue['transaction_id'] 	= $tran_id;
								$data_bis_revenue['type'] 				= 1; // 1: Credit
								$data_bis_revenue['payment_id'] 		= 0;
								$data_bis_revenue['business_id'] 		= $val['business_id'];
								$data_bis_revenue['transaction_amount'] = $val['amount'];
								$data_bis_revenue['commission_percent'] = $percent;
								$data_bis_revenue['total_amount']		= $biz_revenue;
								$data_bis_revenue['created'] 			= date('Y-m-d H:i:s');
								$data_bis_revenue['modified'] 			= date('Y-m-d H:i:s');

								$this->BusinessRevenue->create();
								$this->BusinessRevenue->save($data_bis_revenue);

								// System
								$data_sys_revenue = array();
								$data_sys_revenue['transaction_id'] 	= $tran_id;
								$data_sys_revenue['business_id']		= $val['business_id'];
								$data_sys_revenue['transaction_amount'] = $val['amount'];
								$data_sys_revenue['commission_percent'] = $percent;
								$data_sys_revenue['total_amount']		= $system_revenue;
								$data_sys_revenue['created'] 			= date('Y-m-d H:i:s');
								$data_sys_revenue['modified'] 			= date('Y-m-d H:i:s');

								$this->SystemRevenue->create();
								$this->SystemRevenue->save($data_sys_revenue);

								// Log
								$action = " Purchased Deal ";
								$logData['Purchase Deal'] = $tranData;
								$logData['Detail'] 		  = $itemDetails;
					            $logMessage = json_encode($logData);
					            parent::generateLog( $logMessage, $action, $user_id );

								// Generate Purchase Claim
								$name = "Purchase Claim-" . date('d-M-Y') . "-" . $tran_id . ".pdf";
								$file = $this->generatePurchaseClaimPDFAttacment( $name, $tran_id );

								$attachments[] = $file;

							}else{

								// Set Messages
								$this->Session->setFlash(__( 'Something went wrong. Please try again !' ),
						                                'default',
						                                array('class'=>'alert alert-dismissable alert-danger align-center ') );

								return $this->redirect( array('action' => 'index') );
							}

						}

						// Send Verification Mail
			            $receiver   = $user_email;
			            $subject    = "Purchased Items With All Deal";
			            $content    = "";
			            // $url        = Router::url("signUpVerification/" . $token, true );

			            $content    .= "<html><body>";
			            $content    .= "<p>Dear " . ucwords($info['Buyer']['full_name']) . ", </p><br>" ;
			            $content    .= "<p>You have purchased items with amount: <b>$ ". number_format($total_amount,2) . "</b></p>" ;

			            $content    .= "<p>Please print attached files to claim your purchased items.</p><br>" ;

			            $content    .= "<p>Thanks for purchasing items with All Deal.</p><br>";
			            // $content    .= $signature ;
			            $content    .= "</body></html>";


			            $sendMail =  $this->sendMail($receiver, $subject, $content, $attachments);

			            // Remove file after attachments
			            foreach( $attachments as $val ){
			            	unlink($val);
			            }
						
						// Set Messages
						$this->Session->setFlash(__('You have successfully purchased the deal. Purchase Claim has been sent to your email address.'),
			                                'default',
			                                array('class'=>'alert alert-dismissable alert-success align-center ') );

						return $this->redirect("/myaccount");

					}else{
						$this->Session->setFlash(__( 'Something went wrong. You cannot make purchase right now. Please try again !' ),
				                                'default',
				                                array('class'=>'alert alert-dismissable alert-danger align-center ') );

						return $this->redirect('/myaccount');
					}

				}else{

					$this->Session->setFlash(__(ucwords($result['description']))
                                            .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                                            'default',
                                            array('class'=>'alert alert-dismissable alert-danger align-center '));

				}
			}

			return $this->redirect('/payment/paygo-confirm/?ref=' . $ref);

		}

	}


}