<?php

/********************************************************************************

File Name: UsersController.php
Description: controlling user module function

Powered By: ABi Investment Group Co., Ltd,

Changed History:

    Date                Author              Description
    2014/06/10          Sim Chhayrambo      Initial Version
    2014/06/11          Sim Chhayrambo      Add function "check_existing_email"
    2014/06/12          Sim Chhayrambo      Validate params
    2014/06/17          Sim Chhayrambo      Change bussiness image path (default.jpg)
    2014/06/19          Sim Chhayrambo      Check max length for street (30)

*********************************************************************************/

App::uses('AppController', 'Controller');

/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */

class UsersController extends AppController {

/**
 * Components
 *
 * @var array
 */

    var $context = 'User';

    var $helpers = array('Html', 'Form');

    //var $components = array('Email');

    var $uses = array('Business', 'User', 'MailConfigure');



/**
 * beforesave method
 *
 * @return void
 */


    public function beforeSave($options = array()) {
    }


/**
 * add method
 *
 * @return void
 */


    public function add() {

        $parmas = $this->request->data;

        $parmasUser = $parmas['User'];
        $parmasBusiness = $parmas['Business'];

        $sucess = "";
        $error = array();

        $token = $parmasUser['email'] . $parmasUser['password'];

        for ($i=0; $i < 10 ; $i++) { 
            $token = sha1($token);
        }

        if( !empty($parmasBusiness['business_name']) && 
            !empty($parmasBusiness['business_main_category']) && 
            !empty($parmasBusiness['email']) && 
            !empty($parmasBusiness['phone']) && 
            !empty($parmasBusiness['street']) && 
            !empty($parmasBusiness['city']) && 
            !empty($parmasBusiness['location']) && 
            !empty($parmasBusiness['sangkat_id']) ){

                if (!filter_var($parmasBusiness['email'], FILTER_VALIDATE_EMAIL)) {
                    array_push($error, "Invalid Email Address!");
                }

                if(!is_numeric($parmasBusiness['phone'])){
                    array_push($error, "Phone must contain number only!");
                }

                if(strlen($parmasBusiness['street']) > 30){
                    array_push($error, "Street must be less than 30 characters!");
                }

                if(!$error){

                    $businessData = array(
                        'Business' => array(
                            'business_name' => $parmasBusiness['business_name'],
                            'business_main_category'=> $parmasBusiness['business_main_category'],
                            'business_sub_category'=> $parmasBusiness['business_sub_category'],
                            'email'=> $parmasBusiness['email'],
                            'phone1'=> $parmasBusiness['phone'],
                            'logo'=> 'img/business/logos/default.jpg',
                            'street'=> $parmasBusiness['street'],
                            'province'=> 0,
                            'city'=> $parmasBusiness['city'],
                            'location'=> $parmasBusiness['location'],
                            'location_id'=> $parmasBusiness['location_id'],
                            'sangkat_id'=> $parmasBusiness['sangkat_id'],
                            'member_level'=> 1,
                            'status'=> 0,
                            'token'=> $token,
                            'created' => date('Y-m-d H:i:s'),
                            'modified' => date('Y-m-d H:i:s'),
                            'latitude'  => '11.5463',
                            'longitude' => '104.8978'
                        )
                    );

                    // prepare the model for adding a new entry
                    $this->Business->create();

                    // save the data
                    $addBusiness = $this->Business->save($businessData);

                    if($addBusiness){

                        $businessID = $this->Business->getLastInsertId();

                        $biz_code   = "M" . str_pad($businessID, 6, 0, STR_PAD_LEFT);
                        $updateData['Business']['id'] = $businessID;
                        $updateData['Business']['business_code'] = $biz_code;
                        $this->Business->save($updateData);

                        $userDay = str_pad($parmasUser['dob']['day'], 2, "0", STR_PAD_LEFT);
                        $userMonth = str_pad($parmasUser['dob']['month'], 2, "0", STR_PAD_LEFT);
                        $userYear = $parmasUser['dob']['year'];

                        if(!empty($parmasUser['phone'])){
                            $parmasUserPhone = $parmasUser['phone'];   
                        }else{
                            $parmasUserPhone = NULL;
                        }

                        $userDOB = $userYear . "-" . $userMonth . "-" . $userDay;

                        if(strlen($parmasUser['first_name']) > 15 || strlen($parmasUser['last_name']) > 15){
                            array_push($error, "Name must be less than 15 characters!");
                        }


                        if(!filter_var($parmasUser['email'], FILTER_VALIDATE_EMAIL)) {
                            array_push($error, "Invalid Email Address!");
                        }

                        $getUser = $this->User->findByEmail($parmasUser['email']);
                        
                        if(!empty($getUser)){
                            array_push($error, "Email is already registered!");
                        }

                        if($parmasUser['password'] != $parmasUser['confirm_password']){
                            array_push($error, "Password does not match!");
                        }

                        if(strlen($parmasUser['password']) < 6 || strlen($parmasUser['confirm_password']) < 6){
                            array_push($error, "Password must be at less 6 characters!");
                        }

                        if(!$error){

                            $userData = array(
                                'User' => array(
                                    'user_id' => uniqid(),
                                    'first_name'=> $parmasUser['first_name'],
                                    'last_name'=> $parmasUser['last_name'],
                                    'dob'=> $userDOB,
                                    'phone'=> $parmasUserPhone,
                                    'email'=> $parmasUser['email'],
                                    'password'=> $parmasUser['password'],
                                    'province_code'=> $parmasBusiness['city'],
                                    'city_code'=> $parmasBusiness['city'],
                                    'access_level'=> 1,
                                    'business_id'=> $businessID,
                                    'registered_date'=> date("Y-m-d H:i:s"),
                                    'status'=> 0,
                                    'token'=> $token
                                )
                            );

                            // prepare the model for adding a new entry
                            $this->User->create();

                            // save the data
                            $addUser = $this->User->save($userData);

                            $userID = $this->User->getLastInsertId();

                            if($addUser){

                                // Update Business Registered By
                                $businessData = array('Business' => array('registered_by' => $userID));
                                $this->Business->id = $businessID;
                                $businessResult = $this->Business->save($businessData);

                                // Send Verification Mail
                                $receiver   = $parmasUser['email'];
                                $subject    = "Sign Up Verification";
                                $content    = "";
                                $url        = Router::url("validate_email/".$token, true );

                                $content    .= "<html><body>";
                                $content    .= "<p>Dear " . $parmasUser['first_name'] . " " . $parmasUser['last_name'] . ", </p>";
                                $content    .= "<p>In order to activate your account, please verify your email address by clicking the following link:</p>";
                                $content    .= "<a href='" . $url . "'>" . $url . "</a><br>";
                                $content    .= "<p>Thanks for helping us maintain the security of your account.</p><br>";
                                $content    .= "</body></html>";

                                $sendMail =  $this->sendMail($receiver, $subject, $content);

                                if($sendMail){
                                    $this->Session->setFlash(__(    '<p>Registered Successfully.</p>'.
                                                                    '<p>Verification link has been sent to ' . $parmasUser['email'] . '</p>')
                                                                    .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                                                                    'default',
                                                                    array('class'=>'alert alert-dismissable alert-success align-center '));
                                    
                                        return $this->redirect(array( 'controller' => 'businesses', 'action' => 'register'));
                                }

                            }else{

                                $this->Session->setFlash(__(    '<p>Failed to register.</p>')
                                                                .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                                                                'default',
                                                                array('class'=>'alert alert-dismissable alert-error align-center '));
                                // return $this->redirect(array('controller'=>'Businesses', 'action' =>'register'));
                                return $this->redirect(array( 'controller' => 'businesses', 'action' => 'register'));
                            }

                        }else{

                            $strError = "<p>Failed to register!</p>";
                            foreach ($error as $key => $value) {
                                $strError .= "<p>".$value."</p>";
                            }
                            // return $this->redirect(array('controller'=>'Businesses', 'action' =>'register'));
                            return $this->redirect(array( 'controller' => 'businesses', 'action' => 'register'));
                        }

                    }else{

                        $this->Session->setFlash(__(    '<p>Failed to register.</p>')
                                                        .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                                                        'default',
                                                        array('class'=>'alert alert-dismissable alert-error align-center '));
                        // return $this->redirect(array('controller'=>'Businesses', 'action' =>'register'));
                        return $this->redirect(array( 'controller' => 'businesses', 'action' => 'register'));

                    }
                }else{

                    $strError = "<p>Failed to register!</p>";
                    foreach ($error as $key => $value) {
                        $strError .= "<p>".$value."</p>";
                    }

                    $this->Session->setFlash(__($strError)
                                                    .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                                                    'default',
                                                    array('class'=>'alert alert-dismissable alert-error align-center '));
                    return $this->redirect(array('controller'=>'Businesses', 'action' =>'register'));

                }
        }else{

            $this->Session->setFlash(__(    '<p>Failed to register.</p>')
                                            .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                                            'default',
                                            array('class'=>'alert alert-dismissable alert-error align-center '));
            return $this->redirect(array('controller'=>'Businesses', 'action' =>'register'));
        }
    }

    public function get_city_by_province( $pro_code = null ){

        if( $pro_code != null ){

            $city = new City();
            $cities =  $city->find('all',array( 'conditions' => array('province_code' => $pro_code, 'status' => 1 ))) ;

            $data = array();
            $data['data'] =  $cities;
            echo json_encode( $data);

            $this->autoRender = false;

        }
    }


    public function sendMail( $recipients = NULL, $subject = NULL , $content = NULL ) {

        return parent::sendMail($recipients, $subject, $content);

    }

    public function validate_email($token = ""){

        if(!empty($token)){

            $userInfo = $this->User->findByToken($token);

            $businessInfo = $this->Business->findByToken($token);

            if($userInfo){

                $userInfo = $userInfo['User'];
                $userID = $userInfo['id'];
                $userData = array('User' => array('status'=> 1, 'token'=>NULL));
                $this->User->id = $userID;
                $userResult = $this->User->save($userData);

            }

            if($businessInfo){

                $businessInfo = $businessInfo['Business'];
                $businessID = $businessInfo['id'];
                $businessData = array('Business' => array('status'=> 1, 'token'=>NULL));
                $this->Business->id = $businessID;
                $businessResult = $this->Business->save($businessData);

            }

            if( $userResult && $businessResult){

                $this->Session->setFlash(__( 'Thank you. Your account has been activate.')
                                                    .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                                                'default',
                                                array('class'=>'alert alert-dismissable alert-success align-center '));

                // return $this->redirect(array('controller'=>'Businesses', 'action' =>'register', 'verified'));
                return $this->redirect(array( 'controller' => 'businesses', 'action' => 'register'));

            }else{

                $this->Session->setFlash(__( 'Invalid Link Request.')
                                                    .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                                                'default',
                                                array('class'=>'alert alert-dismissable alert-danger align-center '));

                // return $this->redirect(array('controller'=>'Businesses', 'action' =>'register', 'verified'));
                return $this->redirect(array( 'controller' => 'businesses', 'action' => 'register'));

            }
        }
    }

    public function check_existing_email($user_email = ""){

        if(!empty($user_email)){
            $getUser = $this->User->findByEmail($user_email);
            if(!empty($getUser)){
                $result = true;
            }else{
                $result = false;
            }
        }else{
            $result = false;
        }

        $data['result'] = $result;
        echo json_encode($data);
        exit();

    }

}