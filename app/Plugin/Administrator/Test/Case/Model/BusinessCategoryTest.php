<?php
App::uses('BusinessCategory', 'Administrator.Model');

/**
 * BusinessCategory Test Case
 *
 */
class BusinessCategoryTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'plugin.administrator.business_category'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->BusinessCategory = ClassRegistry::init('Administrator.BusinessCategory');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->BusinessCategory);

		parent::tearDown();
	}

}
