<?php 
	/********************************************************************************
	    File: Add City
	    Author: PHEA RATTANA
	
	    Confidential ABi Technologies property.
	
	  	Changed History:
	  	Date 					Author				Description
	  	2014/01/05 				PHEA RATTANA		Initial
	*********************************************************************************/
	

	// var_dump($provinces);
	$pro_name = array();

	if( !empty($provinces) ){
		foreach( $provinces as $key => $value ){
			$pro_name[$value['Province']['province_code']] = $value['Province']['province_code'] . " - " . $value['Province']['province_name'];
			// $pro_code[] = $value['Province']['province_code'];
		}
	}

?>

<div class="cities form">
	<div class="row-fluid">		
		<!-- Pie: Box -->
		<div class="span12">

			<!-- Pie: Top Bar -->
			<div class="top-bar">
				<h3><i class="icon-list"></i> Manage Cities</h3>
			</div>
			<!-- / Pie: Top Bar -->

			<!-- Pie: Content -->
			<div class="well">

			<a href="<?php echo $this->Html->url( array('action' => 'index')); ?>"> 
				<button class="btn btn-linkedin" type="button"><i class="icon-list-alt"></i> City List</button>
			</a>
			

			<div class="clearfix"></div>
				
				<?php echo $this->Form->create('City', array('data-validate'=>'parsley')); ?>
					<fieldset style="margin-bottom: 140px;">

						<legend><?php echo __('Add New City'); ?></legend>

						<div class="span3">
							<?php
								echo $this->Form->input('city_code', array('placeholder'=>'City Code', 'required' => 'required'));
							?>	
						</div>

						<div class="span3">
							<?php
								echo $this->Form->input('city_name', array('placeholder'=>'City Name', 'required' => 'required'));
							?>	
						</div>

						<div class="span3">
							<?php
								echo $this->Form->input(	'province_code' ,array(
														   'type' 		=> 'select',
														   'label' 		=> 'Province Code',
														   // 'values'		=> $pro_code,
														   'options'	=> $pro_name
														));
							?>	
						</div>

						<div class="span2" style="padding-top:19px;">
							<?php 
								echo $this->Form->button(__('Save'), array('class' => 'btn btn-primary', 'id' => 'save', 'type' => 'button'	) );
							?>	
						</div>	
						
					</fieldset>
			

			</div>
			<!-- / Pie: Content -->

		</div>
		<!-- / Pie -->
		
	</div>

</div>

<style>


	.equalto, .required, .type {
		color: red;
		font-size: 12px !important;
		list-style-type: none;
	}

	.parsley-error-list{
		margin: 0px !important;
		padding: 0px !important;
	}

</style>


<div id="message" class="modal hide fade" tabindex="-1" 
	role="dialog" aria-labelledby="myModalLabel" 
	aria-hidden="true"
	style="">

	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="myModalLabel"><i class="icon-exclamation"></i> Warning !</h3>
	</div>

	<div class="span6" style="padding-top: 10px; padding-bottom: 20px;">
		
		<h5 id="message"></h5>

		<button class="btn btn-google cancel" data-dismiss='modal' type="button">Close</button>
	</div>

</div> 


<a href="#message" data-toggle="modal" id="message"></a>

<script type="text/javascript">

    $(document).ready(function(){
    	
    	function validateCode(){

			var code = $("#CityCityCode").val();
			var id = null ;

			var result = true;

			var url = '<?php echo $this->webroot ?>' + 'administrator/Cities/checkDuplicateCode/' + code + "/" + id ;

			$.ajax({
		            type: 'POST',
		            url: url,
		            data: { code : code, id : id },
		            dataType: 'json',
		            async: false,
		            
		            success: function (data){
		               	console.log(data);

		               	if( data.status == 1 ){
		               		result = true;
		               	}else{
		               		result = false;
		               	}
		            },

		            error: function ( err ){
		            	console.log(err);
		            }

		        });

			return result;
		}


		$("#save").click( function(){

			if(validateCode()){
				$("#CityAddForm").submit();
			}else{
				$("h5#message").text("This code is already been in use. Please try another code.");
				$("a#message").click();
				return false;	
			}
			return false;
		})

	});


</script>
