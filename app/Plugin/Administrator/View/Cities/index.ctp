<?php 

	/********************************************************************************
	    File: City Listing
	    Author: PHEA RATTANA
	
	    Confidential ABi Technologies property.
	
	  	Changed History:
	  	Date 					Author				Description
	  	2014/01/05 				PHEA RATTANA		Initial
	*********************************************************************************/
	

	$all_provinces = array();
	$pro_name = array();

	if( !empty($provinces) ){
		foreach( $provinces as $key => $pro ){
			$all_provinces[$pro['Province']['province_code']] = $pro['Province'];
			$pro_name[$pro['Province']['province_code']] = $pro['Province']['province_code'] . " - " . $pro['Province']['province_name'];
		}
	}

?>

<div class="provinces index">
	<div class="row-fluid">		
		<!-- Pie: Box -->
		<div class="span12">

			<!-- Pie: Top Bar -->
			<div class="top-bar">
				<h3><i class="icon-list"></i> Manage Cities</h3>
			</div>
			<!-- / Pie: Top Bar -->

			<!-- Pie: Content -->
			<div class="well">

			<a href="#" id="add-city" > 
				<button class="btn btn-linkedin" type="button"><i class="icon-plus"></i> Add New City</button>
			</a>
			

			<div class="clearfix"></div>
				
				<table class="table-list">
					<thead>
						<tr>
							<!--th><?php echo $this->Paginator->sort('id'); ?></th-->
							<th width="200px;"><?php echo $this->Paginator->sort('city_code'); ?></th>
							<th><?php echo $this->Paginator->sort('city_name'); ?></th>
							<th><?php echo $this->Paginator->sort('province_code',"Province Name"); ?></th>
							<th width="120px;" class="actions"><?php echo __('Actions'); ?></th>
						</tr>
					</thead>
					<tbody>
					<?php foreach ($data as $city): ?>
						<tr>
							<!--td><?php echo h($province['Province']['id']); ?>&nbsp;</td-->
							<td><?php echo h($city['City']['city_code']); ?>&nbsp;</td>
							<td><?php echo h($city['City']['city_name']); ?>&nbsp;</td>
							<td><?php echo h($all_provinces[$city['City']['province_code']]['province_name']); ?>&nbsp;</td>
							<td class="actions">
								<a  data-action="<?php echo $this->Html->url(array('action' => 'edit', $city['City']['id'])); ?>"
									href="#"
									data-code="<?php echo $city['City']['city_code'] ?>"
									data-name="<?php echo $city['City']['city_name'] ?>"
									data-pro-code="<?php echo $city['City']['province_code'] ?>"
									class="edit-city"> 
									<button class="btn btn-foursquare" type="button">Edit</button>
								</a>
								<a href="#delete" data-toggle="modal" class="delete" data-id="<?php echo $city['City']['id'] ?>">
									<button class="btn btn-google" type="button">Delete</button>
								</a>
							</td>
						</tr>
					<?php endforeach; ?>
					</tbody>
				</table>

				<p class="page">
					<?php
					echo $this->Paginator->counter(array(
					'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total')
					));
					?>	
				</p>

				<div class="paging">
				<?php
					echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
					echo $this->Paginator->numbers(array('separator' => ''));
					echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
				?>
				</div>
			</div>
			<!-- / Pie: Content -->

		</div>
		<!-- / Pie -->
		
	</div>

	</div>



<div id="delete" class="modal hide fade" tabindex="-1" 
	role="dialog" aria-labelledby="myModalLabel" 
	aria-hidden="true">

	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="myModalLabel">Are You Sure?</h3>
	</div>

	<!-- Any Info-customization -->
	<form action="" 
		  style="padding: 20px;" method="POST" class="form-horizontal" id="deteteFrom">
		
		<h5>Are you sure you want to delete this City?</h5>
		
		<div class="submit" style="padding-top: 20px; clear: both;">
			<button class="btn btn-google" data-dismiss='modal' type="button">Cancel</button>
		 	<input class="btn btn-primary" type="submit" value="Yes">
		</div>
			
	</form>

</div> 

<div id="add-city-modal" class="modal hide fade" tabindex="-1" 
	role="dialog" aria-labelledby="myModalLabel" 
	aria-hidden="true">

	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="myModalLabel">Add New City</h3>
	</div>

	<!-- Any Info-customization -->
	<form action="<?php echo $this->Html->url( array('action' => 'add')); ?>" 
		  style="padding: 20px;" method="POST" class="form-horizontal">
		

		<div class="span3">
			<?php
				echo $this->Form->input(	'province_code' ,array(
										   'type' 		=> 'select',
										   'label' 		=> 'Province Code',
										   'options'	=> $pro_name
										));
			?>	
		</div>

		<div class="span3">
			<?php
				echo $this->Form->input('city_code', array('placeholder'=>'City Code', 'required' => 'required'));
			?>	
		</div>

		<div class="span6  clearfix" style="margin-top:10px;">
			<?php
				echo $this->Form->input('city_name', array('placeholder'=>'City Name', 'required' => 'required', 'style'=>"width:440px"));
			?>	
		</div>
		
		<div class="submit" style="padding-top: 20px; clear: both;">
			<button class="btn btn-google" data-dismiss='modal' type="button">Cancel</button>
		 	<input class="btn btn-primary" type="submit" value="Save">
		</div>
			
	</form>

</div> 

<div id="edit-city-modal" class="modal hide fade" tabindex="-1" 
	role="dialog" aria-labelledby="myModalLabel" 
	aria-hidden="true">

	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="myModalLabel">Add New City</h3>
	</div>

	<!-- Any Info-customization -->
	<form action="" style="padding: 20px;" method="POST" class="form-horizontal">		

		<div class="span3">
			<?php
				echo $this->Form->input(	'province_code' ,array(
										   'type' 		=> 'select',
										   'label' 		=> 'Province Code',
										   'id' 		=> 'province-code',
										   'options'	=> $pro_name
										));
			?>	
		</div>

		<div class="span3">
			<?php
				echo $this->Form->input('city_code', array('placeholder'=>'City Code', 'required' => 'required', 'id' => 'city-code'));
			?>	
		</div>

		<div class="span6  clearfix" style="margin-top:10px;">
			<?php
				echo $this->Form->input('city_name', array('placeholder'=>'City Name', 'required' => 'required', 'style'=>"width:440px", 'id' => 'city-name' ));
			?>	
		</div>
		
		<div class="submit" style="padding-top: 20px; clear: both;">
			<button class="btn btn-google" data-dismiss='modal' type="button">Cancel</button>
		 	<input class="btn btn-primary" type="submit" value="Save Change">
		</div>
			
	</form>

</div> 


<script>

	$(".delete").click( function(){
		 var me = $(this),
		 	id = me.data('id');

		 var url = "<?php echo $this->Html->url(array('action' => 'delete')); ?>";

		var action = url + "/" + id;

		 $('#deteteFrom').attr('action', action) ;

	});

	$('#add-city').click( function( event ){
		event.preventDefault();

		$('#add-city-modal').modal('show');

	});

	$('.edit-city').click(function( event ) {
		event.preventDefault();

		var me 			= $(this),
			action  	= me.attr('data-action'),
			pro_code  	= me.attr('data-pro-code'),
			code 		= me.attr('data-code'),
			name 		= me.attr('data-name');

		var modal 	= $('div#edit-city-modal'),
			form 	= modal.find('form');

		form.find('#province-code').val(pro_code);
		form.find('#city-code').val(code);
		form.find('#city-name').val(name);

		form.attr('action', action);

		modal.modal('show');
	});

</script>


