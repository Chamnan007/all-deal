<?php
/**
 *
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = __d('cake_dev', 'All Deal');

if( strtolower($title_for_layout) == 'users'){
    $title_for_layout = "Administrator";
}

?>

<!DOCTYPE html>
<html>
<head>
    <?php echo $this->Html->charset(); ?>
    <title>
        
        <?php echo $title_for_layout; ?> | <?php echo $cakeDescription ?>
    </title>


    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,400,600,300' rel='stylesheet' type='text/css'>

    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>

    <?php
        echo $this->Html->meta('icon');

        // echo $this->Html->css('admin/cake.generic');

        echo $this->Html->css('admin/chosen');
        echo $this->Html->css('admin/bootstrap.min');
        echo $this->Html->css('admin/avocado');
        echo $this->Html->css('admin/prism');
        echo $this->Html->css('admin/fullcalendar'); 
        
        echo $this->Html->css('admin/bootstrap-responsive');

        echo $this->Html->script('admin/jquery-1.9.1.min');
        echo $this->Html->script('admin/charts/excanvas.min');
        echo $this->Html->script('admin/charts/jquery.flot');
        echo $this->Html->script('admin/jquery.jpanelmenu.min');
        echo $this->Html->script('admin/jquery.cookie');
        echo $this->Html->script('admin/avocado-custom-predom');
        echo $this->Html->script('admin/jquery.hotkeys');
        echo $this->Html->script('admin/calendar/fullcalendar.min');
        echo $this->Html->script('admin/jquery-ui-1.10.2.custom.min');
        echo $this->Html->script('admin/jquery.pajinate');
        echo $this->Html->script('admin/jquery.prism.min');
        // echo $this->Html->script('jquery.dataTables.min');

        echo $this->Html->script('admin/charts/jquery.flot.time');
        echo $this->Html->script('admin/charts/jquery.flot.pie');
        echo $this->Html->script('admin/charts/jquery.flot.resize');
        echo $this->Html->script('admin/bootstrap/bootstrap.min');
        echo $this->Html->script('admin/bootstrap/bootstrap-wysiwyg');
        echo $this->Html->script('admin/bootstrap/bootstrap-typeahead');
        echo $this->Html->script('admin/jquery.easing.min');
        echo $this->Html->script('admin/jquery.chosen.min');
        echo $this->Html->script('admin/avocado-custom');

        echo $this->Html->script('admin/form-parsley/parsley.es.min');
        echo $this->Html->script('admin/form-parsley/parsley.extend.min');
        echo $this->Html->script('admin/form-parsley/parsley.min');

        echo $this->fetch('meta');
        echo $this->fetch('css');
        echo $this->fetch('script');
    ?>

</head>
<body>

    <?php echo $this->fetch('content'); ?>

    <?php //echo $this->element('sql_dump'); ?>
    
</body>
</html>