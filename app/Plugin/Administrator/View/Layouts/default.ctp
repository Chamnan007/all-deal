<?php

/**
 *
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = __d('cake_dev', 'All Deal');

if( strtolower($title_for_layout) == "businesses" ){
	$title_for_layout = "Merchants";
}


if( strtolower($title_for_layout) == "buyertransactions" ){
	$title_for_layout = "Purchase Transactions";
}

if( strtolower($title_for_layout) == "systemrevenues" ){
	$title_for_layout = "Revenues";
}

$action 	= ($this->params['action'] != 'index' )?$this->params['action']:NULL ;

if( strtolower($action) == 'payment' ){
	$title_for_layout = "Payment History";
}else if( strtolower($action) == 'makepayment' ){
	$title_for_layout = 'Make Payment';
}

if( strtolower($title_for_layout) == 'buyers' ){
	$title_for_layout = "Users";
}

$title_for_layout = ucwords(ltrim(preg_replace('/[A-Z]/', ' $0', $title_for_layout)));
$action = ucwords(ltrim(preg_replace('/[A-Z]/', ' $0', $action)));

$title_for_layout = $action . " " . $title_for_layout;

?>

<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $title_for_layout; ?> | <?php echo $cakeDescription ?>
	</title>

	<link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,400,600,300' rel='stylesheet' type='text/css'>

	<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>

	<?php
		echo $this->Html->meta('icon');

		// echo $this->Html->css('cake.generic');

		echo $this->Html->css('admin/chosen');
		echo $this->Html->css('admin/bootstrap.min');
		echo $this->Html->css('admin/avocado');
		echo $this->Html->css('admin/prism');
		echo $this->Html->css('admin/fullcalendar'); 

		echo $this->Html->css('../js/admin/bootstrape-timepicker/css/bootstrap-datetimepicker.min'); 


		echo $this->Html->script('admin/jquery-1.9.1.min');
		echo $this->Html->script('admin/charts/excanvas.min');
		echo $this->Html->script('admin/charts/jquery.flot');
		echo $this->Html->script('admin/jquery.jpanelmenu.min');
		echo $this->Html->script('admin/jquery.cookie');
		echo $this->Html->script('admin/avocado-custom-predom');
		echo $this->Html->script('admin/jquery.hotkeys');
		echo $this->Html->script('admin/calendar/fullcalendar.min');
		echo $this->Html->script('admin/jquery-ui-1.10.2.custom.min');
		echo $this->Html->script('admin/jquery.pajinate');
		echo $this->Html->script('admin/jquery.prism.min');
		echo $this->Html->script('admin/jquery.dataTables.min');

		echo $this->Html->script('admin/charts/jquery.flot.time');
		echo $this->Html->script('admin/charts/jquery.flot.pie');
		echo $this->Html->script('admin/charts/jquery.flot.resize');
		echo $this->Html->script('admin/bootstrap/bootstrap.min');
		echo $this->Html->script('admin/bootstrap/bootstrap-wysiwyg');
		echo $this->Html->script('admin/bootstrap/bootstrap-typeahead');
		echo $this->Html->script('admin/jquery.easing.min');
		echo $this->Html->script('admin/jquery.chosen.min');
		echo $this->Html->script('admin/avocado-custom');

		echo $this->Html->script('admin/bootstrape-timepicker/js/bootstrap-datetimepicker.min');

		echo $this->Html->script('admin/form-ckeditor/ckeditor');
		echo $this->Html->script('admin/form-ckeditor/config');
		echo $this->Html->script('admin/form-ckeditor/styles');

		echo $this->Html->script('admin/form-parsley/parsley.es.min');
		echo $this->Html->script('admin/form-parsley/parsley.extend.min');
		echo $this->Html->script('admin/form-parsley/parsley.min');

		// Shadowbox
		echo $this->Html->css('../js/admin/shadowbox/shadowbox'); 
		echo $this->Html->script('admin/shadowbox/shadowbox');

		// Ajax Upload
		echo $this->Html->script('admin/ajaxupload/js/jquery.form.min');
		echo $this->Html->css('../js/admin/ajaxupload/style/style'); 


		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>

</head>

<body  style="overflow-x:scroll;">

	<?php echo $this->element('header') ?>

	<?php echo $this->Session->flash(); ?>
	
	<?php echo $this->fetch('content'); ?>

</div>

<?php echo $this->element('footer'); ?>

<?php //echo $this->element('sql_dump'); ?>
	
</body>
</html>