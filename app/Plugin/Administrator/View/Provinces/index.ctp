<?php 
	/********************************************************************************
	    File: Province Listing
	    Author: PHEA RATTANA
	
	    Confidential ABi Technologies property.
	
	  	Changed History:
	  	Date 					Author				Description
	  	2014/01/05 				PHEA RATTANA		Initial
	*********************************************************************************/
	
 ?>
<div class="provinces index">
	<div class="row-fluid">		
		<!-- Pie: Box -->
		<div class="span12">

			<!-- Pie: Top Bar -->
			<div class="top-bar">
				<h3><i class="icon-list"></i> Manage Provinces</h3>
			</div>
			<!-- / Pie: Top Bar -->

			<!-- Pie: Content -->
			<div class="well">

				<a href="#" id="add-province"> 
					<button class="btn btn-linkedin" type="button"><i class="icon-plus"></i> Add New Province</button>
				</a>
				

				<div class="clearfix"></div>
					
				<table class="table-list">
					<thead>
						<tr>
							<!--th><?php echo $this->Paginator->sort('id'); ?></th-->
							<th width="200px;"><?php echo $this->Paginator->sort('province_code'); ?></th>
							<th><?php echo $this->Paginator->sort('province_name'); ?></th>
							<th width="120px;" class="actions"><?php echo __('Actions'); ?></th>
						</tr>
					</thead>
					<tbody>
					<?php foreach ($data as $province): ?>
						<tr>
							<!--td><?php echo h($province['Province']['id']); ?>&nbsp;</td-->
							<td><?php echo h($province['Province']['province_code']); ?>&nbsp;</td>
							<td><?php echo h($province['Province']['province_name']); ?>&nbsp;</td>
							<td class="actions">
								<a 	href="#" class="edit-province" 
									data-code="<?php echo $province['Province']['province_code'] ?>"
									data-name="<?php echo $province['Province']['province_name'] ?>"
									data-action="<?php echo $this->Html->url(array('action' => 'edit', $province['Province']['id'])); ?>"> 
									<button class="btn btn-foursquare" type="button">Edit</button>
								</a>
								<a href="#delete" data-toggle="modal" class="delete" data-id="<?php echo $province['Province']['id'] ?>">
									<button class="btn btn-google" type="button">Delete</button>
								</a>
							</td>
						</tr>
					<?php endforeach; ?>
					</tbody>
				</table>

				<p class="page">
					<?php
					echo $this->Paginator->counter(array(
					'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total')
					));
					?>	
				</p>

				<div class="paging">
				<?php
					echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
					echo $this->Paginator->numbers(array('separator' => ''));
					echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
				?>
				</div>
			</div>
			<!-- / Pie: Content -->

		</div>
		<!-- / Pie -->
		
	</div>

</div>


<div id="delete" class="modal hide fade" tabindex="-1" 
	role="dialog" aria-labelledby="myModalLabel" 
	aria-hidden="true">

	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="myModalLabel">Are You Sure?</h3>
	</div>

	<!-- Any Info-customization -->
	<form action="" 
		  style="padding: 20px;" method="POST" class="form-horizontal" id="deteteFrom">
		
		<h5>Are you sure you want to delete this Province?</h5>
		
		<div class="submit" style="padding-top: 20px; clear: both;">
			<button class="btn btn-google" data-dismiss='modal' type="button">Cancel</button>
		 	<input class="btn btn-primary" type="submit" value="Yes">
		</div>
			
	</form>

</div> 

<div id="add-province-modal" class="modal hide fade" tabindex="-1" 
	role="dialog" aria-labelledby="myModalLabel" 
	aria-hidden="true">

	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="myModalLabel">Add New Province</h3>
	</div>

	<!-- Any Info-customization -->
	<form action="<?php echo $this->Html->url(array('action' => 'add' )); ?>" 
		  style="padding: 20px;" method="POST" class="form-horizontal">

		<fieldset>

			<div class="span3">
				<?php
					echo $this->Form->input('province_code', array('placeholder'=>'Province Code', 'required' => 'required'));
				?>	
			</div>

			<div class="span3">
				<?php
					echo $this->Form->input('province_name', array('placeholder'=>'Province Name', 'required' => 'required'));
				?>	
			</div>

			<div class="span3" style="padding-top:19px;">
				<button class="btn btn-google" data-dismiss='modal' type="button">Cancel</button>
				<?php 
					echo $this->Form->button(__('Save'), array('class' => 'btn btn-primary', 'id' => 'save', 'type' => 'submit'	) ); 
				 ?>	
			</div>

		</fieldset>
			
	</form>

</div> 


<div id="edit-province-modal" class="modal hide fade" tabindex="-1" 
	role="dialog" aria-labelledby="myModalLabel" 
	aria-hidden="true">

	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="myModalLabel">Edit Province</h3>
	</div>

	<!-- Any Info-customization -->
	<form action="<?php echo $this->Html->url(array('action' => 'edit' )); ?>" 
		  style="padding: 20px;" method="POST" class="form-horizontal">

		<fieldset>

			<div class="span3">
				<?php
					echo $this->Form->input('province_code', 
							array(	'placeholder'=>'Province Code', 'required' => 'required',
									'id' => 'province-code'
									));
				?>	
			</div>

			<div class="span3">
				<?php
					echo $this->Form->input('province_name', array('placeholder'=>'Province Name', 'required' => 'required',
									'id' => 'province-name'));
				?>	
			</div>

			<div class="span3" style="padding-top:19px;">
				<button class="btn btn-google" data-dismiss='modal' type="button">Cancel</button>
				<?php 
					echo $this->Form->button(__('Save Change'), array('class' => 'btn btn-primary', 'id' => 'save', 'type' => 'submit'	) ); 
				 ?>	
			</div>

		</fieldset>
			
	</form>

</div> 


<script>

	$(".delete").click( function(){
		 var me = $(this),
		 	id = me.data('id');

		 var url = "<?php echo $this->Html->url(array('action' => 'delete')); ?>";

		var action = url + "/" + id;

		 $('#deteteFrom').attr('action', action) ;

	});

	$('#add-province').click( function( event ){
		event.preventDefault();

		$('div#add-province-modal').modal('show');
	});

	$('.edit-province').click( function( event ){
		event.preventDefault();

		var modal = $('div#edit-province-modal');

		var action = $(this).attr('data-action');

		var code = $(this).attr('data-code');
		var name = $(this).attr('data-name');

		var form = modal.find('form');

		form.find('#province-code').val(code);
		form.find('#province-name').val(name);

		form.attr('action', action);
		modal.modal('show');

	})

</script>


