<?php 

	/********************************************************************************
	    File: Comission Listing
	    Author: PHEA RATTANA
	
	    Confidential ABi Technologies property.
	
	  	Changed History:
	  	Date 					Author				Description
	  	2015/Apr/09 			PHEA RATTANA		Initial
	*********************************************************************************/

	// Type of Comission

	// 1 = Bussiness Comission
	// 2 = Sign Up Bonus
	// 3 = Referer
	// 4 = Register


	$bis_commission = "<i>Not Set</i>";
	$signup_bonus 	= "<i>Not Set</i>";
	$referer 		= "<i>Not Set</i>";
	$register 		= "<i>Not Set</i>";

	if( $result && !empty($result) ){

		if( isset($result[1]) ){
			$bis_commission_val = $result[1]['commission'];
			$bis_commission = number_format($bis_commission_val, 2) . " %";
			$bis_commission_status = $result[1]['status'];
		}

		if( isset($result[2]) ){
			$signup_bonus_val 		= $result[2]['commission'];
			$signup_bonus 			= "$ " . number_format($signup_bonus_val, 2) ;
			$signup_bonus_status 	= $result[2]['status'];
		}

		if( isset($result[3]) ){
			$referer_val 	= $result[3]['commission'];
			$referer 		= "$ " . number_format($referer_val, 2) ;
			$referer_status = $result[3]['status'];
		}

		if( isset($result[4]) ){
			$register_val 		= $result[4]['commission'];
			$register 			= "$ " . number_format($register_val, 2) ;
			$register_status 	= $result[4]['status'];
		}

	}

?>

<div class="comissions index">
	<div class="row-fluid">		
		<!-- Pie: Box -->
		<div class="span12">

			<!-- Pie: Top Bar -->
			<div class="top-bar">
				<h3><i class="icon-cog"></i> Commission Setting</h3>
			</div>
			<!-- / Pie: Top Bar -->

			<!-- Pie: Content -->
			<div class="well">			

				<div class="clearfix"></div>
				
				<table class="table-list">
					<tr>
						<th width="30px">No</th>
						<th>Type of Comission</th>
						<th width="200px;">Commission</th>
						<th width="100px">Status</th>
						<th width="80px">Action</th>
					</tr>

					<tr>
						<td>1</td>	
						<td><?php echo $type[1] ?></td>
						<td><?php echo $bis_commission ?></td>
						<td style="text-align:center">
							<?php if(isset($bis_commission_status)) { ?>
								<span class="label label-<?php echo $status[$bis_commission_status]['color'] ?>"><?php echo h($status[$bis_commission_status]['status']); ?></span>
							<?php } ?>
						</td>
						<td class="actions" style="text-align: center">
							<?php if($user['access_level'] == 6 ){ ?>
								<a 	href="#" 
									data-type="1" 
									data-status="<?php echo @$bis_commission_status ?>" 
									data-value="<?php echo @$bis_commission_val ?>"
									class="edit-btn"> 
									
									<button class="btn btn-foursquare" type="button">Edit</button>
								</a>
							<?php } ?>
						</td>
					</tr>

					<tr>
						<td>2</td>
						<td><?php echo $type[2] ?></td>
						<td><?php echo $signup_bonus ?></td>
						<td style="text-align:center">
							<?php if(isset($signup_bonus_status)) { ?>
								<span class="label label-<?php echo $status[$signup_bonus_status]['color'] ?>"><?php echo h($status[$signup_bonus_status]['status']); ?></span>
							<?php } ?>
						</td>
						<td class="actions" style="text-align: center">
							<?php if($user['access_level'] == 6 ){ ?>
								<a 	href="#" 
									data-type="2" 
									data-status="<?php echo @$signup_bonus_status ?>"
									data-value="<?php echo @$signup_bonus_val ?>" 
									class="edit-btn"> 
									<button class="btn btn-foursquare" type="button">Edit</button>
								</a>
							<?php } ?>
						</td>
					</tr>

					<tr style="background: #DDD;">
						<td>3</td>
						<th colspan="4" style="text-align:left">Referal Sign Up</th>						
					</tr>

					<tr>
						<td></td>
						<td style="padding-left: 20px;">- <?php echo $type[3] ?></td>
						<td><?php echo $referer ?></td>
						<td style="text-align:center">
							<?php if(isset($referer_status)) { ?>
								<span class="label label-<?php echo $status[$referer_status]['color'] ?>"><?php echo h($status[$referer_status]['status']); ?></span>
							<?php } ?>
						</td>
						<td class="actions" style="text-align: center">
							<?php if($user['access_level'] == 6 ){ ?>
								<a 	href="#" 
									data-type="3" 
									data-status="<?php echo @$referer_status ?>"
									data-value="<?php echo @$referer_val ?>"
									class="edit-btn"> 
									<button class="btn btn-foursquare" type="button">Edit</button>
								</a>
							<?php } ?>
						</td>
					</tr>

					<tr>
						<td></td>
						<td style="padding-left: 20px;">- <?php echo $type[4] ?></td>
						<td><?php echo $register ?></td>
						<td style="text-align:center">
							<?php if(isset($register_status)) { ?>
								<span class="label label-<?php echo $status[$register_status]['color'] ?>"><?php echo h($status[$register_status]['status']); ?></span>
							<?php } ?>
						</td>
						<td class="actions" style="text-align: center">
							<?php if($user['access_level'] == 6 ){ ?>
								<a 	href="#" 
									data-type="4" 
									data-status="<?php echo @$register_status ?>"
									data-value="<?php echo @$register_val ?>"
									class="edit-btn">
									<button class="btn btn-foursquare" type="button">Edit</button>
								</a>
							<?php } ?>
						</td>
					</tr>

				</table>
				
			</div>
			<!-- / Pie: Content -->

		</div>
		<!-- / Pie -->
		
	</div>

</div>



<div id="edit-commission-modal" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true"
  style="">

  	<div class="modal-header">
    	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    	<h3 id="myModalLabel"><i class="icon-edit"></i> Edit Commission</h3>
  	</div>

  	<form action="<?php echo $this->Html->url(array('action' => 'save')) ?>" method="POST">
	  	<div class="span6" style="padding-top: 10px; padding-bottom: 20px; width: 500px;">

	  		<h5 id='type-name'></h5>

	  		<div class="span4" style="width: 200px">
				<?php
					echo $this->Form->input('commission', array('placeholder'=>'Commission',
																'type' => 'text',
																'required' => 'required',
																'id' => 'commission',
																'name' 	=> 'Commission[commission]',
																'style' => "width:200px;",
																'maxLength' => '11',
																'onkeypress' => 'return numericAndDotOnly(this)'));
				?>	
			</div>

	  		<div class="span4" style="width: 200px; margin-left: 50px;">
				<div class="input text">
					<label for="commission">Status</label>
					<select style="width: 200px;" name="Commission[status]" id='status'>
						<?php foreach( $com_status as $k => $val ){ ?>
							<option value="<?php echo $k ?>"><?php echo $val ?></option>
						<?php } ?>
					</select>
				</div>
			</div>

	  		<div class="clearfix" style="padding-top:30px;"></div>
	    	<button class="btn btn-google cancel" data-dismiss='modal' type="button">Cancel</button>
	    	<button class="btn btn-primary" type="submit" >Save Change</button>

		</div>	
	</form>
</div> 

<script>

	function numericAndDotOnly(elementRef) {
        
        var keyCodeEntered = (event.which) ? event.which : (window.event.keyCode) ? window.event.keyCode : -1;

      if ((keyCodeEntered >= 48) && (keyCodeEntered <= 57)) {
        return true;
      }   
      // '.' decimal point...
      else if (keyCodeEntered == 46) {
        // Allow only 1 decimal point ('.')...
        if ((elementRef.value) && (elementRef.value.indexOf('.') >= 0))
          return false;
        else
          return true;
      }

      return false;

    }

	$('.edit-btn').click ( function(event){

		event.preventDefault();

		var modal 	= $('div#edit-commission-modal');
		var type_id = $(this).attr('data-type');
		var types 	= <?php echo json_encode($type) ?>;
		var name 	= types[type_id];

		var form 	= modal.find('form'),
			action 	= form.attr('action');

		action = action + "/" + type_id;

		form.attr('action', action)

		modal.find('h5#type-name').text(name);

		var value 	= $(this).attr('data-value');
		var status 	= $(this).attr('data-status');

		if( value == ""){
			value = 0;
		}

		if( status == "" ){
			status = 1;
		}

		form.find('#commission').val(value);
		form.find('#status').val(status);

		modal.modal('show');
	})

</script>


