<?php 
	/********************************************************************************
	    File: Services Category Listing
	    Author: PHEA RATTANA
	
	    Confidential ABi Technologies property.
	
	  	Changed History:
	  	Date 					Author				Description
	  	2014/01/05 				PHEA RATTANA		Initial
	*********************************************************************************/
	
 ?>
<div class="servicesCategories index">
	<div class="row-fluid">		
		<!-- Pie: Box -->
		<div class="span12">

			<!-- Pie: Top Bar -->
			<div class="top-bar">
				<h3><i class="icon-list"></i> Manage Services Categories</h3>
			</div>
			<!-- / Pie: Top Bar -->

			<!-- Pie: Content -->
			<div class="well">

			<a href="<?php echo $this->Html->url( array('action' => 'add')); ?>"> 
				<button class="btn btn-linkedin" type="button"><i class="icon-plus"></i> Add New Services Category</button>
			</a>
			

			<div class="clearfix"></div>
				
				<table class="table-list">
					<thead>
						<tr>
							<!--th><?php echo $this->Paginator->sort('id'); ?></th-->
							<th><?php echo $this->Paginator->sort('category' ,'Services Category'); ?></th>
							<th width="120px;" class="actions"><?php echo __('Actions'); ?></th>
						</tr>
					</thead>
					<tbody>
					<?php foreach ($data as $servicesCategory): ?>
						<tr>
							<td><?php echo h($servicesCategory['ServicesCategory']['category']); ?>&nbsp;</td>
							<td class="actions">
								<!--a href="<?php echo $this->Html->url(array('action' => 'view', $servicesCategory['ServicesCategory']['id'])); ?>"> 
									<button class="btn btn-foursquare" type="button">Detail</button>
								</a-->
								<a href="<?php echo $this->Html->url(array('action' => 'edit', $servicesCategory['ServicesCategory']['id'])); ?>"> 
									<button class="btn btn-foursquare" type="button">Edit</button>
								</a>
								
								<a href="#delete" data-toggle="modal" class="delete" data-id="<?php echo $servicesCategory['ServicesCategory']['id'] ?>">
									<button class="btn btn-google" type="button">Delete</button>
								</a>
							</td>
						</tr>
					<?php endforeach; ?>
					</tbody>
				</table>

				<p class="page">
					<?php
					echo $this->Paginator->counter(array(
					'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total')
					));
					?>	
				</p>

				<div class="paging">
				<?php
					echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
					echo $this->Paginator->numbers(array('separator' => ''));
					echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
				?>
				</div>
			</div>
			<!-- / Pie: Content -->

		</div>
		<!-- / Pie -->
		
	</div>

</div>


<div id="delete" class="modal hide fade" tabindex="-1" 
	role="dialog" aria-labelledby="myModalLabel" 
	aria-hidden="true">

	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="myModalLabel">Are You Sure?</h3>
	</div>

	<!-- Any Info-customization -->
	<form action="" 
		  style="padding: 20px;" method="POST" class="form-horizontal" id="deteteFrom">
		
		<h5>Are you sure you want to delete this category?</h5>
		
		<div class="submit" style="padding-top: 20px; clear: both;">
			<button class="btn btn-google" data-dismiss='modal' type="button">Cancel</button>
		 	<input class="btn btn-primary" type="submit" value="Yes">
		</div>
			
	</form>

</div> 


<script>

	$(".delete").click( function(){
		 var me = $(this),
		 	id = me.data('id');

		 var url = "<?php echo $this->Html->url(array('action' => 'delete')); ?>";

		var action = url + "/" + id;

		 $('#deteteFrom').attr('action', action) ;

	})

</script>
