<?php 
	/********************************************************************************
	    File: Edit Location
	    Author: PHEA RATTANA
	
	    Confidential ABi Technologies property.
	
	  	Changed History:
	  	Date 					Author				Description
	  	2014/01/05 				PHEA RATTANA		Initial
	*********************************************************************************/
	

	// var_dump($cities);

	$city_name = array();

	if( !empty($cities) ){
		foreach( $cities as $key => $value ){
			$city_name[$value['City']['city_code']] = $value['City']['city_code'] . " - " . $value['City']['city_name'];
		}
	}

	$pro_name = array();

	if( !empty($provinces) ){
		foreach( $provinces as $key => $value ){
			$pro_name[$value['Province']['province_code']] = $value['Province']['province_code'] . " - " . $value['Province']['province_name'];
			// $pro_code[] = $value['Province']['province_code'];
		}
	}

	// var_dump($this->request->data);

	$select_province = $this->request->data['City']['province_code'];

?>

<div class="cities form">
	<div class="row-fluid">		
		<!-- Pie: Box -->
		<div class="span12">

			<!-- Pie: Top Bar -->
			<div class="top-bar">
				<h3><i class="icon-list"></i> Manage Locations</h3>
			</div>
			<!-- / Pie: Top Bar -->

			<!-- Pie: Content -->
			<div class="well">

			<a href="<?php echo $this->Html->url( array('action' => 'index')); ?>"> 
				<button class="btn btn-linkedin" type="button"><i class="icon-list-alt"></i> Location List</button>
			</a>			

			<div class="clearfix"></div>
				
				<?php echo $this->Form->create('Location'); ?>
					<fieldset style="margin-bottom: 160px;">

						<legend><?php echo __('Edit Information'); ?></legend>

						<div class="span3">
							<?php
								echo $this->Form->input('location_name', array(  'type'=>'text', 
																			'required'=>'required',
																			'placeholder'=>'Location', 
																			"style"=>"width:200px;"));
							?>	
						</div>

						<div class="span3">
							<?php
								echo $this->Form->input(	'province_code' ,array(
														   'type' 		=> 'select',
														   'label' 		=> 'Province',
														   'empty' 		=> 'Select a province',
														   'options'	=> $pro_name,
														   'default' 	=> $select_province
														));

							?>	
						</div>

						<div class="span3">
							<?php
								echo $this->Form->input(	'city_code' ,array(
														   'type' 		=> 'select',
														   'required'	=> 'required',
														   'label' 		=> 'City Code',
														   'options'	=> $city_name
														));

							?>	
						</div>

						<div class="span2" style="padding-top:19px;">
							<?php echo $this->Form->submit(__('Save Change'), array('class' => 'btn btn-primary') ); ?>	
						</div>	
						
					</fieldset>
			

			</div>
			<!-- / Pie: Content -->

		</div>
		<!-- / Pie -->
		
	</div>

</div>


<script type="text/javascript">

    $(document).ready(function(){


    	$("#LocationProvinceCode").change( function (){
			var me = $(this);

			var citySelect = $("#LocationCityCode");

			var pro_code = me.val();

			var url = '<?php echo $this->webroot ?>' + 'administrator/users/get_city_by_province/' + pro_code ;

			if( pro_code != '' ){
				$.ajax({
		            type: 'POST',
		            url: url,
		            data: { pro_code : pro_code },
		            dataType: 'json',
		            
		            beforeSend: function(){
		                citySelect.parent().find('span').text('Loading...');
		                // console.log("Loading...");
		            },
		            success: function (data){
		               	console.log('success');

		               	var data = data.data;

		               	citySelect.parent().find('span').text('Loading...');
		               	citySelect.html("<option value=''>select a city</option>")
		               	$.each( data, function (ind, val ){
		               		var city = val.City;

		               		citySelect.append("<option value='" + city.city_code + "'>" + city.city_code + " - " + city.city_name + "</option>" )
		               	});

		               	citySelect.removeClass('chzn-done').show().next().remove();
	             		citySelect.chosen();

		            },

		            error: function ( err ){
		            	console.log(err);
		            }

		        });

			}else{

				citySelect.html("<option value=''>select an option</option>");

			}

		});


	});


</script>
