<?php 
	/********************************************************************************
	    File: Location Listing
	    Author: PHEA RATTANA
	
	    Confidential ABi Technologies property.
	
	  	Changed History:
	  	Date 					Author				Description
	  	2014/01/95 				PHEA RATTANA		Initial
	*********************************************************************************/
	

	$all_cities = array();

	if( !empty($cities) ){
		foreach( $cities as $key => $city ){
			$all_cities[$city['City']['city_code']] = $city['City'];
		}
	}

	$pro_name = array();

	if( !empty($provinces) ){
		foreach( $provinces as $key => $value ){
			$pro_name[$value['Province']['province_code']] = $value['Province']['province_code'] . " - " . $value['Province']['province_name'];
		}
	}

?>

<div class="citites index">
	<div class="row-fluid">		
		<!-- Pie: Box -->
		<div class="span12">

			<!-- Pie: Top Bar -->
			<div class="top-bar">
				<h3><i class="icon-list"></i> Manage Locations/ Khan</h3>
			</div>
			<!-- / Pie: Top Bar -->

			<!-- Pie: Content -->
			<div class="well">

			<a class="btn-add-location"> 
				<button class="btn btn-linkedin" type="button"><i class="icon-plus"></i> Add New Location/ Khan</button>
			</a>
			

			<div class="clearfix"></div>
				
				<table class="table-list">
					<thead>
						<tr>
							<!--th><?php echo $this->Paginator->sort('id'); ?></th-->
							<th width="300px;"><?php echo $this->Paginator->sort('city_code', 'City Name'); ?>&nbsp;</th>
							<th><?php echo $this->Paginator->sort('location'); ?>&nbsp;</th>
							<th width="120px;" class="actions"><?php echo __('Actions'); ?></th>
						</tr>
					</thead>
					<tbody>
					<?php foreach ($data as $location): ?>
						<tr>
							
							<td><?php echo h($location['Location']['city_code'] . " - " . $location['City']['city_name']); ?>&nbsp;</td>
							<td><?php echo h($location['Location']['location_name']); ?>&nbsp;</td>
							<td class="actions">
								<a data-action="<?php echo $this->Html->url(array('action' => 'edit', $location['Location']['id'])); ?>"
									class="btn-edit-location"
									data-location="<?php echo $location['Location']['location_name'] ?>"
									data-city-code="<?php echo $location['Location']['city_code'] ?>"
									data-pro-code="<?php echo $location['City']['province_code'] ?>"
									href="#"
									> 
									<button class="btn btn-foursquare" type="button">Edit</button>
								</a>
								<a href="#delete" data-toggle="modal" class="delete" data-id="<?php echo $location['Location']['id'] ?>">
									<button class="btn btn-google" type="button">Delete</button>
								</a>
							</td>
						</tr>
					<?php endforeach; ?>
					</tbody>
				</table>

				<p class="page">
					<?php
					echo $this->Paginator->counter(array(
					'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total')
					));
					?>	
				</p>

				<div class="paging">
				<?php
					echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
					echo $this->Paginator->numbers(array('separator' => ''));
					echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
				?>
				</div>
			</div>
			<!-- / Pie: Content -->

		</div>
		<!-- / Pie -->
		
	</div>

</div>

<div id="delete" class="modal hide fade" tabindex="-1" 
	role="dialog" aria-labelledby="myModalLabel" 
	aria-hidden="true">

	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="myModalLabel">Are You Sure?</h3>
	</div>

	<!-- Any Info-customization -->
	<form action="" 
		  style="padding: 20px;" method="POST" class="form-horizontal" id="deteteFrom">
		
		<h5>Are you sure you want to delete this Location?</h5>
		
		<div class="submit" style="padding-top: 20px; clear: both;">
			<button class="btn btn-google" data-dismiss='modal' type="button">Cancel</button>
		 	<input class="btn btn-primary" type="submit" value="Yes">
		</div>
			
	</form>

</div> 

<div id="add-location-modal" class="modal hide fade" tabindex="-1" 
	role="dialog" aria-labelledby="myModalLabel" 
	aria-hidden="true">

	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="myModalLabel">Add New Location/ Khan</h3>
	</div>

	<!-- Any Info-customization -->
	<form action="<?php echo $this->Html->url( array('action' => 'add')); ?>" 
		  style="padding: 20px;" method="POST" class="form-horizontal">
		

		<div class="span3">
			<?php
				echo $this->Form->input(	'province_code' ,array(
										   'type' 		=> 'select',
										   'label' 		=> 'Province',
										   'empty' 		=> 'Select a province',
										   'required'	=> 'required',
										   'options'	=> $pro_name
										));

			?>	
		</div>

		<div class="span3">
			<?php
				echo $this->Form->input(	'city_code' ,array(
										   'type' 		=> 'select',
										   'required'	=> 'required',
										   'label' 		=> 'City Code',
										   'empty' 		=> 'select a city',
										   'options'	=> array()
										));

			?>	
		</div>

		<div class="span6" style="clear:both; margin-top:10px;">
			<?php
				echo $this->Form->input('location_name', array(  'type'=>'text', 
															'required'=>'required',
															'placeholder'=>'Location', 
															"style"=>"width:440px;"));
			?>	
		</div>
		
		<div class="submit" style="padding-top: 20px; clear: both;">
			<button class="btn btn-google" data-dismiss='modal' type="button">Cancel</button>
		 	<input class="btn btn-primary" type="submit" value="Save">
		</div>
			
	</form>

</div> 

<div id="edit-location-modal" class="modal hide fade" tabindex="-1" 
	role="dialog" aria-labelledby="myModalLabel" 
	aria-hidden="true">

	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="myModalLabel">Edit Location</h3>
	</div>

	<!-- Any Info-customization -->
	<form action="" style="padding: 20px;" method="POST" class="form-horizontal">
		

		<div class="span3">
			<?php
				echo $this->Form->input(	'province_code' ,array(
										   'type' 		=> 'select',
										   'label' 		=> 'Province',
										   'empty' 		=> 'Select a province',
										   'required'	=> 'required',
										   'id' 		=> 'province-code-edit',
										   'options'	=> $pro_name
										));

			?>	
		</div>

		<div class="span3">
			<?php
				echo $this->Form->input(	'city_code' ,array(
										   'type' 		=> 'select',
										   'required'	=> 'required',
										   'label' 		=> 'City Code',
										   'id'			=> 'city-code-edit',
										   'empty' 		=> 'select a city',
										   'options'	=> array()
										));

			?>	
		</div>

		<div class="span6" style="clear:both; margin-top:10px;">
			<?php
				echo $this->Form->input('location_name', array(  'type'=>'text', 
															'required'=>'required',
															'placeholder'=>'Location', 
															'id'=>'location', 
															"style"=>"width:440px;"));
			?>	
		</div>
		
		<div class="submit" style="padding-top: 20px; clear: both;">
			<button class="btn btn-google" data-dismiss='modal' type="button">Cancel</button>
		 	<input class="btn btn-primary" type="submit" value="Save Change">
		</div>
			
	</form>

</div>

<script>

	$("#province_code").change( function (){
		var me = $(this);

		var citySelect = $("#city_code");

		var pro_code = me.val();

		var url = '<?php echo $this->webroot ?>' + 'administrator/users/get_city_by_province/' + pro_code ;

		if( pro_code != '' ){
			$.ajax({
	            type: 'POST',
	            url: url,
	            data: { pro_code : pro_code },
	            dataType: 'json',
	            
	            beforeSend: function(){
	                // citySelect.parent().find('span').text('Loading...');
	                citySelect.html('<option>Loading...</option>');
	                // console.log("Loading...");
	            },
	            success: function (data){
	               	console.log('success');

	               	var data = data.data;

	               	// citySelect.parent().find('span').text('Loading...');
	               	citySelect.html("<option value=''>select a city</option>")
	               	$.each( data, function (ind, val ){
	               		var city = val.City;

	               		citySelect.append("<option value='" + city.city_code + "'>" + city.city_code + " - " + city.city_name + "</option>" )
	               	});
	            },

	            error: function ( err ){
	            	console.log(err);
	            }

	        });

		}else{

			citySelect.html("<option value=''>select an option</option>");

		}

	});

	
	$("#province-code-edit").change( function (e){

		var me = $(this);
		var citySelect = $("#city-code-edit");

		var pro_code = me.val();

		var url = '<?php echo $this->webroot ?>' + 'administrator/users/get_city_by_province/' + pro_code ;

		if( pro_code != '' ){
			$.ajax({
	            type: 'POST',
	            url: url,
	            data: { pro_code : pro_code },
	            dataType: 'json',
	            
	            beforeSend: function(){
	                citySelect.html('<option>Loading...</option>');
	            },
	            success: function (data){
	               	console.log('success');

	               	var data = data.data;
	               	citySelect.html("<option value=''>select a city</option>")
	               	$.each( data, function (ind, val ){
	               		var city = val.City;

	               		citySelect.append("<option value='" + city.city_code + "'>" + city.city_code + " - " + city.city_name + "</option>" )
	               	});
	            },

	            error: function ( err ){
	            	console.log(err);
	            }

	        });

		}else{

			citySelect.html("<option value=''>select an option</option>");

		}

	});

	$(".delete").click( function(){

		 var me = $(this),
		 	id = me.data('id');

		 var url = "<?php echo $this->Html->url(array('action' => 'delete')); ?>";

		var action = url + "/" + id;

		 $('#deteteFrom').attr('action', action) ;

	});

	$('a.btn-add-location').click( function( event ){
		event.preventDefault();

		$('div#add-location-modal').modal('show');

	});

	$('.btn-edit-location').click( function( event ){

		event.preventDefault();

		var me 			= $(this),
			action 		= me.attr('data-action'),
			pro_code 	= me.attr('data-pro-code'),
			city_code 	= me.attr('data-city-code'),
			name 		= me.attr('data-location');

		var modal 		= $('div#edit-location-modal'),
			form 		= modal.find('form');

		var citySelect = $('#city-code-edit');

		form.find('#province-code-edit').val(pro_code)

		var url = '<?php echo $this->webroot ?>' + 'administrator/users/get_city_by_province/' + pro_code ;

		if( pro_code != '' ){
			$.ajax({
	            type: 'POST',
	            url: url,
	            data: { pro_code : pro_code },
	            dataType: 'json',
	            
	            beforeSend: function(){
	                citySelect.html('<option>Loading...</option>');
	            },
	            success: function (data){
	               	console.log('success');

	               	var data = data.data;
	               	citySelect.html("<option value=''>select a city</option>")
	               	$.each( data, function (ind, val ){
	               		var city = val.City;

	               		var selected = "";
	               		if( city.city_code == city_code ){
	               			selected = " selected='selected'";
	               		}
	               		citySelect.append("<option value='" + city.city_code + "' " + selected + ">" + city.city_code + " - " + city.city_name + "</option>" )
	               	});
	            },

	            error: function ( err ){
	            	console.log(err);
	            }

	        });

		}else{

			citySelect.html("<option value=''>select an option</option>");

		}

		form.find('#location').val(name);
		form.attr('action', action);

		modal.modal('show');

	})


</script>
