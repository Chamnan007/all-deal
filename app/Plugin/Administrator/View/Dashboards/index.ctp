<?php 
     
    /********************************************************************************

      File Name: index.ctp ( Dashboard )
      Description: Listing All Deals

      Powered By: ABi Investment Group Co., Ltd,

      Changed History:
      Date                Author              Description
      2014/06/25          Rattana Phea        Initial Version
      2014/06/01          Rattana Phea        Update on dashboard information, pending deal, approve deal

    *********************************************************************************/

	$goods_category = array();

  	if( !empty($goods) ){
    	foreach( $goods as $key => $value ){
      		$goods_category[$value['GoodsCategory']['id']] = urldecode($value['GoodsCategory']['category']);
    	}
  	}

 ?>

<style>
	
	table, tr, td{
		vertical-align: top;
	}

	table.no-border{
		border: none;
	}

	.no-border th, .no-border tr, .no-border td,.no-border td{
		border: none !important;
		padding: 2px !important;
	}

</style>

<div class="dashboards index">
	<div class="row-fluid">		
		<!-- Pie: Box -->
		<div class="span12">

			<div class="top-bar">
				<h3><i class="icon-list"></i> Dashboard</h3>
			</div>

			<div class="well" style="min-height:300px; color: #333;">
				<legend style="overflow: hidden; font-size: 15px; ">
					<h5 class="pull-left"><?php echo $period_string; ?></h5>
					<div class="pull-right">
						<a id="btn-search-transaction" href="#" class="btn btn-linkedin">
							<i class="icon icon-search"></i> Search Filter
						</a>
						<a id="btn-search-clear" href="#" class="btn btn-google">
							<i class="icon icon-trash"></i> Clear Search
						</a>
					</div>
				</legend>
				
				<div class="span11">

					<table style="color:#333;">
						<tr>
							<td><h5 style="color:#333;">- Total Sale</h5></td>
							<td width="50px"> : </td>
							<td width="100px;">
								<h5 style="color: #06F">
									<?php echo ($total_sale)?"$ " . $this->MyHtml->formatNumber($total_sale):0; ?>
								</h5>
							</td>
							<td style="padding-top:0px;">
								<!-- <a target="_blank" href="<?php echo $this->Html->url( array('action' => 'index', 'controller' => 'SystemRevenues', 'plugin' => 'administrator' )); ?>"> 
									<button style="font-size:12px; padding: 2px 5px;" class="btn btn-skype" type="button">View</button>
								</a> -->
							</td>
						</tr>
						<tr>
							<td><h5 style="color:#333;">- Total Commission</h5></td>
							<td> : </td>
							<td>
								<h5 style="color: #06F">
									<?php echo ($total_commission)?"$ " . $this->MyHtml->formatNumber($total_commission):0; ?>
								</h5>
							</td>
							<td style="padding-top:0px;">
								<!-- <a target="_blank" href="<?php echo $this->Html->url( array('action' => 'index', 'controller'=>'businesses')); ?>"> 
									<button style="font-size:12px; padding: 2px 5px;" class="btn btn-skype" type="button">View</button>
								</a> -->
							</td>
						</tr>
						<tr>
							<td><h5 style="color:#333;">- Total Account Payable</h5></td>
							<td> : </td>
							<td>
								<h5 style="color: #06F">
									<?php echo ($total_account_payable)?"$ " . $this->MyHtml->formatNumber($total_account_payable):0; ?>
								</h5>
							</td>
							<td style="padding-top:0px;">
								<a target="_blank" 
									href="<?php echo $this->Html->url(array('controller'=>'SystemRevenues', 'action'=>'makepayment', 'plugin'=> 'administrator')); ?>" >
									<button style="font-size:12px; padding: 2px 5px;" class="btn btn-skype" type="button">View</button>
								</a>
							</td>
						</tr>
						<tr>
							<td><h5 style="color:#333;">- Total Merchant Created</h5></td>
							<td> : </td>
							<td><h5 style="color: #06F"><?php echo @$total_biz ?></h5></td>
							<td style="padding-top:0px;">
								<a target="_blank" href="<?php echo $this->Html->url( array('action' => 'index', 'controller'=>'businesses', 'plugin'=> 'administrator')); ?>" >
									<button style="font-size:12px; padding: 2px 5px;" class="btn btn-skype" type="button">View</button>
								</a>
							</td>
						</tr>
						<tr>
							<td><h5 style="color:#333;">- Total Pending Merchants</h5></td>
							<td> : </td>
							<td><h5 style="color: red"><?php echo count(@$pending_biz) ?></h5></td>
							<td style="padding-top:0px;">
								<a target="_blank" href="<?php echo $this->Html->url( array('action' => 'index/pending', 'controller'=>'businesses', 'plugin'=> 'administrator')); ?>"> 
									<button style="font-size:12px; padding: 2px 5px;" class="btn btn-skype" type="button">View</button>
								</a>
							</td>
						</tr>

						<tr>
							<td><h5 style="color:#333;">- Total Deal Created</h5></td>
							<td> : </td>
							<td><h5 style="color: #06F"><?php echo @$total_deal ?></h5></td>
							<td style="padding-top:0px;">
								<!-- <a target="_blank" href="<?php echo $this->Html->url( array('action' => 'index', 'controller'=>'deals')); ?>"> 
									<button style="font-size:12px; padding: 2px 5px;" class="btn btn-skype" type="button">View</button>
								</a> -->
							</td>
						</tr>

						<tr>
							<td><h5 style="color:#333;">- Total Deal Pending</h5></td>
							<td> : </td>
							<td><h5 style="color: red"><?php echo count($pending_deals) ?></h5></td>
							<td style="padding-top:0px;">
							</td>
						</tr>

					</table>
				</div>

				<?php if($pending_deals){ ?>
				<div class="clearfix"></div>
				<legend>Pending Deals: </legend>

				<div class="span12" style="margin-left: 0px;">

					<table class="table-list">
						<tr>
							<th width="60px">Code</th>
							<th width="100px;'">Image</th>
					        <th width="250px">Deal Title</th>
					        <th>Detail</th>
					        <th width="220px">Validity</th>
					        <th width="80px">Status</th>
					        <th width="80px">Action</th>
						</tr>

						<?php foreach( $pending_deals as $k => $deal ){ 

								$index = 0;
					            $split = end(explode("/", $deal['Deal']['image']));
					            $thumb_img =  "img/deals/thumbs/" . $split ;

					            $other_images = $deal['Deal']['other_images'];
					            if( $other_images ){
					                
					                $other_images = json_decode($other_images);
					                foreach( $other_images as $k => $img ){

					                  if( $img != "img/deals/default.jpg" ){
					                    $index ++;
					                    $spt    = end(explode("/", $img ));
					                    $thumb  = "img/deals/thumbs/" . $spt;

					                    $data_images[$deal['Deal']['id']][$index]['img'] = $img;
					                    $data_images[$deal['Deal']['id']][$index]['thumb'] = $thumb;
					                  }
					                }

					            }else if($deal['Deal']['image'] != "img/deals/default.jpg" ) {
					              $data_images[$deal['Deal']['id']][$index]['img']   = $deal['Deal']['image'];
					              $data_images[$deal['Deal']['id']][$index]['thumb']  = $thumb_img;
					            }

					            $selected = @$data_images[$deal['Deal']['id']][array_rand($data_images[$deal['Deal']['id']])];

					            if( $selected ){ 
					                $selected_img   = $selected['img'];
					                $selected_thumb = $selected['thumb']; 
					            }else{
					                $selected_img   = "img/deals/default.jpg" ;
					                $selected_thumb = "img/deals/thumbs/default.jpg" ;
					            }

					            $dealCategories = $deal['Deal']['goods_category'];
					            $dealCategories = json_decode($dealCategories);

					            $cate_display = "";
					            foreach( $dealCategories as $val ){
					                if( isset($goods_category[$val]) ){
					                  $cate_display .= $goods_category[$val] . ", ";
					                }else{
					                  $cate_display .= $val . ", ";
					                }
					            }

					            $cate_display = rtrim($cate_display, ", ");

					            $last_minute = "";

					            if( $deal['Deal']['is_last_minute_deal']){
					                $last_minute = "Yes";
					            }

						?>	

						<tr>
							<td>
								<a 	href="#" data-toggle="modal" class="btn-view-deal"
					              	data-id="<?php echo $deal['Deal']['id'] ?>"
					              	title="View Deal Detail"
					              	data-from="<?php echo date('d-M-Y h:i:s A', strtotime($deal['Deal']['valid_from'])) ?>"
					              	data-to="<?php echo date('d-M-Y h:i:s A', strtotime($deal['Deal']['valid_to'])) ?>"
					              	data-category="<?php echo $cate_display ?>"
					              	data-posted-date="<?php echo date('d-M-Y h:i:s A', strtotime($deal['Deal']['created'])) ?>" >
					              	 <strong><?php echo $deal['Deal']['deal_code'] ?></strong>
					            </a>
							</td>
				            <td>
				              <a href="<?php echo $this->webroot . $selected_img ?>"  rel="shadowbox">
				                <img src="<?php echo $this->webroot . $selected_thumb ?>" alt=""
				                 style="max-width= 70px; max-height:60px; height: auto; padding: 2px; border: 1px solid #CCC;" >
				              </a>
				            </td>
				            <td>
				              <strong>Title :</strong> 
				              	<a href="<?php echo $this->Html->url( array('controller' => 'businesses', 'action' => 'view', $deal['BusinessInfo']['id'], 'deal' )); ?>" 
				              		target="_blank"
				              		title="View Deal in Merchant Profile">
				              		<?php echo urldecode($deal['Deal']['title']) ?><br>
				              	</a>
				              <strong>Category : </strong><?php echo $cate_display ?>
				               <?php if( $last_minute != "" ){ ?>
				                  <br>Is Last Minute Deal : <strong style="color:#06F"><?php echo $last_minute ?></strong>
				              <?php  } ?>
				            </td>
				            <td>
				              <span><strong>Created Date : </strong><?php echo date( 'd-F-Y', strtotime($deal['Deal']['created']) ) ?> AT <?php echo date( 'h:i:s A', strtotime($deal['Deal']['created']) ) ?></span>, By : <?php echo urldecode($deal['CreatedBy']['last_name'] . " " . $deal['CreatedBy']['first_name']) ?><br>

				              <span><strong>Last Updated : </strong><?php echo date( 'd-F-Y', strtotime($deal['Deal']['modified']) ) ?> AT <?php echo date( 'h:i:s A', strtotime($deal['Deal']['modified']) ) ?></span>, By : <?php echo urldecode($deal['UpdatedBy']['last_name'] . " " . $deal['UpdatedBy']['first_name']) ?>
				            </td>
				            <td>
				              From : <?php echo date( 'd-F-Y', strtotime($deal['Deal']['valid_from']) ) ?> AT <?php echo date( 'h:i:s A', strtotime($deal['Deal']['valid_from']) ) ?><br>
				              To : <?php echo date( 'd-F-Y', strtotime($deal['Deal']['valid_to']) ) ?> AT <?php echo date( 'h:i:s A', strtotime($deal['Deal']['valid_to']) ) ?>
				            </td>
				            <td style="text-align: center; vertical-align: top">
				              <?php 
				                if( $deal['Deal']['status'] != 0 && (strtotime($deal['Deal']['valid_to']) < time()) ){
				                  $deal['Deal']['status'] = -1;
				                } 
				              ?>
				              <span class="label label-<?php echo $deal_status[$deal['Deal']['status']]['color'] ?>">
				                <?php echo h($deal_status[$deal['Deal']['status']]['status']); ?>
				              </span>
				            </td>
				            <td class="actions" style="vertical-align: top; text-align:right">
				              <a href="#" data-toggle="modal" class="btn btn-linkedin btn-view-deal"
				              	 data-id="<?php echo $deal['Deal']['id'] ?>"
				              	 title="View Deal Detail"
				              	 data-from="<?php echo date('d-M-Y h:i:s A', strtotime($deal['Deal']['valid_from'])) ?>"
				              	 data-to="<?php echo date('d-M-Y h:i:s A', strtotime($deal['Deal']['valid_to'])) ?>"
				              	 data-category="<?php echo $cate_display ?>"
				              	 data-posted-date="<?php echo date('d-M-Y h:i:s A', strtotime($deal['Deal']['created'])) ?>" >
				              	 <i class="icon icon-eye-open"
				              	 style="padding-right:0px;"></i></a>

				              <a  href="#"
				                  data-bisid =""
				                  data-id="<?php echo $deal['Deal']['id'] ?>"
				                  class="deal_images btn btn-foursquare"
				                  data-action="" 
				                  title="Deal Images">
				                  <i class="icon icon-picture"></i>
				              </a>

				            </td>
				          </tr>

						<?php } ?>

					</table>

				</div>

				<?php } ?>

			</div>
		</div>
	</div>

</div>

<!--Deal Images Modal -->
<div id="deal_images" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true"
  style="width:800px !important; left:40% !important; min-height:300px; max-height: 500px;">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel"><i class="icon-picture"></i> Deal Images</h3>
  </div>
    
    <div class="clearfix"></div>
    <span id="error-msg" style="margin-left:20px; font-size:16px; color:red; display: none">
        
    </span>

  <div class="span9" style="margin-top:20px; 
                            width:775px; 
                            position:relative;
                            max-height: 380px; min-height:180px;
                            overflow-y:scroll" id='deal-image-container'>
        
      <div  class='deal-image' >
            
      </div>

      <div style="width:32px; height:32px; margin-left:48%; margin-top:8%; position:absolute;" id="loading-img">
            <img src="<?php echo $this->webroot . '/img/ajax-loader.gif' ?>" alt="">
      </div>

  </div>

  <div style="clear:both;margin-bottom:20px; margin-left:20px;">
      <button class="btn btn-google cancel" data-dismiss='modal' type="button">Close</button>

  </div>

</div>

<!-- Deal Detail Form -->
<div id="detail-deal-modal" class="modal hide fade" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true"
  style="padding-bottom: 10px !important; width:1024px !important; left:30% !important">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel"><i class="icon-list"></i><font>Deal Detail</font></h3>
  </div>
  
  <div style="max-height:500px; overflow-x: scroll;">
  <form action="<?php echo $this->Html->url( array('controller' => 'Deals', 'action' => 'approve')); ?>" 
      style="padding: 20px; padding-top:10px; padding-bottom: 0px;" method="POST" id="dealDetailForm"
      enctype = "multipart/form-data" >
	<div style="width:100%; min-height: 300px; color: #333 !important;">
		
		<div class="content">
			<div class="span4">
				<table width="100%;">

					<tr>
						<th width="100px;" style="text-align:left; vertical-align:top;">Deal Title</th>
						<th width="20px;" style="vertical-align:top;">:</th>
						<td id="deal-title" style="font-weight:bold; font-size:15px; color: blue; vertical-align:top;"></td>
					</tr>

					<tr>
						<th style="text-align:left; vertical-align:top;">Valid From</th>
						<th style="vertical-align:top;">:</th>
						<td id="deal-valid-from" style="vertical-align:top;"></td>
					</tr>

					<tr>
						<th style="text-align:left; vertical-align:top;">Valid To</th>
						<th style="vertical-align:top;">:</th>
						<td id="deal-valid-to" style="vertical-align:top;"></td>
					</tr>

					<tr>
						<th style="text-align:left; vertical-align:top;">Categories</th>
						<th style="vertical-align:top;">:</th>
						<td id="deal-goods-group" style="vertical-align:top;"></td>
					</tr>

					<tr>
						<td colspan="3" style="padding-top:20px;"></td>
					</tr>

					<tr>
						<th  style="text-align:left">Posted Date</th>
						<th>:</th>
						<td id="deal-posted-date"></td>
					</tr>

					<tr>
						<th style="text-align:left; vertical-align:top;">Status</th>
						<th style="vertical-align:top;">:</th>
						<td id="deal-status" style="vertical-align:top;">
							<span class="label label-warning">Pending</span>
						</td>
					</tr>


					<tr>
						<td colspan="3" style="padding-top:20px;">
							<label for='last-minute-deal' style="float:left; font-weight: bold;">Is Last Minute Deal</label>
							<input type="checkbox" name="last-minute" value="1" id="last-minute-deal" style="float:left; margin-left:10px;" />
						</td>
					</tr>


				</table>
			</div>
			<div class="span8">
				<div class="span3" style="width:290px;">
					<table width="100%;">
						<tr>
							<th width="100px;" style="text-align:left; vertical-align:top;">Description:</th>
						</tr>

						<tr>
							<td id="deal-description">
								<div style="min-height:250px; max-height:300px; overflow-y:scroll;">
									
								</div>
							</td>
						</tr>

					</table>
				</div>

				<div class="span3" style="width:290px;">
					<table width="100%;">
						<tr>
							<th width="100px;" style="text-align:left; vertical-align:top;">Conditions:</th>
						</tr>

						<tr>
							<td id="deal-conditions">
								<div style="min-height:250px; max-height:300px; overflow-y:scroll;">
									
								</div>
							</td>
						</tr>

					</table>
				</div>		

			</div>

			<div class="span11" style="margin-left:0px; width:100%;">
				<legend style="font-size:17px; font-weight:bold;">Related Items</legend>
				<table class="table-list" id="table-item-listing" >
		            <thead>
		            	<th width="80px;">Image</th>
		                <th width="200px;"><?php echo __('Menu Category'); ?></th>
		                <th><?php echo __('Item Name'); ?></th>
		                <th style="text-align:right" width="100px;"><?php echo __('Original Price'); ?></th>
		                <th style="text-align:right" width="100px;"><?php echo __('Discounted Price'); ?></th>
		                <th style="text-align:right" width="100px;"><?php echo __('Available Qty'); ?></th>
		            </thead>    

		            <tbody>
                    	<tr class="related-item-row">
                    		<td class="item-image"></td>
		                    <td class='item-menu' style="font-weight: bold;"></td>
		                    <td class='item-title'></td>
		                    <td style="text-align:right" class='item-price'></td>
		                    <td style="text-align:right" class='item-discount-price'></td>
		                    <td style="text-align:right" class='item-available-qty'></td>
		                </tr>
                    </tbody>

                </table>

			</div>

		</div>

		<div style="width:32px; height:32px; margin-left:48%; margin-top:8%; position:absolute;" id="loading-img">
            <img src="<?php echo $this->webroot . '/img/ajax-loader.gif' ?>" alt="">
      	</div>

	</div>

    <div class="clearfix"></div>

    <div class="submit" style="margin-top: 20px; clear: both; margin-left:20px;">
    	<input type="hidden" value="" id="deal-id" name="deal-id" >
      <button class="btn btn-google cancel" data-dismiss='modal' type="button">Close</button>
      <button class="btn btn-linkedin" type="submit">Approve</button>
      
    </div>
      
  </form>

  </div>

</div>

<!-- Search Filter Dialog -->


<div id="search-transaction-modal" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel"><i class="icon-search"></i> Search Filter</h3>
  </div>

  <!-- Any Info-customization -->
  <form action="<?php echo $this->Html->url(array('action' => 'index')); ?>" 
      	style="padding: 20px;" method="POST" id="transactionSearchForm"
      	enctype = "multipart/form-data" >

    <div style="width:100%;">

		<fieldset>

			<div style="display:block; clear:both">
				<label for="BuyerTransactionPurchaseDate" style="float:left; width:100px; padding-top:5px;">Purchase Date</label>
				<div id="dealValidFromPicker" class="input-append date datetimepicker" style="float:left">
	                <input data-format="dd-MM-yyyy" type="text" 
	                    name="purchaseDateFrom"
	                    style="width: 135px"
	                    value="<?php echo(isset($dateFrom))?date('d-m-Y', strtotime($dateFrom)):"" ?>"
	                    placeholder="From">
	                <span class="add-on">
	                  <i data-time-icon="icon-time" data-date-icon="icon-calendar">
	                  </i>
	                </span>
	            </div>
				<span style="float:left; margin-left:8px; margin-right:7px; padding-top:5px;">TO</span>
				<div id="dealValidFromPicker" class="input-append date datetimepicker" style="float:left">
	                <input data-format="dd-MM-yyyy" type="text" 
	                    name="purchaseDateTo"
	                    style="width: 135px"
	                    value="<?php echo(isset($dateTo))?date('d-m-Y', strtotime($dateTo)):"" ?>"
	                    placeholder="To">
	                <span class="add-on">
	                  <i data-time-icon="icon-time" data-date-icon="icon-calendar">
	                  </i>
	                </span>
	            </div>
				
			</div>

		</fieldset>

    </div>

    <div class="submit" style="margin-top: 40px; clear: both;">
      <input class="btn btn-primary" type="submit" value="Submit Search">
      <button type="button" class="btn btn-linkedin reset-btn">Reset</button>
      <button class="btn btn-google cancel" data-dismiss='modal' type="button">Close</button>
    </div>
      
  </form>

</div> 

<script>

	var div_image = $('.deal-image');

    $('.deal-image').remove();

 	$(document).ready( function(){

		$('.datetimepicker').datetimepicker();
		$('.datetimepicker').find('input').attr('readonly', 'readonly');
		$('.datetimepicker').find('input').css('cursor', 'default');

 		Shadowbox.init();

 		$('.deal_images').click ( function(event){

        event.preventDefault();

        $('#deal-image-container').find('.deal-image').remove();

	        var image_modal     = $('div#deal_images');
	        var deal_id         = $(this).data('id');
	        
	        var getDealImageURL__ = '<?php echo $this->webroot ?>' + 'administrator/Businesses/getDealImagesAjax__/' + deal_id ;
	        // getDealImageURL__ += "/" + deal_id ;

	        image_modal.modal('show');
	        var root = '<?php echo $this->webroot ?>';

	        var loading_img = $('div#loading-img');
	        var time = new Date().getTime();

	        loading_img.show();

	        if( deal_id ){
	            $.ajax({

	                url: getDealImageURL__,
	                type: "POST",
	                data: {},
	                async: false,
	                dataType: 'json',

	                success: function(data){
	                    if( data.status == true ){

	                        setTimeout( function(){
	                            loading_img.hide();

	                            var images = data.deal_images;

	                            $.each( images, function(ind, val){
	                                time += 300;

	                                var img_name = val.split("/");
	                                img_name = img_name[img_name.length - 1];

	                                var img = root + "img/deals/" + img_name;
	                                var thb = root + "img/deals/thumbs/" + img_name;

	                                div_image.attr( 'data-id' , deal_id + "_" + time );
	                                div_image.html('<img alt="" src="' + thb + '">');

	                                $('#deal-image-container').prepend(div_image.clone());
	                            });

	                        }, 1000);
	                    }
	                },
	            
	                error: function(err){
	                    
	                }

	            });
	        }

	    });

		var table_listing  = $('div#detail-deal-modal').find('table#table-item-listing').find('tbody');
    	var item_row_model = table_listing.find('.related-item-row');
    	$('div#detail-deal-modal').find('table#table-item-listing').find('tbody').find('.related-item-row').remove();

		$('a.btn-view-deal').click( function(e){
			e.preventDefault();

			table_listing.html("");

			var me 			= $(this);

			var modal 		= $('div#detail-deal-modal');
			var loading 	= modal.find('#loading-img');
			var div_content = modal.find('.content');

			var biz_title 	= modal.find('h3#myModalLabel').find('font');
			biz_title.text('Deal Detail');
			var deal_id 	= me.attr('data-id');

			modal.find('input#deal-id').val(deal_id);

			var url = '<?php echo $this->webroot ?>' + 'administrator/Deals/getDealDetailAjax__/' + deal_id ;

			loading.show();
			div_content.hide();
			modal.modal('show');

			if( deal_id ){
	            $.ajax({

	                url: url,
	                type: "POST",
	                data: {},
	                async: false,
	                dataType: 'json',

	                success: function(data){
	                	
	                    if( data.status == true ){

	                    	var detail = data.data;

	                    	// Define Element
	                    	var td_deal_title 		= div_content.find('td#deal-title');
	                    	var td_deal_desc  		= div_content.find('td#deal-description').find('div');
	                    	var td_deal_conditions	= div_content.find('td#deal-conditions').find('div');
	                    	var td_deal_valid_from 	= div_content.find('td#deal-valid-from');
	                    	var td_deal_valid_to 	= div_content.find('td#deal-valid-to');
	                    	var td_deal_posted_date = div_content.find('td#deal-posted-date');

	                    	var td_deal_goods_group 		= div_content.find('td#deal-goods-group');

	                    	// Get Data
	                    	var biz_name 	= detail.BusinessInfo.business_name;

	                    	// Set Data
	                    	biz_title.text(biz_name);	
	                    	td_deal_title.text(detail.Deal.title);
	                    	td_deal_valid_from.text(me.attr('data-from'));
	                    	td_deal_valid_to.text(me.attr('data-to'));
	                    	td_deal_posted_date.text(me.attr('data-posted-date'));
	                    	// td_deal_published_date.text(me.attr('data-published-date'));

	                    	// Goods Category
	                    	var goods_cates = me.attr('data-category');
	                    	
	                    	td_deal_goods_group.text(goods_cates);

	                    	td_deal_desc.html(detail.Deal.description);
	                    	td_deal_conditions.html(detail.Deal.deal_condition);

	                    	if( detail.Deal.is_last_minute_deal == 1 ){
	                    		div_content.find('#last-minute-deal').attr('checked','checked');
	                    	}else{
	                    		div_content.find('#last-minute-deal').removeAttr('checked');
	                    	}

	                    	// Related Items
	                    	var getRelatedItemURL__ = '<?php echo $this->webroot ?>' + 'administrator/businesses/getDealRelatedItemsAjax__' ;

						    $.ajax({
					          	type: 'POST',
					          	url: getRelatedItemURL__,
					         	data: { deal_id : deal_id },
					          	dataType: 'json',

					          	success: function( data ){
					          		if( data.status == true && data.result.length > 0 ){
					          			
					          			$.each( data.result , function( ind, val ){

					          				var item_id 			= val.ItemDetail.id;
					          				var discounted_price 	= val.DealItemLink.discount;
					          				var original_price	 	= val.ItemDetail.price;
					          				var av_qty 				= val.DealItemLink.available_qty;

					          				if( val.DealItemLink.original_price != 0 ){
					          					original_price = val.DealItemLink.original_price;
					          				}

					          				
					          				item_row_model.find('.hidden-item-id').val(item_id);
								            item_row_model.find('.hidden-item-discount').val(discounted_price);

								            // Biz Menu
								            var menu_name = val.ItemDetail.MenuDetail.name ;
								            var item_name = val.ItemDetail.title;

								            var thumb_img  	= val.ItemDetail.image;
								            thumb_img 		= thumb_img.replace('/menus/', '/menus/thumbs/');
								            var img = '<img style="max-width:80px; max-height:60px;" src="' + thumb_img + '" />';

								            item_row_model.attr('data-id', item_id);
								            item_row_model.find('.item-image').html(img);
								            item_row_model.find('.item-menu').text(menu_name);
								            item_row_model.find('.item-title').text(item_name);
								            item_row_model.find('.item-price').text("$ " + parseFloat(original_price).toFixed(2));
								            item_row_model.find('.item-discount-price').text("$ " + parseFloat(discounted_price).toFixed(2));

								            item_row_model.find('.item-available-qty').text(parseFloat(av_qty).toFixed(2));

								            item_row_model.find('.remove-selected-item').attr('data-id', item_id);


								            table_listing.append(item_row_model.show().clone());
					          			})
					          		}
					          	},

					          	error: function(err, msg){
					          		console.log(msg);
					          		location.reload();
					          	}
						    })
							
	                    	loading.hide();	                    	
	                    	div_content.show();
	                    	// console.log(data.data);
	                    }
	                },
	            
	                error: function(err){
	                    
	                }

	            });
			}

		});

 	});

	$('#clear-search').click( function(){
		$('input#search-text').val("");
		$('select#status').val('all');

		$('form#frmPurchaseTransaction').submit();
	
	});

	$('#btn-search-transaction').click( function(event){

		event.preventDefault();
		var modal = $('div#search-transaction-modal');
		modal.modal('show');

	});

	$('#btn-search-clear').click( function( event ){
		
		event.preventDefault();
		var modal = $('div#search-transaction-modal');
		$('.reset-btn').click();
		$('form#transactionSearchForm').submit();

	});

	$('.reset-btn').click( function( event ){

		event.preventDefault();
	 	var modal = $('div#search-transaction-modal');
	 	modal.find('input[type="text"]').val("");
	 	modal.find('select#merchant').val("");

	});

</script>