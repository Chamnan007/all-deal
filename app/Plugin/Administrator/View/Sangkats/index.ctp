<?php 
	/********************************************************************************
	    File: Location Listing
	    Author: PHEA RATTANA
	
	    Confidential ABi Technologies property.
	
	  	Changed History:
	  	Date 					Author				Description
	  	2014/01/95 				PHEA RATTANA		Initial
	*********************************************************************************/

	$all_cities = array();

	if( !empty($cities) ){
		foreach( $cities as $key => $city ){
			$all_cities[$city['City']['city_code']] = $city['City']['city_name'];
		}
	}

?>

<div class="citites index">
	<div class="row-fluid">		
		<!-- Pie: Box -->
		<div class="span12">

			<!-- Pie: Top Bar -->
			<div class="top-bar">
				<h3><i class="icon-list"></i> Manage Sangkat</h3>
			</div>
			<!-- / Pie: Top Bar -->

			<!-- Pie: Content -->
			<div class="well">

			<a class="btn-add-sangkat"> 
				<button class="btn btn-linkedin" type="button"><i class="icon-plus"></i> Add New Sangkat</button>
			</a>
			

			<div class="clearfix"></div>
				
				<table class="table-list">
					<thead>
						<tr>
							<th width="300px;"><?php echo $this->Paginator->sort('Location.city_code', 'Province/ City'); ?>&nbsp;</th>
							<th><?php echo $this->Paginator->sort('Location.location_name', 'Location' ); ?>&nbsp;</th>
							<th><?php echo $this->Paginator->sort("Sangkat.name", 'Sangkat Name'); ?>&nbsp;</th>
							<th width="120px;" class="actions"><?php echo __('Actions'); ?></th>
						</tr>
					</thead>
					<tbody>
					<?php foreach ($data as $value): ?>
						<tr>
							
							<td><?php echo h($value['Location']['city_code'] . " - " . $all_cities[$value['Location']['city_code']]); ?>&nbsp;</td>
							<td><?php echo h($value['Location']['location_name']); ?></td>
							<td><?php echo h($value['Sangkat']['name']); ?></td>
							<td class="actions">
								<a data-action="<?php echo $this->Html->url(array('action' => 'edit', $value['Sangkat']['id'])); ?>"
									class="btn-edit-sangkat"
									data-location="<?php echo $value['Location']['id'] ?>"
									data-name="<?php echo $value['Sangkat']['name'] ?>"
									data-city-code="<?php echo $value['Location']['city_code'] ?>"
									data-pro-code=""
									href="#"
									> 
									<button class="btn btn-foursquare" type="button">Edit</button>
								</a>
								<a href="#delete" data-toggle="modal" class="delete" data-id="<?php echo $value['Sangkat']['id'] ?>">
									<button class="btn btn-google" type="button">Delete</button>
								</a>
							</td>
						</tr>
					<?php endforeach; ?>
					</tbody>
				</table>

				<p class="page">
					<?php
					echo $this->Paginator->counter(array(
					'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total')
					));
					?>	
				</p>

				<div class="paging">
				<?php
					echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
					echo $this->Paginator->numbers(array('separator' => ''));
					echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
				?>
				</div>
			</div>
			<!-- / Pie: Content -->

		</div>
		<!-- / Pie -->
		
	</div>

</div>

<div id="delete" class="modal hide fade" tabindex="-1" 
	role="dialog" aria-labelledby="myModalLabel" 
	aria-hidden="true">

	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="myModalLabel">Are You Sure?</h3>
	</div>

	<!-- Any Info-customization -->
	<form action="" 
		  style="padding: 20px;" method="POST" class="form-horizontal" id="deteteFrom">
		
		<h5>Are you sure you want to delete this Sangkat?</h5>
		
		<div class="submit" style="padding-top: 20px; clear: both;">
			<button class="btn btn-google" data-dismiss='modal' type="button">Cancel</button>
		 	<input class="btn btn-primary" type="submit" value="Yes">
		</div>
			
	</form>

</div> 

<div id="add-sangkat-modal" class="modal hide fade" tabindex="-1" 
	role="dialog" aria-labelledby="myModalLabel" 
	aria-hidden="true">

	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="myModalLabel">Add New Sangkat</h3>
	</div>

	<!-- Any Info-customization -->
	<form action="<?php echo $this->Html->url( array('action' => 'add')); ?>" 
		  style="padding: 20px;" method="POST" class="form-horizontal">
		

		<div class="span3">
			<?php
				echo $this->Form->input(	'province_code' ,array(
										   'type' 		=> 'select',
										   'label' 		=> 'Province/ City',
										   'empty' 		=> 'selct an option',
										   'required'	=> 'required',
										   'options'	=> $all_cities
										));

			?>	
		</div>

		<div class="span3">
			<?php
				echo $this->Form->input(	'location_id' ,array(
										   'type' 		=> 'select',
										   'required'	=> 'required',
										   'label' 		=> 'Location',
										   'empty' 		=> 'select a location',
										   'options'	=> array()
										));

			?>	
		</div>

		<div class="span6" style="clear:both; margin-top:10px;">
			<?php
				echo $this->Form->input('name', array(  'type'=>'text', 
															'required'=>'required',
															'label'=>'Sangkat Name', 
															'placeholder'=>'Sangkat Name', 
															"style"=>"width:440px;"));
			?>	
		</div>
		
		<div class="submit" style="padding-top: 20px; clear: both;">
			<button class="btn btn-google" data-dismiss='modal' type="button">Cancel</button>
		 	<input class="btn btn-primary" type="submit" value="Save">
		</div>
			
	</form>

</div> 

<div id="edit-sangkat-modal" class="modal hide fade" tabindex="-1" 
	role="dialog" aria-labelledby="myModalLabel" 
	aria-hidden="true">

	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="myModalLabel">Edit Sangkat Information</h3>
	</div>

	<!-- Any Info-customization -->
	<form action="" style="padding: 20px;" method="POST" class="form-horizontal">
		

		<div class="span3">
			<?php
				echo $this->Form->input(	'province_code' ,array(
										   'type' 		=> 'select',
										   'label' 		=> 'Province/ City',
										   'empty' 		=> 'Select a province',
										   'required'	=> 'required',
										   'id' 		=> 'province-code-edit',
										   'options'	=> $all_cities
										));

			?>	
		</div>

		<div class="span3">
			<?php
				echo $this->Form->input(	'location_id' ,array(
										   'type' 		=> 'select',
										   'required'	=> 'required',
										   'label' 		=> 'Location',
										   'id'			=> 'location-edit',
										   'empty' 		=> 'select a location',
										   'options'	=> array()
										));

			?>	
		</div>

		<div class="span6" style="clear:both; margin-top:10px;">
			<?php
				echo $this->Form->input('name', array(  'type'=>'text', 
															'required'=>'required',
															'label'=>'Sangkat Name', 
															'placeholder'=>'Sangkat Name', 
															'id'=>'sangkat', 
															"style"=>"width:440px;"));
			?>	
		</div>
		
		<div class="submit" style="padding-top: 20px; clear: both;">
			<button class="btn btn-google" data-dismiss='modal' type="button">Cancel</button>
		 	<input class="btn btn-primary" type="submit" value="Save Change">
		</div>
			
	</form>

</div>

<script>

	$("#province_code").change( function (){
		var me = $(this);

		var locationSelect = $("#location_id");

		var city_code = me.val();

		var url = '<?php echo $this->webroot ?>' + 'administrator/locations/get_location_by_city_code/' + city_code ;

		if( city_code != '' ){
			$.ajax({
	            type: 'POST',
	            url: url,
	            data: { city_code : city_code },
	            dataType: 'json',
	            
	            beforeSend: function(){
	                locationSelect.html('<option>Loading...</option>');
	            },
	            success: function (data){
	               	var data = data.data;

	               	locationSelect.html("<option value=''>select a location</option>")
	               	$.each( data, function (ind, val ){
	               		var loc = val.Location;

	               		locationSelect.append("<option value='" + loc.id + "'>" + loc.location_name + "</option>" )
	               	});
	            },

	            error: function ( err ){
	            	console.log(err);
	            }

	        });

		}else{

			locationSelect.html("<option value=''>select an option</option>");

		}

	});
	
	$("#province-code-edit").change( function (e){

		var me = $(this);
		var locationSelect = $("#location-edit");

		var city_code = me.val();

		var url = '<?php echo $this->webroot ?>' + 'administrator/locations/get_location_by_city_code/' + city_code ;

		if( city_code != '' ){
			$.ajax({
	            type: 'POST',
	            url: url,
	            data: { city_code : city_code },
	            dataType: 'json',
	            
	            beforeSend: function(){
	                locationSelect.html('<option>Loading...</option>');
	            },
	            success: function (data){
	               	console.log('success');

	               	var data = data.data;
	               	locationSelect.html("<option value=''>select a city</option>")
	               	$.each( data, function (ind, val ){
	               		var loc = val.Location;

	               		locationSelect.append("<option value='" + loc.id + "'>" + loc.location_name + "</option>" )
	               	});
	            },

	            error: function ( err ){
	            	console.log(err);
	            }

	        });

		}else{

			locationSelect.html("<option value=''>select an option</option>");

		}

	});

	$(".delete").click( function(){

		 var me = $(this),
		 	id = me.data('id');

		 var url = "<?php echo $this->Html->url(array('action' => 'delete')); ?>";

		var action = url + "/" + id;

		 $('#deteteFrom').attr('action', action) ;

	});

	$('a.btn-add-sangkat').click( function( event ){
		event.preventDefault();

		$('div#add-sangkat-modal').modal('show');

	});

	$('.btn-edit-sangkat').click( function( event ){

		event.preventDefault();

		var me 			= $(this),
			action 		= me.attr('data-action'),
			city_code 	= me.attr('data-city-code'),
			location 	= me.attr('data-location'),
			name 		= me.attr('data-name');

		var modal 		= $('div#edit-sangkat-modal'),
			form 		= modal.find('form');

		var locationSelect = $('#location-edit');

		form.find('#province-code-edit').val(city_code);

		var url = '<?php echo $this->webroot ?>' + 'administrator/locations/get_location_by_city_code/' + city_code ;

		if( city_code != '' ){
			$.ajax({
	            type: 'POST',
	            url: url,
	            data: { city_code : city_code },
	            dataType: 'json',
	            
	            beforeSend: function(){
	                locationSelect.html('<option>Loading...</option>');
	            },
	            success: function (data){

	               	var data = data.data;
	               	locationSelect.html("<option value=''>select a city</option>")
	               	$.each( data, function (ind, val ){
	               		var loc = val.Location;
	               		var selected = "";
	               		if( location == loc.id ){
	               			selected = " selected='selected'";
	               		}
	               		locationSelect.append("<option value='" + loc.id + "'"+ selected +">" + loc.location_name + "</option>" )
	               	});
	            },

	            error: function ( err ){
	            	console.log(err);
	            }

	        });

		}else{

			locationSelect.html("<option value=''>select an option</option>");

		}

		form.find('#sangkat').val(name);
		form.attr('action', action);

		modal.modal('show');

	})


</script>
