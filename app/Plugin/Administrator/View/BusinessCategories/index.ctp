<?php 
  /********************************************************************************
      File: Business Category Listing
      Author: PHEA RATTANA
  
      Confidential ABi Technologies property.
  
      Changed History:
      Date          Author        Description
      2014/01/11        PHEA RATTANA    Initial

  *********************************************************************************/

  // var_dump($data);
  
 ?>
<div class="businessCategories index">
  <div class="row-fluid">   
    <!-- Pie: Box -->
    <div class="span12">

      <!-- Pie: Top Bar -->
      <div class="top-bar">
        <h3><i class="icon-list"></i> Manage Category</h3>
      </div>
      <!-- / Pie: Top Bar -->

      <!-- Pie: Content -->
      <div class="well">

      <a href="#"
        id="add-new-category"> 
        <button class="btn btn-linkedin" type="button"><i class="icon-plus"></i> Add New Category</button>
      </a>

      <div class="clearfix"></div>
        
        <table class="table-list">
          <thead>
            <tr>
              <th width="20">N<sup>o</sup></th>
              <th><?php echo $this->Paginator->sort('category'); ?></th>
              <!-- <th><?php echo $this->Paginator->sort('parent_category'); ?></th> -->
              <th width="130px;" class="actions"><?php echo __('Actions'); ?></th>
            </tr>
          </thead>
          <tbody>
          <?php 
              $index = 1;
          ?>
          <?php foreach ($data as $businessCategory): ?>
            <tr>
              <td style="text-align:center"><?php echo $index ?></td>
              <td <?php echo($businessCategory['BusinessCategory']['parent_id'] == 0)?" style='padding-left:10px;'":"" ?>><?php echo h($businessCategory['BusinessCategory']['category']); ?>&nbsp;</td>
                
              <td class="actions">
                <a href="#"
                     data-action="<?php echo $this->Html->url(array('action' => 'edit', $businessCategory['BusinessCategory']['id'])); ?>"                     
                     class="edit-btn"
                     data-category="<?php echo $businessCategory['BusinessCategory']['category'] ?>"> 
                  <button class="btn btn-foursquare" type="button">Edit</button>
                </a> 

                <a href="#" data-id="<?php echo $businessCategory['BusinessCategory']['id'] ?>" data-toggle="modal" class="delete-category"> 
                  <button class="btn btn-google"  type="button">Delete</button>
                </a>

              </td>
            </tr>

            <?php 
              $childs = $businessCategory['ChildCategory'] ;
              foreach ( $childs as $key => $cate ){
            ?>

              <tr>
                <td style="padding-left:40px;"><?php echo h($cate['category']); ?>&nbsp;</td>
                  
                <td class="actions">

                  <a href="#"
                     data-action="<?php echo $this->Html->url(array('action' => 'edit', $cate['id'])); ?>"                     
                     class="edit-btn"
                     data-category="<?php echo $cate['category'] ?>"> 
                    <button class="btn btn-foursquare" type="button">Edit</button>
                  </a>

                  <a href="#" data-id="<?php echo $cate['id'] ?>"  data-toggle="modal" class="delete-category"> 
                    <button class="btn btn-google" type="button">Delete</button>
                  </a>

                </td>
              </tr>

            <?php } 

                $index ++;
            ?>

          <?php endforeach; ?>
          </tbody>
        </table>

      </div>
      <!-- / Pie: Content -->

    </div>
    <!-- / Pie -->
    
  </div>

</div>

<div id="delete-category" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true"
  style="z-index:999999">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel"><i class="icon-exclamation"></i> Are You Sure ?</h3>
  </div>
  
  <form action="<?php echo $this->Html->url(array('action' => 'delete')); ?>" method="post">
    <div class="span6" style="padding-top: 10px; padding-bottom: 20px;">
      
      <h5 id="pass-alert">Are you sure you want to delete this category?</h5>
      
      <button class="btn btn-google cancel" data-dismiss='modal' type="button">Cancel</button>
      <input type="submit" class="btn btn-primary" value="Yes" id="yes-delete" >
    </div>
  </form>

</div> 

<div id="add-new-category-modal" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true"
  style="z-index:999999">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel"><i class="icon-plus"></i> Add New Category</h3>
  </div>
  
  <form action="<?php echo $this->Html->url(array('action' => 'add')); ?>" method="post">
    <div class="span6" style="padding-top: 10px; padding-bottom: 20px;">
      
      <?php
            echo $this->Form->input('category', array(  'placeholder'=>'Merchant Category',
                                    'type' => 'text',
                                  'style' => "width:100%;" ) );

          ?>  

      <input type="hidden" name="data[parent_category]" value="0" />
      
      <button class="btn btn-google cancel" data-dismiss='modal' type="button">Cancel</button>
      <input type="submit" class="btn btn-primary" value="Save" >
    </div>
  </form>

</div> 


<div id="edit-category-modal" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true"
  style="z-index:999999">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel"><i class="icon-plus"></i> Edit Category</h3>
  </div>
  
  <form action="" method="post">
    <div class="span6" style="padding-top: 10px; padding-bottom: 20px;">
      
      <?php
            echo $this->Form->input('category', array(  'placeholder'=>'Merchant Category',
                                    'type' => 'text',
                                  'style' => "width:100%;" ) );

          ?>  

      <input type="hidden" name="data[parent_category]" value="0" />
      
      <button class="btn btn-google cancel" data-dismiss='modal' type="button">Cancel</button>
      <input type="submit" class="btn btn-primary" value="Save" >
    </div>
  </form>

</div> 

<script type="text/javascript">

      var modal = $('div#delete-category');
      var form  = modal.find('form');
      var action = form.attr('action');

    $('a.delete-category').click(function(e){
        e.preventDefault();

        var me = $(this),
            id = me.attr('data-id');

        action = action + "/" + id;

        form.attr('action', action );

        modal.modal('show');

    });

    $('#add-new-category').click( function(event){
        event.preventDefault();

        $('div#add-new-category-modal').modal('show');
    });

    $('.edit-btn').click(function(event){
        event.preventDefault();

        var me        = $(this),
            category  = me.attr('data-category'),
            action    = me.attr('data-action') ;

        var modal     = $('div#edit-category-modal'),
            form      = modal.find('form');

        form.find('input#category').val(category);
        form.attr('action', action);

        modal.modal('show');

    })

</script>

