<?php

/********************************************************************************
    File: Add Business Category
    Author: PHEA RATTANA

    Confidential ABi Technologies property.

  	Changed History:
  	Date 					Author				Description
  	2014/01/06 				PHEA RATTANA		Initial
*********************************************************************************/

$parentCate = array( '0'=>'Main Category');

if( !empty($parentCategories) ){
	foreach( $parentCategories as $ke => $cate ){
		$parentCate[$cate['BusinessCategory']['id']] = $cate['BusinessCategory']['category'];
	}
}

// var_dump($parentCate);

?>

<div class="businessCategories form">
	<div class="row-fluid">		
		<!-- Pie: Box -->
		<div class="span12">

			<!-- Pie: Top Bar -->
			<div class="top-bar">
				<h3><i class="icon-list"></i> Manage Category</h3>
			</div>
			<!-- / Pie: Top Bar -->

			<!-- Pie: Content -->
			<div class="well">

			<a href="<?php echo $this->Html->url( array('action' => 'index')); ?>"> 
				<button class="btn btn-linkedin" type="button"><i class="icon-list-alt"></i> Category List</button>
			</a>
			

			<div class="clearfix"></div>
				
				<?php echo $this->Form->create('BusinessCategory' ); ?>
					<fieldset style="margin-bottom:150px;">

						<legend><?php echo __('Add New Category'); ?></legend>

						<div class="span4">
							
							<?php
								echo $this->Form->input('category', array(	'placeholder'=>'Merchant Category',
																		  	'type' => 'text',
																			'style' => "width:300px;" ) );

							?>	

						</div>

						<div class="span3">
							<?php

								echo $this->Form->input('parent_id', array(	'type'=>'select', 
																			'label' => 'Parent Category',
																			'options' => $parentCate ) );

							?>	
						</div>

						<div class="span3" style="padding-top:19px;">
							<?php echo $this->Form->submit(__('Save'), array('class' => 'btn btn-primary') ); ?>	
						</div>	
						
					</fieldset>
			

			</div>
			<!-- / Pie: Content -->

		</div>
		<!-- / Pie -->
		
	</div>

</div>
