<?php 
	/********************************************************************************
	    File: Goods Category Listing
	    Author: PHEA RATTANA
	
	    Confidential ABi Technologies property.
	
	  	Changed History:
	  	Date 					Author				Description
	  	2014/01/05 				PHEA RATTANA		Initial
	*********************************************************************************/
	
 ?>
<div class="provinces index">
	<div class="row-fluid">		
		<!-- Pie: Box -->
		<div class="span12">

			<!-- Pie: Top Bar -->
			<div class="top-bar">
				<h3><i class="icon-list"></i> Manage Deal Categories</h3>
			</div>
			<!-- / Pie: Top Bar -->

			<!-- Pie: Content -->
			<div class="well">

				<a href="#"
					class="btn-add-category"> 
					<button class="btn btn-linkedin" type="button"><i class="icon-plus"></i> Add New Deal Category</button>
				</a>
				

				<div class="clearfix"></div>
				
				<table class="table-list">
					<thead>
						<tr>
							<!--th><?php echo $this->Paginator->sort('id'); ?></th-->
							<th width="20px">N<sup>o</sup></th>
							<th><?php echo $this->Paginator->sort('category', 'Deal Category'); ?></th>
							<th width="120px;" class="actions"><?php echo __('Actions'); ?></th>
						</tr>
					</thead>
					<tbody>
					<?php $index = 1; ?>
					<?php foreach ($data as $goodsCategory): ?>
						<tr>
							<td style="text-align:center"><?php echo $index ?></td>
							<td><?php echo h($goodsCategory['GoodsCategory']['category']); ?>&nbsp;</td>
							<td class="actions">
								
								<a 	href="#"
									class="btn-edit-category"
									data-category="<?php echo $goodsCategory['GoodsCategory']['category'] ?>"
									data-action="<?php echo $this->Html->url(array('action' => 'edit', $goodsCategory['GoodsCategory']['id'])); ?>"> 
									<button class="btn btn-foursquare" type="button">Edit</button>
								</a>
								
								<a href="#delete" data-toggle="modal" class="delete" data-id="<?php echo $goodsCategory['GoodsCategory']['id'] ?>">
									<button class="btn btn-google" type="button">Delete</button>
								</a>

							</td>
						</tr>
					<?php $index++; ?>
					<?php endforeach; ?>
					</tbody>
				</table>

				<p class="page">
					<?php
					echo $this->Paginator->counter(array(
					'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total')
					));
					?>	
				</p>

				<div class="paging">
				<?php
					echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
					echo $this->Paginator->numbers(array('separator' => ''));
					echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
				?>
				</div>
			</div>
			<!-- / Pie: Content -->

		</div>
		<!-- / Pie -->
		
	</div>

</div>

<div id="delete" class="modal hide fade" tabindex="-1" 
	role="dialog" aria-labelledby="myModalLabel" 
	aria-hidden="true">

	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="myModalLabel">Are You Sure?</h3>
	</div>

	<!-- Any Info-customization -->
	<form action="" 
		  style="padding: 20px;" method="POST" class="form-horizontal" id="deteteFrom">
		
		<h5>Are you sure you want to delete this category?</h5>
		
		<div class="submit" style="padding-top: 20px; clear: both;">
			<button class="btn btn-google" data-dismiss='modal' type="button">Cancel</button>
		 	<input class="btn btn-primary" type="submit" value="Yes">
		</div>
			
	</form>

</div> 

<div id="add-category-modal" class="modal hide fade" tabindex="-1" 
	role="dialog" aria-labelledby="myModalLabel" 
	aria-hidden="true">

	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="myModalLabel">Add New Category</h3>
	</div>

	<!-- Any Info-customization -->
	<form action="<?php echo $this->Html->url( array('action' => 'add')); ?>" 
		  style="padding: 20px;" method="POST" class="form-horizontal">
		
		<fieldset>
				
			<?php
				echo $this->Form->input('category', array('placeholder'=>'Category Name',
															'type' => 'text',
															'label' => 'Category Name',
															'required' => 'required',
															'style' => "width:480px;"));
			?>
			
		</fieldset>
		
		<div class="submit" style="padding-top: 20px; clear: both;">
			<button class="btn btn-google" data-dismiss='modal' type="button">Cancel</button>
		 	<input class="btn btn-primary" type="submit" value="Save">
		</div>
			
	</form>

</div> 


<div id="edit-category-modal" class="modal hide fade" tabindex="-1" 
	role="dialog" aria-labelledby="myModalLabel" 
	aria-hidden="true">

	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="myModalLabel">Edit Category</h3>
	</div>

	<!-- Any Info-customization -->
	<form action="#" 
		  style="padding: 20px;" method="POST" class="form-horizontal">
		
		<fieldset>
				
			<?php
				echo $this->Form->input('category', array('placeholder'=>'Category Name',
															'type' => 'text',
															'label' => 'Category Name',
															'required' => 'required',
															'id' => 'category-name',
															'style' => "width:480px;"));
			?>
			
		</fieldset>
		
		<div class="submit" style="padding-top: 20px; clear: both;">
			<button class="btn btn-google" data-dismiss='modal' type="button">Cancel</button>
		 	<input class="btn btn-primary" type="submit" value="Save Change">
		</div>
			
	</form>

</div> 


<script>

	$(".delete").click( function(){
		 var me = $(this),
		 	id = me.data('id');

		 var url = "<?php echo $this->Html->url(array('action' => 'delete')); ?>";

		var action = url + "/" + id;

		 $('#deteteFrom').attr('action', action) ;

	});

	$('.btn-add-category').click( function( event ){
		event.preventDefault();

		var modal = $('div#add-category-modal');

		modal.modal('show');
	});

	$('.btn-edit-category').click(function( event ){
		event.preventDefault();

		var me 			= $(this),
			cate_name	= me.attr('data-category'),
			action 		= me.attr('data-action');

		var modal 		= $('div#edit-category-modal'),
			form 		= modal.find('form');

		form.find('#category-name').val(cate_name);
		form.attr('action', action);

		modal.modal('show');
	})

</script>
