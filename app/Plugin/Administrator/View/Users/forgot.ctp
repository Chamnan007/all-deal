<?php 
  /********************************************************************************
      File: Login and Forgot password Form
      Author: PHEA RATTANA
  
      Confidential ABi Technologies property.
  
      Changed History:
      Date                Author            Description
      2014/04/20          PHEA RATTANA      Initial
      2014/06/12          RATTANA           Add Forgot Password
  *********************************************************************************/

  
 ?>

<style type="text/css">
    body { padding-top: 50px; }

    #flashMessage{
        margin-top: 20px;
        margin-bottom: 10px;
    }

</style>

<div class="container">  

    <form class="form-signin form-horizontal" method="POST" action="<?php echo $this->webroot . 'administrator/forgot' ?>">

        <div style="margin: 0 auto; width: 100px; height: 100px; margin-bottom: 20px;">
            <a href="<?php echo Router::url('/', true) ?>">
              <img src="<?php echo Router::url('/img/logo/admin_logo.png', true) ?>" alt="">
            </a>
        </div>

        <div class="top-bar">
          <h3>Forget Password</h3>
        </div>

        <div class="well no-padding">

            <?php echo $this->Session->flash(); ?>

          <div class="control-group">
            <label class="control-label" for="inputName"><i class="icon-user"></i></label>
            <div class="controls">
              <input type="email" id="email" name="data[User][email]" placeholder="Email" required> 
            </div>
          </div>
            
            <div style="padding:15px; font-style: italic; color:red">
                <label for="">Please enter your email, and new password will be sent to this email.</label>
            </div>

            <div style="padding: 0 20px 20px 20px; overflow: hidden">
              <button class="btn btn-primary" type="submit" style="float: right;">Submit</button>
              <a href="<?php echo $this->webroot . 'administrator/login' ?>" style="color: green; float:left">&#8594; Login</a>
            </div>

        </div>
    </form>

 <div style="text-align: center; color: #666">All Right Reserved. Copyright © 2014 - All Deal</div>

</div> 