<?php 
	/********************************************************************************
	    File: Detail Information User
	    Author: PHEA RATTANA
	
	    Confidential ABi Technologies property.
	
	  	Changed History:
	  	Date 					Author				Description
	  	2014/01/05 				PHEA RATTANA		Initial
	*********************************************************************************/
	

	$province_name = array();

	if( !empty($provinces) ){
		foreach( $provinces as $key => $pro ){
			$province_name[$pro['Province']['province_code']] = $pro['Province']['province_name'];
		}
	}


	$city_name = array();

	if( !empty($cities) ){
		foreach( $cities as $key => $value ){
			$city_name[$value['City']['city_code']] = $value['City']['city_name'];
			// $pro_code[] = $value['Province']['province_code'];
		}
	}

	// var_dump($province_name); var_dump($city_name);

?>

<style>
	td{
		font-size: 13px;
		padding: 5px;
	}
	.title {
		width: 160px;
		font-weight: bold;
	}
	
	.col {
		width: 20px;
	}

	.equalto, .required{
		color: red;
		font-size: 12px !important;
		list-style-type: none;
	}

</style>

<div class="users form">

	<div class="row-fluid">		
		<!-- Pie: Box -->
		<div class="span12">

			<!-- Pie: Top Bar -->
			<div class="top-bar">
				<h3><i class="icon-list"></i> Manage User</h3>
			</div>
			<!-- / Pie: Top Bar -->

			<!-- Pie: Content -->
			<div class="well">

				<a href="<?php echo $this->Html->url( array('action' => 'index')); ?>"> 
					<button class="btn btn-linkedin" type="button"><i class="icon-list-alt"></i> User List</button>
				</a>

				<a href="<?php echo $this->Html->url( array('action' => 'edit', $user['User']['id'])); ?>"> 
					<button class="btn btn-skype" type="button"><i class="icon-list-alt"></i> Edit Information</button>
				</a>

				<a href="#reset_password"  data-toggle="modal"> 
					<button class="btn btn-skype" type="button">Reset Password</button>
				</a>
				

				<div class="clearfix"></div>
				
				<table style="margin-left: 30px;">

					<tr>
						<td class='title'>Name</td>
						<td class="col">:</td>
						<td><?php echo h($user['User']['last_name'] . " " . $user['User']['first_name']); ?></td>
					</tr>

					<tr>
						<td class='title'>Gender</td>
						<td class="col">:</td>
						<td><?php echo h($user['User']['gender']); ?></td>
					</tr>

					<tr>
						<td class='title'>Date of Birth</td>
						<td class="col">:</td>
						<td><?php echo date("d F Y", strtotime($user['User']['dob'])); ?></td>
					</tr>

					<tr>
						<td class='title'>Address</td>
						<td class="col">:</td>
						<td>
							<?php 
								echo ($user['User']['street'] == NULL)?"":$user['User']['street'] . ", " ; 
								echo h($city_name[$user['User']['city_code']]) . ", ";
								echo h($province_name[$user['User']['province_code']]) ;
							?>
						</td>
					</tr>

					<tr>
						<td class='title'>Phone</td>
						<td class="col">:</td>
						<td><?php echo h($user['User']['phone']); ?></td>
					</tr>

					<tr>
						<td class='title'>Email</td>
						<td class="col">:</td>
						<td><?php echo h($user['User']['email']); ?></td>
					</tr>

					<tr>
						<td class='title'>Position</td>
						<td class="col">:</td>
						<td><?php echo h($user['User']['position']); ?></td>
					</tr>

					<tr>
						<td class='title'>Access Level</td>
						<td class="col">:</td>
						<td><?php echo h($access_level[$user['User']['access_level']]); ?></td>
					</tr>

					<tr>
						<td class='title'>Registered Date</td>
						<td class="col">:</td>
						<td><?php echo date("d-M-Y | h:i A", strtotime($user['User']['registered_date'])); ?></td>
					</tr>

					<tr>
						<td class='title'>Status</td>
						<td class="col">:</td>
						<td>
							<span class="label label-<?php echo $status[$user['User']['status']]['color'] ?>">
								<?php echo h($status[$user['User']['status']]['status']); ?>
							</span>
						</td>
					</tr>
					<?php if( $user['User']['status'] == '-1' ): ?>

						<tr>
							<td class='title'>Inactive Date</td>
							<td class="col">:</td>
							<td><?php echo date("d-M-Y | h:i A", strtotime($user['User']['inactive_date'])); ?></td>
						</tr>

					<?php endif ?>

				</table>	

			</div>
			<!-- / Pie: Content -->

		</div>
		<!-- / Pie -->
		
	</div>

</div>


<div id="reset_password" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="myModalLabel"><i class="icon-cog"></i> Reset Password</h3>
	</div>

	<!-- Any Info-customization -->
	
	<?php echo $this->Form->create('User', array('data-validate'=>'parsley', 'action' => 'change_password/' . $user['User']['id'], 'id' => 'UserChangePassword'  )) ; ?>

	<div class="well center" style="padding-left:170px;">	
		<?php 
			echo $this->Form->input('id', array('value' => $user['User']['id'],'type'=> 'hidden') );
			echo $this->Form->input('password', array('placeholder' => 'New Password','label'=>"New Password", 'required'=>'required', 'maxLength' => 30 ));
			echo $this->Form->input('confirm_password', array('placeholder' => 'Confirm Password', 'type'=>'password', 'data-equalto' => '#UserPassword', 'label'=>'Confirm New Password', 'required'=>'required' ));
		?>

		 <div class="submit">
			<button class="btn btn-google" data-dismiss='modal' type="button">Cancel</button>
		 	<input class="btn btn-primary" type="button" value="Save Change" id="change_password">
		 </div>

		

	</div>

</div>



<div id="pass-alert" class="modal hide fade" tabindex="-1" 
	role="dialog" aria-labelledby="myModalLabel" 
	aria-hidden="true"
	style="z-index:999999">

	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="myModalLabel"><i class="icon-exclamation"></i> Warning !</h3>
	</div>

	<div class="span6" style="padding-top: 10px; padding-bottom: 20px;">
		
		<h5 id="pass-alert"></h5>

		<button class="btn btn-google cancel" data-dismiss='modal' type="button">Close</button>
	</div>

</div> 

<a href="#pass-alert" data-toggle="modal" id="pass-alert"></a>

<script type="text/javascript">

    $(document).ready(function(){

    	$("#UserConfirmPassword").blur( function (){
	       	return $(this).parsley( 'validate');
    	});

    	$("#change_password").click( function (){

    		var pass = $("#UserPassword").val().length;

			if( pass < 6 ){
				$("h5#pass-alert").text("Password must be at least 6 characters.");
				$("a#pass-alert").click();
				return false;
			}

			$("#UserChangePassword").submit();

    	})

	});


</script>