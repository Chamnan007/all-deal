<?php 

	/********************************************************************************
	    File: Users Listing
	    Author: PHEA RATTANA
	
	    Confidential ABi Technologies property.
	
	  	Changed History:
	  	Date 					Author				Description
	  	2014/01/05 				PHEA RATTANA		Initial
	*********************************************************************************/

	
 ?>

<style>
  table, tr, td{
     vertical-align: top; 
  }
</style>

<div class="users index">
	<div class="row-fluid">		
		<!-- Pie: Box -->
		<div class="span12">

			<!-- Pie: Top Bar -->
			<div class="top-bar">
				<h3><i class="icon-list"></i> Manage Users</h3>
			</div>
			<!-- / Pie: Top Bar -->

			<!-- Pie: Content -->
			<div class="well">

			<a href="<?php echo $this->Html->url( array('action' => 'add')); ?>"> 
				<button class="btn btn-linkedin" type="button"><i class="icon-plus"></i> Add New User</button>
			</a>
			

			<div class="clearfix"></div>
				
				<table class="table-list">
					<thead>
						<tr>
							<th width="150px;"><?php echo $this->Paginator->sort('last_name', 'Name'); ?></th>
							<th width="100px;"><?php echo $this->Paginator->sort('gender'); ?></th>
							<th width="150px;"><?php echo $this->Paginator->sort('position'); ?></th>
							<th><?php echo $this->Paginator->sort('contact_info'); ?></th>
							<th width="150px;"><?php echo $this->Paginator->sort('access_level'); ?></th>
							<th><?php echo $this->Paginator->sort('status'); ?></th>
							<th width="110px;" class="actions"><?php echo __('Actions'); ?></th>
						</tr>
					</thead>
					<tbody>
					<?php foreach ($data as $user): ?>
						<tr>
							<td><?php echo h($user['User']['last_name'] . " " . $user['User']['first_name']); ?>&nbsp;</td>
							<td><?php echo h($user['User']['gender']); ?>&nbsp;</td>
							<td><?php echo h($user['User']['position']); ?>&nbsp;</td>
							<td>
								<?php echo h("Phone: " . $user['User']['phone']); ?>&nbsp;<br>
								<?php echo h("Email: " . $user['User']['email']); ?>&nbsp;
							</td>
							<td><?php echo h($access_level[$user['User']['access_level']]); ?>&nbsp;</td>
							<td style="text-align: center;">
								<span class="label label-<?php echo $status[$user['User']['status']]['color'] ?>"><?php echo h($status[$user['User']['status']]['status']); ?></span>
							</td>
								

							<td class="actions">
								<a href="<?php echo $this->Html->url(array('action' => 'view', $user['User']['id'])); ?>"> 
									<button class="btn btn-primary" type="button">Detail</button>
								</a>
								<a href="<?php echo $this->Html->url(array('action' => 'edit', $user['User']['id'])); ?>"> 
									<button class="btn btn-foursquare" type="button">Edit</button>
								</a>

								<!-- <a href="<?php echo $this->Html->url(array('action' => 'delete', $user['User']['id'])); ?>"> 
									<button class="btn" type="button">Delete</button>
								</a> -->
							</td>
						</tr>
					<?php endforeach; ?>
					</tbody>
				</table>

				<p class="page">
					<?php
					echo $this->Paginator->counter(array(
					'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total')
					));
					?>	
				</p>

				<div class="paging">
				<?php
					echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
					echo $this->Paginator->numbers(array('separator' => ''));
					echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
				?>
				</div>
			</div>
			<!-- / Pie: Content -->

		</div>
		<!-- / Pie -->
		
	</div>

</div>