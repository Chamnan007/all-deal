<?php 

	/********************************************************************************
	    File: Buyers Listing
	    Author: PHEA RATTANA
	
	    Confidential ABi Technologies property.
	
	  	Changed History:
	  	Date 					Author				Description
	  	2015/01/May 			PHEA RATTANA		Initial
	*********************************************************************************/

	
 ?>
<style>
  table, tr, td{
     vertical-align: top; 
  }
</style>

<div class="users index">
	<div class="row-fluid">		
		<!-- Pie: Box -->
		<div class="span12">
			
			<form action="" method="POST" id="frmBuyerList">

				<!-- Pie: Top Bar -->
				<div class="top-bar">
					<h3><i class="icon-list"></i> Payment History</h3>
				</div>
				<!-- / Pie: Top Bar -->

				<!-- Pie: Content -->
				<div class="well">

					<a id="btn-search-transaction" href="#" class="btn btn-linkedin" style="float:left; margin-right:10px;">
						<i class="icon icon-search"></i> Search History</a>

					<a id="btn-search-clear" href="#" class="btn btn-google" style="float:left"><i class="icon icon-trash"></i> Clear Search</a>

					<a 	href="<?php echo $this->Html->url(array('controller'=>'SystemRevenues', 'action'=>'makepayment', 'plugin'=> 'administrator')); ?>" 
						class="btn btn-linkedin" style="float:right; margin-right:10px;"><i class="icon icon-plus"></i> Make Payment</a>

					<div class="clearfix"></div>
					<?php if( $historyStates['payDateFrom'] && $historyStates['payDateTo']): ?>
						<h5>
							Period : <?php echo date('d-F-Y', strtotime($historyStates['payDateFrom'])) . ' to ' ?>
							<?php echo date('d-F-Y', strtotime($historyStates['payDateTo'])) ?>
						</h5>
					<?php endif; ?>
					
					<table class="table-list">
						
						<thead>
							<tr>
								<th width="20px;">N<sup>o</sup></th>
								<th width="100px;"><?php echo $this->Paginator->sort('pay_date','Paid Date'); ?></th>
								<th><?php echo $this->Paginator->sort('BusinessDetail.business_name','Merchant'); ?></th>
								<th width="200px">Deals</th>
								<th width="200px;"><?php echo $this->Paginator->sort('payment_method'); ?></th>
								<th width="150px;"><?php echo $this->Paginator->sort('pay_amount'); ?></th>
								<th width="50px;">Action</th>
							</tr>
						</thead>

						<tbody>

						<?php if( $data ){ ?>
							<?php foreach ($data as $key => $value ):
									@$count++;
							?>
								<tr>
									<td style="text-align:center"><?php echo $count ?></td>
									<td><?php echo date('d-F-Y', strtotime($value['PaymentHistory']['pay_date'])) ?></td>
									<td>
										<a href="<?php echo $this->Html->url(array('controller'=>'businesses', 'action'=>'view', 'plugin'=> 'administrator', $value['BusinessDetail']['id'])); ?>" target="_blank">
											<strong><?php echo $value['BusinessDetail']['business_name'] ?></strong>
										</a>
									</td>
									<?php 
										// $desc 		= $value['PaymentHistory']['description'];
										// $short_desc = (strlen($desc)>50)?substr($desc, 0, 50) . "...":$desc; 

										$deal_ids = explode(",", $value['PaymentHistory']['deal_ids']);
										
									?>
									<td>
									<?php foreach( $deal_ids as $ind => $id ): ?>
										<a href="<?php echo $this->Html->url(array('controller'=>'deals', 'action'=>'view', 'plugin'=> 'administrator', $id)); ?>" target="_blank"
											title="click to view deal" >
											<strong>
												<?php echo $dealInfo[$id]['Deal']['deal_code']; ?>
											</strong> 
										</a>
										<?php echo (isset($deal_ids[$ind+1]))?", ":""; ?>
									<?php endforeach; ?>
									</td>

									<td>
										Via : <strong><?php echo $PayMethods[$value['PaymentHistory']['payment_method']] ?></strong>
										<?php if( $value['PaymentHistory']['payment_method'] == 1 ){ ?>
											<br>From : <?php echo $value['PaymentHistory']['from_bank'] . " - " . $value['PaymentHistory']['to_bank'] ?>
										<?php } ?>
									</td>
									<td style="text-align:right">
										<strong>
											$ <?php echo $this->MyHtml->formatNumber($value['PaymentHistory']['pay_amount']); ?>
										</strong>
									</td>
									<td style="text-align:center">
										<a href="<?php echo $this->Html->url(array('controller'=>'SystemRevenues', 'action'=>'view', $value['PaymentHistory']['id'] , 'plugin'=> 'administrator')); ?>"
											title="click to view detail"
											class="btn btn-linkedin">
											<i class="icon icon-eye-open"></i>
										</a>
									</td>
								</tr>
							<?php endforeach; ?>
						<?php }else{ ?>
								<tr>
									<td colspan="10"><i>No Transaction Found !</i></td>
								</tr>
						<?php } ?>
						</tbody>
					</table>

					<p class="page">
						<?php
						echo $this->Paginator->counter(array(
						'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total')
						));
						?>	
					</p>

					<div class="paging">
					<?php
						echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
						echo $this->Paginator->numbers(array('separator' => ''));
						echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
					?>
					</div>
				</div>
				<!-- / Pie: Content -->

			</form>

		</div>
		<!-- / Pie -->
		
	</div>

</div>


<div id="search-transaction-modal" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel"><i class="icon-search"></i> Search Transaction</h3>
  </div>

  <!-- Any Info-customization -->
  <form action="<?php echo $this->Html->url(array('action' => 'payment')); ?>" 
      	style="padding: 20px;" method="POST" id="transactionSearchForm"
      	enctype = "multipart/form-data" >

    <div style="width:100%;">

		<fieldset>

			<div style="display:block; clear:both">
				
				<label style="float:left; width:100px; padding-top:5px;">Merchant</label>
				
				<select name="business_id" id="merchant" style="width:410px;">
					<option value="">Any Merchant</option>
					<?php foreach( $bizData as $k => $v ){ 
							$selected = ( @$historyStates['business_id'] && $historyStates['business_id'] == $v['Business']['id'] )?" selected='selected'":"" ;
					?>
						<option value="<?php echo $v['Business']['id'] ?>" <?php echo $selected ?>><?php echo $v['Business']['business_name'] ?></option>
					<?php } ?>
				</select>

			</div>

			<div style="display:block; clear:both">
				
				<label style="float:left; width:100px; padding-top:5px;">Payment Method</label>
				
				<select name="payment_method" id="payment-method" style="width:410px;">
					<option value="">Any Method</option>
					<?php foreach( $PayMethods as $k => $v ){ 
							$selected = ( @$historyStates['payment_method'] && $historyStates['payment_method'] == $k )?" selected='selected'":"" ;
					?>
						<option value="<?php echo $k ?>" <?php echo $selected ?>><?php echo $v ?></option>
					<?php } ?>
				</select>

			</div>

			<div style="display:block; clear:both">
				<label for="BuyerTransactionPurchaseDate" style="float:left; width:100px; padding-top:5px;">Payment Date</label>
				<div id="payDateFrom" class="input-append date datetimepicker" style="float:left">
	                <input data-format="dd-MM-yyyy" type="text" 
	                    name="payDateFrom"
	                    style="width: 135px"
	                    id="payDateFrom"
	                    value="<?php echo date('d-m-Y', strtotime($historyStates['payDateFrom'])) ?>"
	                    placeholder="From"
	                    required>
	                <span class="add-on">
	                  <i data-time-icon="icon-time" data-date-icon="icon-calendar">
	                  </i>
	                </span>
	            </div>
				<span style="float:left; margin-left:8px; margin-right:7px; padding-top:5px;">TO</span>
				<div id="payDateTo" class="input-append date datetimepicker" style="float:left">
	                <input data-format="dd-MM-yyyy" type="text" 
	                    name="payDateTo"
	                    style="width: 135px"
	                    value="<?php echo date('d-m-Y', strtotime($historyStates['payDateTo'])) ?>"
	                    placeholder="To"
	                    id="payDateTo"
	                    required>
	                <span class="add-on">
	                  <i data-time-icon="icon-time" data-date-icon="icon-calendar">
	                  </i>
	                </span>
	            </div>
				
			</div>

		</fieldset>

    </div>

    <div class="submit" style="margin-top: 40px; clear: both;">
      <input class="btn btn-primary" type="submit" value="Submit Search">
      <button type="button" class="btn btn-linkedin reset-btn">Reset</button>
      <button class="btn btn-google cancel" data-dismiss='modal' type="button">Close</button>
    </div>
      
  </form>

</div> 

<script>

	$(document).ready( function(){
		$('.datetimepicker').datetimepicker();
		$('.datetimepicker').find('input').attr('readonly', 'readonly');
		$('.datetimepicker').find('input').css('cursor', 'default');
	})

	$('#clear-search').click( function(){
		$('input#search-text').val("");
		$('select#status').val('all');

		$('form#frmPurchaseTransaction').submit();
	})

	$('#btn-search-transaction').click( function(event){

		event.preventDefault();
		var modal = $('div#search-transaction-modal');
		modal.modal('show');

	});

	$('#btn-search-clear').click( function( event ){

		event.preventDefault();

		var modal = $('div#search-transaction-modal');

		$('.reset-btn').click();
		$('form#transactionSearchForm').submit();
	});

	$('.reset-btn').click( function(){

	 	var modal = $('div#search-transaction-modal');

	 	var from = "<?php echo date('01-m-Y') ?>";
	 	var to = "<?php echo date('t-m-Y') ?>";

	 	modal.find('input[type="text"]').val("");
	 	modal.find('select').val("");
	 	modal.find('input#payDateFrom').val(from);
	 	modal.find('input#payDateTo').val(to);

	})

</script>