<?php 

	/********************************************************************************
	    File: Buyers Listing
	    Author: PHEA RATTANA
	
	    Confidential ABi Technologies property.
	
	  	Changed History:
	  	Date 					Author				Description
	  	2015/01/May 			PHEA RATTANA		Initial
	*********************************************************************************/

	$province_name = array();
	foreach( $provinces as $k => $prov ){
		$province_name[$prov['Province']['province_code']] = $prov['Province']['province_name'];
	}

	$city_name 	= array();
	foreach( $cities as $k => $val ){
		$city_name[$val['City']['city_code']] = $val['City']['city_name'];
	}

 ?>
<style>
  table, tr, td{
     vertical-align: top; 
  }
</style>

<div class="users index">
	<div class="row-fluid">		
		<!-- Pie: Box -->
		<div class="span12">
			
			<form action="" method="POST" id="frmBuyerList" >

				<!-- Pie: Top Bar -->
				<div class="top-bar">
					<h3><i class="icon-list"></i> Make Payment</h3>
				</div>
				<!-- / Pie: Top Bar -->

				<!-- Pie: Content -->
				<div class="well" style="min-height: 300px; padding-bottom: 100px;">

					<a id="btn-search-merchant" href="#" class="btn btn-linkedin" style="float:left; margin-right:10px;">
						<i class="icon icon-search"></i> Search Merchant
					</a>

					<a id="btn-search-clear" href="#" class="btn btn-google" style="float:left">
						<i class="icon icon-trash"></i> Clear Search</a>

					<a 	href="<?php echo $this->Html->url(array('controller'=>'SystemRevenues', 'action'=>'payment', 'plugin'=> 'administrator')); ?>" 
						class="btn btn-linkedin" 
						style="float:right; margin-right:10px;">
						<i class="icon icon-list"></i> View Payment History</a>

					<div class="clearfix"></div>

					<h5>Total Account Payable : 
						<span style="color: #06F">
							<?php echo($total_account_payable)?"$ " . $this->MyHtml->formatNumber($total_account_payable):"-" ?>
						</span>
					</h5>
					
					<table class="table-list">
						<thead>
							<tr>
								<th width="30px">N<sup>o</sup></th>
								<th width="60px"><?php echo $this->Paginator->sort('Business.bsuiness_code', 'Merchant Code'); ?></th>
								<th width="250px;"><?php echo $this->Paginator->sort('Business.business_name', 'Merchant Name'); ?></th>
								<th width="150px"><?php echo $this->Paginator->sort('Business.business_main_category', 'Category'); ?></th>
								<th>Contact Information</th>
								<th width="100px;"><?php echo $this->Paginator->sort('Business.ending_balance','Total Balance'); ?></th>
								<th width="100px" title="Payable Balance of finished deals">Payable Balance</th>
								<th width="90px;">Action</th>
							</tr>
						</thead>
						<tbody>

						<?php if( $data ){ ?>
							<?php foreach ($data as $key => $value ): 

									@$count++;

									$address = ($value['Business']['street'])?$value['Business']['street'] . ", ":"";
									$address .= $value['Business']['location'] . ", " . $province_name[$value['Business']['province']];

									$balance = $value['Business']['ending_balance'];

									$pay = false;
									if( $balance > 0 ){
										$balance = "$ " . $this->MyHtml->formatNumber($value['Business']['ending_balance']);
									}else{
										$balance = "-";
									}

									// Payable
									$payable = 0;
									$deal_ids 	= "";
									$deal_codes = "";

									if( isset($payableData[$value['Business']['id']]) ){
										$payable_list  = $payableData[$value['Business']['id']];
										foreach($payable_list as $k => $pay ){
											$payable += $pay[0]['total'];
											$deal_ids .= $k . ",";
											$deal_codes .= $pay['DealInfo']['deal_code']. ", " ;
										}
									}

									$deal_ids 	= rtrim($deal_ids,',');
									$deal_codes = rtrim($deal_codes,', ');

									$pay_amount = $payable;

									if( $payable ){
										$pay = true;
										$payable = "$ " . $this->MyHtml->formatNumber($payable);
									}else{
										$payable = "-";
									}
																	
							?>
								<tr>
									<td style="text-align:center"><?php echo $count ?></td>
									<td>
										<a href="<?php echo $this->Html->url(array('controller'=>'businesses', 'action'=>'view', $value['Business']['id'] , 'plugin'=> 'administrator')); ?>" target="_blank"
											title="click to view detail">
											<strong><?php echo $value['Business']['business_code'] ?></strong>
										</a>
									</td>
									<td>
										<a href="<?php echo $this->Html->url(array('controller'=>'businesses', 'action'=>'view', $value['Business']['id'] , 'plugin'=> 'administrator')); ?>" target="_blank"
											title="click to view detail">
											<strong><?php echo $value['Business']['business_name'] ?></strong>
										</a>
									</td>
									<td><?php echo $value['MainCategory']['category'] ?></td>
									<td><?php echo $address ?></td>									
									<td style="text-align:right;">
										<strong>
										<?php echo $balance ?>
										</strong>
									</td>

									<td style="text-align:right">
										<strong><?php echo $payable ?></strong>
									</td>

									<td style="text-align:right;">
										
										<a href="<?php echo $this->Html->url(array('controller'=>'businesses', 'action'=>'view', $value['Business']['id'] , 'plugin'=> 'administrator', 'revenue' )); ?>" target="_blank"
											title="click to view detail"
											class="btn btn-linkedin">
											<i class="icon icon-eye-open"></i>
										</a>

										<?php if($pay){ ?>
											<a 	href="#" 
												data-action="<?php echo $this->Html->url(array('action' => 'savePayment', $value['Business']['id'])); ?>"
												data-balance="<?php echo $pay_amount ?>"
												data-balance-text="<?php echo $payable ?>"
												data-deal-ids="<?php echo $deal_ids ?>"
												data-deal-codes = "<?php echo $deal_codes ?>"
												data-merchant="<?php echo $value['Business']['business_name'] ?>"
												class="btn btn-skype btn-pay-now" title="Click to Pay">
												Pay Now
											</a>
										<?php } ?>

									</td>
								</tr>
							<?php endforeach; ?>
						<?php }else{ ?>
								<tr>
									<td colspan="10"><i>No Merchant(s) Found !</i></td>
								</tr>
						<?php } ?>
						</tbody>
					</table>

					<!-- <p class="page">
						<?php
						echo $this->Paginator->counter(array(
						'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total')
						));
						?>	
					</p>

					<div class="paging">
					<?php
						echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
						echo $this->Paginator->numbers(array('separator' => ''));
						echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
					?>
					</div> -->
				</div>
				<!-- / Pie: Content -->

			</form>

		</div>
		<!-- / Pie -->
		
	</div>

</div>


<div id="search-merchant-modal" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel"><i class="icon-search"></i> Search Merchant</h3>
  </div>

  <!-- Any Info-customization -->
  <form action="<?php echo $this->Html->url(array('action' => 'makepayment')); ?>" 
      	style="padding: 20px;" method="POST" id="search-merchant"
      	enctype = "multipart/form-data" >

    <div style="width:100%;">

		<fieldset>

			<div style="display:block; clear:both">
				
				<label style="float:left; width:100px; padding-top:5px;">Merchant Code :</label>				
				<input 	type="text" name="merchant-code" placeholder="Merchant Code" 
						style="width: 386px;"
						value="<?php echo strtoupper(@$merchantStates['merchant-code']) ?>" />

			</div>

			<div style="display:block; clear:both">
				
				<label style="float:left; width:100px; padding-top:5px;">Merchant Name :</label>				
				<input 	type="text" name="merchant-text" placeholder="Merchant Name" 
						style="width: 386px;"
						value="<?php echo @$merchantStates['merchant-text'] ?>" />

			</div>

			<div style="display:block; clear:both">
				<label style="float:left; width:100px; padding-top:5px;">Categoryt :</label>

				<select name="category" style="width:408px;">
					<option value="">Any category</option>
					<?php foreach( $MerchantCategories as $k => $cate ){ 
							$selected = (isset($merchantStates['merchant-category']) && $merchantStates['merchant-category'] == $cate['BusinessCategory']['id'])?" selected='selected'":"";
					?>
						<option value="<?php echo $cate['BusinessCategory']['id'] ?>"
								<?php echo $selected ?>><?php echo $cate['BusinessCategory']['category'] ?></option>
					<?php } ?>
				</select>
			</div>

		</fieldset>

    </div>

    <div class="submit" style="margin-top: 40px; clear: both;">
      <input class="btn btn-primary" type="submit" value="Submit Search">
      <button type="button" class="btn btn-linkedin reset-btn">Reset</button>
      <button class="btn btn-google cancel" data-dismiss='modal' type="button">Close</button>
    </div>
      
  </form>

</div> 


<div id="add-payment-modal" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel"><i class="icon-plus"></i> Make Payment</h3>
  </div>

  <!-- Any Info-customization -->
  <form action="" 
      	style="padding: 20px;" method="POST" id="search-merchant"
      	enctype = "multipart/form-data" >

    <div style="width:100%;">

		<fieldset>

			<div style="display:block; clear:both">				
				<label style="float:left; width:100px;">Merchant Name</label>				
				<label style="float:left; font-weight:bold;" id="merchant-name"></label>
			</div>

			<div style="display:block; clear:both; padding-top:10px;">				
				<label style="float:left; width:100px;">Payable Balance</label>				
				<label style="float:left; font-weight:bold; color: blue;" id="balance"></label>
				<input type="hidden" id="hidden-balance" />
				<input type="hidden" id="hidden-deal-ids" name="deal_ids"/>
			</div>

			<div style="display:block; clear:both; padding-top:10px;">				
				<label style="float:left; width:100px;">Pay to Deals</label>				
				<label style="float:left; font-weight:bold;" id="deal-codes"></label>
			</div>

			<div style="display:block; clear:both; padding-top:10px;">				
				<label 	style="float:left; width:100px; padding-top:5px;">Pay Date</label>				
				<div id="payDate" class="input-append date datetimepicker" style="float:left">
	                <input data-format="dd-MM-yyyy" type="text" 
	                    name="pay_date"
	                    value="<?php echo date('d-m-Y') ?>"
	                    required="required">
	                <span class="add-on">
	                  <i data-time-icon="icon-time" data-date-icon="icon-calendar">
	                  </i>
	                </span>
	            </div>
			</div>

			<div style="display:block; clear:both">				
				<label 	style="float:left; width:100px; padding-top:5px;">Pay Amount</label>				
				<input type="text" name="pay_amount" value="" required="required" maxlength="11"
						placeholder="Pay Amount"
						id="pay_amount"
						readonly="readonly" 
                        onkeypress="return numericAndDotOnly(this)"
						style="width:230px; font-weight:bold;">	                
	            
			</div>

			<div style="display:block; clear:both">				
				<label 	style="float:left; width:100px; padding-top:5px;">Payment Method</label>
				<select name="payment_method" style="width:253px;" id="pay_method">
					<?php foreach( $PayMethods as $k => $val ){ ?>
						<option value="<?php echo $k ?>"><?php echo $val ?></option>
					<?php } ?>
				</select>	            
			</div>

			<div id="bank-transfer">
				<label 	style="float:left; width:100px; padding-top:5px;">From Bank</label>
				<input type="text" name="from_bank" placeholder="From Bank" style="float:left; width:150px;" id="from"/>
				<label 	style="float:left; width:30px; padding-top:5px; margin-left:15px;">TO</label>
				<input type="text" name="to_bank" placeholder="To Bank" style="float:left; width:150px;" id="to"/>
			</div>

			<div style="display:block; clear:both">				
				<label 	style="float:left; width:100px; padding-top:5px;">Description</label>				
				<textarea style="width:370px;" name="description" placeholder="Payment Description" ></textarea>	            
			</div>

		</fieldset>

    </div>

    <div class="submit" style="margin-top: 40px; clear: both;">
      <input class="btn btn-primary" type="submit" value="Submit Payment" id="save-payment">
      <button class="btn btn-google cancel" data-dismiss='modal' type="button">Cancel</button>
    </div>
      
  </form>

</div> 


<div id="warning-modal" class="modal hide fade" tabindex="-1" 
	role="dialog" aria-labelledby="myModalLabel" 
	aria-hidden="true">

	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="myModalLabel"><i class="icon icon-exclaimation"></i> Warning !</h3>
	</div>

	<!-- Any Info-customization -->
	<form action="" style="padding: 20px;" method="POST" class="form-horizontal" >
		
		<div id="msg">
			
		</div>
		
		<div class="submit" style="padding-top: 20px; clear: both;">
			<button class="btn btn-google" data-dismiss='modal' type="button">Close</button>
		</div>
			
	</form>

</div> 

<script>
	function numericAndDotOnly(elementRef) {
        
		var keyCodeEntered = (event.which) ? event.which : (window.event.keyCode) ? window.event.keyCode : -1;

		if ((keyCodeEntered >= 48) && (keyCodeEntered <= 57)) {
		return true;
		}   
		// '.' decimal point...
		else if (keyCodeEntered == 46) {
			// Allow only 1 decimal point ('.')...
			if ((elementRef.value) && (elementRef.value.indexOf('.') >= 0))
			  return false;
			else
			  return true;
		}

      	return false;
  	}


	$(document).ready( function(){
		$('.datetimepicker').datetimepicker();
		$('.datetimepicker').find('input').attr('readonly', 'readonly');
		$('.datetimepicker').find('input').css('cursor', 'default');
	})

	$('#clear-search').click( function(){
		$('input#search-text').val("");
		$('select#status').val('all');

		$('form#frmPurchaseTransaction').submit();
	})

	$('#btn-search-merchant').click( function(event){

		event.preventDefault();
		var modal = $('div#search-merchant-modal');
		modal.modal('show');

	});

	$('#btn-search-clear').click( function(){

		var modal = $('div#search-merchant-modal');

		$('.reset-btn').click();
		modal.find('form').submit();
	});

	$('.reset-btn').click( function(){

	 	var modal = $('div#search-merchant-modal');

	 	modal.find('input[type="text"]').val("");
	 	modal.find('select').val("");

	});

	$('a.btn-pay-now').click( function(event){
		event.preventDefault();

		var modal 		= $('div#add-payment-modal');
		var me 	  		= $(this),
			action 		= me.attr('data-action'),
			biz_name 	= me.attr('data-merchant'),
			balance 	= me.attr('data-balance'),
			balance_text= me.attr('data-balance-text'),
			deal_ids 	= me.attr('data-deal-ids'),
			deal_codes 	= me.attr('data-deal-codes') ;

		var form 		= modal.find('form');

		form.attr('action', action);
		form.find('#merchant-name').text(": " + biz_name);
		form.find('#balance').text(": " + balance_text);
		form.find('input#hidden-balance').val(balance);
		form.find('input#pay_amount').val(balance);
		form.find('input#hidden-deal-ids').val(deal_ids);
		form.find('#deal-codes').text(deal_codes);

		modal.modal('show');

	});

	$('#save-payment').click( function(event){
		event.preventDefault();

		var form 		= $(this).closest('form');
		var balance 	= form.find('input#hidden-balance').val();

		var pay_amount 	= form.find('input#pay_amount').val();
		var pay_method 	= form.find('select#pay_method').val();

		var warning_modal = $('div#warning-modal');
		var msg_block 	  = warning_modal.find('div#msg');
		msg_block.html("");

		var err = new Array();
		if( pay_amount == '' || pay_amount == 0 ){
			err.push('Please enter Amount to pay !');
		}

		if( parseFloat(pay_amount) > parseFloat(balance) ){
			err.push("Invalid Pay Amount ! You cannot pay more than balance !");
		}

		var bank_tranfer_from 	= $('div#bank-transfer').find('input#from').val();
		var bank_tranfer_to 	= $('div#bank-transfer').find('input#to').val();

		if( pay_method == 1 ){ // 1: bank transferred

			bank_tranfer_from 	= bank_tranfer_from.trim();
			bank_tranfer_to 	= bank_tranfer_to.trim();

			if( bank_tranfer_from == "" || bank_tranfer_to == "" ){
				err.push("Please enter transferred banks !");
			}

		}

		if( err.length > 0 ){

			$.each( err, function( ind, val ){	
				msg_block.append('<p>- '+ val +'</p>');
			})

			warning_modal.modal('show');
			return false;
		}

		form.submit();

	});

	$('select#pay_method').change( function(){
		var val = $(this).val();

		var div_banK = $('div#bank-transfer');

		if( val == 1 ){
			div_banK.find('input[type="text"]').attr('required', true);
			div_banK.fadeIn();
		}else{
			div_banK.find('input[type="text"]').removeAttr('required');
			div_banK.fadeOut();
		}

	}).trigger('change');

</script>