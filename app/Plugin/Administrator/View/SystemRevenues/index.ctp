<?php 

	/********************************************************************************
	    File: Buyers Listing
	    Author: PHEA RATTANA
	
	    Confidential ABi Technologies property.
	
	  	Changed History:
	  	Date 					Author				Description
	  	2015/01/May 			PHEA RATTANA		Initial
	*********************************************************************************/

?>

<style>

  table, tr, td{
     vertical-align: top; 
  }

</style>

<div class="users index">
	<div class="row-fluid">		
		<!-- Pie: Box -->
		<div class="span12">
			
			<form action="" method="POST" id="frmBuyerList" >

				<!-- Pie: Top Bar -->
				<div class="top-bar">
					<h3><i class="icon-list"></i> Revenues</h3>
				</div>
				<!-- / Pie: Top Bar -->

				<!-- Pie: Content -->
				<div class="well">

					<div class="pull-left">

						<table>
							<tr>
								<td>
									<h5 style="margin-bottom: 0px;"><?php echo $income_string; ?></h5>
								</td>
								<td width="20px;">
									<h5 style="margin-bottom: 0px;">:</h5>
								</td>
								<td>
									<h5 style="color: #06F; margin-bottom: 0px;">
										<?php echo ($total_amount)?"$ " . $this->MyHtml->formatNumber($total_amount):"0.00" ?>
									</h5>
								</td>
							</tr>
							<tr>
								<td>
									<h5><?php echo $period_string; ?></h5>
								</td>
								<td width="20px;">
									<h5>:</h5>
								</td>
								<td>
									<h5 style="color: #06F">
										<?php echo ($total_revenue)?"$ " . $this->MyHtml->formatNumber($total_revenue):"0.00" ?>
									</h5>
								</td>
							</tr>
						</table>

					</div>

					<a id="btn-search-clear" href="#" class="btn btn-google" style="float:right">
						<i class="icon icon-trash"></i> Clear Search
					</a>

					<a id="btn-search-transaction" href="#" class="btn btn-linkedin" style="float:right; margin-right:10px;">
						<i class="icon icon-search"></i> Search Transaction
					</a>
					
					<table class="table-list">
						<thead>
							
							<tr>
								
								<th width="30px;">N<sup>o</sup></th>
								<th width="160px;"><?php echo $this->Paginator->sort('created', 'Date'); ?></th>
								<th><?php echo $this->Paginator->sort('BusinessDetail.business_name','Merchant'); ?></th>
								<th width="100px;">
									<?php echo $this->Paginator->sort('TransactionInfo.DealInfo.deal_code','Deal Code'); ?>
								</th>
								<th width="150px;"><?php echo $this->Paginator->sort('transaction_amount', 'Transaction Amount' ); ?></th>
								<th width="100px;"><?php echo $this->Paginator->sort('commission_percent', 'Commission' ); ?></th>
								<th width="150px;"><?php echo $this->Paginator->sort('total_amount', 'Revenue' ); ?></th>
								<th width="30px">Action</th>
							</tr>

						</thead>
						<tbody>

						<?php if( $data ){ ?>						
							<?php foreach ($data as $key => $value ): 
									@$count++;

									$dealInfo = $value['TransactionInfo']['DealInfo'];
							?>
								<tr>
									<td style="text-align:center"><?php echo $count ?></td>
									<td>
										<?php echo date('d-F-Y h:i:s A', strtotime($value['SystemRevenue']['created'])) ?>
									</td>

									<td>
										<a href="<?php echo $this->Html->url(array('action' => 'view', 'controller' => 'businesses', $value['BusinessDetail']['id'] )); ?>" target="_blank">
											<strong>
												<?php echo $value['BusinessDetail']['business_name'] ?>
											</strong>
										</a>
									</td>

									<td>
										<a href="<?php echo $this->Html->url(array('action' => 'view', 'controller' => 'deals', $dealInfo['id'] )); ?>" target="_blank">
											<strong>
												<?php echo $dealInfo['deal_code'] ?>
											</strong>
										</a>										
									</td>

									<td style="text-align: right;">
										<?php echo ($value['SystemRevenue']['transaction_amount'])?"$ " . $this->MyHtml->formatNumber($value['SystemRevenue']['transaction_amount']):"-" ?>
									</td>
									<td style="text-align: right;">
									<?php echo $this->MyHtml->formatNumber($value['SystemRevenue']['commission_percent']) . " %" ?>
									</td>

									<td style="text-align:right">
										<?php echo ($value['SystemRevenue']['total_amount'])?"$ " . $this->MyHtml->formatNumber($value['SystemRevenue']['total_amount'], 3):"-" ?>
									</td>

									<td style="text-align: center;">
										<a href="<?php echo $this->Html->url(array('action' => 'view', 'controller' => 'BuyerTransactions', $value['SystemRevenue']['transaction_id'] )); ?>" class="btn btn-linkedin" 
											target="_blank"
											title="View Transaction Detail" >
											<i class="icon icon-eye-open"></i>
										</a>
									</td>

								</tr>
							<?php endforeach; ?>
						<?php }else{ ?>
								<tr>
									<td colspan="10"><i>No Transaction Found !</i></td>
								</tr>
						<?php } ?>
						</tbody>
					</table>

					<p class="page">
						<?php
						echo $this->Paginator->counter(array(
						'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total')
						));
						?>	
					</p>

					<div class="paging">
					<?php
						echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
						echo $this->Paginator->numbers(array('separator' => ''));
						echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
					?>
					</div>
				</div>
				<!-- / Pie: Content -->

			</form>

		</div>
		<!-- / Pie -->
		
	</div>

</div>


<div id="search-transaction-modal" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel"><i class="icon-search"></i> Search Transaction</h3>
  </div>

  <!-- Any Info-customization -->
  <form action="<?php echo $this->Html->url(array('action' => 'index')); ?>" 
      	style="padding: 20px;" method="POST" id="transactionSearchForm"
      	enctype = "multipart/form-data" >

    <div style="width:100%;">

		<fieldset>

			<div style="display:block; clear:both">
				
				<label style="float:left; width:100px; padding-top:5px;">Merchant</label>
				
				<select name="business_id" id="merchant" style="width:410px;">
					<option value="">All</option>
					<?php foreach( $bizData as $k => $v ){ 
							$selected = ( @$states['business_id'] && $states['business_id'] == $v['Business']['id'] )?" selected='selected'":"" ;
					?>
						<option value="<?php echo $v['Business']['id'] ?>" <?php echo $selected ?>><?php echo $v['Business']['business_name'] ?></option>
					<?php } ?>
				</select>

			</div>

			<div style="display:block; clear:both">
				<label for="BuyerTransactionPurchaseDate" style="float:left; width:100px; padding-top:5px;">Purchase Date</label>
				<div id="dealValidFromPicker" class="input-append date datetimepicker" style="float:left">
	                <input data-format="dd-MM-yyyy" type="text" 
	                    name="purchaseDateFrom"
	                    style="width: 135px"
	                    value="<?php echo @$states['purchaseDateFrom'] ?>"
	                    placeholder="From">
	                <span class="add-on">
	                  <i data-time-icon="icon-time" data-date-icon="icon-calendar">
	                  </i>
	                </span>
	            </div>
				<span style="float:left; margin-left:8px; margin-right:7px; padding-top:5px;">TO</span>
				<div id="dealValidFromPicker" class="input-append date datetimepicker" style="float:left">
	                <input data-format="dd-MM-yyyy" type="text" 
	                    name="purchaseDateTo"
	                    style="width: 135px"
	                    value="<?php echo @$states['purchaseDateTo'] ?>"
	                    placeholder="To">
	                <span class="add-on">
	                  <i data-time-icon="icon-time" data-date-icon="icon-calendar">
	                  </i>
	                </span>
	            </div>
				
			</div>

		</fieldset>

    </div>

    <div class="submit" style="margin-top: 40px; clear: both;">
      <input class="btn btn-primary" type="submit" value="Submit Search">
      <button type="button" class="btn btn-linkedin reset-btn">Reset</button>
      <button class="btn btn-google cancel" data-dismiss='modal' type="button">Close</button>
    </div>
      
  </form>

</div> 

<script>

	$(document).ready( function(){
		$('.datetimepicker').datetimepicker();
		$('.datetimepicker').find('input').attr('readonly', 'readonly');
		$('.datetimepicker').find('input').css('cursor', 'default');
	})

	$('#clear-search').click( function(){
		$('input#search-text').val("");
		$('select#status').val('all');

		$('form#frmPurchaseTransaction').submit();
	})

	$('#btn-search-transaction').click( function(event){

		event.preventDefault();

		var modal = $('div#search-transaction-modal');

		modal.modal('show');

	});

	$('#btn-search-clear').click( function( event ){
		event.preventDefault();
		var modal = $('div#search-transaction-modal');

		$('.reset-btn').click();
		$('form#transactionSearchForm').submit();
	});

	$('.reset-btn').click( function( event ){

		event.preventDefault();

	 	var modal = $('div#search-transaction-modal');

	 	modal.find('input[type="text"]').val("");
	 	modal.find('select#merchant').val("");

	})

</script>