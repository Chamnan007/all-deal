<?php 

	/********************************************************************************
	    File: Buyers Listing
	    Author: PHEA RATTANA
	
	    Confidential ABi Technologies property.
	
	  	Changed History:
	  	Date 					Author				Description
	  	2015/01/May 			PHEA RATTANA		Initial
	*********************************************************************************/

?>

<style>
  table, tr, td{
     vertical-align: top; 
  }
</style>

<div class="users index">
	<div class="row-fluid">		
		<!-- Pie: Box -->
		<div class="span12">
			
			<form action="" method="POST" id="frmBuyerList">

				<!-- Pie: Top Bar -->
				<div class="top-bar">
					<h3><i class="icon-list"></i> Payment History</h3>
				</div>
				<!-- / Pie: Top Bar -->

				<!-- Pie: Content -->
				<div class="well">

					<a 	href="<?php echo $this->Html->url(array('controller'=>'SystemRevenues', 'action'=>'payment', 'plugin'=> 'administrator')); ?>" 
						class="btn btn-linkedin" style="float:left;">
							<i class="icon icon-list"></i> Payment List</a>

					<div class="clearfix"></div>

					<div class="span6" style="margin-left:0px;">
						<legend>Payment Information</legend>
						<table width="100%">
							<tr>
								<td width="100px;">Pay Date</td>
								<td width="20px;">:</td>
								<td><?php echo date('d-F-Y', strtotime($data['PaymentHistory']['pay_date'])) ?></td>
							</tr>

							<tr>
								<td>Pay Amount</td>
								<td>:</td>
								<td style="color: #06F; font-size: 15px;">
									<strong><?php echo "$ " . $this->MyHtml->formatNumber($data['PaymentHistory']['pay_amount']); ?></strong>
								</td>
							</tr>

							<tr>
								<td>Payment Method</td>
								<td>:</td>
								<td><?php echo $PayMethods[$data['PaymentHistory']['payment_method']] ?></td>
							</tr>

							<?php if( $data['PaymentHistory']['payment_method'] === 1): ?>
								<tr>
									<td>Transferred Bank</td>
									<td>:</td>
									<td>
										<?php echo $data['PaymentHistory']['from_bank'] . " - " . $data['PaymentHistory']['to_bank'] ?>
									</td>
								</tr>
							<?php endif ?>

							<tr>
								<td>Note</td>
								<td>:</td>
								<td><?php echo $data['PaymentHistory']['description'] ?></td>
							</tr>

							<tr>
								<td style="padding-top: 20px;"></td>
							</tr>

							<tr>
								<td>Added Date</td>
								<td>:</td>
								<td><?php echo date('d-F-Y h:i:s', strtotime($data['PaymentHistory']['created'])) ?></td>
							</tr>

							<tr>
								<td>Added By</td>
								<td>:</td>
								<td><?php echo $data['CreatedBy']['last_name'] . " " . $data['CreatedBy']['first_name'] ?></td>
							</tr>

						</table>
					</div>	

					<div class="span6">
						<legend>Merchant Information</legend>

						<table width="100%">
							<tr>
								<td width="100px">Code</td>
								<td width="20px;">:</td>
								<td>
									<a href="<?php echo $this->Html->url(array('controller'=>'businesses', 'action'=>'view', 'plugin'=> 'administrator', $data['BusinessDetail']['id'])); ?>" target="_blank" >
										<strong><?php echo $data['BusinessDetail']['business_code'] ?></strong>
									</a>
								</td>
							</tr>
							<tr>
								<td width="100px">Name</td>
								<td width="20px;">:</td>
								<td>
									<a href="<?php echo $this->Html->url(array('controller'=>'businesses', 'action'=>'view', 'plugin'=> 'administrator', $data['BusinessDetail']['id'])); ?>" target="_blank" >
										<strong><?php echo $data['BusinessDetail']['business_name'] ?></strong>
									</a>
								</td>
							</tr>

							<tr>
								<td>Contact Number</td>
								<td>:</td>
								<td>
									<?php 
										echo $data['BusinessDetail']['phone1'];
										echo ($data['BusinessDetail']['phone2'])?" / " . $data['BusinessDetail']['phone2']:"";
									?>
								</td>
							</tr>

							<tr>
								<td>Email</td>
								<td>:</td>
								<td><?php echo $data['BusinessDetail']['email'] ?></td>
							</tr>

							<tr>
								<td>Address</td>
								<td>:</td>
								<td>
									<?php 
										echo ($data['BusinessDetail']['street'])?$data['BusinessDetail']['street'] . ", ":""; 
										echo $data['BusinessDetail']['location'] . ", ";
										echo $data['BusinessDetail']['city'];
									?>
								</td>
							</tr>

						</table>

					</div>

					<div class="clearfix"></div>
					<div class="span12" style="margin-left: 0px;">
						<legend>Deals Information</legend>

						<table class="table-list">
							<tr>
								<th width="30px;">N<sup>o</sup></th>
								<th width="100px">Deal Code</th>
								<th>Deal Name</th>
							</tr>

							<?php foreach( $dealData as $k => $deal ): ?>
								<tr>
									<td style="text-align:center"><?php echo $k + 1 ?></td>
									<td>
										<a 	href="<?php echo $this->Html->url(array('controller'=>'deals', 'action'=>'view', 'plugin'=> 'administrator', $deal['Deal']['id'])); ?>"
											target="_blank">
											<strong><?php echo $deal['Deal']['deal_code'] ?></strong>
										</a>
									</td>
									<td>
										<a 	href="<?php echo $this->Html->url(array('controller'=>'deals', 'action'=>'view', 'plugin'=> 'administrator', $deal['Deal']['id'])); ?>"
											target="_blank">
											<?php echo $deal['Deal']['title'] ?>
										</a>
									</td>
								</tr>
							<?php endforeach; ?>
						</table>
					</div>
					
				</div>
				<!-- / Pie: Content -->

			</form>

		</div>
		<!-- / Pie -->
		
	</div>

</div>


<script>

</script>