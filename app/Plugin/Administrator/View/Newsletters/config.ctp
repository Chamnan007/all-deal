<?php 
	/********************************************************************************
	    File: Newsletter Listing
	    Author: PHEA RATTANA
	
	    Confidential ABi Technologies property.
	
	  	Changed History:
	  	Date 					Author				Description
	  	2014/June/04 			PHEA RATTANA		Initial
	*********************************************************************************/

	$edit = false; 

	if( !empty($this->request['pass']) && $this->request['pass'][0] == 'edit' ){
		$edit = true;
	}

	// var_dump($configuration);
 ?>

<style>

	.control-group{
		background-image: none !important;
		border-bottom: 1px solid #e5e5e5 !important;
		box-shadow: none !important;
	}

	table tr td{
		vertical-align: top;
	}

	table p{
		margin-top: 0px !important;
	}

</style>

<div class="newsletters index">
	<div class="row-fluid">		
		<!-- Pie: Box -->
		<div class="span12">

			<!-- Pie: Top Bar -->
			<div class="top-bar">
				<h3><i class="icon-list"></i> Mail Configuration</h3>
			</div>
			<!-- / Pie: Top Bar -->

			<!-- Pie: Content -->
			<div class="well">

				<a href="<?php echo $this->Html->url( array('action' => 'compose')); ?>"> 
					<button class="btn btn-linkedin" type="button"><i class="icon-envelope-alt"></i> Compose New Newsletter</button>
				</a>				

				<div class="clearfix"></div>
				
				<legend style="margin-bottom: 0px;">Mail Configuration</legend>

				<?php if( !empty($configuration) && !$edit ){ ?>

					<div class="span11" style="margin-top: 20px;">
						<table>
							<tr>
								<td width="100"><b>Sender Email</b></td>
								<td width="20"> : </td>
								<td><?php echo $configuration['MailConfigure']['user_name'] ?></td>
							</tr>

							<!-- <tr>
								<td width="100"><b>Host</b></td>
								<td width="20"> : </td>
								<td><?php echo $configuration['MailConfigure']['host'] ?></td>
							</tr>

							<tr>
								<td width="100"><b>Port</b></td>
								<td width="20"> : </td>
								<td><?php echo $configuration['MailConfigure']['port'] ?></td>
							</tr> -->

							<tr>
								<td width="100"><b>Mail Signature</b></td>
								<td width="20"> : </td>
								<td><?php echo $configuration['MailConfigure']['signature'] ?></td>
							</tr>

						</table>

						<a href="<?php echo $this->Html->url( array('action' => 'config/edit')); ?>"> 
							<button class="btn btn-skype" type="button"><i class="icon-cog"></i> Edit Configuration</button>
						</a>

					</div>

				<?php }else{ ?>

					<?php echo $this->Form->create('Newsletter'); ?>
				
					<div class="control-group" style="overflow:hidden">
						<div class="span6">
							<label class="control-label" for="email"><i class="icon-envelope-alt"></i>Sender Email</label>
							<div class="controls">
								<input type="hidden" id="email" placeholder="Email" class="input-block-level"
											name="data[MailConfigure][user_name]" maxlength="50"
											value="<?php echo(!empty($configuration))?$configuration['MailConfigure']['user_name']:"" ?>" 
											required="required">
								<label for=""><?php echo(!empty($configuration))?$configuration['MailConfigure']['user_name']:"" ?></label>
							</div>
						</div>
					</div>
					<!-- <div class="clearfix"></div> -->

					<!-- <div class="control-group" style="overflow:hidden">
						<div class="span6">
							<label class="control-label" for="host"><i class="icon-random"></i>Host</label>
							<div class="controls">
								<input type="text" id="host" placeholder="Host" class="input-block-level"
											name="data[MailConfigure][host]" maxlength="255"
											value="<?php echo(!empty($configuration))?$configuration['MailConfigure']['host']:"" ?>" 
											required="required">
							</div>
						</div>
						<div class="span6">
							<label class="control-label" for="port"><i class="icon-signin"></i>Port</label>
							<div class="controls">
								<input type="text" id="port" placeholder="Port" class="input-block-level"
											name="data[MailConfigure][port]" maxlength="10"
											value="<?php echo(!empty($configuration))?$configuration['MailConfigure']['port']:"" ?>" 
											required="required">
							</div>
						</div>
					</div> -->
					<!-- <div class="clearfix"></div> -->

					<div class="control-group">
						<label class="control-label" for="signature"><i class="icon-edit"></i>Mail Signature</label>
						<div class="controls">
							<textarea 	name="data[MailConfigure][signature]" 
										id="signature" cols="30" rows="10" class="ckeditor"
										maxlength="65535"><?php echo(!empty($configuration))?$configuration['MailConfigure']['signature']:"" ?></textarea>
						</div>
					</div>

					<div class="submit" style="margin-top: 20px; clear: both; float:right; padding-right:20px;">
						<a href="<?php echo $this->Html->url( array('action' => 'index')); ?>"> 
							<button class="btn btn-google cancel" type="button">Cancel</button>
						</a>
					 	<input class="btn btn-primary" type="submit" value="Save">
					</div>
				<?php } ?>
				
			</div>
			<!-- / Pie: Content -->

		</div>
		<!-- / Pie -->
		
	</div>

</div>

<script>


</script>


