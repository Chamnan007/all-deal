<?php

/********************************************************************************
    File: Add Discount Category
    Author: PHEA RATTANA

    Confidential ABi Technologies property.

  	Changed History:
  	Date 					Author				Description
  	2014/01/05 				PHEA RATTANA		Initial
*********************************************************************************/
?>

<div class="discountCategories form">
	<div class="row-fluid">		
		<!-- Pie: Box -->
		<div class="span12">

			<!-- Pie: Top Bar -->
			<div class="top-bar">
				<h3><i class="icon-list"></i> Manage Discount Groups</h3>
			</div>
			<!-- / Pie: Top Bar -->

			<!-- Pie: Content -->
			<div class="well">

			<a href="<?php echo $this->Html->url( array('action' => 'index')); ?>"> 
				<button class="btn btn-linkedin" type="button"><i class="icon-list-alt"></i> Discount Group List</button>
			</a>
			

			<div class="clearfix"></div>
				
				<?php echo $this->Form->create('DiscountCategory'); ?>
					<fieldset>

						<legend><?php echo __('Edit Information'); ?></legend>

						<div class="span4">
							<?php
								echo $this->Form->input('id');
								echo $this->Form->input('category', array('placeholder'=>'Discount Category',
																			'type' => 'text',
																			'label' => 'Group Name',
																			'required' => 'required',
																			'style' => "width:300px;"));
							?>	
						</div>

						<div class="span3" style="padding-top:19px;">
							<?php echo $this->Form->submit(__('Save Change'), array('class' => 'btn btn-primary') ); ?>	
						</div>	
						
					</fieldset>
			

			</div>
			<!-- / Pie: Content -->

		</div>
		<!-- / Pie -->
		
	</div>

</div>