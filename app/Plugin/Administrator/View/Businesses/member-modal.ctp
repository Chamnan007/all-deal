
<!-- Add New Member -->
<div id="member_add" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true"
  style="padding-bottom: 10px !important; width:820px !important; left:40% !important">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel"><i class="icon-user"></i> Add New Member</h3>
  </div>

  <!-- Any Info-customization -->
  <form action="<?php echo $this->Html->url(array('action' => 'memberAdd', $business['Business']['id'])); ?>" 
      style="padding: 20px;" method="POST" data-validate = "parsley" id="FormMemberAdd">
    
    <div class="span3">

      <div class="input text required">
        <label for="UserFirstName">First Name</label>
        <input  name="User[first_name]" placeholder="First Name" maxlength="50" 
            type="text" id="UserFirstName" required="required" >
      </div>

      <div class="input text required">
        <label for="UserLastName">Last Name</label>
        <input  name="User[last_name]" placeholder="Last Name" maxlength="50" 
            type="text" id="UserLastName" required="required" >
      </div>

      <?php 


        echo $this->Form->input(   'gender' ,array(
                       'type'     => 'select',
                       'id'     => 'UserGender',
                       'label'    => 'Gender',
                       'name'   => "User[gender]",
                       'options'  => $gender
                    ));
        
        
        echo $this->Form->input(  'dob', array( 
                      'type'    => 'date',
                      'style'   => 'width: 32% !important;',
                      'label'   => 'Date of Birth',
                      'dateFormat'   => 'DMY',
                        'id'    => 'UserDob',
                        'name'    => "User[dob][]",
                      'minYear'   => date('Y') - 90,
                      'maxYear'   => date('Y')) );

      ?>

    </div>

    <div class="span3">

      <?php 


        echo $this->Form->input('position', array('placeholder' => 'Position',
                            'id' => 'UserPosition',
                            'maxlength' => 100,
                            'name' => 'User[position]'));
        
        echo $this->Form->input('access_level', array('type' => 'hidden',
                                    'id' => 'UserAccessLevel',
                                    'name' => 'User[access_level]',
                                    'value' => '2' ) );

        echo $this->Form->input(   'province' ,array(
                       'type'     => 'select',
                       'label'    => 'Province',
                        'id'    => 'UserProvince',
                        'required'  => 'required',
                        'name'    => "User[province_code]",
                       'empty'    => 'Select a province',
                       'options'  => $province_name
                    ));


        echo $this->Form->input(   'city' ,array(
                       'type'     => 'select',
                       'label'    => 'City',
                         'id'     => 'UserCity',
                         'required' => 'required',
                         'name'   => "User[city_code]",
                       'empty'    => 'Select a city',
                       'options'  => array()
                    ));


        echo $this->Form->input('street', array('placeholder' => 'Street',
                            'id' => 'UserStreet',
                            'maxLength' => 100,
                            'name' => 'User[street]'));

        
       ?>

    </div>

    <div class="span3">
      
      <?php 


        echo $this->Form->input('phone', array('placeholder' => 'Phone Number',
                            'id' => 'UserPhone',
                            'required' => 'required',
                            'maxLength' => 20,
                            'name' => 'User[phone]'));

        echo $this->Form->input('email', array('placeholder' => 'Email', 
                            'type' => 'email',
                            'required' => 'required',
                            'id' => 'UserEmail',
                            'maxlength' => '50',
                            'name' => 'User[email]'));

        echo $this->Form->input('password', array('placeholder' => 'Password',
                                    'id' => 'UserPassword',
                            'required' => 'required',
                            'maxLength' => 30 ,
                                    'name' => 'User[password]'));

        echo $this->Form->input('confirm_password', 
                    array(  'placeholder' => 'Confirm Password', 
                        'type'=>'password', 
                        'data-equalto' => '#UserPassword',
                        'id' => 'UserConfirmPassword',
                            'required' => 'required',
                        'name' => 'User[confirm_password]'));


        echo $this->Form->input(  'status' ,array(
                        'type'    => 'hidden',
                        'value'   => '1',
                      'name' => 'User[status]'
                    ));

       ?>
      

    </div>

    <div style="clear:both; padding-top: 20px;"></div>

    <div class="submit" style=" margin-left:20px;">
      <!-- <input class="btn btn-primary" type="button" value="Save" id="save"> -->
      <button class="btn btn-primary" type="button" id="save">Save</button>
      <button class="btn btn-google cancel" data-dismiss='modal' type="button">Cancel</button>
    </div>

      
  </form>

</div> 

<!-- Edit Member Information -->
<div id="member_edit" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true"
  style="padding-bottom: 10px !important; width:820px !important; left:40% !important">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel"><i class="icon-user"></i> Edit Member Information</h3>
  </div>

  <!-- Any Info-customization -->
  <form action="<?php echo $this->Html->url(array('action' => 'memberEdit', $business['Business']['id'])); ?>" 
      style="padding: 20px;" method="POST"  data-validate = "parsley"
      id="memberEditForm" >
    
    <div class="span3">

      <input  name="User[id]" type="hidden" id="UserId" >

      <div class="input text required">
        <label for="UserFirstName">First Name</label>
        <input  name="User[first_name]" placeholder="First Name" maxlength="50" 
            type="text" id="UserFirstName" required="required" >

      </div>


      <div class="input text required">
        <label for="UserLastName">Last Name</label>
        <input  name="User[last_name]" placeholder="Last Name" maxlength="50" 
            type="text" id="UserLastName" required="required" >
      </div>

      <?php 
        echo $this->Form->input(   'gender' ,array(
                       'type'     => 'select',
                       'id'     => 'UserGender',
                       'label'    => 'Gender',
                       'name'   => "User[gender]",
                       'options'  => $gender
                    ));

        echo $this->Form->input(  'dob', array( 
                      'type'    => 'date',
                      'style'   => 'width: 32% !important;',
                      'label'   => 'Date of Birth',
                      'dateFormat'  => 'DMY',
                        'id'    => 'UserDob',
                        'name'    => "User[dob][]",
                      'minYear'   => date('Y') - 90,
                      'maxYear'   => date('Y')) );

      ?>



    </div>

    <div class="span3">

      <?php 
      

        echo $this->Form->input('position', array('placeholder' => 'Position',
                            'id' => 'UserPosition',
                            'maxLength'=> 100,
                            'name' => 'User[position]'));

        echo $this->Form->input(   'province' ,array(
                       'type'     => 'select',
                       'label'    => 'Province',
                       'required' => 'required',
                        'id'    => 'UserProvinceEdit',
                        'name'    => "User[province_code]",
                       'empty'    => 'Select a province',
                       'options'  => $province_name
                    ));


        echo $this->Form->input(   'city' ,array(
                       'type'     => 'select',
                       'label'    => 'City',
                       'required' => 'required',
                         'id'     => 'UserCityEdit',
                         'name'   => "User[city_code]",
                       'options'  => array()
                    ));


        echo $this->Form->input('street', array('placeholder' => 'Street',
                            'id' => 'UserStreet',
                            'name' => 'User[street]',
                            'maxLength'=> 100));


        // $biz_access_level = array( '1' => $access_level[1], '2' => $access_level[2] );


        echo $this->Form->input(  'access_level' ,array(
                        'type'    => 'select',
                        'label'   => 'Access Level',
                        'options' => $member_level,
                      'id' => 'UserAccessLevel',
                      'name' => 'User[access_level]'
                    ));
       ?>

    </div>

    <div class="span3">

      <?php 


        echo $this->Form->input('phone', array('placeholder' => 'Phone Number',
                            'id' => 'UserPhone',
                            'required' => 'required',
                            'maxLength'=> 20,
                            'name' => 'User[phone]'));


        echo $this->Form->input('email', array('placeholder' => 'Email', 
                            'type' => 'email',
                            'required' => 'required',
                            'id' => 'UserEmail',
                            'name' => 'User[email]'));

        echo $this->Form->input('password', array('placeholder' => 'Password',
                                    'id' => 'UserPasswordEdit',
                                    'maxLength' => 30,
                                    'name' => 'User[password]'));

        echo $this->Form->input('confirm_password', 
                    array(  'placeholder' => 'Confirm Password', 
                        'type'=>'password', 
                        'data-equalto' => '#UserPasswordEdit',
                        'id' => 'UserConfirmPasswordEdit',
                        'name' => 'User[confirm_password]'));





        echo $this->Form->input(  'status' ,array(
                        'type'    => 'select',
                        'label'   => 'Status',
                        'options' => $status_arr,
                      'id' => 'UserStatus',
                      'name' => 'User[status]'
                    ));
        
       ?>

    </div>
    

    <div class="submit" style="margin-top: 20px; clear: both;">
      <input class="btn btn-primary" type="button" value="Save" id="edit_save">
      <button class="btn btn-google" data-dismiss='modal' type="button" id='canel_edit'>Cancel</button>
    </div>

      
  </form>

</div> 


<!-- Deactivate User -->
<div id="deactivate_user" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel"><i class="icon-edit"></i> Are You Sure ?</h3>
  </div>

  <form action="#" 
      style="padding: 20px;" method="POST" id="ActivateUser"
      enctype = "multipart/form-data" >

    <div>
      <h5>Are you sure you want to deactivate this member?</h5>
    </div>

    <div class="submit" style="margin-top: 20px; clear: both;">
      <input class="btn btn-primary" type="submit" value="Yes">
      <button class="btn btn-google cancel" data-dismiss='modal' type="button">No</button>
    </div>
      
  </form>

</div>