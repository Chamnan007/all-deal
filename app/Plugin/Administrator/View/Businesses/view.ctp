<?php

  $active_tab = 'detail';

  $menu_array = array("deal", 'menu', 'member', 'media', 'revenue', 'balance' );

  if( isset($this->params['pass'][1]) ){
    $active_tab = $this->params['pass'][1] ;
  }

  if( !in_array($active_tab, $menu_array)){
    $active_tab = 'detail';
  }

  $city     = urldecode($business['City']['city_name']);

  $location = ( $business['Business']['location'] != "" )? $business['Business']['location'] . ", " :"";
  $street   = ( $business['Business']['street'] != "" )? $business['Business']['street'] . ", " :"";

  $sangkat   = ( $business['SangkatInfo']['name'])? $business['SangkatInfo']['name'] . ", " :"";

  $address = $street . $sangkat . $location . $city ;

  $main_category = urldecode($business['MainCategory']['category']);
  $sub_category = urldecode($business['SubCategory']['category']);


  $biz_name = urldecode($business['Business']['business_name']);

  $province_name = array();

  if( !empty($provinces) ){
    foreach( $provinces as $key => $pro ){
      $province_name[$pro['Province']['province_code']] = $pro['Province']['province_name'];
    }
  }

  $city_name = array();

  if( !empty($cities) ){
    foreach( $cities as $key => $value ){
      $city_name[$value['City']['city_code']] = $value['City']['city_name'];
    }
  }

  $status_arr = array();  

  foreach ($status as $key => $value) {
    $status_arr[$key] = $value['status'];
  }

  $bis_categories = array();

  if( !empty($businessCategories) ){
    foreach( $businessCategories as $key => $value ){
      $bis_categories[$value['BusinessCategory']['id']] = $value['BusinessCategory']['category'] ;
    }
  }

  $goods_category = array();

  if( !empty($goods) ){
    foreach( $goods as $key => $value ){
      $goods_category[$value['GoodsCategory']['id']] = urldecode($value['GoodsCategory']['category']);
    }
  }


  $lat = $business['Business']['latitude'];
  $lng = $business['Business']['longitude'];
  $name = urldecode($business['Business']['business_name']);


  // Branches Data
  $data_braches = array();
  $time = time();

  if( isset($business['Branches']) && !empty($business['Branches']) ){
      foreach( $business['Branches'] as $k => $val ){
         $time += 300;

         $data_braches[$k]['index']       = $time;
         $data_braches[$k]['id']          = $val['id'];
         $data_braches[$k]['branch_name'] = $val['branch_name'];
         $data_braches[$k]['latitude']    = $val['latitude'];
         $data_braches[$k]['longitude']    = $val['longitude'];
      }
  }

  $menu_id = 0;

  if( isset($this->params['pass'][2]) ){
    $menu_id = $this->params['pass'][2];
  }

  $limit = 10;

  $total_items = $count_menu_items ;
  $array_total_items = array();

  foreach( $total_items as $key => $val ){
     $menu_cate_id = $val['BusinessMenuItem']['menu_id'];
     $array_total_items[$menu_cate_id] = $val[0]['total'];
  }

  $base_url = $this->Html->url( array('action' => 'view', $business['Business']['id']));
  $ending_balance = $business['Business']['ending_balance'];

?>

<style>
   
  .row-active{
     background-color: rgb(249, 252, 210) !important;
     font-weight: bold !important;
  }

  .sub-row{
    display: none;
    cursor: pointer;
  }

  .sub-row:hover{
    font-weight: bold;
  }

  .sub-row-active{
    display: table-row !important;
  }

  .sub-row-click{
    font-weight: bold;
  }

  .expand{
    cursor: pointer;
  }

  .tran-detail{
    display: none;
    background-color: #EEE; 
    font-size: 12px;
  }

  .tran-detail-active{
    display: table-row !important;
  }


  .video_box{
    width: 250px;
    height: 190px;
    border: 1px solid #CCC;
    float: left;
    margin-right: 10px;
    margin-bottom: 10px;  
    position: relative;   
    background-color: white;      
  }

  .select {
    margin-bottom: 0px !important;
  }

  .equalto, .required{
    color: red;
    font-size: 12px !important;
    list-style-type: none;
  }

  .gmnoprint img { max-width: none; }

  .labels{
      color: black;
      padding: 5px 10px 5px 10px;
      background: #9AD8F9;
      font-weight: bold;
      text-align: center;
    }


  .menu-row:hover{
    cursor: pointer;
  }

  .selected{
     background: #CAE5F9 !important;
  }


   .deal-image{
      width:223px; 
      height: 133px; 
      border: 1px solid #CCC; 
      padding:3px; 
      float:left; 
      margin-right:20px;
      margin-top:10px;
      margin-bottom: 10px;
      position: relative;
  }

  .deal-image:hover > .remove-img{
      display: block;    
  }

  .deal-image .remove-img{
      position: absolute;
      display: none;
      background: rgb(255, 101, 81);
      padding: 5px 5px 5px 7px ;
      text-align: center;
      border: rgb(255, 101, 81);
      border-radius: 3px ;
      left:44%;
      top:40%;
  }
  .deal-image .remove-img i{
      color:white;
      font-size:15px;
  }
  .deal-image .remove-img:hover{
      background: rgb(217, 64, 44);
      cursor: pointer;
  }


  .deal-image-upload{
      background: #DDD;
      width:223px; 
      height: 133px; 
      border: 1px solid #CCC; 
      padding:3px; 
      float:left; 
      margin-right:20px;
      margin-top:10px;
      margin-bottom: 10px;
      position: relative;
  }
  .deal-image-upload .upload{
      position: absolute;
      background: rgb(54, 132, 252);
      padding: 5px 5px 5px 7px ;
      text-align: center;
      border: rgb(54, 132, 252);
      border-radius: 3px ;
      left:40%;
      top:40%;
  }
  .deal-image-upload .upload i{
      color:white;
      font-size:15px;
  }
  .deal-image-upload .upload:hover{
      background: rgb(14, 93, 216);
      cursor: pointer;
  }


  table.no-border{
    border: none;
  }

  .no-border th, .no-border tr, .no-border td,.no-border td{
    border: none !important;
    padding: 2px !important;
  }

  td{
    vertical-align: top !important;
  }

</style>

<div class="business form">
  
  <div class="row-fluid">
    
    <div class="span12">

      <div class="top-bar">
        <ul class="tab-container">
            <li <?php echo ($active_tab == 'detail')?" class='active'":"" ?>>
              <a href="#tab-detail" tab="" ><i class="icon-list"></i> Detail Information</a></li>
            
            <li <?php echo ($active_tab == 'deal')?" class='active'":"" ?>>
              <a href="#tab-deal" tab="deal"><i class="icon-list"></i> Deals</a>
            </li>

            <li <?php echo ($active_tab == 'revenue')?" class='active'":"" ?>>
              <a href="#tab-revenue" tab="revenue"><i class="icon-list"></i> Revenue & Commission</a>
            </li>

            <li <?php echo ($active_tab == 'balance')?" class='active'":"" ?>>
              <a href="#tab-balance" tab="balance"><i class="icon-list"></i> Balance</a>
            </li>

            <li <?php echo ($active_tab == 'menu')?" class='active'":"" ?>>
              <a href="#tab-menu" tab="deal"><i class="icon-list-alt"></i> Menu/ Items</a>
            </li>
            
            <li <?php echo ($active_tab == 'member')?" class='active'":"" ?>>
              <a href="#tab-member" tab="member"><i class="icon-user"></i> Members</a>
            </li>
            
            <li <?php echo ($active_tab == 'media')?" class='active'":"" ?>>
              <a href="#tab-media" tab="media"><i class="icon-picture"></i> Media</a>
            </li>
        </ul>

      </div>

      <div class="tab-content">

        <a href="<?php echo $this->Html->url( array('action' => 'index')); ?>"> 
          <button class="btn btn-linkedin" type="button"><i class="icon-list-alt"></i> Merchants List</button>
        </a>

        <div class="clearfix"></div>
      
        <div class="tab-content">

          <!-- Tab Detail -->
          <div class="tab-pane  <?php echo ($active_tab == 'detail')?" active":"" ?>" id="tab-detail">
            <legend><?php echo __('Detail Information'); ?></legend>
            <div class="span6">


              <a href="<?php echo $this->Html->url(array('action' => 'edit', $business['Business']['id'], 'detail')); ?>"> 
                <button class="btn btn-skype" type="button"><i class="icon-edit"></i> Edit Information</button>
              </a>

              <h5 style="margin-top:20px; color: #333 !important;"><?php echo __($biz_name); ?></h5>

              <table width="100%">

                <tr>
                  <td>Merchant Code</td>
                  <td>:</td>
                  <td>
                    <strong><?php echo $business['Business']['business_code'] ?></strong>
                  </td>
                </tr>

                <tr>
                  <td>Category </td>
                  <td> : </td>
                  <td><strong><?php echo h($main_category); ?></strong></td>
                </tr>

                <tr>
                  <td>Address </td>
                  <td> : </td>
                  <td><?php echo h($address); ?></td>
                </tr>
                
                <?php 

                  $website = (strpos($business['Business']['website'], 'http://') !== false )?$business['Business']['website']: "http://" . $business['Business']['website'];
                ?>

                <tr>
                  <td>Website</td>
                  <td> : </td>
                  <td>
                    <a style="color: blue !important" href="<?php echo $website ?>" target="_blank"><?php echo str_replace("http://", "", $business['Business']['website']) ?></a>
                  </td>
                </tr>

                <tr>
                  <td>Email</td>
                  <td> : </td>
                  <td>
                    <?php echo h($business['Business']['email']) ?>
                  </td>
                </tr>

                <tr>
                  <td>Phone</td>
                  <td> : </td>
                  <td>
                    <?php echo h($business['Business']['phone1']) ?>
                    <?php echo($business['Business']['phone2'] != NULL )?" / " . $business['Business']['phone2']:"" ?>
                  </td>
                </tr>

                <tr>
                  <td>Accept Payment</td>
                  <td> : </td>
                  <td>
                  
                  <?php $payment = json_decode($business['Business']['accept_payment']) ;

                    if( !empty($payment) ){

                      foreach( $payment as $key => $value ){
                        // echo "<strong>" . ucfirst($value) . "</strong>, ";
                        echo $this->Html->image( $value . '.jpg', array(  'alt' => ucfirst($value),
                                              'title'=>ucfirst($value),
                                              'style'=>'width:50px; margin-right:5px;'));
                      }
                    }

                  ?>
                        
                  </td>
                </tr>

                <tr>
                  <td width="120px;">Member Level </td>
                  <td> : </td>
                  <td <?php echo($business['Business']['member_level'] == 1)?" style='color:red'":" style='color:green'" ?>>
                    <b><?php echo $access_level[$business['Business']['member_level']] ?></b>
                  </td>
                </tr>

                <tr>
                  <td>Registered Date</td>
                  <td> : </td>
                  <td style="color: #06F">
                    <?php echo h(date("d-F-Y h:i:s A", strtotime($business['Business']['created']) )) ?>
                  </td>
                </tr>

                <tr>
                  <td>Registered By</td>
                  <td> : </td>
                  <td>
                    <?php echo urldecode( $business['RegisteredBy']['first_name'] . " " . 
                                  $business['RegisteredBy']['last_name'] ) ?>
                  </td>
                </tr>

                <tr>
                  <td>Business Status</td>
                  <td> : </td>
                  <td>
                    <span class="label label-<?php echo $status[$business['Business']['status']]['color'] ?>">
                      <?php echo h($status[$business['Business']['status']]['status']); ?>
                    </span>
                  </td>
                </tr>

                <?php if( $business['Business']['status'] == 1 ){ ?>
                  <tr>
                    <td>Approved Date</td>
                    <td> : </td>
                    <td>
                      <?php echo h(date("d-F-Y h:i:s A", strtotime($business['Business']['approved_date']) )) ?>
                    </td>
                  </tr>
                  <tr>
                    <td>Approved By</td>
                    <td> : </td>
                    <td>
                      <?php echo urldecode( $business['ApprovedBy']['first_name'] . " " . 
                                  $business['ApprovedBy']['last_name'] ) ?>
                    </td>
                  </tr>
                <?php } else if( $business['Business']['status'] == 2){ ?>
                  <tr>
                    <td>Suspended Date</td>
                    <td> : </td>
                    <td>
                      <?php echo h(date("d-F-Y h:i:s A", strtotime($business['Business']['suspend_date']) )) ?>
                    </td>
                  </tr>
                <?php }elseif( $business['Business']['status'] == -1){ ?>
                  <tr>
                    <td>Inactive Date</td>
                    <td> : </td>
                    <td>
                      <?php echo h(date("d-F-Y h:i:s A", strtotime($business['Business']['inactive_date']) )) ?>
                    </td>
                  </tr>
                <?php } ?>


              </table>

              <div style="clear:both; padding-top: 10px;"></div>
              <h5 style="margin-right:10px;"><?php echo __('Description'); ?></h5>
              <?php echo $business['Business']['description'] ?>


            </div>

            <div class="span5">

              <a href="#operationHourEdit" id="editHour"  data-toggle="modal" data-detail='<?php  echo json_encode($business['OperationHour']) ?>'> 
                <button class="btn btn-skype" type="button"><i class="icon-edit"></i> Edit Operation Hours</button>
              </a>

              <h5 style="margin-top:20px;"><?php echo __('Operation Hours'); ?></h5>

              <table width="100%">

                <?php foreach( $business['OperationHour'] as $key => $hour ):  
                ?>
                  <tr>
                    <td width="100px;"><?php echo h(strtoupper($hour['day'])) ?></td>
                    <td> : </td>
                    <td>
                    <?php if($hour['from'] == NULL && $hour['to'] == NULL ){ 
                        echo "<i>Closed</i>";
                    }else if( $hour['from'] == $hour['to'] ){ 
                        echo "24 hours";
                     }else{ ?>

                    <?php echo ($hour['from']!=NULL) ? date("h:i A", strtotime($hour['from']) ):"<i>Empty</i>"; ?> - 
                    <?php echo ($hour['to']!=NULL) ? date("h:i A", strtotime($hour['to']) ):"<i>Empty</i>"; ?>

                    <?php } ?>

                    </td>
                  </tr>
                <?php endforeach; ?>

              </table>

              <hr><h5><?php echo __('Business Logo'); ?></h5>

              <div style="width:200px; height: 200px; border: 1px solid #CCC; float:left">
                <a rel="shadowbox" href="<?php echo $this->webroot . $business['Business']['logo'] ?>">
                  <img src="<?php echo $this->webroot . $business['Business']['logo'] ?>" alt="" style="width: 100%; height: 100%;">
                </a>
              </div>



              <a href="#logo_change" data-toggle="modal" style="float:left; margin-left: 20px;"> 
                <button class="btn btn-linkedin" type="button"><i class="icon-edit"></i> Change Logo</button>
              </a>

              <a href="#removeLogo" data-toggle="modal" style="float:left; margin-left: 20px; margin-top: 20px;"> 
                <button class="btn btn-google cancel" type="button"><i class="icon-edit"></i> Remove Logo</button>
              </a>


            </div>
			
			     <div class="clearfix"></div>
            <h5 style="margin-right:10px;"><?php echo __('Map'); ?></h5>
			
			     <div style="margin-bottom:5px;">
            	<img style="width:17px;margin-right:10px;" src="<?php echo $this->webroot . 'img/pin-yellow.png' ?>" alt="">
            	Main Location
            </div>

            <div style="padding-bottom:20px;">
            	<img style="width:15px;margin-right:10px;" src="<?php echo $this->webroot . 'img/pin-blue.png' ?>" alt="">
            	Branches Location
            </div>
            
            <!-- Map -->
            <div id="div-map" class="span12" style="padding-bottom: 20px; width: 98%; height: 350px; border: 1px solid #CCC">

            </div>

          </div>

          <!-- Tab Member -->
          <div class="tab-pane <?php echo ($active_tab == 'member')?" active":"" ?>" id="tab-member">
            
            <legend><?php echo __('Merchant Members'); ?></legend>

            <a href="#member_add" data-toggle="modal"> 
              <button class="btn btn-linkedin" type="button"><i class="icon-plus"></i> Add New Member</button>
            </a>

            <div style="clear: both; padding-top: 20px;" ></div>

              <table class="table-list">
                <tr>
                  <th width="30px">N<sup>o</sup></th>
                  <th width="150px;">Name</th>
                  <th>Gender</th>
                  <th>Position</th>
                  <th>Contact Info</th>
                  <th>Detail Info</th>
                  <th></th>
                </tr>
            <?php 
              $members = $business['User'];

              foreach( $members as $key => $mem ){
            ?>
                
                <tr>
                  <td style="text-align: center"><?php echo $key + 1 ?></td>
                  <td><?php echo urldecode($mem['first_name'] . " " . $mem['last_name']) ?></td>
                  <td style="text-align: center"><?php echo $mem['gender'] ?></td>
                  <td><?php echo urldecode($mem['position']) ?></td>
                  <td>
                    <strong>Address : </strong>
                    <?php 
                      echo ($mem['street'] == NULL)?"":$mem['street'] . ", " ; 
                      echo h($city_name[$mem['city_code']]) . ", ";
                      echo h($province_name[$mem['province_code']]) ;
                    ?>
                    <br><strong>Phone : </strong><?php echo $mem['phone'] ?><br>
                    <strong>Email : </strong><?php echo $mem['email'] ?>
                  </td>
                  <td>
                    <strong>Access Level : </strong><?php echo h($member_level[$mem['access_level']]); ?><br>
                    
                    <strong>Registered Date : </strong>
                    <?php echo date("d-F-Y h:i:s A", strtotime($mem['registered_date']) ); ?><br>

                    <strong>Status : </strong>
                    <span class="label label-<?php echo $status[$mem['status']]['color'] ?>">
                      <?php echo h($status[$mem['status']]['status']); ?>
                    </span>

                    <?php if($mem['status'] == -1 ){ ?>
                    <br><strong>Inactive Date : </strong>
                    <?php echo date("d-F-Y h:i:s A", strtotime($mem['inactive_date']) ); ?>
                    <?php } ?>

                  </td>

                  <td class="actions" style="vertical-align: top">
                    <a  href="#member_edit" data-toggle="modal" class="MemberEdit" 
                      data-userid = "<?php echo $mem['id'] ?>" 
                      data-detail='<?php echo json_encode($mem); ?>' > 
                      <button class="btn btn-foursquare" type="button">Edit</button>
                    </a>

                    <?php if( $mem['status'] != -1 ){ ?>

                      <a  class='activate' href="#deactivate_user" 
                        data-toggle="modal" 
                        data-userid="<?php echo $mem['id'] ?>"
                        data-bisid="<?php echo $business['Business']['id'] ?>"
                        data-action="0"> 
                        <button class="btn btn-google cancel" type="button">Deactivate</button>
                      </a>

                    <?php }else{

                    ?>

                      <a  class="activate" href="#deactivate_user" 
                        data-toggle="modal" 
                        data-userid="<?php echo $mem['id'] ?>"  
                        data-bisid="<?php echo $business['Business']['id'] ?>"
                        data-action="1"> 
                        <button class="btn btn-linkedin" type="button">Activate</button>
                      </a>

                    <?php
                      } ?>
                  </td>

                </tr>

            <?php
              }

             ?>


            </table>
          </div>

          <!-- Revenue & Commission Tab -->
          <div class="tab-pane <?php echo ($active_tab == 'revenue')?" active":"" ?>" id="tab-revenue">
              <legend><?php echo __('Revenue & Commission'); ?></legend>
              
              <?php 
                  $groupedData = array();
                  $date = array();
                  $deal_amount = array();
                  $deal_revenue = array();
                  $status = 1;

                  foreach( $revenue_data as $key => $val ){
                    $deal_code = $val['DealInfo']['deal_code'];
                    $deal_id   = $val['BuyerTransaction']['deal_id'];

                    $deal_amount[$deal_code] += $val['BuyerTransaction']['amount'];

                    $date[$deal_code][] = $val['BuyerTransaction']['created'];

                    if( $val['DealInfo']['status'] == -1 || strtotime($val['DealInfo']['valid_to']) < time() ){
                      $status = -1;
                    }

                    $groupedData[$deal_code]['deal_id']     = $deal_id;
                    $groupedData[$deal_code]['date']      = $date[$deal_code][0];
                    $groupedData[$deal_code]['status']      = $status;
                    $groupedData[$deal_code]['total_amount']  = $deal_amount[$deal_code] ;
                    $groupedData[$deal_code]['commission']    = $val['BuyerTransaction']['commission_percent'] ;
                    $groupedData[$deal_code]['detail'][]    = $val;

                  }

              ?>

              <div class="span12" style="margin-left:0px;">

                <h5 style="float:left; color:#333;">
                  <?php echo $period_string; ?> :
                  <span style="color: blue; margin-left:20px;">
                    <?php echo ($total_revenue)?"$ " . $this->MyHtml->formatNumber($total_revenue):"$ 0.00" ?>
                  </span>
                  <br>
                  <?php echo $commission_text; ?> :
                  <span style="color: blue; margin-left:20px;">
                    <?php echo ($total_commission)?"$ " . $this->MyHtml->formatNumber($total_commission):"$ 0.00" ?>
                  </span>
                </h5>

                <a id="btn-search-clear-revenue" href="#" class="btn btn-google pull-right" >
                  <i class="icon icon-trash"></i>
                  Clear Search</a>
                <a id="btn-search-revenue" href="#" class="btn btn-linkedin pull-right" style="margin-right:10px;">
                  <i class="icon icon-search"></i>
                  Filter</a>

                <div class="clearfix"></div>

                <table class="table-list">
                    <thead>
                      <th width="10px;"></th>
                      <th width="160px;">Date</th>
                      <th width="100px">Deal Code</th>
                      <th width="100px">Deal Status</th>
                      <th width="150px;">Amount</th>
                      <th width="150px;">Commission</th>
                      <th width="150px;"> Revenue</th>
                    </thead>

                    <tbody>

                      <?php if($groupedData){ 

                          $current_date = "";
                          $now = date('Y-m-d H:i:s');
                          $row_total= 0;
                      ?>
                      <?php foreach ($groupedData as $key => $value ): 

                          @$count++;  

                          $deal_id  = $value['deal_id'];
                          $deal_code  = $key;

                          $d_state     = $value['status'];
                          $d_state_text  = "Active";
                          $color      = "red" ;

                          if( $d_state == -1 ){
                            $d_state_text = "Finished";
                            $color     = "blue";
                          }

                          $amount   = $value['total_amount'];
                          $commission = $value['commission'];
                          $com_amount = $amount * $commission / 100 ;

                          $revenue  = $amount - $com_amount;

                          $row_total  += $revenue;
                      ?>

                      <tr>
                        <td style="text-align:center" class="expand indicator" data-code="<?php echo $key ?>"><strong>+</strong></td>
                        <td class="expand" data-code="<?php echo $key ?>"><?php echo date('d-F-Y h:i:s A', strtotime($value['date'])) ?></td>

                        <td>
                          <a  href="<?php echo $this->Html->url(array('action' => 'view','controller' => 'deals', $deal_id, 'plugin' => 'administrator' )); ?>" 
                            title="View Deal"
                            target="_blank">
                            <strong><?php echo $deal_code ?></strong>
                          </a>
                        </td>
                        <td style="color:<?php echo $color ?>">
                          <?php echo $d_state_text ?>
                        </td>

                        <td style="text-align:right">
                          <?php echo ($amount)?"$ " .$this->MyHtml->formatNumber($amount):"-" ?>
                        <td style="text-align:right">
                          <?php echo ($commission)?$this->MyHtml->formatNumber($commission) . "%" :"-" ?>
                        </td>
                        <td style="text-align:right">
                          <?php echo ($revenue)?"$ " .$this->MyHtml->formatNumber($revenue):"-" ?>
                        </td>
                      </tr> 

                      <?php 

                        $detail = $value['detail'];

                          foreach( $detail as $k => $val ):

                            $amount   = $val['BuyerTransaction']['amount'];
                            $commission = $val['BuyerTransaction']['commission_percent'];
                            $com_amount = $amount * $commission / 100 ;

                            $revenue  = $amount - $com_amount;

                      ?>
                          <tr class="sub-row sub-row-<?php echo $key ?>" 
                            title="View Detail"
                            data-id="<?php echo $val['BuyerTransaction']['id'] ?>"
                            style="background-color: rgb(244, 252, 231) !important;">
                            <td colspan="4" style="padding-left:50px;">
                              <?php echo date('d-F-Y h:i:s A', strtotime($val['BuyerTransaction']['created'])) ?>
                            </td>
                            <td style="text-align:right">
                              <?php echo ($amount)?"$ " . $this->MyHtml->formatNumber($amount):"-" ?>
                            <td style="text-align:right">
                              <?php echo ($commission)?$this->MyHtml->formatNumber($commission) . "%" :"-" ?>
                            </td>
                            <td style="text-align:right">
                              <?php echo ($revenue)?"$ " . $this->MyHtml->formatNumber($revenue):"-" ?>
                            </td>
                          </tr>

                      <?php
                            $tranDetail = $val['TransactionDetail'];

                            foreach( $tranDetail as $ind => $item ):

                              $item_price = $item['unit_price'];
                              $qty    = $item['qty'];

                              $amount   = $item_price * $qty;
                      ?>
                            <tr class="tran-detail tran-<?php echo $val['BuyerTransaction']['id'] ?>" >
                              <td colspan="4" style="padding-left: 80px">
                                <?php echo "- " . $item['ItemDetail']['title'] ?>
                              </td>
                              <td style="text-align:left">
                                <?php echo ($item_price)?"$ " .$this->MyHtml->formatNumber($item_price):"-" ?>
                                <span style="padding: 0 10px;">x</span><?php echo $qty ?>
                                <span style="padding: 0 10px;">=</span>
                                <span style="float:right">
                                  <?php echo ($amount)?"$ " .$this->MyHtml->formatNumber($amount):"-" ?>
                                </span>
                              </td>
                              <td colspan="2"></td>
                            </tr>

                      <?php                   
                            endforeach;
                          endforeach;
                       ?>

                      <?php 
                          endforeach;
                        }else{
                     ?>
                        <tr>
                          <td colspan="10"><i>No Transaction Found !</i></td>
                        </tr>
                     <?php
                      }
                      ?>
                    </tbody>
                </table>

              </div>

          </div>

          <!-- Balance Tab -->
          <div class="tab-pane <?php echo ($active_tab == 'balance')?" active":"" ?>" id="tab-balance">

              <legend><?php echo __('Balance'); ?></legend>

              <?php 
                $groupedData  = array();
                $date       = array();
                $deal_amount  = array();
                $deal_balance   = array();
                $status     = 1;
                $debit      = 0;
                $credit     = 0;
                $status     = 1;

                foreach( $data_balance as $key => $val ){
    
                  $type = $val['BusinessRevenue']['type'] ;
                  // 1: Transaction
                  // 2: Payment

                  if( $type == 1 ){
                    $transactionDetail = $val['TransactionInfo'];
                    $dealInfo        = $val['TransactionInfo']['DealInfo'];
                    $deal_code       = $dealInfo['deal_code'];
                    $deal_id         = $dealInfo['id'];

                    $deal_amount[$deal_code] += $val['BusinessRevenue']['total_amount'];

                    if( $dealInfo['status'] == -1 || strtotime($dealInfo['valid_to']) < time() ){
                      $status = -1;
                    }

                    $groupedData[$deal_code]['type']        = $type;
                    $groupedData[$deal_code]['deal_id']       = $deal_id;
                    $groupedData[$deal_code]['date'][]        = strtotime($val['BusinessRevenue']['created']);
                    $groupedData[$deal_code]['status']        = $status;
                    $groupedData[$deal_code]['credit']        = $deal_amount[$deal_code];
                    $groupedData[$deal_code]['debit']       = 0;
                    $groupedData[$deal_code]['method']        = "";
                    $groupedData[$deal_code]['transferred_date']  = "";
                    $groupedData[$deal_code]['detail'][]      = $val['TransactionDetail'];

                  }else{

                    $paymentInfo = $val['PaymentInfo'];

                    $deal_id = explode(",", $paymentInfo['deal_ids']);

                    $method = "- " . $pay_method[$paymentInfo['payment_method']];

                    if( $paymentInfo['payment_method'] == 1 ){ // Bank Transferred
                      $method .= "<br>";
                      $method .= "- Via: " . $paymentInfo['from_bank'] . " - " . $paymentInfo['to_bank'] ;
                    }

                    $groupedData[$paymentInfo['id']]['type']        = $type;
                    $groupedData[$paymentInfo['id']]['deal_id']       = $deal_id;
                    $groupedData[$paymentInfo['id']]['date'][]        = strtotime($val['BusinessRevenue']['created']);
                    $groupedData[$paymentInfo['id']]['status']        = -1;
                    $groupedData[$paymentInfo['id']]['credit']        = 0;
                    $groupedData[$paymentInfo['id']]['debit']       = $val['BusinessRevenue']['total_amount'];
                    $groupedData[$paymentInfo['id']]['method']        = $method ;
                    $groupedData[$paymentInfo['id']]['transferred_date']  = $paymentInfo['created'];
                    $groupedData[$paymentInfo['id']]['detail'][]      = "" ;
                  }
                  
                }
              ?>
          
              <div class="span12" style="margin-left:0px;">
                  <h5 style="float:left">Available Balance : 
                    <span style="color: blue">
                      <?php echo ($ending_balance)?"$ " . $this->MyHtml->formatNumber($ending_balance):"$ 0.00" ?></span>
                  </h5>

                  <a id="btn-search-balance-clear" href="#" class="btn btn-google pull-right" >
                    <i class="icon icon-trash"></i>Clear Search
                  </a>
                  <a id="btn-search-balance" href="#" class="btn btn-linkedin pull-right" style="margin-right:10px;">
                    <i class="icon icon-search"></i> Filter
                  </a>

                  <div class="clearfix"></div>

                  <div class="span12" style="margin-left:0px;">
                      <table class="table-list">
                        <thead>
                          <tr>
                            <!-- <th width="10px"></th> -->
                            <th width="160px;">Date</th>
                            <th width="100px">Deal Code</th>
                            <th width="100px">Deal Status</th>
                            <th width="150px;">Debits</th>
                            <th width="150px;">Credits</th>
                            <th>Method</th>
                            <th width="100px;">Transferred Date</th>
                            <th width="100px">Balance</th>
                          </tr>
                        </thead>
                        <tbody>

                        <?php if($groupedData){ 

                          $old_credit = $old_debit = 0;
                        ?>
                        <?php foreach ($groupedData as $key => $value ): 

                            @$count++;    

                            $date = max($value['date']);
                            $date = date('d-F-Y h:i:s A', $date);

                            $deal_id  = $value['deal_id'];

                            $deal_code  = $key;   
                            
                            $status     = $value['status'];
                            $deal_status_text  = "Active";
                            $color      = "red" ;

                            if( $status == -1 ){
                              $deal_status_text = "Finished";
                              $color     = "blue";
                            }

                            $debit = $value['debit'];
                            $credit = $value['credit'];

                            $transferred_date = ($value['transferred_date'])?date('d-F-Y', strtotime($value['transferred_date'])):"-";

                            $method = ($value['method'])?$value['method']:"-";

                            $balance = $ending_balance + $old_debit - $old_credit;

                            $ending_balance = $balance;
                            $old_debit  = $debit;
                            $old_credit = $credit;
                            
                        ?>

                        <!-- Data Row Here -->
                          <tr>
                            <!-- <td style="text-align:center" class="" data-code="<?php echo $key ?>">
                              <strong>+</strong>
                            </td> -->
                            <td class="expand" data-code="<?php echo $key ?>">
                              <?php echo $date ?>
                            </td>
                            <td>
                              <?php if( $value['type'] == 1){ ?>
                              <a  href="<?php echo $this->Html->url(array('action' => 'view','controller' => 'deals', $deal_id )); ?>" 
                                title="View Deal"
                                target="_blank">
                                <strong><?php echo $deal_code ?></strong>
                              </a>
                              <?php }else{
                                foreach( $deal_id as $ind => $id ){
                                  $deal_code = $deal_data[$id]['deal_code'];
                               ?> 
                                 <a   href="<?php echo $this->Html->url(array('action' => 'view','controller' => 'deals', $id )); ?>" 
                                  title="View Deal"
                                  target="_blank">
                                  <strong><?php echo $deal_code ?></strong>
                                </a>

                                <?php echo(isset($deal_data[$ind+1]))?", ":"" ?>

                              <?php }
                                } 
                              ?>
                            </td>
                            <td style="color:<?php echo $color ?>">
                              <?php echo $deal_status_text; ?>
                            </td>
                            <td style="text-align:right">
                              <?php echo ($debit)?"$ " .$this->MyHtml->formatNumber($debit):"-" ?>
                            </td>
                            <td style="text-align:right">
                              <?php echo ($credit)?"$ " .$this->MyHtml->formatNumber($credit):"-" ?>
                            </td>
                            <td>
                              <?php echo $method ?>
                            </td>
                            <td>
                              <?php echo $transferred_date ?>
                            </td>

                            <td style="font-weight:bold; text-align:right">
                              <?php echo ($balance)?"$ " .$this->MyHtml->formatNumber($balance):"-" ?>
                            </td>

                          </tr>

                        <?php 
                          endforeach;

                          }else{ ?>
                          <tr>
                            <td colspan="10"><i>No Transaction Found !</i></td>
                          </tr>

                        <?php } ?>
                        </tbody>
                      </table>
                  </div>
              </div>
          </div>

          <!-- Media Tab -->
          <div class="tab-pane <?php echo ($active_tab == 'media')?" active":"" ?>" id="tab-media">
              <legend><?php echo __('Merchant Media'); ?></legend>
          
              <div class="span6">
                <h5>Pictures</h5>
                <a href="#media_add_image" data-toggle="modal"> 
                  <button class="btn btn-linkedin" type="button"><i class="icon-picture"></i> Upload Pictures</button>
                </a>

                <div class="gallery no-padding" style="padding-left: 0px;">

                <?php 
                  $pictures = $business['Pictures'];

                  if( !empty($pictures) ){
                    foreach( $pictures as $key => $pic ){
                ?>
                  <div class="element" style="width: 150px ; height: 150px;" id="box-<?php echo $pic['id'] ?>" >
                    <a rel="shadowbox" href="<?php echo $this->webroot . $pic['media_path'] ?>">
                      <img src="<?php echo $this->webroot . $pic['media_path'] ?>" width="240" height="240" class="img-polaroid">
                    </a>
                    
                    <div  class="remove-btn"
                        href="#remove_img"
                        data-toggle="modal"
                        data-id="<?php echo $pic['id'] ?>"
                        title="remove" >
                      <img src="<?php echo $this->webroot ?>img/trash-red.png" alt="">
                    </div>

                  </div>  
                
                <?php
                    }
                  }
                ?>

                </div>

              </div>

              <div class="span6" style="margin-left:0px;">
                <h5>Videos</h5>
                <a href="#media_add_video" data-toggle="modal"> 
                  <button class="btn btn-linkedin" type="button"><i class="icon-plus"></i> Add Video</button>
                </a>

                <div style="padding-top: 20px;">
                  <?php 
                    $videos = $business['Videos'];

                    if( !empty($videos) ){
                      foreach( $videos as $key => $video ){

                        $src = str_replace("watch?v=", 'embed/', $video['media_embeded']);

                        echo "<div class='video_box' id='box-" . $video['id'] . "' >";
                  ?>
                        <iframe src="<?php echo $src ?>" 
                            frameborder="0" allowfullscreen>
                        </iframe>

                        <div  class="remove-btn1"
                            href="#remove_video"
                            data-toggle="modal"
                            data-id="<?php echo $video['id'] ?>"
                            title="remove" >
                          <img src="<?php echo $this->webroot ?>img/trash-red.png" alt="">
                        </div>

                  <?php
                        echo "</div>";
                      }
                    }
                  ?>
                </div>
              </div>
          </div>

          <!-- Deal Tab -->
          <div class="tab-pane <?php echo ($active_tab == 'deal')?" active":"" ?>" id="tab-deal">
              
              <legend><?php echo __('Merchant Deals'); ?></legend>

              <a href="#deal_add" data-toggle="modal"> 
                <button class="btn btn-linkedin" type="button"><i class="icon-plus"></i> Create New Deal</button>
              </a>

              <a class="btn btn-google pull-right" href="#" id="clear-search-btn" style="margin-right:0px;">
                <i class="icon icon-trash"></i>
                Clear Search
              </a>

              <a class="btn btn-skype pull-right" href="#" id="filter-btn">
                <i class="icon icon-search"></i>
                Deals Search
              </a>

              <div style="clear: both; padding-top: 20px;" ></div>
              
              	<?php 
                		$deals = $business['Deals'];

               	?>

  	            <table class="table-list">
  	              <tr>
  	                <th width="80px;'">Image</th>
  	                <th width="200px">Deal Title</th>
  	                <th width="200px">Detail</th>
  	                <th width="200px">Validity</th>
  	                <th>Status</th>
                    <th width="100px;">Deal of the Day</th>
  	                <th width="100px">Action</th>
  	              </tr>

  	              <?php if(!empty($deals) ){ 
                      
                        foreach( $deals as $key => $value ){ 
  	                
    	                  	$split = end(explode("/", $value['Deal']['image']));
    	                  	$thumb_img =  "img/deals/thumbs/" . $split ;

      	                  $other_images = $value['Deal']['other_images'];
      			             
                          if( $other_images ){
      			                
      			                $other_images = json_decode($other_images);
      			                foreach( $other_images as $k => $img ){

      			                  if( $img != "img/deals/default.jpg" ){
      			                    @$index ++;
      			                    $spt    = end(explode("/", $img ));
      			                    $thumb  = "img/deals/thumbs/" . $spt;

      			                    $data_images[$value['Deal']['id']][@$index]['img'] = $img;
      			                    $data_images[$value['Deal']['id']][@$index]['thumb'] = $thumb;
      			                  }
      			                }

      			              }else if($value['Deal']['image'] != "img/deals/default.jpg" ) {
        			              $data_images[$value['Deal']['id']][@$index]['img']   = $value['Deal']['image'];
        			              $data_images[$value['Deal']['id']][@$index]['thumb']  = $thumb_img;
        			            }

        			             $selected = @$data_images[$value['Deal']['id']][array_rand($data_images[$value['Deal']['id']])];

        			            if( $selected ){ 
        			                $selected_img   = $selected['img'];
        			                $selected_thumb = $selected['thumb']; 
        			            }else{
        			                $selected_img   = "img/deals/default.jpg" ;
        			                $selected_thumb = "img/deals/thumbs/default.jpg" ;
        			            }

                        $value['Deal']['valid_from'] = date( 'd-m-Y H:i:s', strtotime($value['Deal']['valid_from']));
                        $value['Deal']['valid_to'] = date( 'd-m-Y H:i:s', strtotime($value['Deal']['valid_to']));

                        $dod_list = "";
                        if( $value['DealOfTheDay'] ){
                          $deal_of_the_days = $value['DealOfTheDay'];

                          $now = date('Y-m-d');
                          foreach( $deal_of_the_days as $indx => $v ){

                              if( $v['available_date'] >= $now ){
                                $dod_list .= "- " . date( "d-F-Y",  strtotime($v['available_date'])) . "<br>" ;
                              }
                          }
                        }

                        $dealCategories = $value['Deal']['goods_category'];
                        $dealCategories = json_decode($dealCategories);

                        $cate_display = "";
                        foreach( $dealCategories as $val ){
                            if( isset($goods_category[$val]) ){
                              $cate_display .= $goods_category[$val] . ", ";
                            }else{
                              $cate_display .= $val . ", ";
                            }
                        }

                        $cate_display = rtrim($cate_display, ", ");

  	                ?>
  	                <tr>
  	                  <td>
  	                    <a href="<?php echo $this->webroot . $selected_img ?>"  rel="shadowbox">
  			                <img src="<?php echo $this->webroot . $selected_thumb ?>" alt=""
  			                 style="max-width:90px; max-height:80px; height: auto; padding: 3px; border: 1px solid #CCC;" >
  			              </a>
  	                  </td>
  	                  <td>
                          Code : 
                          <a href="#" data-toggle="modal" class="btn-view-deal"
                           data-id="<?php echo $value['Deal']['id'] ?>"
                           title="View Deal Detail"
                           data-category="<?php echo $cate_display ?>"
                           data-from="<?php echo date('d-M-Y h:i:s A', strtotime($value['Deal']['valid_from'])) ?>"
                           data-to="<?php echo date('d-M-Y h:i:s A', strtotime($value['Deal']['valid_to'])) ?>"
                           data-posted-date="<?php echo date('d-M-Y h:i:s A', strtotime($value['Deal']['created'])) ?>" >
                            <strong style="color:#06F"><?php echo $value['Deal']['deal_code'] ?></strong>
                          </a><br>
                          <strong>Title :</strong> 
                          <a href="#" data-toggle="modal" class="btn-view-deal"
                           data-id="<?php echo $value['Deal']['id'] ?>"
                           title="View Deal Detail"
                           data-category="<?php echo $cate_display ?>"
                           data-from="<?php echo date('d-M-Y h:i:s A', strtotime($value['Deal']['valid_from'])) ?>"
                           data-to="<?php echo date('d-M-Y h:i:s A', strtotime($value['Deal']['valid_to'])) ?>"
                           data-posted-date="<?php echo date('d-M-Y h:i:s A', strtotime($value['Deal']['created'])) ?>" >
                              <?php echo urldecode($value['Deal']['title']) ?>
                          </a><br>
                          <strong>Category : </strong><?php echo $cate_display ?>
                      </td>
  	                  <td>
  	                    <b>Created Date: </b>
  	                    <?php echo date( 'd-F-Y h:i:s A', strtotime($value['Deal']['created']) ) ?> , 
                            By : <?php echo urldecode($value['CreatedBy']['last_name'] . " " . $value['CreatedBy']['first_name'] ) ?>
    
                        <?php 
                          $updated_by = ($value['Deal']['updated_by'])?'UpdatedBy':'CreatedBy' ;
                         ?>
  	                    <br><b>Last Updated: </b>
  	                    <?php echo date( 'd-F-Y h:i:s A', strtotime($value['Deal']['modified']) ) ?> , 
                          By : <?php echo urldecode($value[$updated_by]['last_name'] . " " . $value[$updated_by]['first_name'] ) ?>
  	                  </td>
  	                  <td style="vertical-align: top">From : <?php echo date( 'd-F-Y h:i:s A', strtotime($value['Deal']['valid_from']) ) ?><br>
  	                      To : <?php echo date( 'd-F-Y h:i:s A', strtotime($value['Deal']['valid_to']) ) ?></td>
  	                  <td style="vertical-align: top">
  	                    <?php 
  	                      if( $value['Deal']['status'] != 0 && (strtotime($value['Deal']['valid_to']) < time()) ){
  	                        $value['Deal']['status'] = '-1' ;
  	                      } 

                          $last_minute = "";
                          if( $value['Deal']['is_last_minute_deal'] ){
                            $last_minute = "Yes";
                          }
  	                    ?>
  	                    <strong>Status : </strong> <span class="deal-status label label-<?php echo $deal_status[$value['Deal']['status']]['color'] ?>">
  	                      <?php echo h($deal_status[$value['Deal']['status']]['status']); ?>
  	                    </span>
                        <?php if( $last_minute){ ?>
                          <br>Last Minute Deal : <strong style="color:#06F"><?php echo $last_minute ?></strong>
                        <?php } ?>
  	                  </td>

                      <td>
                        <?php echo $dod_list ?>
                      </td>

  	                  <td class="actions" style="vertical-align: top; text-align:right">

  	                    <a href="#" data-toggle="modal" class="btn btn-linkedin btn-view-deal"
                           data-id="<?php echo $value['Deal']['id'] ?>"
                           title="View Deal Detail"
                           data-category="<?php echo $cate_display ?>"
                           data-from="<?php echo date('d-M-Y h:i:s A', strtotime($value['Deal']['valid_from'])) ?>"
                           data-to="<?php echo date('d-M-Y h:i:s A', strtotime($value['Deal']['valid_to'])) ?>"
                           data-posted-date="<?php echo date('d-M-Y h:i:s A', strtotime($value['Deal']['created'])) ?>" >
                           <i class="icon icon-eye-open"
                           style="padding-right:0px;"></i></a>

                        <a  href="#deal_edit" 
                          data-toggle="modal"
                          data-bisid ="<?php echo $business['Business']['id'] ?>"
                          data-id="<?php echo $value['Deal']['id'] ?>"
                          data-detail='<?php echo json_encode($value['Deal']) ?>' 
                          class="edit_deal"
                          title="Edit Deal"
                          data-action = "<?php echo $this->Html->url(array('action' => 'editDeal', $business['Business']['id'])); ?>" >
                          <button class="btn btn-linkedin" type="button" style="margin-left:0px;">
                              <i class="icon-edit" style="padding-right:0px;"></i>
                          </button>
                        </a>

                        <a  href="<?php echo $this->Html->url(array('action' => 'repost', 'controller' => 'deals', $value['Deal']['id'], $business['Business']['id'] )); ?>" 
                          title="Repost Deal" >
                          <button class="btn btn-skype" type="button" style="margin-left:0px;">
                              <i class="icon-refresh" style="padding-right:0px;"></i>
                          </button>

                        </a>

    			              <div style="clear:both; padding-top:4px;"></div>

      			              <a  href="#"
      			                  data-bisid ="<?php echo $business['Business']['id'] ?>"
      			                  data-id="<?php echo $value['Deal']['id'] ?>"
      			                  class="deal_images btn btn-foursquare"
      			                  data-action="" 
                              title="Deal Images">
                              <i class="icon-picture" style="padding-right:0px;"></i>
      			              </a>
              
                          <?php if($permission == 6 ){ ?>
                            <a  href="#deal_delete"
                              data-toggle="modal"
                              data-bisid ="<?php echo $business['Business']['id'] ?>"
                              data-id="<?php echo $value['Deal']['id'] ?>"
                              class="detele_deal" 
                              data-action = "<?php echo $this->Html->url(array('action' => 'deleteDeal', $business['Business']['id'], $value['Deal']['id'])); ?>" title="Delete Deal">
                              <button class="btn btn-google" type="button" style="margin-left:0px;">
                               <i class="icon-trash" style="padding-right:0px;"></i> 
                              </button>
                            </a>
                          <?php } ?>


  	                  </td>
  	                </tr>
  	              <?php } ?>

  	              <?php }else{ ?>
  	                <tr>
  	                  <td colspan="7"><i>There is no deal yet.</i></td>
  	                </tr>
  	              
  	              <?php } ?>
  	            </table>

                <p class="page">
                  <?php
                  echo $this->Paginator->counter(array(
                  'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total')
                  ));
                  ?>  
                </p>

                <div class="paging">
                <?php
                  echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
                  echo $this->Paginator->numbers(array('separator' => ''));
                  echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
                ?>
                </div>
                
          </div>
	         
          <!-- Menu Tab -->
		      <div class="tab-pane <?php echo ($active_tab == 'menu')?" active":"" ?>" id="tab-menu">

            <div class="span4">

                <legend><?php echo __('Category'); ?></legend>

                <?php 
                    $menuCategories = $business['MenuCategories'];
                ?>

                <a href="#" id="add-menu-category">
                    <button class="btn btn-linkedin" type="button"><i class="icon-plus"></i> Add New Category</button>
                </a>
                <div style="clear: both; padding-top: 20px;" ></div>

                <table class="table-list" id="category-table-list">

                    <thead>
                        <th width="30px;"><?php echo __('No'); ?></th>
                        <th><?php echo __('Category Name'); ?></th>
                        <th width="50px;"><?php echo __('Items') ?></th>
                        <th width="70px;"><?php echo __('Action'); ?></th>
                    </thead>

                    <tbody>
                        <?php 
                            if( $menuCategories ){
                                foreach( $menuCategories as $k => $val ){
                                    
                                    $total = isset($array_total_items[$val['id']])?$array_total_items[$val['id']]:0 ;

                                    $selected = ($menu_id == $val['id'] )?" selected":"" ;

                        ?>
                            <tr class="menu-row <?php echo $selected ?>" data-id="<?php echo $val['id'] ?>">
                              <td><?php echo $k + 1; ?></td>
                              <td class="name" ><?php echo $val['name'] ?></td>
                              <td style="text-align:center; font-weight:bold"><?php echo $total ?></td>
                              <td style="text-align:right">
                                <a href="#" class="btn btn-foursquare edit-menu-category" style="font-size:12px;"
                                    data-action="<?php echo $this->Html->url(array('action' => 'saveMenuCategory', $business['Business']['id'], $val['id'])); ?>">
                                    <i class="icon-edit"></i>
                                </a>

                                <a href="#" class="btn btn-google delete-menu-category" style="font-size:12px;"
                                    data-action="<?php echo $this->Html->url(array('action' => 'deleteMenuCategory', $business['Business']['id'], $val['id'])); ?>">
                                    <i class="icon-trash"></i>
                                </a>
                              </td>
                            </tr>
                        <?php 
                                }
                            }else{
                        ?>
                            <tr>
                                <td colspan="4"><i>Empty Category.</i></td>
                            </tr>
                        <?php 
                            }
                        ?>
                    </tbody>

                </table>

            </div>

            <div class="span8">
                <legend><?php echo __('Menu/ Items'); ?></legend>

                <h5 class="pull-left" style="margin-bottom:0px; color:#06F" id="menu-name"></h5>
                <a href="#" id="add-menu-item" class='pull-right' menu-id="0"
                    data-action="<?php echo $this->Html->url(array('action' => 'saveMenuItem', $business['Business']['id'])); ?>">
                    <button class="btn btn-linkedin" type="button"><i class="icon-plus"></i> Add New Item</button>
                </a>

                <div class="span12" style="margin-left:0px; text-align:center; margin-top:50px; display:none;" id="loading-item">
                    <img src="<?php echo $this->webroot . "img/ajax-loader.gif" ?>" alt="">
                    <h5>Loading...</h5>
                </div>

                <div class="span12" style="margin-left:0px;" id="item-container">
                    
                    <div style="clear: both; padding-top: 20px;" ></div>
                    <table class="table-list" id="menu-item-table">

                        <thead>
                            <th width="30px;"><?php echo __('No'); ?></th>
                            <th width="100px"><?php echo __('Image') ?></th>
                            <th><?php echo __('Title'); ?></th>
                            <th width="100px;"><?php echo __('Price') ?></th>
                            <th width="110px;"><?php echo __('Action'); ?></th>
                        </thead>

                        <tbody>
                                <tr class="item-row" style="display:none">
                                    <td class='item-no' ></td>
                                    <td class='item-image'>
                                        <a href="#"  rel="shadowbox">
                                          <img src="#" alt=""
                                           style="max-width:100px; padding: 2px; border: 1px solid #CCC;" >
                                        </a>
                                    </td>
                                    <td class='item-title'></td>
                                    <td class='item-price' style="font-weight: bold; color: #06F; text-align:right"></td>
                                    <td class='item-action' style="text-align:right">
                                        <!-- View -->
                                        <a href="#" class="btn btn-skype view-menu-item" style="font-size:12px;">
                                            <i class="icon-search"></i>
                                        </a>

                                        <!-- Edit -->
                                        <a href="#" class="btn btn-foursquare edit-menu-item" style="font-size:12px;"
                                            data-action="<?php echo $this->Html->url(array('action' => 'saveMenuItem', $business['Business']['id'])); ?>">
                                            <i class="icon-edit"></i>
                                        </a>
                                        
                                        <!-- Delete -->
                                        <a href="#" class="btn btn-google delete-menu-item" style="font-size:12px;"
                                            data-action="<?php echo $this->Html->url(array('action' => 'deleteMenuItem', $business['Business']['id'])); ?>">
                                            <i class="icon-trash"></i>
                                        </a>
                                    </td>
                                </tr>

                                <tr id="menu-item-empty">
                                    <td colspan="5">
                                        <i>Empty item.</i>
                                    </td>
                                </tr>
                        </tbody>

                    </table>
                </div>

                <div class="span12" style="margin-left:0px; text-align:center; margin-top:30px;">
                    <img src="<?php echo $this->webroot . "img/ajax-loader.gif" ?>" alt="" id="load-more-img" style="display:none" >
                    <button id="load-more-btn" type="button" class="btn btn-linkedin" style="display:none">Load More</button>
                </div>

            </div>
          
          </div>

        </div>        
        
      </div>

    </div>

  </div>

</div>

<style>

  .chosen-container ,.chosen-container-single{
    width: 100% !important;
  }


  #UserDobMonth + div.chosen-container-single{
    width: 32% !important;
  }

  #UserDobDay + div.chosen-container-single{
    width: 32% !important;
  }

  #UserDobYear + div.chosen-container-single{
    width: 32% !important;
  }

  /*.checkbox{
    width:100% !important;
  }*/


  .remove-btn{
    display: block; 
    position: absolute; 
    width:20px; 
    height: 20px; 
    /*border: 1px solid red; */
    background-color: white;
    padding:1px;
    bottom:15px;
    right: 8px;
  }
  .remove-btn img{
    width:100%; 
    height: 100%; 
  }

  .remove-btn:hover{
    cursor: pointer;
  }

  .remove-btn1{
    display: block; 
    position: absolute; 
    width:20px; 
    height: 20px; 
    /*border: 1px solid red; */
    background-color: white;
    padding:1px;
    bottom:0px;
    right: 0px;
  }
  
  .remove-btn1 img{
    width:100%; 
    height: 100%; 
  }

  .remove-btn1:hover{
    cursor: pointer;
  }

</style>


<!-- Include View for Business Category and Items -->
<?php include( dirname(__FILE__) . "/business-menu-modal.ctp" ); ?>

<?php include( dirname(__FILE__) . "/deal-modal.ctp" ); ?>
<?php include( dirname(__FILE__) . "/media-modal.ctp" ); ?>
<?php include( dirname(__FILE__) . "/member-modal.ctp" ); ?>


<div id="warning-modal" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true"
  style="">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel" style="color:red"><i class="icon-exclamation"></i> Warning !</h3>
  </div>

  <div class="span6" style="padding-top: 10px; padding-bottom: 20px;">
    
    <h5 id="warning-modal"></h5>
    <button class="btn btn-google cancel" data-dismiss='modal' type="button">Close</button>

  </div>
</div> 


<!-- Operation Hour -->

<div id="operationHourEdit" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true"
  style="padding-bottom: 30px !important;">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel"><i class="icon-cog"></i> Edit Operaton Hours</h3>
  </div>

  <!-- Any Info-customization -->
  <form action="<?php echo $this->Html->url(array('action' => 'editOperation', $business['Business']['id'])); ?>" 
      style="padding: 20px;" method="POST" id="operationHourEditForm" >
    
    <div class="span6">
      <table width="100% !important">
        <tr>
          <td>Monday</td>
          <td>
            <input type="hidden" name="OperationHour[mon][id]" id="monID">
            <select name="OperationHour[mon][f]" style="width:100px !important;" id="MonFrom" class="from">
              <?php foreach($operation_hours as $key => $hour ): ?>
                <option value="<?php echo $key ?>"><?php echo $hour ?></option>
              <?php endforeach; ?>
            </select>
          </td>
          <td>
            <select name="OperationHour[mon][t]" style="width:100px !important;" id="MonTo" class="to">
              <?php foreach($operation_hours as $key => $hour ): ?>
                <option value="<?php echo $key ?>"><?php echo $hour ?></option>
              <?php endforeach; ?>
            </select>
          </td>
        </tr>

        <tr>
          <td>Tuesday</td>
          <td>
            <input type="hidden" name="OperationHour[tues][id]" id="tuesID">
            <select name="OperationHour[tues][f]" style="width:100px !important;" id="TuesFrom" class="from">
              <?php foreach($operation_hours as $key => $hour ): ?>
                <option value="<?php echo $key ?>"><?php echo $hour ?></option>
              <?php endforeach; ?>
            </select>
          </td>
          <td>
            <select name="OperationHour[tues][t]" style="width:100px !important;" id="TuesTo" class="to">
              <?php foreach($operation_hours as $key => $hour ): ?>
                <option value="<?php echo $key ?>"><?php echo $hour ?></option>
              <?php endforeach; ?>
            </select>
          </td>
        </tr>
        
        <tr>
          <td>Wednesday</td>
          <td>
            <input type="hidden" name="OperationHour[wed][id]" id="wedID">
            <select name="OperationHour[wed][f]" style="width:100px !important;" id="WedFrom" class="from">
              <?php foreach($operation_hours as $key => $hour ): ?>
                <option value="<?php echo $key ?>"><?php echo $hour ?></option>
              <?php endforeach; ?>
            </select>
          </td>
          <td>
            <select name="OperationHour[wed][t]" style="width:100px !important;" id="WedTo" class="to">
              <?php foreach($operation_hours as $key => $hour ): ?>
                <option value="<?php echo $key ?>"><?php echo $hour ?></option>
              <?php endforeach; ?>
            </select>
          </td>
        </tr>
        
        <tr>
          <td>Thursday</td>
          <td>
            <input type="hidden" name="OperationHour[thur][id]" id="thurID">
            <select name="OperationHour[thur][f]" style="width:100px !important;" id="ThurFrom" class="from">
              <?php foreach($operation_hours as $key => $hour ): ?>
                <option value="<?php echo $key ?>"><?php echo $hour ?></option>
              <?php endforeach; ?>
            </select>
          </td>
          <td>
            <select name="OperationHour[thur][t]" style="width:100px !important;" id="ThurTo" class="to">
              <?php foreach($operation_hours as $key => $hour ): ?>
                <option value="<?php echo $key ?>"><?php echo $hour ?></option>
              <?php endforeach; ?>
            </select>
          </td>
        </tr>
        
        <tr>
          <td>Friday</td>
          <td>
            <input type="hidden" name="OperationHour[fri][id]" id="friID">
            <select name="OperationHour[fri][f]" style="width:100px !important;" id="FriFrom" class="from">
              <?php foreach($operation_hours as $key => $hour ): ?>
                <option value="<?php echo $key ?>"><?php echo $hour ?></option>
              <?php endforeach; ?>
            </select>
          </td>
          <td>
            <select name="OperationHour[fri][t]" style="width:100px !important;" id="FriTo" class="to">
              <?php foreach($operation_hours as $key => $hour ): ?>
                <option value="<?php echo $key ?>"><?php echo $hour ?></option>
              <?php endforeach; ?>
            </select>
          </td>
        </tr>
        
        <tr>
          <td>Saturday</td>
          <td>
            <input type="hidden" name="OperationHour[sat][id]" id="satID">
            <select name="OperationHour[sat][f]" style="width:100px !important;" id="SatFrom" class="from">
              <?php foreach($operation_hours as $key => $hour ): ?>
                <option value="<?php echo $key ?>"><?php echo $hour ?></option>
              <?php endforeach; ?>
            </select>
          </td>
          <td>
            <select name="OperationHour[sat][t]" style="width:100px !important;" id="SatTo" class="to">
              <?php foreach($operation_hours as $key => $hour ): ?>
                <option value="<?php echo $key ?>"><?php echo $hour ?></option>
              <?php endforeach; ?>
            </select>
          </td>
        </tr>
        
        <tr>
          <td>Sunday</td>
          <td>
            <input type="hidden" name="OperationHour[sun][id]" id="sunID">
            <select name="OperationHour[sun][f]" style="width:100px !important;" id="SunFrom" class="from">
              <?php foreach($operation_hours as $key => $hour ): ?>
                <option value="<?php echo $key ?>"><?php echo $hour ?></option>
              <?php endforeach; ?>
            </select>
          </td>
          <td>
            <select name="OperationHour[sun][t]" style="width:100px !important;" id="SunTo" class="to">
              <?php foreach($operation_hours as $key => $hour ): ?>
                <option value="<?php echo $key ?>"><?php echo $hour ?></option>
              <?php endforeach; ?>
            </select>
          </td>
        </tr>
        
      </table>
    </div>

    <div class="submit" style="margin-top: 20px; clear: both; padding-top:20px; padding-left:20px;">
      <input class="btn btn-primary" type="submit" value="Save">
      <button class="btn btn-google cancel" data-dismiss='modal' type="button">Cancel</button>
    </div>
      
  </form>

</div> 

<!-- Change Logo -->

<div id="logo_change" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true"
  style="padding-bottom: 30px !important; ">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel"><i class="icon-edit"></i> Change Business Logo</h3>
  </div>

  <!-- Any Info-customization -->
  <form action="<?php echo $this->Html->url(array('action' => 'changeLogo', $business['Business']['id'])); ?>" 
      style="padding: 20px;" method="POST" id="ChangeLogoForm"
      enctype = "multipart/form-data" >

    <div>
      <h5>Select new logo ( JPG, JPEG, PNG ). Minimume size 225 x 225 pixels.</h5>
      <input type="file" name="logo" required="required">
    </div>

    <div class="submit" style="margin-top: 20px; clear: both;">
      <input class="btn btn-primary" type="submit" value="Upload">
      <button class="btn btn-google cancel" data-dismiss='modal' type="button">Cancel</button>
    </div>
      
  </form>

</div> 


<!-- Remove Business Logo -->
<div id="removeLogo" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true"
  style="padding-bottom: 30px !important; ">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel"><i class="icon-edit"></i> Are You Sure ?</h3>
  </div>

  <!-- Any Info-customization -->
  <form action="<?php echo $this->Html->url(array('action' => 'removeLogo', $business['Business']['id'])); ?>" 
      style="padding: 20px;" method="POST" id="ChangeLogoForm"
      enctype = "multipart/form-data" >

    <div>
      <h5>Are you sure you want to remove this logo?</h5>
    </div>

    <div class="submit" style="margin-top: 20px; clear: both;">
      <input class="btn btn-primary" type="submit" value="Yes">
      <button class="btn btn-google cancel" data-dismiss='modal' type="button">No</button>
    </div>
      
  </form>

</div> 

<!-- Revenue -->

<div id="search-revenue-modal" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel"><i class="icon-search"></i> Filter Revenue</h3>
  </div>

  <!-- Any Info-customization -->
  <form action="<?php echo $this->Html->url(array('action' => 'view', $business['Business']['id'], "revenue" )); ?>" 
        style="padding: 20px;" method="POST" id="transactionSearchForm"
        enctype = "multipart/form-data" >

    <div style="width:100%;">

    <fieldset>

      <div style="display:block; clear:both">
        <label for="BuyerTransactionPurchaseDate" style="float:left; width:100px; padding-top:5px;">Purchase Date</label>
        <div id="dealValidFromPicker" class="input-append date datetimepicker" style="float:left">
                  <input data-format="dd-MM-yyyy" type="text" 
                      name="DateFrom"
                      style="width: 135px"
                      value="<?php echo @$revenueStates['DateFrom'] ?>"
                      placeholder="From">
                  <span class="add-on">
                    <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                    </i>
                  </span>
              </div>
        <span style="float:left; margin-left:8px; margin-right:7px; padding-top:5px;">TO</span>
        <div id="dealValidFromPicker" class="input-append date datetimepicker" style="float:left">
                  <input data-format="dd-MM-yyyy" type="text" 
                      name="DateTo"
                      style="width: 135px"
                      value="<?php echo @$revenueStates['DateTo'] ?>"
                      placeholder="To">
                  <span class="add-on">
                    <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                    </i>
                  </span>
              </div>
        
      </div>

    </fieldset>

    </div>

    <div class="submit" style="margin-top: 40px; clear: both;">
      <input class="btn btn-primary" type="submit" value="Submit Search">
      <button type="button" class="btn btn-linkedin reset-revenue-btn">Reset</button>
      <button class="btn btn-google cancel" data-dismiss='modal' type="button">Close</button>
    </div>
      
  </form>

</div>

<!-- Balance -->


<div id="search-balance-modal" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel"><i class="icon-search"></i> Filter Balance</h3>
  </div>

  <!-- Any Info-customization -->
  <form action="<?php echo $this->Html->url(array('action' => 'view', $business['Business']['id'], "balance" )); ?>" 
        style="padding: 20px;" method="POST" id="balanceSearchForm"
        enctype = "multipart/form-data" >

    <div style="width:100%;">

    <fieldset>

      <div style="display:block; clear:both">
        <label for="BuyerTransactionPurchaseDate" style="float:left; width:100px; padding-top:5px;">Purchase Date</label>
        <div id="dealValidFromPicker" class="input-append date datetimepicker" style="float:left">
                  <input data-format="dd-MM-yyyy" type="text" 
                      name="DateFrom"
                      style="width: 135px"
                      value="<?php echo @$balanceStates['DateFrom'] ?>"
                      placeholder="From">
                  <span class="add-on">
                    <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                    </i>
                  </span>
              </div>
        <span style="float:left; margin-left:8px; margin-right:7px; padding-top:5px;">TO</span>
        <div id="dealValidFromPicker" class="input-append date datetimepicker" style="float:left">
                  <input data-format="dd-MM-yyyy" type="text" 
                      name="DateTo"
                      style="width: 135px"
                      value="<?php echo @$balanceStates['DateTo'] ?>"
                      placeholder="To">
                  <span class="add-on">
                    <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                    </i>
                  </span>
              </div>
        
      </div>

    </fieldset>

    </div>

    <div class="submit" style="margin-top: 40px; clear: both;">
      <input class="btn btn-primary" type="submit" value="Submit Search">
      <button type="button" class="btn btn-linkedin reseet-balance-btn">Reset</button>
      <button class="btn btn-google cancel" data-dismiss='modal' type="button">Close</button>
    </div>
      
  </form>

</div> 

<!-- Warning Message -->
<div id="message" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true"
  style="z-index:999999">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel"><i class="icon-exclamation"></i> Warning !</h3>
  </div>

  <div class="span6" style="padding-top: 10px; padding-bottom: 20px;">
    
    <h5 id="message"></h5>

    <button class="btn btn-google cancel" data-dismiss='modal' type="button">Close</button>

  </div>

</div> 

<a href="#message" data-toggle="modal" id="message"></a>


<div id="search-deal-modal" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel"><i class="icon-search"></i> Search Filter</h3>
  </div>

  <!-- Any Info-customization -->
  <form action="<?php echo $this->Html->url(array('action' => 'view', $business['Business']['id'], 'deal')); ?>" 
        style="padding: 20px;" method="POST" id="searchDealForm"
        enctype = "multipart/form-data" >

    <div style="width:100%;">

    <fieldset>

      <div style="display:block; clear:both">
        <label for="dealTitle" style="float:left; width:100px; padding-top:5px;">Deal Code</label>
        <input  name="deal-code" 
            placeholder="Deal Code" 
            style="float:left; width:390px;" 
            id="dealTitle" 
            value="<?php echo @$dealStates['deal-code'] ?>"
            maxlength="100" type="text" >
      </div>

      <div style="display:block; clear:both">
        <label for="dealTitle" style="float:left; width:100px; padding-top:5px;">Deal Title</label>
        <input  name="deal-title" 
            placeholder="Deal Title" 
            style="float:left; width:390px;" 
            id="dealTitle" 
            value="<?php echo @$dealStates['deal-title'] ?>"
            maxlength="100" type="text" >
      </div>

      <div style=" clear:both">
        <label style="float:left; width:100px; padding-top:5px;">Category</label>
        <select name="deal-category" id="deal-category" style="width:410px;">
          <option value="">Any category</option>
          <?php foreach( $goods as $k => $v ){ 
              $selected = ( isset($dealStates['deal-category']) && $dealStates['deal-category'] == $v['GoodsCategory']['id'] )?" selected='selected'":"" ;
          ?>
            <option value="<?php echo $v['GoodsCategory']['id'] ?>" <?php echo $selected ?>><?php echo $v['GoodsCategory']['category'] ?></option>
          <?php } ?>
        </select>
      </div>

      <div style="display:block; clear:both">
        <label for="BuyerTransactionPurchaseDate" style="float:left; width:100px; padding-top:5px;">Posted Date</label>
        <div id="dealValidFromPicker" class="input-append date datetimepicker" style="float:left">
                  <input data-format="dd-MM-yyyy" type="text" 
                      name="postedFrom"
                      style="width: 140px"
                      value="<?php echo @$dealStates['postedFrom'] ?>"
                      placeholder="From">
                  <span class="add-on">
                    <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                    </i>
                  </span>
              </div>
        <span style="float:left; margin-left:8px; margin-right:7px; padding-top:5px;">TO</span>
        <div id="dealValidFromPicker" class="input-append date datetimepicker" style="float:left">
                  <input data-format="dd-MM-yyyy" type="text" 
                      name="postedTo"
                      style="width: 133px"
                      value="<?php echo @$dealStates['postedTo'] ?>"
                      placeholder="To">
                  <span class="add-on">
                    <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                    </i>
                  </span>
              </div>
        
      </div>

      <div style="display:block; clear:both">
        <label for="BuyerTransactionReceivedDate" style="float:left; width:100px; padding-top:5px;">Validity</label>
        <div id="dealValidFromPicker" class="input-append date datetimepicker" style="float:left">
                  <input data-format="dd-MM-yyyy" type="text" 
                      name="valideFrom"
                      style="width: 140px"
                      value="<?php echo @$dealStates['valideFrom'] ?>"
                      placeholder="From">
                  <span class="add-on">
                    <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                    </i>
                  </span>
              </div>
        <span style="float:left; margin-left:8px; margin-right:7px; padding-top:5px;">TO</span>
        <div id="dealValidFromPicker" class="input-append date datetimepicker" style="float:left">
              <input data-format="dd-MM-yyyy" type="text" 
                  name="validTo"
                  style="width: 133px"
                  value="<?php echo @$dealStates['validTo'] ?>"
                  placeholder="To">
              <span class="add-on">
                <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                </i>
              </span>
          </div>
      </div>

      <div style="display:block; clear:both">
        <label for="BuyerTransactionPurchaseDate" style="float:left; width:100px; padding-top:5px;">Publisehd Date</label>
        <div id="dealValidFromPicker" class="input-append date datetimepicker" style="float:left">
                  <input data-format="dd-MM-yyyy" type="text" 
                      name="publishedFrom"
                      style="width: 140px"
                      value="<?php echo @$dealStates['publishedFrom'] ?>"
                      placeholder="From">
                  <span class="add-on">
                    <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                    </i>
                  </span>
              </div>
        <span style="float:left; margin-left:8px; margin-right:7px; padding-top:5px;">TO</span>
        <div id="dealValidFromPicker" class="input-append date datetimepicker" style="float:left">
                  <input data-format="dd-MM-yyyy" type="text" 
                      name="publishedTo"
                      style="width: 133px"
                      value="<?php echo @$dealStates['publishedTo'] ?>"
                      placeholder="To">
                  <span class="add-on">
                    <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                    </i>
                  </span>
              </div>
        
      </div>

      <div style="display:block; clear:both">
        <label style="float:left; width:100px; padding-top:5px;">Deal Status</label>
        <select name="deal-status" id="deal-status" style="width:410px;">
          <option value="all">Any status</option>
          <?php foreach( $deal_status as $k => $v ){ 
              $selected = ( isset($dealStates['deal-status']) && $dealStates['deal-status'] != 'all' && $dealStates['deal-status'] == $k )?" selected='selected'":"" ;
          ?>
            <option value="<?php echo $k ?>" <?php echo $selected ?>><?php echo $v['status'] ?></option>
          <?php } ?>
        </select>
      </div>

      <div style="display:block; clear:both">
        <label for='filter-last-minute' style="float:left; width:100px; padding-top:3px;">Last Minute Deal</label>
        <?php 

          $checked = (isset($dealStates['last-minute']) && $dealStates['last-minute'] == 1)?" checked='checked'":"" ;
         ?>
        <input id="filter-last-minute" type="checkbox" value="1" name="last-minute" style="flaot:left; margin-left:10px;" <?php echo $checked ?>/>
      </div>

    </fieldset>

    </div>

    <div class="submit" style="margin-top: 40px; clear: both;">
      <input class="btn btn-primary" type="submit" value="Submit Search">
      <button type="button" class="btn btn-linkedin reset-btn">Reset</button>
      <button class="btn btn-google cancel" data-dismiss='modal' type="button">Close</button>
    </div>
      
  </form>

</div> 

<style>

  .equalto, .required, .type{
    color: red;
    font-size: 12px !important;
    list-style-type: none;
  }

  .parsley-error-list{
    margin: 0px !important;
    padding: 0px !important;
  }

</style>

<div id="pass-alert" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true"
  style="">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel"><i class="icon-exclamation"></i> Warning !</h3>
  </div>

  <div class="span6" style="padding-top: 10px; padding-bottom: 20px;">
    
    <h5 id="pass-alert"></h5>

    <button class="btn btn-google cancel" data-dismiss='modal' type="button">Close</button>
  </div>

</div> 

<a href="#pass-alert" data-toggle="modal" id="pass-alert"></a>

<script src="<?php echo $this->webroot . "js/admin/markerwithlabel.js" ?>" type="text/javascript"></script>
<script src="https://google-maps-utility-library-v3.googlecode.com/svn/tags/markerwithlabel/1.1.9/src/markerwithlabel.js" type="text/javascript"></script>

<script>  

	var table_listing_edit  = $('table#table-item-listing-edit').find('tbody');
    var item_row_model_edit = table_listing_edit.find('.related-item-row-edit');
    $('table#table-item-listing-edit').find('tbody').find('.related-item-row-edit').remove();

    var row_empty_edit = $('tr#row-empty-item-edit');

	$('select#MonFrom').change( function(){
		var val = $(this).val();

		$('select.from').val(val);

	})

	$('select#MonTo').change( function(){
		var val = $(this).val();

		$('select.to').val(val);

	})

  function validateEmail(){

	    var email = $("#FormMemberAdd").find("#UserEmail").val();
	    var id = null;

	    var result = true;

	    var url = '<?php echo $this->webroot ?>' + 'administrator/Users/checkDuplicateEmail/' + email + "/" + id ;

	    $.ajax({
	              type: 'POST',
	              url: url,
	              data: { email : email, id : id },
	              dataType: 'json',
	              async: false,
	              
	              success: function (data){
	                  console.log(data);

	                  if( data.status == 1 ){
	                    result = true;
	                  }else{
	                    result = false;
	                  }
	              },

	              error: function ( err ){
	                // console.log(err);
	              }

	          });

	    return result;
	
	}

	$("#save").click( function(){

	    if(validateEmail()){

	      var pass = $("#FormMemberAdd").find("#UserPassword").val().length;

	      if( pass < 6 ){
	        $("h5#pass-alert").text("Password must be at least 6 characters.");
	        $("a#pass-alert").click();
	        return false;
	      }

	      $("#FormMemberAdd").submit();
	    }else{
	      $("h5#message").text("This email is already been registered. Please try another email.");
	      $("a#message").click();
	      return false; 
	    }
	    return false;
	
	})

  	$("input#UserPhone").keydown(function (e) {

	    // Allow: backspace, delete, tab, escape, enter and .
	    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
           	// Allow: Ctrl+A
          	(e.keyCode == 65 && e.ctrlKey === true) || 
           	// Allow: home, end, left, right
          	(e.keyCode >= 35 && e.keyCode <= 39)) {
               // let it happen, don't do anything
               return;
      	}
	      // Ensure that it is a number and stop the keypress
	    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
	          e.preventDefault();
	    }

	});

  	function validateEmailEdit(){

	    var email = $("#memberEditForm").find("#UserEmail").val();
	    var id =$("#memberEditForm").find("#UserId").val();

	    var result = true;

	    var url = '<?php echo $this->webroot ?>' + 'administrator/Users/checkDuplicateEmail/' + email + "/" + id ;

	    $.ajax({
	              type: 'POST',
	              url: url,
	              data: { email : email, id : id },
	              dataType: 'json',
	              async: false,
	              
	              success: function (data){
	                  console.log(data);

	                  if( data.status == 1 ){
	                    result = true;
	                  }else{
	                    result = false;
	                  }
	              },

	              error: function ( err ){
	                console.log(err);
	              }

	          });

	    return result;
	
	}

	$("#edit_save").click( function(){

	    if(validateEmailEdit()){

	      var pass = $("#memberEditForm").find("#UserPasswordEdit").val().length;

	      if( pass > 0 && pass < 6 ){
	        $("h5#pass-alert").text("Password must be at least 6 characters.");
	        $("a#pass-alert").click();
	        return false;
	      }

	      $("#memberEditForm").submit();
	    }else{
	      $("h5#message").text("This email is already been registered. Please try another email.");
	      $("a#message").click();
	      return false; 
	    }
	    return false;

	})

	$("#editHour").click( function (){

	    var me = $(this);
	    var detail = me.data('detail');

	    var operationHourForm = $("#operationHourEdit");

	    operationHourForm.find("#monID").val(detail[0]['id']);
	    operationHourForm.find("#tuesID").val(detail[1]['id']);
	    operationHourForm.find("#wedID").val(detail[2]['id']);
	    operationHourForm.find("#thurID").val(detail[3]['id']);
	    operationHourForm.find("#friID").val(detail[4]['id']);
	    operationHourForm.find("#satID").val(detail[5]['id']);
	    operationHourForm.find("#sunID").val(detail[6]['id']);

	    operationHourForm.find("#MonFrom").val(detail[0]['from']);
	    operationHourForm.find("#MonTo").val(detail[0]['to']);


	    operationHourForm.find("#TuesFrom").val(detail[1]['from']);
	    operationHourForm.find("#TuesTo").val(detail[1]['to']);

	    operationHourForm.find("#WedFrom").val(detail[2]['from']);
	    operationHourForm.find("#WedTo").val(detail[2]['to']);

	    operationHourForm.find("#ThurFrom").val(detail[3]['from']);
	    operationHourForm.find("#ThurTo").val(detail[3]['to']);

	    operationHourForm.find("#FriFrom").val(detail[4]['from']);
	    operationHourForm.find("#FriTo").val(detail[4]['to']);

	    operationHourForm.find("#SatFrom").val(detail[5]['from']);
	    operationHourForm.find("#SatTo").val(detail[5]['to']);

	    operationHourForm.find("#SunFrom").val(detail[6]['from']);
	    operationHourForm.find("#SunTo").val(detail[6]['to']);

	});


	$(".delete").click( function(){
	    var cnf = confirm("Are you sure you want to deactivate this member?");

	    if( !cnf ){
	      return false;
	    }
	
	})

	$(".delete1").click( function(){
	    var cnf = confirm("Are you sure you want to activate this member?");

	    if( !cnf ){
	      return false;
	    }
  	
  	})


	$(".MemberEdit").click( function () {
	    
	    var me = $(this),
	      detail = me.data('detail'),
	      id  = me.data('id');

	    var edit_form = $("div#member_edit");

	    edit_form.find("#UserId").val(detail.id);
	    edit_form.find("#UserFirstName").val(detail.first_name);
	    edit_form.find("#UserLastName").val(detail.last_name);

	    edit_form.find("#UserStreet").val(detail.street);
	    edit_form.find("#UserPhone").val(detail.phone);
	    edit_form.find("#UserEmail").val(detail.email);
	    edit_form.find("#UserPosition").val(detail.position);
	    edit_form.find("#UserProvinceEdit").val(detail.province_code);
	    edit_form.find("#UserGender").val(detail.gender);
	    edit_form.find("#UserAccessLevel").val(detail.access_level);

	    $("#UserProvinceEdit").change();

	    setTimeout( function(){

	      edit_form.find("#UserCityEdit").val(detail.city_code);

	    },1000);

	    edit_form.find("#UserStatus").val(detail.status);

	    var dob = detail.dob.split('-');

	    var Y = dob[0];
	    var m = dob[1];
	    var d = dob[2];

	    edit_form.find("#UserDobMonth").val(m);
	    edit_form.find("#UserDobDay").val(d);
	    edit_form.find("#UserDobYear").val(Y);

	});


	$("#UserProvince").change( function (){

	    var me = $(this);
	    var citySelect  = $("#UserCity");
	    var pro_code = me.val();

	    var url = '<?php echo $this->webroot ?>' + 'administrator/users/get_city_by_province/' + pro_code ;

	    if( pro_code != '' ){
	      $.ajax({
	              type: 'POST',
	              url: url,
	              data: { pro_code : pro_code },
	              dataType: 'json',
	              
	              beforeSend: function(){
	                  // citySelect.parent().find('span').text('Loading...');
	                  // console.log("Loading...");
	                  citySelect.html("<option value=''>loading...</option>");
	              },

	              success: function (data){
	                  // console.log(data);

	                  var data = data.data;

	                  citySelect.html("<option value=''>select a city</option>")
	          
	                  $.each( data, function (ind, val ){
	                    var city = val.City;

	                    citySelect.append("<option value='" + city.city_code + "'>" + city.city_code + " - " + city.city_name + "</option>" )
	                  });


	              },

	              error: function ( err ){
	                console.log(err);
	              }

	          });

	    }else{

	      citySelect.html("<option value=''>select an option</option>");
	      locationSelect.html("<option value=''>select an option</option>");

	    }

	});


	$("#UserProvinceEdit").change( function (){
	    var me = $(this);

	    var citySelect    = $("#UserCityEdit");

	    var pro_code = me.val();

	    var url = '<?php echo $this->webroot ?>' + 'administrator/users/get_city_by_province/' + pro_code ;

	    if( pro_code != '' ){
	      $.ajax({
	              type: 'POST',
	              url: url,
	              data: { pro_code : pro_code },
	              dataType: 'json',
	              
	              beforeSend: function(){
	                  citySelect.html("<option value=''>loading...</option>");
	                  // console.log("Loading...");
	              },
	              success: function (data){
	                  // console.log(data);

	                  var data = data.data;

	                  citySelect.html("<option value=''>select a city</option>")
	          
	                  $.each( data, function (ind, val ){
	                    var city = val.City;

	                    citySelect.append("<option value='" + city.city_code + "'>" + city.city_code + " - " + city.city_name + "</option>" )
	                  });

	                  // citySelect.removeClass('chzn-done').show().next().remove();
	                // citySelect.chosen();

	              },

	              error: function ( err ){
	                console.log(err);
	              }

	          });

	    }else{

	      citySelect.html("<option value=''>select an option</option>");
	      locationSelect.html("<option value=''>select an option</option>");

	    }

	});


  	$("#province").change( function (){
      var me = $(this);

      var citySelect    = $("#city");
      var locationSelect  = $("#location");

      var pro_code = me.val();

      var url = '<?php echo $this->webroot ?>' + 'administrator/users/get_city_by_province/' + pro_code ;

      if( pro_code != '' ){
        $.ajax({
                type: 'POST',
                url: url,
                data: { pro_code : pro_code },
                dataType: 'json',
                
                beforeSend: function(){
                    citySelect.html("<option value=''>loading...</option>");
                    // console.log("Loading...");
                },
                success: function (data){
                    // console.log(data);

                    var data = data.data;

                    citySelect.html("<option value=''>select a city</option>")
                    locationSelect.html("<option value=''>select a location</option>");
                    $.each( data, function (ind, val ){
                      var city = val.City;

                      citySelect.append("<option value='" + city.city_code + "'>" + city.city_code + " - " + city.city_name + "</option>" )
                    });

                },

                error: function ( err ){
                  console.log(err);
                }

            });

      }else{
        citySelect.html("<option value=''>select an option</option>");
        locationSelect.html("<option value=''>select an option</option>");
      }
  	});


	$("#city").change( function (){
	    var me = $(this);

	    var locationSelect  = $("#location");

	    var city_code = me.val();

	    var url = '<?php echo $this->webroot ?>' + 'administrator/locations/get_location_by_city_code/' + city_code ;

	    if( city_code != '' ){
	      $.ajax({
	              type: 'POST',
	              url: url,
	              data: { city_code : city_code },
	              dataType: 'json',
	              
	              beforeSend: function(){
	                  locationSelect.html("<option value=''>loading...</option>");
	              },
	              success: function (data){
	                  // console.log(data);

	                  var data = data.data;

	              locationSelect.html("<option value=''>select a location</option>");
	                  $.each( data, function (ind, val ){
	                    var loc = val.Location;

	                    locationSelect.append("<option value='" + loc.location_name + "'>" + loc.location_name + "</option>" )
	                  });

	              },

	              error: function(xhr, status, error) {
	          var err = eval("(" + xhr.responseText + ")");
	            console.log(err);
	        }

	          });

	    }else{
	      locationSelect.html("<option value=''>select an option</option>");
	    }

	});


	$("#BusinessEditForm").submit( function(){

	    var province  = $("#BusinessEdit").find("#province").val(),
	      city    = $("#BusinessEdit").find("#city").val(),
	      location  = $("#BusinessEdit").find("#location").val();


	    if( province == "" || province == null ){
	      alert("Please select business province !!!"); return false;
	    }
	    if( city == "" || city == null){
	      alert("Please select business city !!!"); return false;
	    }
	    if( location == "" || location == null){
	      alert("Please select business location !!!"); return false;
	    }

	});

  $(".activate").click( function() {

	    var me    = $(this),
	      user_id = me.data('userid'),
	      action  = me.data('action'),
	      bis_id  = me.data('bisid');

    	var url = '<?php echo $this->webroot ?>' + 'administrator/businesses/';

	    if( action == 1 ){
	      url += 'activate/' + user_id + '/' + bis_id ;
	      var h5 = "Are you sure you want to activate this member?";
	    }else{
	      url += 'deactivate/' + user_id + '/' + bis_id ;
	      var h5 = "Are you sure you want to deactivate this member?";
	    }

	    $("form#ActivateUser").attr('action', url);
	    $("form#ActivateUser").find('h5').html(h5);

  	});


	$(".remove-btn").click ( function(){

	    var me = $(this),
	      id = me.data('id');

	    $('#yes').attr('data-id',id);     

	});

	$("#yes").click( function(){

	    var id = $(this).attr('data-id');

	    var url = '<?php echo $this->webroot ?>' + 'administrator/businesses/removeMedia/' + id ;

	    $.ajax({
	            type: 'POST',
	            url: url,
	            data: {},
	            dataType: 'json',
	            
	            beforeSend: function(){
	                // subCategory.parent().find('span').text('Loading...');
	            },
	            success: function (data){

			        console.log(data);

			        var boxId = "#box-" + id;

			        $(boxId).fadeOut(500);

	            },

	            error: function ( err ){
	              console.log(err);
	            }

        	});
  	
  	})


	$(".remove-btn1").click ( function(){

	    var me = $(this),
	      id = me.data('id');

	    $('#yes1').attr('data-id',id);      

	  });

		$("#yes1").click( function(){

	    var id = $(this).attr('data-id');

	    var url = '<?php echo $this->webroot ?>' + 'administrator/businesses/removeMedia/' + id ;

	    $.ajax({
	            type: 'POST',
	            url: url,
	            data: {},
	            dataType: 'json',
	            
	            success: function (data){

	        console.log(data);

	        var boxId = "#box-" + id;

	        $(boxId).fadeOut(500);

	            },

	            error: function ( err ){
	              console.log(err);
	            }

        });
  	});


	$(".edit_deal").click( function ( event ){

      event.preventDefault();

	    var me    = $(this),
	      deal_id = me.data('id'),
	      detail  = me.data('detail');

	    var action  = me.data('action');

	    action = action + "/" + deal_id;

	    var form = $("#dealEditForm");

	    form.attr('action', action);

	    form.find('#DealTitle').val(detail.title);

	    form.find('#old_image_path').val(detail.image);

      CKEDITOR.instances.DealDescription.setData(detail.description);
      CKEDITOR.instances.DealConditions.setData(detail.deal_condition);

	    $('#datetimepicker1').datetimepicker('setDate', detail.valid_from);
	    $('#datetimepicker2').datetimepicker('setDate', detail.valid_to);

	    var gccheckboxs = (form.find('#DealGoodsCategoryEdit').find('input[type=checkbox]'));
	    var selected = $.parseJSON(detail.goods_category);

	    gccheckboxs.each( function (i){
	      var val = gccheckboxs[i]['value'] ;
         gccheckboxs[i]['checked'] = false;
	      if( selected.indexOf(val) != -1 ){
	        gccheckboxs[i]['checked'] = true;
	      }
	    });

      var select_status = form.find('select#deal-status');

      select_status.val(detail.status);

      form.find('#merchant-branch').val(detail.branch_id);
      $('select#merchant-branch').change();


      if( detail.is_last_minute_deal == 1 ){
          form.find('input#last-minute').prop('checked' ,true);
      }else{
          form.find('input#last-minute').prop('checked' ,false);
      }

	    table_listing_edit.find('.related-item-row-edit').remove();
	    row_empty_edit.show();

	    // Get Related Item Link
	    var url = '<?php echo $this->webroot ?>' + 'administrator/businesses/getDealRelatedItemsAjax__' ;
	    $.ajax({
          	type: 'POST',
          	url: url,
         	data: { deal_id : deal_id },
          	dataType: 'json',

          	success: function( data ){
          		if( data.status == true && data.result.length > 0 ){
          			row_empty_edit.hide();
          			$.each( data.result , function( ind, val ){

          				var item_id 			= val.ItemDetail.id;
          				var discounted_price 	= val.DealItemLink.discount;
          				var original_price	 	= val.DealItemLink.original_price;
                  var available_qty     = val.DealItemLink.available_qty;

                  if( original_price == 0 ){
                    original_price    = val.ItemDetail.price;
                  }

                  if( available_qty == "" || available_qty == null ){
                    available_qty = 0;
                  }
          				
          				item_row_model_edit.find('.hidden-item-id').val(item_id);
                  item_row_model_edit.find('.hidden-item-original-price').val(original_price);
                  item_row_model_edit.find('.hidden-item-discount').val(discounted_price);
                  item_row_model_edit.find('.hidden-item-qty').val(available_qty);

			            // Biz Menu
			            var menu_name = val.ItemDetail.MenuDetail.name ;
			            var item_name = val.ItemDetail.title;

			            item_row_model_edit.attr('data-id', item_id);
			            item_row_model_edit.find('.item-menu').text(menu_name);
			            item_row_model_edit.find('.item-title').text(item_name);
			            item_row_model_edit.find('.item-price').text("$ " + parseFloat(original_price).toFixed(2));

                  item_row_model_edit.find('.item-discount-price').find('span').text("$ " + parseFloat(discounted_price).toFixed(2));
                  item_row_model_edit.find('.item-discount-price').find('input').val(discounted_price);
                  
                  item_row_model_edit.find('.item-available-qty').find('span').text(parseFloat(available_qty).toFixed(2));
                  item_row_model_edit.find('.item-available-qty').find('input').val(available_qty);

                  item_row_model_edit.find('.remove-selected-item').attr('data-id', item_id);

                  item_row_model_edit.find('.save-change').attr('data-price', original_price);

			            item_row_model_edit.find('.remove-selected-item').attr('data-id', item_id);

			            table_listing_edit.append(item_row_model_edit.show().clone());
          			})
          		}
          	}
	    });

	    
	});

  	$(".change_image_deal").click( function(){

	    var me    = $(this),
	      deal_id = me.data('id');
	    var action  = me.data('action');

	    action = action + "/" + deal_id;

	    var form = $("#changeImageDeal");

	    form.attr('action', action);

	})

  	$(".detele_deal").click( function(){

	    var me    = $(this),
	      deal_id = me.data('id');

	    var action  = me.data('action');
	    var form = $("#dealDeleteForm") ;

    	form.attr('action', action);
  	
  	});


    $('#add-menu-category').click( function(e){
        e.preventDefault();
        $('#add-menu-category-modal').find('input').val("");
        $('#add-menu-category-modal').modal('show');
    });

    // Edit Menu Category
    $('.edit-menu-category').click( function(e){
        e.preventDefault();

        var edit_modal  = $('div#edit-menu-category-modal');
        var edit_form   = $('form#editMenuCategoryForm');

        var row         = $(this).closest('tr'),
            id          = row.data('id'),
            name        = row.find('td.name').text(),
            action      = $(this).data('action');

        edit_form.attr('action', action);
        edit_form.find('input#menu-category-name').val(name);

        edit_modal.modal('show');

        return false;
    });


    // Delete Menu Category
    $('.delete-menu-category').click ( function(e){
        e.preventDefault();

        var delete_modal  = $('div#delete-menu-category-modal');
        var delete_form   = $('form#deleteMenuCategoryForm');

        var row         = $(this).closest('tr'),
            id          = row.data('id'),
            action      = $(this).data('action');

        delete_form.attr('action', action);

        delete_modal.modal('show');

        return false;

    });

    // Click on row, and get its item or product
    // ============================
    var loading_item = $('div#loading-item');
    loading_item.hide();

    var item_container = $('div#item-container');
    // item_container.hide();

    var item_row = $('tr.item-row');
    $('tr.item-row').remove();

    var item_row_empty = $('tr#menu-item-empty');
    var menu_id_selected = <?php echo $menu_id ?>;

    var menu_item_tbody = item_container.find('table#menu-item-table').find('tbody');

    function countNumberOfRow(){

        var count = $('table#menu-item-table').find('tbody').find('tr.item-row').length;

        if( count == 0 ){
            item_row_empty.show();
        }

    }

    var i            = 1;
    var limit_data   = <?php echo $limit ?>;
    var offset       = 0;
    var page         = 1;

    $('.menu-row').click( function(){

        i               = 1;
        limit_data      = <?php echo $limit ?>;
        page            = 1;
        offset          = ( page - 1) * limit_data ;

        loading_item.hide();
        item_container.hide();

        $(this).closest('tbody').find('tr').removeClass('selected');
        $(this).addClass('selected');

        var menu_id     = $(this).attr('data-id');
        var menu_name   = $(this).find('td.name').text();

        $('#add-menu-item').attr('menu-id', menu_id);
        $('h5#menu-name').text(menu_name);
        $('#load-more-btn').attr('menu-id', menu_id);

        setTimeout(function(){
            loading_item.show();
        }, 100);

        var getItemURL__ = "<?php echo $this->Html->url( array('action' => 'getMenuItemByMenuIDAjax__')); ?>" ;
        
        menu_item_tbody.find('tr.item-row').remove();

        if( menu_id ){
            $.ajax({

                url: getItemURL__,
                type: "POST",
                data: { menu_id: menu_id, limit: limit_data + 1, offset:offset },
                async: false,
                dataType: 'json',

                success: function(data){

                    if( data.status == true ){

                        item_row_empty.hide();
                        var root = "<?php echo $this->webroot ?>";
                        var result = data.result;

                        if( result.length > limit_data ){
                            $('#load-more-btn').show();
                            result.splice(limit_data, 1);
                            
                        }else{
                            $('#load-more-btn').hide();
                        }

                        $.each( result, function(ind, val){
                           
                            var img     = root + val.BusinessMenuItem.image;
                            var price   = parseFloat(val.BusinessMenuItem.price) ;

                            var thm_img = img.replace("/menus/", "/menus/thumbs/" ) ;

                            price = price.toFixed(2);

                            item_row.attr('id', val.BusinessMenuItem.id);

                            item_row.find('td.item-no').text(i);
                            item_row.find('td.item-image').find('a').attr('href', "#");
                            item_row.find('td.item-image').find('img').attr('src', thm_img);

                            item_row.find('td.item-title').text(val.BusinessMenuItem.title);
                            item_row.find('td.item-price').text("$ " + price);

                            item_row.find('.edit-menu-item').attr('data-id', val.BusinessMenuItem.id);
                            item_row.find('.view-menu-item').attr('data-id', val.BusinessMenuItem.id);
                            item_row.find('.delete-menu-item').attr('data-id', val.BusinessMenuItem.id);

                            menu_item_tbody.append(item_row.clone().show());

                            i++;

                        });

                        setTimeout(function(){
                            loading_item.hide();
                            item_container.fadeIn();
                        }, 500);

                    }else{
                        $('#load-more-btn').hide();
                        setTimeout(function(){
                            loading_item.hide();
                            item_row_empty.show();
                            item_container.fadeIn();
                        }, 500);
                    }

                },

                error: function(err, msg){
                    console.log(msg)
                }

            });
        }

        Shadowbox.init();

    });
    
    // Load More Items
    $('#load-more-btn').click( function(){

        page++;
        offset = ( page - 1) * limit_data;

        var me          = $(this);
        var menu_id     = me.attr('menu-id');

        me.hide();
        $('#load-more-img').show();

        var getItemURL__ = "<?php echo $this->Html->url( array('action' => 'getMenuItemByMenuIDAjax__')); ?>" ;
        var show = 0;

        if( menu_id ){
            $.ajax({

                url: getItemURL__,
                type: "POST",
                data: { menu_id: menu_id, limit: limit_data + 1, offset:offset },
                async: false,
                dataType: 'json',

                success: function(data){

                    if( data.status == true ){

                        item_row_empty.hide();
                        var root = "<?php echo $this->webroot ?>";
                        var result = data.result;

                        if( result.length > limit_data ){
                            show = 1;
                            result.splice(limit_data, 1);                            
                        }else{
                            $('#load-more-btn').hide();
                        }

                        $.each( result, function(ind, val){
                           
                            var img     = root + val.BusinessMenuItem.image;
                            var price   = parseFloat(val.BusinessMenuItem.price) ;

                            var thm_img = img.replace("/menus/", "/menus/thumbs/" ) ;

                            price = price.toFixed(2);

                            item_row.attr('id', val.BusinessMenuItem.id);

                            item_row.find('td.item-no').text(i);
                            item_row.find('td.item-image').find('a').attr('href', "#");
                            item_row.find('td.item-image').find('img').attr('src', thm_img);

                            item_row.find('td.item-title').text(val.BusinessMenuItem.title);
                            item_row.find('td.item-price').text("$ " + price);

                            item_row.find('.edit-menu-item').attr('data-id', val.BusinessMenuItem.id);
                            item_row.find('.view-menu-item').attr('data-id', val.BusinessMenuItem.id);
                            item_row.find('.delete-menu-item').attr('data-id', val.BusinessMenuItem.id);

                            menu_item_tbody.append(item_row.clone().show());

                            i++;

                        });

                        if( show == 1 ){
                            setTimeout( function(){
                                $('#load-more-img').hide();
                                me.show();
                            },500);
                        }else{
                            setTimeout( function(){
                                $('#load-more-img').hide();
                                me.hide();
                            },500);
                        }

                        Shadowbox.init();

                    }else{                        
                        $('#load-more-img').hide();
                        $('#load-more-btn').hide();
                    }

                },

                error: function(err, msg ){

                }
            });
        }

    });

    // Add Menu Item
    // ===================
    $('#add-menu-item').click( function(e){

        e.preventDefault();

        var menu_id = $(this).attr('menu-id');
        var action  = $(this).attr('data-action');

        action += "/" + menu_id;

        if( menu_id == 0 ){

            $("div#pass-alert").find('h5').text("Please select category first !");
            $('div#pass-alert').modal('show');
            return false;
        }

        var add_item_modal = $('div#add-menu-item-modal');
        add_item_modal.find('form').attr('action', action);

        add_item_modal.find('input').val("");
        CKEDITOR.instances.itemDescription.setData("");

        add_item_modal.modal('show');

    })

    // Edit Menu Item
    $('table#menu-item-table').on('click', '.edit-menu-item', function(event){
        event.preventDefault();

        var me      = $(this),
            id      = me.data('id'),
            action    = me.data('action') ;

        var menu_id = $('#add-menu-item').attr('menu-id');

        var URL__ = "<?php echo $this->Html->url( array('action' => 'getMenuItemDetailAjax__')); ?>" ;

        if( id ){
            $.ajax({

                url: URL__,
                type: "POST",
                data: { item_id: id},
                async: false,
                dataType: 'json',

                success: function(data){

                    if( data.status == true ){

                        var editModal   = $('div#edit-menu-item-modal');
                        var form        = editModal.find('form');
                        var detail      = data.detail;

                        form.find('#BusinessMenuItemTitle').val(detail.title);
                        form.find('#BusinessMenuItemPrice').val(detail.price);
                        form.find('#data_old_img').val(detail.image);
                        CKEDITOR.instances.itemDescriptionEdit.setData(detail.description);

                        form.attr('action', action + "/" + menu_id + "/" + id );
                        editModal.modal('show');

                    }else{

                       $('div#message').find('h5').text(data.message);
                       $('div#message').modal('show');
                       return false;
                    }

                },

                error: function(err, msg){
                    console.log(msg);
                }

            });
        }

    })

    // View Detail Menu Item
    $('table#menu-item-table').on('click', '.view-menu-item', function(event){

        event.preventDefault();
        var me      = $(this),
            id      = me.data('id');

        var menu_id = $('#add-menu-item').attr('menu-id');

        var URL__ = "<?php echo $this->Html->url( array('action' => 'getMenuItemDetailAjax__')); ?>" ;

        if( id ){
            $.ajax({

                url: URL__,
                type: "POST",
                data: { item_id: id},
                async: false,
                dataType: 'json',

                success: function(data){

                    if( data.status == true ){

                        var root = "<?php echo $this->webroot ?>";

                        var viewModal   = $('div#detail-menu-item-modal');
                        var detail      = data.detail;
                        var price = parseFloat(detail.price).toFixed(2);

                        viewModal.find('#title').text(detail.title);
                        viewModal.find('#price').text("$ " + price);
                        viewModal.find('#desc').html(detail.description);
                        viewModal.find('#image').attr('src', root + detail.image );
                        viewModal.find('#image').attr('alt', detail.title );

                        viewModal.modal('show');

                    }else{

                       $('div#message').find('h5').text(data.message);
                       $('div#message').modal('show');
                       return false;
                    }

                },

                error: function(err, msg){
                    console.log(msg);
                }

            });
        }

    })

    
    // Delete Menu Item
    $('table#menu-item-table').on('click', '.delete-menu-item', function(event){

        event.preventDefault();

        var me      = $(this),
            id      = me.data('id');

        $('div#delete-menu-item-modal').find('#confirm-delete-item').attr('item-id', id );
        $('div#delete-menu-item-modal').modal('show');

    });

    $('#confirm-delete-item').click( function(){
        var me  = $(this),
            id  = me.attr('item-id');

        var deleteItemURL__ = "<?php echo $this->Html->url( array('action' => 'deleteMenuItemAjax__')); ?>" ;

        var deleteModal = $('div#delete-menu-item-modal');

        if( id ){
            $.ajax({
                url: deleteItemURL__,
                type: "POST",
                data: { item_id: id},
                async: false,
                dataType: 'json',

                success: function(data){

                    if( data.status == true ){
                        deleteModal.modal('hide');
                        $('table#menu-item-table').find('tbody').find('tr#' + id).fadeOut(1000).remove();

                        countNumberOfRow();

                    }else{
                        console.log(data.message);
                    }

                },

                error: function(err, msg){
                    
                }

            });
        }

    });


    // ============================================
    var div_image = $('.deal-image');
    $('.deal-image').remove();
    var div_upload = $('div.deal-image-upload');

    // Deal Images View
    $('.deal_images').click ( function(event){

        event.preventDefault();

        $('#deal-image-container').find('.deal-image').remove();

        var image_modal     = $('div#deal_images');
        var deal_id         = $(this).data('id');
        
        var getDealImageURL__ = "<?php echo $this->Html->url(array('action' => 'getDealImagesAjax__')); ?>";
        getDealImageURL__ += "/" + deal_id ;

        image_modal.modal('show');
        var root = '<?php echo $this->webroot ?>';

        var loading_img = $('div#loading-img');
        var time = new Date().getTime();

        loading_img.show();
        $('div.deal-image-upload').hide();

        if( deal_id ){
            $.ajax({

                url: getDealImageURL__,
                type: "POST",
                data: {},
                async: false,
                dataType: 'json',

                success: function(data){
                    if( data.status == true ){

                        setTimeout( function(){
                            loading_img.hide();

                            var images = data.deal_images;

                            $.each( images, function(ind, val){
                                time += 300;

                                var img_name = val.split("/");
                                img_name = img_name[img_name.length - 1];

                                var img = root + "img/deals/" + img_name;
                                var thb = root + "img/deals/thumbs/" + img_name;

                                div_image.attr( 'data-id' , deal_id + "_" + time );
                                div_image.html('<img alt="" src="' + thb + '"><div title="Remove Image" class="remove-img" data-img="'+ img +'" data-thumb="'+ thb +'" data-dealid="' + deal_id + '"><i class="icon icon-trash"></i></div>');

                                $('#deal-image-container').prepend(div_image.clone());
                            });

                            checkUploadButton(deal_id);

                        }, 1000);
                    }
                },
            
                error: function(err){
                    
                }

            });
        }

    });

    function checkUploadButton( deal_id ){

        var imageCount = $('#deal-image-container').find('.deal-image').length;
        var allowImagesSize = <?php echo $dealImagesSize ?>;

        if( imageCount < allowImagesSize ){
            $('div.deal-image-upload').find('.upload').attr('data-dealid', deal_id);
            $('div.deal-image-upload').find('i').show();
            $('div.deal-image-upload').find('img').hide();
            $('div.deal-image-upload').show();

        }else{
            $('div.deal-image-upload').hide();
        }
    }

    $('#deal-image-container').on('click', '.upload', function(){
        $('#upload_deal_image').trigger('click');
    })

    $('input#upload_deal_image').on( 'change', function(){

        var max_size = <?php echo $__MAXIMUM_IMAGE_SIZE ?>;
        var files = $($(this)[0].files);

        var size = files[0].size ;

        if( size > max_size ){
          $('input#upload_deal_image').val('');
          $('div#warning-modal').find('h5').text( "Images reached size limitation. Images should be less then 2MB." );
          $('div#warning-modal').modal('show');

          return false;
        }

        $('form#frm-upload-deal-image').submit();
    
    })

    $('form#frm-upload-deal-image').submit( function(event){

        event.preventDefault();

        var div_container   = $('div.deal-image-upload').find('.upload');
        var icon_add        = div_container.find('i');
        var loading_img     = div_container.find('img');

        var deal_id = div_container.data('dealid');

        var form = $(this);
        var action = form.attr('action');
        var root = '<?php echo $this->webroot ?>';

        action += "/" + deal_id;

        $.ajax({

            url: action,
            type: "POST",
            data:  new FormData(this),
            contentType: false,
            cache: false,
            async: false,
            dataType: 'json',
            processData:false,
            
            beforeSend: function(){
                icon_add.hide();
                loading_img.show();

            },

            success: function(data){

                setTimeout( function(){

                    icon_add.show();
                    loading_img.hide();    

                    if( data.status == true ){

                        var img_name = data.uploaded_image.split("/");
                        img_name = img_name[img_name.length - 1];

                        var img = root + 'img/deals/' + img_name ;
                        var thb = root + 'img/deals/thumbs/' +  img_name;

                        var time = new Date().getTime();

                        div_image.attr( 'data-id' , deal_id + "_" + time );

                        div_image.html('<img alt="" src="' + thb + '"><div title="Remove Image" class="remove-img" data-img="'+ img +'" data-thumb="'+ thb +'" data-dealid="' + deal_id + '"><i class="icon icon-trash"></i></div>');

                        $('#deal-image-container').prepend(div_image.clone()).fadeIn();
                        checkUploadButton(deal_id);
                        $('form#frm-upload-deal-image')[0].reset();
                    }else{
                        var msg = data.message;
                        $('span#error-msg').text(msg).fadeIn();
                        setTimeout(function(){
                            $('span#error-msg').fadeOut();
                        },3000);
                    }

                }, 2000);
                          
            },
        
            error: function(){             
            
            }

        });

        return false;

    });

    // Remvoe Deal
    $('#deal-image-container').on('click', '.remove-img', function(){
        var me      = $(this),
            img     = me.data('img'),
            deal_id = me.data('dealid'),
            img_box = me.closest(".deal-image").attr("data-id") ;

        var remove_image_modal   = $('div#remove-deal-image');

        remove_image_modal.attr('box-id', img_box);
        remove_image_modal.attr('deal-id', deal_id);
        remove_image_modal.attr('data-img', img);

        remove_image_modal.modal('show');

    });


    $('div#remove-deal-image').on('click', '#confirm-delete', function(){
        
        var modal   = $('div#remove-deal-image');
        var box_id  = modal.attr('box-id');
        var deal_id = modal.attr('deal-id');
        var img     = modal.attr('data-img');

        var img_name = img.split("/");

        img_name = img_name[img_name.length - 1];
        
        var deleteURL__   = "<?php echo $this->Html->url(array('action' => 'deleteDealImageAjax__')); ?>";

        deleteURL__ += "/" + deal_id + "/" + img_name;

        if( deal_id && img ){

            $.ajax({

                url: deleteURL__,
                type: "POST",
                data: {},
                async: false,
                dataType: 'json',

                success: function(data){
                    if(data.status == true ){
                        modal.modal('hide');
                        var remove_box = $('div#deal_images').find('.deal-image[data-id="'+ box_id +'"]');
                        remove_box.fadeOut('slow').remove();
                        checkUploadButton(deal_id);
                    }else{
                        var msg = data.message;
                        $('span#error-msg').text(msg).fadeIn();
                        setTimeout(function(){
                            $('span#error-msg').fadeOut();
                        },3000);
                    }
                },
            
                error: function(){
                }

            });
        }

        return false;

    });

   function numericAndDotOnly(elementRef) {
        
      var keyCodeEntered = (event.which) ? event.which : (window.event.keyCode) ? window.event.keyCode : -1;

      if ((keyCodeEntered >= 48) && (keyCodeEntered <= 57)) {
        return true;
      }   
      // '.' decimal point...
      else if (keyCodeEntered == 46) {
        // Allow only 1 decimal point ('.')...
        if ((elementRef.value) && (elementRef.value.indexOf('.') >= 0))
          return false;
        else
          return true;
      }

      return false;
  }


    // Create Deal - Add Related Items
    $('a#add-related-item-btn').click( function(event){

        event.preventDefault();

        var related_item_modal = $('div#related-item-modal');

        related_item_modal.find('select#business-menu-item').val("");
        $('select#business-menu-item').change();

        related_item_modal.modal('show');

    });


    $('select#business-menu').change( function(){
        var me      = $(this),
            menu_id = me.val(),
            limit   = null,
            offset  = 0;

        var select_item         = $('select#business-menu-item'),
            original_price      = $('input#original-price'),
            discounted_price    = $('input#discounted-price');

        var empty_option        = "<option value=''>-- select an item --</option>";
        var loading             = "<option value=''>loading...</option>";

        var getItemURL__ = "<?php echo $this->Html->url( array('controller' => 'Businesses', 'action' => 'getMenuItemByMenuIDAjax__')); ?>" ;


        if( menu_id != '' ){
            $.ajax({

                url: getItemURL__,
                type: "POST",
                data: { menu_id: menu_id, limit: limit , offset: offset },
                async: false,
                dataType: 'json',

                beforeSend: function(){
                    select_item.html(loading);
                    original_price.val('0.00');
                    discounted_price.val("");
                },

                success: function(data){

                    select_item.html(empty_option);

                    if( data.status == true ){

                        var result = data.result;
                        $.each( result, function(ind, val){
                            var price  = parseFloat(val.BusinessMenuItem.price).toFixed(2);
                            var option = "<option value='" + val.BusinessMenuItem.id + "' data-price='" + price + "' data-title='" + val.BusinessMenuItem.title + "'>"+ val.BusinessMenuItem.title +"</option>"; 


                            select_item.append(option);
                        });

                    }

                },

                error: function(err, msg){
                    console.log(msg)
                }

            });
        }else{
            select_item.html(empty_option);
            original_price.val('0.00');
            discounted_price.val("");
        }

    });

    $('select#business-menu-item').change( function(){
        var me         = $(this),
            item_id    = me.val();

        var original_price      = $('input#original-price'),
            discounted_price    = $('input#discounted-price');

        original_price.val('0.00')
        discounted_price.val('');

        if( item_id != '' ){

            var selected   = $('option:selected', me),
                price      = selected.attr('data-price');

            original_price.val(price);
        }
        
    });

    var table_listing  = $('table#table-item-listing').find('tbody');
    var item_row_model = table_listing.find('.related-item-row');
    $('table#table-item-listing').find('tbody').find('.related-item-row').remove();

    var row_empty = $('tr#row-empty-item');

    $('#select-item-btn').click( function(){

        var item_id           = $('select#business-menu-item').val(),
            original_price    = $('input#original-price').val(),
            discounted_price  = $('input#discounted-price').val(),
            available_qty     = $('input#available-qty').val() ;

        var err = new Array();

        if( item_id == '' ){
            err.push("Please select an item !");
        }

        if( discounted_price == '' || isNaN(parseFloat(discounted_price)) || parseFloat(discounted_price) == 0 ){
            err.push("Please enter discounted price !");
        }else if( parseFloat(discounted_price) >= parseFloat(original_price) ){
            err.push("Discounted Price must be less than original price !");
        }

        if( table_listing.find('tr[data-id="' + item_id + '"]').length > 0 ){
            err.push("This item is already selected !");
        }

        if( available_qty == 0 || isNaN(parseFloat(available_qty)) ){
            err.push('Please enter available quantity !') ;
        }

        if( err.length == 0 ){

            row_empty.hide();

            item_row_model.find('.hidden-item-id').val(item_id);
            item_row_model.find('.hidden-item-original-price').val(original_price);
            item_row_model.find('.hidden-item-discount').val(discounted_price);
            item_row_model.find('.hidden-item-qty').val(available_qty);

            // Biz Menu
            var menu_name = $('option:selected', $("select#business-menu")).attr('data-name');
            var item_name = $('option:selected', $('select#business-menu-item')).attr('data-title');

            item_row_model.attr('data-id', item_id);
            item_row_model.find('.item-menu').text(menu_name);
            item_row_model.find('.item-title').text(item_name);
            item_row_model.find('.item-price').text("$ " + parseFloat(original_price).toFixed(2));

            item_row_model.find('.item-discount-price').find('span').text("$ " + parseFloat(discounted_price).toFixed(2));
            item_row_model.find('.item-discount-price').find('input').val(discounted_price);
            
            item_row_model.find('.item-available-qty').find('span').text(parseFloat(available_qty).toFixed(2));
            item_row_model.find('.item-available-qty').find('input').val(available_qty);

            item_row_model.find('.save-change').attr('data-price', original_price)

            item_row_model.find('.remove-selected-item').attr('data-id', item_id);

            table_listing.append(item_row_model.show().clone());

        }else{

            $('div#warning-modal').find('h5').html("");
            $.each( err, function(ind, val){
                $('div#warning-modal').find('h5').append(val + "<br/>" );
            })

            $('div#warning-modal').modal('show');
            return false;
        }        

    })

    $('table#table-item-listing').on('click', '.remove-selected-item', function(event){

        event.preventDefault();

        var confirm_modal = $('div#remove-item-modal');

        confirm_modal.find('#confirm-remove').attr('data-id', $(this).attr('data-id'));

        confirm_modal.modal('show');

    });

    $('#confirm-remove').click( function(){
        var id = $(this).attr('data-id');
        $('#table-item-listing').find('tbody').find('tr[data-id="' + id + '"]').fadeOut().remove();
        countTotalItemSelected();
    });

    $('table#table-item-listing').on('click', '.edit-selected-item', function(event){

        event.preventDefault();

        var me              = $(this),
            row_tr          = me.closest('tr'),
            
            row_discount    = row_tr.find('.item-discount-price'),
            discount_span   = row_discount.find('span'),
            discount_input  = row_discount.find('input'),

            row_qty         = row_tr.find('.item-available-qty'),
            qty_span        = row_qty.find('span'),
            qty_input       = row_qty.find('input'),

            hidden_discount = row_tr.find('input#hidden-item-discount'),
            hidden_qty      = row_tr.find('input#hidden-item-qty') ;

        var save_btn = row_tr.find('.save-change');

        me.hide();
        save_btn.fadeIn();

        discount_span.hide();
        discount_input.fadeIn();

        qty_span.hide();
        qty_input.fadeIn();

    });

    $('table#table-item-listing').on('click', '.save-change', function(event){
        event.preventDefault();

        var me              = $(this),
            row_tr          = me.closest('tr'),
            
            row_discount    = row_tr.find('.item-discount-price'),
            discount_span   = row_discount.find('span'),
            discount_input  = row_discount.find('input'),

            row_qty         = row_tr.find('.item-available-qty'),
            qty_span        = row_qty.find('span'),
            qty_input       = row_qty.find('input'),

            hidden_discount = row_tr.find('input.hidden-item-discount'),
            hidden_qty      = row_tr.find('input.hidden-item-qty') ;

        var edit_btn = row_tr.find('.edit-selected-item');

        var new_discount    = discount_input.val();
        var new_qty         = qty_input.val();

        var original_price  = me.attr('data-price'); 
        
        var err = new Array();

        if( new_discount == '' || isNaN(parseFloat(new_discount)) || parseFloat(new_discount) == 0 ){
            err.push("Please enter discounted price !");
        }else if( parseFloat(new_discount) >= parseFloat(original_price) ){
            err.push("Discounted Price must be less than original price !");
        }      

        if( new_qty == 0 || isNaN(parseFloat(new_qty)) ){
            err.push('Please enter available quantity !');
        }

        if( err.length == 0 ){
            hidden_discount.val(new_discount);
            hidden_qty.val(new_qty);

            qty_span.text(parseFloat(new_qty).toFixed(2));
            discount_span.text("$ " + parseFloat(new_discount).toFixed(2));

            me.hide();
            edit_btn.fadeIn();

            discount_input.hide();
            discount_span.fadeIn();

            qty_input.hide();
            qty_span.fadeIn();

        }else{

            $('div#warning-modal').find('h5').html("");
            $.each( err, function(ind, val){
                $('div#warning-modal').find('h5').append(val + "<br/>" );
            })

            $('div#warning-modal').modal('show');
            return false;
        } 

    });

    function countTotalItemSelected(){
        var count = $('table#table-item-listing').find('tbody').find('tr.related-item-row').length;
        if(count == 0){
            row_empty.show();
        }
    }

    function totalSelectedItem(){
    	return count = $('table#table-item-listing').find('tbody').find('tr.related-item-row').length;
    }

    $('#add-deal-submit').click( function(){

      var loading = $('div#loading-add');

      loading.show();

      var div_category  = $('div#DealGoodsCategoryAdd');
      var checked       = div_category.find('input[type="checkbox"]').is(':checked');

      if( checked == false ){
        loading.hide();
        $('div#warning-modal').find('h5').html("Please select at least one category!");
        $('div#warning-modal').modal('show');

        return false;
      }else if( totalSelectedItem() == 0 ){
        loading.hide();
    		$('div#warning-modal').find('h5').html("Please select related item !");
    		$('div#warning-modal').modal('show');

    		return false;
    	}else{
    		$('from#dealAddForm').submit();
    	}
    })

    // Edit Deal

    $('a#add-related-item-edit-btn').click( function(event){

        event.preventDefault();

        var related_item_modal = $('div#related-item-edit-modal');

        related_item_modal.find('select#business-menu-item').val("");
        $('select#business-menu-item-edit').change();

        related_item_modal.modal('show');

    });


    $('select#business-menu-edit').change( function(){
        var me      = $(this),
            menu_id = me.val(),
            limit   = null,
            offset  = 0;

        var select_item         = $('select#business-menu-item-edit'),
            original_price      = $('input#original-price-edit'),
            discounted_price    = $('input#discounted-price-edit');

        var empty_option        = "<option value=''>-- select an item --</option>";
        var loading             = "<option value=''>loading...</option>";

        var getItemURL__ = "<?php echo $this->Html->url( array('controller' => 'Businesses', 'action' => 'getMenuItemByMenuIDAjax__')); ?>" ;


        if( menu_id != '' ){
            $.ajax({

                url: getItemURL__,
                type: "POST",
                data: { menu_id: menu_id, limit: limit , offset: offset },
                async: false,
                dataType: 'json',

                beforeSend: function(){
                    select_item.html(loading);
                    original_price.val('0.00');
                    discounted_price.val("");
                },

                success: function(data){

                    select_item.html(empty_option);

                    if( data.status == true ){

                        var result = data.result;
                        $.each( result, function(ind, val){
                            var price  = parseFloat(val.BusinessMenuItem.price).toFixed(2);
                            var option = "<option value='" + val.BusinessMenuItem.id + "' data-price='" + price + "' data-title='" + val.BusinessMenuItem.title + "'>"+ val.BusinessMenuItem.title +"</option>"; 


                            select_item.append(option);
                        });

                    }

                },

                error: function(err, msg){
                    console.log(msg)
                }

            });
        }else{
            select_item.html(empty_option);
            original_price.val('0.00');
            discounted_price.val("");
        }

    });


    $('select#business-menu-item-edit').change( function(){
        var me         = $(this),
            item_id    = me.val();

        var original_price      = $('input#original-price-edit'),
            discounted_price    = $('input#discounted-price-edit');

        original_price.val('0.00')
        discounted_price.val('');

        if( item_id != '' ){

            var selected   = $('option:selected', me),
                price      = selected.attr('data-price');

            original_price.val(price);
        }
        
    });

    $('#select-item-edit-btn').click( function(){

        var item_id           = $('select#business-menu-item-edit').val(),
            original_price    = $('input#original-price-edit').val(),
            discounted_price  = $('input#discounted-price-edit').val(),
            available_qty     = $('input#available-qty-edit').val();

        var err = new Array();

        if( item_id == '' ){
            err.push("Please select an item !");
        }

        if( discounted_price == '' || isNaN(parseFloat(discounted_price)) || parseFloat(discounted_price) == 0 ){
            err.push("Please enter discounted price !");
        }else if( parseFloat(discounted_price) >= parseFloat(original_price) ){
            err.push("Discounted Price must be less than original price !");
        }

        if( table_listing_edit.find('tr[data-id="' + item_id + '"]').length > 0 ){
            err.push("This item is already selected !");
        }
        
        if( available_qty == 0 || isNaN(parseFloat(available_qty)) ){
            err.push('Please enter available quantity !') ;
        }

        if( err.length == 0 ){

            row_empty_edit.hide();

            item_row_model_edit.find('.hidden-item-id').val(item_id);
            item_row_model_edit.find('.hidden-item-original-price').val(original_price);
            item_row_model_edit.find('.hidden-item-discount').val(discounted_price);
            item_row_model_edit.find('.hidden-item-qty').val(available_qty);

            // Biz Menu
            var menu_name = $('option:selected', $("select#business-menu-edit")).attr('data-name');
            var item_name = $('option:selected', $('select#business-menu-item-edit')).attr('data-title');

            item_row_model_edit.attr('data-id', item_id);
            item_row_model_edit.find('.item-menu').text(menu_name);
            item_row_model_edit.find('.item-title').text(item_name);
            item_row_model_edit.find('.item-price').text("$ " + parseFloat(original_price).toFixed(2));

            item_row_model_edit.find('.item-discount-price').find('span').text("$ " + parseFloat(discounted_price).toFixed(2));
            item_row_model_edit.find('.item-discount-price').find('input').val(discounted_price);
            
            item_row_model_edit.find('.item-available-qty').find('span').text(parseFloat(available_qty).toFixed(2));
            item_row_model_edit.find('.item-available-qty').find('input').val(available_qty);

            item_row_model_edit.find('.save-change').attr('data-price', original_price)

            item_row_model_edit.find('.remove-selected-item').attr('data-id', item_id);

            table_listing_edit.append(item_row_model_edit.show().clone());

        }else{

            $('div#warning-modal').find('h5').html("");
            $.each( err, function(ind, val){
                $('div#warning-modal').find('h5').append(val + "<br/>" );
            })

            $('div#warning-modal').modal('show');
            return false;
        }        

    })

    $('table#table-item-listing-edit').on('click', '.remove-selected-item', function(event){

        event.preventDefault();

        var confirm_modal = $('div#remove-item-edit-modal');

        confirm_modal.find('#confirm-remove-edit').attr('data-id', $(this).attr('data-id'));

        confirm_modal.modal('show');

    });

    $('#confirm-remove-edit').click( function(){
        var id = $(this).attr('data-id');
        $('#table-item-listing-edit').find('tbody').find('tr[data-id="' + id + '"]').fadeOut().remove();
        countTotalItemSelectedEdit();
    });

    $('table#table-item-listing-edit').on('click', '.edit-selected-item', function(event){

        event.preventDefault();

        var me              = $(this),
            row_tr          = me.closest('tr'),
            
            row_discount    = row_tr.find('.item-discount-price'),
            discount_span   = row_discount.find('span'),
            discount_input  = row_discount.find('input'),

            row_qty         = row_tr.find('.item-available-qty'),
            qty_span        = row_qty.find('span'),
            qty_input       = row_qty.find('input'),

            hidden_discount = row_tr.find('input#hidden-item-discount'),
            hidden_qty      = row_tr.find('input#hidden-item-qty') ;

        var save_btn = row_tr.find('.save-change');

        me.hide();
        save_btn.fadeIn();

        discount_span.hide();
        discount_input.fadeIn();

        qty_span.hide();
        qty_input.fadeIn();

    });

    $('table#table-item-listing-edit').on('click', '.save-change', function(event){
        event.preventDefault();

        var me              = $(this),
            row_tr          = me.closest('tr'),
            
            row_discount    = row_tr.find('.item-discount-price'),
            discount_span   = row_discount.find('span'),
            discount_input  = row_discount.find('input'),

            row_qty         = row_tr.find('.item-available-qty'),
            qty_span        = row_qty.find('span'),
            qty_input       = row_qty.find('input'),

            hidden_discount = row_tr.find('input.hidden-item-discount'),
            hidden_qty      = row_tr.find('input.hidden-item-qty') ;

        var edit_btn = row_tr.find('.edit-selected-item');

        var new_discount    = discount_input.val();
        var new_qty         = qty_input.val();

        var original_price  = me.attr('data-price'); 
        
        var err = new Array();

        if( new_discount == '' || isNaN(parseFloat(new_discount)) || parseFloat(new_discount) == 0 ){
            err.push("Please enter discounted price !");
        }else if( parseFloat(new_discount) >= parseFloat(original_price) ){
            err.push("Discounted Price must be less than original price !");
        }      

        if( new_qty == 0 || isNaN(parseFloat(new_qty)) ){
            err.push('Please enter available quantity !');
        }

        if( err.length == 0 ){
            hidden_discount.val(new_discount);
            hidden_qty.val(new_qty);

            qty_span.text(parseFloat(new_qty).toFixed(2));
            discount_span.text("$ " + parseFloat(new_discount).toFixed(2));

            me.hide();
            edit_btn.fadeIn();

            discount_input.hide();
            discount_span.fadeIn();

            qty_input.hide();
            qty_span.fadeIn();

        }else{

            $('div#warning-modal').find('h5').html("");
            $.each( err, function(ind, val){
                $('div#warning-modal').find('h5').append(val + "<br/>" );
            })

            $('div#warning-modal').modal('show');
            return false;
        } 

    });

    function countTotalItemSelectedEdit(){
        var count = $('table#table-item-listing-edit').find('tbody').find('tr.related-item-row-edit').length;
        if(count == 0){
            row_empty_edit.show();
        }
    }

    function totalSelectedItemEdit(){
    	return count = $('table#table-item-listing-edit').find('tbody').find('tr.related-item-row-edit').length;
    }

    $('#save-deal-edit').click( function(){

      var loading = $('div#loading-edit');
      loading.show();

    	var div_category  = $('div#DealGoodsCategoryEdit');
      var checked       = div_category.find('input[type="checkbox"]').is(':checked');

      if( checked == false ){
        loading.hide();
        $('div#warning-modal').find('h5').html("Please select at least one category!");
        $('div#warning-modal').modal('show');

        return false;
      }else if( totalSelectedItemEdit() == 0 ){
        loading.hide();
    		$('div#warning-modal').find('h5').html("Please select related item !");
    		$('div#warning-modal').modal('show');

    		return false;
    	}else{
    		$('from#dealEditForm').submit();
    	}

    })

    var image_preview   = $('div.deal-image-preview');

    $('#deal_image_add').on( 'change', function(e){

        var count = this.files.length;
        var number_of_image_allow = <?php echo $dealImagesSize; ?> ;
        var max_size = <?php echo $__MAXIMUM_IMAGE_SIZE ?>;
        
        var min_deal_width  = <?php echo $__DEAL_WIDTH ?>;
        var min_deal_height = <?php echo $__DEAL_HEIGHT ?>;

        $('#deal-image-div').html("");

        if( count > number_of_image_allow ){

            $('div#warning-modal').find('h5').text('Deal Images allow only ' + number_of_image_allow + ' maximum !');
            $('div#warning-modal').modal('show');

            $('#deal_image_add').val('');
            return false;
        }

        if (typeof (FileReader) != "undefined") {
          
            var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.png)$/;

            var error = false;
            var ext_msg   = "";
            var size_msg  = "";
            var dimension_msg = "";

            var files = $($(this)[0].files);

            files.each(function () {
                var file = $(this);
                var size = file[0].size;

                if (!regex.test(file[0].name.toLowerCase())) {
                    error = true;
                    ext_msg = "Invalid Image type." ;
                }

                if( error == false && size > max_size ){
                    error = true;
                    size_msg = "Images reached size limitation. Images should be less then 2MB.";
                }

                if( error == false ){ 

                  var reader = new FileReader();
                  reader.onload = function (e) {
                    var image = new Image();
                    image.src = e.target.result;
                    
                    image.onload = function() {
                      var img_w = this.width;
                      var img_h = this.height;

                      if( img_w < min_deal_width || img_h < min_deal_height ){
                          error = true;
                          dimension_msg = "Images size should be " + min_deal_width + " x " + min_deal_height + " PX minimum." ;
                          
                          $('#deal_image_add').val('');
                          $('#deal-image-div').html('');
                          $('div#warning-modal').find('h5').html( dimension_msg );
                          $('div#warning-modal').modal('show');

                          return false;

                      }else if(error == false ){
                          image_preview.find('img').attr('src', e.target.result);                                
                          $('#deal-image-div').append(image_preview.clone().fadeIn());      
                      }
                    }
                  }
                  reader.readAsDataURL(file[0]);
                }else{
                    var mss = "";
                    if( ext_msg ){
                        mss += ext_msg + "<br>" ;
                    }

                    if( size_msg ){
                        mss += size_msg + "<br>" ;
                    }

                    if( dimension_msg ){
                        mss += dimension_msg;
                    }

                    $('#deal_image_add').val('');
                    $('#deal-image-div').html('');
                    $('div#warning-modal').find('h5').html( mss );
                    $('div#warning-modal').modal('show');

                    return false;
                }
            });

        } else {

            $('div#warning-modal').find('h5').text('This browser does not support !');
            $('div#warning-modal').modal('show');

            $('#deal_image_add').val('');
            $('#deal-image-div').html('');
            return false;
        }

    });

   $('input.menu-item-image').on( 'change', function(){

        var me = $(this);

        var max_size = <?php echo $__MAXIMUM_IMAGE_SIZE ?>;
        var files = $($(this)[0].files);

        var size = files[0].size ;

        var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.png)$/;

        var error = false;
        var ext_msg   = "";
        var size_msg  = "";
        var dimension_msg = "";

        var min_deal_width  = <?php echo $__DEAL_WIDTH ?>;
        var min_deal_height = <?php echo $__DEAL_HEIGHT ?>;

        if (!regex.test(files[0].name.toLowerCase())) {
            error = true;
            ext_msg = "Invalid Image type." ;
        }

        if( error == false && size > max_size ){
            error = true;
            size_msg = "Images reached size limitation. Images should be less then 2MB.";
        }

        if( error == false ){ 
          var file = files[0];
          var reader = new FileReader();
          var image  = new Image();

          reader.readAsDataURL(file);  
          reader.onload = function(_file) {
              image.src    = _file.target.result;              // url.createObjectURL(file);
              image.onload = function() {
                  var w = this.width,
                      h = this.height;

                  if( w < min_deal_width && h < min_deal_height ){

                    $('input.menu-item-image').val('');
                    $('div#warning-modal').find('h5').text("Image size should be " + min_deal_width + " x " + min_deal_height + " pixels." );
                    $('div#warning-modal').modal('show');

                    return false;

                  }
                  
              };      
          };

        }else{
          
          var mss = "";
          if( ext_msg ){
              mss += ext_msg + "<br>" ;
          }

          if( size_msg ){
              mss += size_msg + "<br>" ;
          }

          if( dimension_msg ){
              mss += dimension_msg;
          }
          
          $('input.menu-item-image').val('');
          $('div#warning-modal').find('h5').text( mss );
          $('div#warning-modal').modal('show');

          return false;

        }

      })


    $('#filter-btn').click( function(event){
        event.preventDefault();
        var modal = $('#search-deal-modal');

        modal.modal('show');
    });

    $('#clear-search-btn').click(function(event){
        event.preventDefault();

        var modal = $('div#search-deal-modal');

        $('.reset-btn').click();

        modal.find('form').submit();

    });

    $('.reset-btn').click( function(){

      var modal = $('div#search-deal-modal');

      modal.find('input[type="text"]').val("");
      modal.find('select#deal-status').val("all");
      modal.find('select#deal-category').val("");
      modal.find('input[type="checkbox"]').removeAttr('checked');

    })

    var detail_table_listing  = $('div#detail-deal-modal').find('table#table-item-listing').find('tbody');
    var detail_item_row_model = detail_table_listing.find('.detail-related-item-row');
    $('div#detail-deal-modal').find('table#table-item-listing').find('tbody').find('.detail-related-item-row').remove();

    // alert(detail_table_listing.size());

    $('a.btn-view-deal').click( function(e){
      e.preventDefault();

      detail_table_listing.html("");

      var me      = $(this);

      var modal     = $('div#detail-deal-modal');
      var loading   = modal.find('#loading-img');
      var div_content = modal.find('.content');

      var biz_title   = modal.find('h3#myModalLabel').find('font');
      biz_title.text('Deal Detail');
      var deal_id   = me.attr('data-id');

      modal.find('input#deal-id').val(deal_id);

      var url = '<?php echo $this->webroot ?>' + 'administrator/Deals/getDealDetailAjax__/' + deal_id ;

      loading.show();
      div_content.hide();
      modal.modal('show');

      if( deal_id ){
              $.ajax({

                  url: url,
                  type: "POST",
                  data: {},
                  async: false,
                  dataType: 'json',

                  success: function(data){
                    
                      if( data.status == true ){

                        var detail = data.data;

                        // Define Element
                        var td_deal_title       = div_content.find('td#deal-title');
                        var td_deal_desc        = div_content.find('td#deal-description').find('div');
                        var td_deal_conditions  = div_content.find('td#deal-conditions').find('div');
                        var td_deal_valid_from  = div_content.find('td#deal-valid-from');
                        var td_deal_valid_to    = div_content.find('td#deal-valid-to');
                        var td_deal_posted_date = div_content.find('td#deal-posted-date');
                        var td_deal_status      = div_content.find('td#deal-status');

                        var td_deal_goods_group = div_content.find('td#deal-goods-group');

                        // Get Data
                        var biz_name  = detail.BusinessInfo.business_name;

                        // Set Data
                        biz_title.text(biz_name); 
                        td_deal_title.text(detail.Deal.title);
                        td_deal_valid_from.text(me.attr('data-from'));
                        td_deal_valid_to.text(me.attr('data-to'));
                        td_deal_posted_date.text(me.attr('data-posted-date'));
                        // td_deal_published_date.text(me.attr('data-published-date'));

                        var status = me.closest('tr').find('span.deal-status');
                        td_deal_status.html(status.clone());

                        // Goods Category
                        var goods_cates = me.attr('data-category');
                        td_deal_goods_group.text(goods_cates);

                        td_deal_desc.html(detail.Deal.description);
                        td_deal_conditions.html(detail.Deal.deal_condition);

                        // Related Items
                        var getRelatedItemURL__ = '<?php echo $this->webroot ?>' + 'administrator/businesses/getDealRelatedItemsAjax__' ;

                        $.ajax({
                              type: 'POST',
                              url: getRelatedItemURL__,
                            data: { deal_id : deal_id },
                              dataType: 'json',

                              success: function( data ){
                                if( data.status == true && data.result.length > 0 ){
                                  
                                  $.each( data.result , function( ind, val ){

                                    var item_id           = val.ItemDetail.id;
                                    var discounted_price  = val.DealItemLink.discount;
                                    var original_price    = val.ItemDetail.price;
                                    var av_qty            = val.DealItemLink.available_qty;

                                    if( val.DealItemLink.original_price != 0 ){
                                      original_price = val.DealItemLink.original_price;
                                    }
                                    
                                    detail_item_row_model.find('.hidden-item-id').val(item_id);
                                    detail_item_row_model.find('.hidden-item-discount').val(discounted_price);

                                    // Biz Menu
                                    var menu_name = val.ItemDetail.MenuDetail.name ;
                                    var item_name = val.ItemDetail.title;

                                    var thumb_img   = val.ItemDetail.image;
                                    thumb_img     = "<?php echo $this->webroot ?>" + thumb_img.replace('/menus/', '/menus/thumbs/');

                                    var img = '<img style="max-width:80px; max-height:60px;" src="' + thumb_img + '" />';

                                    detail_item_row_model.attr('data-id', item_id);
                                    detail_item_row_model.find('.item-image').html(img);
                                    detail_item_row_model.find('.item-menu').text(menu_name);
                                    detail_item_row_model.find('.item-title').text(item_name);
                                    detail_item_row_model.find('.item-price').text("$ " + parseFloat(original_price).toFixed(2));
                                    detail_item_row_model.find('.item-discount-price').text("$ " + parseFloat(discounted_price).toFixed(2));

                                    detail_item_row_model.find('.item-available-qty').text(parseFloat(av_qty).toFixed(2));

                                    detail_item_row_model.find('.remove-selected-item').attr('data-id', item_id);

                                    detail_table_listing.append(detail_item_row_model.show().clone());

                                  })
                                }
                              },

                              error: function(err, msg){
                                console.log(msg);
                                location.reload();
                              }
                        })
              
                        loading.hide();                       
                        div_content.show();
                        // console.log(data.data);
                      }
                  },
              
                  error: function(err){
                      
                  }

              });
      }

    });


    var expand_table_listing  = $('div#detail-expand-request-modal').find('table#table-item-listing').find('tbody');
    var expand_item_row_model = expand_table_listing.find('.extend-related-item-row');
    $('div#detail-expand-request-modal').find('table#table-item-listing').find('tbody').find('.extend-related-item-row').remove();

  $('a.btn-view-detail-expand').click( function(e){
      e.preventDefault();

      expand_table_listing.html("");

      var me      = $(this);

      var modal     = $('div#detail-expand-request-modal');
      var loading   = modal.find('#loading-img');
      var div_content = modal.find('.content');

      var biz_title   = modal.find('h3#myModalLabel').find('font');
      biz_title.text('Deal Detail');
      var deal_id   = me.attr('data-id');
      var expand_id   = me.attr('data-expand-id');

      modal.find('input#deal-id').val(deal_id);
      modal.find('input#expand-id').val(expand_id);

      var url = '<?php echo $this->webroot ?>' + 'administrator/Deals/getDealDetailAjax__/' + deal_id ;

      loading.show();
      div_content.hide();
      modal.modal('show');

      if( deal_id ){
          $.ajax({

              url: url,
              type: "POST",
              data: {},
              async: false,
              dataType: 'json',

              success: function(data){

                  if( data.status == true ){

                    var detail = data.data;

                    // Define Element
                    var td_deal_title     = div_content.find('td#deal-title');
                    var td_deal_desc      = div_content.find('td#deal-description').find('div');
                    var td_deal_conditions  = div_content.find('td#deal-conditions').find('div');
                    var td_deal_valid_from  = div_content.find('td#deal-valid-from');
                    var td_deal_valid_to  = div_content.find('td#deal-valid-to');
                    var td_deal_posted_date = div_content.find('td#deal-posted-date');
                    var td_deal_published_date = div_content.find('td#deal-published-date');

                    var td_deal_goods_group     = div_content.find('td#deal-goods-group');

                    // Get Data
                    var biz_name  = detail.BusinessInfo.business_name;

                    // Set Data
                    biz_title.text(biz_name); 
                    td_deal_title.text(detail.Deal.title);
                    td_deal_valid_from.text(me.attr('data-from'));
                    td_deal_valid_to.text(me.attr('data-to'));
                    td_deal_posted_date.text(me.attr('data-posted-date'));
                    td_deal_published_date.text(me.attr('data-published-date'));

                    // Goods Category
                    var goods_cates = $.parseJSON(detail.Deal.goods_category);
                    var st = "";
                    $.each( goods_cates, function(ind,val){
                      st += val + ", ";
                    });
                    td_deal_goods_group.text(st.substr(0, st.length -2));

                    td_deal_desc.html(detail.Deal.description);
                    td_deal_conditions.html(detail.Deal.deal_condition);

                    // Related Items
                    var getRelatedItemURL__ = '<?php echo $this->webroot ?>' + 'administrator/businesses/getDealRelatedItemsAjax__' ;

                $.ajax({
                    type: 'POST',
                    url: getRelatedItemURL__,
                    data: { deal_id : deal_id },
                    dataType: 'json',

                    success: function( data ){
                      if( data.status == true && data.result.length > 0 ){
                        
                        $.each( data.result , function( ind, val ){

                          var item_id       = val.ItemDetail.id;
                          var discounted_price  = val.DealItemLink.discount;
                          var original_price    = val.ItemDetail.price;
                          var av_qty        = val.DealItemLink.available_qty;

                          if( val.DealItemLink.original_price != 0 ){
                            original_price = val.DealItemLink.original_price;
                          }
                          
                          expand_item_row_model.find('.hidden-item-id').val(item_id);
                          expand_item_row_model.find('.hidden-item-discount').val(discounted_price);

                          // Biz Menu
                          var menu_name = val.ItemDetail.MenuDetail.name ;
                          var item_name = val.ItemDetail.title;

                          var thumb_img   = val.ItemDetail.image;
                          thumb_img     = '<?php echo $this->webroot ?>' + thumb_img.replace('/menus/', '/menus/thumbs/');
                          var img = '<img style="max-width:80px; max-height:60px;" src="' + thumb_img + '" />';

                          expand_item_row_model.attr('data-id', item_id);
                          expand_item_row_model.find('.item-image').html(img);
                          expand_item_row_model.find('.item-menu').text(menu_name);
                          expand_item_row_model.find('.item-title').text(item_name);
                          expand_item_row_model.find('.item-price').text("$ " + parseFloat(original_price).toFixed(2));
                          expand_item_row_model.find('.item-discount-price').text("$ " + parseFloat(discounted_price).toFixed(2));

                          expand_item_row_model.find('.item-available-qty').text(parseFloat(av_qty).toFixed(2));

                          expand_item_row_model.find('.remove-selected-item').attr('data-id', item_id);

                          expand_table_listing.append(expand_item_row_model.show().clone());
                        })
                      }
                    },

                    error: function(err, msg){
                      console.log(msg);
                      location.reload();
                    }
                })
          
                    loading.hide();                       
                    div_content.show();
                    // console.log(data.data);
                  }
              },
          
              error: function(err, msg ){
                  console.log(msg);
              }

          });
      }

  });

var reject_modal = $('div#reject-expand-request-modal');
var reject_form  = reject_modal.find('form');
var form_action  = reject_form.attr('action');
var biz_id       = "<?php echo $business['Business']['id'] ?>";

$('.btn-reject').click(function( event ){
  event.preventDefault();

  var expand_id = $('div#detail-expand-request-modal').find('input#expand-id').val();

  reject_form.attr('action', form_action + "/" + expand_id + "/" + biz_id);
  reject_modal.modal('show');

});

$('select#merchant-branch').change( function(event) {
    event.preventDefault();

    var me    = $(this),
        lat   = $(":selected", me).attr('data-lat'),
        lng   = $(":selected", me).attr('data-lng');

    me.closest('div').find('input#latitude').val(lat);
    me.closest('div').find('input#longitude').val(lng);

})

// Initialize Map

var listBranchMarker = {};
var map = null;
var pin_icon_blue = '<?php echo $this->webroot ?>' + 'img/pin-blue.png';
var iw = null;

function addBranchMarker(index,latLng, brandName, latLngText) {

  listBranchMarker[index] = new MarkerWithLabel({
          map:map,
          position:latLng,
          icon: pin_icon_blue,
          labelContent: brandName  ,
          title:brandName,

          labelAnchor : new google.maps.Point(45, 67),
          labelClass : "labels",
          labelInBackground : false
      });

  var b_name = new google.maps.InfoWindow({ content: brandName + latLngText });

  google.maps.event.addListener( listBranchMarker[index], "click", function (e) { b_name.open(map, this); });
  
}

function initialize() {

    var fixLatitude = <?php echo $lat; ?>;
    var fixLongtitude = <?php echo $lng; ?>;
    var desc = "<?php echo $biz_name ?>";

    var pin_icon = '<?php echo $this->webroot ?>' + 'img/pin-yellow.png';

    var myLatlng  = new google.maps.LatLng( fixLatitude, fixLongtitude );

    var mapOptions = {
                        zoom: 13,
                        center: myLatlng,
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    };

    map = new google.maps.Map(document.getElementById('div-map'), mapOptions);

    var marker = new MarkerWithLabel({
                map: map,
                position: myLatlng,
                icon : pin_icon,
                // labelContent : number,
                labelAnchor : new google.maps.Point( 5, 36 ),   
                labelClass : "pin-label",
                labelInBackground : false
            });

    var iw = new google.maps.InfoWindow({
                   content: desc
                 });


    google.maps.event.addListener(marker, "click", function (e) { iw.open(map, this); });

    var data_branch = <?php echo json_encode($data_braches) ?>;
    $.each( data_branch, function( ind, val ){
        var name  = val.branch_name,
            lat   = val.latitude,
            lng   = val.longitude,
            index = val.index;

        var newLatLng  = new google.maps.LatLng(lat, lng );

        latLngText = " (" + lat + ", " + lng + ")";
        addBranchMarker( index ,newLatLng, name, latLngText );

    });

}

google.maps.event.addDomListener(window, 'load', initialize);

$(document).ready(function(){

  $("#UserConfirmPassword").blur( function (){
        return $(this).parsley('validate');
    });

    $("#UserConfirmPasswordEdit").blur( function (){
        return $(this).parsley('validate');
    });


    $("#memberEditForm").submit( function(){
      var pass = $("#UserPasswordEdit").val();
      var cf_pass = $("#UserConfirmPasswordEdit").val();
      
      if(pass != "" && cf_pass == '' ){
        $("#UserConfirmPasswordEdit").focus();
        return false;
      }
      
    })



  $('table#category-table-list').find('tr.menu-row.selected').click();

  $('#datetimepicker1, #datetimepicker2').datetimepicker();
  $('#datetimepicker1, #datetimepicker2').find('input').attr('readonly', 'readonly');
  $('#datetimepicker1, #datetimepicker2').find('input').css('cursor', 'default');


  $('#datetimepicker1-repost, #datetimepicker2-repost').datetimepicker();
  $('#datetimepicker1-repost, #datetimepicker2-repost').find('input').attr('readonly', 'readonly');
  $('#datetimepicker1-repost, #datetimepicker2').find('input').css('cursor', 'default');

  $('#dealValidFromPicker, #dealValidToPicker').datetimepicker();
  $('#dealValidFromPicker, #dealValidToPicker').find('input').attr('readonly', 'readonly');
  $('#dealValidFromPicker, #dealValidToPicker').find('input').css('cursor', 'default');

  Shadowbox.init();

});

$('.tab-container li').click( function( event ){
    event.preventDefault();

    var tab = $(this).find('a').attr('tab');
    var base_url = "<?php echo $base_url ?>";

    var new_url = base_url + "/" + tab;

    window.history.pushState("", "", new_url);

});

// For Revenue & Commission
$('.expand').click( function( event ){

  event.preventDefault();

  var me    = $(this);
  var tr    = me.closest('tr');
  var tbody   = tr.closest('tbody');
  var deal_code = me.attr('data-code');

  tbody.find('.indicator').find('strong').text('+');

  tbody.find('tr').not(tr).removeClass('row-active');
  tr.toggleClass('row-active');

  tbody.find('tr.sub-row').not('tr.sub-row-' + deal_code).removeClass('sub-row-active');
  tbody.find('tr.sub-row-' + deal_code).toggleClass('sub-row-active');

  if( tr.hasClass('row-active') ){
    tr.find('.indicator').find('strong').text('-');
  }

  // Hide Tran Detail
  tbody.find('tr.tran-detail').removeClass('tran-detail-active');
  tbody.find('tr.sub-row').removeClass('sub-row-click');

})

$('.sub-row').click( function(event){
  event.preventDefault();

  var me    = $(this),
    tbody   = me.closest('tbody'),
    tran_id = me.attr('data-id') ;

  tbody.find('tr.sub-row').not(me).removeClass('sub-row-click');
  me.toggleClass('sub-row-click');

  tbody.find('tr.tran-detail').not(me).removeClass('tran-detail-active');
  tbody.find('tr.tran-'+ tran_id ).toggleClass('tran-detail-active');

});

  
$('#btn-search-revenue').click( function(event){

    event.preventDefault();

    var modal = $('div#search-revenue-modal');

    modal.modal('show');

  });

$('#btn-search-clear-revenue').click( function( event ){

  event.preventDefault();

  $('.reset-revenue-btn').click();
  $('div#search-revenue-modal').find('form#transactionSearchForm').submit();
});

$('.reset-revenue-btn').click( function( event ){

  event.preventDefault();

  var modal = $('div#search-revenue-modal');

  modal.find('input[type="text"]').val("");

});

  
$('#btn-search-balance').click( function(event){

    event.preventDefault();

    var modal = $('div#search-balance-modal');

    modal.modal('show');

  });

$('#btn-search-balance-clear').click( function( event ){

  event.preventDefault();

  $('.reseet-balance-btn').click();
  $('div#search-balance-modal').find('form#balanceSearchForm').submit();
});

$('.reseet-balance-btn').click( function( event ){

  event.preventDefault();

  var modal = $('div#search-balance-modal');

  modal.find('input[type="text"]').val("");

});


</script>