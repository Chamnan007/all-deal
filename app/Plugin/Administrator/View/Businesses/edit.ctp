<?php 
  /********************************************************************************
      File: Edit Business
      Author: PHEA RATTANA
  
      Confidential ABi Technologies property.
  
      Changed History:
      Date          Author        Description
      2014/01/05        PHEA RATTANA    Initial
  *********************************************************************************/

  $pro_name = array();

  if( !empty($provinces) ){
    foreach( $provinces as $key => $value ){
      $pro_name[$value['Province']['province_code']] = $value['Province']['province_code'] . " - " . $value['Province']['province_name'];
    }
  }

  $city_name = array();
  if( !empty($cities) ){
    foreach( $cities as $key => $value ){
      $city_name[$value['City']['city_code']] = $value['City']['city_code'] . " - " . $value['City']['city_name'];
    }
  }

  $selected_loc_id = $this->request->data['Business']['location_id'];
  $location_arr = array();
  if( !empty($locations) ){
    foreach( $locations as $key => $value ){
      $location_arr[$value['Location']['id']] = $value['Location']['location_name'] ;

      if( $value['Location']['id'] == $selected_loc_id || $value['Location']['location_name'] == $this->request->data['Business']['location'] ){
          $selected_loc_id = $value['Location']['id'];
      }
    }
  }

  $status_arr = array();  

  foreach ($status as $key => $value) {
    $status_arr[$key] = $value['status'];
  }


  $bis_categories = array();

  if( !empty($businessCategories) ){
    foreach( $businessCategories as $key => $value ){
      $bis_categories[$value['BusinessCategory']['id']] = $value['BusinessCategory']['category'] ;
    }
  }

  $sub_categories = array();

  if( !empty($subCates) ){
    foreach( $subCates as $key => $value ){
      $sub_categories[$value['BusinessCategory']['id']] = $value['BusinessCategory']['category'] ;
    }
  }

  $lat = $this->request->data['Business']['latitude'];
  $lng = $this->request->data['Business']['longitude'];

?>

<div class="businesses form">
  <div class="row-fluid">   
    <!-- Pie: Box -->
    <div class="span12">

      <!-- Pie: Top Bar -->
      <div class="top-bar">
        <h3><i class="icon-list"></i> Manage Merchants</h3>
      </div>
      <!-- / Pie: Top Bar -->

      <!-- Pie: Content -->
      <div class="tab-content">

      <a href="<?php echo $this->Html->url( array('action' => 'index')); ?>"> 
        <button class="btn btn-linkedin" type="button"><i class="icon-list-alt"></i> Merchants List</button>
      </a>

      <a href="<?php echo $this->Html->url( array('action' => 'view', $this->request->data['Business']['id'])); ?>"> 
        <button class="btn btn-skype" type="button"><i class="icon-list-alt"></i> Merchant Detail</button>
      </a>
      

      <div class="clearfix"></div>
        
        <?php echo $this->Form->create('Business', array('data-validate'=>'parsley', 'enctype' => "multipart/form-data") ); ?>
          <fieldset style="margin-bottom: 0px;">

            <input type="hidden" id="hidden-location-id" 
                    value="<?php echo $selected_loc_id ?>" 
                    name="data[Business][location_id]">

            <legend><?php echo __('Edit Merchant Information'); ?></legend>
            
            <?php if( $this->request->data['Business']['status'] == 1 ){ ?>
                <input type="hidden" name="done-activated" value="1">
            <?php } ?>

            <div class="span3">
              <?php

                echo $this->Form->input('business_name', 
                            array('placeholder' => 'Merchant Name',
                                'label' => 'Merchant Name',
                                'required'=>'required',
                                'type'=>'textarea',
                                'maxlength' => 300));

                // echo $this->Form->input(  'province' ,array(
                //                'type'     => 'select',
                //                'label'    => 'Province',
                //                'empty'    => 'Select a province',
                //                'options'  => $pro_name
                //             ));

                echo $this->Form->input(  'city' ,array(
                               'type'     => 'select',
                               'label'    => 'Province/ City',
                               'empty'    => "Select a province/city",
                               'options'  => $city_name
                            ));

                // echo $this->Form->input(  'location' ,array(
                //                'type'     => 'select',
                //                'label'    => 'Location/ Khan',                                
                //                'required' =>'required',
                //                'empty'    => "Select a location",
                //                'options'  => $location_arr
                //             ));

        ?>
              
          <div class="input select">
              <label for="BusinessLocation">Location/ Khan</label>
              <select name="data[Business][location]" required="required" id="BusinessLocation" class="parsley-validated">
                  <option value="" data-id="">Select a location</option>  
                  <?php foreach( $location_arr as $k => $val ){ 
                        $selected = ( $k == $this->request->data['Business']['location_id'] || $val == $this->request->data['Business']['location'])?" selected='selected'":"";
                    ?>
                    <option value="<?php echo $val ?>" data-id="<?php echo $k ?>" <?php echo $selected ?>><?php echo $val ?></option>
                  <?php } ?>
              </select>
          </div>


          <div class="input select">
              <label for="BusinessLocation">Sangkat</label>
              <select name="data[Business][sangkat_id]" id="BusinessSangkatId">
                  <option value="">select a sangkat</option>  
                  <?php foreach( $SangkatData as $k => $val ){ 
                        $selected = ( $val['Sangkat']['id'] == $this->request->data['Business']['sangkat_id'])?" selected='selected'":"";
                    ?>
                    <option value="<?php echo $val['Sangkat']['id'] ?>" data-id="<?php echo $k ?>" <?php echo $selected ?>><?php echo $val['Sangkat']['name'] ?></option>
                  <?php } ?>
              </select>
          </div>

        <?php

                echo $this->Form->input('street', array('placeholder' => 'Street'));

                echo $this->Form->input('latitude', array('placeholder' => 'Latitude', 'type' => 'text', 'maxlength' => 15, 'readonly' => true ));

                echo $this->Form->input('longitude', array('placeholder' => 'Longitude', 'type' => 'text', 'maxlength' => 15, 'readonly' => true));

              ?>  
            </div>

            <div class="span3" style="margin-left:0px;">
              <?php 

                echo $this->Form->input(  'business_main_category' ,array(
                               'type'     => 'select',
                               'label'    => 'Main Category',
                               'required' => 'required',
                               'empty'    => 'Select a category',
                               'options'  => $bis_categories
                            ));

                echo $this->Form->input('website', array('placeholder' => 'www.example.com', 
                                    'type' => 'text' ));

                echo $this->Form->input('email', array('placeholder' => 'example@email.com', 
                                    'type' => 'email',
                                    'required' => true ));

                echo $this->Form->input('phone1', array('placeholder' => 'Phone Number','required'=>'required'));
                echo $this->Form->input('phone2', array('placeholder' => 'Phone Number'));

                $acc_payments = json_decode($this->request->data['Business']['accept_payment']);

            ?>



              <div class="input select">
                  
                  <label for="ModelName" style='font-weight: bold;'>Accept Payment</label>

                  <div class="checkbox">
                      <input name="data[Business][accept_payment][]" value="cash" id="ModelName1" type="checkbox"
                            <?php echo(!empty($acc_payments) && in_array('cash', $acc_payments))?" checked='checked'":"" ?>>
                      <label for="ModelName1">
                        <?php echo $this->Html->image('cash.jpg', array(  'alt' => 'Cash',
                                                  'title'=>'Cash',
                                                  'style'=>'width:50px;')); ?>
                      </label>
                  </div>
                  
                  <div class="checkbox">
                      <input name="data[Business][accept_payment][]" value="mastercard" id="ModelName3" type="checkbox"
                            <?php echo(!empty($acc_payments) && in_array('mastercard', $acc_payments))?" checked='checked'":"" ?>>
                      <label for="ModelName3">
                        <?php echo $this->Html->image('mastercard.jpg', array(  'alt' => 'Master Card',
                                                  'title'=>'Master Card',
                                                  'style'=>'width:50px;')); ?>
                      </label>
                  </div>
                  
                  <div class="checkbox" style='clear:both; margin-top:5px;'>
                       <input name="data[Business][accept_payment][]" value="visa" id="ModelName4" type="checkbox"
                              <?php echo(!empty($acc_payments) && in_array('visa', $acc_payments))?" checked='checked'":"" ?>>
                      <label for="ModelName4">
                        <?php echo $this->Html->image('visa.jpg', array(  'alt' => 'Visa',
                                                  'title'=>'Visa',
                                                  'style'=>'width:50px;')); ?>
                      </label>
                  </div>

              </div>

              <div style="clear:both; padding-top:27px;"></div>
            </div>

            <div class="span6" style="margin-left: 0px;">
              
              <?php 


                echo $this->Form->input('description', array('placeholder' => 'Business Description',
                                        'label'     => 'Business Description',
                                        'type'      => 'textarea',
                                        'class'     => 'ckeditor',
                                        'maxlength' => '65535'));

               ?>

              <div class="span6" style="margin-left:0px;">

                <div class="input select" style="margin-top:18px;">
                  <label for="BusinessStatus">Member Level</label>
                  <select name="data[Business][member_level]" id="BusinessStatus">
                    <?php foreach($member_biz_level as $k => $v){ ?>
                      <option value="<?php echo $k ?>" 
                          <?php echo ($k == $this->request->data['Business']['member_level'])?" selected='selected'":"" ?>><?php echo $v ?></option>
                    <?php } ?>
                  </select>
                </div>

              </div>

              <div class="span5" style="margin-top:18px;">

                <?php 
                  echo $this->Form->input( 'status' ,array(
                                 'type'     => 'select',
                                 'label'  => 'Status',
                                 'options'  => $status_arr
                            ));
                ?>

              </div>

            </div>

            <div class="clreafix"></div>

            <div class="span7" style="margin-left:0px; margin-top:30px; clear:both;">
                <h5 style="margin-left:15px !important;"><i class="icon-fullscreen"></i> Drag pin icon to adjust location or right-click to get main location</h5>

                <div class="span11" style="height:310px; border: 1px solid #CCC; margin-bottom:20px; padding:5px;">
                  <div class="span12" id='map-div' style="margin-left:0px;height:300px;"></div>            
                </div>
            </div>

            <div class="span5" style="margin-left:0px; margin-top:30px;">
                <div class="span12">
                    <h5 style="float:left"><i class="icon-flag-alt"></i> Branches</h5>
                    <a href="#" class="btn btn-linkedin pull-right" id="add-branch"><i class="icon-plus"></i> Add Branch</a>
                </div>
                <table width="100%" style="clear:both;" class="table-list">
                    <tr>
                        <th style="text-align:left">Branch Name</th>
                        <th width="100px;" style="text-align:left">Latitude</th>
                        <th width="100px;" style="text-align:left">Longitude</th>
                        <th width="20px;"></th>
                    </tr>
                    <tr id="row-branch-model" data-key="">
                        <input type="hidden" id="hidden-branch-name" name="data[BusinessBranch][branch_name][]" maxlength="100">
                        <input type="hidden" id="hidden-branch-latitude" name="data[BusinessBranch][latitude][]">
                        <input type="hidden" id="hidden-branch-longitude" name="data[BusinessBranch][longitude][]">
                        <td id="show-branch-name"></td>
                        <td id="show-branch-latitude"></td>
                        <td id="show-branch-longitude"></td>
                        <td style="text-align:center; color:red; font-size:15px;">
                            <i class="icon-trash remove-branch" title="Remove" data-key=""></i>
                        </td>
                    </tr>

                    <?php 
                      $data_branches = array();
                      
                      if( $branches && !empty($branches) ) {
                          $time = time();                          
                          foreach( $branches as $key => $val ){

                            $time += 300;

                            $data_branches[$key]['index'] = $time;
                            $data_branches[$key]['branch_name'] = $val['BusinessBranch']['branch_name'];
                            $data_branches[$key]['latitude'] = $val['BusinessBranch']['latitude'];
                            $data_branches[$key]['longitude'] = $val['BusinessBranch']['longitude'];

                    ?>  
                        <tr id="<?php echo $time ?>" data-key="<?php echo $time ?>">
                            <input type="hidden" id="hidden-branch-name" name="data[BusinessBranch][branch_name][]" maxlength="100"
                                value="<?php echo $val['BusinessBranch']['branch_name'] ?>" >
                            <input type="hidden" id="hidden-branch-latitude" name="data[BusinessBranch][latitude][]"
                                value="<?php echo $val['BusinessBranch']['latitude'] ?>" >
                            <input type="hidden" id="hidden-branch-longitude" name="data[BusinessBranch][longitude][]"
                                value="<?php echo $val['BusinessBranch']['longitude'] ?>" >
                            <td id="show-branch-name"><?php echo $val['BusinessBranch']['branch_name'] ?></td>
                            <td id="show-branch-latitude"><?php echo $val['BusinessBranch']['latitude'] ?></td>
                            <td id="show-branch-longitude"><?php echo $val['BusinessBranch']['longitude'] ?></td>
                            <td style="text-align:center; color:red; font-size:15px;">
                                <i class="icon-trash remove-branch" title="Remove" data-key="<?php echo $time ?>"></i>
                            </td>
                        </tr>
                    <?php 
                          }
                        } 
                    ?>

                      <tr id="empty-branch">
                          <td colspan="4">
                              <i>Empty branch.</i>
                          </td>
                      </tr>

                </table>

            </div>            
            
            <div class='clearfix span4'>

              <?php 
                echo $this->Form->submit(__('Save'), array('class' => 'btn btn-primary') ); 
               ?>

            </div>  
            
          </fieldset>
      

      </div>
      <!-- / Pie: Content -->

    </div>
    <!-- / Pie -->
    
  </div>

</div>

<div id="add-branch" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
    style="width:1024px; padding-bottom: 10px !important; left:33% !important">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel"><i class="icon-flag-alt"></i> Add New Branch</h3>
  </div>

  <div class="span12" style="padding-top: 10px; padding-bottom: 20px; width: 985px;">
      <div class="span6" style="margin-left:0px;">
        <label for="branch-name" style="float:left ;margin-right:10px; padding-top:4px; font-weight: bold;">Branch Name : </label>
        <input type="text" class="form-control" style="width:320px;" id="branch-name" placeholder="Branch Name">
      </div>

      <div class="span6">
          <label for="branch-latitude" style="float:left ;margin-right:10px; padding-top:4px; font-weight: bold;">Latitude : </label>
          <input type="text" id="branch-latitude" placeholder="Latitude" style="width:100px; float:left; margin-right:30px;" readonly="readonly">

          <label for="branch-longitude" style="float:left ;margin-right:10px; padding-top:4px; font-weight: bold;">Longitude : </label>
          <input type="text" id="branch-longitude" placeholder="Longitude" style="width:100px; float:left" readonly="readonly">
      </div>

  </div>

  <h5 style="margin-left:20px !important; clear:both"><i class="icon-fullscreen"></i> Drag pin icon to adjust location or right-click to get location
  </h5>

  <div class="span12" style="padding-bottom: 20px; width: 985px; height: 250px; border: 1px solid #CCC" id="add-branch-map">
        
  </div>

  <div class="span12" style="width:1000px;">
    <div class="pull-right" style="padding:20px;">
        <button class="btn btn-linkedin" data-dismiss='modal' type="button" id="btn-add-branch">Select Location</button>
        <button class="btn btn-google cancel" data-dismiss='modal' type="button">Close</button>
    </div>
  </div>

</div> 


<div id="message" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true"
  style="">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel"  style="color:red"><i class="icon-exclamation"></i> Warning !</h3>
  </div>

  <div class="span6" style="padding-top: 10px; padding-bottom: 20px;">
    
    <h5 id="message"></h5>

    <button class="btn btn-google cancel" data-dismiss='modal' type="button">Close</button>
  </div>

</div> 

<div id="remove-branch-modal" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true"
  style="">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel" style="color:red"><i class="icon-exclamation"></i> Are you sure ?</h3>
  </div>

  <div class="span6" style="padding-top: 10px; ">    
    <h5>Are you sure you want to remove branch?</h5>
  </div>

   <div class="span7 clearfix">
    <div class="pull-left" style="padding:20px;">
        <button class="btn btn-primary" data-dismiss='modal' type="button" id="confirm-remove-branch">Yes</button>
        <button class="btn btn-google cancel" data-dismiss='modal' type="button">Close</button>
    </div>
  </div>

</div> 

<style>

  .gmnoprint img { max-width: none; }

  .equalto, .required{
    color: red;
    font-size: 12px !important;
    list-style-type: none;
  }

  .remove-branch:hover{
      cursor: pointer;
  }

</style>

<script src="<?php echo $this->webroot . "js/admin/markerwithlabel.js" ?>" type="text/javascript"></script>
<script src="https://google-maps-utility-library-v3.googlecode.com/svn/tags/markerwithlabel/1.1.9/src/markerwithlabel.js" type="text/javascript"></script>


<script type="text/javascript">


var branch_list = $('table.table-list');

var branch_row_model = $('tr#row-branch-model');
$('tr#row-branch-model').remove();

var branch_row_empty = $('tr#empty-branch');

var listBranchMarker = {};
var map = null;
var pin_icon_blue = '<?php echo $this->webroot ?>' + 'img/pin-blue.png';
var iw = null;

function addBranchMarker(index, latLng, brandName) {

    listBranchMarker[index] = new MarkerWithLabel({
            map:map,
            position:latLng,
            icon: pin_icon_blue,
            title: brandName
        });

    var b_name = new google.maps.InfoWindow({ content: brandName });

    // google.maps.event.addListener( listBranchMarker[index], "click", function (e) { b_name.open(map, this); });

    google.maps.event.addListener( listBranchMarker[index], "mouseover", function (e) { b_name.open(map, this); });
    google.maps.event.addListener( listBranchMarker[index], "mouseout", function (e) { b_name.close(map, this); });

}

function removeMarker(index){

    var tmpIndex= String(index);        
    var obj = listBranchMarker[tmpIndex];

    obj.setMap(null);
    obj = null;

}

$(document).ready(function(){

    var branch_count = "<?php echo count($branches) ?>";
    if( branch_count > 0 ){
        $('tr#empty-branch').remove();
    }

    $('div#add-branch').on('shown.bs.modal',function(){
        initializeBranchMap();
    });

    $('a#add-branch').click( function(event){
        event.preventDefault();
        $('div#add-branch').find('#branch-name').val('');
        $('div#add-branch').modal('show');
    
    });

    branch_list.on('click', '.remove-branch', function(){
        var key = $(this).attr('data-key');
        $('div#remove-branch-modal').find('#confirm-remove-branch').attr('data-key', key);
        $('div#remove-branch-modal').modal('show');
    
    });

    $('#confirm-remove-branch').click( function(){

        var me = $(this);
        var key_time = me.attr('data-key');

        branch_list.find('tr[data-key="'+ key_time +'"]').remove();
        removeMarker(key_time);

        var count = branch_list.find('tr').length;

        if(count == 1 ){
            branch_list.append(branch_row_empty);
        }
    
    })

    $('#btn-add-branch').click( function(){

        var time = new Date().getTime();

        var branch_name = $('input#branch-name').val().trim();
        var lat         = $('input#branch-latitude').val();
        var lng         = $('input#branch-longitude').val();

        if( branch_name == "" ){
            $("div#message").find('h5').text("Please enter Branch Name !");
            $('div#message').modal('show');
            return false;
        }

        branch_row_empty.remove();

        branch_row_model.attr('id', time);
        branch_row_model.attr('data-key', time );

        // Set Hidden
        branch_row_model.find('input#hidden-branch-name').val(branch_name);
        branch_row_model.find('input#hidden-branch-latitude').val(lat);
        branch_row_model.find('input#hidden-branch-longitude').val(lng);

        // Set showing data
        branch_row_model.find('td#show-branch-name').text(branch_name);
        branch_row_model.find('td#show-branch-latitude').text(lat);
        branch_row_model.find('td#show-branch-longitude').text(lng);
        branch_row_model.find('i.remove-branch').attr('data-key', time);

        branch_list.append(branch_row_model.clone());

        var newLatLng  = new google.maps.LatLng(lat, lng );
        addBranchMarker( time ,newLatLng, branch_name );

    })

    $("input#BusinessPhone1, input#BusinessPhone2").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) || 
             // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }

    });

    $("#BusinessCity").change( function (){
      var me = $(this);

      var locationSelect  = $("#BusinessLocation");
      var SangkatSelect  = $("#BusinessSangkatId");

      var city_code = me.val();

      var url = '<?php echo $this->webroot ?>' + 'administrator/locations/get_location_by_city_code/' + city_code ;

      if( city_code != '' ){
        $.ajax({
                type: 'POST',
                url: url,
                data: { city_code : city_code },
                dataType: 'json',
                
                beforeSend: function(){
                    locationSelect.html("<option>loading...</option>");
                    SangkatSelect.html("<option>loading...</option>");
                },
                success: function (data){
                    // console.log(data);

                    var data = data.data;

                    locationSelect.html("<option value=''>select a location</option>");
                    SangkatSelect.html("<option value=''>select an option</option>");
                    
                    $.each( data, function (ind, val ){
                      var loc = val.Location;

                      locationSelect.append("<option value='" + loc.location_name + "'>" + loc.location_name + "</option>" )
                    });

                },

                error: function(xhr, status, error) {
                   var err = eval("(" + xhr.responseText + ")");
                 }

            });

      }else{
        locationSelect.html("<option value=''>select an option</option>");
        SangkatSelect.html("<option value=''>select an option</option>");
      }

    });

    $("#BusinessLocation").change( function (){
      var me = $(this);

      var SangkatSelect  = $("#BusinessSangkatId");

      var optoin_selected = $('#BusinessLocation option:selected');
      var location_id =   optoin_selected.attr('data-id');

      $('input#hidden-location-id').val(location_id);

      var url = '<?php echo $this->webroot ?>' + 'administrator/sangkats/getSangkatByLocation/' + location_id ;

      if( location_id != '' ){
        $.ajax({
                type: 'POST',
                url: url,
                data: { location_id : location_id },
                dataType: 'json',
                
                beforeSend: function(){
                    SangkatSelect.html('<option>Loading...</option>');
                },
                success: function (result){
                    var data = result.data;

                    // console.log(data);

                    SangkatSelect.html("<option value=''>select an option</option>");

                    $.each( data, function (ind, val ){
                      var sk = val.Sangkat;
                      SangkatSelect.append("<option value='" + sk.id + "'>" + sk.name + "</option>" );

                    });

                },

                error: function(xhr, status, error) {
                  var err = eval("(" + xhr.responseText + ")");
                    console.log(err);
                }

            });

      }else{
        SangkatSelect.html("<option value=''>select an option</option>");
      }

    });

    // Load Map
    google.maps.event.addDomListener(window, 'load', initialize);

});

function initialize() {

    var fixLatitude = <?php echo $lat; ?>;
    var fixLongtitude = <?php echo $lng; ?>;

    var pin_icon = '<?php echo $this->webroot ?>' + 'img/pin-yellow.png';

    var myLatlng  = new google.maps.LatLng( fixLatitude, fixLongtitude );

    var mapOptions = {
                        zoom: 14,
                        center: myLatlng,
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    };

    map = new google.maps.Map(document.getElementById('map-div'), mapOptions);

    var marker = new MarkerWithLabel({
                map: map,
                position: myLatlng,
                icon : pin_icon,
                labelAnchor : new google.maps.Point( 5, 36 ),   
                labelClass : "pin-label",
      draggable: true
            });

    var latInput = $("#BusinessLatitude");
    var lngInput = $("#BusinessLongitude");

    google.maps.event.addListener(marker, "dragend", function(marker){

      var latLng = marker.latLng;

      latInput.val(parseFloat(latLng.lat()).toFixed(4));
      lngInput.val(parseFloat(latLng.lng()).toFixed(4));

    });

    //##### drop a new marker on right click ######
    google.maps.event.addListener(map, 'rightclick', function(event) {

        var latLng = event.latLng;

        latInput.val(parseFloat(latLng.lat()).toFixed(4));
        lngInput.val(parseFloat(latLng.lng()).toFixed(4));

        var newLatLng  = new google.maps.LatLng( latLng.lat(), latLng.lng() );

        marker.setPosition(newLatLng);

    });

    var iw = new google.maps.InfoWindow({
               content: "Drag this icon to adjust location."
             });

    google.maps.event.addListener(marker, "mouseover", function (e) { iw.open(map, this); });
    google.maps.event.addListener(marker, "mouseout", function (e) { iw.close(map, this); });

    // Set Branche Markers on Map
    var branches = <?php echo json_encode($data_branches) ?>;

    $.each( branches, function( ind, val ){

        var time        = val.index,
            branch_name = val.branch_name,
            lat         = val.latitude,
            lng         = val.longitude ;

        var newLatLng  = new google.maps.LatLng(lat, lng );
        addBranchMarker( time ,newLatLng, branch_name );

    })

}

function initializeBranchMap() {

    var CurrentLat = <?php echo $lat; ?>;
    var CurrentLng = <?php echo $lng; ?>;

    if(navigator.geolocation) {
        navigator.geolocation.getCurrentPosition (
            // successFunction
            function(position) {
                CurrentLat = position.coords.latitude;
                CurrentLng = position.coords.longitude;
            }
        );
    }

    var latInput = $("#branch-latitude");
    var lngInput = $("#branch-longitude");
    
    var fixLatitude = CurrentLat;
    var fixLongtitude = CurrentLng;

    latInput.val(parseFloat(CurrentLat).toFixed(4));
    lngInput.val(parseFloat(CurrentLng).toFixed(4));
    var myNewLatlng  = new google.maps.LatLng( fixLatitude, fixLongtitude );

    var mapOptions = {
                        zoom: 14,
                        center: myNewLatlng,
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    };

    var branch_map = new google.maps.Map(document.getElementById('add-branch-map'), mapOptions);

    var BranchMarker = new MarkerWithLabel({
              map: branch_map,
              position: myNewLatlng,
              icon : pin_icon_blue,
              labelAnchor : new google.maps.Point( 5, 36 ),   
              labelClass : "pin-label",
              draggable: true
            });

    google.maps.event.addListener(BranchMarker, "drag", function(BranchMarker){

      var latLng = BranchMarker.latLng;

      latInput.val(parseFloat(latLng.lat()).toFixed(4));
      lngInput.val(parseFloat(latLng.lng()).toFixed(4));

    });

    google.maps.event.addListener(BranchMarker, "dragend", function(event){

      var latLng = event.latLng;

      latInput.val(parseFloat(latLng.lat()).toFixed(4));
      lngInput.val(parseFloat(latLng.lng()).toFixed(4));

    });

    //##### drop a new marker on right click ######
    google.maps.event.addListener(branch_map, 'rightclick', function(event) {

        var latLng = event.latLng;

        latInput.val(parseFloat(latLng.lat()).toFixed(4));
        lngInput.val(parseFloat(latLng.lng()).toFixed(4));

        var newLatLng  = new google.maps.LatLng( latLng.lat(), latLng.lng() );

        BranchMarker.setPosition(newLatLng);

    });

}

</script>