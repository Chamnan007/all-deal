
<!-- Upload Images -->
<div id="media_add_image" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true"
  style="padding-bottom: 30px !important; ">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel"><i class="icon-picture"></i> Upload Pictures</h3>
  </div>

  <!-- Any Info-customization -->
  <form action="<?php echo $this->Html->url(array('action' => 'uploadMedia', $business['Business']['id'], 0 )); ?> " 
      style="padding: 20px;" method="POST" id="ChangeLogoForm"
      enctype = "multipart/form-data" >

    <div>
      <h5>Select Pictures : ( JPG, JPEG, PNG ) 300  x 300 Pixels minimum.</h5>
      <br>
      <input type="file" name="images[]" multiple required='required'>
    </div>

    <div class="submit" style="margin-top: 20px; clear: both;">
      <input class="btn btn-primary" type="submit" value="Upload">
      <button class="btn btn-google cancel" data-dismiss='modal' type="button">Cancel</button>
    </div>
      
  </form>

</div> 

<!-- Upload Video -->
<div id="media_add_video" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true"
  style="padding-bottom: 30px !important; ">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel"><i class="icon-edit"></i> Add Video</h3>
  </div>

  <!-- Any Info-customization -->
  <form action="<?php echo $this->Html->url(array('action' => 'uploadMedia', $business['Business']['id'], 1)); ?>" 
      style="padding: 20px;" method="POST"
      enctype = "multipart/form-data" >

    <div>
      <h5>Video URL : </h5>
      <i style="color:#999">***Note: link from youtube only.</i>
      <!-- <textarea name="video_url" cols="30" rows="10" required="required" style="width: 90% !important"></textarea> -->
      <input type="text" name="video_url" required="required" style="width: 90% !important">
    </div>

    <div class="submit" style="margin-top: 20px; clear: both;">
      <input class="btn btn-primary" type="submit" value="Save">
      <button class="btn btn-google cancel" data-dismiss='modal' type="button">Cancel</button>
    </div>
      
  </form>

</div>


<!-- Remove Business Media -->
<div id="remove_img" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel"><i class="icon-picture"></i> Are You Sure ?</h3>
  </div>

  <form action="#" style="padding: 20px;" method="POST" id="remove_image"
      enctype = "multipart/form-data" >

    <div>
      <h5>Are you sure you want to delete this image?</h5>
    </div>

    <div class="submit" style="margin-top: 20px; clear: both;">
      <input class="btn btn-primary" data-dismiss='modal' type='button'  value="Yes" id="yes">
      <button class="btn btn-google cancel" data-dismiss='modal' type="button">No</button>
    </div>
      
  </form>

</div>

<!-- Deactivate User -->
<div id="remove_video" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel"><i class="icon-picture"></i> Are You Sure ?</h3>
  </div>

  <form action="#" style="padding: 20px;" method="POST" id="remove_image"
      enctype = "multipart/form-data" >

    <div>
      <h5>Are you sure you want to delete this video?</h5>
    </div>

    <div class="submit" style="margin-top: 20px; clear: both;">
      <input class="btn btn-primary" data-dismiss='modal' type='button'  value="Yes" id="yes1">
      <button class="btn btn-google cancel" data-dismiss='modal' type="button">No</button>
    </div>
      
  </form>

</div>