<?php 
  /********************************************************************************
      File: Add New Business
      Author: PHEA RATTANA
  
      Confidential ABi Technologies property.
  
      Changed History:
      Date          Author        Description
      2014/01/05        PHEA RATTANA    Initial
  *********************************************************************************/

  $pro_name = array();

  if( !empty($provinces) ){

    foreach( $provinces as $key => $value ){
      $pro_name[$value['Province']['province_code']] = $value['Province']['province_code'] . " - " . $value['Province']['province_name'];
    }

  }

  $city_name_arr = array();

  if( !empty($cities) ){

    foreach( $cities as $key => $value ){
      $city_name_arr[$value['City']['city_code']] = $value['City']['city_code'] . " - " . $value['City']['city_name'];
    }

  }

  $status_arr = array();  

  foreach ($status as $key => $value) {
    $status_arr[$key] = $value['status'];
  }


  $bis_categories = array();

  if( !empty($businessCategories) ){
    foreach( $businessCategories as $key => $value ){
      $bis_categories[$value['BusinessCategory']['id']] = $value['BusinessCategory']['category'] ;
    }
  }


?>

<div class="businesses form">
  <div class="row-fluid">   
    <!-- Pie: Box -->
    <div class="span12">

      <!-- Pie: Top Bar -->
      <div class="top-bar">
        <h3><i class="icon-list"></i> Manage Merchants</h3>
      </div>
      <!-- / Pie: Top Bar -->

      <!-- Pie: Content -->
      <div class="tab-content">

      <a href="<?php echo $this->Html->url( array('action' => 'index')); ?>"> 
        <button class="btn btn-linkedin" type="button"><i class="icon-list-alt"></i> Merchants List</button>
      </a>

      <div class="clearfix"></div>
        
        <?php echo $this->Form->create('Business', array('data-validate'=>'parsley', 'enctype' => "multipart/form-data") ); ?>
          <fieldset style="margin-bottom: 20px;">

            <legend><?php echo __('Create New Merchant'); ?></legend>

            <input type="hidden" id="hidden-location-id" value="0" name="data[Business][location_id]">

            <div class="span3">
              <?php
                echo $this->Form->input('business_name', 
                            array('placeholder' => 'Merchant Name',
                                  'label' => 'Merchant Name',
                                'required'=>'required',
                                'type'=>'textarea',
                                'maxlength' => '100'));

                // echo $this->Form->input(  'province' ,array(
                //                'type'     => 'select',
                //                'label'    => 'Province',
                //                'empty'    => 'Select a province',
                //                'required' => true,
                //                'options'  => $pro_name
                //             ));

                echo $this->Form->input(  'city' ,array(
                               'type'     => 'select',
                               'label'    => 'Province/ City',
                               'required' => true,
                               'empty'  => 'select an province/city',
                               'options'  => $city_name_arr

                            ));

                echo $this->Form->input(  'location' ,array(
                               'type'     => 'select',
                               'label'    => 'Location/ Khan',
                               'empty'  => 'select a location/khan',
                               'required' => true,
                               'options'  => array()
                            ));

                echo $this->Form->input(  'sangkat_id' ,array(
                               'type'     => 'select',
                               'label'    => 'Sangkat',
                               'empty'  => 'select a sangkat',
                               'options'  => array()
                            ));


                echo $this->Form->input('street', array('placeholder' => 'Street'));


                echo $this->Form->input('latitude', array(
                              'placeholder' => 'Latitude', 
                              'type' => 'text', 
                              'maxlength' => 15,
                              'value' => 0,
                              'readonly' => true ));

                echo $this->Form->input('longitude', array(
                              'placeholder' => 'Longitude', 
                              'type' => 'text', 
                              'maxlength' => 15,
                              'value' => 0,
                              'readonly' => true));


              ?>  

            </div>

            <div class="span3" style="margin-left:0px;">
              <?php 

                echo $this->Form->input(  'business_main_category' ,array(
                               'type'     => 'select',
                               'label'    => 'Main Category',
                               'required' => true,
                               'empty'    => 'Select a category',
                               'options'  => $bis_categories
                            ));


                // echo $this->Form->input(  'business_sub_category' ,array(
                //                'type'     => 'select',
                //                'label'    => 'Sub Category',
                //                'empty'    => 'Select a category',
                //                'options'  => array()
                //             ));

                echo $this->Form->input('website', array('placeholder' => 'www.example.com', 
                                    'type' => 'text' ));

                echo $this->Form->input('email', array('placeholder' => 'example@email.com', 
                                    'type' => 'email',
                                    'required' => 'required' ));

                echo $this->Form->input('phone1', array('placeholder' => 'Phone Number','required'=>'required'));

                echo $this->Form->input('phone2', array('placeholder' => 'Phone Number'));
                
               ?>



              <div class="span12" style="margin:0px !important">
                <div class="input select" style="">
                  <label for="BusinessStatus">Member Level</label>
                  <select name="data[Business][member_level]" id="BusinessStatus">
                    <?php foreach($member_biz_level as $k => $v){ ?>
                      <option value="<?php echo $k ?>"><?php echo $v ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>

               <div class="input select">
                  
                  <label for="ModelName" style='font-weight: bold;'>Accept Payment</label>

                  <div class="checkbox">
                      <input name="data[Business][accept_payment][]" value="cash" id="ModelName1" type="checkbox">
                      <label for="ModelName1">
                        <?php echo $this->Html->image('cash.jpg', array(  'alt' => 'Cash',
                                                  'title'=>'Cash',
                                                  'style'=>'width:50px;')); ?>
                      </label>
                  </div>
                  
                  <div class="checkbox">
                      <input name="data[Business][accept_payment][]" value="mastercard" id="ModelName3" type="checkbox">
                      <label for="ModelName3">
                        <?php echo $this->Html->image('mastercard.jpg', array(  'alt' => 'Master Card',
                                                  'title'=>'Master Card',
                                                  'style'=>'width:50px;')); ?>
                      </label>
                  </div>
                  
                  <div class="checkbox" style='clear:both; margin-top:5px;'>
                      <input name="data[Business][accept_payment][]" value="visa" id="ModelName4" type="checkbox">
                      <label for="ModelName4">
                        <?php echo $this->Html->image('visa.jpg', array(  'alt' => 'Visa',
                                                  'title'=>'Visa',
                                                  'style'=>'width:50px;')); ?>
                      </label>
                  </div>                  

              </div>


              <?php 
                // echo $this->Form->input('logo', array('type' => 'file', 'name' => 'logo', 'label' => '<strong>Logo</strong> Size:  225 x 225 pixels (JPG, JPEG, PNG)' ));
               ?>
            </div>

            <div class="span6" style="margin-left:0px;">

              <?php

                echo $this->Form->input(  'status' ,array(
                                'type'    => 'hidden',
                                'label'   => 'Status',
                                'value'   => 1
                            ));
                
              ?>  


              <?php 

                echo $this->Form->input('description', array('placeholder' => 'Business Description',
                                       'type' => 'textarea',
                                       'class' => 'ckeditor',
                                       'maxlength' => '65535'));
               ?>


              <div class="span12" style="margin-top:16px; border:1px solid #CCC; padding:5px; float:right">
                <?php 
                echo $this->Form->input('logo', array( 'required' => true, 'type' => 'file', 'name' => 'logo', 'label' => '<strong>Logo</strong> Size: 225 x 225 pixels (JPG, JPEG, PNG, GIF)' ));
               ?>
              </div>
              
            </div>

            <div class="clreafix"></div>

            <div class="span7" style="margin-left:0px; margin-top:30px; clear:both;">
                <h5 style="margin-left:15px !important;"><i class="icon-fullscreen"></i> Drag pin icon to adjust location or right-click to get main location</h5>

                <!-- <button type="button" id="load-map">Load Map</button> -->

                <div class="span11" style="height:310px; border: 1px solid #CCC; margin-bottom:20px; padding:5px;">
                    
                      <div class="span12" id='map-div' style="margin-left:0px;height:300px; ">
                        
                      </div>
                </div>
            </div>

            <div class="span5" style="margin-left:0px; margin-top:30px;">
                <div class="span12">
                    <h5 style="float:left"><i class="icon-flag-alt"></i> Branches</h5>
                    <a href="#" class="btn btn-linkedin pull-right" id="add-branch"><i class="icon-plus"></i> Add Branch</a>
                </div>
                <table width="100%" style="clear:both;" class="table-list">
                    <tr>
                        <th style="text-align:left">Branch Name</th>
                        <th width="100px;" style="text-align:left">Latitude</th>
                        <th width="100px;" style="text-align:left">Longitude</th>
                        <th width="20px;"></th>
                    </tr>
                    <tr id="row-branch-model" data-key="">
                        <input type="hidden" id="hidden-branch-name" name="data[BusinessBranch][branch_name][]" maxlength="100">
                        <input type="hidden" id="hidden-branch-latitude" name="data[BusinessBranch][latitude][]">
                        <input type="hidden" id="hidden-branch-longitude" name="data[BusinessBranch][longitude][]">
                        <td id="show-branch-name"></td>
                        <td id="show-branch-latitude"></td>
                        <td id="show-branch-longitude"></td>
                        <td style="text-align:center; color:red; font-size:15px;">
                            <i class="icon-trash remove-branch" title="Remove" data-key=""></i>
                        </td>
                    </tr>

                    <tr id="empty-branch">
                        <td colspan="4">
                            <i>Empty branch.</i>
                        </td>
                    </tr>

                </table>

            </div>

            <div class='clearfix'></div>

            <div class="span3">

              <legend><?php echo __('Operation Hours'); ?></legend>

              <div class="input" style="clear:both; padding-top:18px;">
                
                <table id="table-operation-hour">
                  <tr>
                    <td style="padding-top:0px;">Monday</td>
                    <td>
                      <select name="OperationHour[mon][f]" style="width:100px !important;" id="select_from">
                        <?php foreach($operation_hours as $key => $hour ): ?>
                          <option value="<?php echo $key ?>"><?php echo $hour ?></option>
                        <?php endforeach; ?>
                      </select>
                    </td>
                    <td>
                      <select name="OperationHour[mon][t]" style="width:100px !important;" id="select_to">
                        <?php foreach($operation_hours as $key => $hour ): ?>
                          <option value="<?php echo $key ?>"><?php echo $hour ?></option>
                        <?php endforeach; ?>
                      </select>
                    </td>
                  </tr>

                  <tr>
                    <td style="padding-top:0px;">Tuesday</td>
                    <td>
                      <select name="OperationHour[tues][f]" style="width:100px !important;" class="from">
                        <?php foreach($operation_hours as $key => $hour ): ?>
                          <option value="<?php echo $key ?>"><?php echo $hour ?></option>
                        <?php endforeach; ?>
                      </select>
                    </td>
                    <td>
                      <select name="OperationHour[tues][t]" style="width:100px !important;" class="to">
                        <?php foreach($operation_hours as $key => $hour ): ?>
                          <option value="<?php echo $key ?>"><?php echo $hour ?></option>
                        <?php endforeach; ?>
                      </select>
                    </td>
                  </tr>
                  
                  <tr>
                    <td style="padding-top:0px;">Wednesday</td>
                    <td>
                      <select name="OperationHour[wed][f]" style="width:100px !important;" class="from">
                        <?php foreach($operation_hours as $key => $hour ): ?>
                          <option value="<?php echo $key ?>"><?php echo $hour ?></option>
                        <?php endforeach; ?>
                      </select>
                    </td>
                    <td>
                      <select name="OperationHour[wed][t]" style="width:100px !important;" class="to">
                        <?php foreach($operation_hours as $key => $hour ): ?>
                          <option value="<?php echo $key ?>"><?php echo $hour ?></option>
                        <?php endforeach; ?>
                      </select>
                    </td>
                  </tr>
                  
                  <tr>
                    <td style="padding-top:0px;">Thursday</td>
                    <td>
                      <select name="OperationHour[thur][f]" style="width:100px !important;" class="from">
                        <?php foreach($operation_hours as $key => $hour ): ?>
                          <option value="<?php echo $key ?>"><?php echo $hour ?></option>
                        <?php endforeach; ?>
                      </select>
                    </td>
                    <td>
                      <select name="OperationHour[thur][t]" style="width:100px !important;" class="to">
                        <?php foreach($operation_hours as $key => $hour ): ?>
                          <option value="<?php echo $key ?>"><?php echo $hour ?></option>
                        <?php endforeach; ?>
                      </select>
                    </td>
                  </tr>
                  
                  <tr>
                    <td style="padding-top:0px;">Friday</td>
                    <td>
                      <select name="OperationHour[fri][f]" style="width:100px !important;" class="from">
                        <?php foreach($operation_hours as $key => $hour ): ?>
                          <option value="<?php echo $key ?>"><?php echo $hour ?></option>
                        <?php endforeach; ?>
                      </select>
                    </td>
                    <td>
                      <select name="OperationHour[fri][t]" style="width:100px !important;" class="to">
                        <?php foreach($operation_hours as $key => $hour ): ?>
                          <option value="<?php echo $key ?>"><?php echo $hour ?></option>
                        <?php endforeach; ?>
                      </select>
                    </td>
                  </tr>
                  
                  <tr>
                    <td style="padding-top:0px;">Saturday</td>
                    <td>
                      <select name="OperationHour[sat][f]" style="width:100px !important;" class="from">
                        <?php foreach($operation_hours as $key => $hour ): ?>
                          <option value="<?php echo $key ?>"><?php echo $hour ?></option>
                        <?php endforeach; ?>
                      </select>
                    </td>
                    <td>
                      <select name="OperationHour[sat][t]" style="width:100px !important;" class="to">
                        <?php foreach($operation_hours as $key => $hour ): ?>
                          <option value="<?php echo $key ?>"><?php echo $hour ?></option>
                        <?php endforeach; ?>
                      </select>
                    </td>
                  </tr>
                  
                  <tr>
                    <td style="padding-top:0px;">Sunday</td>
                    <td>
                      <select name="OperationHour[sun][f]" style="width:100px !important;" class="from">
                        <?php foreach($operation_hours as $key => $hour ): ?>
                          <option value="<?php echo $key ?>"><?php echo $hour ?></option>
                        <?php endforeach; ?>
                      </select>
                    </td>
                    <td>
                      <select name="OperationHour[sun][t]" style="width:100px !important;" class="to">
                        <?php foreach($operation_hours as $key => $hour ): ?>
                          <option value="<?php echo $key ?>"><?php echo $hour ?></option>
                        <?php endforeach; ?>
                      </select>
                    </td>
                  </tr>
                  
                </table>

              </div>
            </div>

            
            <div class="span8">
              
            <legend><?php echo __('Member Information'); ?></legend>
            
            <div class="span4">

              <div class="input text required">
                <label for="UserFirstName">First Name</label>
                <input  name="User[first_name]" placeholder="First Name" maxlength="50" 
                    type="text" id="UserFirstName" required="required" >
              </div>

              <div class="input text required">
                <label for="UserFirstName">Last Name</label>
                <input  name="User[last_name]" placeholder="Last Name" maxlength="50" 
                    type="text" id="UserFirstName" required="required" >
              </div>

              <?php 

                echo $this->Form->input(  'gender' ,array(
                               'type'     => 'select',
                               'id'     => 'UserGender',
                               'label'    => 'Gender',
                               'name'   => "User[gender]",
                               'maxlength'  => 50,
                               'options'  => $gender
                            ));

                echo $this->Form->input(  'dob', array( 
                              'style'   =>'width:70px;',
                                'id'    => 'UserDob',
                                'dateFormat' => 'DMY', 
                                'label'   => 'Date of Birth',
                                'name'    => "User[dob][]",
                                'type'    => 'date',
                              'minYear'   => date('Y') - 90,
                              'maxlength' => 50,
                              'maxYear'   => date('Y')) );

               ?>

            </div>


            <div class="span4">

              <?php 

                echo $this->Form->input(  'province' ,array(
                               'type'     => 'select',
                               'label'    => 'Province',
                                'id'    => 'UserProvince',
                                'name'    => "User[province_code]",
                               'empty'    => 'Select a province',
                               'required' => true,
                               'options'  => $pro_name
                            ));


                echo $this->Form->input(  'city' ,array(
                               'type'     => 'select',
                               'label'    => 'City',
                                'id'    => 'UserCity',
                                'empty'   => 'select a city' ,
                                'required'  => true,
                                'name'    => "User[city_code]",
                               'options'  => array()
                            ));


                echo $this->Form->input('street', array('placeholder' => 'Street',
                                    'id' => 'UserStreet',
                                    'maxlength' => '100',
                                    'name' => 'User[street]'));


                echo $this->Form->input('position', array('placeholder' => 'Position',
                                    'id' => 'UserPosition',
                                    'maxlength' => '50',
                                    'name' => 'User[position]'));

               ?>

            </div>

            <div class="span3" >

              <?php

                echo $this->Form->input('access_level', array('type' => 'hidden',
                                    'id' => 'UserAccessLevel',
                                    'name' => 'User[access_level]',
                                    'value' => '1' ) );

                echo $this->Form->input('phone', array('placeholder' => 'Phone Number',
                                    'id' => 'UserPhone',
                                    'required' => 'required',
                                    'maxlength' => '20',
                                    'name' => 'User[phone]'));

                echo $this->Form->input('email', array('placeholder' => 'Email', 'type' => 'email',
                                    'id' => 'UserEmail',
                                    'required' => 'required',
                                    'maxlength' => '50',
                                    'name' => 'User[email]'));




                echo $this->Form->input('password', array('placeholder' => 'Password',
                                    'id' => 'UserPassword',
                                    'type' => 'password',
                                    'required' => 'required',
                                    'maxlength' => 30,
                                    'name' => 'User[password]'));

                echo $this->Form->input('confirm_password', 
                            array(  'placeholder' => 'Confirm Password', 
                                'type'=>'password', 
                                'data-equalto' => '#UserPassword',
                                'id' => 'UserConfirmPassword',
                                'name' => 'User[confirm_password]'));


                echo $this->Form->input(  'status' ,array(
                                'type'    => 'hidden',
                                'value'   => '1',
                              'name' => 'User[status]'
                             //    'options'  => $status_arr,
                              // 'id' => 'UserStatus',
                            ));
                
              ?>  

            </div>
            </div>

            
            <div class='clearfix span4' style="padding-top:30px;">

              <?php 
                echo $this->Form->button(__('Save'), array('class' => 'btn btn-primary', 'id' => 'save', 'type' => 'button') ); 
               ?>

            </div>  
            
          </fieldset>
      

      </div>
      <!-- / Pie: Content -->

    </div>
    <!-- / Pie -->
    
  </div>

</div>

<div id="add-branch" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
      style="width:1024px; padding-bottom: 10px !important; left:33% !important">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel"><i class="icon-flag-alt"></i> Add New Branch</h3>
  </div>

  <div class="span12" style="padding-top: 10px; padding-bottom: 20px; width: 985px;">
      <div class="span6" style="margin-left:0px;">
        <label for="branch-name" style="float:left ;margin-right:10px; padding-top:4px; font-weight: bold;">Branch Name : </label>
        <input type="text" class="form-control" style="width:320px;" id="branch-name" placeholder="Branch Name">
      </div>

      <div class="span6">
          <label for="branch-latitude" style="float:left ;margin-right:10px; padding-top:4px; font-weight: bold;">Latitude : </label>
          <input type="text" id="branch-latitude" placeholder="Latitude" style="width:100px; float:left; margin-right:30px;" readonly="readonly">

          <label for="branch-longitude" style="float:left ;margin-right:10px; padding-top:4px; font-weight: bold;">Longitude : </label>
          <input type="text" id="branch-longitude" placeholder="Longitude" style="width:100px; float:left" readonly="readonly">
      </div>

  </div>

  <h5 style="margin-left:20px !important; clear:both"><i class="icon-fullscreen"></i> Drag pin icon to adjust location or right-click to get location
  </h5>

  <div class="span12" style="padding-bottom: 20px; width: 985px; height: 250px; border: 1px solid #CCC" 
        id="add-branch-map">
        
  </div>

  <div class="span12" style="width:1000px;">
    <div class="pull-right" style="padding:20px;">
        <button class="btn btn-linkedin" data-dismiss='modal' type="button" id="btn-add-branch">Select Location</button>
        <button class="btn btn-google cancel" data-dismiss='modal' type="button">Close</button>
    </div>
  </div>

</div> 


<div id="message" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true"
  style="">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel" style="color:red"><i class="icon-exclamation"></i> Warning !</h3>
  </div>

  <div class="span6" style="padding-top: 10px; padding-bottom: 20px;">
    
    <h5 id="message"></h5>

    <button class="btn btn-google cancel" data-dismiss='modal' type="button">Close</button>
  </div>

</div> 
<a href="#message" data-toggle="modal" id="message"></a>


<div id="pass-alert" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true"
  style="">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel" style="color:red"><i class="icon-exclamation"></i> Warning !</h3>
  </div>

  <div class="span6" style="padding-top: 10px; padding-bottom: 20px;">
    
    <h5 id="pass-alert"></h5>

    <button class="btn btn-google cancel" data-dismiss='modal' type="button">Close</button>
  </div>

</div> 
<a href="#pass-alert" data-toggle="modal" id="pass-alert"></a>


<div id="remove-branch-modal" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true"
  style="">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel" style="color:red"><i class="icon-exclamation"></i> Are you sure ?</h3>
  </div>

  <div class="span6" style="padding-top: 10px; ">    
    <h5>Are you sure you want to remove branch?</h5>
  </div>

   <div class="span7 clearfix">
    <div class="pull-right" style="padding:20px;">
        <button class="btn btn-linkedin" data-dismiss='modal' type="button" id="confirm-remove-branch">Yes</button>
        <button class="btn btn-google cancel" data-dismiss='modal' type="button">Close</button>
    </div>
  </div>

</div> 

<style>

  .equalto, .required, .type{
    color: red;
    font-size: 12px !important;
    list-style-type: none;
  }

  .parsley-error-list{
    margin: 0px !important;
    padding: 0px !important;
  }

  .remove-branch:hover{
        cursor: pointer;
    }


 /*.gmnoprint img { max-width: none; }*/

</style>



<script src="<?php echo $this->webroot . "js/admin/markerwithlabel.js" ?>" type="text/javascript"></script>
<script src="https://google-maps-utility-library-v3.googlecode.com/svn/tags/markerwithlabel/1.1.9/src/markerwithlabel.js" type="text/javascript"></script>
<!-- <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places"></script> -->


<script type="text/javascript">

var branch_list = $('table.table-list');

var branch_row_model = $('tr#row-branch-model');
$('tr#row-branch-model').remove();

var branch_row_empty = $('tr#empty-branch');

var listBranchMarker = {};
var map = null;
var pin_icon_blue = '<?php echo $this->webroot ?>' + 'img/pin-blue.png';
var pin_icon = '<?php echo $this->webroot ?>' + 'img/pin-yellow.png';
var iw = null;

function addBranchMarker(index,latLng, brandName) {

    listBranchMarker[index] = new MarkerWithLabel({
            map:map,
            position:latLng,
            icon: pin_icon_blue,
            title: brandName
        });

    var b_name = new google.maps.InfoWindow({ content: brandName });
    google.maps.event.addListener( listBranchMarker[index], "mouseover", function (e) { b_name.open(map, this); });
    google.maps.event.addListener( listBranchMarker[index], "mouseout", function (e) { b_name.close(map, this); });

}

function removeMarker(index){

    var tmpIndex= String(index);        
    var obj = listBranchMarker[tmpIndex];
    obj.setMap(null);
    obj = null;
}

$('div#add-branch').on('shown.bs.modal',function(){
    initializeBranchMap();
});

$('a#add-branch').click( function(event){
    event.preventDefault();
    $('div#add-branch').find('#branch-name').val('');
    $('div#add-branch').modal('show');
});

branch_list.on('click', '.remove-branch', function(){
    var key = $(this).attr('data-key');
    $('div#remove-branch-modal').find('#confirm-remove-branch').attr('data-key', key);
    $('div#remove-branch-modal').modal('show');
})

$('#confirm-remove-branch').click( function(){

    var me = $(this);
    var key_time = me.attr('data-key');

    branch_list.find('tr[data-key="'+ key_time +'"]').remove();
    removeMarker(key_time);

    var count = branch_list.find('tr').length;

    if(count == 1 ){
        branch_list.append(branch_row_empty);
    }
})

$('#btn-add-branch').click( function(){

    var time = new Date().getTime();

    var branch_name = $('input#branch-name').val().trim();
    var lat         = $('input#branch-latitude').val();
    var lng         = $('input#branch-longitude').val();

    if( branch_name == "" ){
        $("h5#pass-alert").text("Please enter Branch Name !");
        $("a#pass-alert").click();
        return false;
    }

    branch_row_empty.remove();

    branch_row_model.attr('id', time);
    branch_row_model.attr('data-key', time );

    // Set Hidden
    branch_row_model.find('input#hidden-branch-name').val(branch_name);
    branch_row_model.find('input#hidden-branch-latitude').val(lat);
    branch_row_model.find('input#hidden-branch-longitude').val(lng);

    // Set showing data
    branch_row_model.find('td#show-branch-name').text(branch_name);
    branch_row_model.find('td#show-branch-latitude').text(lat);
    branch_row_model.find('td#show-branch-longitude').text(lng);
    branch_row_model.find('i.remove-branch').attr('data-key', time);

    branch_list.append(branch_row_model.clone());

    var newLatLng  = new google.maps.LatLng(lat, lng );

    console.log(newLatLng);

    addBranchMarker( time ,newLatLng, branch_name );

})

$('select#select_from').change( function(){
    var val = $(this).val();

    $('table#table-operation-hour').find('.from').val(val);

});

$('select#select_to').change( function(){
    var val = $(this).val();

    $('table#table-operation-hour').find('.to').val(val);

});


$("#UserConfirmPassword").blur( function (){

  return $(this).parsley( 'validate');

});

$("input#BusinessPhone1, input#BusinessPhone2, input#UserPhone").keydown(function (e) {
    // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
         // Allow: Ctrl+A
        (e.keyCode == 65 && e.ctrlKey === true) || 
         // Allow: home, end, left, right
        (e.keyCode >= 35 && e.keyCode <= 39)) {
             // let it happen, don't do anything
             return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }

});

$("#save").click( function(){

  if(validateEmail()){
    
    var pass = $("#UserPassword").val().length;

    if( pass > 1 && pass < 6 ){
      $("h5#pass-alert").text("Password must be at least 6 characters.");
      $("a#pass-alert").click();
      return false;
    }

    $("#BusinessAddForm").submit();
  }else{
    $("h5#message").text("This email is already registered. Please try another email.");
    $("a#message").click();
    return false; 
  }
  return false;

})


$("#BusinessCity").change( function (){
  var me = $(this);

  var locationSelect  = $("#BusinessLocation");
  var SangkatSelect  = $("#BusinessSangkatId");

  var city_code = me.val();

  var url = '<?php echo $this->webroot ?>' + 'administrator/locations/get_location_by_city_code/' + city_code ;

  if( city_code != '' ){
    $.ajax({
            type: 'POST',
            url: url,
            data: { city_code : city_code },
            dataType: 'json',
            
            beforeSend: function(){
                locationSelect.html('<option>Loading...</option>');
                SangkatSelect.html('<option>Loading...</option>');
            },
            success: function (result){
                var data = result.data;

                locationSelect.html("<option value=''>select a location</option>");
                SangkatSelect.html("<option value=''>select an option</option>");

                $.each( data, function (ind, val ){
                  var loc = val.Location;

                  locationSelect.append("<option value='" + loc.location_name + "' data-id='"+loc.id+"'>" + loc.location_name + "</option>" );

                });

            },

            error: function(xhr, status, error) {
              var err = eval("(" + xhr.responseText + ")");
                console.log(err);
            }

        });

  }else{
    locationSelect.html("<option value=''>select an option</option>");
    SangkatSelect.html("<option value=''>select an option</option>");
  }

});


$("#BusinessLocation").change( function (){
  var me = $(this);

  var SangkatSelect  = $("#BusinessSangkatId");

  var optoin_selected = $('#BusinessLocation option:selected');
  var location_id =   optoin_selected.attr('data-id');

  $('input#hidden-location-id').val(location_id);

  var url = '<?php echo $this->webroot ?>' + 'administrator/sangkats/getSangkatByLocation/' + location_id ;

  if( location_id != '' ){
    $.ajax({
            type: 'POST',
            url: url,
            data: { location_id : location_id },
            dataType: 'json',
            
            beforeSend: function(){
                SangkatSelect.html('<option>Loading...</option>');
            },
            success: function (result){
                var data = result.data;

                // console.log(data);

                SangkatSelect.html("<option value=''>select an option</option>");

                $.each( data, function (ind, val ){
                  var sk = val.Sangkat;
                  SangkatSelect.append("<option value='" + sk.id + "'>" + sk.name + "</option>" );

                });

            },

            error: function(xhr, status, error) {
              var err = eval("(" + xhr.responseText + ")");
                console.log(err);
            }

        });

  }else{
    SangkatSelect.html("<option value=''>select an option</option>");
  }

});

$("#UserProvince").change( function (){
  var me = $(this);

  var citySelect    = $("#UserCity");

  var pro_code = me.val();

  var url = '<?php echo $this->webroot ?>' + 'administrator/users/get_city_by_province/' + pro_code ;

  if( pro_code != '' ){
    $.ajax({
            type: 'POST',
            url: url,
            data: { pro_code : pro_code },
            dataType: 'json',
            
            beforeSend: function(){
                citySelect.html("<option value=''>select a location</option>");
                // console.log("Loading...");
            },
            success: function (data){
                // console.log(data);

                var data = data.data;

                citySelect.html("<option value=''>select a city</option>")
        
                $.each( data, function (ind, val ){
                  var city = val.City;

                  citySelect.append("<option value='" + city.city_code + "'>" + city.city_code + " - " + city.city_name + "</option>" )
                });

            },

            error: function ( err ){
              console.log(err);
            }

        });

  }else{

    citySelect.html("<option value=''>select an option</option>");
    locationSelect.html("<option value=''>select an option</option>");

  }

});

$(document).ready(function(){

    initialize();
    // initializeBranchMap();

});

// Validate Email for user
function validateEmail(){

  var email = $("#UserEmail").val();
  var id = null;
  var result = true;

  var url = '<?php echo $this->webroot ?>' + 'administrator/Users/checkDuplicateEmail/' + email + "/" + id ;

  $.ajax({

      type: 'POST',
      url: url,
      data: { email : email, id : id },
      dataType: 'json',
      async: false,
      
      success: function (data){
          console.log(data);

          if( data.status == 1 ){
            result = true;
          }else{
            result = false;
          }
      },

      error: function ( err ){
        console.log(err);
      }

  });

  return result;

}

function initialize() {

    var CurrentLat = '11.5463';
    var CurrentLng = '104.8933';

    if(navigator.geolocation) {
        navigator.geolocation.getCurrentPosition (
            // successFunction
            function(position) {
                CurrentLat = position.coords.latitude;
                CurrentLng = position.coords.longitude;
            }
        );
    }

    var latInput = $("#BusinessLatitude");
    var lngInput = $("#BusinessLongitude");
    
    var fixLatitude = CurrentLat;
    var fixLongtitude = CurrentLng;

    latInput.val(parseFloat(CurrentLat).toFixed(4));
    lngInput.val(parseFloat(CurrentLng).toFixed(4));
    
    var myNewLatlng  = new google.maps.LatLng( fixLatitude, fixLongtitude );

    var mapOptions = {
                      zoom: 14,
                      center: myNewLatlng,
                      mapTypeId: google.maps.MapTypeId.ROADMAP
                  };

    map = new google.maps.Map(document.getElementById('map-div'), mapOptions);

    var BranchMarker = new MarkerWithLabel({
              map: map,
              position: myNewLatlng,
              icon : pin_icon,
              labelAnchor : new google.maps.Point( 5, 36 ),   
              labelClass : "pin-label",
              draggable: true
            });

    google.maps.event.addListener(BranchMarker, "drag", function(BranchMarker){
        var latLng = BranchMarker.latLng;
        latInput.val(parseFloat(latLng.lat()).toFixed(4));
        lngInput.val(parseFloat(latLng.lng()).toFixed(4));
    });

    google.maps.event.addListener(BranchMarker, "dragend", function(event){

      var latLng = event.latLng;

      latInput.val(parseFloat(latLng.lat()).toFixed(4));
      lngInput.val(parseFloat(latLng.lng()).toFixed(4));

    });

    //##### drop a new marker on right click ######
    google.maps.event.addListener(map, 'rightclick', function(event) {

        var latLng = event.latLng;

        latInput.val(parseFloat(latLng.lat()).toFixed(4));
        lngInput.val(parseFloat(latLng.lng()).toFixed(4));

        var newLatLng  = new google.maps.LatLng( latLng.lat(), latLng.lng() );

        BranchMarker.setPosition(newLatLng);

    });

}

// google.maps.event.addDomListener(window, 'load', initialize);

function initializeBranchMap() {

    var CurrentLat = 11.5463;
    var CurrentLng = 104.8933;

    if(navigator.geolocation) {
        navigator.geolocation.getCurrentPosition (
            // successFunction
            function(position) {
                CurrentLat = position.coords.latitude;
                CurrentLng = position.coords.longitude;
            }
        );
    }

    var latInput = $("#branch-latitude");
    var lngInput = $("#branch-longitude");
    
    var fixLatitude = CurrentLat;
    var fixLongtitude = CurrentLng;

    latInput.val(parseFloat(CurrentLat).toFixed(4));
    lngInput.val(parseFloat(CurrentLng).toFixed(4));
    var myNewLatlng  = new google.maps.LatLng( fixLatitude, fixLongtitude );

    var mapOptions = {
                        zoom: 14,
                        center: myNewLatlng,
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    };

    var branch_map = new google.maps.Map(document.getElementById('add-branch-map'), mapOptions);

    var BranchMarker = new MarkerWithLabel({
              map: branch_map,
              position: myNewLatlng,
              icon : pin_icon_blue,
              labelAnchor : new google.maps.Point( 5, 36 ),   
              labelClass : "pin-label",
              draggable: true
            });

    google.maps.event.addListener(BranchMarker, "drag", function(BranchMarker){

      var latLng = BranchMarker.latLng;

      latInput.val(parseFloat(latLng.lat()).toFixed(4));
      lngInput.val(parseFloat(latLng.lng()).toFixed(4));

    });

    google.maps.event.addListener(BranchMarker, "dragend", function(event){

      var latLng = event.latLng;

      latInput.val(parseFloat(latLng.lat()).toFixed(4));
      lngInput.val(parseFloat(latLng.lng()).toFixed(4));

    });

    //##### drop a new marker on right click ######
    google.maps.event.addListener(branch_map, 'rightclick', function(event) {

        var latLng = event.latLng;

        latInput.val(parseFloat(latLng.lat()).toFixed(4));
        lngInput.val(parseFloat(latLng.lng()).toFixed(4));

        var newLatLng  = new google.maps.LatLng( latLng.lat(), latLng.lng() );

        BranchMarker.setPosition(newLatLng);

    });

}

</script>