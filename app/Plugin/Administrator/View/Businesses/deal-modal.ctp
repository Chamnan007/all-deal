<!--Deal Images Modal -->
<div id="deal_images" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true"
  style="width:800px !important; left:40% !important; min-height:300px; max-height: 500px;">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel"><i class="icon-picture"></i> Deal Images</h3>
  </div>
    
    <div class="clearfix"></div>
    <span id="error-msg" style="margin-left:20px; font-size:16px; color:red; display: none">
        
    </span>

  <div class="span9" style="margin-top:20px; 
                            width:775px; 
                            position:relative;
                            max-height: 380px; min-height:180px;
                            overflow-y:scroll" id='deal-image-container'>
        
      <div  class='deal-image' >
            
      </div>

      <div style="width:32px; height:32px; margin-left:48%; margin-top:8%; position:absolute;" id="loading-img">
            <img src="<?php echo $this->webroot . 'img/ajax-loader.gif' ?>" alt="">
      </div>

      <div  class='deal-image-upload' style="text-align:center; display:none;">
            <div class="upload" title="Upload Deal Image">
                <i class='icon-plus'></i>
                <img src="<?php echo $this->webroot . 'img/ajax-loader.gif' ?>" alt="" style="display:none;">
            </div>
            Size: 705 x 422 pixels <br>Allow File Types: (JPG, JPEG, PNG) 
      </div>

  </div>

  <div style="clear:both;margin-bottom:20px; margin-left:20px;">
      <button class="btn btn-google cancel" data-dismiss='modal' type="button">Close</button>
  </div>

</div>

<!-- Deal Detail Form -->
<div id="detail-deal-modal" class="modal hide fade" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true"
  style="padding-bottom: 10px !important; width:1024px !important; left:30% !important">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel"><i class="icon-search"></i><font>Deal Detail</font></h3>
  </div>
  
  <div style="max-height:500px; overflow-x: scroll;">
  <form action="<?php echo $this->Html->url( array('controller' => 'Deals', 'action' => 'approve')); ?>" 
      style="padding: 20px; padding-top:10px; padding-bottom: 0px;" method="POST" id="dealDetailForm"
      enctype = "multipart/form-data" >
  <div style="width:100%; min-height: 300px;">
    
    <div class="content">
      <div class="span4">
        <table width="100%;">

          <tr>
            <th width="100px;" style="text-align:left; vertical-align:top;">Deal Title</th>
            <th width="20px;" style="vertical-align:top;">:</th>
            <td id="deal-title" style="font-weight:bold; font-size:15px; color: blue; vertical-align:top;"></td>
          </tr>

          <tr>
            <th style="text-align:left; vertical-align:top;">Valid From</th>
            <th style="vertical-align:top;">:</th>
            <td id="deal-valid-from" style="vertical-align:top;"></td>
          </tr>

          <tr>
            <th style="text-align:left; vertical-align:top;">Valid To</th>
            <th style="vertical-align:top;">:</th>
            <td id="deal-valid-to" style="vertical-align:top;"></td>
          </tr>

          <tr>
            <th style="text-align:left; vertical-align:top;">Categories</th>
            <th style="vertical-align:top;">:</th>
            <td id="deal-goods-group" style="vertical-align:top;"></td>
          </tr>

          <tr>
            <td colspan="3" style="padding-top:20px;"></td>
          </tr>

          <tr>
            <th  style="text-align:left">Posted Date</th>
            <th>:</th>
            <td id="deal-posted-date"></td>
          </tr>

          <tr>
            <th style="text-align:left; vertical-align:top;">Status</th>
            <th style="vertical-align:top;">:</th>
            <td id="deal-status" style="vertical-align:top;"></td>
          </tr>

        </table>
      </div>
      <div class="span8">
        <div class="span3" style="width:290px;">
          <table width="100%;">
            <tr>
              <th width="100px;" style="text-align:left; vertical-align:top;">Description:</th>
            </tr>

            <tr>
              <td id="deal-description">
                <div style="min-height:250px; max-height:300px; overflow-y:scroll;">
                  
                </div>
              </td>
            </tr>

          </table>
        </div>

        <div class="span3" style="width:290px;">
          <table width="100%;">
            <tr>
              <th width="100px;" style="text-align:left; vertical-align:top;">Conditions:</th>
            </tr>

            <tr>
              <td id="deal-conditions">
                <div style="min-height:250px; max-height:300px; overflow-y:scroll;">
                  
                </div>
              </td>
            </tr>

          </table>
        </div>    

      </div>

      <div class="span11" style="margin-left:0px; width:100%;">
        <legend style="font-size:17px; font-weight:bold;">Related Items</legend>
        <table class="table-list" id="table-item-listing" >
                <thead>
                  <th width="80px;">Image</th>
                    <th width="200px;"><?php echo __('Menu Category'); ?></th>
                    <th><?php echo __('Item Name'); ?></th>
                    <th style="text-align:right" width="100px;"><?php echo __('Original Price'); ?></th>
                    <th style="text-align:right" width="100px;"><?php echo __('Discounted Price'); ?></th>
                    <th style="text-align:right" width="100px;"><?php echo __('Available Qty'); ?></th>
                </thead>    

                <tbody>
                      <tr class="detail-related-item-row">
                        <td class="item-image"></td>
                        <td class='item-menu' style="font-weight: bold;"></td>
                        <td class='item-title'></td>
                        <td style="text-align:right" class='item-price'></td>
                        <td style="text-align:right" class='item-discount-price'></td>
                        <td style="text-align:right" class='item-available-qty'></td>
                    </tr>
                    </tbody>

                </table>

      </div>

    </div>

    <div style="width:32px; height:32px; margin-left:48%; margin-top:8%; position:absolute;" id="loading-img">
            <img src="<?php echo $this->webroot . 'img/ajax-loader.gif' ?>" alt="">
        </div>

  </div>

    <div class="clearfix"></div>

    <div class="submit" style="margin-top: 20px; clear: both; margin-left:20px;">
      <button class="btn btn-google cancel" data-dismiss='modal' type="button">Close</button>
      
    </div>
      
  </form>

  </div>

</div>

<!-- Deactivate Deal -->
<div id="deal_delete" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel"><i class="icon-question"></i> Are You Sure ?</h3>
  </div>

  <form   action="#" 
      id="dealDeleteForm"
        style="padding: 20px;" method="POST" >

    <div>
      <h5>Are you sure you want to delete this deal?</h5>
    </div>

    <div class="submit" style="margin-top: 20px; clear: both; width:100%">
      <input class="btn btn-primary" type="submit" value="Yes">
      <button class="btn btn-google cancel" data-dismiss='modal' type="button">No</button>
    </div>
      
  </form>

</div>

<form action="<?php echo $this->Html->url(array('action' => 'uploadDealImageAjax__', $business['Business']['id'])); ?>"
        method="POST"  enctype="multipart/form-data"
        id="frm-upload-deal-image">
    <input type="file" name='upload_deal_image' id="upload_deal_image" style="display:none;">
</form>

<!-- Deal Add Form -->
<div id="deal_add" class="modal hide fade" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true"
  style="padding-bottom: 10px !important; width:1024px !important; left:30% !important">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel"><i class="icon-plus"></i> Create New Deal</h3>
  </div>
  
  <div style="max-height:550px; overflow-x: scroll;">
  <form action="<?php echo $this->Html->url(array('action' => 'addDeal', $business['Business']['id'])); ?>" 
      style="padding: 20px; padding-top:10px;" method="POST" id="dealAddForm"
      enctype = "multipart/form-data" >

     <div class="span3">

        <?php

          echo $this->Form->input('title', array(
                          'placeholder' => 'Deal Title',
                          'label' => 'Deal Title',
                          'required'=>'required',
                          'type' => 'textarea',
                           'name' => 'data[Deal][title]',
                           'id' => 'DealTitle',
                          'maxLength' => 255));   


        ?>  

        <div id="dealValidFromPicker" class="input-append date">
          <label for="">Valid From</label>
            <input data-format="dd-MM-yyyy hh:mm:ss" type="text" 
                name="data[Deal][valid_from]" id="DealValidFrom"
                style="width: 165px !important"
                placeholder="valid From" required='required'>
            <span class="add-on">
              <i data-time-icon="icon-time" data-date-icon="icon-calendar">
              </i>
            </span>
        </div>

        <div id="dealValidToPicker" class="input-append date">
          <label for="">Valid To</label>
            <input data-format="dd-MM-yyyy hh:mm:ss" type="text" 
                name="data[Deal][valid_to]" id="DealValidTo"
                style="width: 165px !important"
                placeholder="valid To" required='required'>
            <span class="add-on">
              <i data-time-icon="icon-time" data-date-icon="icon-calendar">
              </i>
            </span>
        </div>


      <div style="width:200px; 
            max-height:75px; 
            height: 75px; 
            display:none;
            border: 1px solid #CCC; 
            padding:10px; 
            overflow-y: auto;
            margin-bottom:5px;" 
          id="DealDiscountCategory">

          <div class="input checkbox" style="width:85%; padding-left:0px;">
            <input  style="float:left; margin-right: 6px;" type="radio" checked="checked" name="data[Deal][discount_category]" id='none' value="">
            <label for="none">None</label>
           </div>

        <?php foreach( $discount_groups as $key => $value ){ ?>

             <div class="input checkbox" style="width:85%; padding-left:0px;">
              <input  style="float:left; margin-right: 6px;" type="radio" name="data[Deal][discount_category]" id='adg<?php echo $value ?>' value="<?php echo $value ?>">
              <label for="adg<?php echo $value ?>"><?php echo $value ?></label>
             </div>

        <?php } ?>
      </div>

      <label for=""><strong>Category</strong></label>
      <div style="width:200px; 
            max-height:173px; 
            height: 173px; 
            border: 1px solid #CCC; 
            padding:10px; 
            overflow-y: auto;
            margin-bottom:5px;" 
          id="DealGoodsCategoryAdd">

        <?php foreach( $goods_category as $key => $value ){ ?>

             <div class="input checkbox" style="width:85%;">
              <input type="checkbox" name="data[Deal][goods_category][]" id='agc<?php echo $value ?>' value="<?php echo $key ?>">
              <label for="agc<?php echo $value ?>"><?php echo $value ?></label>
             </div>

        <?php } ?>
      </div>
    

    </div>

    <div class="span9">
      

        <div class="span4" style="maring-left:0px; width:330px;">

          <?php 

            echo $this->Form->input('description', array('placeholder' => 'Deal Description',
                                   'type' => 'textarea',
                                   'class' => 'ckeditor',
                                   'name' => 'data[Deal][description]',
                                   'id' => 'DealDescriptionAdd',
                                   'maxlength' => '65535'));
           ?>

           <div class="clearfix" style="margin-top:5px;"></div>
           <label class="pull-left" style="margin-top:5px;"><strong>Branch</strong></label>
           <select id="merchant-branch" class="pull-left" style="margin-left:10px; width:277px" name="data[Deal][branch_id]">
                <option value="0" data-lat="<?php echo $lat ?>" data-lng="<?php echo $lng ?>">Any branch</option>
                <?php foreach( $data_braches as $k => $val ){ ?>
                  <option value="<?php echo $val['id'] ?>"
                          data-lat="<?php echo $val['latitude'] ?>"
                          data-lng="<?php echo $val['longitude'] ?>"><?php echo $val['branch_name'] ?></option>
                <?php } ?>
           </select>

          <input type="hidden" value="<?php echo $lat ?>" name="data[Deal][latitude]"   id="latitude" />
          <input type="hidden" value="<?php echo $lng ?>" name="data[Deal][longitude]"  id="longitude" />
          
        </div>

      <div class="span4" style="width:330px;">


          <?php 

            echo $this->Form->input('deal_condition', array('placeholder' => 'Deal Conditions',
                                   'type' => 'textarea',
                                   'class' => 'ckeditor',
                                   'name' => 'data[Deal][deal_condition]',
                                   'id' => 'DealDescriptionAdd',
                                   'maxlength' => '65535'));
           ?> 


        <div style="display:block; clear:both; margin-top:25px;">
          <label for='add-last-minute' style="float:left; width:100px; padding-top:3px; font-weight: bold;">Last Minute Deal</label>
          <input id="add-last-minute" type="checkbox" value="1" name="data[Deal][is_last_minute_deal]" style="flaot:left; margin-left:5px;"/>
        </div>

        
      </div> 

      <!-- <div class="clearfix"></div> -->


        <div class="input checkbox" style="width:90%; padding-top:20px; float: left; margin-left:0px; ">
          <strong>Deal Images (Max 5 Images) - Size: 705 x 422 pixels (JPG, JPEG, PNG)</strong>
          <br><input type="file" name='deal_image[]' id="deal_image_add" required="required" multiple>
        </div>

        <div class="deal-image-preview">
             <img src="#">
         </div>

         <div id="deal-image-div" style="clear:both;">
             
         </div>

    </div>

    <div class="clearfix"></div>
    <legend style="font-size:16px;"><h5>Related Items</h5></legend>

    <a href="#" class="btn btn-linkedin" id="add-related-item-btn"><i class="icon-plus" style="color:white"></i> Add Related Item</a>

    <div class="span11" style="margin-left:0px; padding-top:20px; width:950px;" id="div-related-item">

        <table class="table-list" id="table-item-listing" >
            <thead>
                <th width="200px;"><?php echo __('Menu Category'); ?></th>
                <th><?php echo __('Item Name'); ?></th>
                <th width="150px;"><?php echo __('Original Price'); ?></th>
                <th width="150px;"><?php echo __('Discounted Price'); ?></th>
                <th width="150px;"><?php echo __('Available Qty'); ?></th>
                <th width="60px;"><?php echo __('Action'); ?></th>
            </thead>    

            <tbody>
                
                <tr class="related-item-row">
                    <input type="hidden" name="DealItemLink[item_id][]" class='hidden-item-id' >
                    <input type="hidden" name="DealItemLink[original_price][]" class='hidden-item-original-price' >
                    <input type="hidden" name="DealItemLink[discount][]" class='hidden-item-discount' >
                    <input type="hidden" name="DealItemLink[available_qty][]" class='hidden-item-qty' >
                    <td class='item-menu' style="font-weight: bold;"></td>
                    <td class='item-title'></td>
                    <td class='item-price'></td>
                    <td class='item-discount-price'>
                        <span></span>
                        <input style="width:100px; text-align:center; display:none;" 
                                 onkeypress="return numericAndDotOnly(this)"
                                value="" />
                    </td>
                    <td class='item-available-qty' style="text-align:right">
                        <span></span>
                        <input style="width:100px; text-align:center; display:none;" 
                                 onkeypress="return numericAndDotOnly(this)"
                                value="" />
                    </td>
                    <td class='item-action' style="text-align:center">

                        <a  href="#" class='btn btn-success save-change' 
                            style="display:none;"
                            data-price="" title="Save Change">
                            <i class='icon-save' style='font-size:12px; padding-right:0px; color:white;'></i></a>

                        <a  href="#" class='btn btn-linkedin edit-selected-item' 
                            title="Edit">
                            <i class='icon-edit' style='font-size:12px; padding-right:0px; color:white;'></i></a>

                        <a href="#" class='btn btn-google remove-selected-item'>
                            <i class='icon-trash' style='font-size:12px; padding-right:0px; color:white'></i></a>
                    </td>
                </tr>

                <tr id="row-empty-item">
                  <td colspan="10"><i>Empty Items !</i></td>
                </tr>
            </tbody>      
        </table>

    </div>

    <div class="clearfix"></div>

    <div class="submit" style="margin-top: 20px; clear: both; margin-left:20px;">
      <button class="btn btn-google cancel" data-dismiss='modal' type="button" style="float:left">Cancel</button>
      <input class="btn btn-primary" type="submit" value="Save" id="add-deal-submit" style="float:left"> 

      <div id="loading-add" class="pull-left" style="display:none;">
          <img src="<?php echo $this->webroot . 'img/ajax-loader.gif' ?>" alt="" style="width:30px;">
          <span style="font-size:15px;">saving...</span>
      </div>
    </div>
      
  </form>
  </div>

</div> 

<!-- Deal Edit Form -->
<div id="deal_edit" class="modal hide fade"
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true"
  style=" width:1020px !important; left:30% !important; margin-top:0 !important;">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel"><i class="icon-edit"></i> Edit Deal Information</h3>
  </div>

  <div style="max-height:550px; overflow-x: scroll;">

    <form action="<?php echo $this->Html->url(array('action' => 'editDeal', $business['Business']['id'])); ?>" 
      style="padding: 20px; padding-top:10px;" method="POST" id="dealEditForm"
      enctype = "multipart/form-data" >

     <div class="span3">
        <?php

          echo $this->Form->input('title', array(
                          'placeholder' => 'Deal Title',
                          'label' => 'Deal Title',
                          'required'=>'required',
                          'type' => 'textarea',
                           'name' => 'data[Deal][title]',
                           'id' => 'DealTitle',
                          'maxLength' => 255));   


        ?>  

        <div id="datetimepicker1" class="input-append date">
          <label for="">Valid From</label>
            <input data-format="dd-MM-yyyy hh:mm:ss" type="text" 
                name="data[Deal][valid_from]" id="DealValidFrom"
                style="width: 165px !important"
                placeholder="valid From" required='required'>
            <span class="add-on">
              <i data-time-icon="icon-time" data-date-icon="icon-calendar">
              </i>
            </span>
        </div>

        <div id="datetimepicker2" class="input-append date">
          <label for="">Valid To</label>
            <input data-format="dd-MM-yyyy hh:mm:ss" type="text" 
                name="data[Deal][valid_to]" id="DealValidTo"
                style="width: 165px !important"
                placeholder="valid To" required='required'>
            <span class="add-on">
              <i data-time-icon="icon-time" data-date-icon="icon-calendar">
              </i>
            </span>
        </div>

      <label for=""><strong>Category</strong></label>
      <div style="width:200px; 
            max-height:173px; 
            height: 173px; 
            border: 1px solid #CCC; 
            padding:10px; 
            overflow-y: auto;
            margin-bottom:5px;" 
          id="DealGoodsCategoryEdit" >

        <?php foreach( $goods_category as $key => $value ){ ?>

             <div class="input checkbox" style="width:85%;">
              <input type="checkbox" name="data[Deal][goods_category][]" id='gc<?php echo $value ?>' value="<?php echo $key ?>">
              <label for="gc<?php echo $value ?>"><?php echo $value ?></label>
             </div>

        <?php } ?>
      </div>
      

      </div>

    <div class="span9">

        <div class="span4" style="maring-left:0px; width:330px;">

          <?php 

            echo $this->Form->input('description', array('placeholder' => 'Deals Description',
                                   'type' => 'textarea',
                                   'class' => 'ckeditor',
                                   'name' => 'data[Deal][description]',
                                   'id' => 'DealDescription',
                                   'maxlength' => '65535'));
           ?>

           <div class="clearfix" style="margin-top:5px;"></div>
           <label class="pull-left" style="margin-top:5px;"><strong>Branch</strong></label>
           <select id="merchant-branch" class="pull-left" style="margin-left:10px; width:277px"  name="data[Deal][branch_id]">
                <option value="0" data-lat="<?php echo $lat ?>" data-lng="<?php echo $lng ?>">Any branch</option>
                <?php foreach( $data_braches as $k => $val ){ ?>
                  <option value="<?php echo $val['id'] ?>"
                          data-lat="<?php echo $val['latitude'] ?>"
                          data-lng="<?php echo $val['longitude'] ?>"><?php echo $val['branch_name'] ?></option>
                <?php } ?>
           </select>

          <input type="hidden" value="<?php echo $lat ?>" name="data[Deal][latitude]"   id="latitude" />
          <input type="hidden" value="<?php echo $lng ?>" name="data[Deal][longitude]"  id="longitude" />


          <div style="display:block; clear:both; margin-top:25px;">
            <label for='last-minute' style="float:left; width:100px; padding-top:3px; font-weight: bold;">Last Minute Deal</label>
            <input id="last-minute" type="checkbox" value="1" name="data[Deal][is_last_minute_deal]" style="flaot:left; margin-left:5px;"/>
          </div>
          
        </div>

      <div class="span4" style="width:330px;">


          <?php 

            echo $this->Form->input('deal_condition', array('placeholder' => 'Deal Conditions',
                                   'type' => 'textarea',
                                   'class' => 'ckeditor',
                                   'name' => 'data[Deal][deal_condition]',
                                   'id' => 'DealConditions',
                                   'maxlength' => '65535'));
           ?>

          <div class="clearfix" style="margin-top:5px;"></div>
          <label for="" style="float:left; margin-top:5px; font-weight:bold;">Status</label>          
          <select name="data[Deal][status]" id="deal-status" style="float:left; margin-left:10px;">
              <?php foreach($deal_status as $key => $val ){ ?>
                  <option value="<?php echo $key ?>"><?php echo $val['status'] ?></option>
              <?php } ?>
          </select>
        
      </div> 

    </div>

    <div class="span6" >

      <div class="span6" style="margin-top:18px; text-align:left;">

         <input type="hidden" id="old_image_path" name="old_image_path" >

      </div>
      
    </div>

    <div class="clearfix"></div>
    <legend style="font-size:16px;"><h5>Related Items</h5></legend>

    <a href="#" class="btn btn-linkedin" id="add-related-item-edit-btn"><i class="icon-plus" style="color:white"></i> Add Related Item</a>

    <div class="span11" style="margin-left:0px; padding-top:20px; width:950px;" id="div-related-item">

        <table class="table-list" id="table-item-listing-edit" >
            <thead>
                <th width="200px;"><?php echo __('Menu Category'); ?></th>
                <th><?php echo __('Item Name'); ?></th>
                <th width="150px;"><?php echo __('Original Price'); ?></th>
                <th width="150px;"><?php echo __('Discounted Price'); ?></th>
                <th width="150px;"><?php echo __('Available Qty'); ?></th>
                <th width="60px;"><?php echo __('Action'); ?></th>
            </thead>    

            <tbody>
                
                <tr class="related-item-row-edit">

                    <input type="hidden" name="DealItemLink[item_id][]" class='hidden-item-id' >
                    <input type="hidden" name="DealItemLink[original_price][]" class='hidden-item-original-price' >
                    <input type="hidden" name="DealItemLink[discount][]" class='hidden-item-discount' >
                    <input type="hidden" name="DealItemLink[available_qty][]" class='hidden-item-qty' >
                    
                    <td class='item-menu' style="font-weight: bold;"></td>
                    <td class='item-title'></td>
                    <td class='item-price'></td>
                    <td class='item-discount-price'>
                        <span></span>
                        <input style="width:100px; text-align:center; display:none;" 
                                 onkeypress="return numericAndDotOnly(this)"
                                value="" />
                    </td>
                    <td class='item-available-qty' style="text-align:right">
                        <span></span>
                        <input style="width:100px; text-align:center; display:none;" 
                                 onkeypress="return numericAndDotOnly(this)"
                                value="" />
                    </td>
                    <td class='item-action' style="text-align:center">

                        <a  href="#" class='btn btn-success save-change' 
                            style="display:none;"
                            data-price="" title="Save Change">
                            <i class='icon-save' style='font-size:12px; padding-right:0px; color:white;'></i></a>

                        <a  href="#" class='btn btn-linkedin edit-selected-item' 
                            title="Edit">
                            <i class='icon-edit' style='font-size:12px; padding-right:0px; color:white;'></i></a>

                        <a href="#" class='btn btn-google remove-selected-item'>
                            <i class='icon-trash' style='font-size:12px; padding-right:0px; color:white'></i></a>
                    </td>
                </tr>

                <tr id="row-empty-item-edit">
                  <td colspan="10"><i>Empty Items !</i></td>
                </tr>
            </tbody>      
        </table>

    </div>

    <div class="clearfix"></div>

    <div class="submit" style="margin-top: 20px; clear: both; margin-left:20px;">
      <button class="btn btn-google cancel" data-dismiss='modal' type="button" style="float:left;">Cancel</button>
      <input class="btn btn-primary" type="submit" value="Save Change" id="save-deal-edit" style="float:left">

      <div id="loading-edit" class="pull-left" style="display:none;">
          <img src="<?php echo $this->webroot . 'img/ajax-loader.gif' ?>" alt="" style="width:30px;">
          <span style="font-size:15px;">saving...</span>
      </div>

    </div>
      
  </form>
  </div>

</div> 


<!-- Deal Repost Form -->
<div id="deal_repost" class="modal hide fade"
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true"
  style=" width:1020px !important; left:30% !important; margin-top:0 !important;">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel"><i class="icon-edit"></i> Repost Deal</h3>
  </div>

  <div style="max-height:550px; overflow-x: scroll;">

    <form action="#" 
      style="padding: 20px; padding-top:10px;" method="POST" id="dealReportForm"
      enctype = "multipart/form-data" >

     <div class="span3">
        <?php
          
          echo $this->Form->input('title', array(
                          'placeholder' => 'Deal Title',
                          'label' => 'Deal Title',
                          'required'=>'required',
                          'type' => 'textarea',
                           'name' => 'data[Deal][title]',
                           'id' => 'DealTitle',
                          'maxLength' => 255));


        ?>

        <div id="datetimepicker1-repost" class="input-append date">
          <label for="">Valid From</label>
            <input data-format="dd-MM-yyyy hh:mm:ss" type="text" 
                name="data[Deal][valid_from]" id="DealValidFrom"
                style="width: 165px !important"
                placeholder="valid From" required='required'>
            <span class="add-on">
              <i data-time-icon="icon-time" data-date-icon="icon-calendar">
              </i>
            </span>
        </div>

        <div id="datetimepicker2-repost" class="input-append date">
          <label for="">Valid To</label>
            <input data-format="dd-MM-yyyy hh:mm:ss" type="text" 
                name="data[Deal][valid_to]" id="DealValidTo"
                style="width: 165px !important"
                placeholder="valid To" required='required'>
            <span class="add-on">
              <i data-time-icon="icon-time" data-date-icon="icon-calendar">
              </i>
            </span>
        </div>

      <label for=""><strong>Category</strong></label>
      <div style="width:200px; 
            max-height:173px;
            height: 173px;
            border: 1px solid #CCC;
            padding:10px;
            overflow-y: auto;
            margin-bottom:5px;"
          id="DealGoodsCategoryRepost" >

        <?php foreach( $goods_category as $key => $value ){ ?>

             <div class="input checkbox" style="width:85%;">
              <input type="checkbox" name="data[Deal][goods_category][]" id='gc<?php echo $value ?>' value="<?php echo $key ?>">
              <label for="gc<?php echo $value ?>"><?php echo $value ?></label>
             </div>

        <?php } ?>
      </div>
      

    </div>

    <div class="span9">

        <div class="span4" style="maring-left:0px; width:330px;">

          <?php 

            echo $this->Form->input('description', array('placeholder' => 'Deals Description',
                                   'type' => 'textarea',
                                   'class' => 'ckeditor',
                                   'name' => 'data[Deal][description]',
                                   'id' => 'DealDescriptionRepost',
                                   'maxlength' => '65535'));
           ?>

           <div class="clearfix" style="margin-top:5px;"></div>
           <label class="pull-left" style="margin-top:5px;"><strong>Branch</strong></label>
           <select id="merchant-branch" class="pull-left" style="margin-left:10px; width:277px"  name="data[Deal][branch_id]">
                <option value="0" data-lat="<?php echo $lat ?>" data-lng="<?php echo $lng ?>">Any branch</option>
                <?php foreach( $data_braches as $k => $val ){ ?>
                  <option value="<?php echo $val['id'] ?>"
                          data-lat="<?php echo $val['latitude'] ?>"
                          data-lng="<?php echo $val['longitude'] ?>"><?php echo $val['branch_name'] ?></option>
                <?php } ?>
           </select>

          <input type="hidden" value="<?php echo $lat ?>" name="data[Deal][latitude]"   id="latitude" />
          <input type="hidden" value="<?php echo $lng ?>" name="data[Deal][longitude]"  id="longitude" />


          <div style="display:block; clear:both; margin-top:25px;">
            <label for='last-minute' style="float:left; width:100px; padding-top:3px; font-weight: bold;">Last Minute Deal</label>
            <input id="last-minute" type="checkbox" value="1" name="data[Deal][is_last_minute_deal]" style="flaot:left; margin-left:5px;"/>
          </div>
          
        </div>

      <div class="span4" style="width:330px;">

          <?php 
            echo $this->Form->input('deal_condition', array('placeholder' => 'Deal Conditions',
                                   'type' => 'textarea',
                                   'class' => 'ckeditor',
                                   'name' => 'data[Deal][deal_condition]',
                                   'id' => 'DealConditionsRepost',
                                   'maxlength' => '65535'));
          ?>

          <div class="clearfix" style="margin-top:5px;"></div>
          <input type="hidden" name="data[Deal][status]" value="1">
        
      </div> 

    </div>

    <div class="span6" >

      <div class="span6" style="margin-top:18px; text-align:left;">

         <input type="hidden" id="old_image_path" name="old_image_path" >

      </div>
      
    </div>

    <div class="clearfix"></div>
    <legend style="font-size:16px;"><h5>Related Items</h5></legend>

    <a href="#" class="btn btn-linkedin" id="add-related-item-repost-btn"><i class="icon-plus" style="color:white"></i> Add Related Item</a>

    <div class="span11" style="margin-left:0px; padding-top:20px; width:950px;" id="div-related-item">

        <table class="table-list" id="table-item-listing-repost" >
            <thead>
                <th width="200px;"><?php echo __('Menu Category'); ?></th>
                <th><?php echo __('Item Name'); ?></th>
                <th width="150px;"><?php echo __('Original Price'); ?></th>
                <th width="150px;"><?php echo __('Discounted Price'); ?></th>
                <th width="150px;"><?php echo __('Available Qty'); ?></th>
                <th width="60px;"><?php echo __('Action'); ?></th>
            </thead>    

            <tbody>
                
                <tr class="related-item-row-repost">

                    <input type="hidden" name="DealItemLink[item_id][]" class='hidden-item-id' >
                    <input type="hidden" name="DealItemLink[original_price][]" class='hidden-item-original-price' >
                    <input type="hidden" name="DealItemLink[discount][]" class='hidden-item-discount' >
                    <input type="hidden" name="DealItemLink[available_qty][]" class='hidden-item-qty' >
                    
                    <td class='item-menu' style="font-weight: bold;"></td>
                    <td class='item-title'></td>
                    <td class='item-price'></td>
                    <td class='item-discount-price'>
                        <span></span>
                        <input style="width:100px; text-align:center; display:none;" 
                                 onkeypress="return numericAndDotOnly(this)"
                                value="" />
                    </td>
                    <td class='item-available-qty' style="text-align:right">
                        <span></span>
                        <input style="width:100px; text-align:center; display:none;" 
                                 onkeypress="return numericAndDotOnly(this)"
                                value="" />
                    </td>
                    <td class='item-action' style="text-align:center">

                        <a  href="#" class='btn btn-success save-change' 
                            style="display:none;"
                            data-price="" title="Save Change">
                            <i class='icon-save' style='font-size:12px; padding-right:0px; color:white;'></i></a>

                        <a  href="#" class='btn btn-linkedin edit-selected-item' 
                            title="Edit">
                            <i class='icon-edit' style='font-size:12px; padding-right:0px; color:white;'></i></a>

                        <a href="#" class='btn btn-google remove-selected-item'>
                            <i class='icon-trash' style='font-size:12px; padding-right:0px; color:white'></i></a>
                    </td>
                </tr>

                <tr id="row-empty-item-repost">
                  <td colspan="10"><i>Empty Items !</i></td>
                </tr>
            </tbody>      
        </table>

    </div>

    <div class="clearfix"></div>

    <div class="submit" style="margin-top: 20px; clear: both; margin-left:20px;">
      <button class="btn btn-google cancel" data-dismiss='modal' type="button">Cancel</button>
      <input class="btn btn-primary" type="submit" value="Submit Repost" id="save-deal-repost" >
    </div>
      
  </form>
  </div>

</div> 

<!-- Remove Deal Image -->
<div id="remove-deal-image" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true"
  style="z-index:999999">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel" style="color:red"><i class="icon-question"></i> Are you sure ?</h3>
  </div>

  <div class="span6" style="padding-top: 10px; padding-bottom: 20px;">
    
    <h5>Are you sure you want to remove this image?</h5>

    <button class="btn btn-primary" type="button" id='confirm-delete'>Yes</button>
    <button class="btn btn-google cancel" data-dismiss='modal' type="button">No</button>

  </div>

</div> 


<!-- Menu Item List Add -->
<div id="related-item-modal" class="modal hide fade"
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true"
  style=" width:820px; left:38%">
  
    <form action="#">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h3 id="myModalLabel"><i class="icon-list"></i> Item List</h3>
        </div>
        
        <div class="clearfix"></div>

        <div class="span4">
            <label for="business-menu"><?php echo __('Business Menu') ?></label>
            <select name="" id="business-menu">
                <option value=""><?php echo __('-- select a menu --'); ?></option>
                <?php foreach( $business_menus as $key => $val ){ ?>
                    <option value="<?php echo $val['BusinessMenu']['id'] ?>"
                            data-name="<?php echo $val['BusinessMenu']['name'] ?>"><?php echo $val['BusinessMenu']['name'] ?></option>
                <?php } ?>
            </select>

            <label for="business-menu-item"><?php echo __('Menu Item') ?></label>
            <select name="" id="business-menu-item" >
                <option value=""><?php echo __('-- select an item --'); ?></option>
            </select>

        </div>

        <div class="span6">
            
            <div style="float:left; width:215px;">

                <label for="original-price"><?php echo __('Original Price (USD)') ?></label>
                <input type="text" id="original-price" 
                        placeholder="Original Price" readonly="readonly" 
                        style="width:160px; font-weight: bold; font-size:15px;"
                        value="0.00" >
            </div>

            <div style="float:left; width:200px;">
                <label for="discounted-price"><?php echo __('Discounted Price (USD)') ?></label>
                <input type="text" id="discounted-price" placeholder="Discounted Price" onkeypress="return numericAndDotOnly(this)"
                        style="width:160px; font-size:15px; font-weight: bold; color:#06F">
            </div>

            <div style="width:200px;">
                <label for="available-qty"><?php echo __('Available Qty') ?></label>
                <input type="text" id="available-qty" placeholder="Available Qty" onkeypress="return numericAndDotOnly(this)"
                        style="width:160px; font-size:15px; font-weight: bold;">
            </div>
            
        </div>
        
        <div class="clearfix"></div>
        <div class="submit" style=" margin-left:20px; clear: both;">
          <button class="btn btn-google cancel" data-dismiss='modal' type="button">Close</button>
          <button class="btn btn-primary" data-dismiss='modal' type="button" id="select-item-btn">Select Item</button>
        </div>

    </form>

</div>


<div id="remove-item-modal" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true"
  style="">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel" style="color:red"><i class="icon-exclamation"></i> Warning !</h3>
  </div>

  <div class="span6" style="padding-top: 10px; padding-bottom: 20px;">
    
    <h5>Are you sure you want to delete this item?</h5>


    <button class="btn btn-google cancel" data-dismiss='modal' type="button">No</button>
    <button class="btn btn-primary" data-dismiss='modal' type="button" id="confirm-remove">Yes</button>

  </div>
</div> 


<!-- Menu Item List Edit -->
<div id="related-item-edit-modal" class="modal hide fade"
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true"
  style=" width:820px; left:38%">
  
    <form action="#">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h3 id="myModalLabel"><i class="icon-list"></i> Item List</h3>
        </div>
        
        <div class="clearfix"></div>

        <div class="span4">
            <label for="business-menu-edit"><?php echo __('Business Menu') ?></label>
            <select name="" id="business-menu-edit">
                <option value=""><?php echo __('-- select a menu --'); ?></option>
                <?php foreach( $business_menus as $key => $val ){ ?>
                    <option value="<?php echo $val['BusinessMenu']['id'] ?>"
                            data-name="<?php echo $val['BusinessMenu']['name'] ?>"><?php echo $val['BusinessMenu']['name'] ?></option>
                <?php } ?>
            </select>


            <label for="business-menu-item-edit"><?php echo __('Menu Item') ?></label>
            <select name="" id="business-menu-item-edit">
                <option value=""><?php echo __('-- select an item --'); ?></option>
            </select>
        </div>

        <div class="span6">
            
            <div style="float:left; width:215px;">

                <label for="original-price-edit"><?php echo __('Original Price (USD)') ?></label>
                <input type="text" id="original-price-edit" 
                        placeholder="Original Price" readonly="readonly" 
                        style="width:160px; font-weight: bold; font-size:15px;"
                        value="0.00" >
            </div>

            <div style="float:left; width:200px;">
                <label for="discounted-price-edit"><?php echo __('Discounted Price (USD)') ?></label>
                <input type="text" id="discounted-price-edit" placeholder="Discounted Price" onkeypress="return numericAndDotOnly(this)"
                        style="width:160px; font-size:15px; font-weight: bold; color:#06F; z-index:999999;">
            </div>

            <div style="width:200px;">
                <label for="available-qty-edit"><?php echo __('Available Qty') ?></label>
                <input type="text" id="available-qty-edit" placeholder="Available Qty" onkeypress="return numericAndDotOnly(this)"
                        style="width:160px; font-size:15px; font-weight: bold;">
            </div>
            
        </div>
        
        <div class="clearfix"></div>
        <div class="submit" style=" margin-left:20px; clear: both;">
          <button class="btn btn-google cancel" data-dismiss='modal' type="button">Close</button>
          <button class="btn btn-primary" data-dismiss='modal' type="button" id="select-item-edit-btn">Select Item</button>
        </div>

    </form>

</div>

<div id="remove-item-edit-modal" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true"
  style="">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel" style="color:red"><i class="icon-exclamation"></i> Warning !</h3>
  </div>

  <div class="span6" style="padding-top: 10px; padding-bottom: 20px;">
    
    <h5>Are you sure you want to delete this item?</h5>


    <button class="btn btn-google cancel" data-dismiss='modal' type="button">No</button>
    <button class="btn btn-primary" data-dismiss='modal' type="button" id="confirm-remove-edit">Yes</button>

  </div>
</div> 


<!-- Menu Item List Repost -->
<div id="related-item-repost-modal" class="modal hide fade"
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true"
  style=" width:820px; left:38%">
  
    <form action="#">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h3 id="myModalLabel"><i class="icon-list"></i> Item List</h3>
        </div>
        
        <div class="clearfix"></div>

        <div class="span4">
            <label for="business-menu-repost"><?php echo __('Business Menu') ?></label>
            <select name="" id="business-menu-repost">
                <option value=""><?php echo __('-- select a menu --'); ?></option>
                <?php foreach( $business_menus as $key => $val ){ ?>
                    <option value="<?php echo $val['BusinessMenu']['id'] ?>"
                            data-name="<?php echo $val['BusinessMenu']['name'] ?>"><?php echo $val['BusinessMenu']['name'] ?></option>
                <?php } ?>
            </select>


            <label for="business-menu-item-repost"><?php echo __('Menu Item') ?></label>
            <select name="" id="business-menu-item-repost">
                <option value=""><?php echo __('-- select an item --'); ?></option>
            </select>
        </div>

        <div class="span6">
            
            <div style="float:left; width:215px;">

                <label for="original-price-repost"><?php echo __('Original Price (USD)') ?></label>
                <input type="text" id="original-price-repost" 
                        placeholder="Original Price" readonly="readonly" 
                        style="width:160px; font-weight: bold; font-size:15px;"
                        value="0.00" >
            </div>

            <div style="float:left; width:200px;">
                <label for="discounted-price-repost"><?php echo __('Discounted Price (USD)') ?></label>
                <input type="text" id="discounted-price-repost" placeholder="Discounted Price" onkeypress="return numericAndDotOnly(this)"
                        style="width:160px; font-size:15px; font-weight: bold; color:#06F; z-index:999999;">
            </div>

            <div style="width:200px;">
                <label for="available-qty-repost"><?php echo __('Available Qty') ?></label>
                <input type="text" id="available-qty-repost" placeholder="Available Qty" onkeypress="return numericAndDotOnly(this)"
                        style="width:160px; font-size:15px; font-weight: bold;">
            </div>
            
        </div>
        
        <div class="clearfix"></div>
        <div class="submit" style=" margin-left:20px; clear: both;">
          <button class="btn btn-google cancel" data-dismiss='modal' type="button">Close</button>
          <button class="btn btn-primary" data-dismiss='modal' type="button" id="select-item-repost-btn">Select Item</button>
        </div>

    </form>

</div>

<div id="remove-item-repost-modal" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true"
  style="">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel" style="color:red"><i class="icon-exclamation"></i> Warning !</h3>
  </div>

  <div class="span6" style="padding-top: 10px; padding-bottom: 20px;">
    
    <h5>Are you sure you want to delete this item?</h5>

    <button class="btn btn-google cancel" data-dismiss='modal' type="button">No</button>
    <button class="btn btn-primary" data-dismiss='modal' type="button" id="confirm-remove-repost">Yes</button>

  </div>
</div> 