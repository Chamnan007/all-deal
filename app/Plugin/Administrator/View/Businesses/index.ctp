<?php 
  /********************************************************************************
      File: Business Listing
      Author: PHEA RATTANA
  
      Confidential ABi Technologies property.
  
      Changed History:
      Date          Author        Description
      2014/01/05        PHEA RATTANA    Initial
  *********************************************************************************/
  // var_dump($data);

  $pro_name = array();

  if( !empty($provinces) ){
    foreach( $provinces as $key => $value ){
      $pro_name[$value['Province']['province_code']] = $value['Province']['province_code'] . " - " . $value['Province']['province_name'];
    }
  }

  $bis_categories = array();

  if( !empty($businessCategories) ){
    foreach( $businessCategories as $key => $value ){
      $bis_categories[$value['BusinessCategory']['id']] = $value['BusinessCategory']['category'] ;
    }
  }

  // var_dump($states);
  
 ?>


<div class="businesses index">
  <div class="row-fluid">   
    <!-- Pie: Box -->
    <div class="span12">

      <!-- Pie: Top Bar -->
      <div class="top-bar">
        <h3><i class="icon-list"></i> Manage Merchants</h3>
      </div>
      <!-- / Pie: Top Bar -->

      <!-- Pie: Content -->
      <div class="well">

      <a href="<?php echo $this->Html->url( array('action' => 'add')); ?>"> 
        <button class="btn btn-linkedin" type="button"><i class="icon-plus"></i> Add New Merchant</button>
      </a>
      

      <a href="#" class="pull-right" data-toggle="modal" style="margin-left:10px;" id="clear_search"> 
        <button class="btn btn-google" type="button"><i class="icon-trash"></i> Clear Search</button>
      </a>

      <a href="#advance_search" class="pull-right" data-toggle="modal"> 
        <button class="btn btn-skype" type="button"><i class="icon-search"></i> Advance Search</button>
      </a>

      <div class="clearfix"></div>
        
        <table class="table-list">

          <thead>
            <tr>
              <th width="250px"><?php echo $this->Paginator->sort('business_name', 'Name'); ?></th>
              <th width="100px;"><?php echo $this->Paginator->sort('business_main_category','Category'); ?></th>
              <th width="200px"><?php echo $this->Paginator->sort('location','Address'); ?></th>
              <th><?php echo $this->Paginator->sort('email','Contact Information'); ?></th>
              <th width="110px"><?php echo $this->Paginator->sort('member_level'); ?></th>
              <th><?php echo $this->Paginator->sort('status'); ?></th>
              <th><?php echo $this->Paginator->sort('ending_balance', 'Available Balance') ?></th>
              <th width="60px;" class="actions"><?php echo __('Actions'); ?></th>
            </tr>
          </thead>

          <tbody>

          <?php 
            if( $data ){

            foreach ($data as $key => $business): 

              $city     = urldecode($business['City']['city_name']);

              $location = ( $business['Business']['location'] != "" )? $business['Business']['location'] . ", " :"";
              $street   = ( $business['Business']['street'] != "" )? $business['Business']['street'] . ", " :"";
              $sangkat  = ($business['SangkatInfo']['name'])?$business['SangkatInfo']['name'] . ", " :"";
              $address = $street . $sangkat . $location . $city ;

              $main_category = urldecode($business['MainCategory']['category']);
              $sub_category = urldecode($business['SubCategory']['category']);

              $balance = $business['Business']['ending_balance'];

              if( $balance > 0 ){
                $balance = "$ " . $this->MyHtml->formatNumber($business['Business']['ending_balance']);
              }else{
                $balance = "-";
              }
          ?>
            <tr>  
              <td style="vertical-align: top">
                Code : <a href="<?php echo $this->Html->url(array('action' => 'view', $business['Business']['id'])); ?>">
                     <strong style="">
                        <?php echo h($business['Business']['business_code']); ?>
                      </strong>
                </a><br>
                Name : <a href="<?php echo $this->Html->url(array('action' => 'view', $business['Business']['id'])); ?>">
                 <strong>
                    <?php echo h($business['Business']['business_name']); ?>
                  </strong>
                </a>
              </td>

              <td style="vertical-align: top"><?php echo h($main_category); ?></td>
              <td style="vertical-align: top"><?php echo h($address); ?>&nbsp;</td>
              <td style="vertical-align: top">Tel: <?php echo h($business['Business']['phone1']); echo($business['Business']['phone2'])?" / " . $business['Business']['phone2']:"" ?><br>
                Email: <?php echo h($business['Business']['email']); ?>
              </td>
              <td style="vertical-align:top"><b><?php echo $biz_access_level[$business['Business']['member_level']] ?></b></td>
              <td style="text-align: center; vertical-align: top">
                <span class="label label-<?php echo $status[$business['Business']['status']]['color'] ?>"><?php echo h($status[$business['Business']['status']]['status']); ?></span>
              </td>

              <td style="vertical-align:top; text-align:right; color: #06F;">
                <strong><?php echo $balance ?></strong>
              </td>
          
              <td class="actions" style="vertical-align: top; text-align:right">
                <a href="<?php echo $this->Html->url(array('action' => 'view', $business['Business']['id'])); ?>" title="View Merchant Detail"> 
                  <button class="btn btn-foursquare" type="button" style="padding:2px 3px;">
                    <i class="icon icon-eye-open"></i>
                  </button>
                </a>
                
                <a href="<?php echo $this->Html->url(array('action' => 'edit', $business['Business']['id'])); ?>" title="Edit Merchant"> 
                  <button class="btn btn-linkedin" type="button" style="padding:2px 3px;">
                    <i class="icon icon-edit"></i>
                  </button>
                </a>
                <!-- <a href="<?php echo $this->Html->url(array('action' => 'delete', $business['Business']['id'])); ?>"> 
                  <button class="btn" type="button">Delete</button>
                </a> -->
              </td>
            </tr>
          <?php endforeach; 
          }else{
          ?>  
            <tr>
              <td colspan="10"><i>No Merchant Found !</i></td>
            </tr>
          <?php  
          }
          ?>
          </tbody>
        </table>

        <p class="page">
          <?php
          echo $this->Paginator->counter(array(
          'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total')
          ));
          ?>  
        </p>

        <div class="paging">

        <?php
          echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
          echo $this->Paginator->numbers(array('separator' => ''));
          echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
        ?>

        </div>
      </div>
      <!-- / Pie: Content -->

    </div>
    <!-- / Pie -->
    
  </div>

</div>


<style>
  .chosen-container ,.chosen-container-single{
    width: 100% !important;
  }

</style>


<div id="advance_search" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true"
  style="padding-bottom: 20px !important; ">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel"><i class="icon-search"></i> Advance Search</h3>
  </div>

  <!-- Any Info-customization -->
  <form action="<?php echo $this->Html->url(array('action' => 'index')); ?>" 
      style="padding: 20px;" method="POST" class="form-horizontal" id="advance_search_from" >
    

    <div class="span3" style="margin-left:0px; margin-right: 25px">

      <div class="input text" style="margin-bottom:10px;">
        <label for="business_code">Code</label>
        <input type="text" id="business_code" 
              value="<?php echo @$statusArray['business_code'] ?>"
              name="business_code" placeholder="Merchant Code" maxlength="6" >
      </div>

      <div class="input text" style="margin-bottom:10px;">
        <label for="BusinessPhone1">Name</label>
        <input type="text" id="business_name" name="business_name" placeholder="Merchant Name" 
              value="<?php echo @$statusArray['business_name'] ?>" >
        <input type="hidden" name="sort" id="sort" value="name" >
      </div>

      <?php 
        echo $this->Form->input(  'province' ,array(
                         'type'     => 'select',
                         'label'    => 'Province',
                         'name'     => 'province',
                         'id'       => 'province',
                         'empty'    => 'All Provinces',
                         'default'  => @$statusArray['province'],
                         'options'  => $pro_name
                      ));

      ?>


      <?php 
        echo $this->Form->input(  'city' ,array(
                         'type'     => 'select',
                         'label'    => 'city',
                         'name'     => 'city',
                         'id'     => 'city',
                         'empty'  => 'All Cities',
                         'options'  => array()
                      ));
      ?>

    </div>
    
    <div class="span3">


      <?php 
        echo $this->Form->input(  'main_category' ,array(
                         'type'     => 'select',
                         'label'    => 'Category',
                         'name'     => 'main_category',
                         'id'     => 'main_category',
                         'empty'  => 'All Categoies',
                         'options'  => $bis_categories
                      ));


        // echo $this->Form->input(  'sub_category' ,array(
        //                  'type'     => 'select',
        //                  'label'    => 'Sub-Category',
        //                  'name'     => 'sub_category',
        //                  'id'     => 'sub_category',
        //                  'empty'  => 'select a category',
        //                  'options'  => array()
        //               ));
      ?>

      <div class="clearfix" style="margin-bottom:10px;"></div>
      <label for="status">Business Status</label>
      <select name="status" id="status" >
        <option value="">All Status</option>
        <?php foreach($status as $key => $val ): ?>
          <option value="<?php echo $key ?>"><?php echo $val['status'] ?></option>
        <?php endforeach; ?>
      </select>


    </div>

    
    <div class="submit" style="padding-top: 20px; clear: both;">
      <button class="btn btn-google" data-dismiss='modal' type="button">Cancel</button>
      <input class="btn btn-primary" type="submit" value="Search">
    </div>
      
  </form>

</div> 

<script>

      $("#business_name").val("");
      $("#province").val("");
      $("#city").val("");
      $("#main_category").val("");
      $("#sub_category").val("");
      $("#status").val("");
      $("#sort").val("name");

  $("#clear_search").click( function (){

      $("#business_name").val("");
      $("#province").val("");
      $("#city").val("");
      $("#main_category").val("");
      $("#sub_category").val("");
      $("#status").val("");
      $("#sort").val("date");

      $("#advance_search_from").submit();
     
  });


  $("#province").change( function (){

    var me = $(this);

    var citySelect    = $("#city");

    var pro_code = me.val();

    var url = '<?php echo $this->webroot ?>' + 'administrator/users/get_city_by_province/' + pro_code ;

    if( pro_code != '' ){
      $.ajax({
              type: 'POST',
              url: url,
              data: { pro_code : pro_code },
              dataType: 'json',
              
              beforeSend: function(){
                  citySelect.html("<option value=''>loading...</option>");
              },

              success: function (data){
                  // console.log(data);

                  var data = data.data;

                  citySelect.html("<option value=''>select a city</option>");
                  $.each( data, function (ind, val ){
                    var city = val.City;

                    citySelect.append("<option value='" + city.city_code + "'>" + city.city_code + " - " + city.city_name + "</option>" )
                  });


              },

              error: function ( err ){
                console.log(err);
              }

          });

    }else{
      citySelect.html("<option value=''>select an option</option>");
    }

  });

</script>