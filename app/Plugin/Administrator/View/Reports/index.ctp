<?php 
  /********************************************************************************
      File: Report
      Author: PHEA RATTANA
  
      Confidential ABi Technologies property.
  
      Changed History:
      Date          Author        Description
      2014/June/13      PHEA RATTANA    Initial
  *********************************************************************************/

  $report = array();
  $report[] = array('User', 'Total' );

  if( isset($result) && !empty($result) ){
    foreach($result as $key => $data){
      $report[] = array( $data['user']['last_name'] . ' ' . $data['user']['first_name'], intval($data[0]['total']) );
    }
  }

  $report = json_encode($report);


  $deal_report = array();
  $deal_report[] = array('User', 'Total', array('role'=>'style'));

  if( isset($deal_result) && !empty($deal_result) ){
    foreach($deal_result as $key => $data){
      $deal_report[] = array( $data['user']['last_name'] . ' ' . $data['user']['first_name'], intval($data[0]['total']), 'red' );
    }
  }

  $deal_report = json_encode($deal_report);
  
 ?>

<div class="provinces index">
  <div class="row-fluid">   
    <!-- Pie: Box -->
    <div class="span12">

      <!-- Pie: Top Bar -->
      <div class="top-bar">
        <h3><i class="icon-list"></i> Report</h3>
      </div>
      <!-- / Pie: Top Bar -->

      <!-- Pie: Content -->
      <div class="well">


      <?php if ( $user_access_level == 6 || $user_access_level == 5 ){ ?>

        <legend>Report Filter</legend>

        <form action="" method="POST">

          <div class="span6" style="margin-left:0px;">

            <div class="control-group" style="padding-bottom:15px;">
              <label class="control-label" for="inputNormal"><i class="icon-user"></i>Select Users</label>
              <div class="controls"  style="  width:350px; 
                              max-height:80px; 
                              height: 80px; 
                              border: 1px solid #CCC; 
                              padding:10px; 
                              padding-top:5px;
                              overflow-y: auto;
                              margin-bottom:5px;">
                
                 <div class="input checkbox" style="width:80%;">
                  <input style="float:left; margin-right: 6px;" type="checkbox" name="user[]" 
                      id='all' 
                      value="all"
                      <?php echo(!empty($user_id) && in_array('all', $user_id))?" checked='checked'":"" ?> >
                  <label for="all">All Users</label>
                 </div>

                  <div class="input checkbox" style="width:80%;">
                  <input style="float:left; margin-right: 6px;" type="checkbox" name="user[]" 
                      id='my_report' 
                      value="<?php echo $my_id ?>" 
                      <?php echo(!empty($user_id) && in_array( $my_id, $user_id))?" checked='checked'":"" ?>>
                  <label for="my_report">My Report</label>
                 </div>

                <?php foreach( $users as $key => $user){ ?>

                 <div class="input checkbox" style="width:80%;">
                  <input style="float:left; margin-right: 6px;" type="checkbox" name="user[]" 
                      id='<?php echo $user['User']['id'] ?>' 
                      value="<?php echo $user['User']['id'] ?>"
                      <?php echo(!empty($user_id) && in_array( $user['User']['id'] , $user_id))?" checked='checked'":"" ?> >
                  <label for="<?php echo $user['User']['id'] ?>"><?php echo urldecode($user['User']['last_name'] . " " . $user['User']['first_name'] ) ?></label>
                 </div>

                <?php } ?>
              </div>
            </div>  

          </div>
        

          <div class="span6" style="margin-left:0px;">

            <div class="control-group" id="datePickerFrom">
              <label class="control-label" for="inputNormal"><i class="icon-time"></i>From Date</label>
              <div class="controls" style="margin-left:115px;">
                <input data-format="dd-MM-yyyy" type="text" 
                      name="from_date" id="DealsValidFrom"
                      style="width: 165px !important"
                      placeholder="From Date" 
                      value="<?php echo($from != "")?date('d-m-Y', strtotime($from)):""; ?>"></input>
                  <span class="add-on">
                    <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                    </i>
                  </span>
              </div>
            </div>  

            <div class="control-group" id="datePickerTo">
              <label class="control-label" for="inputNormal"><i class="icon-time"></i>To Date</label>
              <div class="controls" style="margin-left:115px;">
                <input data-format="dd-MM-yyyy" type="text" 
                      name="to_date" id="DealsValidFrom"
                      style="width: 165px !important"
                      placeholder="To Date"
                      value="<?php echo($to != "")?date('d-m-Y', strtotime($to)):"" ?>" ></input>
                  <span class="add-on">
                    <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                    </i>
                  </span>
              </div>
            </div>  

          </div>


          <div class="submit" style="margin-top: 20px; clear: both; float:right; padding-right:70px;">
              <input class="btn btn-primary" type="submit" value="Submit Filter">
          </div>  

        </form>

      <?php }else{ ?>

        <legend>Report Filter</legend>

        <form action="" method="POST">
        

          <div class="span5" style="margin-left:0px;">

            <div class="control-group" id="datePickerFrom">
              <label class="control-label" for="inputNormal"><i class="icon-time"></i>From Date</label>
              <div class="controls" style="margin-left:115px;">
                <input data-format="dd-MM-yyyy" type="text" 
                      name="from_date" id="DealsValidFrom"
                      style="width: 165px !important"
                      placeholder="From Date" 
                      value="<?php echo($from != "")?date('d-m-Y', strtotime($from)):""; ?>"></input>
                  <span class="add-on">
                    <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                    </i>
                  </span>
              </div>
            </div>  

          </div>


          <div class="span5" style="margin-left:0px;">

            <div class="control-group" id="datePickerTo">
              <label class="control-label" for="inputNormal"><i class="icon-time"></i>To Date</label>
              <div class="controls" style="margin-left:115px;">
                <input data-format="dd-MM-yyyy" type="text" 
                      name="to_date" id="DealsValidFrom"
                      style="width: 165px !important"
                      placeholder="To Date"
                      value="<?php echo($to != "")?date('d-m-Y', strtotime($to)):"" ?>" ></input>
                  <span class="add-on">
                    <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                    </i>
                  </span>
              </div>
            </div>  


          </div>

          <div class="span2" style="margin-left:0px;">
            <div class="control-group" style="padding-bottom:22px;">
              <input class="btn btn-primary" type="submit" value="Submit Filter">
            </div>
          </div>  

        </form>
      <?php } ?>
  
      <?php if( isset($result) ){ ?>
        
        <legend>Result</legend>

        <div class="span6" id="chart_div" style="height:300px; margin-left:0px;">
            <?php echo(empty($result))?"<h5>No Report Available.</h5>":"" ?>
        </div>

        <div class="span6" id="chart_div_deal" style="height:300px;">
          
        </div>

      <?php } ?>
        
      </div>
      <!-- / Pie: Content -->

    </div>
    <!-- / Pie -->
    
  </div>

</div>


<a href="#alert_div" target="_blank" data-toggle="modal" id="alert_click"></a>


<div id="alert_div" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true"
  style=" width:80% !important; left:30% !important; margin-top:0 !important;">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="title"></h3>
  </div>

  <!-- Any Info-customization -->    
  <div style="max-height:550px; overflow-x: scroll;">
    
    <div style="padding:20px;" id="content">
        <span id='date'></span>

        <div class="clearfix"></div>
        
        <table class="table-list" id="list_here">

          <thead>
            <tr>
              <th width="200px">Business Name</th>
              <th width="150px;">Category</th>
              <th width="200px">Location</th>
              <th>Contact Information</th>
              <th width="110px">Member Level</th>
              <th>Status</th>
              <th width="100px;" class="actions"><?php echo __('Actions'); ?></th>
            </tr>
          </thead>
          
          <tbody>
              <tr>
                
              </tr>
          </tbody>

        </table>
    </div>
    
    <div class="submit" style="padding-top: 20px; clear: both; padding:20px;">
      <button class="btn btn-google" data-dismiss='modal' type="button">Close</button>
    </div>

  </div>

</div> 



<script type="text/javascript" src="https://www.google.com/jsapi"></script>

<script>

  $(document).ready(function (){

    $('#datePickerTo, #datePickerFrom').datetimepicker();

  });

  google.load("visualization", "1", {packages:["corechart"]});
    
    function drawChartBusiness() {

      var report = <?php echo $report ?> ;

      var data = google.visualization.arrayToDataTable(report) ;

        var options = {
          title: 'Business Registered',
          hAxis: {title: "User's Name", titleTextStyle: {color: 'blue'}}
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));


        // google.visualization.events.addListener(chart, 'select', function(){

        //       var div = $("div#alert_div");

        //       var from = "<?php echo date('d-M-Y', strtotime($from)) ?>";
        //       var to = "<?php echo date('d-M-Y', strtotime($to)) ?>";
              
        //       var selectedItem = chart.getSelection()[0];

        //       if (selectedItem) {
        //         var topping = data.getValue(selectedItem.row, 0);
        //         div.find("h3#title").html("<i class='icon-list-alt'></i>Business Added By " + topping );
        //       }

        //       div.find('#content').find('span#date').html("<b>From</b> : " + from + "  <b>To</b> : " + to );

        //       $('a#alert_click').click();
        // }); 

        chart.draw(data, options);


    }  

    function drawChartDeal() {

      var report_daeal = <?php echo $deal_report ?> ;

        var data1 = google.visualization.arrayToDataTable(report_daeal) ;
        var options1 = {
          title: 'Deal Created',
          hAxis: {title: "User's Name", titleTextStyle: {color: 'blue'}},
          colors: ['red']
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('chart_div_deal'));
        chart.draw(data1, options1);

    }

</script>

<?php if(isset($result) && !empty($result) ){ ?>
  
  <script>
    if(document.getElementById("chart_div")){
        google.setOnLoadCallback(drawChartBusiness);  
    }
  </script>

<?php } ?>



<?php if(isset($deal_result) && !empty($deal_result) ){  ?>
  
  <script>
    if(document.getElementById("chart_div_deal")){
        google.setOnLoadCallback(drawChartDeal);  
    }
  </script>

<?php } ?>