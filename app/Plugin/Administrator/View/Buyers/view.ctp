<?php 
	/********************************************************************************
	    File: Detail Information User
	    Author: PHEA RATTANA
	
	    Confidential ABi Technologies property.
	
	  	Changed History:
	  	Date 					Author				Description
	  	2014/01/05 				PHEA RATTANA		Initial
	*********************************************************************************/

	$active_tab = "info";

	if( isset($this->params['pass'][1]) ){
	    $active_tab = $this->params['pass'][1];
	}

	$user = CakeSession::read("Auth.User");	

?>

<style>
	td{
		font-size: 13px;
		padding: 5px;
	}
	.title {
		width: 160px;
		font-weight: bold;
	}
	
	.col {
		width: 20px;
	}

	.equalto, .required{
		color: red;
		font-size: 12px !important;
		list-style-type: none;
	}

</style>

<div class="users form">

	<div class="row-fluid">		
		<!-- Pie: Box -->
		<div class="span12">

			<!-- Pie: Top Bar -->
			<div class="top-bar">
				<ul class="tab-container">
		            <li <?php echo ($active_tab == 'info')?" class='active'":"" ?>><a href="#tab-info"><i class="icon-list"></i> User Information</a></li>
		            <li <?php echo ($active_tab == 'balance')?" class='active'":"" ?>><a href="#tab-balance"><i class="icon-list-alt"></i> Balance</a></li>
		            <li <?php echo ($active_tab == 'transaction')?" class='active'":"" ?>><a href="#tab-history"><i class="icon-list-alt"></i> Transactions History</a></li>
		        </ul>
			</div>
			<!-- / Pie: Top Bar -->

			<!-- Pie: Content -->
			<div class="well">

				<a href="<?php echo $this->Html->url( array('action' => 'index')); ?>"> 
					<button class="btn btn-linkedin" type="button"><i class="icon-list-alt"></i> Back to List</button>
				</a>				

				<div class="clearfix"></div>
      
        		<div class="tab-content">

          			<div class="tab-pane  <?php echo ($active_tab == 'info')?" active":"" ?>" id="tab-info">

          				<div class="span6">
          					<legend><?php echo __('Detail Information'); ?></legend>
				
							<table style="margin-left: 30px;">

								<tr>
									<td class='title'>Name</td>
									<td class="col">:</td>
									<td><?php echo ucwords($data['Buyer']['full_name']); ?></td>
								</tr>

								<tr>
									<td class='title'>Gender</td>
									<td class="col">:</td>
									<td><?php echo h($data['Buyer']['gender']); ?></td>
								</tr>

								<tr>
									<td class='title'>Date of Birth</td>
									<td class="col">:</td>
									<td><?php echo date("d-F-Y", strtotime($data['Buyer']['dob'])); ?></td>
								</tr>

								<tr>
									<td class='title'>Address</td>
									<td class="col">:</td>
									<td>
										<?php 

											echo ($data['Buyer']['street'] == NULL)?"":$data['Buyer']['street'] . ", " ; 
											echo ($data['SangkatInfo']['name'])?$data['SangkatInfo']['name'] . ", ":"";
											echo h($data['Buyer']['location']) . ", ";
											echo h($data['City']['city_name']) ;
										?>
									</td>
								</tr>

								<tr>
									<td class='title'>Phone</td>
									<td class="col">:</td>
									<td><?php echo h($data['Buyer']['phone1']); echo($data['Buyer']['phone2'])?" / " . $data['Buyer']['phone2']:"" ?></td>
								</tr>

								<tr>
									<td class='title'>Email</td>
									<td class="col">:</td>
									<td><?php echo h($data['Buyer']['email']); ?></td>
								</tr>

								<tr>
									<td class='title'>Registered Date</td>
									<td class="col">:</td>
									<td><?php echo date("d-M-Y | h:i:s A", strtotime($data['Buyer']['created'])); ?></td>
								</tr>

								<tr>
									<td class='title'>Status</td>
									<td class="col">:</td>
									<td>
										<span class="label label-<?php echo $status[$data['Buyer']['status']]['color'] ?>">
											<?php echo h($status[$data['Buyer']['status']]['status']); ?>
										</span>
									</td>
								</tr>
								<?php if( $data['Buyer']['status'] == '-1' ): ?>

									<tr>
										<td class='title'>Inactive Date</td>
										<td class="col">:</td>
										<td><?php echo date("d-M-Y | h:i A", strtotime($data['Buyer']['modified'])); ?></td>
									</tr>

								<?php endif ?>

							</table>
          				</div>

          				<div class="span6">
          					<legend>Referer Information</legend>

          					<?php 
          						$RefererInfo = $data['RefererInfo'];

          						if( $data['Buyer']['referer_id'] ){
          					 ?>
								<table width="100%">
									<tr>
										<td width="100px;">Name</td>
										<td width="20px">:</td>
										<td><?php echo ucwords($RefererInfo['full_name']) ?></td>
									</tr>

									<tr>
										<td>Gender</td>
										<td>:</td>
										<td><?php echo $RefererInfo['gender'] ?></td>
									</tr>

									<tr>
										<td>Phone</td>
										<td>:</td>
										<td><?php echo h($RefererInfo['phone1']); echo($RefererInfo['phone2'])?" / " . $RefererInfo['phone2']:"" ?>
										</td>
									</tr>

									<tr>
										<td>Email</td>
										<td>:</td>
										<td><?php echo $RefererInfo['email'] ?></td>
									</tr>
								</table>

          					 <?php }else{ ?>
									
									<i>No Referer !</i>

          					 <?php } ?>
          				</div>
            				
					</div>



					<div class="tab-pane  <?php echo ($active_tab == 'balance')?" active":"" ?>" id="tab-balance">
						<legend style="overflow:hidden">
							<h5 style="float:left">
								Amount Balance : <span style="color:#06F">$ <?php echo number_format($data['Buyer']['amount_balance'], 2) ?></span>
							</h5>
							
							<?php 
								if( $user['access_level'] == 6 ){
							 ?>

								<button style="float:right;"
										id="top-up-button"
										data-code="<?php echo $data['Buyer']['buyer_code'] ?>"
										data-name="<?php echo ucwords($data['Buyer']['full_name']) ?>"
										class="btn btn-linkedin">
									Topup Balance
								</button>

							<?php } ?>
						</legend>
						<table class="table-list">
			                <thead>
				                <tr>
				                    <th width="50px">N<sup>o</sup></th>
				                    <th width="190px;">Date</th>
				                    <th>Description</th>
				                    <th width="100px;" style="text-align:right">Amount</th>
				                </tr>
			                </thead>

			                <tbody>
			                	<?php if( $balanceHistory ){ 
			                			foreach( $balanceHistory as $k => $val ){
			                	?>	
									<tr>
										<td><?php echo $k+1 ?></td>
										<td><?php echo date('d-F-Y h:i:s A', strtotime($val['BuyerTransaction']['created'])) ?></td>
										<td><?php echo $val['BuyerTransaction']['description'] ?></td>
										<td style="text-align:right; font-weight:bold;">
											$ <?php echo number_format($val['BuyerTransaction']['amount'], 2) ?>
										</td>
									</tr>
			                	<?php }
			                			}else{ ?>
									<tr>
										<td colspan="4"><i>Empty history !</i></td>
									</tr>
			                	<?php } ?>
			                </tbody>

			            </table>
					</div>

					<div class="tab-pane  <?php echo ($active_tab == 'transaction')?" active":"" ?>" id="tab-history" 
						 style="min-height:200px;">
						<legend><?php echo __('Transactoins History'); ?>
							<a id="btn-search-clear" href="#" class="btn btn-google" style="float:right">Clear Search</a>

							<a id="btn-search-transaction" href="#" class="btn btn-linkedin" style="float:right; margin-right:10px;">Search Transaction</a>

						</legend>

						 <table class="table-list">
			                <thead>
				                <tr>
				                    <th width="50px">N<sup>o</sup></th>
				                    <th width="170px;">Date</th>
				                    <th>Description</th>
				                    <th>Merchant</th>
				                    <th width="150px;">Payment Type</th>
				                    <th width="100px;" style="text-align:right">Amount</th>
				                    <th width="100px;">Status</th>
				                    <th width="50px;"></th>
				                </tr>
			                </thead>

			                <tbody>

			                <?php if($histories){ 

			                	foreach( $histories as $k => $value ){

			                		$desc = rtrim($value['BuyerTransaction']['description'], ", ");

			                		$status = $value['BuyerTransaction']['status'];
			                		$status_text    = $buy_transaction_status[$status]['status'];
                        			$status_color   = $buy_transaction_status[$status]['color'];

                        			

			                ?>

								<tr style="font-size:13px;<?php echo($value['BuyerTransaction']['type'] == 1)?'color:blue':'' ?>">
									<td style="text-align:center; vertical-align:top"><?php echo $k + 1 ?></td>
									<td style="vertical-align:top"><?php echo date('d-F-Y h:i:s A', strtotime($value['BuyerTransaction']['created'])) ?></td>
									<td style="vertical-align:top"><?php echo $desc ?></td>
									<td style="vertical-align:top">
										<a href="<?php echo $this->Html->url(array('controller' => 'businesses', 'action' => 'view', $value['BuyerTransaction']['business_id'] )); ?>"
											target="_blank">
											<?php echo $value['BusinessDetail']['business_name'] ?>
										</a>
									</td>
									<td style="vertical-align:top"><?php echo $payment_method[$value['BuyerTransaction']['payment_type']] ?></td>

									<td style="vertical-align:top;text-align:right; <?php echo($value['BuyerTransaction']['type'] == 1)?'color:blue':'' ?>">
										$ <?php echo $this->MyHtml->formatNumber($value['BuyerTransaction']['amount']); ?>
									</td>
									<td style="text-align:center; vertical-align:top;">
										<?php if( $value['BuyerTransaction']['type'] == 0 ){ ?>
			                                <span class="label label-<?php echo $status_color ?>"><?php echo $status_text ?></span>
			                            <?php } ?>
									</td>
									<td style="text-align:right; vertical-align:top">
			                            <?php if( $value['BuyerTransaction']['type'] == 0 ){ ?>
			                                <!-- <a  href="#" class="view-detail-transaction btn btn-linkedin" 
			                                    title="View Detail"
			                                    data-id="<?php echo $value['BuyerTransaction']['id'] ?>"
			                                    data-date="<?php echo date("d-F-Y", strtotime($value['BuyerTransaction']['created'])) ?>"
			                                    data-time="<?php echo date("h:i:s A", strtotime($value['BuyerTransaction']['created'])) ?>"
			                                    data-code="<?php echo $value['BuyerTransaction']['code'] ?>"><strong>view</strong></a> -->
			                            <?php } ?>

			                            <?php if( $value['BuyerTransaction']['type'] == 0 ){ ?>
			                                <a  href="<?php echo $this->Html->url(array('action' => 'purchase', $value['BuyerTransaction']['id'])); ?>" class="btn btn-linkedin" 
			                                    title="View Detail"
			                                    ><strong>view</strong></a>
			                            <?php } ?>
			                        </td>

								</tr>

			                <?php 
			                	}
			                }else{ 
			                ?>
								<tr>
									<td colspan="10"><i>No Transaction Found !</i></td>
								</tr>
			                <?php } ?>
			                </tbody>

			            </table>
			            <div class="clearfix" style=""></div>
            
			            <?php if($prev){ ?>
			                <a class="pull-left btn btn-info"  
			                    href="<?php echo $this->Html->url(array('action'=>'view', $data['Buyer']['id'],'transaction', $page - 1)) ?>">Previous</a>
			            <?php } ?>

			            <?php if($next){ ?>
			                <a class="pull-right btn btn-info" 
			                    href="<?php echo $this->Html->url(array('action'=>'view', $data['Buyer']['id'],'transaction', $page + 1)) ?>">Next</a>
			            <?php } ?>


					</div>
				</div>

			</div>
			<!-- / Pie: Content -->

		</div>
		<!-- / Pie -->
		
	</div>

</div>

<div id="search-transaction-modal" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel"><i class="icon-search"></i> Search Transaction</h3>
  </div>

  <!-- Any Info-customization -->
  <form action="<?php echo $this->Html->url(array('action' => 'view', $data['Buyer']['id'], 'transaction' )); ?>" 
      	style="padding: 20px;" method="POST" id="transactionSearchForm"
      	enctype = "multipart/form-data" >

    <div style="width:100%;">

		<fieldset>

			<div style="display:block; clear:both">
				<label for="BuyerTransactionCode" style="float:left; width:100px; padding-top:5px;">Code</label>
				<input 	name="code" 
						placeholder="Code" 
						style="float:left; width:390px;" 
						id="BuyerTransactionCode" 
						maxlength="30" type="text" >
			</div>

			<div style="display:block; clear:both">
				<label style="float:left; width:100px; padding-top:5px;">Purchase Status</label>
				<select name="status" id="purchase-status" style="width:410px;">
					<option value="all">Any Status</option>
					<?php foreach( $buy_transaction_status as $k => $v ){ 
							$selected = (@$states['status'] != 'all' && $states['status'] == $k )?" selected='selected'":"" ;
					?>
						<option value="<?php echo $k ?>" <?php echo $selected ?>><?php echo $v['status'] ?></option>
					<?php } ?>
				</select>
			</div>

			<div style="display:block; clear:both">
				<label for="BuyerTransactionPurchaseDate" style="float:left; width:100px; padding-top:5px;">Purchase Date</label>
				<div id="dealValidFromPicker" class="input-append date datetimepicker" style="float:left">
	                <input data-format="dd-MM-yyyy" type="text" 
	                    name="purchaseDateFrom"
	                    style="width: 140px"
	                    value="<?php echo @$states['purchaseDateFrom'] ?>"
	                    placeholder="From">
	                <span class="add-on">
	                  <i data-time-icon="icon-time" data-date-icon="icon-calendar">
	                  </i>
	                </span>
	            </div>
				<span style="float:left; margin-left:8px; margin-right:7px; padding-top:5px;">TO</span>
				<div id="dealValidFromPicker" class="input-append date datetimepicker" style="float:left">
	                <input data-format="dd-MM-yyyy" type="text" 
	                    name="purchaseDateTo"
	                    style="width: 140px"
	                    value="<?php echo @$states['purchaseDateTo'] ?>"
	                    placeholder="To">
	                <span class="add-on">
	                  <i data-time-icon="icon-time" data-date-icon="icon-calendar">
	                  </i>
	                </span>
	            </div>
				
			</div>

			<div style="display:block; clear:both">
				<label for="BuyerTransactionReceivedDate" style="float:left; width:100px; padding-top:5px;">Received Date</label>
				<div id="dealValidFromPicker" class="input-append date datetimepicker" style="float:left">
	                <input data-format="dd-MM-yyyy" type="text" 
	                    name="receivedDateFrom"
	                    style="width: 140px"
	                    value="<?php echo @$states['receivedDateFrom'] ?>"
	                    placeholder="From">
	                <span class="add-on">
	                  <i data-time-icon="icon-time" data-date-icon="icon-calendar">
	                  </i>
	                </span>
	            </div>
				<span style="float:left; margin-left:8px; margin-right:7px; padding-top:5px;">TO</span>
				<div id="dealValidFromPicker" class="input-append date datetimepicker" style="float:left">
	                <input data-format="dd-MM-yyyy" type="text" 
	                    name="receivedDateTo"
	                    style="width: 140px"
	                    value="<?php echo @$states['receivedDateTo'] ?>"
	                    placeholder="To">
	                <span class="add-on">
	                  <i data-time-icon="icon-time" data-date-icon="icon-calendar">
	                  </i>
	                </span>
	            </div>
			</div>

		</fieldset>

    </div>

    <div class="submit" style="margin-top: 40px; clear: both;">
      <input class="btn btn-primary" type="submit" value="Submit Search">
      <button type="button" class="btn btn-linkedin reset-btn">Reset</button>
      <button class="btn btn-google cancel" data-dismiss='modal' type="button">Close</button>
    </div>
      
  </form>

</div>

<div id="purchase-detail-modal" class="modal hide fade" tabindex="-1" 
	role="dialog" aria-labelledby="myModalLabel" 
	aria-hidden="true"
	style="width:900px !important; left:37%; height:550px;">

	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="myModalLabel"><i class="icon-search"></i> Purchase Detail</h3>
	</div>

	<div class="span6" id="content-show" 
		style="padding-top: 10px; padding-bottom: 20px; height:400px;max-height:400px; width:860px; overflow-y: scroll">
			<div style="float:left">
                <div id="logo" style="width:100px; height:100px; padding-top:18px; margin-bottom:20px;">
                    <img src="#" alt="" style="width: 85px; height: 85px; border: 1px solid #DEDEDE">
                </div>

            </div>
            <div style="float:left; padding-top:15px;">
                <span id="business-name" style="font-weight:bold;"></span><br>
                Phone: <span style="font-size:12px;" id="phone"></span><br>
                Email: <span style="font-size:12px;" id="email"></span><br> 
                Address: <span style="font-size:12px;" id="address"></span>
            </div>

            <div style="float:right;font-size: 12px; margin-right:20px;">
                <div id="qr-code" style="float:left">
                    
                </div>
                <div style="float:left; margin-left: -5px; padding-top:15px;">
                    <b>Date: </b><span id="date"></span><br>
                    <b>Time: </b><span id="time"></span>
                </div>
            </div>

            <div class="clearfix" style="padding-top:20px;"></div>
            
            <table id="detail-table" style="font-size:12px; width:99%;" class="table-list">
                <thead>
                    <tr>
                        <th width="50px;">N<sup>o</sup></th>
                        <th style="text-align:left"><?php echo __('Name') ?></th>
                        <th width="120px;"><?php echo __('Unit Price') ?></th>
                        <th width="100px;"><?php echo __('Qty') ?></th>
                        <th width="150px;"  style="text-align:right;"><?php echo __('Amount') ?></th>
                    </tr>
                </thead>

                <tbody id="item-detial-list">
                    <tr class="row-model">
                        <td class="no" style="text-align:center"></td>
                        <td class="name"></td>
                        <td class="unit-price" style="text-align:center;"></td>
                        <td class="qty" style="text-align:center;"></td>
                        <td class="amount"  style="text-align:right;"></td>
                    </tr>
                </tbody>

                <tfoot>
                    <tr style="border: none !important;">
                        <td style="padding-top:5px;border: none !important;"></td>
                    </tr>
                    <tr style="border: none !important;">
                        <td colspan="3" style="border:none !important;"></td>
                        <td style="text-align:center; font-size: 15px; font-weight:bold; border:none !important; border: 1px solid #CCC; background: #DDD">TOTAL :</td>
                        <td style="text-align:right; font-size:15px; font-weight:bold; border:none !important; border: 1px solid #CCC; background: #DDD" class="total"></td>
                    </tr>

                </tfoot>

            </table>
	</div>

	<div id="loading" style="position: absolute; top:30%; left: 45%; text-align: center; display:none; height:400px;">
        <img src="<?php echo $this->webroot . "/img/ajax-loader.gif" ?>" alt="">
        <h5>Loading...</h5>
    </div>
	
	<div class="span12" style="width:860px; margin-bottom: 20px; text-align:right; position: absolute; bottom:0px;">
		<button class="btn btn-google cancel" data-dismiss='modal' type="button">Close</button>
	</div>

</div> 


<div id="topup-modal" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel"><i class="icon-edit"></i> Top Up</h3>
  </div>

  <form action="<?php echo $this->Html->url(array('action' => 'saveTopup', $data['Buyer']['id'], true )); ?>" style="padding: 20px;" method="POST" >

    <div style="display:block; clear:both">
		<label for="topup-user-search-text" style="float:left; width:120px; padding-top:5px;">Topup Amount ($)</label>
		<input 	name="data[Buyer][amount_balance]" 
				placeholder="Topup Amount" 
				style="float:left; width:370px;" 
				maxlength="11" 
				required="required"
				onkeypress="return numericAndDotOnly(this)" 
				id="topup-amount" type="text">
	</div>

    <div class="submit" style="margin-top: 20px; clear: both; text-align:right;">
      <input class="btn btn-primary" type='submit' id="save-topup"  value="Save Topup">
      <button class="btn btn-google cancel" data-dismiss='modal' type="button">Cancel</button>
    </div>
      
  </form>

</div>



<div id="confirm-modal" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true">

	<div class="modal-header">
	    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	    <h3 id="myModalLabel"><i class="icon-exclamation"></i> Topup Confirm</h3>
	</div>
	
	<form action="#" style="padding: 20px;" method="POST" >
	    <div style="width:100%;">
			<h5>Confrim your topup information:</h5>
			<table>
				<tr>
					<th width="100px" style="text-align:left;">User ID</th>
					<td width="20px;">:</td>
					<td id="code"></td>
				</tr>
				<tr>
					<th style="text-align:left;">User Name</th>
					<td>:</td>
					<td id="name"></td>
				</tr>
				<tr>
					<th style="text-align:left;">Topup Amount</th>
					<td>:</td>
					<td id="amount" style="color:#06F; font-size:20px;"></td>
				</tr>
			</table>

			<div class="submit" style="margin-top: 20px; clear: both; text-align:right;">
			  <input class="btn btn-primary" type='button'  value="Yes" id="yes-confirm"  data-dismiss='modal'>
			  <button class="btn btn-google cancel" data-dismiss='modal' type="button">No</button>
			</div>

		</div>
	</form>

</div>

<div id="warning-modal" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true">

	<div class="modal-header">
	    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	    <h3 id="myModalLabel"><i class="icon-exclamation"></i> Warning !</h3>
	</div>
	
	<form action="#" style="padding: 20px;" method="POST" >
	    <div style="width:100%;">
			<h5></h5>

			<div class="submit" style="margin-top: 20px; clear: both; text-align:right;">
			  <button class="btn btn-google cancel" data-dismiss='modal' type="button">Close</button>
			</div>

		</div>
	</form>

</div>


<div id="pass-alert" class="modal hide fade" tabindex="-1" 
	role="dialog" aria-labelledby="myModalLabel" 
	aria-hidden="true"
	style="z-index:999999">

	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="myModalLabel"><i class="icon-exclamation"></i> Warning !</h3>
	</div>

	<div class="span6" style="padding-top: 10px; padding-bottom: 20px;">
		
		<h5 id="pass-alert"></h5>

		<button class="btn btn-google cancel" data-dismiss='modal' type="button">Close</button>
	</div>

</div> 

<a href="#pass-alert" data-toggle="modal" id="pass-alert"></a>

<script type="text/javascript">

    $(document).ready(function(){

		$('.datetimepicker').datetimepicker();
		$('.datetimepicker').find('input').attr('readonly', 'readonly');
		$('.datetimepicker').find('input').css('cursor', 'default');
	});

	$('#btn-search-transaction').click( function(event){

		event.preventDefault();

		var modal = $('div#search-transaction-modal');

		modal.modal('show');

	});

	$('#btn-search-clear').click( function(){
		var modal = $('div#search-transaction-modal');

		$('.reset-btn').click();
		modal.find('select#purchase-status').val('all');

		$('form#transactionSearchForm').submit();
	});

	$('.reset-btn').click( function(){

	 	var modal = $('div#search-transaction-modal');

	 	modal.find('input[type="text"]').val("");
	 	modal.find('select#purchase-status').val("all");

	});


    var modal 	= $('div#purchase-detail-modal');
    var content = modal.find('#content-show');
    var loading = modal.find('#loading');

	var tbody       = modal.find('tbody#item-detial-list');
    var row_model   = tbody.find('tr.row-model');

    tbody.find('tr.row-model').remove();

	$('.view-detail-transaction').click( function(e){

        e.preventDefault();

        var me      = $(this); 

        tbody.html("");

        var code    = me.attr('data-code');
        var date    = me.attr('data-date');
        var time    = me.attr('data-time');        

        var url     = '<?php echo $this->Html->url(array("plugin" => "administrator", "controller" => "buyers", "action"=>"getTransactionDetailAjax__")) ?>' ;
        var id      = me.attr('data-id');

        content.hide();
        loading.show();

        modal.modal('show');
        
        $.ajax({

            type: 'POST',
            url: url,
            data: { tranID: id },
            dataType: 'json',
            
            success: function (data){

                if( data.status == true ){

                	loading.hide();
                	content.show();

                   	var qr_code = "<img src=\"http://chart.googleapis.com/chart?chs=120x120&cht=qr&chl=" + code + "\" />";
                        modal.find('#qr-code').html(qr_code);
                        modal.find('span#date').text(date);
                        modal.find('span#time').text(time);

                        var result = data.data;
                        // Set Biz Info
                        var biz_logo = result.BusinessDetail.logo;
                        modal.find('#logo').find('img').attr('src', "<?php echo $this->webroot ?>" + biz_logo );
                        modal.find('#business-name').text(result.BusinessDetail.business_name);

                        var tel = result.BusinessDetail.phone1;
                        
                        if( result.BusinessDetail.phone2 != "" ){
                            tel += " / " + result.BusinessDetail.phone2;
                        }

                        modal.find('#phone').text(tel);
                        modal.find('#email').text(result.BusinessDetail.email);

                        var address = result.BusinessDetail.street + ", " + result.BusinessDetail.location + ", " + result.BusinessDetail.city ;
                        modal.find('#address').text(address);

                        var itemDetail = result.TransactionDetail;

                        var total = 0;

                        $.each( itemDetail, function( ind, val ){

                            var price  = val.unit_price;
                            var qty    = val.qty;
                            var amount = price * qty;

                            total += amount;

                            price = parseFloat(price).toFixed(2);
                            amount= parseFloat(amount).toFixed(2);

                            row_model.find(".no").text(ind+1);
                            row_model.find('.name').text(val.ItemDetail.title);
                            row_model.find('.unit-price').text( "$" + price );
                            row_model.find('.qty').text( qty );
                            row_model.find('.amount').text( "$" + amount );

                            tbody.append(row_model.clone());
                        })

                        modal.find('tfoot').find('.total').text("$" + parseFloat(total).toFixed(2) );

                }                   

            },

            error: function( err, msg ){
                console.log(msg);
            }

        })

    });

	$('button#top-up-button').click( function( event ){
		event.preventDefault();

		var me 		= $(this);
		var modal 	= $('div#topup-modal');

		modal.find('form').find('input[type="text"]').val("");
		modal.find('input#save-topup').attr('data-code', me.attr('data-code'));
		modal.find('input#save-topup').attr('data-name', me.attr('data-name'));

		modal.modal('show');
	});
	

	
	$('#save-topup').click(function( event ){
		event.preventDefault();

		var me 		= $(this),
			code  	= me.attr('data-code'),
			name 	= me.attr('data-name');

		var amount  = me.closest('form').find('#topup-amount').val();
		amount = amount.trim();

		var confirm_modal = $('div#confirm-modal');

		if( amount == "" || amount == 0 ){
			$('div#warning-modal').find('h5').text('Please enter topup amount.');
			$('div#warning-modal').modal('show');
			return false;
		}

		confirm_modal.find('#code').text(code);
		confirm_modal.find('#name').text(name);
		confirm_modal.find('#amount').text( "$ " + parseFloat(amount).toFixed(2));

		confirm_modal.modal('show');
		return false;

	});

	$('#yes-confirm').click( function( event ){
		event.preventDefault();

		var form = $('div#topup-modal').find('form');
		form.submit();
	})


	function numericAndDotOnly(elementRef) {        
        var keyCodeEntered = (event.which) ? event.which : (window.event.keyCode) ? window.event.keyCode : -1;

      	if ((keyCodeEntered >= 48) && (keyCodeEntered <= 57)) {
        	return true;
      	}   
      	// '.' decimal point...
      	else if (keyCodeEntered == 46) {
        // Allow only 1 decimal point ('.')...
        	if ((elementRef.value) && (elementRef.value.indexOf('.') >= 0))
          		return false;
        	else
          		return true;
      	}

      	return false;

    };

</script>