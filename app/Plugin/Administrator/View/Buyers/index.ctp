<?php 

	/********************************************************************************
	    File: Buyers Listing
	    Author: PHEA RATTANA
	
	    Confidential ABi Technologies property.
	
	  	Changed History:
	  	Date 					Author				Description
	  	2015/01/May 			PHEA RATTANA		Initial
	*********************************************************************************/

?>

<style>
  table, tr, td{
     vertical-align: top; 
  }
</style>

<div class="users index">
	<div class="row-fluid">		
		<!-- Pie: Box -->
		<div class="span12">
			
			<form action="" method="POST" id="frmBuyerList" >

				<!-- Pie: Top Bar -->
				<div class="top-bar">
					<h3><i class="icon-list"></i> Users List</h3>
				</div>
				<!-- / Pie: Top Bar -->

				<!-- Pie: Content -->
				<div class="well">

				<div class="pull-left">

					<input type="hidden" name='type' value="free" />
					<span>Filter :</span>
					<input type="text" style="width:150px; margin-top:9px;" 
							id="search-text"
							name="user-search-text" placeholder="ID, Name, Email, Phone"
							value="<?php echo @$states['user-search-text'] ?>">

					<span>Status :</span>
					<select name="user-status" id="status" style="width:100px; margin-top:9px;">
						<option value="all">All</option>
						<?php foreach( $status as $k => $val ): 
								$selected = (@$states['user-status'] != 'all' &&  $k == $states['user-status'] )?" selected='selected'":"" ;
						?>
							<option value="<?php echo $k ?>" <?php echo $selected ?>><?php echo $val['status'] ?></option>
						<?php endforeach ?>
					</select>

					<button class="btn btn-skype" type="submit" style="margin-left:10px; margin-right:5px;" ><i class="icon-search"></i> Search</button>
					<button class="btn btn-google" type="button" id="clear-search" ><i class="icon-trash"></i> Clear Search</button>
				</div>

				<button class="btn btn-skype advanced-search-btn pull-right" type="button" style="margin-top:12px; margin-right:5px;" ><i class="icon-search"></i> Advanced Search</button>

				<div class="clearfix"></div>
					
					<table class="table-list">
						<thead>
							<tr>
								<th width="50px;"><?php echo $this->Paginator->sort('buyer_code', 'ID'); ?></th>
								<th width="120px;"><?php echo $this->Paginator->sort('full_name', 'Name'); ?></th>
								<th width="50px;"><?php echo $this->Paginator->sort('gender'); ?></th>
								<th><?php echo $this->Paginator->sort('province_code','Address'); ?></th>
								<th width="250px;"><?php echo $this->Paginator->sort('contact_info'); ?></th>
								<th width="140px;"><?php echo $this->Paginator->sort('created', 'Registered Date'); ?></th>
								<th width="60px"><?php echo $this->Paginator->sort('status'); ?></th>
								<th width="100px;"><?php echo $this->Paginator->sort('amount_balance', 'Balance'); ?></th>
								<th width="70px;"><?php echo __('Actions'); ?></th>
							</tr>
						</thead>
						<tbody>
						<?php if( $data ){ ?>
						<?php foreach ($data as $key => $value ): 

								@$count++;

								$address = ($value['Buyer']['street'])?$value['Buyer']['street'] . ", ":"";
								$address .= ($value['SangkatInfo']['name'])?$value['SangkatInfo']['name'] . ", ":"";
								$address .= ($value['Buyer']['location'])?$value['Buyer']['location'] . ", ":"";
								$address .= $value['City']['city_name'];

						?>
							<tr>
								<td>
									<a 	href="<?php echo $this->Html->url(array('action' => 'view', $value ['Buyer']['id'])); ?>"
										title="View Buyer Detail" > 
										<strong>
											<?php echo $value['Buyer']['buyer_code'] ?></td>
										</strong>
									</a>
								<td>
									<a 	href="<?php echo $this->Html->url(array('action' => 'view', $value ['Buyer']['id'])); ?>"
										title="View Buyer Detail" > 
										<?php echo ucwords($value ['Buyer']['full_name']) ?>
									</a>
								</td>
								<td style="text-align:center"><?php echo ucfirst($value ['Buyer']['gender']) ?></td>
								<td><?php echo $address ?></td>
								<td>
									Phone: <?php echo $value ['Buyer']['phone1']; echo ($value ['Buyer']['phone2'])?" / " . $value ['Buyer']['phone2']:"" ?><br>
									Email: <?php echo $value ['Buyer']['email'] ?>
								</td>
								<td>
									<?php echo date('d-M-Y h:i:s A', strtotime($value['Buyer']['created'])) ?>
								</td>
								<td style="text-align:center">
									<?php 
										$title = "Activate";
										$state = $value['Buyer']['status'];
										$action = 1;
										if( $state == 1 ){
											$title = "Deactivate";
											$action = -1;
										}
									 ?>
									<a 	href="#" title="Click to <?php echo $title ?>" 
										data-title="<?php echo $title ?>"
										data-action="<?php echo $this->Html->url(array('action' => 'changeState', $value ['Buyer']['id'], $action)); ?>"
										class="action-btn">
										<span class="label label-<?php echo $status[$value ['Buyer']['status']]['color'] ?>"><?php echo h($status[$value ['Buyer']['status']]['status']); ?></span>
									</a>
								</td>
								<td style="text-align:right; font-weight:bold; color: #06F">
									<?php echo "$ " . number_format($value['Buyer']['amount_balance'], 2) ?>
								</td>
								<td style="text-align:right">
									<a 	href="<?php echo $this->Html->url(array('action' => 'view', $value ['Buyer']['id'])); ?>"
										title="View Buyer Detail" > 
										<button class="btn btn-skype" type="button">
											<i class="icon icon-eye-open"></i>
										</button>
									</a>

									<?php if($value['Buyer']['status'] == 0 ){ ?>										
										<a 	href="#" 
											class="btn-delete"
											title="Delete Buyer"
											data-action="<?php echo $this->Html->url(array('action' => 'delete', $value ['Buyer']['id'])); ?>"> 
											<button class="btn btn-google" type="button">
												<i class="icon icon-trash"></i>
											</button>
										</a>
									<?php } ?>
								</td>
							</tr>
						<?php endforeach; ?>

						<?php }else{ ?>
								<tr>
									<td colspan="10"><i>No record found !</i></td>
								</tr>
						<?php } ?>
						</tbody>
					</table>

					<p class="page">
						<?php
						echo $this->Paginator->counter(array(
						'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total')
						));
						?>	
					</p>

					<div class="paging">
					<?php
						echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
						echo $this->Paginator->numbers(array('separator' => ''));
						echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
					?>
					</div>
				</div>
				<!-- / Pie: Content -->

			</form>

		</div>
		<!-- / Pie -->
		
	</div>

</div>

<div id="advanced-search-modal" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel"><i class="icon-search"></i> Advanced Search</h3>
  </div>

  <!-- Any Info-customization -->
  <form action="<?php echo $this->Html->url(array('action' => 'index')); ?>" 
      	style="padding: 20px;" method="POST" id="buyerSearchForm"
      	enctype = "multipart/form-data" >

    <div style="width:100%;">

		<fieldset>

			<input type="hidden" name='type' value="advanced" />

			<div style="display:block; clear:both">
				<label for="user-search-text" style="float:left; width:100px; padding-top:5px;">Search Text</label>
				<input 	name="user-search-text" 
						placeholder="ID, Name, Email, Phone" 
						style="float:left; width:390px;" 
						id="user-search-text" type="text" 
						value="<?php echo @$states['user-search-text'] ?>">
			</div>

			<div style="display:block; clear:both;">
				<label style="float:left; width:100px; padding-top:5px;">Province</label>
				<select name="province-code" id="province" style="width:410px;">
					<option value="">Any Province</option>
					<?php foreach( $provinces as $k => $v ){ 
							$selected = (isset($states['province-code']) && $states['province-code'] == $v['Province']['province_code'] )?" selected='selected'":"" ;
					?>
						<option value="<?php echo $v['Province']['province_code'] ?>" <?php echo $selected ?>><?php echo $v['Province']['province_code'] . " - " . $v['Province']['province_name'] ?></option>
					<?php } ?>
				</select>
			</div>
			<div style="display:block; clear:both;">
				<label style="float:left; width:100px; padding-top:5px;">City</label>
				<select name="city-code" id="city" style="width:410px;">
					<option value="">Any City</option>
				</select>
			</div>

			<div style="display:block; clear:both">
				<label for="BuyerTransactionPurchaseDate" style="float:left; width:100px; padding-top:5px;">Registered Date</label>
				<div id="" class="input-append date datetimepicker" style="float:left">
	                <input data-format="dd-MM-yyyy" type="text" 
	                    name="registeredFrom"
	                    style="width: 135px"
	                    value="<?php echo @$states['registeredFrom'] ?>"
	                    placeholder="From">
	                <span class="add-on">
	                  <i data-time-icon="icon-time" data-date-icon="icon-calendar">
	                  </i>
	                </span>
	            </div>
				<span style="float:left; margin-left:8px; margin-right:15px; padding-top:5px;">TO</span>
				<div id="" class="input-append date datetimepicker" style="float:left">
	                <input data-format="dd-MM-yyyy" type="text" 
	                    name="registeredTo"
	                    style="width: 135px"
	                    value="<?php echo @$states['registeredTo'] ?>"
	                    placeholder="To">
	                <span class="add-on">
	                  <i data-time-icon="icon-time" data-date-icon="icon-calendar">
	                  </i>
	                </span>
	            </div>
				
			</div>

			<div style="display:block; clear:both">
				<label style="float:left; width:100px; padding-top:5px;">Status</label>
				<select name="user-status" id="user-status" style="width:410px;">
					<option value="all">Any Status</option>
					<?php foreach( $status as $k => $v ){ 
							$selected = (@$states['user-status'] != 'all' &&  $k == $states['user-status'] )?" selected='selected'":"" ;
					?>
						<option value="<?php echo $k ?>" <?php echo $selected ?>><?php echo $v['status'] ?></option>
					<?php } ?>
				</select>
			</div>

		</fieldset>

    </div>

    <div class="submit" style="margin-top: 40px; clear: both;">
      <input class="btn btn-primary" type="submit" value="Submit Search">
      <button type="button" class="btn btn-linkedin reset-btn">Reset</button
>      <button class="btn btn-google cancel" data-dismiss='modal' type="button">Close</button>
    </div>
      
  </form>

</div> 


<div id="confirm-modal" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel"><i class="icon-question"></i> Are you sure ?</h3>
  </div>

  <!-- Any Info-customization -->
  <form action="#" 
      	style="padding: 20px;" method="POST" 
      	enctype = "multipart/form-data" >

    <div style="width:100%;">

		<h5 id="msg"></h5>

    </div>

    <div class="submit" style="margin-top: 40px; clear: both;">
      <input class="btn btn-primary" type="submit" value="Yes">
      <button class="btn btn-google cancel" data-dismiss='modal' type="button">No</button>
    </div>
      
  </form>

</div> 

<script>
	
	$(document).ready( function(){
		$('.datetimepicker').datetimepicker();
		$('.datetimepicker').find('input').attr('readonly', 'readonly');
		$('.datetimepicker').find('input').css('cursor', 'default');
	})

	$('#clear-search').click( function(event){

		event.preventDefault();

		$('input#search-text').val("");
		$('select#status').val('all');

		$('form#frmBuyerList').submit();
	});


	$('.advanced-search-btn').click(function(event){

		event.preventDefault();
		var modal = $('#advanced-search-modal');

		modal.modal('show');

	});

	$('.reset-btn').click( function(){

	 	var modal = $('div#advanced-search-modal');

	 	modal.find('input[type="text"]').val("");
	 	modal.find('select#user-status').val("all");
	 	modal.find('select').val("").change();

	});

	$("#province").change( function (){

	    var me = $(this);
	    var citySelect    = $("#city");

	    var pro_code = me.val();
	    var url = '<?php echo $this->webroot ?>' + 'administrator/users/get_city_by_province/' + pro_code ;

	    var cityCode = '<?php echo @$states["city-code"] ?>';

	    if( pro_code != '' ){
	      $.ajax({
	              type: 'POST',
	              url: url,
	              data: { pro_code : pro_code },
	              dataType: 'json',
	              
	              beforeSend: function(){
	                  citySelect.html("<option value=''>loading...</option>");
	              },

	              success: function (data){
	                  var data = data.data;

	                  citySelect.html("<option value=''>select a city</option>");
	                  $.each( data, function (ind, val ){
	                    var city = val.City;
	                    var selected = "";

	                    if( cityCode != "" && cityCode == city.city_code ){
	                    	selected = " selected='selected'";
	                    }

	                    citySelect.append("<option " + selected + "value='" + city.city_code + "'>" + city.city_code + " - " + city.city_name + "</option>" )
	                  });


	              },
	              error: function ( err ){
	                console.log(err);
	              }

	          });

	    }else{
	      citySelect.html("<option value=''>select an option</option>");
	    }

  }).trigger('change');
	
	$('.btn-delete').click(function(event){
		event.preventDefault();

		var me = $(this);
		var action = me.attr('data-action');
		
		var confirm_modal = $('div#confirm-modal'),
			form 		  = confirm_modal.find('form');

		form.attr('action', action);

		form.find('#msg').text('Are you sure you want to delete this user?'  );

		confirm_modal.modal('show');

	});


	$('.action-btn').click(function(event){
		event.preventDefault();

		var me 		= $(this),
			action 	= me.attr('data-action'),
			title 	= me.attr('data-title');

		var confirm_modal = $('div#confirm-modal'),
			form 		  = confirm_modal.find('form');

		form.attr('action', action);

		form.find('#msg').text('Are you sure you want to ' + title + ' this user?'  );

		confirm_modal.modal('show');

	})



</script>
