<?php 

	/********************************************************************************
	    File: Buyers Listing
	    Author: PHEA RATTANA
	
	    Confidential ABi Technologies property.
	
	  	Changed History:
	  	Date 					Author				Description
	  	2015/01/May 			PHEA RATTANA		Initial
	*********************************************************************************/

	$status = $data['BuyerTransaction']['status'];

	$buyer_id = $data['BuyerTransaction']['buyer_id'];

 ?>
<style>
  table, tr, td{
     vertical-align: top; 
  }
</style>

<div class="users index">
	<div class="row-fluid">		
		<!-- Pie: Box -->
		<div class="span12">
			
			<form action="" method="POST" id="frmPurchaseTransaction" >

				<!-- Pie: Top Bar -->
				<div class="top-bar">
					<h3><i class="icon-list"></i> Purchase Transaction Detail</h3>
				</div>
				<!-- / Pie: Top Bar -->

				<!-- Pie: Content -->
				<div class="well" style="min-height:300px;">

					<a href="<?php echo $this->Html->url(array('action' => 'index')); ?>" class="btn btn-linkedin">Buyer List</a>
					
					
					<a href="<?php echo $this->Html->url(array('action' => 'view', $buyer_id, 'transaction')); ?>" class="btn btn-skype">Transaction List</a>
					

					<div class="clearfix"></div>

					<?php if($data){ ?>
						<div class="span6" style="margin-left:0px;">
							<legend>Purchase Information</legend>

							<table width="100%">
								<tr>
									<td width="120px;">Purchased Date</td>
									<td width="20px">:</td>
									<td>
										<?php 
											echo date('d-F-Y', strtotime($data['BuyerTransaction']['created']));
											echo " at " . date('h:i:s A', strtotime($data['BuyerTransaction']['created']));
										?>
									</td>
								</tr>

								<tr>
									<td>Description</td>
									<td>:</td>
									<td><?php echo rtrim($data['BuyerTransaction']['description'], ", "); ?></td>
								</tr>

								<tr>
									<td>Payment Method</td>
									<td>:</td>
									<td><?php echo $payment_method[$data['BuyerTransaction']['payment_type']] ?></td>
								</tr>

								<tr>
									<td>Purchase Amount</td>
									<td>:</td>
									<td style="color:blue; font-weight:bold; font-size: 15px;">
										<?php echo "$" . $this->MyHtml->formatNumber($data['BuyerTransaction']['amount']) ?></td>
								</tr>

								<tr>
									<td style="padding-top:10px;"></td>
								</tr>

							</table>

						</div>	

						<div class="span6">
							<legend>Buyer Information</legend>

							<?php 
								$buyerInfo = $data['BuyerInfo'];

								$tel = $buyerInfo['phone1'];
								if( $buyerInfo['phone2']){
									$tel .= " / " . $buyerInfo['phone2'];
								}
							?>

							<table width="100%">
								<tr>
									<td width="120px;">Name</td>
									<td width="20px;">:</td>
									<td><?php echo ucwords($buyerInfo['full_name']) ?></td>
								</tr>
								<tr>
									<td>Phone</td>
									<td>:</td>
									<td><?php echo $tel ?></td>
								</tr>

								<tr>
									<td>Email</td>
									<td>:</td>
									<td><?php echo $buyerInfo['email'] ?></td>
								</tr>
								
								<tr>
									<td>Address</td>
									<td>:</td>
									<td>
										<?php 
											echo ($buyerInfo['street'])?$buyerInfo['street'] . ", ":"";
											echo $buyerInfo['location'] . ", " . $buyerInfo['city_code'];
										 ?>
									</td>
								</tr>

							</table>
						</div>

						<div class="span12" style="margin-left:0px;">

							<legend>Purchase Status</legend>
							
							<div class="span6" style="margin-left:0px;">
								<table width="100%">

									<tr>
										<td width="120px;">Status</td>
										<td width="20px;">:</td>
										
										<td>
											<span class="label label-<?php echo $buy_transaction_status[$status]['color'] ?>">
											<?php echo h($buy_transaction_status[$status]['status']); ?></span>
										</td>
										
									</tr>

									<?php if($status == 1){ ?>
										<tr>
											<td>Note</td>
											<td>:</td>
											<td><?php echo $data['BuyerTransaction']['received_note'] ?></td>
										</tr>

									<?php } ?>
								</table>
							</div>
							
							<div class="span6">

								<?php if($status == 1){ ?>
									<table width="100%">
										
										<tr>
											<td width="120px;">Mark Received By</td>
											<td width="20px;">:</td>										
											<td>
												<?php 
													echo ($data['MarkReceivedBy'])?$data['MarkReceivedBy']['last_name'] . " " . $data['MarkReceivedBy']['first_name']:"";
												?>
											</td>

										</tr>

										<tr>
											<td>Date</td>
											<td>:</td>
											<td>
												<?php 
													echo date('d-F-Y', strtotime($data['BuyerTransaction']['received_date'])); 
													echo " at " . date('h:i:s A', strtotime($data['BuyerTransaction']['received_date']));
												?>
											</td>
										</tr>

									</table>
								<?php } ?>

							</div>

						</div>

						<legend>Purchase Items</legend>

						<?php 
							$purchaseDetail = $data['TransactionDetail'];
						 ?>
							
							<table class="table-list">
								<thead>
									<th width="30px">N<sup>o</sup></th>
									<th>Name</th>
									<th width="120px;">Unit Price</th>
									<th width="120px;">Qty</th>
									<th width="120px;">Amount</th>
								</thead>

								<tbody>
									<?php foreach( $purchaseDetail as $k => $item ){ 
									?>	
										<tr>
											<td style="text-align:center"><?php echo $k + 1 ?></td>
											<td><?php echo $item['ItemDetail']['title'] ?></td>
											<td style="text-align:right">
												<?php echo "$" . $this->MyHtml->formatNumber($item['unit_price']) ?></td>
											<td style="text-align:center">
												<?php echo $item['qty'] ?></td>
											<td style="text-align:right">$<?php echo $this->MyHtml->formatNumber($item['amount']) ?></td>
										</tr>
									<?php } ?>
								</tbody>

							</table>

					<?php }else{ ?>
						<h5><i>No Purchase Found !</i></h5>
					<?php } ?>

				</div>

			</form>

		</div>
		<!-- / Pie -->
		
	</div>

</div>

<div id="mark-received-modal" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true"
  style="padding-bottom: 30px !important; ">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel"><i class="icon-edit"></i> Mark Received</h3>
  </div>

  <!-- Any Info-customization -->
  <form action="<?php echo $this->Html->url(array('action' => 'markReceived')); ?>" 
      style="padding: 20px;" method="POST" id="markCompleteForm"
      enctype = "multipart/form-data" >

    <div style="width:100%;">	
		<input type="hidden" name="data[BuyerTransaction][id]" value="<?php echo $data['BuyerTransaction']['id'] ?>" >

      	<label><b>Receive Note</b></label>
      	<textarea name="data[BuyerTransaction][received_note]" 
      			  style="width:96%" placeholder="Receive Note"
      			  maxLength="300" required="required"></textarea>

    </div>

    <div class="submit" style="margin-top: 20px; clear: both;">
      <input class="btn btn-primary" type="submit" value="Submit">
      <button class="btn btn-google cancel" id="note" data-dismiss='modal' type="button">Cancel</button>
    </div>
      
  </form>

</div> 

<script>
	
	$(document).ready( function(){

		$('.btn-mark-receive').click( function(event){

			event.preventDefault();

			var me = $(this);

			var modal = $('div#mark-received-modal');

			modal.modal('show');

		});

	});

</script>