<?php 
	/********************************************************************************
	    File: Add User
	    Author: PHEA RATTANA
	
	    Confidential ABi Technologies property.
	
	  	Changed History:
	  	Date 					Author				Description
	  	2014/01/05 				PHEA RATTANA		Initial
	*********************************************************************************/

	$status_arr = array();

	foreach ($status as $key => $value) {
		$status_arr[$key] = $value['status'];
	}

?>

<style>
	td{
		font-size: 13px;
		padding: 5px;
		vertical-align: top !important;
	}
	.title {
		width: 160px;
		font-weight: bold;
	}
	
	.col {
		width: 20px;
	}

	.equalto, .required{
		color: red;
		font-size: 12px !important;
		list-style-type: none;
	}

</style>

<div class="users form">
	<div class="row-fluid">		
		<!-- Pie: Box -->
		<div class="span12">

			<!-- Pie: Top Bar -->
			<div class="top-bar">
				<h3><i class="icon-list"></i> User Topup</h3>
			</div>
			<!-- / Pie: Top Bar -->

			<!-- Pie: Content -->
			<form action="" method="POST" id="frmBuyerList" >

				<div class="well">

					<div class="pull-left">
						<input type="hidden" name='type' value="free" />
						<span>Filter :</span>
						<input type="text" style="width:150px; margin-top:9px;" 
								id="search-text"
								name="topup-user-search-text" placeholder="ID, Name, Email, Phone"
								value="<?php echo @$states['topup-user-search-text'] ?>">

						<span>Status :</span>
						<select name="topup-user-status" id="status" style="width:100px; margin-top:9px;">
							<option value="all">All</option>
							<?php foreach( $status as $k => $val ): 
									$selected = (@$states['topup-user-status'] != 'all' &&  $k == $states['topup-user-status'] )?" selected='selected'":"" ;
							?>
								<option value="<?php echo $k ?>" <?php echo $selected ?>><?php echo $val['status'] ?></option>
							<?php endforeach ?>
						</select>

						<button class="btn btn-skype" type="submit" style="margin-left:10px; margin-right:5px;" ><i class="icon-search"></i> Search</button>
						<button class="btn btn-google" type="button" id="clear-search" ><i class="icon-trash"></i> Clear Search</button>
					</div>

					<button class="btn btn-skype advanced-search-btn pull-right" type="button" style="margin-top:12px; margin-right:5px;" ><i class="icon-search"></i> Advanced Search</button>

					<div class="clearfix"></div>

					<table class="table-list">
						<thead>
							<tr>
								<th width="50px;"><?php echo $this->Paginator->sort('buyer_code', 'ID'); ?></th>
								<th width="150px;"><?php echo $this->Paginator->sort('full_name', 'Name'); ?></th>
								<th width="80px;"><?php echo $this->Paginator->sort('gender'); ?></th>
								<th><?php echo $this->Paginator->sort('province_code','Address'); ?></th>
								<th width="250px;"><?php echo $this->Paginator->sort('contact_info'); ?></th>
								<th width="140px;"><?php echo $this->Paginator->sort('amount_balance', 'Balance'); ?></th>
								<th width="60px"><?php echo $this->Paginator->sort('status'); ?></th>
								<th width="100px;"><?php echo __('Actions'); ?></th>
							</tr>
						</thead>
						<tbody>
						<?php if( $data ){ ?>
						<?php foreach ($data as $key => $value): 

								@$count++;

								$address = ($value['Buyer']['street'])?$value['Buyer']['street'] . ", ":"";
								$address .= ($value['SangkatInfo']['name'])?$value['SangkatInfo']['name'] . ", ":"";
								$address .= ($value['Buyer']['location'])?$value['Buyer']['location'] . ", ":"";
								$address .= $value['City']['city_name'];
						?>
							<tr>
								<td>
									<a 	href="<?php echo $this->Html->url(array('action' => 'topupView', $value['Buyer']['id'])); ?>"
										title="View Buyer Detail" > 
										<strong>
											<?php echo $value['Buyer']['buyer_code'] ?></td>
										</strong>
									</a>
								<td>
									<a 	href="<?php echo $this->Html->url(array('action' => 'topupView', $value['Buyer']['id'])); ?>"
										title="View Buyer Detail" > 
										<?php echo ucwords($value['Buyer']['full_name']) ?>
									</a>
								</td>
								<td style="text-align:center"><?php echo ucfirst($value['Buyer']['gender']) ?></td>
								<td><?php echo $address ?></td>
								<td>
									Phone: <?php echo $value['Buyer']['phone1']; echo ($value['Buyer']['phone2'])?" / " . $value['Buyer']['phone2']:"" ?><br>
									Email: <?php echo $value['Buyer']['email'] ?>
								</td>
								<td  style="text-align:right; font-weight: bold;">
									<?php echo "$ " . number_format($value['Buyer']['amount_balance'], 2) ?>
								</td>
								<td style="text-align:center">
									<?php 
										$title = "Activate";
										$state = $value['Buyer']['status'];
										$action = 1;
										if( $state == 1 ){
											$title = "Deactivate";
											$action = -1;
										}
									 ?>
									<span class="label label-<?php echo $status[$value['Buyer']['status']]['color'] ?>"><?php echo h($status[$value['Buyer']['status']]['status']); ?></span>
								</td>
								<td style="text-align:right">
									<a 	href="#"
										title="Topup Now"
										class="top-up-button"
										data-code="<?php echo $value['Buyer']['buyer_code'] ?>"
										data-name="<?php echo ucwords($value['Buyer']['full_name']) ?>"
										data-action="<?php echo $this->Html->url(array('action' => 'saveTopup', $value['Buyer']['id'] )); ?>"> 
										<button class="btn btn-linkedin" type="button">
											<i class="icon icon-edit"></i>
											Topup
										</button>
									</a>
									<a 	href="<?php echo $this->Html->url(array('action' => 'topupView', $value['Buyer']['id'] )); ?>"
										title="View Buyer Detail"  > 
										<button class="btn btn-skype" type="button">
											<i class="icon icon-eye-open"></i>
										</button>
									</a>
								</td>
							</tr>
						<?php endforeach; ?>

						<?php }else{ ?>
								<tr>
									<td colspan="10"><i>No record found !</i></td>
								</tr>
						<?php } ?>
						</tbody>
					</table>

					<p class="page">
						<?php
						echo $this->Paginator->counter(array(
						'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total')
						));
						?>	
					</p>

					<div class="paging">
					<?php
						echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
						echo $this->Paginator->numbers(array('separator' => ''));
						echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
					?>
					</div>
					
				</div>

			</form>
			<!-- / Pie: Content -->

		</div>
		<!-- / Pie -->
		
	</div>

</div>


<div id="advanced-search-modal" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel"><i class="icon-search"></i> Advanced Search</h3>
  </div>

  <!-- Any Info-customization -->
  <form action="<?php echo $this->Html->url(array('action' => 'topup')); ?>" 
      	style="padding: 20px;" method="POST" id="buyerSearchForm"
      	enctype = "multipart/form-data" >

    <div style="width:100%;">

		<fieldset>

			<input type="hidden" name='type' value="advanced" />

			<div style="display:block; clear:both">
				<label for="topup-user-search-text" style="float:left; width:100px; padding-top:5px;">Search Text</label>
				<input 	name="topup-user-search-text" 
						placeholder="ID, Name, Email, Phone" 
						style="float:left; width:390px;" 
						id="topup-user-search-text" type="text" 
						value="<?php echo @$states['user-search-text'] ?>">
			</div>

			<div style="display:block; clear:both;">
				<label style="float:left; width:100px; padding-top:5px;">Province</label>
				<select name="topup-province-code" id="province" style="width:410px;">
					<option value="">Any Province</option>
					<?php foreach( $provinces as $k => $v ){ 
							$selected = (isset($states['topup-province-code']) && $states['province-code'] == $v['Province']['province_code'] )?" selected='selected'":"" ;
					?>
						<option value="<?php echo $v['Province']['province_code'] ?>" <?php echo $selected ?>><?php echo $v['Province']['province_code'] . " - " . $v['Province']['province_name'] ?></option>
					<?php } ?>
				</select>
			</div>
			<div style="display:block; clear:both;">
				<label style="float:left; width:100px; padding-top:5px;">City</label>
				<select name="topup-city-code" id="city" style="width:410px;">
					<option value="">Any City</option>
				</select>
			</div>

			<div style="display:block; clear:both">
				<label for="BuyerTransactionPurchaseDate" style="float:left; width:100px; padding-top:5px;">Registered Date</label>
				<div id="" class="input-append date datetimepicker" style="float:left">
	                <input data-format="dd-MM-yyyy" type="text" 
	                    name="topup-registeredFrom"
	                    style="width: 135px"
	                    value="<?php echo @$states['topup-registeredFrom'] ?>"
	                    placeholder="From">
	                <span class="add-on">
	                  <i data-time-icon="icon-time" data-date-icon="icon-calendar">
	                  </i>
	                </span>
	            </div>
				<span style="float:left; margin-left:8px; margin-right:15px; padding-top:5px;">TO</span>
				<div id="" class="input-append date datetimepicker" style="float:left">
	                <input data-format="dd-MM-yyyy" type="text" 
	                    name="topup-registeredTo"
	                    style="width: 135px"
	                    value="<?php echo @$states['topup-registeredTo'] ?>"
	                    placeholder="To">
	                <span class="add-on">
	                  <i data-time-icon="icon-time" data-date-icon="icon-calendar">
	                  </i>
	                </span>
	            </div>
				
			</div>

			<div style="display:block; clear:both">
				<label style="float:left; width:100px; padding-top:5px;">Status</label>
				<select name="topup-user-status" id="topup-user-status" style="width:410px;">
					<option value="all">Any Status</option>
					<?php foreach( $status as $k => $v ){ 
							$selected = (@$states['user-status'] != 'all' &&  $k == $states['user-status'] )?" selected='selected'":"" ;
					?>
						<option value="<?php echo $k ?>" <?php echo $selected ?>><?php echo $v['status'] ?></option>
					<?php } ?>
				</select>
			</div>

		</fieldset>

    </div>

    <div class="submit" style="margin-top: 40px; clear: both;">
      <input class="btn btn-primary" type="submit" value="Submit Search">
      <button type="button" class="btn btn-linkedin reset-btn">Reset</button>
      <button class="btn btn-google cancel" data-dismiss='modal' type="button">Close</button>
    </div>
      
  </form>

</div> 

<div id="topup-modal" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel"><i class="icon-edit"></i> Top Up</h3>
  </div>

  <form action="#" style="padding: 20px;" method="POST" >

    <div style="display:block; clear:both">
		<label for="topup-user-search-text" style="float:left; width:120px; padding-top:5px;">Topup Amount ($)</label>
		<input 	name="data[Buyer][amount_balance]" 
				placeholder="Topup Amount" 
				style="float:left; width:370px;" 
				maxlength="11" 
				required="required"
				onkeypress="return numericAndDotOnly(this)" 
				id="topup-amount" type="text">
	</div>

    <div class="submit" style="margin-top: 20px; clear: both; text-align:right;">
      <input class="btn btn-primary" type='submit'  value="Save Topup" id="save-topup">
      <button class="btn btn-google cancel" data-dismiss='modal' type="button">Cancel</button>
    </div>
      
  </form>

</div>


<div id="confirm-modal" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true">

	<div class="modal-header">
	    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	    <h3 id="myModalLabel"><i class="icon-exclamation"></i> Topup Confirm</h3>
	</div>
	
	<form action="#" style="padding: 20px;" method="POST" >
	    <div style="width:100%;">
			<h5>Confrim your topup information:</h5>
			<table>
				<tr>
					<th width="100px" style="text-align:left;">User ID</th>
					<td width="20px;">:</td>
					<td id="code"></td>
				</tr>
				<tr>
					<th style="text-align:left;">User Name</th>
					<td>:</td>
					<td id="name"></td>
				</tr>
				<tr>
					<th style="text-align:left;">Topup Amount</th>
					<td>:</td>
					<td id="amount" style="color:#06F; font-size:20px;"></td>
				</tr>
			</table>

			<div class="submit" style="margin-top: 20px; clear: both; text-align:right;">
			  <input class="btn btn-primary" type='button'  value="Yes" id="yes-confirm"  data-dismiss='modal'>
			  <button class="btn btn-google cancel" data-dismiss='modal' type="button">No</button>
			</div>

		</div>
	</form>

</div>

<div id="warning-modal" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true">

	<div class="modal-header">
	    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
	    <h3 id="myModalLabel"><i class="icon-exclamation"></i> Warning !</h3>
	</div>
	
	<form action="#" style="padding: 20px;" method="POST" >
	    <div style="width:100%;">
			<h5></h5>

			<div class="submit" style="margin-top: 20px; clear: both; text-align:right;">
			  <button class="btn btn-google cancel" data-dismiss='modal' type="button">Close</button>
			</div>

		</div>
	</form>

</div>



<script type="text/javascript">

    $(document).ready( function(){

		$('.datetimepicker').datetimepicker();
		$('.datetimepicker').find('input').attr('readonly', 'readonly');
		$('.datetimepicker').find('input').css('cursor', 'default');

	})

	$('#clear-search').click( function(event){

		event.preventDefault();

		$('input#search-text').val("");
		$('select#status').val('1');

		$('form#frmBuyerList').submit();
	});


	$('.advanced-search-btn').click(function(event){

		event.preventDefault();
		var modal = $('#advanced-search-modal');

		modal.modal('show');

	});

	$('.reset-btn').click( function(){

	 	var modal = $('div#advanced-search-modal');

	 	modal.find('input[type="text"]').val("");
	 	modal.find('select#user-status').val("1");
	 	modal.find('select').val("").change();

	});

	$("#province").change( function (){

	    var me = $(this);
	    var citySelect    = $("#city");

	    var pro_code = me.val();
	    var url = '<?php echo $this->webroot ?>' + 'administrator/users/get_city_by_province/' + pro_code ;

	    var cityCode = '<?php echo @$states["city-code"] ?>';

	    if( pro_code != '' ){
	      $.ajax({
	              type: 'POST',
	              url: url,
	              data: { pro_code : pro_code },
	              dataType: 'json',
	              
	              beforeSend: function(){
	                  citySelect.html("<option value=''>loading...</option>");
	              },

	              success: function (data){
	                  var data = data.data;

	                  citySelect.html("<option value=''>select a city</option>");
	                  $.each( data, function (ind, val ){
	                    var city = val.City;
	                    var selected = "";

	                    if( cityCode != "" && cityCode == city.city_code ){
	                    	selected = " selected='selected'";
	                    }

	                    citySelect.append("<option " + selected + "value='" + city.city_code + "'>" + city.city_code + " - " + city.city_name + "</option>" )
	                  });


	              },
	              error: function ( err ){
	                console.log(err);
	              }

	          });

	    }else{
	      citySelect.html("<option value=''>select an option</option>");
	    }

  }).trigger('change');
	
	$('.btn-delete').click(function(event){
		event.preventDefault();

		var me = $(this);
		var action = me.attr('data-action');
		
		var confirm_modal = $('div#confirm-modal'),
			form 		  = confirm_modal.find('form');

		form.attr('action', action);

		form.find('#msg').text('Are you sure you want to delete this user?'  );

		confirm_modal.modal('show');

	});

	$('.action-btn').click(function(event){
		event.preventDefault();

		var me 		= $(this),
			action 	= me.attr('data-action'),
			title 	= me.attr('data-title');

		var confirm_modal = $('div#confirm-modal'),
			form 		  = confirm_modal.find('form');

		form.attr('action', action);

		form.find('#msg').text('Are you sure you want to ' + title + ' this user?'  );

		confirm_modal.modal('show');

	});

	function numericAndDotOnly(elementRef) {        
        var keyCodeEntered = (event.which) ? event.which : (window.event.keyCode) ? window.event.keyCode : -1;

      	if ((keyCodeEntered >= 48) && (keyCodeEntered <= 57)) {
        	return true;
      	}   
      	// '.' decimal point...
      	else if (keyCodeEntered == 46) {
        // Allow only 1 decimal point ('.')...
        	if ((elementRef.value) && (elementRef.value.indexOf('.') >= 0))
          		return false;
        	else
          		return true;
      	}

      	return false;

    }


	$('.top-up-button').click( function( event ){
		event.preventDefault();

		var me 		= $(this),
			action 	= me.attr('data-action') ;
		var modal = $('div#topup-modal');

		modal.find('form').attr('action', action);
		modal.find('form').find('input[type="text"]').val("");
		modal.find('input#save-topup').attr('data-code', me.attr('data-code'));
		modal.find('input#save-topup').attr('data-name', me.attr('data-name'));
		modal.modal('show');
	})


	$('#save-topup').click(function( event ){
		event.preventDefault();

		var me 		= $(this),
			code  	= me.attr('data-code'),
			name 	= me.attr('data-name');

		var amount  = me.closest('form').find('#topup-amount').val();
		amount = amount.trim();

		var confirm_modal = $('div#confirm-modal');

		if( amount == "" || amount == 0 ){
			$('div#warning-modal').find('h5').text('Please enter topup amount.');
			$('div#warning-modal').modal('show');
			return false;
		}

		confirm_modal.find('#code').text(code);
		confirm_modal.find('#name').text(name);
		confirm_modal.find('#amount').text( "$ " + parseFloat(amount).toFixed(2));

		confirm_modal.modal('show');
		return false;

	});

	$('#yes-confirm').click( function( event ){
		event.preventDefault();

		var form = $('div#topup-modal').find('form');
		form.submit();
	})


</script>