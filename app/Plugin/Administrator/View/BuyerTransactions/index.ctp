<?php 

	/********************************************************************************
	    File: Buyers Listing
	    Author: PHEA RATTANA
	
	    Confidential ABi Technologies property.
	
	  	Changed History:
	  	Date 					Author				Description
	  	2015/01/May 			PHEA RATTANA		Initial
	*********************************************************************************/
	
 ?>
 
<style>
  table, tr, td{
     vertical-align: top; 
  }
</style>

<div class="users index">
	<div class="row-fluid">		
		<!-- Pie: Box -->
		<div class="span12">
			
			<form action="#" method="POST" id="frmPurchaseTransaction" >

				<!-- Pie: Top Bar -->
				<div class="top-bar">
					<h3><i class="icon-list"></i> Purchase Transactions</h3>
				</div>
				<!-- / Pie: Top Bar -->

				<!-- Pie: Content -->
				<div class="well" style="min-height:300px;">

					<legend>

						Unredeemed Purchases : <font style="color: blue"><?php echo $totalPendingPurchase ?></font>

						<a id="btn-search-clear" href="#" class="btn btn-google" style="float:right">Clear Search</a>

						<a id="btn-search-transaction" href="#" class="btn btn-linkedin" style="float:right; margin-right:10px;">Search Transaction</a>

					</legend>

					<table class="table-list">

						<thead>
							<tr>
								<th width="30px;">N<sup>o</sup></th>
								<th width="150px;"><?php echo $this->Paginator->sort('created', 'Date'); ?></th>
								<th><?php echo $this->Paginator->sort('description'); ?></th>
								<th><?php echo $this->Paginator->sort('BusinessDetail.business_name', 'Merchant') ?></th>
								<th width="120px;"><?php echo $this->Paginator->sort('amount'); ?></th>
								<th width="80px;"><?php echo $this->Paginator->sort('status'); ?></th>
								<th width="50px;">Action</th>
							</tr>
						</thead>

						<tbody>							
							<?php if( $data ){ ?>
							
								<?php foreach( $data as $key => $val ): 

										$desc 	= rtrim( $val['BuyerTransaction']['description'] , ", ");
										$status = $val['BuyerTransaction']['status'];
								?>
									<tr <?php echo($val['BuyerTransaction']['type'] == 1 )?" style='color:blue'":"" ?>>
										<td style="text-align:center"><?php echo $key + 1 ?></td>
										<td><?php echo date('d-M-Y h:i:s A', strtotime($val['BuyerTransaction']['created'])) ?></td>
										<td><?php echo $desc ?></td>
										<td>
											<a href="<?php echo $this->Html->url(array( 'controller' => 'businesses', 'action' => 'view', $val['BuyerTransaction']['business_id'])); ?>" target="_blank">
												<?php echo $val['BusinessDetail']['business_name'] ?>
											</a>
										</td>
										<td style="text-align:right">
											$<?php echo number_format($val['BuyerTransaction']['amount'],2) ?>
										</td>

										<td style="text-align:center">
											<?php if($val['BuyerTransaction']['type'] == 0 ): ?>
												<span class="label label-<?php echo $purchase_status[$status]['color'] ?>"><?php echo h($purchase_status[$status]['status']); ?></span>
											<?php endif ?>
										</td>

										<td style="text-align:center">
											<?php if($val['BuyerTransaction']['type'] == 0 ): ?>
											<a 	href="<?php echo $this->Html->url(array('action' => 'view', $val['BuyerTransaction']['id'])); ?>" 
												class="btn-view-detail" > 
												<button class="btn btn-skype" type="button">View</button>
											</a>
											<?php endif ?>
										</td>

									</tr>
								<?php endforeach ?>
							<?php }else{ ?>
								<tr>
									<td colspan="10"><i>There is no purchase found !</i></td>
								</tr>
							<?php } ?>

						</tbody>

					</table>

					<p class="page">
						<?php
						echo $this->Paginator->counter(array(
						'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total')
						));
						?>	
					</p>

					<div class="paging">
					<?php
						echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
						echo $this->Paginator->numbers(array('separator' => ''));
						echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
					?>
					</div>
				</div>

			</form>

		</div>
		<!-- / Pie -->
		
	</div>

</div>

<div id="search-transaction-modal" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel"><i class="icon-search"></i> Search Transaction</h3>
  </div>

  <!-- Any Info-customization -->
  <form action="<?php echo $this->Html->url(array('action' => 'index')); ?>" 
      	style="padding: 20px;" method="POST" id="transactionSearchForm"
      	enctype = "multipart/form-data" >

    <div style="width:100%;">

		<fieldset>

			<div style="display:block; clear:both">
				<label for="BuyerTransactionCode" style="float:left; width:100px; padding-top:5px;">Code</label>
				<input 	name="code" 
						placeholder="Code" 
						style="float:left; width:390px;" 
						id="BuyerTransactionCode" 
						maxlength="30" type="text" >
			</div>


			<div style="display:block; clear:both">
				
				<label style="float:left; width:100px; padding-top:5px;">Merchant</label>
				
				<select name="business_id" id="merchant" style="width:410px;">
					<option value="">All</option>
					<?php foreach( $bizData as $k => $v ){ 
							$selected = ( @$states['business_id'] && $states['business_id'] == $v['Business']['id'] )?" selected='selected'":"" ;
					?>
						<option value="<?php echo $v['Business']['id'] ?>" <?php echo $selected ?>><?php echo $v['Business']['business_name'] ?></option>
					<?php } ?>
				</select>

			</div>

			<div style="display:block; clear:both">
				<label style="float:left; width:100px; padding-top:5px;">Purchase Status</label>
				<select name="status" id="purchase-status" style="width:410px;">
					<option value="all">Any Status</option>
					<?php foreach( $purchase_status as $k => $v ){ 
							$selected = (@$states['status'] != 'all' && $states['status'] == $k )?" selected='selected'":"" ;
					?>
						<option value="<?php echo $k ?>" <?php echo $selected ?>><?php echo $v['status'] ?></option>
					<?php } ?>
				</select>
			</div>

			<div style="display:block; clear:both">
				<label for="BuyerTransactionPurchaseDate" style="float:left; width:100px; padding-top:5px;">Purchase Date</label>
				<div id="dealValidFromPicker" class="input-append date datetimepicker" style="float:left">
	                <input data-format="dd-MM-yyyy" type="text" 
	                    name="purchaseDateFrom"
	                    style="width: 135px"
	                    value="<?php echo @$states['purchaseDateFrom'] ?>"
	                    placeholder="From">
	                <span class="add-on">
	                  <i data-time-icon="icon-time" data-date-icon="icon-calendar">
	                  </i>
	                </span>
	            </div>
				<span style="float:left; margin-left:8px; margin-right:7px; padding-top:5px;">TO</span>
				<div id="dealValidFromPicker" class="input-append date datetimepicker" style="float:left">
	                <input data-format="dd-MM-yyyy" type="text" 
	                    name="purchaseDateTo"
	                    style="width: 135px"
	                    value="<?php echo @$states['purchaseDateTo'] ?>"
	                    placeholder="To">
	                <span class="add-on">
	                  <i data-time-icon="icon-time" data-date-icon="icon-calendar">
	                  </i>
	                </span>
	            </div>
				
			</div>

			<div style="display:block; clear:both">
				<label for="BuyerTransactionReceivedDate" style="float:left; width:100px; padding-top:5px;">Received Date</label>
				<div id="dealValidFromPicker" class="input-append date datetimepicker" style="float:left">
	                <input data-format="dd-MM-yyyy" type="text" 
	                    name="receivedDateFrom"
	                    style="width: 135px"
	                    value="<?php echo @$states['receivedDateFrom'] ?>"
	                    placeholder="From">
	                <span class="add-on">
	                  <i data-time-icon="icon-time" data-date-icon="icon-calendar">
	                  </i>
	                </span>
	            </div>
				<span style="float:left; margin-left:8px; margin-right:7px; padding-top:5px;">TO</span>
				<div id="dealValidFromPicker" class="input-append date datetimepicker" style="float:left">
	                <input data-format="dd-MM-yyyy" type="text" 
	                    name="receivedDateTo"
	                    style="width: 135px"
	                    value="<?php echo @$states['receivedDateTo'] ?>"
	                    placeholder="To">
	                <span class="add-on">
	                  <i data-time-icon="icon-time" data-date-icon="icon-calendar">
	                  </i>
	                </span>
	            </div>
			</div>

		</fieldset>

    </div>

    <div class="submit" style="margin-top: 40px; clear: both;">
      <input class="btn btn-primary" type="submit" value="Submit Search">
      <button type="button" class="btn btn-linkedin reset-btn">Reset</button>
      <button class="btn btn-google cancel" data-dismiss='modal' type="button">Close</button>
    </div>
      
  </form>

</div> 

<script>

	$(document).ready( function(){
		$('.datetimepicker').datetimepicker();
		$('.datetimepicker').find('input').attr('readonly', 'readonly');
		$('.datetimepicker').find('input').css('cursor', 'default');
	})

	$('#clear-search').click( function(){
		$('input#search-text').val("");
		$('select#status').val('all');

		$('form#frmPurchaseTransaction').submit();
	})

	$('#btn-search-transaction').click( function(event){

		event.preventDefault();

		var modal = $('div#search-transaction-modal');

		modal.modal('show');

	});

	$('#btn-search-clear').click( function(){
		var modal = $('div#search-transaction-modal');

		$('.reset-btn').click();
		modal.find('select#purchase-status').val(0);

		$('form#transactionSearchForm').submit();
	});

	$('.reset-btn').click( function(){

	 	var modal = $('div#search-transaction-modal');

	 	modal.find('input[type="text"]').val("");
	 	modal.find('select#purchase-status').val("all");
	 	modal.find('select#merchant').val("");

	})

</script>