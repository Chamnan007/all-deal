<?php 
     
     /********************************************************************************

      File Name: index.ctp ( Deal of the Day Listing )
      Description: Listing All Deals of the Day

      Powered By: ABi Investment Group Co., Ltd,

      Changed History:
          Date                Author              Description
          2014/06/30          Rattana Phea        Initial Version

      *********************************************************************************/ 

          
  $goods_category = array();

  if( !empty($goods) ){
    foreach( $goods as $key => $value ){
      $goods_category[$value['GoodsCategory']['id']] = urldecode($value['GoodsCategory']['category']);
    }
  }

 ?>

<style>
  tr{
    vertical-align: top;
  } 

  .deal-image{

      width:223px; 
      height: 133px; 
      border: 1px solid #CCC; 
      padding:3px; 
      float:left; 
      margin-right:20px;
      margin-top:10px;
      margin-bottom: 10px;
      position: relative;

  }

  .deal-image:hover > .remove-img{
      display: block;    
  }

  .deal-image .remove-img{
      position: absolute;
      display: none;
      background: rgb(255, 101, 81);
      padding: 5px 5px 5px 7px ;
      text-align: center;
      border: rgb(255, 101, 81);
      border-radius: 3px ;
      left:44%;
      top:40%;
  }
  .deal-image .remove-img i{
      color:white;
      font-size:15px;
  }
  .deal-image .remove-img:hover{
      background: rgb(217, 64, 44);
      cursor: pointer;
  }


  .deal-image-upload{
      background: #DDD;
      width:223px; 
      height: 133px; 
      border: 1px solid #CCC; 
      padding:3px; 
      float:left; 
      margin-right:20px;
      margin-top:10px;
      margin-bottom: 10px;
      position: relative;
  }
  .deal-image-upload .upload{
      position: absolute;
      background: rgb(54, 132, 252);
      padding: 5px 5px 5px 7px ;
      text-align: center;
      border: rgb(54, 132, 252);
      border-radius: 3px ;
      left:40%;
      top:40%;
  }
  .deal-image-upload .upload i{
      color:white;
      font-size:15px;
  }
  .deal-image-upload .upload:hover{
      background: rgb(14, 93, 216);
      cursor: pointer;
  }

  #deal-action-menu{
    position:absolute; 
    width: 200px; 
    height: auto;
    min-height: 50px; 
    background: #FFF;
    top: 33px;
    right:0px;

    -webkit-box-shadow: 2px 6px 31px -4px rgba(87,87,87,1);
    -moz-box-shadow: 2px 6px 31px -4px rgba(87,87,87,1);
    box-shadow: 2px 6px 31px -4px rgba(87,87,87,1);

    display: none;
    
  }

  #deal-action-menu ul{
    width: 100%;
    margin: 0px !important;
    padding-top:10px;
    padding-bottom:10px;
  }

  #deal-action-menu ul li{
    list-style-type: none;
    padding: 0px 10px 2px 10px;
  }
  #deal-action-menu ul a li{
    color: black;
    font-size: 13px;
  }

  #deal-action-menu ul li:hover {
      background: #CCC !important;
      cursor: pointer;
  }

</style>

<div class="business form">
  
  <div class="row-fluid">

    <div class="top-bar">
      <h3><i class="icon-list"></i> Deal of the Day</h3>
    </div>

    <div class="well" style="padding-bottom: 100px;">

      <!-- <a href="<?php echo $this->Html->url( array('action' => 'add')); ?>" data-toggle="modal"> 
        <button class="btn btn-linkedin" type="button"><i class="icon-plus"></i> Create New Deal</button>
      </a> -->

      <a class="btn btn-skype pull-left" href="#" id="filter-btn">
        <i class="icon icon-search"></i>
        Fitler
      </a>

      <a class="btn btn-google pull-left" href="#" id="clear-search-btn" style="margin-right:0px;">
        <i class="icon icon-trash"></i>
        Clear Filter
      </a>

      <div style="position: relative;" class="pull-right">
        <a class="btn btn-skype pull-right" href="#" id="deal-actions" style="margin-right:0px;">
          <i class="icon icon-edit"></i>
          Actions
        </a>

        <div id="deal-action-menu">

           <ul>

            <a href="#"
               data-action="remove"
               data-url="" 
               data-title="Remove from Deal of the Day List"
               id="remove-dod">
              <li> Remove from Deal of the Day</li>
            </a>

           </ul>

        </div>

      </div>

      <div style="clear: both; padding-top: 20px;" ></div>

      <table class="table-list" >
        <tr>
          <th width="110px">Available Date</th>
          <th width="80px">Image</th>
          <th>Deal Information</th>
          <th width="200px">Deal Validity</th>
          <th width="200px">Setting Info</th>
          <th width="30">Action</th>
          <th width="20px">
            <input type="checkbox" value="" id="check-all"/>
          </th>
        </tr>

        <tbody>

        <?php 

          if($data){

          foreach( $data as $key => $value ){ 

            $index = 0;

            $bizInfo  = $value['DealDetail']['BusinessInfo'];
            $dealInfo = $value['DealDetail'];

            $split = end(explode("/", $dealInfo['image']));
            $thumb_img =  "img/deals/thumbs/" . $split ;

            $other_images = $dealInfo['other_images'];

            if( $other_images ){
                
                $other_images = json_decode($other_images);
                foreach( $other_images as $k => $img ){

                  if( $img != "img/deals/default.jpg" ){
                    $index ++;
                    $spt    = end(explode("/", $img ));
                    $thumb  = "img/deals/thumbs/" . $spt;

                    $data_images[$dealInfo['id']][$index]['img'] = $img;
                    $data_images[$dealInfo['id']][$index]['thumb'] = $thumb;
                  }
                }

            }else if($dealInfo['image'] != "img/deals/default.jpg" ) {

              $data_images[$dealInfo['id']][$index]['img']   = $dealInfo['image'];
              $data_images[$dealInfo['id']][$index]['thumb']  = $thumb_img;
            }

            $selected = @$data_images[$dealInfo['id']][array_rand($data_images[$dealInfo['id']])];

            if( $selected ){ 
                $selected_img   = $selected['img'];
                $selected_thumb = $selected['thumb']; 
            }else{
                $selected_img   = "img/deals/default.jpg" ;
                $selected_thumb = "img/deals/thumbs/default.jpg" ;
            }


            $dealCategories = $value['DealDetail']['goods_category'];
            $dealCategories = json_decode($dealCategories);

            $cate_display = "";
            foreach( $dealCategories as $val ){
                if( isset($goods_category[$val]) ){
                  $cate_display .= $goods_category[$val] . ", ";
                }else{
                  $cate_display .= $val . ", ";
                }
            }

            $cate_display = rtrim($cate_display, ", ");

            $last_minute = "";
            if( $value['Deal']['is_last_minute_deal']){
               $last_minute = "Yes";
            }


          ?>
          <tr>
            <td><strong><?php echo date('d-F-Y', strtotime($value['DealOfTheDay']['available_date'])) ?></strong></td>
            
            <td>
              <a href="<?php echo $this->webroot . $selected_img ?>"  rel="shadowbox">
                <img src="<?php echo $this->webroot . $selected_thumb ?>" alt=""
                 style="max-width:90px; max-height:80px; height: auto; padding: 3px; border: 1px solid #CCC;" >
              </a>
            </td>

            <td>
              Merhant: <a href="<?php echo $this->Html->url( array('controller' => 'businesses', 'action' => 'view', $bizInfo['id'], 'plugin' => 'administrator' )); ?>" 
                          target="_blank"
                          title="click to see detail information"><strong><?php echo $bizInfo['business_name'] ?></strong></a><br>
              Deal Code:  <strong><?php echo ($value['DealDetail']['deal_code']) ?></strong><br>
              Deal Title: 
                <a href="<?php echo $this->Html->url( array('controller' => 'deals', 'action' => 'view', $dealInfo['id'], 'plugin' => 'administrator' )); ?>" 
                          target="_blank"
                          title="click to see detail information">
                <strong><?php echo urldecode($dealInfo['title']) ?></strong>
              </a><br>
                <strong>Deal Category : </strong><?php echo $cate_display ?>
            </td>
            <td>
              <strong>From :</strong> <?php echo date( 'd-F-Y', strtotime($dealInfo['valid_from']) ) ?> at <?php echo date( 'h:i:s A', strtotime($dealInfo['valid_from']) ) ?><br>
              <strong>To :</strong> <?php echo date( 'd-F-Y', strtotime($dealInfo['valid_to']) ) ?> at <?php echo date( 'h:i:s A', strtotime($dealInfo['valid_to']) ) ?>
            </td>

            <td>
              <strong>Set Date : </strong><?php echo date('d-M-Y | h:i:s A', strtotime($value['DealOfTheDay']['created'])) ?><br>
              <strong>Set By : </strong><?php echo $value['CreatedBy']['last_name'] . " " . $value['CreatedBy']['first_name'] ?>
            </td>

            <td class="actions" style="vertical-align: top; text-align: center">

              <a href="<?php echo $this->Html->url( array('action' => 'view', $dealInfo['id'])); ?>" 
                data-toggle="modal"
                class="btn btn-linkedin"
                title="View Deal Detail"
                style="margin-left:0px;"> 
                <i class="icon icon-eye-open" style="padding-right:0px;"></i>
              </a>

            </td>
            <td style="text-align:center">
                <input type="checkbox" value="<?php echo $value['DealOfTheDay']['id'] ?>" name="checked-id[]" class="chk-checked"/>
            </td>
          </tr>
        <?php 
          } 

        }else{

        ?>
          <tr>
            <td colspan="10"><i>No deal found !</i></td>
          </tr>

        <?php
          }
        ?>
        </tbody>

       </table>

       <p class="page">
          <?php
          echo $this->Paginator->counter(array(
          'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total')
          ));
          ?>  
        </p>

        <div class="paging">
        <?php
          echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
          echo $this->Paginator->numbers(array('separator' => ''));
          echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
        ?>
        </div>

    </div>
  </div>

</div>


<div id="search-deal-modal" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel"><i class="icon-search"></i> Search Filter</h3>
  </div>

  <!-- Any Info-customization -->
  <form action="<?php echo $this->Html->url(array('action' => 'dod')); ?>" 
        style="padding: 20px;" method="POST" id="searchDealForm"
        enctype = "multipart/form-data" >

    <div style="width:100%;">

    <fieldset>

      <div style="clear:both; ">
        <label style="float:left; width:100px; padding-top:5px;">Merchant</label>
        <select name="business-id" id="business-id" style="width:410px;">
          <option value="">Any merchant</option>
          <?php foreach( $businesses as $key => $val ){ 

                $selected = (isset($dod_states['business-id']) && $dod_states['business-id'] == $val['Business']['id'])?" selected='selected'":"" ;
          ?>
            <option value="<?php echo $val['Business']['id'] ?>" <?php echo $selected ?>><?php echo $val['Business']['business_name'] ?></option>
          <?php } ?>
        </select>
      </div>

      <div style="clear:both; ">
        <label style="float:left; width:100px; padding-top:5px;">Deal Code</label>
        <input type="text" name="deal-code" id="deal-code" placeholder="Deal Code" style="width:385px;"
              value="<?php echo strtoupper($dod_states['deal-code']) ?>">
      </div>

      <div style="clear:both; ">
        <label style="float:left; width:100px; padding-top:5px;">Deal Category</label>
        <select name="deal-category" id="deal-category" style="width:410px;">
          <option value="">Any category</option>
          <?php foreach( $goods as $k => $v ){ 
              $selected = ( isset($dod_states['deal-category']) && $dod_states['deal-category'] == $v['GoodsCategory']['id'] )?" selected='selected'":"" ;
          ?>
            <option value="<?php echo $v['GoodsCategory']['id'] ?>" <?php echo $selected ?>><?php echo $v['GoodsCategory']['category'] ?></option>
          <?php } ?>
        </select>
      </div>

      <div style="display:block; clear:both">
        <label for="BuyerTransactionReceivedDate" style="float:left; width:100px; padding-top:5px;">Availabel Date</label>
        <div id="dealValidFromPicker" class="input-append date datetimepicker" style="float:left">
            <input data-format="dd-MM-yyyy" type="text" 
                name="available_date"
                style="width: 140px"
                value="<?php echo (@$dod_states['available_date'])?date('d-m-Y' ,strtotime($dod_states['available_date'])):'' ?>"
                placeholder="Availabel Date">
            <span class="add-on">
              <i data-time-icon="icon-time" data-date-icon="icon-calendar">
              </i>
            </span>
        </div>
      </div>

    </fieldset>

    </div>

    <div class="submit" style="margin-top: 40px; clear: both;">
      <input class="btn btn-primary" type="submit" value="Submit Search">
      <button type="button" class="btn btn-linkedin reset-btn">Reset</button>
      <button class="btn btn-google cancel" data-dismiss='modal' type="button">Close</button>
    </div>
      
  </form>

</div> 

<div id="remove-dod-modal" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel" style="font-size:20px; ">Remove From Deal of the Day List</h3>
  </div>

  <!-- Any Info-customization -->
  <form action="<?php echo $this->Html->url(array('action' => 'removeDodFromList')); ?>" 
      style="padding: 20px;" method="POST" class="form-horizontal">
      
      <div class="dod-ids">
        
      </div>      
      <h5>Are you sure you want to remove from Deal of the Day list?</h5>

      <div class="clearfix"></div>
      <label for="total-selected-deal" style="float:left; width:100%; padding-top:5px;">
        Total Selected Deal : 
        <span style="font-weight:bold; font-size:18px;" id="total-deal">10</span>
      </label>      
    
      <div class="submit" style="padding-top: 20px; clear: both;">
        <button class="btn btn-google" data-dismiss='modal' type="button">Cancel</button>
        <button class="btn btn-primary" >Submit</button>
      </div>
      
  </form>

</div> 

<div id="warning-modal" class="modal hide fade" tabindex="-1" 
  role="dialog" aria-labelledby="myModalLabel" 
  aria-hidden="true">

  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel">Warning !</h3>
  </div>

  <!-- Any Info-customization -->
  <form action="" 
      style="padding: 20px;" method="POST" class="form-horizontal">
    
    <h5>Please select any deal.</h5>
    
    <div class="submit" style="padding-top: 20px; clear: both; text-align:right">
      <button class="btn btn-google" data-dismiss='modal' type="button">Close</button>
    </div>
      
  </form>

</div> 


<script>

  $(document).ready(function(){

    $('.datetimepicker').datetimepicker();
    $('.datetimepicker').find('input').attr('readonly', 'readonly');
    $('.datetimepicker').find('input').css('cursor', 'default');

    $('#expand-valid-to').removeAttr('readonly');


    function msieversion() {

            var ua = window.navigator.userAgent;
            var msie = ua.indexOf("MSIE ");
            var ieversion;

            if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer, return version number
                return parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)));
            else                 // If another browser, return 0
                return false;
    }

    var checkIEBrowser = msieversion();

    Shadowbox.init();

    $(".deleteDeal").click( function(){

      var id = $(this).data('id');
      var action = '<?php echo $this->webroot ?>' + 'member/deals/deleteDeal/' + id ;
      $("#dealDeleteForm").attr('action', action) ;

    });

    $(".change_image_deal").click( function(){

      var me    = $(this),
        deal_id = me.data('id');

      var action = '<?php echo $this->webroot ?>' + 'member/deals/changeDealImage/' + deal_id ;

      var form = $("#changeImageDeal");

      form.attr('action', action);
    })

    if( checkIEBrowser ){

       $("#save_upload").click( function(){

          var img = $('#deal_image_change').val();

          if( img == "" ){
            alert("Please select new deal image"); return false;
          }

      }) 
    };

    // ============================================
    var div_image = $('.deal-image');
    $('.deal-image').remove();
    var div_upload = $('div.deal-image-upload');

    // Deal Images View
    $('.deal_images').click ( function(event){

        event.preventDefault();

        $('#deal-image-container').find('.deal-image').remove();

        var image_modal     = $('div#deal_images');
        var deal_id         = $(this).data('id');
        
        var getDealImageURL__ = "<?php echo $this->Html->url(array('action' => 'getDealImagesAjax__')); ?>";
        getDealImageURL__ += "/" + deal_id ;

        image_modal.modal('show');
        var root = '<?php echo $this->webroot ?>';

        var loading_img = $('div#loading-img');
        var time = new Date().getTime();

        loading_img.show();
        $('div.deal-image-upload').hide();

        if( deal_id ){
            $.ajax({

                url: getDealImageURL__,
                type: "POST",
                data: {},
                async: false,
                dataType: 'json',

                success: function(data){
                    if( data.status == true ){

                        setTimeout( function(){
                            loading_img.hide();

                            var images = data.deal_images;

                            $.each( images, function(ind, val){
                                time += 300;

                                var img_name = val.split("/");
                                img_name = img_name[img_name.length - 1];

                                var img = root + "img/deals/" + img_name;
                                var thb = root + "img/deals/thumbs/" + img_name;

                                div_image.attr( 'data-id' , deal_id + "_" + time );
                                div_image.html('<img alt="" src="' + thb + '"><div title="Remove Image" class="remove-img" data-img="'+ img +'" data-thumb="'+ thb +'" data-dealid="' + deal_id + '"><i class="icon icon-trash"></i></div>');

                                $('#deal-image-container').prepend(div_image.clone());
                            });

                            checkUploadButton(deal_id);

                        }, 1000);
                    }
                },
            
                error: function(err){
                    
                }

            });
        }

    });

    

    $('#filter-btn').click( function(event){
        event.preventDefault();
        var modal = $('#search-deal-modal');

        modal.modal('show');
    });

    $('#clear-search-btn').click(function(event){
        event.preventDefault();

        var modal = $('div#search-deal-modal');

        modal.find('input[type="text"]').val("");
        modal.find('select#deal-category').val("");
        modal.find('select#business-id').val("");

        modal.find('form').submit();

    });

    $('.reset-btn').click( function(){

      var modal = $('div#search-deal-modal');

      modal.find('input[type="text"]').val("");
      modal.find('select#status').val(1);
      modal.find('select#deal-category').val("");
      modal.find('select#business-id').val("");

    });

    $('#check-all').click( function(){
        var chk = $(this).closest('table').find('tbody').find(':checkbox');
        chk.prop('checked' ,this.checked);
    });

    $('#deal-actions').click(function( event ){
          
        event.preventDefault();

        var menu = $('#deal-action-menu');
        menu.toggle()

    });

    $('#deal-action-menu').mouseleave(function(){

        var menu = $('#deal-action-menu');
        menu.hide()

    });

    $('#remove-dod').click(function(event){

        event.preventDefault();

        var me      = $(this),
            title   = me.attr('data-title');

        var chk_checked = $('table.table-list').find('tbody').find('.chk-checked:checked') ;

        var warning_modal = $('div#warning-modal');

        if( chk_checked.length == 0 ){
            warning_modal.modal('show');
            return false;
        }

        var modal_id = me.attr('data-action') + "-dod-modal";
        var action_modal = $('#' + modal_id);
        
        action_modal.find('#myModalLabel').text(title);
        action_modal.find('span#total-deal').text(chk_checked.length);

        var block_dod_ids = action_modal.find('.dod-ids');
        block_dod_ids.html("");

        if( chk_checked.length == 0 ){
            warning_modal.modal('show');
            return false;
        }

        $.each( chk_checked, function( ind, val ){
            var id = $(val).val();
            block_dod_ids.append("<input name='id[]' value='" + id + "' type='hidden'/>")
        })

        action_modal.modal('show');


    });

  });

</script>