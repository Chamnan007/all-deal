<?php

/********************************************************************************
    File: Add News Category
    Author: PHEA RATTANA

    Confidential ABi Technologies property.

  	Changed History:
  	Date 					Author				Description
  	2014/01/05 				PHEA RATTANA		Initial
*********************************************************************************/
?>

<div class="newsCategories form">
	<div class="row-fluid">		
		<!-- Pie: Box -->
		<div class="span12">

			<!-- Pie: Top Bar -->
			<div class="top-bar">
				<h3><i class="icon-list"></i> Manage News Categories</h3>
			</div>
			<!-- / Pie: Top Bar -->

			<!-- Pie: Content -->
			<div class="well">

			<a href="<?php echo $this->Html->url( array('action' => 'index')); ?>"> 
				<button class="btn btn-linkedin" type="button"><i class="icon-list-alt"></i> News Category List</button>
			</a>
			

			<div class="clearfix"></div>
				
				<?php echo $this->Form->create('NewsCategory'); ?>
					<fieldset>

						<legend><?php echo __('Add New News Category'); ?></legend>

						<div class="span4">
							<?php
								echo $this->Form->input('category', array('placeholder'=>'News Category',
																			'type' => 'text',
																			'required' => 'required',
																			'style' => "width:300px;"));
							?>	
						</div>

						<div class="span3" style="padding-top:19px;">
							<?php echo $this->Form->submit(__('Save'), array('class' => 'btn btn-primary') ); ?>	
						</div>	
						
					</fieldset>
			

			</div>
			<!-- / Pie: Content -->

		</div>
		<!-- / Pie -->
		
	</div>

</div>