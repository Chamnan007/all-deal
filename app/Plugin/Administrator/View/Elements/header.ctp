<?php 

	/********************************************************************************
	    File: Admin Template Header
	    Author: PHEA RATTANA
	
	    Confidential ABi Technologies property.
	
	  	Changed History:
	  	Date 					Author				Description
	  	2014/01/01 				PHEA RATTANA		Initial
	*********************************************************************************/
	
	$controller = $this->params['controller'];

	$action 	= ($this->params['action'] != 'index' )?$this->params['action']:NULL ;

	$c1 = $controller;

	if( $controller == "business_categories"){
		$controller = "businesses";
		$action = "Category";
	}

	$controller = str_replace("_", " ", $controller);

	$breadcrumb = $controller;

	if( strtolower($breadcrumb) == "businesses" ){
		$breadcrumb = "Merchants";
	}

	
	if( strtolower($breadcrumb) == "buyertransactions" ){
		$breadcrumb = "Purchase Transactions";
	}
	
	if( strtolower($breadcrumb) == "systemrevenues" ){
		$breadcrumb = "Finance";
	}


	if( strtolower($action) == 'dod' ){
		$action = "Deal of the Day";
	}

	if( strtolower($controller) == 'systemrevenues' ){

		if( strtolower($action) == 'makepayment' ){
			$action = 'Make Payment';
		}else if( strtolower($action) == 'payment'){
			$action = 'Payment History';
		}else{
			$action = "Revenue";
		}

	}

	$user = CakeSession::read("Auth.User");	

?>

<div id="settings" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		<h3 id="myModalLabel"><i class="icon-cog"></i> Account settings</h3>
	</div>

	<!-- Any Info-customization -->
	<form action="">
		

	</form>

</div> 

<div id="content" class="container" style="margin: 0 auto; width: 1170px; min-height:400px;">

	<div class="navbar navbar-inverse navbar-fixed-top">
		<!-- Top Fixed Bar: Navbar Inner -->
		<div class="navbar-inner">

			<!-- Top Fixed Bar: Container -->
			<div class="container">

				<!-- Mobile Menu Button -->
				<a href="#">
					<button type="button" class="btn btn-navbar mobile-menu">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</a>
				<!-- / Mobile Menu Button -->

				<!-- / Logo / Brand Name -->
				<a class="brand" href="<?php echo $this->Html->url(array('controller'=>'dashboards', 'plugin'=> 'administrator')) ?>" style="padding:5px !important; margin-left:20px !important;">
					<img src="<?php echo Router::url('/img/logo/admin_logo.png', true); ?>" alt="" style="width:70px;">
				</a>
				<!-- / Logo / Brand Name -->

				<!-- User Navigation -->
				<ul class="nav pull-right">

					<!-- User Navigation: User -->
					<li class="dropdown">

						<!-- User Navigation: User Link -->
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<i class="icon-user icon-white"></i> 
							<span class="hidden-phone"><?php echo urldecode($user['last_name'] . " " . urldecode($user['first_name'])) ?></span>
						</a>
						<!-- / User Navigation: User Link -->

						<!-- User Navigation: User Dropdown -->
						<ul class="dropdown-menu">
							<!-- <li><a href="#"><i class="icon-user"></i> Profile</a></li>
							<li><a href="#settings" data-toggle="modal"><i class="icon-cog"></i> Settings</a></li> -->
							<!-- <li><a href="#messages" data-toggle="modal"><i class="icon-envelope"></i> Messages</a></li> -->
							<li><a href="<?php echo $this->Html->url( array('controller' => 'users', 'action' => 'profile', 'plugin' => 'administrator')); ?>"><i class="icon-user"></i> My Profile</a></li>
							<!-- <li class="divider"></li> -->
							<li><a href="<?php echo $this->Html->url( array('controller' => 'users', 'action' => 'logout')); ?>"><i class="icon-off"></i> Logout</a></li>
						</ul>
						<!-- / User Navigation: User Dropdown -->

					</li>
					<!-- / User Navigation: User -->

				</ul>
				<!-- / User Navigation -->

			</div>
			<!-- / Top Fixed Bar: Container -->

		</div>
		<!-- / Top Fixed Bar: Navbar Inner -->

		<!-- Top Fixed Bar: Breadcrumb -->
		<div class="breadcrumb clearfix">

			<!-- Top Fixed Bar: Breadcrumb Container -->
			<div class="container">

				<!-- Top Fixed Bar: Breadcrumb Location -->
				<ul class="pull-left">
					<li>
						<a href="<?php echo $this->Html->url(array('plugin'=>'administrator', 'controller'=>'dashboards')); ?>"><i class="icon-home"></i> Home</a> 
						<span class="divider">/</span>
					</li>
					<li class="<?php echo(!$action)?'active':'' ?>">
						<a href="<?php echo $this->Html->url(array('plugin'=>'administrator', 'controller'=>$c1, 'action'=>'index')); ?>"><i class="icon-list-alt"></i> <?php echo ucwords($breadcrumb) ?></a>
						<?php echo($action)?"<span class='divider'>/</span>":"" ?>
					</li>
					<?php if($action){ ?>
					<li class="active">
						<a href="#"><i class="icon-list-alt"></i> <?php echo ucwords($action) ?></a>
					</li>
					<?php } ?>

				</ul>
				<!-- / Top Fixed Bar: Breadcrumb Location -->

			</div>
			<!-- / Top Fixed Bar: Breadcrumb Container -->

		</div>
		<!-- / Top Fixed Bar: Breadcrumb -->

	</div>
	<!-- Main Navigation: Box -->

<div class="navbar navbar-inverse" id="nav">

	<!-- Main Navigation: Inner -->
	<div class="navbar-inner">

		<!-- Main Navigation: Nav -->
		<ul class="nav">

			<!-- Main Navigation: Dashboard -->
			<li class="<?php echo($controller=='dashboards')?'active':'' ?>">
				<a href="<?php echo $this->Html->url(array('controller'=>'dashboards', 'action'=>'index', 'plugin'=> 'administrator')); ?>"><i class="icon-align-justify"></i> Dashboard</a></li>
			<!-- / Main Navigation: Dashboard -->

			<!-- Main Navigation: General -->
			<li class="dropdown <?php echo($controller=='businesses')?'active':'' ?>">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">
					<i class="icon-th"></i> Merchants <b class="caret"></b>
				</a>
				<ul class="dropdown-menu">
					<li><a href="<?php echo $this->Html->url(array('controller'=>'businesses', 'action'=>'add', 'plugin'=> 'administrator')); ?>"><i class="icon-plus"></i> Add New Merchant</a></li>

					<li><a href="<?php echo $this->Html->url(array('controller'=>'businesses', 'action'=>'index', 'plugin'=> 'administrator')); ?>"><i class="icon-list"></i> Merchant List</a></li>
					
					<?php if($user['access_level'] == 6): ?>

					<li><a href="<?php echo $this->Html->url(array('controller'=>'business_categories', 'plugin'=> 'administrator', 'action'=>'index')) ?>"><i class="icon-list"></i> Category</a></li>
					<?php endif ?>
				</ul>
			</li>

			<li class="dropdown <?php echo($controller=='deals')?'active':'' ?>">
				
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">
					<i class="icon-th"></i> Deals <b class="caret"></b>
				</a>

				<ul class="dropdown-menu">
					<li><a href="<?php echo $this->Html->url(array('controller'=>'deals', 'action'=>'index', 'plugin'=> 'administrator')); ?>"><i class="icon-list-alt"></i> Deals List</a></li>
					
					
					<li><a href="<?php echo $this->Html->url(array('controller'=>'deals', 'plugin'=> 'administrator', 'action'=>'dod')) ?>"><i class="icon-list-alt"></i> Deal of the Day</a></li>
					
				</ul>
			</li>

			<li class="<?php echo($controller=='buyers' && $action != 'topup' && $action != 'topupView' )?'active':'' ?>">
				<a href="<?php echo $this->Html->url(array('controller'=>'buyers', 'action'=>'index', 'plugin'=> 'administrator')) ?>">
					<i class="icon-user"></i> Users</a>
			</li>
			<?php if($user['access_level'] == 6): ?>
				<li class="<?php echo($controller=='buyers' && ( $action == "topup" || $action == 'topupView') )?'active':'' ?>">
					<a href="<?php echo $this->Html->url(array('controller'=>'buyers', 'action'=>'topup', 'plugin'=> 'administrator')) ?>">
						<i class="icon-list-alt"></i> Top Up</a>
				</li>
			<?php endif ?>

			<li class="<?php echo($controller=='BuyerTransactions' && $action == '' )?'active':'' ?>">
		        <a href="<?php echo $this->Html->url(array('controller'=>'BuyerTransactions', 'action'=>'index', 'plugin'=> 'administrator')); ?>"><i class="icon-search"></i> Purchase Transactions</a>
		    </li>

		    <li class="<?php echo($controller == 'BuyerTransactions' && $action == "redeem")?'active':'' ?>">
		        <a href="<?php echo $this->Html->url(array('controller'=>'BuyerTransactions', 'action'=>'redeem', 'plugin'=> 'administrator')); ?>"><i class="icon-list"></i> Redeem Purchase </a>
		    </li>

			<?php if($user['access_level'] == 6): ?>
				<li class="dropdown <?php echo($controller=='reports' || $controller == 'SystemRevenues' )?'active':'' ?>">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-list">
						</i> Finance <b class="caret"></b>
					</a>
					<ul class="dropdown-menu">
						<li>
							<a href="<?php echo $this->Html->url(array('controller'=>'SystemRevenues', 'action'=>'index', 'plugin'=> 'administrator')) ?>">
								<i class="icon-list"></i> Revenue</a>
						</li>
						<li class="">
							<a href="<?php echo $this->Html->url(array('controller'=>'SystemRevenues', 'action'=>'makepayment', 'plugin'=> 'administrator')); ?>"><i class="icon-align-justify"></i> Make Payment</a>
						</li>
						<li class="">
							<a href="<?php echo $this->Html->url(array('controller'=>'SystemRevenues', 'action'=>'payment', 'plugin'=> 'administrator')); ?>"><i class="icon-align-justify"></i> Payment History</a>
						</li>
					</ul>
				</li>
			<?php endif; ?>
			
			<?php if($user['access_level'] == 6): ?>
				
			<li class="dropdown <?php echo ($controller=='provinces' || $controller == 'cities' || $controller == 'locations' || $controller == 'goods categories' || $controller == 'services categories' || $controller == 'interests categories' || $controller == 'discount categories' || $controller == 'news categories' || $controller == 'commissions' || $controller == 'users')?'active':'' ?>">
				
				<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-cog">
					</i> Setting <b class="caret"></b>
				</a>
				
				<ul class="dropdown-menu">

					<li>
						<a href="<?php echo $this->Html->url(array("controller"=> "users", "action"=> "index",
							 "plugin"=>"administrator")); ?>">
							<i class="icon-edit"></i> Manage Users
						</a>
					</li>

					<li><a href="<?php echo $this->Html->url(array('controller'=>'provinces', 'action'=>'index', 'plugin'=> 'administrator')) ?>"><i class="icon-edit"></i> Manage Provinces</a></li>

					<li><a href="<?php echo $this->Html->url(array('controller'=>'cities', 'action'=>'index', 'plugin'=> 'administrator')) ?>"><i class="icon-edit"></i> Manage Cities</a></li>

					<li><a href="<?php echo $this->Html->url(array('controller'=>'locations', 'action'=>'index', 'plugin'=> 'administrator')) ?>"><i class="icon-edit"></i> Manage Locations/ Khan</a></li>

					<li><a href="<?php echo $this->Html->url(array('controller'=>'sangkats', 'action'=>'index', 'plugin'=> 'administrator')) ?>"><i class="icon-edit"></i> Manage Sangkat</a></li>

					<li><a href="<?php echo $this->Html->url(array('controller'=>'goods_categories', 'action'=>'index', 'plugin'=> 'administrator')) ?>"><i class="icon-edit"></i> Manage Deal Category</a></li>

					<li>
						<a href="<?php echo $this->Html->url(array('controller'=>'commissions', 'action'=>'index', 'plugin'=> 'administrator')) ?>">
							<i class="icon-cog"></i> Commission Setting</a>
					</li>

				</ul>
			</li>
			
			<?php endif; ?>




		</ul>
		<!-- / Main Navigation: Nav -->
	
	</div>
	<!-- / Main Navigation: Inner -->

</div>