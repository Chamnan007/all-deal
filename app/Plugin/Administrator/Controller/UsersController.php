<?php
App::uses('AdministratorAppController', 'Administrator.Controller');
App::uses('Province', 'Administrator.Model');
App::uses('City', 'Administrator.Model');
App::uses('MailConfigure', 'Administrator.Model');

/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */

class UsersController extends AdministratorAppController {

/**
 * Components
 *
 * @var array
 */
  var $context = 'User';

  var $uses = array('Administrator.User', 'Administrator.MailConfigure' );
  
  var $components = array('Email');

/**
 * index method
 *
 * @return void
 */
  public function index() {


    $user = CakeSession::read("Auth.User"); 

    if( $user['access_level'] != 6 ){
      return $this->redirect(array( 'controller' => 'dashboards' ,'action' => 'index' ));
    }

    $this->set('status', $this->status);
    $this->set('access_level', $this->access_level);

    $this->User->recursive = 0;

    $this->paginate['conditions'] = array("User.id != " . $user['id'], 'User.access_level' => array(4,5,6) ) ;
    $this->paginate['order'] = array("User.last_name" => 'ASC' ) ;
    parent::index() ;

  }

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
  public function view($id = null) {


    $user = CakeSession::read("Auth.User"); 

    if( $user['access_level'] != 6 ){
      return $this->redirect(array( 'controller' => 'dashboards' ,'action' => 'index' ));
    }


    if (!$this->User->exists($id)) {
      throw new NotFoundException(__('Invalid user'));
    }

    $city = new City();
    $this->set('cities', $city->find('all'));

    $province = new Province();
    $this->set('provinces', $province->find('all'));


    $this->set('access_level', $this->access_level_action);
    $this->set('status', $this->status);

    $options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
    $this->set('user', $this->User->find('first', $options));

  }

/**
 * add method
 *
 * @return void
 */


  public function add() {

    
    $user = CakeSession::read("Auth.User"); 

    if( $user['access_level'] != 6 ){
      return $this->redirect(array( 'controller' => 'dashboards' ,'action' => 'index' ));
    }

    $this->set('gender', $this->gender);

    $city = new City();
    $this->set('cities', $city->find('all'));

    $province = new Province();
    $this->set('provinces', $province->find('all'));

    
    $this->set('access_level', $this->access_level_action);
    $this->set('status', $this->status);


    if ($this->request->is('post')) {
      $this->User->create();

      // var_dump($this->request->data['User']); exit;

      unset( $this->request->data['User']['confirm_password'] );

      $password = $this->request->data['User']['password'] ;

      if($this->request->data['User']['password'] == "" ){
        $this->request->data['User']['confirm_password'] = NULL;
      }

      $this->request->data['User']['user_id'] = uniqid() ;
      $this->request->data['User']['business_id'] = NULL ;

      $this->request->data['User']['registered_date'] = DboSource::expression('NOW()');

      $this->request->data['User']['dob'] = $this->request->data['User']['dob']['year'] . "-" . $this->request->data['User']['dob']['month'] . "-" . $this->request->data['User']['dob']['day'];

      // var_dump($this->request->data); exit;

      $this->User->create();

      if( $this->User->save($this->request->data)){
        $this->Session->setFlash(__('<div class="alert alert-success"><button type="button" class="close" ' . 
                                    'data-dismiss="alert">×</button>' . 
                                    '<b>Success!!!</b> New user is created successfull.</div>'));

        $this->redirect(array('action' => 'view', $this->User->getInsertID()));
      }else{
         $this->Session->setFlash(__('<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button>' . 
                                    '<b>Failed!!!</b> Creating new member failed. Please, try again.</div>'));
      }

      $this->redirect(array('action' => 'add'));
    }
  }

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */

  public function beforeFilter() {
      parent::beforeFilter();

    $this->Auth->allow('forgot', 'activate' );
  }


  public function forgot(){

    if ($this->request->is('post')) { 
      $mail = $this->request->data['User']['email'];

      $conditions = array('conditions' => array('User.email' => $mail, 'User.status = 1') );

      $info = $this->User->find( 'first', $conditions );

      if( !empty($info) ){

        $token = $this->randomPassword() . $mail ;

        for( $i =1; $i <= 10; $i++ ){
          $token = sha1($token);
        }

        $activate_link = Router::url("/administrator/Users/activate/"  . $token , true);

        $data['User']['id'] = $info['User']['id'];
        $data['User']['token'] = $token;

        $subject = "Password Reset Verification Message";

        $content = "<h4>Dear " . $info['User']['last_name'] . " " . $info['User']['first_name'] . ",</h4><p>Please click on verification link below to complete your password reset.</p>";
        $content .= "<p><a href='" . $activate_link . "'>" . $activate_link . "</a></p><br/>";
        // $content .= "<p>Thanks,</p><p>Admin</p>";

        if( $this->sendEmail($mail, $subject, $content) ){

          if( $this->User->save($data)){
              $this->Session->setFlash(__( "Verification email has been sent to your email: " . $mail )
                      .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                    'default',
                    array('class'=>'alert alert-dismissable alert-success '));
            return $this->redirect(array('action' =>'login'));
          }
        }else{
          $this->Session->setFlash(__( "Something went wrong. Please try again." )
                      .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                    'default',
                    array('class'=>'alert alert-dismissable alert-danger '));
          return $this->redirect(array('action' =>'forgot'));
        }
  
      }else{
        $this->Session->setFlash(__( "Reset Password Failed. Make sure you account is activated." )
                      .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                    'default',
                    array('class'=>'alert alert-dismissable alert-danger '));

      return $this->redirect(array('action' =>'forgot'));
      }

    }

    $this->layout = 'login';
  }


  public function activate( $token = NULL ){

    if( !$token ){
      $this->Session->setFlash(__( "Verification Failed. Try again!" )
                      .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                    'default',
                    array('class'=>'alert alert-dismissable alert-danger '));

      return $this->redirect(array('action' => 'login'));

    }

    $info = $this->User->findByToken($token);

    if( !empty($info) ){

      $mail = $info['User']['email'];

      $new_pass = $this->randomPassword();

      $user_id = $info['User']['id'];
      $this->User->id = $user_id;
      $data['User']['id'] = $user_id ;
      $data['User']['password'] = $new_pass;
      $data['User']['token'] = "";

      $Login = Router::url("/administrator/Users/login" , true);

      $subject = "Password Reset";

      $content = "<h4>Dear " . $info['User']['last_name'] . " " . $info['User']['first_name'] . ",</h4><p>Your password is successfully reset.</p><p>Your new password: $new_pass</p>";
      $content .= "<p><a href='" . $Login . "'>Goto Login</a></p><br/>";
      // $content .= "<p>Thanks,</p><p>Admin</p>";

      if( $this->sendEmail($mail, $subject, $content) ){

        if( $this->User->save($data)){
            $this->Session->setFlash(__( "Your new password has been sent to your email : " . $mail )
                    .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                  'default',
                  array('class'=>'alert alert-dismissable alert-success '));
          return $this->redirect(array('action' =>'login'));
        }
      }else{
        $this->Session->setFlash(__( "Something went wrong. Please try again." )
                    .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                  'default',
                  array('class'=>'alert alert-dismissable alert-danger '));
        return $this->redirect(array('action' =>'forgot'));
      }

    }else{
      $this->Session->setFlash(__( "Verification Failed." )
                      .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                    'default',
                    array('class'=>'alert alert-dismissable alert-danger '));

      return $this->redirect(array('action' => 'login'));
    }


  }


public function sendEmail( $recipients = NULL, $subject = NULL , $content = NULL ) {

    return parent::sendMail($recipients, $subject, $content );

}


  public function randomPassword() {

      $alphabet = "0123456789abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
      $pass = array(); 
      $alphaLength = strlen($alphabet) - 1; 

      for ($i = 0; $i < 10; $i++) {
          $n = rand(0, $alphaLength);
          $pass[] = $alphabet[$n];
      }

      return implode($pass);
  }
  
  public function edit($id = null) {

    if (!$this->User->exists($id)) {
      // throw new NotFoundException(__('Invalid user'));
      $this->redirect(array('action' => 'index'));
    }


    $user = CakeSession::read("Auth.User"); 

    if( $user['access_level'] != 6 ){
      return $this->redirect(array( 'controller' => 'dashboards' ,'action' => 'index' ));
    }

    $this->set('gender', $this->gender);

    $city = new City();
    $this->set('cities', $city->find('all'));

    $province = new Province();
    $this->set('provinces', $province->find('all'));

    $this->set('access_level', $this->access_level_action);



    $this->set('status', $this->status);

    if ($this->request->is(array('post', 'put'))) {

      $this->request->data['User']['inactive_date'] = DboSource::expression('NOW()');

      // parent::save($id);


      $this->request->data['User']['dob'] = $this->request->data['User']['dob']['year'] . "-" . $this->request->data['User']['dob']['month'] . "-" . $this->request->data['User']['dob']['day'];


      if( $this->User->save($this->request->data)){
        $this->Session->setFlash(__('<div class="alert alert-success"><button type="button" class="close" ' . 
                                    'data-dismiss="alert">×</button>' . 
                                    '<b>Success!!!</b> Information is successfully changed.</div>'));
      }else{
         $this->Session->setFlash(__('<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button>' . 
                                    '<b>Failed!!!</b> Information could not be changed. Please, try again.</div>'));
      }

      $this->redirect(array('action' => 'view', $id));

    } else {

      $options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
      $this->request->data = $this->User->find('first', $options);  

      // var_dump($this->request->data);
      $city = new City();
      $this->set('cities', $city->find('all', array('conditions' => array("City.province_code" => $this->request->data['User']['province_code'], 'City.status' => 1 ) ) ));
      
    }
  }


  public function change_password( $id = null){

    if (!$this->User->exists($id)) {
      throw new NotFoundException(__('Invalid user'));
    }

    unset($this->request->data['User']['confirm_password']);

    if ($this->User->save($this->request->data)) {
      $this->Session->setFlash(__('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button><b>Success!!!</b> Password is successfully changed.</div>'));

      $obj  = $this->User->findById($id);
      $logMessage = json_encode($obj);
      parent::generateLog($logMessage,' RESET PASSWORD :');
      
    } else {
      $this->Session->setFlash(__('<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button><b>Failed!!!</b> Password could not be changed. Please, try again.</div>'));
    }

    return $this->redirect(array('action' => 'view' , $this->request->data['User']['id']));

  }



  public function get_user_by_id( $id = 0 ){
    $data = array();

    if (!$this->User->exists($id)) {
      $data['status'] = false;
      echo json_encode($data);
      exit;
    }

    $options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
    $user_info = $this->User->find('first', $options);    

    $data['status'] = true;
    $data['result'] = $user_info;

    echo json_encode($data);
    exit;

  }

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
  // public function delete($id = null) {
    
  //   $this->User->id = $id;

  //   if (!$this->User->exists()) {
  //     throw new NotFoundException(__('Invalid user'));
  //   }
  //   $this->request->onlyAllow('post', 'delete');
  //   if ($this->User->delete()) {
  //     $this->Session->setFlash(__('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button><b>Success!!!</b> The user has been deleted.</div>'));
  //   } else {
  //     $this->Session->setFlash(__('<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button><b>Failed!!!</b> The user could not be deleted. Please, try again.</div>'));
  //   }
  //   return $this->redirect(array('action' => 'index'));
  // }


  public function get_city_by_province( $pro_code = null ){

    if( $pro_code != null ){

      $city = new City();
      $cities =  $city->find('all',array( 'conditions' => array('province_code' => $pro_code, 'status' => 1 ))) ;

      $data = array();
      $data['data'] =  $cities;
      echo json_encode( $data);

      $this->autoRender = false;

    }
  }


  public function logout()

  {
    return $this->redirect($this->Auth->logout());
  }

  public function login()
  {
    if ($this->request->is('post')) 
    {
            if ($this->Auth->login()) 
            {
                return $this->redirect($this->Auth->redirect());

            }

            $this->Session->setFlash(__('<span style="color:red; padding: 35px;">Invalid username or password, try again</span>'));
        }
        $this->layout = 'login';
  }


  public function validateEmail()
  {
    $email = $this->request->data['email'];
    $userId = $this->request->data['userid'];
    $count = $this->User->find('count',
        array('conditions'=>
              array(
                  'email'=>$email,
                  'id !='=>$userId
                )
            ));
    if($count)echo 0;
    else echo 1;
    exit;
  }

  public function profile() {

    $user = CakeSession::read("Auth.User"); 

    $city = new City();
    $this->set('cities', $city->find('all', array('conditions' => array('City.status' => 1) )));

    $province = new Province();
    $this->set('provinces', $province->find('all', array('conditions' => array('Province.status' => 1) )));


    $this->set('access_level', $this->access_level);
    $this->set('status', $this->status);

    $options = array('conditions' => array('User.' . $this->User->primaryKey => $user['id']));
    $this->set('user', $this->User->find('first', $options));

  }

  public function p_change_password(){

    $user = CakeSession::read("Auth.User");

    unset($this->request->data['User']['confirm_password']);

    if ($this->User->save($this->request->data)) {
      $this->Session->setFlash(__('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button><b>Success!!!</b> Password is successfully changed.</div>'));

      $obj  = $this->User->findById($user['id']);
      $logMessage = json_encode($obj);
      parent::generateLog($logMessage,' RESET PASSWORD');
      
      
    } else {
      $this->Session->setFlash(__('<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button><b>Failed!!!</b> Password could not be changed. Please, try again.</div>'));
    }

    return $this->redirect(array( 'controller' => 'users' , 'action' => 'profile', 'plugin' => 'administrator' ));

  }

  public function editProfile() {

    $user = CakeSession::read("Auth.User");
    $this->set('user_ac_lv', $user['access_level']);

    $this->set('gender', $this->gender);

    $province = new Province();
    $this->set('provinces', $province->find('all', array('conditions' => array('Province.status' => 1) )));

    $this->set('access_level', $this->access_level);

    $this->set('status', $this->status);

    if ($this->request->is(array('post', 'put'))) {

      $this->request->data['User']['inactive_date'] = DboSource::expression('NOW()');
      $this->request->data['User']['id'] = $user['id'];

      $this->request->data['User']['dob'] = $this->request->data['User']['dob']['year'] . "-" . $this->request->data['User']['dob']['month'] . "-" . $this->request->data['User']['dob']['day'];

      if($this->User->save($this->request->data)){

        $options = array('conditions' => array('User.' . $this->User->primaryKey => $user['id']));
        $new_info = $this->User->find('first', $options); 

        $this->Session->write("Auth.User", $new_info['User']);

        $this->Session->setFlash(__( "Profile is updated successfully !" )
                      .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                    'default',
                    array('class'=>'alert alert-dismissable alert-success '));


      }else{
        $this->Session->setFlash(__( "Updating Failed !" )
                      .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                    'default',
                    array('class'=>'alert alert-dismissable alert-danger '));

      }

       return $this->redirect(array('action' =>'profile'));

    } else {

      $options = array('conditions' => array('User.' . $this->User->primaryKey => $user['id']));
      $this->request->data = $this->User->find('first', $options);  

      // var_dump($this->request->data);
      $city = new City();
      $this->set('cities', $city->find('all', array('conditions' => array("City.province_code" => $this->request->data['User']['province_code'], 'City.status' => 1 ) ) ));
      
    }
  }


  public function checkDuplicateEmail( $email = NULL, $userid = NULL ){
    
    if( $userid != NULL ){
      $conditions = array('conditions'=> array('User.email' => $email, 'User.id !=' => $userid )) ;
    }else{
      $conditions = array('conditions'=> array('User.email' => $email )) ;
    }


    if( $this->User->find('first', $conditions ) ){
      $data['status'] = 0;
      $data['msg'] = "This email is already registered. Please use another email." ;
    }else{
      $data['status'] = 1;
    }

    echo json_encode($data); exit;

  }


}