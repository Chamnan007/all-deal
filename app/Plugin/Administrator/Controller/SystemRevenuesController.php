<?php
App::uses('AdministratorAppController', 'Administrator.Controller');
/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class SystemRevenuesController extends AdministratorAppController {

/**
 * Components
 *
 * @var array
 */
  // var $components = array('Paginator');

  var $context = "SystemRevenue";

  var $uses = array(  'Administrator.SystemRevenue',
                      'Administrator.BusinessCategory',
                      'Administrator.BuyerTransaction',
                      'Administrator.BusinessRevenue',
                      'Administrator.PaymentHistory',
                      'Administrator.Business',
                      'Administrator.Province',
                      'Administrator.City',
                      'Administrator.Deal'
                  );

  var $bizConds = array();
  var $historyConditions = array();
  
  /**
   * index method
   *
   * @return void
  */

  public function setCondition(){

      parent::setCondition();

      $default_from = date('Y-m-01');
      $default_to   = date('Y-m-t');
      
      $from     = $default_from;
      $to       = $default_to;
      $default  = true;

      if( isset($this->request->data['purchaseDateFrom']) ){
          $dateFrom = $this->request->data['purchaseDateFrom'];
          $this->setState('purchaseDateFrom',  $dateFrom );
      }
      
      if( isset($this->request->data['purchaseDateTo']) ){
          $dateTo = $this->request->data['purchaseTo'] ;
          $this->setState('purchaseDateTo', $dateTo );
      }
      
      if( isset($this->request->data['business_id']) ){
          $this->setState('business_id', $this->request->data['business_id'] );
      }

      if( $business_id = $this->getState('business_id', NULL )){
          $this->conditionFilter['SystemRevenue.business_id'] = $business_id ;            
      }

      if( $dateFrom = $this->getState('purchaseDateFrom', NULL ) ){
          $from = $dateFrom ;
      }

      if( $dateTo = $this->getState('purchaseDateTo', NULL ) ){
          $to = $dateTo;
      }

      if( $from != $default_from || $to != $default_to ){
          $default = false;
      }

      $this->conditionFilter['SystemRevenue.created >='] = date("Y-m-d 00:00:00", strtotime($from)) ;
      $this->conditionFilter['SystemRevenue.created <='] = date("Y-m-d 23:59:59", strtotime($to)) ;

      $period_string = "This month revenue" ;
      $income_string = "This month total income" ;

      if( !$default ){
        $period_string = "Revenue from " . date('d-F-Y', strtotime($from)) . " to " . date('d-F-Y', strtotime($to)) ;
        $income_string = "Total Income from " . date('d-F-Y', strtotime($from)) . " to " . date('d-F-Y', strtotime($to)) ;
      }

      $this->set('period_string', $period_string);
      $this->set('income_string', $income_string);
      
      $this->paginate['conditions'] = $this->conditionFilter;
      $this->paginate['order']      = array( 
                                          'SystemRevenue.created'   => 'DESC',
                                          'TransactionDetail.code'  => 'DESC'
                                        );

      $this->set('states', $this->getState());

  }


  public function beforeFilter(){

      parent::beforeFilter();
    
      $this->user = CakeSession::read("Auth.User");
      $this->set('user', $this->user);   

      $this->set('status', $this->status);   

      // Get Business Category
      $conds['conditions']  = array('BusinessCategory.status' => 1);
      $conds['order']       = array('BusinessCategory.category' => 'ASC');
      $categories   = $this->BusinessCategory->find('all', $conds);

      $this->set('MerchantCategories', $categories);

      // Get Province List
      $provinces = $this->Province->find('all', array('conditions' => array('Province.status' => 1)) );
      $this->set('provinces', $provinces);

      // Get City List
      $cities = $this->City->find('all', array('conditions' => array('City.status' => 1)) );
      $this->set('cities', $cities);

      $this->set('PayMethods', $this->pay_method );

  }


  public function index(){

      $this->setCondition();
      $this->SystemRevenue->recursive = 2;

      parent::index();

      // Get Total Revenue
      $revenueConds['conditions'] = $this->conditionFilter;
      $revenueConds['fields']     = array( 'SUM(total_amount) AS total' );
      $this->SystemRevenue->recursive = -1;
      $total_revenue = $this->SystemRevenue->find('first', $revenueConds);
      $this->set('total_revenue', floatval($total_revenue[0]['total']));


      // Get Total Income
      $incomeConds['conditions'] = $this->conditionFilter;
      $incomeConds['fields']     = array( 'SUM(transaction_amount) AS total' );
      $this->SystemRevenue->recursive = -1;
      $total_amount = $this->SystemRevenue->find('first', $incomeConds);
      $this->set('total_amount', floatval($total_amount[0]['total']));

      // Get Business Listing
      $this->Business->recursive = -1;
      $bizData = $this->Business->find('all', array('conditions' => array('Business.status' => 1),
                                                    'order' => array('Business.business_name' => "ASC" ) ));

      $this->set('bizData', $bizData);

  }

  public function setHistoryConditions(){

      $this->context = "PaymentHistory" ;

      $data = $this->request->data;

      $from = date('Y-m-01');
      $to   = date('Y-m-t');

      if( isset($data['business_id']) ){
          $this->setState('business_id', $data['business_id']);
      }

      if( isset($data['payment_method']) ){
          $this->setState('payment_method', $data['payment_method']);
      }

      if( isset($data['payDateFrom']) && $data['payDateFrom'] != "" ){
          $this->setState('payDateFrom', $data['payDateFrom']);
      }

      if( isset($data['payDateTo']) && $data['payDateTo'] != "" ){
          $this->setState('payDateTo', $data['payDateTo']);
      }

      if( $biz_id = $this->getState('business_id', NULL ) ){
          $this->historyConditions['PaymentHistory.business_id'] = $biz_id;
      }

      if( $payment_method = $this->getState('payment_method', NULL ) ){
          $this->historyConditions['PaymentHistory.payment_method'] = $payment_method;
      }

      if( $this->getState('payDateFrom', $from )){
          $from = $this->getState('payDateFrom');
      }

      if( $this->getState('payDateTo', $to)){
          $to = $this->getState('payDateTo');
      }

      $this->setState('payDateFrom', $from);
      $this->setState('payDateTo', $to);

      $this->historyConditions['PaymentHistory.pay_date >='] = date("Y-m-d", strtotime($from));
      $this->historyConditions['PaymentHistory.pay_date <='] = date("Y-m-d", strtotime($to));

      $this->paginate['conditions'] = $this->historyConditions;
      $this->paginate['order']      = array( 'PaymentHistory.pay_date' => 'DESC' );

      $this->set('historyStates', $this->getState());

  }

  public function payment(){

      $this->setHistoryConditions();
      $data = $this->paginate($this->getContext()); 

      $this->set(compact('data'));

      // Get Business Listing
      $this->Business->recursive = -1;
      $bizData = $this->Business->find('all', array('conditions' => array('Business.status' => 1),
                                                    'order' => array('Business.business_name' => "ASC" )  ));

      $this->set('bizData', $bizData);

      // Get Deal Info
      $deal_ids = array();
      foreach( $data as $k => $val ){
         $ids = explode(",", $val['PaymentHistory']['deal_ids']);
         foreach( $ids as $id ){
            $deal_ids[] = $id;
         }
      }

      $dealConds['conditions'] = array( 'Deal.id' => $deal_ids );
      $dealConds['fields']     = array( 'Deal.id', 'Deal.deal_code' );

      $this->Deal->recursive = -1;
      $dealData = $this->Deal->find('all', $dealConds);
      $dealInfo = array();

      foreach( $dealData as $ind => $deal ){
        $dealInfo[$deal['Deal']['id']] = $deal;
      }

      $this->set('dealInfo', $dealInfo);

  }

  public function setMerchantConditions(){

      $this->context = "Business";

      $data = $this->request->data;

      if( isset($data['merchant-text']) ){
          $this->setState('merchant-text', $data['merchant-text']);
      }

      if( isset($data['merchant-code']) ){
          $this->setState('merchant-code', $data['merchant-code']);
      }

      if( isset($data['category']) ){
          $this->setState('merchant-category', $data['category']);
      }

      if( $search = $this->getState('merchant-text') ){
          $this->bizConds['Business.business_name LIKE'] = '%' . $search . '%';
      }
      if( $code = $this->getState('merchant-code') ){
          $this->bizConds['Business.business_code'] = $code ;
      }

      if( $cate = $this->getState('merchant-category') ){
          $this->bizConds['Business.business_main_category'] = $cate;
      }

      $this->bizConds['Business.status'] = 1;

      $this->paginate['conditions'] = $this->bizConds;
      $this->paginate['order']      = array( 'Business.business_name' => 'ASC' );

      $this->set('merchantStates', $this->getState());

  }

  public function makepayment(){

      $this->setMerchantConditions();

      // unset($this->paginate['limit']);
      $limit = $this->Business->find('count', $this->paginate);
      $this->paginate['limit'] = $limit;
      $data = $this->paginate($this->getContext());
      $this->set(compact('data'));

      $bis_ids = array();
      foreach( $data as $k => $val ){
        $bis_ids[] = $val['Business']['id'];
      }

      $now = date('Y-m-d H:i:s');

      // Caluculate Payable Balance of each business
      // Query on expired deals
      $amountConds['conditions'] = array( 'BuyerTransaction.business_id' => $bis_ids,
                                    'OR' => array(
                                          'DealInfo.status' => -1,
                                          'DealInfo.valid_to <' => $now
                                        ),
                                    'DealInfo.is_paid' => 0
                                  ) ;
      $amountConds['fields'] = array( 'BuyerTransaction.*', 
                                      'DealInfo.*', 
                                      'SUM(BuyerTransaction.amount * (100 - BuyerTransaction.commission_percent )/ 100) AS total' );

      $amountConds['group'] = array( 'BuyerTransaction.deal_id' );

      $TranData = $this->BuyerTransaction->find('all', $amountConds);
      $payableData = array();
      if( $TranData){
          foreach( $TranData as $k => $val ){
            $payableData[$val['BuyerTransaction']['business_id']][$val['BuyerTransaction']['deal_id']] = $val;
          }
      }

      $this->set('payableData', $payableData);

      // Calculate Total Account Payable
      $conds['conditions']  = array( 
                                    'Business.status' => 1
                                 );
      $conds['fields']      = array( 'SUM(Business.ending_balance) AS total' );
      $total = $this->Business->find('first', $conds);
      $this->set('total_account_payable', $total[0]['total']);
  }

  public function savePayment( $biz_id = 0 ){
      
      if( !$this->request->is('post') || $biz_id == 0 ){
          return $this->redirect(array('action' => 'makepayment' ));
      }

      $user_id  = $this->user['id'];
      $data = $this->request->data;

      $pay_amount = $data['pay_amount'];

      // Update Ending Balance of Business
      $this->Business->recursive = -1;
      $bizConds['conditions'] = array( 'Business.id' => $biz_id ) ;
      $bizInfo = $this->Business->find('first', $bizConds);

      if( $bizInfo ){

        // Last Ending Balance
        $last_balance = $bizInfo['Business']['ending_balance'];
        
        if( $pay_amount > $last_balance ){

          $this->Session->setFlash(__( 'Invalid Pay Amount ! You cannot pay more than balance !')
                        .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                      'default',
                      array('class'=>'alert alert-dismissable alert-danger '));

          return $this->redirect( array( 'action' => 'makepayment' ));

        }

        $new_end_balance = $last_balance - $pay_amount;

        $newBizData = array();
        $this->Business->id = $biz_id;
        $newBizData['Business']['Business']['id'] = $biz_id;
        $newBizData['Business']['ending_balance'] = $new_end_balance;

        // Save Update Last Ending Balance
        if( $this->Business->save($newBizData) ){
            $saveData = array();
            // Save Payment History
            $saveData['PaymentHistory']['pay_date']       = date('Y-m-d', strtotime($data['pay_date']));
            $saveData['PaymentHistory']['pay_amount']     = $pay_amount;
            $saveData['PaymentHistory']['payment_method'] = $data['payment_method'];
            $saveData['PaymentHistory']['from_bank']      = $data['from_bank'];
            $saveData['PaymentHistory']['to_bank']        = $data['to_bank'];
            $saveData['PaymentHistory']['description']    = $data['description'];
            $saveData['PaymentHistory']['business_id']    = $biz_id;
            $saveData['PaymentHistory']['deal_ids']       = $data['deal_ids'];
            $saveData['PaymentHistory']['created']        = date('Y-m-d H:i:s');
            $saveData['PaymentHistory']['modified']       = date('Y-m-d H:i:s');
            $saveData['PaymentHistory']['created_by']     = $user_id ;

            $this->PaymentHistory->create();

            if( $this->PaymentHistory->save( $saveData ) ){

                // Update Deal as already paid
                $dealids = explode(',', $data['deal_ids']);
                
                $updateData = array();
                foreach( $dealids as $id ){
                  $tmp = array();
                  $tmp['Deal']['id']      = $id;
                  $tmp['Deal']['is_paid'] = 1;

                  $updateData[] = $tmp;
                }

                $this->Deal->saveAll($updateData);

                $payment_id = $this->PaymentHistory->getLastInsertId();

                // Save Transaction to Business Revenue
                $data_bis_revenue = array();
                $data_bis_revenue['transaction_id']     = 0;
                $data_bis_revenue['type']               = 2; // 2: Debite
                $data_bis_revenue['payment_id']         = $payment_id ;
                $data_bis_revenue['business_id']        = $biz_id;
                $data_bis_revenue['transaction_amount'] = $pay_amount;
                $data_bis_revenue['commission_percent'] = 0;
                $data_bis_revenue['total_amount']       = $pay_amount;
                $data_bis_revenue['created']            = date('Y-m-d H:i:s');
                $data_bis_revenue['modified']           = date('Y-m-d H:i:s');

                $this->BusinessRevenue->create();
                $this->BusinessRevenue->save($data_bis_revenue);

                $this->Session->setFlash(__( 'Payment has been successfully saved.')
                        .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                      'default',
                      array('class'=>'alert alert-dismissable alert-success '));

                return $this->redirect( array( 'action' => 'makepayment' ));

            }

        }else{
          $this->Session->setFlash(__( 'Something went wrong while trying to process payment ! Please try again.')
                        .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                      'default',
                      array('class'=>'alert alert-dismissable alert-danger '));

          return $this->redirect( array( 'action' => 'makepayment' ));
        }

      }else{

          $this->Session->setFlash(__( 'Something went wrong while trying to process payment ! Please try again.')
                        .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                      'default',
                      array('class'=>'alert alert-dismissable alert-danger '));

          return $this->redirect( array( 'action' => 'makepayment' ));
      }     

  }

  public function view( $id = 0 ){
      
      if( !$id ){
          $this->Session->setFlash(__( 'Invalid Request! Please try again.')
                        .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                      'default',
                      array('class'=>'alert alert-dismissable alert-danger '));

          return $this->redirect( array( 'action' => 'payment' ));
      }

      $conds['conditions'] = array( 'PaymentHistory.id' => $id );
      $data = $this->PaymentHistory->find('first', $conds);

      $this->set('data', $data);

      $dealData = array();

      if( $data ){
        $deal_ids = explode(",", $data['PaymentHistory']['deal_ids']);
        $dealData = $this->Deal->find('all', array('conditions' => array('Deal.id' => $deal_ids) ) );
      }

      $this->set('dealData', $dealData);

  }




}