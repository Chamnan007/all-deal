<?php
App::uses('AdministratorAppController', 'Administrator.Controller');
App::uses('Business', 'Administrator.Model');
App::uses('Deal', 'Administrator.Model');
App::uses('User', 'Administrator.Model');

/**
 * Locations Controller
 *
 * @property Location $Location
 * @property PaginatorComponent $Paginator
 */
class ReportsController extends AdministratorAppController {


  var $uses = array( 'Administrator.User', 'Administrator.Business', 'Administrator.Deal' ) ;

  public function index(){

    $user = CakeSession::read("Auth.User");

    $this->set('user_access_level', $user['access_level']);

    $this->set('my_id', $user['id']) ;

    $this->set('user_id', '' ) ;
    $this->set('from', '' ) ;
    $this->set('to', '') ;

    if( $user['access_level'] == 6 || $user['access_level'] == 5 ){
      
      $user_conditions = array( 'conditions' => array( 
                    'OR' => array(
                      'User.access_level = 4', 
                      'User.access_level = 5', 
                      'User.access_level = 6', 
                      'User.access_level = 0'
                      ),
                    'User.id != ' . $user['id'],
                    'User.business_id' => NULL
                    ),
                  'order' => array(
                      'User.last_name'  => 'ASC'
                      )
                );

      $this->set("users", $this->User->find('all', $user_conditions ) );

    }

    if( $this->request->is('post')){

      $user_id = "";

      if( $user['access_level'] == 6 ||  $user['access_level'] == 5 ){
        if(isset($this->request->data['user'])){
          $user_id = $this->request->data['user'];
        }
      }else{
        $user_id = array($user['id']);
      }

      $arr_user = $user_id;

      if( !empty($user_id) && in_array("all", $user_id) ){
        $user_id = "";
      }

      $from   = $this->request->data['from_date'];
      $to   = $this->request->data['to_date'];

      $conditions = array();

      $conditions['conditions']['OR'] = array(
                          'user.access_level = 4', 
                          'user.access_level = 5', 
                          'user.access_level = 6', 
                          'user.access_level = 0'
                        );

      $deal_conditions['conditions']['OR'] = array(
                          'user.access_level = 4', 
                          'user.access_level = 5', 
                          'user.access_level = 6',
                          'user.access_level = 0'
                        );

      if( $user_id != "" ){
        $conditions['conditions']['Business.registered_by'] = $user_id;
        $deal_conditions['conditions']['Deal.created_by'] = $user_id;
      }

      $conditions['conditions']['Business.status !='] = '-1' ;
      $deal_conditions['conditions']['Deal.status !='] = '0' ;

      if( $from != "" && $to == "" ){
        $from = date('Y-m-d 00:00:00', strtotime($from) );
        $conditions['conditions'][] = array( "Business.created >='" . $from . "' " );
        $deal_conditions['conditions'][] = array( "Deal.created >='" . $from . "' " );

      }else if ( $from == "" && $to != "" ) {
        $to = date('Y-m-d 23:59:59', strtotime($to) );
        $conditions['conditions'][] = array( "Business.created <='" . $to . "' " );
        $deal_conditions['conditions'][] = array( "Deal.created <='" . $to . "' " );

      }else if( $from != "" && $to != "" ){ 
        $from = date('Y-m-d 00:00:00', strtotime($from) );
        $to = date('Y-m-d 23:59:59', strtotime($to) );
        $conditions['conditions'][] = array( "Business.created BETWEEN '" . $from . "' AND '" . $to . "' " );
        $deal_conditions['conditions'][] = array( "Deal.created BETWEEN '" . $from . "' AND '" . $to . "' " );
      } 

      $this->set('user_id', $arr_user);
      $this->set('from', $from);
      $this->set('to', $to);


      $conditions['group'] = array("Business.registered_by");
      $conditions['fields'] = array("Business.registered_by", "COUNT(Business.id) AS total", "user.first_name", "user.last_name");
      $conditions['order'] = array("user.last_name ASC");

      $deal_conditions['group'] = array("Deal.created_by");
      $deal_conditions['fields'] = array("Deal.created_by", "COUNT(Deal.id) AS total", "user.first_name", "user.last_name");
      $deal_conditions['order'] = array("user.last_name ASC");

      $conditions['joins'] = array(
              array( 'table' => 'users',
                    'alias' => 'user',
                    'type' => 'LEFT',
                    'conditions' => array(
                              'user.id = Business.registered_by'
                          ),
                    'fields' => array( "id", "first_name", 'last_name', 'access_level', 'business_id')
                )
            );

      $deal_conditions['joins'] = array(
              array( 'table' => 'users',
                    'alias' => 'user',
                    'type' => 'LEFT',
                    'conditions' => array(
                              'user.id = Deal.created_by'
                          ),
                    'fields' => array( "id", "first_name", 'last_name', 'access_level', 'business_id')
                )
            );

      // var_dump($conditions); exit;

      $this->Business->recursive = -1;
      $business = $this->Business->find( 'all', $conditions );
      $this->set('result', $business);

      $this->Deal->recursive = -1;
      $deal = $this->Deal->find( 'all', $deal_conditions );
      $this->set('deal_result', $deal);

    }

  }

}
