<?php
App::uses('AdministratorAppController', 'Administrator.Controller');
/**
 * NewsCategories Controller
 *
 * @property NewsCategory $NewsCategory
 * @property PaginatorComponent $Paginator
 */
class NewsCategoriesController extends AdministratorAppController {

/**
 * Components
 *
 * @var array
 */
	var $context = 'NewsCategory';

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->NewsCategory->recursive = 0;
		
		$this->conditionFilter['status'] = 1;
		$this->paginate['order'] = array("NewsCategory.category" => 'ASC' ) ;
		$this->paginate['conditions'] = $this->conditionFilter;
		parent::index();
	}



public function beforeFilter(){
		$user =  CakeSession::read("Auth.User"); 

		if( $user['access_level'] != 6 ){
			$this->redirect(array('action' => 'index', 'controller' => 'dashboards' ));
		}
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->NewsCategory->exists($id)) {
			throw new NotFoundException(__('Invalid news category'));
		}
		$options = array('conditions' => array('NewsCategory.' . $this->NewsCategory->primaryKey => $id));
		$this->set('newsCategory', $this->NewsCategory->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		// if ($this->request->is('post')) {
		// 	$this->NewsCategory->create();
		// 	if ($this->NewsCategory->save($this->request->data)) {
		// 		$this->Session->setFlash(__('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button><b>Success!!!</b> The category has been saved.</div>'));
		// 		return $this->redirect(array('action' => 'index'));
		// 	} else {
		// 		$this->Session->setFlash(__('<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button><b>Failed!!!</b> The category could not be saved. Please, try again.</div>'));
		// 	}
		// }

		if ($this->request->is('post')) {
			parent::save();
		}
		
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->NewsCategory->exists($id)) {
			throw new NotFoundException(__('Invalid news category'));
		}
		if ($this->request->is(array('post', 'put'))) {
			parent::save($id);
		} else {
			$options = array('conditions' => array('NewsCategory.' . $this->NewsCategory->primaryKey => $id));
			$this->request->data = $this->NewsCategory->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {

		if (!$this->NewsCategory->exists($id)) {
			throw new NotFoundException(__('Invalid Category'));
		}

		$this->NewsCategory->id = $id;
		
		if ($this->NewsCategory->save(array('status'=>0))) {
			$this->Session->setFlash(__('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button><b>Success!!!</b> The Goods Category has been deleted.</div>'));

			$obj 	= $this->NewsCategory->findById($id);
			$logMessage = json_encode($obj);
			parent::generateLog($logMessage,' DELETE :'.$id);

		} else {
			$this->Session->setFlash(__('<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button><b>Failed!!!</b> The Goods Category could not be deleted. Please, try again.</div>'));
		}

		return $this->redirect(array('action' => 'index'));
	}

}
