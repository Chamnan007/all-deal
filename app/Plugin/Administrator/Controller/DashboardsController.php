<?php
App::uses('AdministratorAppController', 'Administrator.Controller');
App::uses('Business', 'Administrator.Model');
App::uses('Deal', 'Administrator.Model');
App::uses('GoodsCategory', 'Administrator.Model');
/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class DashboardsController extends AdministratorAppController {

/**
 * Components
 *
 * @var array
 */
  public $components = array('Paginator');

  var $uses = array(  'Administrator.Business', 
                      'Administrator.Deal', 
                      'Administrator.DealExpandRequest', 
                      'Administrator.GoodsCategory', 
                      'Administrator.SystemRevenue' );


/**
 * index method
 *
 * @return void
 */

  public function beforeFilter(){
      parent::beforeFilter();

    $goods = new GoodsCategory();
    $this->set('goods', $goods->find('all', array(  'conditions'=> array('GoodsCategory.status' => 1), 
                                                    'order' => array('GoodsCategory.category' => 'ASC' ) 
                                                  )
                                      ));
  }

  public function index(){

      $user = CakeSession::read("Auth.User");

      $from = "";
      $to   = "";

      if( isset($this->request->data['purchaseDateFrom']) ){
          $this->setState( 'dateFrom', $this->request->data['purchaseDateFrom'] );
      }

      if( isset($this->request->data['purchaseDateTo']) ){
          $this->setState( 'dateTo', $this->request->data['purchaseDateTo'] );
      }

      if( $this->getState('dateFrom', NULL) ){
          $from = $this->getState('dateFrom');
          $from = date('Y-m-d', strtotime($from));
      }

      if( $this->getState('dateTo', NULL) ){
          $to   = $this->getState('dateTo');
          $to   = date('Y-m-d', strtotime($to));
      }

      if( $user['access_level'] == 6 || $user['access_level'] == 5 ){

          $biz_conditions           = array( 'conditions' => array("Business.status"  => 0));
          $deal_conditions          = array( 'conditions' => array( "Deal.isdeleted"  => 0 ) );
          $deal_pending_conditions  = array( 'conditions' => array( "Deal.status"     => 0 ) );
          $tota_biz_cond            = NULL ;

      }else{
          $biz_conditions           = array( 'conditions' => array("Business.status"  => 0, "Business.registered_by" => $user['id'] ) );
          $deal_conditions          = array( 'conditions' => array( "Deal.created_by" => $user['id'], "Deal.isdeleted" => 0 ) );
          $tota_biz_cond            = array( 'conditions' => array( "Business.registered_by" => $user['id'] ) );
          $deal_pending_conditions  = array( 'conditions' => array( "Deal.created_by" => $user['id'], "Deal.status" => 0 ) );
      }

      if( $from ){
        $this->set('dateFrom', $from);
        $from = date('Y-m-d 00:00:00', strtotime($from)) ;

        $biz_conditions['conditions']['Business.created >=']  = $from ;
        $tota_biz_cond['conditions']['Business.created >=']   = $from ;
        $deal_conditions['conditions']['Deal.created >=']     = $from ;
        $deal_pending_conditions['conditions']['Deal.created >='] = $from ;
      }

      if( $to ){
        $this->set('dateTo', $to);
        $to = date('Y-m-d 23:59:59', strtotime($to));

        $biz_conditions['conditions']['Business.created <=']  = $to ;
        $tota_biz_cond['conditions']['Business.created <=']   = $to ;
        $deal_conditions['conditions']['Deal.created <=']     = $to ;
        $deal_pending_conditions['conditions']['Deal.created <='] = $to ;
      }

      $deal_pending_conditions['order'] = array( 'Deal.created' => 'DESC' );

      $this->Business->recursive = -1;
      $pending_business = $this->Business->find('all', $biz_conditions);
      $total_biz = $this->Business->find('count', $tota_biz_cond);
      $total_deal = $this->Deal->find('count', $deal_conditions);
      $deal_pending = $this->Deal->find('all', $deal_pending_conditions);

      $this->set("pending_biz", $pending_business);
      $this->set("total_biz", $total_biz);
      $this->set("total_deal", $total_deal);
      $this->set("pending_deals", $deal_pending);
      $this->set('deal_status', $this->deal_status);

      // Get Total Sale
      if( $from ){
        $saleConds['conditions']['SystemRevenue.created >='] = $from;
      }
      if( $to ){
        $saleConds['conditions']['SystemRevenue.created <='] = $to;
      }
      $saleConds['fields']      = array( 'SUM(transaction_amount) AS total' );
      $totalSale = $this->SystemRevenue->find('first', $saleConds );
      $this->set('total_sale', $totalSale[0]['total'] );

      // Get Total Commission
      if( $from ){
        $comConds['conditions']['SystemRevenue.created >='] = $from;
      }
      if( $to ){
        $comConds['conditions']['SystemRevenue.created <='] = $to;
      }
      $comConds['fields']     = array( 'SUM(total_amount) AS total');
      $totalCommission = $this->SystemRevenue->find('first', $comConds );
      $this->set('total_commission', $totalCommission[0]['total'] );

      // Get Total Account Payable      
      if( $from ){
        $saleConds['conditions']['Business.created >='] = $from;
      }
      if( $to ){
        $saleConds['conditions']['Business.created <='] = $to;
      }
      $conds['conditions']['Business.status'] = 1 ;
      $conds['fields']     = array( 'SUM(ending_balance) AS total' );
      $total = $this->Business->find('first', $conds);
      $this->set('total_account_payable', $total[0]['total'] );

      $period_string = "Summary Information" ;

      if( $from && !$to ){
        $period_string = "Information from " . date('d-F-Y', strtotime($from)) . " till now"  ;
      }else if( !$from && $to ){
        $period_string = "Information from beginning to " . date('d-F-Y', strtotime($to)) ;
      }else if( $from && $to ){
        $period_string = "Information from " . date('d-F-Y', strtotime($from)) . " to " . date('d-F-Y', strtotime($to))  ;
      }

      $this->set('period_string', $period_string);

  }
  


}