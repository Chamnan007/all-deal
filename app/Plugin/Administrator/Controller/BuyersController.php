<?php
App::uses('AdministratorAppController', 'Administrator.Controller');
/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class BuyersController extends AdministratorAppController {


/**
 * Components
 *
 * @var array
*/

  // var $components = array('Paginator');
  //public $components = array('Email');

  var $context = "Buyer";
  var $uses = array(  'Administrator.Buyer', 
                      'Administrator.BuyerTransaction', 
                      'Administrator.Province', 
                      'Administrator.City',
                      'Administrator.MailConfigure' );


  var $senderEmail = '';

/**
 * index method
 *
 * @return void
 */

  var $user = "" ;
  var $transaction_limit = 30;

  public function setCondition(){

        parent::setCondition();

        $status = $this->getState('status', '0');

        if( isset($this->request->data['status']) ){
            $status = $this->request->data['status'];
        }

        if( $status ){
            $this->setState('status', $status);
        }

        if( isset($this->request->data['code']) ){
            $this->setState('code', $this->request->data['code'] );
        }

        if( isset($this->request->data['purchaseDateFrom']) ){
            $this->setState('purchaseDateFrom', $this->request->data['purchaseDateFrom'] );
        }
        if( isset($this->request->data['purchaseDateTo']) ){
            $this->setState('purchaseDateTo', $this->request->data['purchaseDateTo'] );
        }

        if( isset($this->request->data['receivedDateFrom']) ){
            $this->setState('receivedDateFrom', $this->request->data['receivedDateFrom'] );
        }

        if( isset($this->request->data['receivedDateTo']) ){
            $this->setState('receivedDateTo', $this->request->data['receivedDateTo'] );
        }

        if( $code = $this->getState('code', NULL )){
            $this->conditionFilter['BuyerTransaction.code'] = $code ;            
        }

        if( $purchaseFrom = $this->getState('purchaseDateFrom', NULL ) ){
            $purchaseFrom = date('Y-m-d 00:00:00', strtotime($purchaseFrom));
            $this->conditionFilter['BuyerTransaction.created >='] = $purchaseFrom;
        }

        if( $purchaseTo = $this->getState('purchaseDateTo', NULL ) ){
            $purchaseTo = date('Y-m-d 23:59:59', strtotime($purchaseTo));
            $this->conditionFilter['BuyerTransaction.created <='] = $purchaseTo;
        }

        if( $receivedFrom = $this->getState('receivedDateFrom', NULL ) ){
            $receivedFrom = date('Y-m-d 00:00:00', strtotime($receivedFrom));
            $this->conditionFilter['BuyerTransaction.received_date >='] = $receivedFrom;
        }

        if( $receivedTo = $this->getState('receivedDateTo', NULL ) ){
            $receivedTo = date('Y-m-d 23:59:59', strtotime($receivedTo));
            $this->conditionFilter['BuyerTransaction.received_date <='] = $receivedTo;
        }

        if( $status != 'all' ){
            $this->conditionFilter['BuyerTransaction.status'] = $status;
        }


        $this->set('states', $this->getState());

    }

  public function SearchConditons(){

        $user_status = $this->getState('user-status', 'all');

        if( isset($this->request->data['user-status']) ){
            $user_status = $this->request->data['user-status'];
        }

        if( $user_status ){
            $this->setState('user-status', $user_status);
        }

        if( isset($this->request->data['user-search-text']) ){
            $this->setState('user-search-text', $this->request->data['user-search-text']);
        }

        if( isset($this->request->data['province-code']) ){
            $this->setState('province-code', $this->request->data['province-code']);
        }

        if( isset($this->request->data['city-code']) ){
            $this->setState('city-code', $this->request->data['city-code']);
        }

        if( isset($this->request->data['registeredFrom']) ){
            $this->setState('registeredFrom', $this->request->data['registeredFrom'] );
        }
        if( isset($this->request->data['registeredTo']) ){
            $this->setState('registeredTo', $this->request->data['registeredTo'] );
        }

        $type = isset($this->request->data['type'])?$this->request->data['type']:"";

        if( $type && $type == 'free' ){
            $this->setState('province-code', NULL);
            $this->setState('city-code', NULL);
            $this->setState('registeredFrom', NULL);
            $this->setState('registeredTo', NULL);
        }

        if( $user_status != "all" ){
            $conds['Buyer.status'] = $user_status;
        }

        if( $search_text = $this->getState('user-search-text', NULL ) ){
            $conds['OR'] =  array(  'Buyer.buyer_code LIKE' => '%' . $search_text .'%',
                                    'Buyer.full_name LIKE' => '%' . $search_text .'%',
                                    'Buyer.email LIKE' => '%' . $search_text .'%',
                                    'Buyer.phone1 LIKE' => '%' . $search_text .'%',
                                    'Buyer.phone2 LIKE' => '%' . $search_text .'%'
                                  ) ;
        }

        if( $registeredFrom = $this->getState('registeredFrom', NULL ) ){
            $registeredFrom = date('Y-m-d 00:00:00', strtotime($registeredFrom));
            $this->conditionFilter['Buyer.created >='] = $registeredFrom;
        }

        if( $registeredTo = $this->getState('registeredTo', NULL ) ){
            $registeredTo = date('Y-m-d 23:59:59', strtotime($registeredTo));
            $this->conditionFilter['Buyer.created <='] = $registeredTo;
        }

        if( $province_code = $this->getState('province-code', NULL ) ){
            $conds['Buyer.province_code'] = $province_code;
        }

        if( $city_code = $this->getState('city-code', NULL ) ){
            $conds['Buyer.city_code'] = $city_code;
        }

        $this->paginate['conditions'] = $conds;

        $this->set('states', $this->getState());
  }


  public function beforeFilter(){

      parent::beforeFilter();

      $this->user = CakeSession::read("Auth.User");
      $this->set('user', $this->user);   

      $this->set('status', $this->status);   

      $this->set('payment_method', $this->payment_method);
      $this->set('buy_transaction_status', $this->buy_transaction_status);
      
      $this->senderEmail = $this->admin_email;
  }

  public function SearchConditonsTopUp(){

        $user_status = $this->getState('topup-user-status', '1');

        if( isset($this->request->data['topup-user-status']) ){
            $user_status = $this->request->data['topup-user-status'];
        }

        
        $this->setState('topup-user-status', $user_status);

        if( isset($this->request->data['topup-user-search-text']) ){
            $this->setState('topup-user-search-text', $this->request->data['topup-user-search-text']);
        }

        if( isset($this->request->data['topup-province-code']) ){
            $this->setState('topup-province-code', $this->request->data['topup-province-code']);
        }

        if( isset($this->request->data['topup-city-code']) ){
            $this->setState('topup-city-code', $this->request->data['topup-city-code']);
        }

        if( isset($this->request->data['topup-registeredFrom']) ){
            $this->setState('topup-registeredFrom', $this->request->data['topup-registeredFrom'] );
        }
        if( isset($this->request->data['topup-registeredTo']) ){
            $this->setState('topup-registeredTo', $this->request->data['topup-registeredTo'] );
        }

        $type = isset($this->request->data['topup-type'])?$this->request->data['topup-type']:"";

        if( $type && $type == 'free' ){
            $this->setState('province-code', NULL);
            $this->setState('city-code', NULL);
            $this->setState('registeredFrom', NULL);
            $this->setState('registeredTo', NULL);
        }

        $user_status = $this->getState('topup-user-status', 1);

        if( $user_status != "all" ){
            $conds['Buyer.status'] = $user_status;
        }

        if( $search_text = $this->getState('topup-user-search-text', NULL ) ){
            $conds['OR'] =  array(  'Buyer.buyer_code LIKE' => '%' . $search_text .'%',
                                    'Buyer.full_name LIKE' => '%' . $search_text .'%',
                                    'Buyer.email LIKE' => '%' . $search_text .'%',
                                    'Buyer.phone1 LIKE' => '%' . $search_text .'%',
                                    'Buyer.phone2 LIKE' => '%' . $search_text .'%'
                                  ) ;
        }

        if( $registeredFrom = $this->getState('topup-registeredFrom', NULL ) ){
            $registeredFrom = date('Y-m-d 00:00:00', strtotime($registeredFrom));
            $this->conditionFilter['Buyer.created >='] = $registeredFrom;
        }

        if( $registeredTo = $this->getState('topup-registeredTo', NULL ) ){
            $registeredTo = date('Y-m-d 23:59:59', strtotime($registeredTo));
            $this->conditionFilter['Buyer.created <='] = $registeredTo;
        }

        if( $province_code = $this->getState('topup-province-code', NULL ) ){
            $conds['Buyer.province_code'] = $province_code;
        }

        if( $city_code = $this->getState('topup-city-code', NULL ) ){
            $conds['Buyer.city_code'] = $city_code;
        }

        $this->paginate['conditions'] = $conds;

        $this->set('states', $this->getState());
  }

  public function topup(){

      $user = CakeSession::read("Auth.User"); 

      if( $user['access_level'] != 6 ){
        return $this->redirect(array( 'controller' => 'dashboards' ,'action' => 'index' ));
      }

      $this->SearchConditonsTopUp();
      $this->paginate['order'] = array("Buyer.id" => 'DESC' ) ;
      $this->paginate['joins'] =  array(
                                    array('table' => 'cities',
                                        'alias' => 'City',
                                        'type' => 'LEFT',
                                        'conditions' => array('Buyer.city_code = City.city_code')
                                      ),

                                    array('table' => 'provinces',
                                        'alias' => 'Province',
                                        'type' => 'LEFT',
                                        'conditions' => array('Buyer.province_code = Province.province_code')
                                      )

                                    );
      $this->paginate['fields'] = array('Buyer.*','City.*','Province.*', 'SangkatInfo.*');

      $data = $this->paginate($this->getContext());
      $this->set('provinces', $this->Province->find('all', array('conditions'=> array('Province.status' => 1) )));

      parent::index();
      
  }

  public function index(){

      $this->SearchConditons();

      $this->paginate['order'] = array("Buyer.id" => 'DESC' ) ;
      $this->paginate['joins'] =  array(
                                    array('table' => 'cities',
                                        'alias' => 'City',
                                        'type' => 'LEFT',
                                        'conditions' => array('Buyer.city_code = City.city_code')
                                      ),

                                    array('table' => 'provinces',
                                        'alias' => 'Province',
                                        'type' => 'LEFT',
                                        'conditions' => array('Buyer.province_code = Province.province_code')
                                      )

                                    );
      $this->paginate['fields'] = array('Buyer.*','City.*','Province.*', 'SangkatInfo.*');

      $data = $this->paginate($this->getContext());
      parent::index() ;

      $this->set('provinces', $this->Province->find('all', array('conditions'=> array('Province.status' => 1) )));

  }

  public function view( $id = 0, $st = NULL, $page = 1 ){

      $this->setCondition();

      $conds['joins'] =  array(
                            array('table' => 'cities',
                                'alias' => 'City',
                                'type' => 'LEFT',
                                'conditions' => array('Buyer.city_code = City.city_code')
                              ),

                            array('table' => 'provinces',
                                'alias' => 'Province',
                                'type' => 'LEFT',
                                'conditions' => array('Buyer.province_code = Province.province_code')
                              ),

                            array('table' => 'buyers',
                                'alias' => 'RefererInfo',
                                'type' => 'LEFT',
                                'conditions' => array('Buyer.referer_id = RefererInfo.id')
                              )

                            );

      $conds['fields'] = array('Buyer.*','City.*','Province.*', 'RefererInfo.*', 'SangkatInfo.*' );
      $conds['conditions'] = array('Buyer.id' => $id);
      $data = $this->Buyer->find('first', $conds );
      $this->set('data', $data);
      
      // Get History
      $prev = false;
      $next = false;

      $conds = array();
      $this->conditionFilter['BuyerTransaction.buyer_id'] = $id;

      $conds['conditions'] = $this->conditionFilter;
      $conds['order']      = array( 'BuyerTransaction.created' => 'DESC', 'BuyerTransaction.code' => 'DESC'  );
      $limit = $this->transaction_limit;

      $offset = ($page - 1) * $limit ;

      $conds['limit'] = $limit + 1;
      $conds['offset'] = $offset;

      // var_dump($conds); exit;
      $histories = $this->BuyerTransaction->find('all', $conds);

      if( count($histories) > $limit ){
          $next = true;
          unset($histories[$limit]);
      }

      if( $page > 1){
         $prev = true;
      }

      // Get Balance History
      $this->BuyerTransaction->recursive = -1;
      $balanceHistory = $this->BuyerTransaction->find('all', array( 'conditions' => array( 
                                                                                      'BuyerTransaction.buyer_id' => $id,
                                                                                      'BuyerTransaction.type'     => 1
                                                                                    ),
                                                                    'order'     => array( 'BuyerTransaction.created' => 'DESC' )
                                                                    )
                                                                );

      $this->set('balanceHistory', $balanceHistory);

      $this->set('histories', $histories);
      $this->set('prev', $prev);
      $this->set('next', $next);
      $this->set('page', $page);

  }

  public function saveTopup( $buyer_id = 0, $detail = false  ){
      $user = CakeSession::read("Auth.User"); 

      if( $user['access_level'] != 6 ){
        return $this->redirect(array( 'controller' => 'dashboards' ,'action' => 'index' ));
      }

      $returnURL = array( 'action' => 'topup' );

      if( $detail ){
        $returnURL = array( 'action' => 'topupView', $buyer_id, 'balance' );
      }

      if( $this->request->is('post') ){

        $data = $this->request->data;

        $saveData = array();
        $saveData = $data;
        $saveData['Buyer']['id'] = $buyer_id;
        $balance = $data['Buyer']['amount_balance'];

        $dataInfo = $this->Buyer->findById($buyer_id);

        if( $dataInfo ){

          $old_balance = $dataInfo['Buyer']['amount_balance'];
          $saveData['Buyer']['amount_balance'] = $old_balance + $saveData['Buyer']['amount_balance'];

          if( $this->Buyer->save($saveData) ){

              // Save Transaction History
              $tranData = array();
              $tranData['BuyerTransaction'] = array( 
                                                  'business_id'   => 0,
                                                  'deal_id'       => 0,
                                                  'code'          => NULL,
                                                  'buyer_id'      => $buyer_id,
                                                  'type'          => 1,
                                                  'description'   => $this->payment_method[3],
                                                  'amount'        => $balance,
                                                  'commission_percent' => 0,
                                                  'payment_type'  => 3,
                                                  'credit_card_number' => NULL,
                                                  'status'        => 1
                                                );

              $this->BuyerTransaction->save($tranData);

              $logMessage = json_encode($saveData);
              parent::generateLog( $logMessage, 'Topped Up' );

              $this->Session->setFlash(__( 'Balance has been topped up to user name: <strong>[' . $dataInfo["Buyer"]['buyer_code'] . "] - " . $dataInfo['Buyer']['full_name'] . "</strong>" )
                                      .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                                      'default',
                                      array('class'=>'alert alert-dismissable alert-success '));

          }else{
              $this->Session->setFlash(__( 'Topup Failed !')
                                      .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                                      'default',
                                      array('class'=>'alert alert-dismissable alert-warning '));
          }

          return $this->redirect($returnURL);  

        }else{

          return $this->redirect($returnURL);  
        }

      }else{
        return $this->redirect($returnURL);
      }

  }

  public function topupView( $id = 0, $st = NULL, $page = 1 ){

      $user = CakeSession::read("Auth.User"); 

      if( $user['access_level'] != 6 ){
        return $this->redirect(array( 'controller' => 'dashboards' ,'action' => 'index' ));
      }

      $this->setCondition();

      $conds['joins'] =  array(
                                    array('table' => 'cities',
                                        'alias' => 'City',
                                        'type' => 'LEFT',
                                        'conditions' => array('Buyer.city_code = City.city_code')
                                      ),

                                    array('table' => 'provinces',
                                        'alias' => 'Province',
                                        'type' => 'LEFT',
                                        'conditions' => array('Buyer.province_code = Province.province_code')
                                      ),

                                    array('table' => 'buyers',
                                        'alias' => 'RefererInfo',
                                        'type' => 'LEFT',
                                        'conditions' => array('Buyer.referer_id = RefererInfo.id')
                                      )

                                    );

      $conds['fields'] = array('Buyer.*','City.*','Province.*', 'RefererInfo.*', 'SangkatInfo.*');
      $conds['conditions'] = array('Buyer.id' => $id);
      $data = $this->Buyer->find('first', $conds );
      $this->set('data', $data);
      
      // Get History
      $prev = false;
      $next = false;

      $conds = array();
      $this->conditionFilter['BuyerTransaction.buyer_id'] = $id;

      $conds['conditions'] = $this->conditionFilter;
      $conds['order']      = array( 'BuyerTransaction.created' => 'DESC', 'BuyerTransaction.code' => 'DESC'  );
      $limit = $this->transaction_limit;

      $offset = ($page - 1) * $limit ;

      $conds['limit'] = $limit + 1;
      $conds['offset'] = $offset;

      $histories = $this->BuyerTransaction->find('all', $conds);

      if( count($histories) > $limit ){
          $next = true;
          unset($histories[$limit]);
      }

      if( $page > 1){
         $prev = true;
      }

      // Get Balance History
      $this->BuyerTransaction->recursive = -1;
      $balanceHistory = $this->BuyerTransaction->find('all', array( 'conditions' => array( 
                                                                                      'BuyerTransaction.buyer_id' => $id,
                                                                                      'BuyerTransaction.type'     => 1
                                                                                    ),
                                                                    'order'     => array( 'BuyerTransaction.created' => 'DESC' )
                                                                    )
                                                                );

      $this->set('balanceHistory', $balanceHistory);
      $this->set('histories', $histories);
      $this->set('prev', $prev);
      $this->set('next', $next);
      $this->set('page', $page);

  }

  public function Purchase( $purchase_id = 0){

     if( !$purchase_id ){
        return $this->redirect(array('action' => 'index' ));
     }

     $conds['conditions'] = array( 'BuyerTransaction.id' => $purchase_id );
     $this->BuyerTransaction->recursive = 2;
     $data = $this->BuyerTransaction->find('first', $conds);

     $this->set('data', $data);

  }

  public function save( $type_id = 0 ){
      $data = $this->request->data;
      $data['Commission']['type'] = $type_id;

      $conds  = array('conditions' => array('type' => $type_id) );
      $result = $this->Commission->find('first', $conds);

      if( $result && !empty($result) ){
          $id = $result['Commission']['id'];
          $data['Commission']['id'] = $id;
      }

      $this->Commission->create();

      if( $this->Commission->save($data) ){

          $action = " ADD ";
          if( isset($id) ){
              $action = " EDIT ";
          }

          $logMessage = json_encode($data);
          parent::generateLog( $logMessage, $action );

          $this->Session->setFlash(__( 'Commission has been saved.')
                                  .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                                  'default',
                                  array('class'=>'alert alert-dismissable alert-success '));

      }

      return $this->redirect(array('action' =>'index'));
      
  }  

  public function changeState( $id = 0, $state = 1 ){
    if( !$this->request->is('post') ){
      return $this->redirect(array('action' => 'index' ));
    }

    $data['Buyer']['id'] = $id;
    $data['Buyer']['status'] = $state;

    if( $this->Buyer->save($data) ){

        if( $state == 1 ){

            $this->Session->setFlash(__( 'Buyer has been activated.')
                                  .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                                  'default',
                                  array('class'=>'alert alert-dismissable alert-success '));

            // Send Email
            $info   = $this->Buyer->findById($id);
            $email  = $info['Buyer']['email'];

            $configuration = $this->MailConfigure->find('first', array( 'conditions' => array('business_id' => NULL) ));
            $signature     = $configuration['MailConfigure']['signature'];

            // Send Verification Mail
            $receiver   = $email;
            $url        = Router::url("/shops/signin", true );
            $login_link = "<a href='" . $url . "' target='_blank'>Sign In</a>";
            $subject    = "Account Activation";
            $content    = "";

            $content    .= "<html><body>";
            $content    .= "<p>Dear " . $info['Buyer']['full_name'] . ", </p>";
            $content    .= "<p>Your account is now activated.</p><br/>";
            $content    .= "<p>Sign In here: " . $login_link . "</p>";
            $content    .= @$signature ;
            $content    .= "</body></html>";

            $sendMail =  $this->sendMail($receiver, $subject, $content);


        }else{

            $this->Session->setFlash(__( 'Buyer has beed deactivated.')
                                  .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                                  'default',
                                  array('class'=>'alert alert-dismissable alert-success '));
        }
        
    }

    return $this->redirect(array('action' => 'index' ));

  }

  public function sendMail( $recipients = NULL, $subject = NULL , $content = NULL ) {

      //return parent::sendMail($recipients, $subject, $content);

  }


  public function getTransactionDetailAjax__(){
    
    $result['status'] = false;

    if( $this->request->is('post') ){
      $tranID = $this->request->data['tranID'];

      $this->BuyerTransaction->recursive = 3;

      $conds['conditions'] = array('BuyerTransaction.id' => $tranID);
      $data = $this->BuyerTransaction->find('first', $conds);

      if( $data ){
        $result['status']   = true;
        $result['data']   = $data;
      }

    }

    echo json_encode($result); exit;
  }


}