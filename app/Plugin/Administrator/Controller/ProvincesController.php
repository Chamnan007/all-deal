<?php
App::uses('AdministratorAppController', 'Administrator.Controller');
/**
 * Provinces Controller
 *
 * @property Province $Province
 * @property PaginatorComponent $Paginator
 */
class ProvincesController extends AdministratorAppController {

/**
 * Components
 *
 * @var array
 */

/**
 * index method
 *
 * @return void
 */
	var $context = 'Province';
	// var $uses  = array('Administrator.Province');

	public function index() {

		$user =  CakeSession::read("Auth.User"); 

		if( $user['access_level'] != 6 ){
			$this->redirect(array('action' => 'index', 'controller' => 'dashboards' ));
		}
		
		$this->Province->recursive = 0;
		$this->conditionFilter['status'] = 1;
		$this->paginate['conditions'] = $this->conditionFilter;
		$this->paginate['order'] = array("Province.province_code" => 'ASC' ) ;
		parent::index();
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$user =  CakeSession::read("Auth.User"); 

		if( $user['access_level'] != 6 ){
			$this->redirect(array('action' => 'index', 'controller' => 'dashboards' ));
		}

		if (!$this->Province->exists($id)) {
			throw new NotFoundException(__('Invalid province'));
		}
		$options = array('conditions' => array('Province.' . $this->Province->primaryKey => $id));
		$this->set('province', $this->Province->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$user =  CakeSession::read("Auth.User"); 

		if( $user['access_level'] != 6 ){
			$this->redirect(array('action' => 'index', 'controller' => 'dashboards' ));
		}

		if ($this->request->is('post')) {
			parent::save();
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {

		$user =  CakeSession::read("Auth.User"); 

		if( $user['access_level'] != 6 ){
			$this->redirect(array('action' => 'index', 'controller' => 'dashboards' ));
		}

		if (!$this->Province->exists($id)) {
			throw new NotFoundException(__('Invalid province'));
		}

		if ($this->request->is(array('post', 'put'))) {
			parent::save($id);
		} else {
			$options = array('conditions' => array('Province.' . $this->Province->primaryKey => $id));
			$this->request->data = $this->Province->find('first', $options);
		}
	}


	public function delete($id = null) {

		$user =  CakeSession::read("Auth.User"); 

		if( $user['access_level'] != 6 ){
			$this->redirect(array('action' => 'index', 'controller' => 'dashboards' ));
		}

		if (!$this->Province->exists($id)) {
			throw new NotFoundException(__('Invalid province'));
		}

		$this->Province->id = $id;
		
		if ($this->Province->save(array('status'=>0))) 
		{
			$this->Session->setFlash(__('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button><b>Success!!!</b> The province has been deleted.</div>'));

			$obj 	= $this->Province->findById($id);
			$logMessage = json_encode($obj);
			parent::generateLog($logMessage,' DELETE :'.$id);

		} else {
			 // debug($this->Province->invalidFields());

			$this->Session->setFlash(__('<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button><b>Failed!!!</b> The province could not be deleted. Please, try again.</div>'));
		}

		// var_dump($this->request->data); exit;
	
		return $this->redirect(array('action' => 'index'));
	}

	public function checkDuplicateCode( $code = NULL, $id = NULL ){

		$user =  CakeSession::read("Auth.User"); 

		if( $user['access_level'] != 6 ){
			$this->redirect(array('action' => 'index', 'controller' => 'dashboards' ));
		}

		if($id == NULL ){
			$conditions = array('conditions'=> array('Province.province_code' => $code, 'Province.status' => 1 )) ;
		}else{
			$conditions = array('conditions'=> array('Province.province_code' => $code, 'Province.id !=' => $id, 'Province.status' => 1 )) ;
		}

		if( $this->Province->find('first', $conditions ) ){
			$data['status'] = 0;
			$data['msg'] = "This code is already been in use. Please try another code." ;
		}else{
			$data['status'] = 1;
		}

		echo json_encode($data); exit;

	}

}
