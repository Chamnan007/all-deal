<?php

/********************************************************************************
    File: Business Controller
    Author: PHEA RATTANA

    Confidential ABi Technologies property.

    Changed History:
    Date              Author          Description
    2014/05/20        PHEA RATTANA    Initial
    2014/06/24        RATTANA         Update Each Methods
    2014/07/14        RATTANA         Change with Image Upload
*********************************************************************************/

App::uses('AdministratorAppController', 'Administrator.Controller');

App::uses('Province', 'Administrator.Model');
App::uses('City', 'Administrator.Model');
App::uses('Location', 'Administrator.Model');
App::uses('BusinessCategory', 'Administrator.Model');
App::uses('User', 'Administrator.Model');
App::uses('OperationHour', 'Administrator.Model');

App::uses('Business', 'Administrator.Model');
App::uses('BusinessBranch', 'Administrator.Model');
App::uses('Deal', 'Administrator.Model');
App::uses('DealItemLink', 'Administrator.Model');
App::uses('InterestsCategory', 'Administrator.Model');
App::uses('ServicesCategory', 'Administrator.Model');
App::uses('GoodsCategory', 'Administrator.Model');
App::uses('DiscountCategory', 'Administrator.Model');
App::uses('BusinessMenu', 'Administrator.Model');
App::uses('BusinessMenuItem', 'Administrator.Model');

App::uses('BuyerTransaction', 'Administrator.Model');

/**
 * Businesses Controller
 *
 * @property Business $Business
 * @property PaginatorComponent $Paginator
**/

class BusinessesController extends AdministratorAppController {

/**
 * Components
 *
 * @var array
 */
  var $context = 'Business';

  var $uses = array(  "Administrator.Business", 
                      "Administrator.OperationHour", 
                      "Administrator.BusinessesMedia", 
                      "Administrator.BusinessCategory",
                      "Administrator.Deal",
                      'Administrator.DealItemLink',
                      "Administrator.User",  
                      "Administrator.City",  
                      "Administrator.Province", 
                      "Administrator.Sangkat", 
                      "Administrator.BusinessBranch",
                      "Administrator.BusinessMenu",
                      "Administrator.BusinessMenuItem" ,
                      "Administrator.BuyerTransaction" ,
                      "Administrator.BusinessRevenue"  );

  var $dealWidth        = 705;
  var $dealHeight       = 422;
  var $dealThumbWidth   = 223;
  var $dealThumbHeight  = 133;
  var $dealImages       = 5;
  var $user             = NULL;

  var $dealDeafultImage = "img/deals/default.jpg";

  var $revenueConditions  = array();
  var $revenuePaginate    = array();

  var $balanceConditions  = array();
  var $balancePaginate    = array();

/**
 * index method
 *
 * @return void
 */

  public function setCondition(){ 

    if(isset($this->request->data['business_name'])){
      $this->setState('business_name', $this->request->data['business_name']);
    }

    if(isset($this->request->data['province'])){
      $this->setState('province',$this->request->data['province']);
    }

    if(isset($this->request->data['city'])){
      $this->setState('city',$this->request->data['city']);
    }

    if(isset($this->request->data['main_category'])){
      $this->setState('main_category',$this->request->data['main_category']);
    }

    if(isset($this->request->data['status']) ){
        $this->setState('status',$this->request->data['status']);
    }

    if( isset($this->request->data['business_code']) ){
        $this->setState('business_code', $this->request->data['business_code']);
    }

    if($filter = $this->getState('business_name',NULL)) {
      $textSearch = trim($this->getState('business_name',NULL));
      $textSearch = split(" ", $textSearch);
      
      foreach( $textSearch as $k => $w ){
          if(!empty($w)){
            $this->conditionFilter['OR'][] = array( 'business_name LIKE '  => '%'.$w.'%') ;
          }
      }

    }

    if($province = $this->getState('province',NULL)) {
      $this->conditionFilter['province'] =  $province ;
    }

    if($city = $this->getState('city',NULL)) {
      $this->conditionFilter['city'] = $city ;
    }

    if($main_category = $this->getState('main_category',NULL)) {
      $this->conditionFilter['business_main_category'] = $main_category ;
    }

    if( $biz_code = $this->getState('business_code', NULL )){
      $this->conditionFilter['business_code LIKE'] = '%' . $biz_code . '%';
    }

    $state = $this->getState('status', '' );
    if($state !== '' ){
      $this->conditionFilter['Business.status'] = $state;
    }

    $this->paginate['conditions'] = $this->conditionFilter;
    $this->paginate['order'] = array("business_name" => 'ASC') ;

  }


  public function beforeFilter(){
      parent::beforeFilter();

      $user =  CakeSession::read("Auth.User"); 
      $this->user = $user;

        if( $user['access_level'] != 6 && $user['access_level'] != 5 && $user['access_level'] != 4 ){
            $this->redirect(array('action' => 'index', 'controller' => 'dashboards' )); 
        }

      $this->set('dealImagesSize', $this->dealImages);
      $this->set('pay_method', $this->pay_method);

      $this->set('__DEAL_WIDTH', $this->dealWidth);
      $this->set('__DEAL_HEIGHT', $this->dealHeight);

  }


  public function index( $pending = NULL ) {

      $this->setCondition();

      $user =  CakeSession::read("Auth.User"); 

      $this->set('status', $this->status);
      $this->set('biz_access_level', $this->biz_access_level);
      
      $province = new Province();
      $this->set('provinces', $province->find('all', array('conditions'=> array('Province.status' => 1) )));

      $bizCategory = new BusinessCategory();
      $this->set('businessCategories', $bizCategory->find('all', array('conditions'=> array('parent_id'=>0, 'status' => 1), 'order' => 'category ASC' )));

      $this->Business->recursive = 1;
      
      $this->paginate['joins'] =  array(
        
                                    array('table' => 'cities',
                                        'alias' => 'City',
                                        'type' => 'LEFT',
                                        'conditions' => array('Business.city = City.city_code')
                                      ),

                                    array('table' => 'provinces',
                                        'alias' => 'Province',
                                        'type' => 'LEFT',
                                        'conditions' => array('Business.province = Province.province_code')
                                      )

                                    );

      $this->paginate['fields'] = array('Business.*', 
                                        'City.*', 
                                        'SangkatInfo.*', 
                                        'Province.*', 
                                        'RegisteredBy.last_name', 
                                        'RegisteredBy.first_name',
                                        'ApprovedBy.last_name', 
                                        'ApprovedBy.first_name',
                                        'MainCategory.*',
                                        'SubCategory.*');

      if( $pending == 'pending' ){

        $this->conditionFilter['Business.status'] = '0';
        $this->paginate['conditions'] = $this->conditionFilter;

      }

      if( $user['access_level'] != 6 && $user['access_level'] != 5 ){
          $this->conditionFilter['Business.registered_by'] = $user['id'];
          $this->paginate['conditions'] = $this->conditionFilter; 
      }

      $this->set('statusArray',$this->getState());

      parent::index();

  }


  public function setDealsCondition( $bis_id = 0 ){

    $dealConditions = array();

    $data = $this->request->data;

    $deal_status = $this->getState('deal-status', 'all');

    if( isset($data['deal-status']) ){
        $deal_status = $data['deal-status'];
    }

      $this->setState('deal-status', $deal_status);

      if( isset($data['deal-code']) ){
        $this->setState('deal-code', $data['deal-code']);
      }
      if( isset($data['deal-title']) ){
        $this->setState('deal-title', $data['deal-title']);
      }

      if( isset($data['deal-category']) ){
        $this->setState('deal-category', $data['deal-category']);
      }

      if( isset($data['postedFrom']) ){
        $this->setState('postedFrom', $data['postedFrom']);
      }
      if( isset($data['postedTo']) ){
        $this->setState('postedTo', $data['postedTo']);
      }

      if( isset($data['validFrom']) ){
        $this->setState('validFrom', $data['validFrom']);
      }
      if( isset($data['validTo']) ){
        $this->setState('validTo', $data['validTo']);
      }

      if( isset($data['publishedFrom']) ){
        $this->setState('publishedFrom', $data['publishedFrom']);
      }
      if( isset($data['publishedTo']) ){
        $this->setState('publishedTo', $data['publishedTo']);
      }

      $statusCondtions = array();
      $now = date('Y-m-d 23:59:59');
      
      if( $deal_status != 'all' ){
        $statusCondtions['Deal.status'] = $deal_status;
      }

      if( $deal_status == -1 ){
        $statusCondtions['OR']['Deal.valid_to <'] = $now ;
        $dealConditions['OR'] = $statusCondtions;

      }else if( $deal_status == 1 ){
        $now = date('Y-m-d 23:59:59');
        $statusCondtions['Deal.valid_to >=']  = $now ;
        $dealConditions['AND'] = $statusCondtions;
      }else if( $deal_status == 0 ){
        $dealConditions['AND'] = $statusCondtions;
      }

      if( $code = $this->getState('deal-code', NULL )){
        $dealConditions['Deal.deal_code LIKE'] = '%' . $code .'%'; 
      }
      if( $title = $this->getState('deal-title', NULL )){
        $dealConditions['Deal.title LIKE'] = '%' . $title .'%'; 
      }

      if( $cate_id = $this->getState('deal-category', NULL )){
        $dealConditions['Deal.goods_category LIKE'] = '%' . $cate_id .'%'; 
      }

      if( $postedFrom = $this->getState('postedFrom') ){
        $postedFrom = date('Y-m-d 00:00:00', strtotime($postedFrom));
        $dealConditions['Deal.created >='] = $postedFrom;
      }
      if( $postedTo = $this->getState('postedTo') ){
        $postedTo = date('Y-m-d 23:59:59', strtotime($postedTo));
        $dealConditions['Deal.created <='] = $postedTo;
      }

      if( $validFrom = $this->getState('validFrom') ){
        $validFrom = date('Y-m-d 00:00:00', strtotime($validFrom));
        $dealConditions['Deal.valid_from >='] = $validFrom;
      }
      if( $validTo = $this->getState('validTo') ){
        $validTo = date('Y-m-d 23:59:59', strtotime($validTo));
        $dealConditions['Deal.valid_to <='] = $validTo;
      }

      if( $publishedFrom = $this->getState('publishedFrom') ){
        $publishedFrom = date('Y-m-d 00:00:00', strtotime($publishedFrom));
        $dealConditions['Deal.publish_date >='] = $publishedFrom;
      }
      if( $publishedTo = $this->getState('publishedTo') ){
        $publishedTo = date('Y-m-d 23:59:59', strtotime($publishedTo));
        $dealConditions['Deal.publishedTo <='] = $publishedTo;
      }

      $last_minute = 0;
      if( isset($data['last-minute']) ){  
        $last_minute = 1;
      }
      $this->setState('last-minute', $last_minute);

      $last_minute = $this->getState('last-minute', 0);

      if( $last_minute == 1 ){
        $dealConditions['Deal.is_last_minute_deal'] = $last_minute;
      }

      $dealConditions['Deal.business_id']  = $bis_id;
      $dealConditions['Deal.isdeleted']  = 0 ;

      $this->paginate['conditions'] = $dealConditions;
      $this->paginate['order']    = array( 'Deal.created' => 'DESC' );
      
      $this->set('dealStates', $this->getState());
  
  }

  // Set Revenue Conditions 
  public function setRevenueConditions(  $business_id = 0 ){

    $default_from = date('Y-m-01');
    $default_to   = date('Y-m-t');

    $from = $default_from;
    $to   = $default_to;
    $default = true;

    if( isset($this->request->data['DateFrom']) ){
        $this->setState('DateFrom', $this->request->data['DateFrom']);
    }

    if( isset($this->request->data['DateTo']) ){
        $this->setState('DateTo', $this->request->data['DateTo']);
    }

    if( $date_from = $this->getState('DateFrom', NULL ) ){
       $from = date('Y-m-d', strtotime($date_from));
    }

    if( $date_to = $this->getState('DateTo', NULL) ){
       $to = date('Y-m-d', strtotime($date_to));
    }

    if( $from != $default_from || $to != $default_to ){
      $default = false;
    }

    $this->revenueConditions['BuyerTransaction.created >=']   = $from . " 00:00:00";
    $this->revenueConditions['BuyerTransaction.created <=']   = $to . " 23:59:59";
    $this->revenueConditions['BuyerTransaction.business_id']  = $business_id;

    $this->revenuePaginate['conditions'] = $this->revenueConditions;
    $this->revenuePaginate['order']      = array( 
                                            'BuyerTransaction.created' => 'DESC',
                                            'DealInfo.created' => 'DESC'
                                    );

    $countConditions['conditions']  = $this->revenueConditions;
    $countConditions['fields']      = array( 'SUM(BuyerTransaction.amount * (100 - BuyerTransaction.commission_percent) / 100 ) AS total' ,
                                              'SUM(BuyerTransaction.amount * (BuyerTransaction.commission_percent) / 100 ) AS total_commission');

    $total = $this->BuyerTransaction->find('first', $countConditions);
    $period_string      = "This month revenue";
    $commission_text    = "This month commission";
    
    if( !$default ){
      $period_string = "Revenue from " . date('d-F-Y', strtotime($from)) . " to " . date('d-F-Y', strtotime($to)) ;
      $commission_text = "Commission from " . date('d-F-Y', strtotime($from)) . " to " . date('d-F-Y', strtotime($to)) ;
    }

    $this->set('period_string', $period_string);
    $this->set('total_revenue', $total[0]['total']);
    $this->set('commission_text', $commission_text);
    $this->set('total_commission', $total[0]['total_commission']);

    $this->set('revenueStates', $this->getState());

  }


  public function setBalanceConditions($id = 0 ){

      $this->context = "BusinessRevenue";

      $default_from = date('Y-m-01');
      $default_to   = date('Y-m-t');

      $from = $default_from;
      $to   = $default_to;
      $default = true;

      if( isset($this->request->data['DateFrom']) ){
          $this->setState('DateFrom', $this->request->data['DateFrom']);
      }

      if( isset($this->request->data['DateTo']) ){
          $this->setState('DateTo', $this->request->data['DateTo']);
      }

      if( $date_from = $this->getState('DateFrom', NULL ) ){
         $from = date('Y-m-d', strtotime($date_from));
      }

      if( $date_to = $this->getState('DateTo', NULL) ){
         $to = date('Y-m-d', strtotime($date_to));
      }

      $this->balanceConditions['BusinessRevenue.created >='] = $from . " 00:00:00";
      $this->balanceConditions['BusinessRevenue.created <='] = $to . " 23:59:59";

      $this->balanceConditions['BusinessRevenue.business_id']= $id;

      $this->balancePaginate['conditions'] = $this->balanceConditions;
      $this->balancePaginate['order']      = array( 
                                              'BusinessRevenue.created' => 'DESC'
                                      );

      $this->set('balanceStates', $this->getState());

  }

  public function view($id = null) {

      if (!$this->Business->exists($id)) {
        // throw new NotFoundException(__('Invalid business'));
        return $this->redirect(array('action' =>'index'));
      }

      $user_data =  CakeSession::read("Auth.User"); 

      $permission = $user_data['access_level'];
      $this->set('permission', $permission);

      $this->Deal->recursive = 1;    

      $options = array( 'joins' => array(
                                    array('table' => 'cities',
                                        'alias' => 'City',
                                        'type' => 'LEFT',
                                        'conditions' => array('Business.city = City.city_code')
                                      ),

                                    array('table' => 'provinces',
                                        'alias' => 'Province',
                                        'type' => 'LEFT',
                                        'conditions' => array('Business.province = Province.province_code')
                                      )

                                    ),
                          'fields' => array('Business.*', 
                                            'City.*', 
                                            'Province.*', 
                                            'SangkatInfo.*', 
                                            'RegisteredBy.last_name', 
                                            'RegisteredBy.first_name',
                                            'ApprovedBy.last_name', 
                                            'ApprovedBy.first_name',
                                            'MainCategory.*',
                                            'SubCategory.*'),
                        );


      if( $permission == 4 ){ 
        // 4: for Sale Role
        $options['conditions'] = array('Business.' . $this->Business->primaryKey => $id, 'Business.registered_by' => $user_data['id'] ) ;
      }else{
        $options['conditions'] = array('Business.' . $this->Business->primaryKey => $id ) ;
      }

      $this->Business->recursive = 3;
      $data = $this->Business->find('first', $options);

      if( !empty($data['Business']) ){

        $this->setDealsCondition( $id );
        $deals = $this->paginate('Deal');
        $data['Deals'] = $deals;

        $this->set('business', $data );

        $city = new City();
        $this->set('cities', $city->find('all', array('conditions'=> array('City.status' => 1), 'order' => 'City.city_name ASC' )));

        $province = new Province();
        $this->set('provinces', $province->find('all', array('conditions'=> array('Province.status' => 1) )));


        $bizCategory = new BusinessCategory();
        $this->set('businessCategories', $bizCategory->find('all', array('conditions'=> array('parent_id'=>0, 'status' => 1) )));

        $this->set('access_level', $this->biz_access_level );
        $this->set('member_level', $this->member_biz_level );
        $this->set('status', $this->status);
        $this->set('deal_status', $this->deal_status);
        $this->set('gender', $this->gender);
        $this->set("operation_hours", $this->operation_hours);

        $goods = new GoodsCategory();
        $this->set('goods', $goods->find('all', array(  'conditions'=> array('GoodsCategory.status' => 1), 
                                    'order' => array('GoodsCategory.category' => 'ASC' ) )));

        // Count Menu Item
        $count = $this->countTotalMenuItems($id);
        $this->set('count_menu_items', $count);

        // Business Menu Category
        $menuObj = new BusinessMenu();
        $this->set('business_menus', $menuObj->find('all', array('conditions'=> array('status' => 1, 'business_id' => $id), 'order' => array('name' => 'ASC'))));

        // Conditions For Revenue & Comission
        $this->setRevenueConditions( $id );
        $this->BuyerTransaction->recursive = 3;

        $btObj = new BuyerTransaction();
        $revenue_info = $this->BuyerTransaction->find('all', $this->revenuePaginate);
        $this->set('revenue_data', $revenue_info);

        // Get Balance of merchant
        $this->setBalanceConditions($id);
        $this->BusinessRevenue->recursive = 3;
        $data_balance = $this->BusinessRevenue->find('all', $this->balancePaginate);

        $this->set('data_balance', $data_balance);

        $deal_ids = array();
        foreach( $data_balance as $ind => $val ){
          if( $val['BusinessRevenue']['payment_id']){
            $ids = explode(",", $val['PaymentInfo']['deal_ids']);
            foreach( $ids as $id ){
              $deal_ids[] = $id;
            }
          }
        }

        // Get Deal Info
        $deaInfo = $this->Deal->find('all', array('conditions' => array('Deal.id' => $deal_ids)) );
        $deal_data = array();
        if( $deaInfo ){
          foreach( $deaInfo as $k => $deal ){
            $deal_data[$deal['Deal']['id']] = $deal['Deal'];
          }
        }

        $this->set('deal_data', $deal_data);


      }else{
          $this->Session->setFlash(__( 'No data found.')
                        .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                      'default',
                      array('class'=>'alert alert-dismissable alert-danger '));

          $this->redirect( array( 'action' => 'index' ));
      }

  }


  public function editOperation( $biz_id = 0 ){

      // var_dump($this->request->data); exit;

      $o_hours = $this->request->data['OperationHour'];

      $arr_hours = array();

      foreach( $o_hours as $key => $value ){
        
        $oh_arr = array();
        $oh_arr['OperationHour']['id'] = $value['id'];        
        $oh_arr['OperationHour']['business_id'] = $biz_id ;
            $oh_arr['OperationHour']['day'] = $key;
        $oh_arr['OperationHour']['from'] = $value['f'];
        $oh_arr['OperationHour']['to'] = $value['t'];

        $arr_hours[] = $oh_arr;

      }

      if( $this->OperationHour->saveAll($arr_hours)){
        $this->Session->setFlash(__( 'Operation Hours have been updated.')
                        .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                      'default',
                      array('class'=>'alert alert-dismissable alert-success '));

          $logMessage = json_encode($arr_hours);

          $this->generateLog($logMessage,' EDIT OPERATION HOURS  '.$biz_id);
      }else{
        $this->Session->setFlash(__( 'Operation hours could not be updated, try again !!!')
                        .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                      'default',
                      array('class'=>'alert alert-dismissable alert-danger '));
      }

      return $this->redirect(array('action' =>'view', $biz_id));

  }

  public function add() {
      
      $user = CakeSession::read("Auth.User"); 

      $this->set('gender', $this->gender);      
      $this->set('member_biz_level', $this->biz_access_level );

      $province = new Province();
      $this->set('provinces', $province->find('all', array('conditions'=> array('Province.status' => 1), 'order' => array('Province.province_code' => 'ASC' )  )));

      $cityObj = new City();
      $this->set('cities', $cityObj->find('all', array('conditions'=> array('City.status' => 1), 'order' => array('City.province_code' => 'ASC' )  )));

      $this->set("operation_hours", $this->operation_hours );

      $bizCategory = new BusinessCategory();
      $this->set('businessCategories', $bizCategory->find('all', array('conditions'=> array('parent_id'=>0, 'status' => 1), 'order' => array('BusinessCategory.category' => 'ASC' )  )));
      
      $this->set('access_level', $this->access_level);
      $this->set('status', $this->status);

      if ($this->request->is('post')) {

        $business['Business']     = $this->request->data['Business'];
        $busniess_member['User']  = $this->request->data['User'];

        $o_hours = array();
        if( isset($this->request->data['OperationHour']) &&  $this->request->data['OperationHour'] != "" ){
          $o_hours = $this->request->data['OperationHour'];
        }

        $business['Business']['accept_payment']   = json_encode(isset($this->request->data['Business']['accept_payment'])?$this->request->data['Business']['accept_payment']:"");
        $business['Business']['member_level']     = 1 ;
        $business['Business']['logo']             = NULL ;
        $business['Business']['operation_hour']   = NULL ;
        $business['Business']['registered_by']    =  $user['id'] ;
        $business['Business']['suspend_date']     = NULL ;
        $business['Business']['inactive_date']    = NULL ;
        $business['Business']['approved_date']    = NULL ;
        $business['Business']['approved_by']      = $user['id'] ;

        if( $business['Business']['status'] == 1 ){
          $business['Business']['approved_date'] = date("Y-m-d H:i:s");
        }

        if( $business['Business']['status'] == -1 ){
          $business['Business']['inactive_date'] = date("Y-m-d H:i:s");
        }

        if( $business['Business']['status'] == 2 ){
          $business['Business']['suspend_date'] = date("Y-m-d H:i:s");
        }

        // saveFile
        $logo = $_FILES['logo'] ;
        $error = "";

        if( $logo['name'] != "" ){

          $dir = 'img/business/logos' ;

          if( !is_dir($dir) ){
            mkdir($dir, 0755);
          }

          $minWidth = 225 ;
          $fileName = $this->saveFile('logo', $dir, array(), $minWidth, $minWidth  );

          if( is_array($fileName) && $fileName['status'] ==  false ){
            $error = $fileName['msg'];
            $fileName = "img/business/logos/default.jpg";

          }else{
            // Resize Images
            $this->Image->prepare( $fileName );
            $this->Image->resize($minWidth, $minWidth);
            $this->Image->save( $fileName , 90, true );
            
          }       

          $business['Business']['logo'] = $fileName;

        }else{
          $business['Business']['logo'] = 'img/business/logos/default.jpg' ;
        }

        unset($busniess_member['User']['confirm_password']);

        if( isset($bis_id) ){
          $business['Business']['id'] = $bis_id;
          $this->Business->id = $bis_id;
        }

        if( $this->Business->save($business) ){

          if( !isset($bis_id) ){
            $bis_id  = $this->Business->getLastInsertId();
          }

          $biz_code = "M" . str_pad($bis_id, 6, 0 , STR_PAD_LEFT);
          $updateData['Business']['id'] = $bis_id;
          $updateData['Business']['business_code'] = $biz_code;
          $this->Business->save($updateData);

          // Add Operation Hour
          // =======================================

          $arr_hours = array();
          foreach( $o_hours as $key => $value ){
            $oh_arr = array();
            $oh_arr['OperationHour']['business_id'] = $bis_id;
            $oh_arr['OperationHour']['day'] = $key;
            $oh_arr['OperationHour']['from'] = $value['f'];
            $oh_arr['OperationHour']['to'] = $value['t'];

            $arr_hours[] = $oh_arr;
          }

          if( !empty($arr_hours) ){
            $this->OperationHour->saveAll($arr_hours);
          }
          
          // Add Business Contact Person
          // =====================================
          $busniess_member['User']['business_id'] = $bis_id;
          $busniess_member['User']['user_id'] = uniqid() ;
          $busniess_member['User']['registered_date'] = date("Y-m-d H:i:s");

          $Y = $this->request->data['User']['dob'][2];
          $m = $this->request->data['User']['dob'][1];
          $d = $this->request->data['User']['dob'][0];

          $busniess_member['User']['dob'] = $Y . '-' . $m . '-' . $d;

          $this->User->save($busniess_member);

          // Add Business Branches
          $bis_branches_data = array();
          if( isset($this->request->data['BusinessBranch']) && !empty($this->request->data['BusinessBranch']) ){
              $branches = $this->request->data['BusinessBranch']['branch_name'];
              foreach( $branches as $k => $val ){
                  $temp = array();
                  $temp['BusinessBranch']['business_id']  = $bis_id;
                  $temp['BusinessBranch']['branch_name']  = $val;
                  $temp['BusinessBranch']['latitude']     = $this->request->data['BusinessBranch']['latitude'][$k];
                  $temp['BusinessBranch']['longitude']    = $this->request->data['BusinessBranch']['longitude'][$k];
                  $temp['BusinessBranch']['status']  = 1;

                  $bis_branches_data[] = $temp;
              }

              $this->BusinessBranch->saveAll($bis_branches_data);
          }

          // ========================================================
          $this->Session->setFlash(__( $this->context.' has been saved. ' . $error )
                        .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                      'default',
                      array('class'=>'alert alert-dismissable alert-success '));

          // $this->redirect();
          $logMessage = json_encode($business);

          $this->generateLog($logMessage,' CREATE NEW');        
          return $this->redirect(array('action' =>'view', $this->Business->getInsertId()));

        }else{
          $this->Session->setFlash(__( $this->context.' could not be save. ' . $error )
                        .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                      'default',
                      array('class'=>'alert alert-dismissable alert-danger '));

          return $this->redirect(array('action' =>'add'));
        }

      }
 
  }


  public function removeLogo( $bis_id = 0 ){

      $this->Business->id = $bis_id;
      if (!$this->Business->exists()) {
        throw new NotFoundException(__('Invalid Request !'));
      }

      $old = $this->Business->findById($bis_id);
      $old_logo = $old['Business']['logo'];
      $data['Business']['id'] = $bis_id;
      $data['Business']['logo'] = 'img/business/logos/default.jpg' ;

      if( $this->Business->save($data)){

        if( $old_logo != $data['Business']['logo']){
          unlink($old_logo);
        }


        $this->generateLog("" ,' REMOVE LOGO TO BUSINESS  '.$bis_id);

        $this->Session->setFlash(__( 'Logo has been removed.')
                        .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                      'default',
                      array('class'=>'alert alert-dismissable alert-success '));

      }else{
        $this->Session->setFlash(__( 'Logo could not be removed.')
                        .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                      'default',
                      array('class'=>'alert alert-dismissable alert-danger '));
      }

      return $this->redirect(array('action' =>'view', $bis_id));
    
  }

  public function changeLogo( $bis_id = 0 ){

      $this->Business->id = $bis_id;
      if (!$this->Business->exists()) {
        throw new NotFoundException(__('Invalid Request !'));
      }

      $old = $this->Business->findById($bis_id);
      $old_logo = $old['Business']['logo'];

      $data['Business']['id'] = $bis_id;

      $logo = $_FILES['logo'] ;

      if( $logo['name'] != "" ){

        $dir = 'img/business/logos' ;

        if( !is_dir($dir) ){
          mkdir($dir, 0755);
        }

        $minWidth = 225;

        $fileName = $this->saveFile('logo', $dir, array(), $minWidth, $minWidth  );

        if( is_array($fileName) && $fileName['status'] ==  false ){
          $this->Session->setFlash(__( 'Logo could not be changed. ' . $fileName['msg'] )
                        .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                      'default',
                      array('class'=>'alert alert-dismissable alert-danger '));
          return $this->redirect(array('action' =>'view', $bis_id));
        }

        // Resize Images
        $this->Image->prepare( $fileName );
        $this->Image->resize($minWidth, $minWidth);
        $this->Image->save( $fileName, 90, true );

        $data['Business']['logo'] = $fileName;
      }else{
        $data['Business']['logo'] = 'img/business/logos/default.jpg' ;
      }

      if( $this->Business->save($data)){

        if( $old_logo != "img/business/logos/default.jpg" ){
          unlink($old_logo);
        }

        $this->generateLog("" ,' CHANGE LOGO TO BUSINESS  '.$bis_id);

        $this->Session->setFlash(__( 'Logo has been changed.')
                        .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                      'default',
                      array('class'=>'alert alert-dismissable alert-success '));

      }else{
        $this->Session->setFlash(__( 'Logo could not be changed.')
                        .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                      'default',
                      array('class'=>'alert alert-dismissable alert-danger '));
      }

      return $this->redirect(array('action' =>'view', $bis_id));

  }


  public function uploadMedia( $bis_id = 0, $type = NULL ){

      $this->Business->id = $bis_id;
      if (!$this->Business->exists()) {
        throw new NotFoundException(__('Invalid Request !'));
      }

      if( $type == 0 ){
        
        $data_array = array();

        if( !empty($_FILES['images']['name']) ){

          foreach( $_FILES['images']['name'] as $key => $value ){
            $data = array();
            $images = array();
            $images['name'] = $value;
            $images['type'] = $_FILES['images']['type'][$key];
            $images['tmp_name'] = $_FILES['images']['tmp_name'][$key];
            $images['error'] = $_FILES['images']['error'][$key];
            $images['size'] = $_FILES['images']['size'][$key];

            $dir = 'img/business/pictures' ;
            if( !is_dir($dir) ){
              mkdir($dir, 0755);
            }

            $w = 300;
            $h = 300;

            $fileName = $this->saveMultiple($images, $dir, array(), $w, $h );

            if( is_array($fileName) && $fileName['status'] ==  false ){
              $this->Session->setFlash(__( 'Images could not be uploaded. ' . $fileName['msg'] )
                            .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                          'default',
                          array('class'=>'alert alert-dismissable alert-danger '));
              return $this->redirect(array('action' =>'view', $bis_id, 'media'));
            }

            $max_width = $max_height = 800 ;

            $img_size    = getimagesize($fileName);
            $img_width    = $img_size[0];
            $img_height   = $img_size[1];

            if( $img_width > $img_height ){
              $max = $img_width - 1;
            }else{
              $max = $img_height - 1;
            }

            if( $img_width > $max_width && $img_height > $max_height ){
              // Resize Images
              $this->Image->prepare( $fileName );
              $this->Image->resize( $max_width, $max_height );
              $this->Image->save( $fileName );
            }else{
              
               $this->Image->prepare( $fileName );
              $this->Image->resize( $max, $max );
              $this->Image->save( $fileName );
            }

            $data['BusinessesMedia']['business_id']   = $bis_id;
            $data['BusinessesMedia']['media_type']    = $type;
            $data['BusinessesMedia']['media_path']    = $fileName;
            $data['BusinessesMedia']['media_embeded']   = NULL;
            $data['BusinessesMedia']['status']      = 1;

            $data_array[] = $data;
          }

          // var_dump($data_array); exit;

          $this->BusinessesMedia->create();

          if( $this->BusinessesMedia->saveAll($data_array)){
            $message = json_encode($data_array);

            $this->generateLog($message ,' UPLOAD PICTURES TO BUSINESS  '.$bis_id);

            $this->Session->setFlash(__( 'Images have been uploaded.')
                            .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                          'default',
                          array('class'=>'alert alert-dismissable alert-success '));

          }else{
            $this->Session->setFlash(__( 'Images could not be uploaded.')
                            .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                          'default',
                          array('class'=>'alert alert-dismissable alert-danger '));
          }
        }
        return $this->redirect(array('action' =>'view', $bis_id, 'media'));

      }else if( $type == 1 ){

        $data['BusinessesMedia']['business_id']   = $bis_id;
        $data['BusinessesMedia']['media_type']    = $type;
        $data['BusinessesMedia']['media_path']    = NULL;
        $data['BusinessesMedia']['media_embeded']   = $this->request->data['video_url'];
        $data['BusinessesMedia']['status']      = 1;

        if( $this->BusinessesMedia->save($data)){
          $message = json_encode($data);

          $this->generateLog($message ,' UPLOAD VIDEO TO BUSINESS  '.$bis_id);

          $this->Session->setFlash(__( 'Video has been uploaded.')
                          .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                        'default',
                        array('class'=>'alert alert-dismissable alert-success '));

        }else{
          $this->Session->setFlash(__( 'Video could not be uploaded.')
                          .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                        'default',
                        array('class'=>'alert alert-dismissable alert-danger '));
        }
        return $this->redirect(array('action' =>'view', $bis_id, 'media'));
      }

  }


  public function memberAdd( $biz_id = 0 ){
      
      if (!$this->Business->exists($biz_id)) {
        throw new NotFoundException(__('Invalid Request !'));
      }

      unset( $this->request->data['User']['confirm_password'] ) ;


      $Y = $this->request->data['User']['dob'][2];
      $m = $this->request->data['User']['dob'][1];
      $d = $this->request->data['User']['dob'][0];

      $this->request->data['User']['dob'] = $Y . '-' . $m . '-' . $d;

      $this->request->data['User']['business_id'] = $biz_id;
      $this->request->data['User']['user_id'] = uniqid() ;
      $this->request->data['User']['registered_date'] = date("Y-m-d H:i:s");
      $this->request->data['User']['inactive_date'] = date("Y-m-d H:i:s");

      if ($this->request->is(array('post', 'put'))) {

        if( $this->User->save($this->request->data) ){
          $this->Session->setFlash(__( 'New member has been saved.')
                        .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                      'default',
                      array('class'=>'alert alert-dismissable alert-success '));

          $logMessage = json_encode($this->request->data);

          $this->generateLog($logMessage,' ADD NEW MEMBER TO BUSINESS  '.$biz_id);        
          
        }else{
          $this->Session->setFlash(__( 'New member could not be save.')
                        .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                      'default',
                      array('class'=>'alert alert-dismissable alert-danger '));

        }

        return $this->redirect(array('action' =>'view', $biz_id, 'member' ));
      
      }
  }


  public function memberEdit( $id = 0 ){
      

      if($this->request->data['User']['password'] == "" ){
        unset($this->request->data['User']['password']);
      }

      unset($this->request->data['User']['confirm_password']);


      $Y = $this->request->data['User']['dob'][2];
      $m = $this->request->data['User']['dob'][1];
      $d = $this->request->data['User']['dob'][0];

      $this->request->data['User']['dob'] = $Y . '-' . $m . '-' . $d;
      $this->request->data['User']['inactive_date'] = date("Y-m-d H:i:s");

      if ($this->request->is(array('post', 'put'))) {

        $this->User->id = $this->request->data['User']['id'];

        if( $this->User->save($this->request->data) ){
          $this->Session->setFlash(__( 'Member has been saved.')
                        .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                      'default',
                      array('class'=>'alert alert-dismissable alert-success '));

          $logMessage = json_encode($this->request->data);

          $this->generateLog($logMessage,' EDIT BUSINESS MEMBER ');       
          
        }else{
          $this->Session->setFlash(__( 'Member could not be save.')
                        .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                      'default',
                      array('class'=>'alert alert-dismissable alert-danger '));
        }

        return $this->redirect(array('action' =>'view', $id, "member"));
      
      }
  }


  public function edit($id = NULL, $detail = NULL ) {

      if (!$this->Business->exists($id)) {
        $this->Session->setFlash(__( 'Invalid Request!')
                        .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                      'default',
                      array('class'=>'alert alert-dismissable alert-danger '));
        if( $detail){
          return $this->redirect(array('action' =>'view', $id));
        }else{
          return $this->redirect(array('action' =>'index'));
        }
      }


      $user = CakeSession::read("Auth.User"); 

      $this->set('gender', $this->gender);
      $this->set("operation_hours", $this->operation_hours );


      $bizCategory = new BusinessCategory();
      $this->set('businessCategories', $bizCategory->find('all', array('conditions'=> array('parent_id'=>0, 'status' => 1 ), 'order' => array('category' => 'ASC' ) )));
      
      $this->set('access_level', $this->access_level);
      $this->set('member_biz_level', $this->biz_access_level );
      $this->set('status', $this->status); 


      $this->Business->id = $id;

      if ($this->request->is(array('post', 'put'))) {

        if( $this->request->data['Business']['status'] == 1 && !isset($this->request->data['done-activated']) ){
          $this->request->data['Business']['approved_date'] = date("Y-m-d H:i:s");
          $this->request->data['Business']['approved_by'] = $user['id'] ;
        }else if( $this->request->data['Business']['status'] == -1 ){
          $this->request->data['Business']['inactive_date'] = date("Y-m-d H:i:s");
        }else if( $this->request->data['Business']['status'] == 2 ){
          $this->request->data['Business']['suspend_date'] = date("Y-m-d H:i:s");
        }

        $this->request->data['Business']['accept_payment'] = json_encode(isset($this->request->data['Business']['accept_payment'])?$this->request->data['Business']['accept_payment']:"");
        $this->request->data['Business']['updated_by']  = $user['id'];

        if ($this->Business->save($this->request->data)) {

          // Remove Old Branches
          $delete_conds = array( 'business_id' => $id );

          if( $this->BusinessBranch->deleteAll( $delete_conds) ){

              // Add New Branches (Location)
              $bis_branches_data = array();
              if( isset($this->request->data['BusinessBranch']) && !empty($this->request->data['BusinessBranch']) ){
                  $branches = $this->request->data['BusinessBranch']['branch_name'];
                  foreach( $branches as $k => $val ){
                      $temp = array();
                      $temp['BusinessBranch']['business_id']  = $id;
                      $temp['BusinessBranch']['branch_name']  = $val;
                      $temp['BusinessBranch']['latitude']     = $this->request->data['BusinessBranch']['latitude'][$k];
                      $temp['BusinessBranch']['longitude']    = $this->request->data['BusinessBranch']['longitude'][$k];
                      $temp['BusinessBranch']['status']  = 1;

                      $bis_branches_data[] = $temp;
                  }

                  $this->BusinessBranch->saveAll($bis_branches_data);

              }

          }

          $this->Session->setFlash(__( 'Information has been saved.')
                        .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                      'default',
                      array('class'=>'alert alert-dismissable alert-success '));

          $logMessage = json_encode($this->Business->findById($id));

          $this->generateLog($logMessage,' EDIT BUSINESS ');    


        } else {
          $this->Session->setFlash(__( 'Information could not be save.')
                        .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                      'default',
                      array('class'=>'alert alert-dismissable alert-danger '));
        }

        if( $detail){
          return $this->redirect(array('action' =>'view', $id));
        }else{
          return $this->redirect(array('action' =>'index'));
        }

      } else {

        if( $user['access_level'] == 4 ){
            $options = array('conditions' => array('Business.' . $this->Business->primaryKey => $id,
                                                    'Business.registered_by' => $user['id'] ));
        }else{
          $options = array('conditions' => array('Business.' . $this->Business->primaryKey => $id));
        }

        $this->Business->recursive = -1;
        $this->request->data = $this->Business->find('first', $options);

        if( !empty($this->request->data['Business']) ){
          $pro_code   = $this->request->data['Business']['province']  ;
          $city_code  = $this->request->data['Business']['city'] ;
          $main  = $this->request->data['Business']['business_main_category']  ;

          // Get Branch Data
          $conds['conditions'] = array( 'BusinessBranch.business_id' => $id );
          $branches = $this->BusinessBranch->find('all', $conds);
          $this->set('branches', $branches);

          $province = new Province();
          $this->set('provinces', $province->find('all', array('conditions'=> array('Province.status' => 1) )));

          $city = new City();
          $this->set('cities', $city->find('all', array('conditions' => array('City.status' => 1 ), 
                    'order' => array('City.province_code' => 'ASC') ) ));
          
          $location = new Location();
          $this->set('locations', $location->find('all', array('conditions' => array("Location.city_code" => $city_code, 'Location.status' => 1 ) ) ));

          $location_id  = $this->request->data['Business']['location_id'];
          $loc_name     = $this->request->data['Business']['location'];

          $conds = array();
          if( !$location_id ){
             $locInfo = $this->Location->findByLocationName($loc_name);
             if( $locInfo ){
               $location_id = $locInfo['Location']['id'];
             }
          }
          $conds['conditions'] = array( 'Sangkat.location_id' => $location_id, 'Sangkat.status' => 1 );
          $this->set('SangkatData', $this->Sangkat->find('all', $conds));

          $main_cate = new BusinessCategory();
          $this->BusinessCategory->recursive = -1;
          $this->set('subCates', $main_cate->find('all', array('conditions' => array("BusinessCategory.parent_id" => $main, 'BusinessCategory.status' => 1 ) ) ));
        }else{
          $this->Session->setFlash(__( 'Data is not found.')
                        .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                      'default',
                      array('class'=>'alert alert-dismissable alert-danger '));

          $this->redirect( array( 'action' => 'index' ));

        }

      }
  }



  public function deactivate($user_id = 0 , $biz_id = 0 ){


      if (!$this->User->exists($user_id)) {
        throw new NotFoundException(__('Invalid User'));
      }

      $this->User->id = $user_id;
      $data['User']['id'] = $user_id;
      $data['User']['status'] = -1 ;
      $data['User']['inactive_date'] =  date("Y-m-d H:i:s");

      if ($this->User->save($data)) {
        $this->Session->setFlash(__( 'Member has been deactivated.')
                        .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                      'default',
                      array('class'=>'alert alert-dismissable alert-success '));



      } else {
        $this->Session->setFlash(__( 'Member could not be deactivated.')
                        .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                      'default',
                      array('class'=>'alert alert-dismissable alert-danger '));
      }

      return $this->redirect(array('action' => 'view', $biz_id, 'member'));

  }

  public function activate($user_id = 0 , $biz_id = 0 ){


      if (!$this->User->exists($user_id)) {
        throw new NotFoundException(__('Invalid User'));
      }

      $this->User->id = $user_id;
      $data['User']['id'] = $user_id;
      $data['User']['status'] = 1 ;

      if ($this->User->save($data)) {
        $this->Session->setFlash(__( 'Member has been activated.')
                        .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                      'default',
                      array('class'=>'alert alert-dismissable alert-success '));
        
        $logMessage = json_encode($this->request->data);

        $this->generateLog($logMessage,' EDIT DEAL ');  

      } else {
        $this->Session->setFlash(__( 'Member could not be activated.')
                        .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                      'default',
                      array('class'=>'alert alert-dismissable alert-danger '));
      }

      return $this->redirect(array('action' => 'view', $biz_id, 'member' ) );

  }


  public function removeMedia( $media_id = 0 ){

      if (!$this->BusinessesMedia->exists($media_id)) {
        throw new NotFoundException(__('Invalid Request'));
      }

      $old_data = $this->BusinessesMedia->findById($media_id);

      if( $this->BusinessesMedia->delete($media_id) ){

        $st = "";
        if( $old_data['BusinessesMedia']['media_type'] == 0 ){
          $img = $old_data['BusinessesMedia']['media_path'];

          if( file_exists($img) ){
            $st = 1;
            unlink($img);
          }else{
            $st = 0;
          }
        }

        $this->generateLog('',' REMOVE IMAGE ');
        $data['status'] = true;
        $data['img'] = $st;

        echo json_encode($data); exit;
      }else{
        $data['status'] = false;
        echo json_encode($data); exit;
      }

  }


  public function addDeal( $bis_id = 0 ){

      if (!$this->Business->exists($bis_id)) {
        // throw new NotFoundException(__('Invalid Request!'));
        $this->redirect(array('action' => 'index'));
      }

      $user = CakeSession::read("Auth.User");

      if ($this->request->is('post')) {

        $this->request->data['Deal']['business_id'] = $bis_id ;
        $this->request->data['Deal']['title_2']   = "";
        $this->request->data['Deal']['en_barcode']  = 0 ;
        $this->request->data['Deal']['status']    = 1 ;

        if(!isset($this->request->data['Deal']['qr_code'])){
          $this->request->data['Deal']['qr_code']   = 0 ;
        }

        if( isset($this->request->data['Deal']['travel_destination']) ){
          $this->request->data['Deal']['travel_destination'] = json_encode($this->request->data['Deal']['travel_destination']);
        }else{
          $this->request->data['Deal']['travel_destination'] = '[""]';
        }

        $this->request->data['Deal']['created_by']    = $user['id'];
        $this->request->data['Deal']['updated_by']    = $user['id'];
        $this->request->data['Deal']['publish_date']  = date("Y-m-d H:i:s");

        $this->request->data['Deal']['discount_category'] = NULL ;
        $this->request->data['Deal']['interest_category'] = json_encode((isset($this->request->data['Deal']['interest_category']))?$this->request->data['Deal']['interest_category']:"" );
        $this->request->data['Deal']['services_category'] = json_encode((isset($this->request->data['Deal']['services_category']))?$this->request->data['Deal']['services_category']:"" );
        $this->request->data['Deal']['goods_category'] = json_encode((isset($this->request->data['Deal']['goods_category']))?$this->request->data['Deal']['goods_category']:"" );

        // Get Business
        $bizConds['conditions'] = array('Business.id' => $bis_id);
        $bizConds['fields']   = array('Business.business_main_category');
        $bisInfo = $this->Business->find('first', $bizConds);

        
        $this->request->data['Deal']['valid_from'] = date('Y-m-d H:i:s', strtotime($this->request->data['Deal']['valid_from']));
        $this->request->data['Deal']['valid_to'] = date('Y-m-d H:i:s', strtotime($this->request->data['Deal']['valid_to']));


        if( isset($this->request->data['Deal']['is_last_minute_deal']) ) {
          $this->request->data['Deal']['is_last_minute_deal'] = 1;
        }else{
          $this->request->data['Deal']['is_last_minute_deal'] = 0;
        }

        $image = $_FILES['deal_image'] ;

        $error = "";

        if( !empty($image['name']) ){

          $dir = 'img/deals' ;

          if( !is_dir($dir) ){
            mkdir($dir, 0755);
          }

          $data_img = array();

          foreach( $image['name'] as $k => $img ){

              $images = array();
              $images['name']     = $img;
              $images['type']     = $image['type'][$k];
              $images['tmp_name'] = $image['tmp_name'][$k];
              $images['error']    = $image['error'][$k];
              $images['size']     = $image['size'][$k];

              $fileName = $this->saveMultiple($images, $dir);

              if( is_array($fileName) && $fileName['status'] ==  false ){
                $error = $fileName['msg'];
                $fileName = "img/deals/default.jpg";
              }else{
                $thumb_dir = 'img/deals/thumbs' ;

                if( !is_dir($thumb_dir) ){
                  mkdir($thumb_dir, 0755);
                }

                $split = end(explode("/", $fileName));
                $thumb_img =  $thumb_dir . "/" . $split ;
                copy ( $fileName , $thumb_img ) ; 

                // Resize Images
                $this->Image->prepare( $fileName );
                $this->Image->resize( $this->dealWidth , $this->dealHeight );
                $this->Image->save( $fileName );

                // Copy a thumbnail
                $this->Image->prepare( $thumb_img );
                $this->Image->resizeThumbnail($this->dealThumbWidth, $this->dealThumbHeight);
                $this->Image->save( $thumb_img );

              }

              if( $k == 0 ){
                  $this->request->data['Deal']['image'] = $fileName;
              }
              
              $data_img[] = $fileName;
              
          }

          $this->request->data['Deal']['other_images'] = json_encode($data_img);


        }else{
          $this->request->data['Deal']['image'] = 'img/deals/default.jpg' ;
          $this->request->data['Deal']['other_images'] = NULL;
        }


        $this->Deal->create();
        if( $this->Deal->save($this->request->data)){

          if( isset($this->request->data['DealItemLink']) ){
              $deal_id = $this->Deal->getLastInsertID();

              $deal_code = "D" . str_pad($deal_id, 6, 0, STR_PAD_LEFT);
              $updatetData = array();
              $updateData['Deal']['id']       = $deal_id;
              $updateData['Deal']['deal_code']= $deal_code;
              $this->Deal->save($updateData);

              $datas = $this->request->data['DealItemLink']['item_id'];
              $item_data = array();
              foreach( $datas as $key => $val ){
                $temp = array();
                $temp['DealItemLink']['deal_id']        = $deal_id;
                $temp['DealItemLink']['item_id']        = $val;
                $temp['DealItemLink']['original_price'] = $this->request->data['DealItemLink']['original_price'][$key];
                $temp['DealItemLink']['discount']       = $this->request->data['DealItemLink']['discount'][$key];
                $temp['DealItemLink']['available_qty']  = $this->request->data['DealItemLink']['available_qty'][$key];
                $temp['DealItemLink']['status']         = 1;

                $item_data[] = $temp;
              }

              $this->DealItemLink->saveAll($item_data);
          }

          $this->Session->setFlash(__( 'New Deal has been created. ' . $error )
                        .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                      'default',
                      array('class'=>'alert alert-dismissable alert-success '));

          $logMessage = json_encode($this->request->data);

          $this->generateLog($logMessage,' CREATE NEW ');       
          
        }else{
          $this->Session->setFlash(__( $this->context.' could not be created. ' . $error )
                        .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                      'default',
                      array('class'=>'alert alert-dismissable alert-danger '));
        }

        return $this->redirect(array('action' =>'view', $bis_id, 'deal' ));
      }

  }

  // Deal
  public function editDeal( $bis_id = 0, $id = 0 ){

      if (!$this->Deal->exists($id)) {
        $this->redirect(array('action' => 'index'));
      }

      $user = CakeSession::read("Auth.User");

      $this->Deal->id = $id;
      $this->request->data['Deal']['id'] = $id;
      $this->request->data['Deal']['discount_category'] = $this->request->data['Deal']['discount_category'];
      $this->request->data['Deal']['interest_category'] = json_encode((isset($this->request->data['Deal']['interest_category']))?$this->request->data['Deal']['interest_category']:"" );
      $this->request->data['Deal']['services_category'] = json_encode((isset($this->request->data['Deal']['services_category']))?$this->request->data['Deal']['services_category']:"" );
      $this->request->data['Deal']['goods_category'] = json_encode((isset($this->request->data['Deal']['goods_category']))?$this->request->data['Deal']['goods_category']:"" );

      // Get Business
      $bizConds['conditions'] = array('Business.id' => $bis_id);
      $bizConds['fields']   = array('Business.business_main_category');
      $bisInfo = $this->Business->find('first', $bizConds);
      
      $this->request->data['Deal']['valid_from'] = date('Y-m-d H:i:s', strtotime($this->request->data['Deal']['valid_from']));
      $this->request->data['Deal']['valid_to'] = date('Y-m-d H:i:s', strtotime($this->request->data['Deal']['valid_to']));

      if(!isset($this->request->data['Deal']['qr_code'])){
        $this->request->data['Deal']['qr_code']   = 0 ;
      }

      if( isset($this->request->data['Deal']['travel_destination']) ){
        $this->request->data['Deal']['travel_destination'] = json_encode($this->request->data['Deal']['travel_destination']);
      }else{
        $this->request->data['Deal']['travel_destination'] = '[""]';
      }


      $this->request->data['Deal']['updated_by'] = $user['id'];

      if( isset($this->request->data['Deal']['is_last_minute_deal']) ) {
        $this->request->data['Deal']['is_last_minute_deal'] = 1;
      }else{
        $this->request->data['Deal']['is_last_minute_deal'] = 0;
      }

      $old_image = $this->request->data['old_image_path'];

      $err = "";

      // Update Deal Image
      if( isset($_FILES['deal_image']) && $_FILES['deal_image']['name'] != "" ){

        $dir = 'img/deals' ;

        if( !is_dir($dir) ){
          mkdir($dir, 0755);
        }

        $fileName = $this->saveFile('deal_image', $dir , array() , $this->dealWidth, $this->dealHeight );

        if( is_array($fileName) && $fileName['status'] ==  false ){

          $err = "Image could not be changed. " . $fileName['msg'] ;

        }else{

          $this->request->data['Deal']['image'] = $fileName;

          $thumb_dir = 'img/deals/thumbs' ;

          if( !is_dir($thumb_dir) ){
            mkdir($thumb_dir, 0755);
          }

          $split = end(explode("/", $fileName));
          $thumb_img =  $thumb_dir . "/" . $split ;
            copy ( $fileName , $thumb_img ) ; 

          // Resize Images
          $this->Image->prepare( $fileName );
          $this->Image->resize( $this->dealWidth, $this->dealHeight );
          $this->Image->save( $fileName );

          // Copy a thumbnail
            $this->Image->prepare( $thumb_img );
          $this->Image->resizeThumbnail($this->dealThumbWidth, $this->dealThumbHeight);
          $this->Image->save( $thumb_img );

          if( $old_image != "img/deals/default.jpg" ){
            unlink($old_image);

            $split = end(explode("/", $old_image));
            $thumb_img =  $thumb_dir . "/" . $split ;

            if( file_exists($thumb_img)){
              unlink($thumb_img);
            }       
          }

        }

      }

      // Save Data
      if( $this->Deal->save($this->request->data)){

        // Delete Old relate and add new related
          $deal_id = $id;
          if( $this->DealItemLink->deleteAll(array('DealItemLink.deal_id' => $deal_id))){
              if( isset($this->request->data['DealItemLink']) ){

                  $datas = $this->request->data['DealItemLink']['item_id'];
                  $item_data = array();
                  foreach( $datas as $key => $val ){
                    $temp = array();
                    $temp['DealItemLink']['deal_id']        = $deal_id;
                    $temp['DealItemLink']['item_id']        = $val;
                    $temp['DealItemLink']['original_price'] = $this->request->data['DealItemLink']['original_price'][$key];
                    $temp['DealItemLink']['discount']       = $this->request->data['DealItemLink']['discount'][$key];
                    $temp['DealItemLink']['available_qty']       = $this->request->data['DealItemLink']['available_qty'][$key];
                    $temp['DealItemLink']['status']         = 1;

                    $item_data[] = $temp;
                  }

                  // var_dump($item_data); exit;

                  $this->DealItemLink->saveAll($item_data);
              }
          }


        $this->Session->setFlash(__( 'Deal has been updated. ' . $err)
                        .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                      'default',
                      array('class'=>'alert alert-dismissable alert-success '));
      } else {
        $this->Session->setFlash(__( 'Deal could not be updated. ' . $err)
                        .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                      'default',
                      array('class'=>'alert alert-dismissable alert-danger '));
      }

      return $this->redirect(array('action' => 'view', $bis_id, 'deal' ) );
      

  }


  public function deleteDeal( $bis_id = 0, $id = 0 ){

      if (!$this->Deal->exists($id)) {
        // throw new NotFoundException(__('Invalid Request!'));
        $this->Session->setFlash(__( 'Invalid Request !')
                        .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                      'default',
                      array('class'=>'alert alert-dismissable alert-danger '));
        $this->redirect(array('action' => 'view', $bis_id, 'deal'));
      }

      $user = CakeSession::read("Auth.User");

      if( $user['access_level'] != 6 ){
          $this->Session->setFlash(__( 'You are not allow to delete deal. Contact your administrator.')
                        .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                      'default',
                      array('class'=>'alert alert-dismissable alert-danger '));
          $this->redirect(array('action' => 'view', $bis_id, 'deal')); 
      }

      $this->Deal->id = $id;

      $detail = $this->Deal->findById($id);

      if( !$detail || empty($detail) ){
         $this->Session->setFlash(__( 'Invalid Request.')
                        .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                      'default',
                      array('class'=>'alert alert-dismissable alert-danger '));
          $this->redirect(array('action' => 'view', $bis_id, 'deal')); 
      }

      $img = $detail['Deal']['image'];

      if( $this->Deal->delete($id)){

        if( $img != "img/deals/default.jpg" && file_exists($img) ){
            unlink($img);

            $split = end(explode("/", $img));
            $thumb_img =  "img/deals/thumbs/" . $split ;

            if( file_exists($thumb_img)){
              unlink($thumb_img);
            }  

        }

        $this->Session->setFlash(__( 'Deal has been deleted.')
                        .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                      'default',
                      array('class'=>'alert alert-dismissable alert-success '));
      } else {
        $this->Session->setFlash(__( 'Deal could not be deleted.')
                        .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                      'default',
                      array('class'=>'alert alert-dismissable alert-danger '));
      }

      return $this->redirect(array('action' => 'view', $bis_id, 'deal' ) );
      

  }

  public function changeDealImage( $bis_id = 0, $id = 0 ){

      if (!$this->Deal->exists($id)) {
        $this->redirect(array('action' => 'view', $bis_id, 'deal'));
      }

      $old = $this->Deal->findById($id);
      $old_image = $old['Deal']['image'];

      $user = CakeSession::read("Auth.User");

      if( $user['access_level'] != 6 && $user['access_level'] != 5 ){
          $this->redirect(array('action' => 'index', 'controller' => 'dashboards' )); 
      }

      $data['Deal']['id'] = $id;
      $data['Deal']['updated_by'] = $user['id'];

      $image = $_FILES['deal_image'] ;

      if( $image['name'] != "" ){

        $dir = 'img/deals' ;

        if( !is_dir($dir) ){
          mkdir($dir, 0755);
        }

        $fileName = $this->saveFile('deal_image', $dir , array() , $this->dealWidth, $this->dealHeight );

        if( is_array($fileName) && $fileName['status'] ==  false ){
          $this->Session->setFlash(__( 'Image could not be changed. ' . $fileName['msg'] )
                        .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                      'default',
                      array('class'=>'alert alert-dismissable alert-danger '));

          return $this->redirect(array('action' =>'view', $bis_id, 'deal'));
        }


        $data['Deal']['image'] = $fileName;

        $thumb_dir = 'img/deals/thumbs' ;

        if( !is_dir($thumb_dir) ){
          mkdir($thumb_dir, 0755);
        }

        $split = end(explode("/", $fileName));
        $thumb_img =  $thumb_dir . "/" . $split ;
        copy ( $fileName , $thumb_img ) ; 

        // Resize Images
        $this->Image->prepare( $fileName );
        $this->Image->resize($this->dealWidth, $this->dealHeight );
        $this->Image->save( $fileName );

        // Copy a thumbnail
        $this->Image->prepare( $thumb_img );
        $this->Image->resizeThumbnail($this->dealThumbWidth, $this->dealThumbHeight );
        $this->Image->save( $thumb_img );
        
      }else{
          
        $data['Deal']['image'] = 'img/deals/default.jpg' ;
      }

      if( $this->Deal->save($data)){

        if( $old_image != "img/deals/default.jpg" ){
          unlink($old_image);

          $split = end(explode("/", $old_image));
          $thumb_img =  $thumb_dir . "/" . $split ;

          if( file_exists($thumb_img)){
            unlink($thumb_img);
          }       

        }

        $this->generateLog("" ,' CHANGE IMAGE TO DEAL');

        $this->Session->setFlash(__( 'Image has been changed.')
                        .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                      'default',
                      array('class'=>'alert alert-dismissable alert-success '));

      }else{
        $this->Session->setFlash(__( 'Image could not be changed.')
                        .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                      'default',
                      array('class'=>'alert alert-dismissable alert-danger '));
      }

      return $this->redirect(array('action' =>'view', $bis_id, 'deal'));

  }

  public function uploadDealImageAjax__( $bis_id = 0, $deal_id = 0 ){

    $data = array();

    $conds['conditions'] = array( 'Deal.id' => $deal_id );
    $deal_data = $this->Deal->find('first', $conds);
    
    if( $deal_data ){

      $data_image = array();

      $images = $deal_data['Deal']['other_images'];
      if( $images ){
        $other_images = json_decode($images);
        foreach( $other_images as $k => $val ){
          if( $val != $this->dealDeafultImage ){
            $data_image[] = $val;
          }
        }
      }else{
        $data_image[] = $deal_data['Deal']['image'];
      }
      
      $img = $_FILES['upload_deal_image'];
      if( $img && $img['name'] != "" ){
          
          $dir = 'img/deals' ;

          if( !is_dir($dir) ){
            mkdir($dir, 0755);
          }

          $fileName = $this->saveFile('upload_deal_image', $dir , array() , $this->dealWidth, $this->dealHeight );

          if( is_array($fileName) && $fileName['status'] ==  false ){
            $data['status']   = false;
            $data['message']  = $fileName['msg'];
            
            echo json_encode($data); exit;

          }

          $data_image[] = $fileName;

          $thumb_dir = 'img/deals/thumbs' ;

          if( !is_dir($thumb_dir) ){
            mkdir($thumb_dir, 0755);
          }

          $split = end(explode("/", $fileName));
          $thumb_img =  $thumb_dir . "/" . $split ;
          copy ( $fileName , $thumb_img ) ;

          // Resize Images
          $this->Image->prepare( $fileName );
          $this->Image->resize($this->dealWidth, $this->dealHeight );
          $this->Image->save( $fileName );

          // Copy a thumbnail
          $this->Image->prepare( $thumb_img );
          $this->Image->resizeThumbnail($this->dealThumbWidth, $this->dealThumbHeight );
          $this->Image->save( $thumb_img );

          $user = CakeSession::read("Auth.User");
          $save_data['Deal']['id'] = $deal_id;
          $save_data['Deal']['updated_by'] = $user['id'];
          $save_data['Deal']['image'] = $fileName;
          $save_data['Deal']['other_images'] = json_encode($data_image);

          // $data['data'] = $save_data;

          // save data
          $this->Deal->save($save_data);

          $data['status'] = true;
          $data['message'] = "Deal image is uploaded successfully !";
          $data['uploaded_image'] = $fileName;

          echo json_encode($data);
          exit;

      } 
    }else{
      $data['status']   = false;
      $data['message']  = "Something went wrong ! Please try again.";
    }

    echo json_encode( $data);
    $this->autoRender = false;

  }

  public function deleteDealImageAjax__ ( $deal_id = 0, $img = NULL ) {

    if( $deal_id == 0 || $img == NULL ){
        $data['status']   = false ;
        $data['message']  = "Something went wrong. Please try again !";
        echo json_encode($data); exit;
    }

    $deal_data = $this->Deal->findById($deal_id);

    if( $deal_data){
      $data_image = array();
      $images = $deal_data['Deal']['other_images'];

      $img_path       = "img/deals/" . $img ;
      $img_thumb_path = "img/deals/thumbs/" . $img ;

      if( $images ){
        $other_images = json_decode($images);
        foreach( $other_images as $k => $val ){
          $data_image[] = $val;
        }
      }else{
        $data_image[] = $deal_data['Deal']['image'];
      }

      $data_image = array_flip($data_image);

      if( in_array( $img_path , $data_image )){
          unset($data_image[$img_path]);
          $data_image = array_flip($data_image);
      }

      if( empty($data_image) ){
         $data_image[] = $this->dealDeafultImage;
      }

      // Save Data, Remove Image
      $save_data['Deal']['id']    = $deal_id;
      $save_data['Deal']['image'] = reset($data_image);
      $save_data['Deal']['other_images'] = json_encode($data_image);

      if( $this->Deal->save($save_data)){
        
        if( $img_path != $this->dealDeafultImage ){
          unlink($img_path);
          unlink($img_thumb_path);
        }

        $data['status']      = true;
        $data['data_images'] = $data_image;

      }else{
          $data['status'] = false;
          $data['message'] = "Something went wrong ! Please try again.";
      }

      echo json_encode($data); exit;

    }else{          
        $data['status']   = false;
        $data['message']  = "Invalid Request ! Please try again.";
        echo json_encode($data); exit;
    }
  
  }


  // Get Deal Images
  public function getDealImagesAjax__ ( $deal_id = 0 ){
      if( $deal_id == 0 ){
          $data['status'] = false;
          $data['message'] = 'Something went wrong ! Please try again.';

          echo json_encode($data); exit;
      }

      $this->Deal->recursive = -1;
      $deal_data = $this->Deal->findById($deal_id);

      if( $deal_data ){
          $data_image = array();
          $images = $deal_data['Deal']['other_images'];

          if( $images ){
            $other_images = json_decode($images);
            foreach( $other_images as $k => $val ){
              if( $val != $this->dealDeafultImage ){
                  $data_image[] = $val;
              }
            }
          }else if($deal_data['Deal']['image'] != $this->dealDeafultImage ) {
            $data_image[] = $deal_data['Deal']['image'];
          }

          $data['status'] = true ;
          $data['deal_images'] = $data_image;
          
      }else{
          $data['status']     = false;
          $data['message']    = "Invalid Request !";
      }

      echo json_encode($data); exit;

  }

  // Business Menu Category
  public function saveMenuCategory( $business_id = 0, $id =  0 ){

      $this->Business->id = $business_id;
      if ( !$this->request->is('post') || !$this->Business->exists()) {
          $this->Session->setFlash(__( 'Invalid Request !')
                        .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                      'default',
                      array('class'=>'alert alert-dismissable alert-danger '));
          $this->redirect(array('action' => 'view', $business_id, 'menu'));
      }

      $data = $this->request->data;

      $user =  CakeSession::read("Auth.User"); 

      $data['BusinessMenu']['business_id']    = $business_id;
      if( !$id ){
          $data['BusinessMenu']['created_by']     = $user['id'] ;
      }else{
          $data['BusinessMenu']['id'] = $id;
      }
      $data['BusinessMenu']['modified_by']    = $user['id'] ;
      $data['BusinessMenu']['status']         = 1;

      if( $this->BusinessMenu->save($data) ){
          $this->Session->setFlash(__( 'Menu Category has been saved.')
                        .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                      'default',
                      array('class'=>'alert alert-dismissable alert-success '));
      }else{
          $this->Session->setFlash(__( 'Faild to save menu category !')
                        .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                      'default',
                      array('class'=>'alert alert-dismissable alert-danger '));
      }

      if( !$id ){
          $id = $this->BusinessMenu->getLastInsertID();
      }

      $this->redirect(array('action' => 'view', $business_id, 'menu', $id));

  }

  // Delete Menu Category
  public function deleteMenuCategory( $biz_id = 0, $id = 0 ){

      $this->BusinessMenu->id = $id;
      $this->Business->id = $biz_id;
      if ( !$this->Business->exists() || !$this->BusinessMenu->exists()) {
          $this->Session->setFlash(__( 'Invalid Request !')
                        .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                      'default',
                      array('class'=>'alert alert-dismissable alert-danger '));
          $this->redirect(array('action' => 'view', $biz_id, 'menu'));
      }

      $user =  CakeSession::read("Auth.User"); 

      $data['BusinessMenu']['id'] = $id;
      $data['BusinessMenu']['status'] = 0;
      $data['BusinessMenu']['modified_by'] = $user['id'];

      if( $this->BusinessMenu->save($data) ){
          $this->Session->setFlash(__( 'Menu Category has been deleted.')
                        .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                      'default',
                      array('class'=>'alert alert-dismissable alert-success '));
      }else{
          $this->Session->setFlash(__( 'Faild to delete menu category !')
                        .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                      'default',
                      array('class'=>'alert alert-dismissable alert-danger '));
      }

      $this->redirect(array('action' => 'view', $biz_id, 'menu'));

  }

  public function getMenuItemByMenuIDAjax__(){

      $menu_id    = $this->request->data['menu_id'];
      $limit      = $this->request->data['limit'];
      $offset     = $this->request->data['offset'];

      if( $menu_id == 0 ){
          $data['status'] = false;
          $data['message'] = 'Loading failed ! Please try again.' ;
          echo json_encode($data); exit;
      }

      $conditions['conditions'] = array(  'BusinessMenuItem.menu_id'  => $menu_id,
                                          'BusinessMenuItem.status'   => 1 
                                      );
      $conditions['order'] = array("BusinessMenuItem.title" => "ASC");
      $conditions['limit'] = $limit;
      $conditions['offset'] = $offset;

      $result = $this->BusinessMenuItem->find('all', $conditions);

      if( $result ){
          $data['status'] = true;
          $data['result'] = $result;
      }else{
          $data['status'] = false;
          $data['message']= "";
      }

      echo json_encode($data); exit;

  }

  public function saveMenuItem( $bis_id = 0, $menu_id = 0, $id = 0 ){

      if( $bis_id == 0 || $menu_id == 0 ){
         
          $this->Session->setFlash(__( 'Invalid Request ! Failed to save data.')
                    .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                  'default',
                  array('class'=>'alert alert-dismissable alert-danger '));

          return $this->redirect(array('action' =>'view', $bis_id, 'menu', $menu_id ));
      }

      $data = $this->request->data;
      
      $dir = 'img/menus' ;
      $thumb_dir = 'img/menus/thumbs' ;

      if( !is_dir($dir) ){
        mkdir($dir, 0755);
      }

      if( !is_dir($thumb_dir) ){
        mkdir($thumb_dir, 0755);
      }

      if( !file_exists($dir . "/default.jpg" )){
          copy ( $this->dealDeafultImage , $dir . "/default.jpg" ) ;     
      }

      if( !file_exists($thumb_dir . "/default.jpg" )){
          copy ( $this->dealDeafultImage , $thumb_dir . "/default.jpg" ) ;     
      }

      $msg = "";

      $img = $_FILES['menuItemImage']['name'];

      if( $img != "" ){
          $fileName = $this->saveFile('menuItemImage', $dir , array() , $this->dealWidth, $this->dealHeight );

          if( is_array($fileName) && $fileName['status'] ==  false ){
              $msg = $fileName['msg'];
              $fileName = $this->dealDeafultImage;
          }else{

              $split = end(explode("/", $fileName));
              $thumb_img =  $thumb_dir . "/" . $split ;

              copy ( $fileName , $thumb_img ) ; 

              // Resize Images
              $this->Image->prepare( $fileName );
              $this->Image->resize($this->dealWidth, $this->dealHeight );
              $this->Image->save( $fileName );

              // Copy a thumbnail
              $this->Image->prepare( $thumb_img );
              $this->Image->resizeThumbnail($this->dealThumbWidth, $this->dealThumbHeight );
              $this->Image->save( $thumb_img );

              // Remove Old Image
              if( isset($data['data_old_img']) && $data['data_old_img'] != "" && $data['data_old_img'] != $dir . "/default.jpg" ){
                  $old_img    = $data['data_old_img'];
                  $old_thumb  = str_replace("/menus/", '/menus/thumbs/', $old_img);

                  if( file_exists($old_img ))     unlink($old_img);
                  if( file_exists($old_thumb))    unlink($old_thumb);
              }
          }

          $data['BusinessMenuItem']['image'] = $fileName; 
      }

      $data['BusinessMenuItem']['business_id'] = $bis_id;
      $data['BusinessMenuItem']['menu_id'] = $menu_id;

      if( !$id ){
          $data['BusinessMenuItem']['created_by'] = $this->user['id']  ;
      }else{
          $data['BusinessMenuItem']['id'] = $id;
      }

      $data['BusinessMenuItem']['modified_by'] = $this->user['id']  ;
      $data['BusinessMenuItem']['status'] = 1  ;

      unset($data['menuItemImage']);

      if( $this->BusinessMenuItem->save($data) ){
          $this->Session->setFlash(__( 'Menu item has been save successfully. ' . $msg )
                    .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                  'default',
                  array('class'=>'alert alert-dismissable alert-success '));
      }else{
          $this->Session->setFlash(__( 'Failed to save . Please try again. ' . $msg )
                    .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                  'default',
                  array('class'=>'alert alert-dismissable alert-dagner '));
      }

      return $this->redirect(array('action' =>'view', $bis_id, 'menu', $menu_id ));

  }

  public function getMenuItemDetailAjax__(){

      $item_id = $this->request->data['item_id'] ;

      if( $item_id == 0 ){
          $data['status']     = false;
          $data['message']    = "Failed to load data ! Please try again." ;

          echo json_decode($data); exit;
      }

      $detail = $this->BusinessMenuItem->findById($item_id);

      if( $detail && !empty($detail) ){
          $data['status'] = true;
          $data['detail'] = $detail['BusinessMenuItem'];
      }else{
          $data['status'] = false;
          $data['message'] = "Failed to load data ! Please try again." ;
      }

      echo json_encode($data); exit;

  }


  public function deleteMenuItemAjax__(){

      $item_id = $this->request->data['item_id'];

      if( $item_id == 0 ){
          $data['status'] = false;
          $data['message'] = "Invalid Request ! Please try again.";
          echo json_encode($data); exit;
      }

      $conds['conditions'] = array( 'BusinessMenuItem.id' => $item_id );

      $ItemData['BusinessMenuItem']['id'] = $item_id;
      $ItemData['BusinessMenuItem']['status'] = 0;

      $default_img = 'img/menus/default.jpg' ;
      $default_thm = 'img/menus/thumbs/default.jpg' ;

      if( $this->BusinessMenuItem->save($ItemData) ){

          $detail = $this->BusinessMenuItem->findById($item_id);
          if( $detail ){
              $image = $detail['BusinessMenuItem']['image'];
              $thumb = str_replace('/menus/' , '/menus/thumbs/' , $image);

              if( file_exists($image) && $image != $default_img ){ unlink($image); }
              if( file_exists($thumb) && $thumb != $default_thm ){ unlink($thumb); }
          }

          $data['status'] = true;
          $data['message'] = "Item has been deleted.";
      }else{
          $data['status'] = false;
          $data['message'] = "Failed to delete item. Please try again.";
      }

      echo json_encode($data); exit;

  }

  public function countTotalMenuItems( $business_id = 0 ){

      $conds['conditions'] = array(   "BusinessMenuItem.business_id" => $business_id,
                                      "BusinessMenuItem.status" => 1 );

      $conds['group'] = array( "BusinessMenuItem.menu_id" ) ;
      $conds['fields'] = array( 'BusinessMenuItem.menu_id', 'COUNT(BusinessMenuItem.id) as total' );

      return $this->BusinessMenuItem->find('all', $conds);
  }


  public function getDealRelatedItemsAjax__() {

    $deal_id = $this->request->data['deal_id'];

    if( $deal_id == 0 ){
        $data['status'] = false;
        $data['message'] = "Something went wrong. Please try again !";

        echo json_encode($data); exit;
    }

    $this->DealItemLink->recursive = 3;

    $items = $this->DealItemLink->find('all', array('conditions' => array('DealItemLink.deal_id' => $deal_id),
                                                    'order'      => array('ItemDetail.title')
                                        ));

    $data['status'] = true;
    $data['result'] = $items;

    echo json_encode($data); exit;

  }

}