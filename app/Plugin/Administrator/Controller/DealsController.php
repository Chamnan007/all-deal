<?php
App::uses('AdministratorAppController', 'Administrator.Controller');

App::uses('Business', 'Administrator.Model');
App::uses('User', 'Administrator.Model');
App::uses('GoodsCategory', 'Administrator.Model');
App::uses('DiscountCategory', 'Administrator.Model');
App::uses('DealItemLink', 'Administrator.Model');

/**
 * Businesses Controller
 *
 * @property Business $Business
 * @property PaginatorComponent $Paginator
 */
class DealsController extends AdministratorAppController {

/**
 * Components
 *
 * @var array
 */
	var $context = 'Deal';
	var $uses  	= array('Administrator.Deal', 
						'Administrator.DealItemLink', 
						'Administrator.Business', 
						'Administrator.DealOfTheDay');

/**
 * index method
 *
 * @return void
 */


	var $dealWidth = 705;
	var $dealHeight = 422;
	var $dealThumbWidth = 223;
	var $dealThumbHeight = 133;
	var $dealImages     = 3;
	var $business_id = 0;

	var $max_expired_day = 7;

	public function beforeFilter(){
		parent::beforeFilter();

		// $this->Deal->recursive = 1;	

		$this->set('deal_status', $this->deal_status);
		$this->set('deal_of_day_text', $this->dod_text);

		$this->set('dealImagesSize', $this->dealImages);

		$goods = new GoodsCategory();
		$this->set('goods', $goods->find('all', array(	'conditions'=> array('GoodsCategory.status' => 1), 
																'order' => array('GoodsCategory.category' => 'ASC' ) )));

	}

	public function setCondition(){

		// $status = $this->getState('status', 1);
		$status = 'all';

		$data = $this->request->data;

        if( isset($data['status']) ){
            $status = $data['status'];
        }else if( $this->getState('status', 'all')){
        	$status = $this->getState('status');
        }

       	$this->setState('status', $status);

       	$last_minute = 0;
       	if( isset($data['last-minute']) ){	
       		$last_minute = 1;
       	}
       	$this->setState('last-minute', $last_minute);

        if( isset($data['deal-title']) ){
        	$this->setState('deal-title', $data['deal-title']);
        }

		if( isset($data['validFrom']) ){
        	$this->setState('validFrom', $data['validFrom']);
        }
        if( isset($data['validTo']) ){
        	$this->setState('validTo', $data['validTo']);
        }

        if( isset($data['business-id']) ){
        	$this->setState('business-id', $data['business-id']);
        }

        if( isset($data['deal-category']) ){
        	$this->setState('deal-category', $data['deal-category']);
        }

        if( isset($data['deal-code']) ){
        	$this->setState('deal-code', $data['deal-code']);
        }

		$now = date('Y-m-d H:i:s');

        if( $status != 'all' ){
        	if( $status == -1 ){
        		$this->conditionFilter['OR']['Deal.status'] = $status;	
				$this->conditionFilter['OR']['Deal.valid_to <'] = $now;
        	}else if($status == 1){
        		$this->conditionFilter['Deal.status'] = $status;	
        		$this->conditionFilter['Deal.valid_to >='] = $now;
        	}else{
        		$this->conditionFilter['Deal.status'] = $status;
        	}
        }

        if( $biz_id = $this->getState('business-id') ){
        	$this->conditionFilter['Deal.business_id'] = $biz_id;
        }

        if( $cate_id = $this->getState('deal-category', NULL) ){
        	$this->conditionFilter['Deal.goods_category LIKE'] = '%"' . $cate_id . '"%' ;
        }

        if( $deal_code = $this->getState('deal-code', NULL) ){
        	$this->conditionFilter['Deal.deal_code'] = $deal_code ;
        }

        $last_minute = $this->getState('last-minute', 0);

        if( $last_minute == 1 ){
        	$this->conditionFilter['Deal.is_last_minute_deal'] = $last_minute;
        }

		$this->paginate['order'] = array("Deal.created" => 'DESC' ) ;
		$this->paginate['conditions'] = $this->conditionFilter;	
		$this->set('dealStates', $this->getState());

		$this->set('conditons', $this->conditionFilter);
	}

	public function index() {

		$this->setCondition();
		parent::index();

		$this->set('status', $this->deal_status);

		// Get Business
		$bizConds['conditions'] = array( 'Business.status' => 1 ) ;
		$bizConds['order'] 		= array( 'Business.business_name' => 'ASC' );

		$this->Business->recursive = -1 ;
		$dataBiz = $this->Business->find('all', $bizConds);
		$this->set('businesses', $dataBiz);

	}

	public function repost( $id = 0, $bis_id = 0 ){

		$user = CakeSession::read("Auth.User");

		$info = $this->Business->find('first', array('conditions' => array('Business.id' => $bis_id) )) ;

		if(	$info ){
			$this->set('lat', $info['Business']['latitude']);
			$this->set('lng', $info['Business']['longitude']);

			$this->set('dealImagesSize', $this->dealImages);
			$this->set('deal_status', $this->deal_status);

			$data_branches = array();
			$data_branches[0]['id'] 		 = 0;
			$data_branches[0]['branch_name'] = "Any branch";
			$data_branches[0]['latitude']	 = $info['Business']['latitude'];
			$data_branches[0]['longitude']	 = $info['Business']['longitude'];

			if( $branches = $info['Branches'] ){
				foreach( $branches as $k => $val ){
					$index = $k + 1;
					$data_branches[$index]['id'] 			= $val['id'];
					$data_branches[$index]['branch_name'] 	= $val['branch_name'];
					$data_branches[$index]['latitude']		= $val['latitude'];
					$data_branches[$index]['longitude']		= $val['longitude'];
				}
			}

			$this->set('merchant_branches', $data_branches);
			$this->set('deal_max_validity', $this->max_expried_day);
		}else{
			return $this->redirect(array('controller' => 'businesses', 'action' =>'view', $bis_id, 'deal', 'plugin' => 'administrator' ));
		}

		$this->Deal->id = $id;
		$this->Deal->recursive = 3;
		$detail = $this->Deal->find('first', array('conditions' => array('Deal.id' => $id, 'Deal.business_id' => $bis_id, 'Deal.isdeleted' => 0) ) );

		if (!$detail || empty($detail) ) {
			$this->Session->setFlash(__( "Invalid Deal Request !" )
											.'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
										'default',
										array('class'=>'alert alert-dismissable alert-danger '));

			return $this->redirect(array('action' =>'view', 'controller' => 'businesses', 'plugin' => 'administrator', $bis_id, 'deal' ));
		}

		if ($this->request->is('post')) {

			$data = $this->request->data;

			$this->request->data['Deal']['business_id'] = $bis_id ;
			$this->request->data['Deal']['title_2']   	= "";
			$this->request->data['Deal']['en_barcode']  = 0 ;
			$this->request->data['Deal']['status']      = 1 ;
			$this->request->data['Deal']['isdeleted']   = 0 ;

			if(!isset($this->request->data['Deal']['qr_code'])){
				$this->request->data['Deal']['qr_code']   = 0 ;
			}

			$this->request->data['Deal']['created_by']    = $user['id'];
			$this->request->data['Deal']['updated_by']    = $user['id'];

			$this->request->data['Deal']['discount_category'] = "";
			$this->request->data['Deal']['interest_category'] = json_encode((isset($this->request->data['Deal']['interest_category']))?$this->request->data['Deal']['interest_category']:"" );
			$this->request->data['Deal']['services_category'] = json_encode((isset($this->request->data['Deal']['services_category']))?$this->request->data['Deal']['services_category']:"" );
			$this->request->data['Deal']['goods_category'] = json_encode((isset($this->request->data['Deal']['goods_category']))?$this->request->data['Deal']['goods_category']:"" );


			$this->request->data['Deal']['valid_from'] = date('Y-m-d H:i:s', strtotime($this->request->data['Deal']['valid_from']));
			$this->request->data['Deal']['valid_to'] = date('Y-m-d H:i:s', strtotime($this->request->data['Deal']['valid_to']));

			if( isset($this->request->data['Deal']['travel_destination']) ){
				$this->request->data['Deal']['travel_destination'] = json_encode($this->request->data['Deal']['travel_destination']);
			}else{
				$this->request->data['Deal']['travel_destination'] = '[""]';
			}

			if( isset($this->request->data['Deal']['is_last_minute_deal']) ) {
				$this->request->data['Deal']['is_last_minute_deal'] = 1;
			}else{
				$this->request->data['Deal']['is_last_minute_deal'] = 0;
			}

			$image = $_FILES['deal_image'] ;

			$dir = 'img/deals' ;
			$thumb_dir = 'img/deals/thumbs' ;

			if( !is_dir($thumb_dir) ){
				mkdir($thumb_dir, 0700);
			}

			if( $image['name'][0] != "" ){

				$dir = 'img/deals' ;

				if( !is_dir($dir) ){
					mkdir($dir, 0700);
				}

				$data_img = array();

				foreach( $image['name'] as $k => $img ){

						$images = array();
						$images['name']     = $img;
						$images['type']     = $image['type'][$k];
						$images['tmp_name'] = $image['tmp_name'][$k];
						$images['error']    = $image['error'][$k];
						$images['size']     = $image['size'][$k];

						$fileName = $this->saveMultiple($images, $dir, array() ,  $this->dealWidth, $this->dealHeight);

						if( is_array($fileName) && $fileName['status'] ==  false ){
							$error = $fileName['msg'];
							$fileName = "img/deals/default.jpg";
						}else{
							$thumb_dir = 'img/deals/thumbs' ;

							if( !is_dir($thumb_dir) ){
								mkdir($thumb_dir, 0755);
							}

							$split = end(explode("/", $fileName));
							$thumb_img =  $thumb_dir . "/" . $split ;
							copy ( $fileName , $thumb_img ) ; 

							// Resize Images
							$this->Image->prepare( $fileName );
							$this->Image->resize( $this->dealWidth , $this->dealHeight );
							$this->Image->save( $fileName );

							// Copy a thumbnail
							$this->Image->prepare( $thumb_img );
							$this->Image->resizeThumbnail($this->dealThumbWidth, $this->dealThumbHeight);
							$this->Image->save( $thumb_img );

						}

						if( $k == 0 ){
								$this->request->data['Deal']['image'] = $fileName;
						}
						
						$data_img[] = $fileName;						
				}

				$this->request->data['Deal']['other_images'] = json_encode($data_img);


			}else{

				if( isset($this->request->data['existed_images']) ){

					$existed_imgs = $this->request->data['existed_images'];
					unset($this->request->data['existed_images']);
					$imgs = array();

					foreach( $existed_imgs as $k => $img ){
						$pos = strpos($img, ".");
						$ext = substr($img, $pos + 1, strlen($img));

						$name = substr($img, 0, $pos);
						$name = $name . time();
						$file = $name . "." . $ext;

						$this->request->data['Deal']['image'] = $file;

						// Copy Big Image
						copy ( $img , $file ) ; 

						$thumb_img 		= str_replace("/deals/", "/deals/thumbs/", $img ) ;
						$new_thmb_img  	= str_replace("/deals/", "/deals/thumbs/", $file ) ;
						// Copy Thumb Image
						copy ( $thumb_img , $new_thmb_img ) ; 

						$imgs[] = $file;
					}

					$this->request->data['Deal']['other_images'] = json_encode($imgs);
				}else{
					$this->request->data['Deal']['image'] = 'img/deals/default.jpg' ;
				}

			}

			$err = "";
			$this->Deal->create();

			if( $this->Deal->save($this->request->data)){

				$deal_id = $this->Deal->getLastInsertID();

				$deal_code 	= "D" . str_pad($deal_id, 6, 0, STR_PAD_LEFT);
				$updateData = array();
				$updateData['Deal']['id'] 		= $deal_id;
				$updateData['Deal']['deal_code']= $deal_code;
				$this->Deal->save($updateData);	

				// Delete Old relate and add new related
				if( isset($this->request->data['DealItemLink']) ){

					$datas = $this->request->data['DealItemLink']['item_id'];
					$item_data = array();
					foreach( $datas as $key => $val ){
						$temp = array();
						$temp['DealItemLink']['deal_id']  		= $deal_id;
						$temp['DealItemLink']['item_id']  		= $val;
						$temp['DealItemLink']['original_price'] = $this->request->data['DealItemLink']['original_price'][$key];
						$temp['DealItemLink']['discount'] 		= $this->request->data['DealItemLink']['discount'][$key];
						$temp['DealItemLink']['available_qty'] 	= $this->request->data['DealItemLink']['availble_qty'][$key];
						$temp['DealItemLink']['status'] 		= 1;

						$item_data[] = $temp;
					}

					$this->DealItemLink->saveAll($item_data);
				}
				

				$this->Session->setFlash(__( 'Deal Information has been saved. ' . $err)
											.'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
										'default',
										array('class'=>'alert alert-dismissable alert-success '));

				$logMessage = json_encode($this->request->data);

				$this->generateLog($logMessage,' CREATE NEW ');       
				
			}else{
				$this->Session->setFlash(__( 'Deal Information could not be saved. ' . $err)
											.'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
										'default',
										array('class'=>'alert alert-dismissable alert-danger '));
			}
			return $this->redirect(array('action' =>'view', 'controller' => 'businesses', 'plugin' => 'administrator', $bis_id, 'deal' ));

		}else{


			// Business Menu Category
			$menuObj = new BusinessMenu();
			$this->set('business_menus', $menuObj->find('all', array('conditions'=> array('status' => 1, 'business_id' => $bis_id), 'order' => array('name' => 'ASC'))));

			$goods = new GoodsCategory();
			$this->set('goods', $goods->find('all', array(  'conditions'=> array('GoodsCategory.status' => 1), 
																	'order' => array('GoodsCategory.category' => 'ASC' ) )));
			

			$this->set('deal', $detail);
			$this->set('bis_id', $bis_id);
		}

	}


	public function setDoDConditions(){

		$this->context = "DealOfTheDay";
		$now = date('Y-m-d');
		$conds = array();

		if( isset($this->request->data['available_date']) && $this->request->data['available_date'] != "" ){
			$now = date('Y-m-d', strtotime($this->request->data['available_date']));
		}		

		$this->setState('available_date', $now);		

		if( isset($this->request->data['business-id']) ){
        	$this->setState('business-id', $this->request->data['business-id']);
        }

		if( isset($this->request->data['deal-category']) ){
        	$this->setState('deal-category', $this->request->data['deal-category']);
        }

		if( isset($this->request->data['deal-code']) ){
        	$this->setState('deal-code', $this->request->data['deal-code']);
        }

        if( $biz_id = $this->getState('business-id', NULL) ){
        	$conds['DealDetail.business_id'] = $biz_id;
        }

        if( $now = $this->getState('available_date', NULL )){
			$conds['DealOfTheDay.available_date'] = $now;
		}

		if( $cate_id = $this->getState('deal-category', NULL) ){
        	$conds['DealDetail.goods_category LIKE'] = '%"' . $cate_id . '"%' ;
        }
		if( $code = $this->getState('deal-code', NULL) ){
        	$conds['DealDetail.deal_code'] = $code  ;
        }
		
		$this->paginate['conditions'] = $conds;
		$this->paginate['order']	 = array( 	'DealOfTheDay.available_date' 	=> 'ASC', 
												'DealOfTheDay.created' 			=> 'DESC',
												'DealDetail.created' 			=> 'DESC' );

		$states = $this->getState();
		$this->set('dod_states',$states);
	}

	public function dod() {

		$this->setDoDConditions();
		$this->set('status', $this->deal_status);

		// Get Business
		$bizConds['conditions'] = array( 'Business.status' => 1 ) ;
		$bizConds['order'] 		= array( 'Business.business_name' => 'ASC' );

		$this->Business->recursive = -1 ;
		$dataBiz = $this->Business->find('all', $bizConds);
		$this->set('businesses', $dataBiz);

		// Get Deal of the Day
		$this->DealOfTheDay->recursive = 2;
		$data = $this->paginate('DealOfTheDay');	

		$this->set(compact('data'));

	}


	public function add(){

		$user = CakeSession::read("Auth.User");	

		$business = new Business();
		$business->recursive = -1;
		$this->set('businesses', $business->find('all', array(	'conditions'=> array('Business.status' => 1), 
																'order' => array('Business.business_name' => 'ASC' ) )));
		$goods = new GoodsCategory();
		$this->set('goods', $goods->find('all', array(	'conditions'=> array('GoodsCategory.status' => 1), 
																'order' => array('GoodsCategory.category' => 'ASC' ) )));

		if ($this->request->is('post')) {

			$this->request->data['Deal']['title_2'] 	= "" ;
			$this->request->data['Deal']['en_barcode'] 	= 0 ;

			$this->request->data['Deal']['created_by'] 	= $user['id'];
			$this->request->data['Deal']['updated_by'] 	= $user['id'];
			$this->request->data['Deal']['publish_date'] 	= DboSource::expression('NOW()');

			$this->request->data['Deal']['discount_category'] = json_encode($this->request->data['Deal']['discount_category']);
			$this->request->data['Deal']['interest_category'] = json_encode($this->request->data['Deal']['interest_category']);
			$this->request->data['Deal']['services_category'] = json_encode($this->request->data['Deal']['services_category']);
			$this->request->data['Deal']['goods_category'] = json_encode($this->request->data['Deal']['goods_category']);

			$this->request->data['Deal']['valid_from'] = date('Y-m-d H:i:s', strtotime($this->request->data['Deal']['valid_from']));
			$this->request->data['Deal']['valid_to'] = date('Y-m-d H:i:s', strtotime($this->request->data['Deal']['valid_to']));

			$image = $_FILES['deal_image'] ;

			if( $image['name'] != "" ){

				$dir = 'img/deals' ;

				if( !is_dir($dir) ){
					mkdir($dir, 0700);
				}

				$fileName = $this->saveFile('deal_image', $dir );
				$error = "";

				if( is_array($fileName) && $fileName['status'] ==  false ){
					$error = $fileName['msg'];
					$fileName = "";
				}			

				$this->request->data['Deal']['image'] = $fileName;
			}else{
				$this->request->data['Deal']['image'] = 'img/deals/default.jpg' ;
			}


			$this->Deal->create();
			if( $this->Deal->save($this->request->data)){
				$this->Session->setFlash(__( 'New Deal has been created. ' . $error )
											.'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
										'default',
										array('class'=>'alert alert-dismissable alert-success '));

				$logMessage = json_encode($this->request->data);

				$this->generateLog($logMessage,' CREATE NEW ');				
				return $this->redirect(array('action' =>'index'));
			}else{
				$this->Session->setFlash(__( $this->context.' could not be created. ' . $error )
											.'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
										'default',
										array('class'=>'alert alert-dismissable alert-danger '));
			}
		}

	}


	public function view( $id = 0 ){

		$this->Deal->id = $id;

		$this->Deal->recursive = 2;
		$detail = $this->Deal->find('first', array('conditions' => array('Deal.id' => $id) ) );

		if (!$detail || empty($detail) ) {
			// throw new NotFoundException(__('Invalid business'));
			$this->Session->setFlash(__( "Invalid Deal Request !" )
											.'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
										'default',
										array('class'=>'alert alert-dismissable alert-danger '));

			return $this->redirect(array('action' =>'index'));
		}

		$goods = new GoodsCategory();
		$this->set('goods', $goods->find('all', array(  'conditions'=> array('GoodsCategory.status' => 1), 
																'order' => array('GoodsCategory.category' => 'ASC' ) )));

		// $deal = $this->Deal->findById($id);
		$this->set('deal', $detail);

	}


	public function getDealDetailAjax__( $deal_id = 0 ){
		
		$result['status'] = false;

		if( $deal_id ){

			$this->Deal->recursive = 2;
			$data = $this->Deal->findById($deal_id);

			if( $data ){
				$result['status'] 	= true;
				$result['data'] 	= $data;
			}
		}

		echo json_encode($result); exit;
	}

	public function approve(){

		if( $this->request->is('post') && $this->request->data['deal-id'] != 0 ){
			$deal_data = array();

			$user = CakeSession::read("Auth.User");

			$deal_data['Deal']['id'] 		= $this->request->data['deal-id'];
			$deal_data['Deal']['status']  	= 1;
			$deal_data['Deal']['updated_by'] = $user['id'];

			if( isset($this->request->data['last-minute']) ) {
				$deal_data['Deal']['is_last_minute_deal'] = 1;
			}else{
				$deal_data['Deal']['is_last_minute_deal'] = 0;
			}

			if( $this->Deal->save($deal_data)){
				$this->Session->setFlash(__( 'Deal has been approved.')
										.'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
									'default',
									array('class'=>'alert alert-dismissable alert-success '));
			}else{
				$this->Session->setFlash(__( 'No Deal has been approved.')
										.'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
									'default',
									array('class'=>'alert alert-dismissable alert-warning '));
			}
		}else{
			$this->Session->setFlash(__( 'No Deal has been approved.')
										.'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
									'default',
									array('class'=>'alert alert-dismissable alert-warning '));
		}

		return $this->redirect('/administrator');

	}


	public function setDealOfTheDay(){

		if( $this->request->is('post') ){
			
			$user = CakeSession::read("Auth.User");	
			$user_id = $user['id'] ;

			$data = $this->request->data;

			$date 	 = date("Y-m-d", strtotime($data['available-date']));
			$dealIds = $data['deal-id'];

			foreach( $dealIds as $k => $id ){

				$tmp = array();
				$tmp['DealOfTheDay']['available_date'] = $date;
				$tmp['DealOfTheDay']['deal_id'] 	   = $id;
				$tmp['DealOfTheDay']['modified']       = date('Y-m-d H:i:s');

				// Get if this deal already been set in this date
				$conds['conditions'] = array( 'DealOfTheDay.available_date' => $date, 'DealOfTheDay.deal_id' => $id );
				$dodData = $this->DealOfTheDay->find('first', $conds);

				if( $dodData && !empty($dodData) ){

					$tmp['DealOfTheDay']['id'] 	= $dodData['DealOfTheDay']['id'];
					$this->DealOfTheDay->id 	= $dodData['DealOfTheDay']['id'];

				}else{

					$tmp['DealOfTheDay']['created'] 	= date('Y-m-d H:i:s');
					$tmp['DealOfTheDay']['created_by'] 	= $user_id;

				}

				$this->DealOfTheDay->create();
				$this->DealOfTheDay->save($tmp);
				
			}

			$this->Session->setFlash(__( 'Deal(s) have been saved as deal of the day.')
						.'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
						'default',
						array('class'=>'alert alert-dismissable alert-success '));

		}
		
		return $this->redirect('/administrator/deals');

	}

	public function removeDealOfTheDay(){

		if( $this->request->is('post') ){
			
			$user = CakeSession::read("Auth.User");	
			$user_id = $user['id'] ;

			$data = $this->request->data;

			$date 	 = date("Y-m-d", strtotime($data['available-date']));
			$dealIds = $data['deal-id'];

			// Set Conditions to remove
			$this->DealOfTheDay->deleteAll( array('DealOfTheDay.available_date' => $date, 'DealOfTheDay.deal_id' => $dealIds), false);

			$this->Session->setFlash(__( 'Deal(s) have been removed from deal of the day list.')
						.'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
						'default',
						array('class'=>'alert alert-dismissable alert-success '));

		}
		
		return $this->redirect('/administrator/deals');
	}

	public function removeDodFromList(){

		if( $this->request->is('post') ){
			
			$user = CakeSession::read("Auth.User");	
			$user_id = $user['id'] ;

			$data = $this->request->data;
			$Ids = $data['id'];

			// Set Conditions to remove
			$this->DealOfTheDay->deleteAll( array('DealOfTheDay.id' => $Ids), false);

			$this->Session->setFlash(__( 'Deal(s) have been removed from deal of the day list.')
						.'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
						'default',
						array('class'=>'alert alert-dismissable alert-success '));

		}
		
		return $this->redirect('/administrator/deals/dod');
	}

}
