<?php
App::uses('AdministratorAppController', 'Administrator.Controller');
/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class CommissionsController extends AdministratorAppController {

/**
 * Components
 *
 * @var array
 */
  public $components = array('Paginator');

  var $uses = array( 'Administrator.Commission' );

/**
 * index method
 *
 * @return void
 */

  var $user = "" ;

  public function beforeFilter(){
      parent::beforeFilter();
      $this->user = CakeSession::read("Auth.User");
      $this->set('user', $this->user);


      $type[1] = "Commission From Bussiness Sale (%)" ;
      $type[2] = "Sign Up Bonus ($)";
      $type[3] = "Bonus For Referer ($)";
      $type[4] = "Bonus For Register ($)";

      $this->set('type', $type);

      $status[1] = "Active";
      $status[0] = 'Inactive';
      $this->set('com_status', $status);

      $this->set('status', $this->status);
      
  }


  public function index(){

      $data = $this->Commission->find('all');

      $result = array();

      if( $data && !empty($data) ){
        foreach( $data as $key => $val ){
          $result[$val['Commission']['type']] = $val['Commission'];
        }
      }

      $this->set('result', $result);

  }

    public function save( $type_id = 0 ){
        $data = $this->request->data;
        $data['Commission']['type'] = $type_id;

        $conds  = array('conditions' => array('type' => $type_id) );
        $result = $this->Commission->find('first', $conds);

        if( $result && !empty($result) ){
            $id = $result['Commission']['id'];
            $data['Commission']['id'] = $id;
        }

        $this->Commission->create();

        if( $this->Commission->save($data) ){

            $action = " ADD ";
            if( isset($id) ){
                $action = " EDIT ";
            }

            $logMessage = json_encode($data);
            parent::generateLog( $logMessage, $action );

            $this->Session->setFlash(__( 'Commission has been saved.')
                                    .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                                    'default',
                                    array('class'=>'alert alert-dismissable alert-success '));

        }

        return $this->redirect(array('action' =>'index'));
        
    }  


}