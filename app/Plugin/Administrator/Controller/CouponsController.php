<?php
App::uses('AdministratorAppController', 'Administrator.Controller');

App::uses('Business', 'Administrator.Model');
App::uses('Province', 'Administrator.Model');
App::uses('City', 'Administrator.Model');
App::uses('User', 'Administrator.Model');
App::uses('InterestsCategory', 'Administrator.Model');
App::uses('ServicesCategory', 'Administrator.Model');
App::uses('GoodsCategory', 'Administrator.Model');
App::uses('DiscountCategory', 'Administrator.Model');

/**
 * Businesses Controller
 *
 * @property Business $Business
 * @property PaginatorComponent $Paginator
 */
class CouponsController extends AdministratorAppController {

/**
 * Components
 *
 * @var array
 */
	var $context = 'Coupon';

/**
 * index method
 *
 * @return void
 */

	public function index() {

		$this->Coupon->recursive = 1;
		$this->paginate['order'] = array("Coupon.created" => 'DESC' ) ;
		parent::index();

	}


	public function add(){

		$province = new Province();
		$this->set('provinces', $province->find('all', array(	'conditions'=> array('Province.status' => 1), 
																'order' => array('Province.province_code' => 'ASC' ) )));

		$business = new Business();
		$business->recursive = -1;
		$this->set('businesses', $business->find('all', array(	'conditions'=> array('Business.status' => 1), 
																'order' => array('Business.business_name' => 'ASC' ) )));

		$discount = new DiscountCategory();
		$this->set('discounts', $discount->find('all', array(	'conditions'=> array('DiscountCategory.status' => 1), 
																'order' => array('DiscountCategory.category' => 'ASC' ) )));

	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */


}
