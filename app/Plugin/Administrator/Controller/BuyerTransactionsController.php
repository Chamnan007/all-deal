<?php
App::uses('AdministratorAppController', 'Administrator.Controller');

/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class BuyerTransactionsController extends AdministratorAppController {

/**
 * Components
 *
 * @var array
 */

  var $context = 'BuyerTransaction' ;

  var $uses = array(
                    "Administrator.BuyerTransaction",
                    "Administrator.Business",
              );

  var $business_id = 0;
  var $userID = 0;

/**
 * index method
 *
 * @return void
 */
    

    public function beforeFilter(){
      parent::beforeFilter();

        $this->set('status', $this->status);   

        $user = CakeSession::read("Auth.User");

        $this->userID = $user['id'];

        $this->set('purchase_status', $this->buy_transaction_status);
        $this->set('payment_method',  $this->payment_method);
        $this->set('__CREDIT_TYPE', $this->__CREDIT_PAYMENT );

    }

    public function setCondition(){

        parent::setCondition();

        $status = $this->getState('status', '0');

        if( isset($this->request->data['status']) ){
            $status = $this->request->data['status'];
        }

        if( $status ){
            $this->setState('status', $status);
        }

        if( isset($this->request->data['code']) ){
            $this->setState('code', $this->request->data['code'] );
        }

        if( isset($this->request->data['purchaseDateFrom']) ){
            $this->setState('purchaseDateFrom', $this->request->data['purchaseDateFrom'] );
        }
        if( isset($this->request->data['purchaseDateTo']) ){
            $this->setState('purchaseDateTo', $this->request->data['purchaseDateTo'] );
        }

        if( isset($this->request->data['receivedDateFrom']) ){
            $this->setState('receivedDateFrom', $this->request->data['receivedDateFrom'] );
        }

        if( isset($this->request->data['receivedDateTo']) ){
            $this->setState('receivedDateTo', $this->request->data['receivedDateTo'] );
        }

        if( isset($this->request->data['business_id']) ){
            $this->setState('business_id', $this->request->data['business_id'] );
        }

        if( $code = $this->getState('code', NULL )){
            $this->conditionFilter['BuyerTransaction.code'] = $code ;            
        }

        if( $business_id = $this->getState('business_id', NULL )){
            $this->conditionFilter['BuyerTransaction.business_id'] = $business_id ;            
        }

        if( $purchaseFrom = $this->getState('purchaseDateFrom', NULL ) ){
            $purchaseFrom = date('Y-m-d 00:00:00', strtotime($purchaseFrom));
            $this->conditionFilter['BuyerTransaction.created >='] = $purchaseFrom;
        }

        if( $purchaseTo = $this->getState('purchaseDateTo', NULL ) ){
            $purchaseTo = date('Y-m-d 23:59:59', strtotime($purchaseTo));
            $this->conditionFilter['BuyerTransaction.created <='] = $purchaseTo;
        }

        if( $receivedFrom = $this->getState('receivedDateFrom', NULL ) ){
            $receivedFrom = date('Y-m-d 00:00:00', strtotime($receivedFrom));
            $this->conditionFilter['BuyerTransaction.received_date >='] = $receivedFrom;
        }

        if( $receivedTo = $this->getState('receivedDateTo', NULL ) ){
            $receivedTo = date('Y-m-d 23:59:59', strtotime($receivedTo));
            $this->conditionFilter['BuyerTransaction.received_date <='] = $receivedTo;
        }

        if( $status != 'all' ){
            $this->conditionFilter['BuyerTransaction.status'] = $status;
        }

        $this->conditionFilter['BuyerTransaction.type'] = 0;

        $this->paginate['conditions'] = $this->conditionFilter;
        $this->paginate['order']      = array( 
                                          'BuyerTransaction.created' => 'DESC',
                                          'TransactionDetail.code' => 'DESC'
                                        );

        $this->set('states', $this->getState());

    }

    public function index(){

        $this->setCondition();

        $data = $this->paginate($this->getContext());  

        // Count Pending
        $pending = $this->BuyerTransaction->find('count', array('conditions' => array('BuyerTransaction.status' => 0, 'BuyerTransaction.type' => 0)) );

        $this->set('totalPendingPurchase', $pending);
        $this->set('data', $data);

        // Get Business Listing
        $this->Business->recursive = -1;
        $bizData = $this->Business->find('all', array('conditions' => array('Business.status' => 1),
                                                      'order' => array('Business.business_name' => "ASC" )  ));

        $this->set('bizData', $bizData);

    }

    public function view( $id = 0 ){

        $this->BuyerTransaction->recursive = 2;
        $conds['conditions'] = array('BuyerTransaction.id' => $id);
        $data = $this->BuyerTransaction->find('first', $conds);

        if( !$data || empty($data) ){

            $this->Session->setFlash(__( "Invalid Request !" )
                      .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                    'default',
                    array('class'=>'alert alert-dismissable alert-danger '));
        }

        $this->set('data', $data);   
    }

    public function redeem(){

        $search = false;
        $data = array();
        $code = "";

        if( $this->request->is('post') ){
            $code = $this->request->data['coupon-code'];
            $conds['conditions'] = array( 'BuyerTransaction.code' => $code );

            $this->BuyerTransaction->recursive = 3;
            $data = $this->BuyerTransaction->find('first', $conds);
            $search = true;
        }

        $this->set('code', $code);
        $this->set('data', $data);
        $this->set('search', $search);

    }

    public function markRedeem(){

        if( $this->request->is('post') ){
            $data = $this->request->data;

            $data['BuyerTransaction']['status'] = 1;
            $data['BuyerTransaction']['mark_received_by'] = $this->userID;
            $data['BuyerTransaction']['received_date']    = date('Y-m-d H:i:s');

            if( $this->BuyerTransaction->save($data) ){

                $id = $data['BuyerTransaction']['id'];

                $this->Session->setFlash(__( "Purchase has been redeemed !" )
                      .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                    'default',
                    array('class'=>'alert alert-dismissable alert-success '));

                return $this->redirect(array('action' => 'view', $id));
            }

        }

        return $this->redirect(array('action' => 'index'));
    }


}