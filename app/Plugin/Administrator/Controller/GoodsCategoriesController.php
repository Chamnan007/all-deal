<?php
App::uses('AdministratorAppController', 'Administrator.Controller');
/**
 * GoodsCategories Controller
 *
 * @property GoodsCategory $GoodsCategory
 * @property PaginatorComponent $Paginator
 */
class GoodsCategoriesController extends AdministratorAppController {

/**
 * Components
 *
 * @var array
 */
		var $context = 'GoodsCategory';
/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->GoodsCategory->recursive = 0;	
			
		$this->conditionFilter['status'] = 1;
		$this->paginate['conditions'] = $this->conditionFilter;
		$this->paginate['order'] = array("GoodsCategory.category" => 'ASC' ) ;
		parent::index();
	}



public function beforeFilter(){
      parent::beforeFilter();
		$user =  CakeSession::read("Auth.User"); 

		if( $user['access_level'] != 6 ){
			$this->redirect(array('action' => 'index', 'controller' => 'dashboards' ));
		}
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->GoodsCategory->exists($id)) {
			throw new NotFoundException(__('Invalid goods category'));
		}
		$options = array('conditions' => array('GoodsCategory.' . $this->GoodsCategory->primaryKey => $id));
		$this->set('goodsCategory', $this->GoodsCategory->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			parent::save();
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->GoodsCategory->exists($id)) {
			throw new NotFoundException(__('Invalid goods category'));
		}
		if ($this->request->is(array('post', 'put'))) {
			parent::save($id);
		} else {
			$options = array('conditions' => array('GoodsCategory.' . $this->GoodsCategory->primaryKey => $id));
			$this->request->data = $this->GoodsCategory->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */

	public function delete($id = null) {

		if (!$this->GoodsCategory->exists($id)) {
			throw new NotFoundException(__('Invalid Category'));
		}

		$this->GoodsCategory->id = $id;
		
		if ($this->GoodsCategory->save(array('status'=>0))) 
		{
			$this->Session->setFlash(__('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button><b>Success!!!</b> The Goods Category has been deleted.</div>'));

			$obj 	= $this->GoodsCategory->findById($id);
			$logMessage = json_encode($obj);
			parent::generateLog($logMessage,' DELETE :'.$id);
		} else {
			$this->Session->setFlash(__('<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button><b>Failed!!!</b> The Goods Category could not be deleted. Please, try again.</div>'));
		}

		// var_dump($this->request->data); exit;
	
		return $this->redirect(array('action' => 'index'));
	}


}
