<?php
App::uses('AdministratorAppController', 'Administrator.Controller');
App::uses('City', 'Administrator.Model');
App::uses('Location', 'Administrator.Model');
App::uses('Province', 'Administrator.Model');
/**
 * Locations Controller
 *
 * @property Location $Location
 * @property PaginatorComponent $Paginator
 */
class SangkatsController extends AdministratorAppController {

/**
 * Components
 *
 * @var array
 */
	var $context = 'Sangkat';

	var $uses  = array(	'Administrator.City' , 
						'Administrator.Location' , 
						'Administrator.Province', 
						'Administrator.Sangkat' );

/**
 * index method
 *
 * @return void
 */
	


	public function beforeFilter(){		
		parent::beforeFilter();		
	}


	public function index() {

		$user =  CakeSession::read("Auth.User"); 

		if( $user['access_level'] != 6 ){
			$this->redirect(array('action' => 'index', 'controller' => 'dashboards' ));
		}else{

			$this->City  = new City();
			$this->set('Cities', $this->City->find('all', array('conditions' => array('City.status' => 1), 
																		'order' => 'City.city_name ASC') ) ) ;

			$this->conditionFilter['Sangkat.status'] = 1 ;
			$this->paginate['conditions'] = $this->conditionFilter;
			
			parent::index();
			
		}
	}


/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$user =  CakeSession::read("Auth.User"); 

		if( $user['access_level'] != 6 ){
			$this->redirect(array('action' => 'index', 'controller' => 'dashboards' ));
		}else{
			if (!$this->Location->exists($id)) {
				throw new NotFoundException(__('Invalid location'));
			}
			$options = array('conditions' => array('Location.' . $this->Location->primaryKey => $id));
			$this->set('location', $this->Location->find('first', $options));
		}
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {

		$user =  CakeSession::read("Auth.User"); 

		if( $user['access_level'] != 6 ){
			$this->redirect(array('action' => 'index', 'controller' => 'dashboards' ));
		}else{
			// $this->City  = new City();
			// $this->set('cities', $this->City->find('all', array('conditions' => array('City.status' => 1) ) ) ) ;

			if ($this->request->is('post')) {
				parent::save();
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {

		$user =  CakeSession::read("Auth.User");

		if( $user['access_level'] != 6 ){
			$this->redirect(array('action' => 'index', 'controller' => 'dashboards' ));
		}else{

			if (!$this->Location->exists($id)) {
				throw new NotFoundException(__('Invalid location'));
			}
			if ($this->request->is('post') ) {

				parent::save($id);

			}
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {

		$user =  CakeSession::read("Auth.User"); 

		if( $user['access_level'] != 6 ){
			$this->redirect(array('action' => 'index', 'controller' => 'dashboards' ));

		}else{

			if (!$this->Sangkat->exists($id)) {
				throw new NotFoundException(__('Invalid Sangkat'));
			}

			$this->Sangkat->id = $id;
			$this->request->data['Sangkat']['id'] 		= $id;
			$this->request->data['Sangkat']['status'] 	= 0;

			if ($this->Sangkat->save($this->request->data)) {

				$this->Session->setFlash(__('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button><b>Success!!!</b> Sangkat has been deleted.</div>'));

				$obj 	= $this->Sangkat->findById($id);
				$logMessage = json_encode($obj);
				parent::generateLog($logMessage,' DELETE :'.$id);

			} else {

				$this->Session->setFlash(__('<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button><b>Failed!!!</b> Sangkat could not be deleted. Please, try again.</div>'));
			}
		
			return $this->redirect(array('action' => 'index'));
		}

	}
	

	public function getSangkatByLocation( $location_id = 0 ){

		if( $location_id != null ){
			$sangkats =  $this->Sangkat->find( 'all', array( 'conditions' => array('Sangkat.location_id' => $location_id ))) ;

			$data = array();
			$data['data'] =  $sangkats;
			echo json_encode( $data);

			$this->autoRender = false;

		}
	}

}
