<?php
App::uses('AdministratorAppController', 'Administrator.Controller');
/**
 * Destinations Controller
 *
 * @property Destination $Destination
 * @property PaginatorComponent $Paginator
 */
class DestinationsController extends AdministratorAppController {

/**
 * Components
 *
 * @var array
 */

/**
 * index method
 *
 * @return void
 */
	var $context = 'Destination';
	// var $uses  = array('Administrator.Destination');

	public function index() {
		
		$this->Destination->recursive = 0;
		$this->conditionFilter['status'] = 1;
		$this->paginate['conditions'] = $this->conditionFilter;
		$this->paginate['order'] = array("Destination.destination" => 'ASC' ) ;
		parent::index();
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			parent::save();
		}
	}


public function beforeFilter(){
      parent::beforeFilter();
		$user =  CakeSession::read("Auth.User"); 

		if( $user['access_level'] != 6 ){
			$this->redirect(array('action' => 'index', 'controller' => 'dashboards' ));
		}
	}


/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {

		if (!$this->Destination->exists($id)) {
			$this->Session->setFlash(__('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">×</button>
											<b>Failed!!!</b> Invalid Request.</div>'));
			$this->redirect(array('action' => 'index'));
		}

		if ($this->request->is(array('post', 'put'))) {
			parent::save($id);
		} else {
			$options = array('conditions' => array('Destination.' . $this->Destination->primaryKey => $id));
			$this->request->data = $this->Destination->find('first', $options);
		}
	}


	public function delete($id = null) {

		if (!$this->Destination->exists($id)) {
			$this->Session->setFlash(__('<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">×</button>
											<b>Failed!!!</b> Invalid Request.</div>'));
			$this->redirect(array('action' => 'index'));
		}

		$this->Destination->id = $id;
		
		if ($this->Destination->save(array('status'=>0))) 
		{
			$this->Session->setFlash(__('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button><b>Success!!!</b> The Destination has been deleted.</div>'));

			$obj 	= $this->Destination->findById($id);
			$logMessage = json_encode($obj);
			parent::generateLog($logMessage,' DELETE :'.$id);

		} else {

			$this->Session->setFlash(__('<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button><b>Failed!!!</b> The Destination could not be deleted. Please, try again.</div>'));
		}
	
		return $this->redirect(array('action' => 'index'));
	}

}
