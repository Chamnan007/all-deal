<?php
App::uses('AdministratorAppController', 'Administrator.Controller');
/**
 * BusinessCategories Controller
 *
 * @property BusinessCategory $BusinessCategory
 * @property PaginatorComponent $Paginator
 */
class BusinessCategoriesController extends AdministratorAppController {

/**
 * Components
 *
 * @var array
 */
	var $context = 'BusinessCategory';
	// public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */

	public function index() {

		$user = CakeSession::read("Auth.User");

		if( $user['access_level'] !=6 ){
			$this->redirect( array('action' => 'index', 'controller' => 'dashboards' ) );
		}

		$this->conditionFilter['BusinessCategory.status'] = 1;
		$this->conditionFilter['BusinessCategory.parent_id'] = 0;
		$this->paginate['conditions'] = $this->conditionFilter;
		$this->paginate['order'] = array("BusinessCategory.category" => 'ASC' ) ;
		parent::index();
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {

		$user = CakeSession::read("Auth.User");

		if( $user['access_level'] !=6 ){
			$this->redirect( array('action' => 'index', 'controller' => 'dashboards' ) );
		}
		
		if (!$this->BusinessCategory->exists($id)) {
			$this->Session->setFlash(__( 'Invalid Request.')
                                        .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                                    'default',
                                    array('class'=>'alert alert-dismissable alert-danger '));

			$this->redirect(array('action' => 'index'));
		}

		$options = array('conditions' => array('BusinessCategory.' . $this->BusinessCategory->primaryKey => $id));
		$this->set('businessCategory', $this->BusinessCategory->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {

		$user = CakeSession::read("Auth.User");

		if( $user['access_level'] !=6 ){
			$this->redirect( array('action' => 'index', 'controller' => 'dashboards' ) );
		}

		$options = array('conditions' => array('BusinessCategory.parent_id = 0 AND BusinessCategory.status = 1'), 'order' => array('BusinessCategory.category' => 'ASC' ) );

		$this->set('parentCategories', $this->BusinessCategory->find('all', $options));

		if ($this->request->is('post')) {
			parent::save();
		}

	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */

	public function delete( $id = NULL ){

		if( !$id || !$this->BusinessCategory->exists($id) ){
			$this->Session->setFlash(__( 'Invalid Request.')
                                        .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                                    'default',
                                    array('class'=>'alert alert-dismissable alert-danger '));

			$this->redirect(array('action' => 'index'));
		}

		$data['BusinessCategory']['id'] = $id;
		$data['BusinessCategory']['status'] = 0;
		$this->BusinessCategory->id = $id;

		if( $this->BusinessCategory->save($data) ){
			$this->Session->setFlash(__( 'Category is deleted.')
                                        .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                                    'default',
                                    array('class'=>'alert alert-dismissable alert-success '));
		}else{
			$this->Session->setFlash(__( 'Unable to delete Category.')
                                        .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                                    'default',
                                    array('class'=>'alert alert-dismissable alert-danger '));
		}

		$this->redirect(array('action' => 'index'));

	}

	public function edit($id = NULL ) {

		$user = CakeSession::read("Auth.User");

		if( $user['access_level'] !=6 ){
			$this->redirect( array('action' => 'index', 'controller' => 'dashboards' ) );
		}


		$options = array('conditions' => array('BusinessCategory.parent_id = 0 AND BusinessCategory.status = 1'), 'order' => array('BusinessCategory.category' => 'ASC' ) );
		$this->set('parentCategories', $this->BusinessCategory->find('all', $options));


		if (!$this->BusinessCategory->exists($id)) {
			$this->Session->setFlash(__( 'Invalid Request.')
                                        .'<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>',
                                    'default',
                                    array('class'=>'alert alert-dismissable alert-danger '));

			$this->redirect(array('action' => 'index'));
		}
		
		if ($this->request->is(array('post', 'put'))) {
			parent::save($id);
		} else {
			$options = array('conditions' => array('BusinessCategory.' . $this->BusinessCategory->primaryKey => $id));
			$this->request->data = $this->BusinessCategory->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */

	public function getCategoryByParentID( $parent_id = 0 ){

		$options = array('conditions' => array('parent_id'=>$parent_id, 'status' => 1 ), 'order' => array('BusinessCategory.category' => 'ASC' ) );

		$result  = $this->BusinessCategory->find('all', $options);

		$data = array();

		$data['data'] =  $result;
		echo json_encode( $data);

		$this->autoRender = false;
	}

}
