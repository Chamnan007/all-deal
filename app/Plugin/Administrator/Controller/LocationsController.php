<?php
App::uses('AdministratorAppController', 'Administrator.Controller');
App::uses('City', 'Administrator.Model');
App::uses('Location', 'Administrator.Model');
App::uses('Province', 'Administrator.Model');
/**
 * Locations Controller
 *
 * @property Location $Location
 * @property PaginatorComponent $Paginator
 */
class LocationsController extends AdministratorAppController {

/**
 * Components
 *
 * @var array
 */
	var $context = 'Location';
	var $uses  = array('Administrator.City' , 'Administrator.Location' , 'Administrator.Province');

/**
 * index method
 *
 * @return void
 */
	public function index() {

		$user =  CakeSession::read("Auth.User"); 

		if( $user['access_level'] != 6 ){
			$this->redirect(array('action' => 'index', 'controller' => 'dashboards' ));
		}else{

			$this->Province  = new Province();
			$this->set('provinces', $this->Province->find('all', array('conditions' => array('Province.status' => 1), 
																		'order' => 'Province.province_code ASC') ) ) ;


			$this->conditionFilter['Location.status'] = 1 ;

			$this->paginate['conditions'] = $this->conditionFilter;
			$this->paginate['order'] = array("Location.city_code" => 'ASC' ) ;
			$this->paginate['joins'] 	= array(
		                                    array('table' 	 => 'cities',
		                                        'alias' 	 => 'City',
		                                        'type' 		 => 'LEFT',
		                                        'conditions' => array('Location.city_code = City.city_code')
		                                      )
	                                    );
			$this->paginate['fields'] = array('Location.*', 'City.*' );

			parent::index();
			
		}
	}



public function beforeFilter(){
		
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$user =  CakeSession::read("Auth.User"); 

		if( $user['access_level'] != 6 ){
			$this->redirect(array('action' => 'index', 'controller' => 'dashboards' ));
		}else{
			if (!$this->Location->exists($id)) {
				throw new NotFoundException(__('Invalid location'));
			}
			$options = array('conditions' => array('Location.' . $this->Location->primaryKey => $id));
			$this->set('location', $this->Location->find('first', $options));
		}
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {

		$user =  CakeSession::read("Auth.User"); 

		if( $user['access_level'] != 6 ){
			$this->redirect(array('action' => 'index', 'controller' => 'dashboards' ));
		}else{
			// $this->City  = new City();
			// $this->set('cities', $this->City->find('all', array('conditions' => array('City.status=1') ) ) ) ;

			// $this->Province->recursive = 1;		
			$this->Province  = new Province();
			$this->set('provinces', $this->Province->find('all', array('conditions' => array('Province.status' => 1), 'order' => 'Province.province_code ASC') ) ) ;

			if ($this->request->is('post')) {
				parent::save();
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {

		$user =  CakeSession::read("Auth.User"); 

		if( $user['access_level'] != 6 ){
			$this->redirect(array('action' => 'index', 'controller' => 'dashboards' ));
		}else{

			if (!$this->Location->exists($id)) {
				throw new NotFoundException(__('Invalid location'));
			}
			if ($this->request->is(array('post', 'put'))) {

				parent::save($id);

			} else {

				$options = array('conditions' => array('Location.' . $this->Location->primaryKey => $id));
				$this->request->data = $this->Location->find('first', $options);
				
				$pro_code = $this->City->find('first', array('conditions' => array('City.status' => '1', 'City.city_code' => $this->request->data['Location']['city_code']) ) );
				$pro_code = $pro_code['City']['province_code'];

				$this->request->data['City']['province_code'] = $pro_code ;
				$this->City  = new City();

				$conds['conditions'] 	= array('City.status=1', 'City.province_code' => $pro_code );
				$conds['order']			= array('City.location_name' => 'ASC' );

				$this->set('cities', $this->City->find('all' , $conds ));

				$this->Province  = new Province();
				$this->set('provinces', $this->Province->find('all', array('conditions' => array('Province.status' => 1), 'order' => 'Province.province_code ASC') ) ) ;
			}
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {

		$user =  CakeSession::read("Auth.User"); 

		if( $user['access_level'] != 6 ){
			$this->redirect(array('action' => 'index', 'controller' => 'dashboards' ));
		}else{

			if (!$this->Location->exists($id)) {
				throw new NotFoundException(__('Invalid Location'));
			}

			$this->Location->id = $id;
			$this->request->data['Location']['status'] = 0;
			
			if ($this->Location->save($this->request->data)) {

				$this->Session->setFlash(__('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">×</button><b>Success!!!</b> The location has been deleted.</div>'));

				$obj 	= $this->Location->findById($id);
				$logMessage = json_encode($obj);
				parent::generateLog($logMessage,' DELETE :'.$id);

			} else {

				$this->Session->setFlash(__('<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">×</button><b>Failed!!!</b> The location could not be deleted. Please, try again.</div>'));
			}
		
			return $this->redirect(array('action' => 'index'));
		}

	}
	

	public function get_location_by_city_code( $city_code = null ){

		if( $city_code != null ){

			$location = new Location();
			$locations =  $location->find( 'all', array( 'conditions' => array('Location.city_code' => $city_code ))) ;

			$data = array();
			$data['data'] =  $locations;
			echo json_encode( $data);

			$this->autoRender = false;

		}
	}

}
