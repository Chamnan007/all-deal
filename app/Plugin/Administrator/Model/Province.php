<?php
/********************************************************************************
    File: Model Province
    Author: PHEA RATTANA

    Confidential ABi Technologies property.

  	Changed History:
  	Date			Author			Description
  	2014/02/04    	PHEA RATTANA	
*********************************************************************************/



App::uses('AdministratorAppModel', 'Administrator.Model');
/**
 * Province Model
 *
 */
class Province extends AdministratorAppModel {

/**
 * Validation rules
 *
 * @var array
 */


	public $actsAs = array(
	     'Sluggable' => array(
	             'fields' => 'province_name',
	             'scope' => false,
	             'conditions' => false,
	             'slugfield' => 'slug',
	             'separator' => '-',
	             'overwrite' => true,
	             'length' => 500,
	             'lower' => true
	         )
	   );
}
