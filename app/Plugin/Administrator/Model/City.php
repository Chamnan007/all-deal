<?php

/********************************************************************************
    File: City Model
    Author: PHEA RATTANA

    Confidential ABi Technologies property.

  	Changed History:
  	Date 					Author				Description
  	2014/mm/dd 				PHEA RATTANA		Initial
*********************************************************************************/


App::uses('AdministratorAppModel', 'Administrator.Model');
/**
 * City Model
 *
 */
class City extends AdministratorAppModel {

/**
 * Validation rules
 *
 * @var array
 */

	public $actsAs = array(
	     'Sluggable' => array(
	             'fields' => 'city_name',
	             'scope' => false,
	             'conditions' => false,
	             'slugfield' => 'slug',
	             'separator' => '-',
	             'overwrite' => true,
	             'length' => 500,
	             'lower' => true
	         )
	   );
	
}
