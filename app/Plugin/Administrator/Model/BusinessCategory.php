<?php
App::uses('AdministratorAppModel', 'Administrator.Model');
/**
 * BusinessCategory Model
 *
 */
class BusinessCategory extends AdministratorAppModel {

/**
 * Validation rules
 *
 * @var array
 */

    public $hasMany = array(
        'ChildCategory' => array(
            'className' => 'Administrator.BusinessCategory',
            'foreignKey' => 'parent_id',
            'dependent' => false,
            'conditions' => array('ChildCategory.status' => 1),
            'fields' => '',
            'order' => array('ChildCategory.category' => 'ASC' ),
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
            )
        );

    public $actsAs = array(
         'Sluggable' => array(
                 'fields' => 'category',
                 'scope' => false,
                 'conditions' => false,
                 'slugfield' => 'slug',
                 'separator' => '-',
                 'overwrite' => true,
                 'length' => 500,
                 'lower' => true
             )
       );

}
