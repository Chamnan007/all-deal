<?php

/********************************************************************************
    File: Commission Model
    Author: PHEA RATTANA

    Confidential ABi Technologies property.

  	Changed History:
  	Date 					Author				Description
  	2014/mm/dd 				PHEA RATTANA		Initial
*********************************************************************************/


App::uses('AdministratorAppModel', 'Administrator.Model');

/**
 * City Model
 *
 */

class PaymentHistory extends AdministratorAppModel {

	/**
	 * Validation rules
	 *
	 * @var array
	 */

	public $belongsTo = array(

		'BusinessDetail' 	=> array(
			'className' 	=> 'Administrator.Business',
			'foreignKey' 	=> 'business_id',
			'conditions' 	=> '',
			'fields' 		=> '',
			'order' 		=> ''
		),

		'CreatedBy' 	=> array(
			'className' 	=> 'Administrator.User',
			'foreignKey' 	=> 'created_by',
			'conditions' 	=> '',
			'fields' 		=> '',
			'order' 		=> ''
		),

	);
	
}
