<?php

/********************************************************************************
    File: Commission Model
    Author: PHEA RATTANA

    Confidential ABi Technologies property.

  	Changed History:
  	Date 					Author				Description
  	2014/mm/dd 				PHEA RATTANA		Initial
*********************************************************************************/


App::uses('AdministratorAppModel', 'Administrator.Model');
/**
 * City Model
 *
 */
class Buyer extends AdministratorAppModel {

/**
 * Validation rules
 *
 * @var array
 */

public $belongsTo = array(

		'SangkatInfo' 	 => array(
			'className'  => 'Sangkat',
			'foreignKey' => 'sangkat_id',
			'conditions' => '',
			'fields' 	 =>  "",
			'order' 	 => ''
		)

	);
	
}
