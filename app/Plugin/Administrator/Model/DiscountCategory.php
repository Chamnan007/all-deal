<?php
App::uses('AdministratorAppModel', 'Administrator.Model');
/**
 * DiscountCategory Model
 *
 */
class DiscountCategory extends AdministratorAppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $actsAs = array(
	     'Sluggable' => array(
	             'fields' => 'category',
	             'scope' => false,
	             'conditions' => false,
	             'slugfield' => 'slug',
	             'separator' => '-',
	             'overwrite' => true,
	             'length' => 500,
	             'lower' => true
	         )
	   );
}
