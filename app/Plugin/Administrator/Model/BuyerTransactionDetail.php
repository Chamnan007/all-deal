<?php

/********************************************************************************

File Name: Buyer Transaction Detail Model
Description: Model file for user

Powered By: ABi Investment Group Co., Ltd,

Changed History:

  	Date				Author				Description
  	2015/04/09    		Phea Rattana		Initial Version

*********************************************************************************/


App::uses('AdministratorAppModel', 'Administrator.Model');
/**
 * User Model
 *
 */
class BuyerTransactionDetail extends AdministratorAppModel {

/**
 * Validation rules
 *
 * @var array
 */
	
	public $belongsTo = array(

		'ItemDetail' => array(
			'className' => 'Administrator.BusinessMenuItem',
			'foreignKey' => 'item_id',
			'conditions' => '',
			'fields' =>  '',
			'order' => ''
		),

	);


}
