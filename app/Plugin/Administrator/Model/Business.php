<?php
App::uses('AdministratorAppModel', 'Administrator.Model');
/**
 * Business Model
 *
 * @property BusinessCategory $BusinessCategory
 * @property OperationHour $OperationHour
 * @property User $User
 */
class Business extends AdministratorAppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'id';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'MainCategory' => array(
			'className' => 'Administrator.BusinessCategory',
			'foreignKey' => 'business_main_category',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),

		'SubCategory' => array(
			'className' => 'Administrator.BusinessCategory',
			'foreignKey' => 'business_sub_category',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),

		'ApprovedBy' => array(
			'className' => 'Administrator.User',
			'foreignKey' => 'approved_by',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),

		'RegisteredBy' => array(
			'className' => 'Administrator.User',
			'foreignKey' => 'registered_by',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),

		'SangkatInfo' => array(
			'className' => 'Administrator.Sangkat',
			'foreignKey' => 'sangkat_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(

		'OperationHour' => array(
			'className' => 'Administrator.OperationHour',
			'foreignKey' => 'business_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),

		'User' => array(
			'className' => 'Administrator.User',
			'foreignKey' => 'business_id',
			'dependent' => false,
			'conditions' => '' ,
			'fields' => '',
			'order' => 'access_level',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),

		'Pictures' => array(
			'className' => 'Administrator.BusinessesMedia',
			'foreignKey' => 'business_id',
			'dependent' => false,
			'conditions' => array("media_type = 0") ,
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),

		'Videos' => array(
			'className' => 'Administrator.BusinessesMedia',
			'foreignKey' => 'business_id',
			'dependent' => false,
			'conditions' => array("media_type = 1")  ,
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),

		'Deals' => array(
			'className' => 'Administrator.Deal',
			'foreignKey' => 'business_id',
			'dependent' => false,
			'conditions' => array( 'Deals.isdeleted' => 0 ) ,
			'fields' => '',
			'order' => array( 'Deals.created' => 'DESC' ),
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),

		'Branches' => array(
			'className' => 'Administrator.BusinessBranch',
			'foreignKey' => 'business_id',
			'dependent' => false,
			'conditions' => array( 'Branches.status' => 1 ) ,
			'fields' => '',
			'order' => array( 'Branches.branch_name' => 'ASC' ),
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),

		'MenuCategories' => array(
			'className' => 'Administrator.BusinessMenu',
			'foreignKey' => 'business_id',
			'dependent' => false,
			'conditions' => array( 'MenuCategories.status' => 1 ) ,
			'fields' => '',
			'order' => array( 'MenuCategories.name' => 'ASC' ),
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

public $actsAs = array(
	     'Sluggable' => array(
	             'fields' => 'business_name',
	             'scope' => false,
	             'conditions' => false,
	             'slugfield' => 'slug',
	             'separator' => '-',
	             'overwrite' => true,
	             'length' => 500,
	             'lower' => true
	         )
	   );

}
