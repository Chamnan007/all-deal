<?php
App::uses('AdministratorAppModel', 'Administrator.Model');
/**
 * Business Model
 *
 * @property BusinessCategory $BusinessCategory
 * @property OperationHour $OperationHour
 * @property User $User
 */
class Deal extends AdministratorAppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'id';

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */

	public $belongsTo = array(
		

		'CreatedBy' => array(
			'className' => 'Administrator.User',
			'foreignKey' => 'created_by',
			'conditions' => '',
			'fields' =>  array('first_name', 'last_name'),
			'order' => ''
		),

		'UpdatedBy' => array(
			'className' => 'Administrator.User',
			'foreignKey' => 'updated_by',
			'conditions' => '',
			'fields' =>  array('first_name', 'last_name'),
			'order' => ''
		),

		'BusinessInfo' => array(
			'className' => 'Administrator.Business',
			'foreignKey' => 'business_id',
			'conditions' => '',
			'fields' =>  '',
			'order' => ''
		)
	);


	public $actsAs = array(
	     'Sluggable' => array(
	             'fields' => 'title',
	             'scope' => false,
	             'conditions' => false,
	             'slugfield' => 'slug',
	             'separator' => '-',
	             'overwrite' => true,
	             'length' => 500,
	             'lower' => true
	         )
	   );

	public $hasMany = array(

		'DealItemLink' => array(
			'className' => 'Administrator.DealItemLink',
			'foreignKey' => 'deal_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),

		'DealOfTheDay' => array(
			'className' => 'Administrator.DealOfTheDay',
			'foreignKey' => 'deal_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => array('DealOfTheDay.available_date' => 'ASC' ) ,
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),

	);
	

}
