<?php

/********************************************************************************

File Name: Buyer Transaction Model
Description: Model file for user

Powered By: ABi Investment Group Co., Ltd,

Changed History:

  	Date				Author				Description
  	2015/04/09    		Phea Rattana		Initial Version

*********************************************************************************/


App::uses('AdministratorAppModel', 'Administrator.Model');
/**
 * User Model
 *
 */
class BuyerTransaction extends AdministratorAppModel {

/**
 * Validation rules
 *
 * @var array
 */

	public $belongsTo = array(

		'BusinessDetail' => array(
			'className' => 'Administrator.Business',
			'foreignKey' => 'business_id',
			'conditions' => '',
			'fields' =>  '' ,
			'order' => ''
		),

		'BuyerInfo' => array(
			'className' => 'Administrator.Buyer',
			'foreignKey' => 'buyer_id',
			'conditions' => '',
			'fields' =>  '' ,
			'order' => ''
		),

		'MarkReceivedBy' => array(
			'className' => 'Administrator.User',
			'foreignKey' => 'mark_received_by',
			'conditions' => '',
			'fields' =>  '' ,
			'order' => ''
		),

		'DealInfo' => array(
			'className' => 'Administrator.Deal',
			'foreignKey' => 'deal_id',
			'conditions' => '',
			'fields' =>  '' ,
			'order' => ''
		)
		
	);
	
	public $hasMany = array(

		'TransactionDetail' => array(
			'className' => 'Administrator.BuyerTransactionDetail',
			'foreignKey' => 'transaction_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)

	);


}
