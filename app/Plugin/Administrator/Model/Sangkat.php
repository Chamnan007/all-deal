<?php
App::uses('AdministratorAppModel', 'Administrator.Model');
/**
 * Location Model
 *
 */
class Sangkat extends AdministratorAppModel {

/**
 * Validation rules
 *
 * @var array
 */


	public $belongsTo = array(
		'Location' => array(
			'className' => 'Location',
			'foreignKey' => 'location_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
	
	public $actsAs = array(
	     'Sluggable' => array(
	             'fields' => 'name',
	             'scope' => false,
	             'conditions' => false,
	             'slugfield' => 'slug',
	             'separator' => '-',
	             'overwrite' => true,
	             'length' => 500,
	             'lower' => true
	         )
	   );
}
