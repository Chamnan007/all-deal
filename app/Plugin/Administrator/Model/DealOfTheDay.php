<?php
App::uses('AdministratorAppModel', 'Administrator.Model');
/**
 * Business Model
 *
 * @property BusinessCategory $BusinessCategory
 * @property OperationHour $OperationHour
 * @property User $User
 */
class DealOfTheDay extends AdministratorAppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'id';


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */

	public $belongsTo = array(
		

		'DealDetail' => array(
			'className' => 'Administrator.Deal',
			'foreignKey' => 'deal_id',
			'conditions' => '',
			'fields' =>  '' ,
			'order' => ''
		),

		'CreatedBy' => array(
			'className' => 'Administrator.User',
			'foreignKey' => 'created_by',
			'conditions' => '' ,
			'fields' =>  array('first_name', 'last_name') ,
			'order' => ''
		)
	);


}
