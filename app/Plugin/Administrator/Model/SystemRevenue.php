<?php

/********************************************************************************
    File: Commission Model
    Author: PHEA RATTANA

    Confidential ABi Technologies property.

  	Changed History:
  	Date 					Author				Description
  	2014/mm/dd 				PHEA RATTANA		Initial
*********************************************************************************/


App::uses('AdministratorAppModel', 'Administrator.Model');
/**
 * City Model
 *
 */
class SystemRevenue extends AdministratorAppModel {

/**
 * Validation rules
 *
 * @var array
 */


public $belongsTo = array(

		'TransactionInfo' => array(
			'className' => 'Administrator.BuyerTransaction',
			'foreignKey' => 'transaction_id',
			'conditions' => '',
			'fields' =>  '',
			'order' => ''
		),

		'BusinessDetail' => array(
			'className' => 'Administrator.Business',
			'foreignKey' => 'business_id',
			'conditions' => '',
			'fields' =>  '',
			'order' => ''
		)

	);
	
}
