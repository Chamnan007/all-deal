<?php
App::uses('AdministratorAppModel', 'Administrator.Model');
/**
 * Location Model
 *
 */
class Location extends AdministratorAppModel {

/**
 * Validation rules
 *
 * @var array
 */
	
	public $actsAs = array(
	     'Sluggable' => array(
	             'fields' => 'location_name',
	             'scope' => false,
	             'conditions' => false,
	             'slugfield' => 'slug',
	             'separator' => '-',
	             'overwrite' => true,
	             'length' => 500,
	             'lower' => true
	         )
	   );
}
