<?php
App::uses('AdministratorAppModel', 'Administrator.Model');
/**
 * Business Model
 *
 * @property BusinessCategory $BusinessCategory
 * @property OperationHour $OperationHour
 * @property User $User
 */
class BusinessBranch extends AdministratorAppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'id';


}
