<?php

/********************************************************************************
    File: City Model
    Author: PHEA RATTANA

    Confidential ABi Technologies property.

  	Changed History:
  	Date 					Author				Description
  	2014/mm/dd 				PHEA RATTANA		Initial
*********************************************************************************/


App::uses('AdministratorAppModel', 'Administrator.Model');
/**
 * City Model
 *
 */
class City extends AdministratorAppModel {

/**
 * Validation rules
 *
 * @var array
 */

	// public $belongsTo = array(
	// 	'Province' => array(
	// 		'className' => 'Province',
	// 		'foreignKey' => 'province_code',
	// 		'conditions' => '',
	// 		'fields' => '',
	// 		'order' => ''
	// 	)
	// );
	
}
