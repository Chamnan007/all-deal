<?php
App::uses('AdministratorAppModel', 'Administrator.Model');
/**
 * Location Model
 *
 */
class Sangkat extends AdministratorAppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $belongsTo = array(
		'Location' => array(
			'className' => 'Location',
			'foreignKey' => 'location_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
