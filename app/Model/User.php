<?php

/********************************************************************************

File Name: User Model
Description: Model file for user

Powered By: ABi Investment Group Co., Ltd,

Changed History:

  	Date				Author				Description
  	2014/06/10    		Sim Chhayrambo		Initial Version

*********************************************************************************/


App::uses('AdministratorAppModel', 'Administrator.Model');
App::uses('SimplePasswordHasher', 'Controller/Component/Auth');
/**
 * User Model
 *
 */
class User extends AdministratorAppModel {

/**
 * Validation rules
 *
 * @var array
 */
	
	// public $belongsTo = array(
	// 			'BusinessInfo' => array(
	// 				'className' => 'Business',
	// 				'foreignKey' => 'business_id',
	// 				'conditions' => '',
	// 				'fields' => '',
	// 				'order' => ''
	// 			)
	// );

	public function beforeSave($options = array()) {
        
	    /* password hashing */

	    if ( isset($this->data[$this->alias]['password']) && $this->data[$this->alias]['password'] != "" ) {
	    	$passwordHasher = new SimplePasswordHasher(array('hashType'=>'sha256'));

	  		$hashedPassword = $passwordHasher->hash(
                $this->data[$this->alias]['password']
            );

	     	$this->data[$this->alias]['password'] = $hashedPassword;
	    }
	    
	    return parent::beforeSave($options);

	}


}
