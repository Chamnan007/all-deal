<?php
App::uses('AdministratorAppModel', 'Administrator.Model');
/**
 * Location Model
 *
 */

class DealItemLink extends AdministratorAppModel {

	public $displayField = 'id';

	public $belongsTo = array(

		'ItemDetail' => array(
			'className' => 'Administrator.BusinessMenuItem',
			'foreignKey' => 'item_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),

	);
	
}
