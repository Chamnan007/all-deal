<?php

/********************************************************************************

File Name: Buyer Model
Description: Model file for user

Powered By: ABi Investment Group Co., Ltd,

Changed History:

  	Date				Author				Description
  	2015/04/09    		Phea Rattana		Initial Version

*********************************************************************************/


App::uses('AdministratorAppModel', 'Administrator.Model');
App::uses('SimplePasswordHasher', 'Controller/Component/Auth');
/**
 * User Model
 *
 */
class Buyer extends AdministratorAppModel {

/**
 * Validation rules
 *
 * @var array
 */
public $displayField = 'id';

	public $belongsTo = array(

		'Referer' => array(
			'className' => 'Buyer',
			'foreignKey' => 'referer_id',
			'conditions' => '',
			'fields' =>  "",
			'order' => ''
		),

		'SangkatInfo' 	 => array(
			'className'  => 'Sangkat',
			'foreignKey' => 'sangkat_id',
			'conditions' => '',
			'fields' 	 =>  "",
			'order' 	 => ''
		)

	);

	public function beforeSave($options = array()) {
        
	    /* password hashing */

	    if ( isset($this->data[$this->alias]['password']) && $this->data[$this->alias]['password'] != "" ) {
	    	$passwordHasher = new SimplePasswordHasher(array('hashType'=>'sha256'));

	  		$hashedPassword = $passwordHasher->hash(
                $this->data[$this->alias]['password']
            );

	     	$this->data[$this->alias]['password'] = $hashedPassword;
	    }
	    
	    return parent::beforeSave($options);

	}


}
