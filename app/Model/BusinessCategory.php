<?php
App::uses('AdministratorAppModel', 'Administrator.Model');
/**
 * BusinessCategory Model
 *
 */
class BusinessCategory extends AdministratorAppModel {

/**
 * Validation rules
 *
 * @var array
 */

    public $hasMany = array(
        'ChildCategory' => array(
            'className' => 'BusinessCategory',
            'foreignKey' => 'parent_id',
            'dependent' => false,
            'conditions' => 'ChildCategory.status = 1',
            'fields' => '',
            'order' => array('ChildCategory.category' => 'ASC' ),
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
            )
        );

}
